/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! \file help/authors.hh
    \brief Présentation des programmeurs.
    \author \ref Guillaume_Terrissol
    \date 2 Avril 2002 - 22 Avril 2007
 */

/*! \page team_page Equipe
    \section Guillaume_Terrissol Guillaume Terrissol (Bug)
    <b>Chef de projet</b><br>
    <b>Programmeur principal</b><dl>
    <dt></dt><dd> GDK
    <dt></dt><dd> GFX
    <dt></dt><dd> ENG
    </dl>
    \htmlonly
    <A HREF=mailto:gterrissol@voila.fr>e-mail
    \endhtmlonly


    \section Laurent_David Laurent David (HBK)
    <b>Programmeur (?)</b><dl>
    <dt></dt><dd> AI
    </dl>


    \section Alexandre_Lautie Alexandre Lautie (Hercule)
    <b>Infographiste</b><br>
    <b>Programmeur</b><dl>
    <dt></dt><dd> SND
    </dl>


    \section Jean_Marc_Challier Jean-Marc Challier (Igx)
    <b>Programmeur</b><dl>
    <dt></dt><dd> SCR
    </dl>


    \section Nicolas_Peri Nicolas Peri (Nicox)
    <b>Programmeur</b><dl>
    <dt></dt><dd> ANI
    <dt></dt><dd> ENG
    </dl>
 

    \section Vincent_David Vincent David (Vincoof)
    <b>Programmeur</b><dl>
    <dt></dt><dd> GFX
    <dt></dt><dd> IDE
    <dt></dt><dd> ENG
    </dl>
 */
