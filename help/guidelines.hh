/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file help/guidelines.hh
    @brief Guide Lines.
    @author @ref Guillaume_Terrissol
    @date 9 Octobre 2001 - 5 Mars 2015

    R�gles de programmation pour le projet Old School Game.
 */

/*! @page DOC_Prefixes_Page Syst�me de pr�fixes
    @version 3.0

    Keep It Simple, Stupid.@n
    Apr�s une longue phase d'exp�rimentation, ce syst�me devrait rester jusqu'� la fin.@n

    @par Types
    <table border="1" cellpadding="5">
    <tr><th> R�gle                                                </th><th>        </th><th> Signification                </th>
    <tr><td><span class="Text_Key">     <b> [A-Z] \\w*</b> </span></td><td>class   </td><td>Classe C++ ou POD             </td>
    <tr><td><span class="String2">      <b>e[A-Z] \\w*</b> </span></td><td>enum    </td><td>Valeur d'un enum              </td>
    <tr><td><span class="String2">      <b>E[A-Z] \\w*</b> </span></td><td>enum    </td><td>Type �num�ration              </td>
    <tr><td><span class="Text_Key1">    <b>T[A-Z] \\w*</b> </span></td><td>template</td><td>Param�tre template (type)     </td>
    <tr><td><span class="String2">      <b>k[A-Z] \\w*</b> </span></td><td>constant</td><td>Constante                     </td>
    <tr><td><span class="Preprocessor"> <b> [A-Z_]*</b>    </span></td><td>macro   </td><td>Macro (�crite en majuscules)  </td>
    <tr><th> Pr�fixe optionel                                     </th><th>        </th><th> Signification                </th>
    <tr> <td> <b> P </b> </td> <td> Pointer     </td> <td> Smart Pointer                   </td>
    <tr> <td> <b> I </b> </td> <td> Iterator    </td> <td> It�rateur                       </td>
    </table>

    @par Variables
    <table border="1" cellpadding="5">
    <tr><th>Pr�fixe  </th><th> Scope ("port�e")            </th>
    <tr> <td> <b> p  </b> </td> <td> Param�tre                     </td>
    <tr> <td> <b> l  </b> </td> <td> Automatique (sur la pile)     </td>
    <tr><td><b>m </b></td><td>Membre d'instance            </td>
    <tr> <td> <b> s  </b> </td> <td> Statique (<b>ou</b> local)    </td>
    <tr> <td> <b> sm </b> </td> <td> Membre de classe              </td>
    <tr> <td> <b> g  </b> </td> <td> Global (� limiter au maximum) </td>
    </table>

    @par Fonctions et m�thodes
    Apr�s m�re r�flexion, et pour une meilleure int�gration avec Qt notamment, le style
    lower<a href="http://fr.wikipedia.org/wiki/CamelCase">CamelCase</a> a �t� choisi.@n
    Les pr�dicats sont conjugu�s � la 3�me personne du singulier.
    Ex: @c isValid(), @c hasChanged(), mais @c doSomething().

    @par Exemples :
    <i>Enum�ration</i>
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">enum</span><span class="Plain"> </span><span class="String2"><span style="text-decoration:underline">E</span>Boolean</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">        </span><span class="String2"><span style="text-decoration:underline">e</span>False</span><span class="Plain">,</span>
    <span class="Plain">        </span><span class="String2"><span style="text-decoration:underline">e</span>True</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    <span class="Plain">    </span><span class="String2"><span style="text-decoration:underline">E</span>Boolean</span><span class="Plain"> lState;</span>
    </code></pre>
    <i>Constante</i> & <i>macro</i>
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Storage_Type">F64</span><span class="Plain">   </span><span class="String2"><span style="text-decoration:underline">k</span>Pi</span><span class="Plain"> = </span><span class="Numeric_Const">3</span><span class="Storage_Type">.</span><span class="Numeric_Const">14159265358979323846</span><span class="Plain">;</span>
    </code></pre>
    <i>Classe</i> et <i>instance</i>
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key"><span style="text-decoration:underline">M</span>yClass</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">public</span><span class="Plain">:</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name M�thodes</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">            </span><span class="Text_Key"><span style="text-decoration:underline">M</span>yclass</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="Text_Key"><span style="text-decoration:underline">V</span>3</span><span class="Plain">  doSomething</span><span class="SteelBlueStyle">(</span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Text_Key"><span style="text-decoration:underline">M</span>4</span><span class="Plain">&amp; pMat</span><span class="SteelBlueStyle">)</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain">    </span><span class="Storage_Type">private</span><span class="Plain">:</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name Attributs</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain"> </span><span class="Storage_Type">static</span><span class="Plain"> </span><span class="Text_Key"><span style="text-decoration:underline">Q</span></span><span class="Plain">  smReference;</span>
    <span class="Plain">        </span><span class="Storage_Type">F32</span><span class="Plain"> mFloat1;</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    <span class="Plain"></span>
    <span class="Plain">    </span><span class="Text_Key"><span style="text-decoration:underline">M</span>yClass</span><span class="Plain"> lMyInstance;</span>
    </code></pre>
    <b>NB:</b> Quelques d�rogations peuvent bien s�r �tre envisag�es lorsque c'est pertinent.
 */

/*! @page DOC_Rules_Page R�gles diverses
    @version 0.6
    @warning En r�fection
    @par Fichiers "sources"
    Tout module <b>MODule</b> comporte les fichiers suivants :
    - MODule.hh : pr�-d�claration de toutes les classes et des macros "publiques" du module,
    - Enum.hh : d�claration des constantes et des �num�rations "publiques" du module,
    - ErrMsg.hh : d�claration des messages d'erreur du module,
    - All.hh : inclusion de tous les en-t�tes constituant l'interface "publique" du module.
    - MODule.cc : toutes les d�finitions n'ayant pu �tre plac�es dans un autre fichier source (comme les
    messages d'erreur).
    Auxquels s'ajoutent des tuples de fichiers pour les d�clarations et d�finitions de classes ou
    fonctions :
    - .hh : d�clarations,
    - .inl : impl�mentations inline,
    - .cc : impl�mentations non inline,
    - .tcc : impl�mentations de template � instancier explicitement.
    Voir le contenu de scripts/etc pour la structure de chaque fichier.

    @par Tabulations
    Les tabulations doivent �tre compos�es de 4 espaces (pas de caract�res tabulation). <br>
    Except�es les instructions pr�-processeur et les d�clarations d'espaces de noms (et quelques
    exceptions), tout code doit �tre pr�c�d� d'au moins <b>1</b> tabulation.

    <pre><code>
    <span class="Keyword">namespace</span><span class="Plain"> </span><span class="Text_Arg">MATH</span>
    <span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key">V3</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">...</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    <span class="SteelBlueStyle">}</span>
    </code></pre>
    Pour les d�clarations de classe, les noms de m�thodes doivent tous �tre align�s :
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="STL">String</span><span class="Plain">&amp;   name</span><span class="SteelBlueStyle">()</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;</span>
    <span class="Plain">    </span><span class="Storage_Type">void</span><span class="Plain">            init</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    </code></pre>
    La directive <i>inline</i> oblige donc a d�caler les d�clarations des m�thodes non-inline :
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="STL">String</span><span class="Plain">&amp;   name</span><span class="SteelBlueStyle">()</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;</span>
    <span class="Plain">             </span><span class="Storage_Type">void</span><span class="Plain">            init</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    </code></pre>
    Les qualificatifs <i>virtual</i> et <i>static</i>, quant � eux, ne d�calent pas les signatures :
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key">MyClass</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">public</span><span class="Plain">:</span>
    <span class="Plain"></span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">                  </span><span class="Text_Key">MyClass</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Storage_Type">virtual</span><span class="Plain">                         ~</span><span class="Text_Key">MyClass</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="STL">String</span><span class="Plain">&amp;   name</span><span class="SteelBlueStyle">()</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;</span>
    <span class="Plain">                </span><span class="Storage_Type">void</span><span class="Plain">             init</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Plain"> </span><span class="Storage_Type">static</span><span class="Plain"> </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">I32</span><span class="Plain">             count</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Plain">       </span><span class="SteelBlueStyle">(</span><span class="Storage_Type">...</span><span class="SteelBlueStyle">)</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    </code></pre>
    Notez le d�calage d'@b 1 caract�re de @e static.
    Pour les membres <i>template</i>, �crivez la signature sur 2 lignes.
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">template</span><span class="SteelBlueStyle">&lt;</span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key1">TType</span><span class="SteelBlueStyle">&gt;</span>
    <span class="Plain">    </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">void</span><span class="Plain">    </span><span class="Storage_Type">operator</span><span class="SteelBlueStyle">&gt;&gt;(</span><span class="Text_Key1">TType</span><span class="Plain">&amp;</span><span class="SteelBlueStyle">)</span><span class="Plain">;</span>
    </code></pre>

    @par Sauts de ligne
    Pour les d�clarations de classes, <b>1</b> saut de ligne apr�s les mots-clef public, protected et
    private. <b>2</b> sauts de lignes apr�s la fin des sections public et protected. Les sauts ne
    s'appliquent que pour le code, pas pour les commentaires doxygen.
    <pre><code>
    <span class="Plain">    </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key">MyClass</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">public</span><span class="Plain">:</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name M�thodes</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">                </span><span class="Text_Key">Myclass</span><span class="SteelBlueStyle">()</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="Storage_Type">void</span><span class="Plain">    doSomething</span><span class="SteelBlueStyle">(</span><span class="Text_Key">MyClass</span><span class="Plain">* pObj</span><span class="SteelBlueStyle">)</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="Text_Key">V3</span><span class="Plain">      doSomethingElse</span><span class="SteelBlueStyle">(</span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Text_Key">M4</span><span class="Plain">&amp; pMat</span><span class="SteelBlueStyle">)</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain">    </span><span class="Storage_Type">private</span><span class="Plain">:</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name Attributs</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain"> </span><span class="Storage_Type">static</span><span class="Plain"> </span><span class="Text_Key">Q</span><span class="Plain">       smReference;</span>
    <span class="Plain">        </span><span class="Storage_Type">float</span><span class="Plain">   mFloat1;</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    <span class="Plain"></span>
    <span class="Plain">    </span><span class="Text_Key">MyClass</span><span class="Plain">    lMyInstance;</span>
    </code></pre>

    @par Exemple complet
    <pre><code>
    <span class="Keyword">namespace</span><span class="Plain"> </span><span class="Text_Arg">PAK</span>
    <span class="SteelBlueStyle">{</span>
    <span class="Plain">    </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key">EngineFile</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">{</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name Classe amie</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">        </span><span class="Storage_Type">friend</span><span class="Plain">  </span><span class="Storage_Type">class</span><span class="Plain"> </span><span class="Text_Key">DiskFile</span><span class="Plain">;                                         </span><span class="My_comments">//!&lt; Fichier sur disque.</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">    </span><span class="Storage_Type">public</span><span class="Plain">:</span>
    <span class="Plain"></span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">                    </span><span class="Text_Key">EngineFile</span><span class="SteelBlueStyle">()</span><span class="Plain">;                          </span><span class="My_comments">//!&lt; Constructeur.</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name Acc�s aux donn�es</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Text_Key">BlockHdl</span><span class="Plain">&amp; dataHdl</span><span class="SteelBlueStyle">()</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;                        </span><span class="My_comments">//!&lt; Handle des donn�es du fichier.</span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">U32</span><span class="Plain">               size</span><span class="SteelBlueStyle">()</span><span class="Plain"> </span><span class="Storage_Type">const</span><span class="Plain">;                          </span><span class="My_comments">//!&lt; Taille du fichier.</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain"></span>
    <span class="Plain">    </span><span class="Storage_Type">protected</span><span class="Plain">:</span>
    <span class="Plain">        </span><span class="My_comments">//! \@name Etablissement des attributs</span>
    <span class="Plain">        </span><span class="My_comments">//\@{</span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">void</span><span class="Plain">             setDataHdl</span><span class="SteelBlueStyle">(</span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Text_Key">BlockHdl</span><span class="Plain">&amp; pDataHdl</span><span class="SteelBlueStyle">)</span><span class="Plain">;  </span><span class="My_comments">//!&lt; Etablit le handle des donn�es.</span>
    <span class="Plain">        </span><span class="Storage_Type">inline</span><span class="Plain">  </span><span class="Storage_Type">void</span><span class="Plain">             resize</span><span class="SteelBlueStyle">(</span><span class="Storage_Type">const</span><span class="Plain"> </span><span class="Storage_Type">U32</span><span class="Plain"> pSize</span><span class="SteelBlueStyle">)</span><span class="Plain">;                </span><span class="My_comments">//!&lt; Etablit la taille du fichier.</span>
    <span class="Plain">        </span><span class="My_comments">//\@}</span>
    <span class="Plain"></span>
    <span class="Plain">    </span><span class="Storage_Type">private</span><span class="Plain">:</span>
    <span class="Plain"></span>
    <span class="Plain">        </span><span class="Text_Key">BlockHdl</span><span class="Plain">    mDataHdl;                                            </span><span class="My_comments">//!&lt; R�f�rence sur le premier octet de donn�es.</span>
    <span class="Plain">        </span><span class="Storage_Type">U32</span><span class="Plain">          mSize;                                               </span><span class="My_comments">//!&lt; Taille du fichier (des donn�es).</span>
    <span class="Plain">    </span><span class="SteelBlueStyle">}</span><span class="Plain">;</span>
    <span class="SteelBlueStyle">};</span>
    </code></pre>

    Dans les fichiers sources, les d�finitions des m�thodes sont s�par�es par <b>2</b> lignes.
    @code
    inline const BlockHdl& EngineFile::DataHdl() const
    {
        return mDataHdl;
    }


    inline U32 EngineFile::Size() const
    {
        return mSize;
    }
    @endcode

    @par Accolades
    Les accolades dune paire doivent toujours �tre en vis-�-vis, m�me pour des blocs :
    @code
    if (...)
    {
        ...
    }
    else [...]
    {
        ...
    }
    ...
    try
    {
        ...
    }
    catch(...)
    {
        ...
    }
    @endcode
    
    @par Espaces
    Les r�gles d'utilisation seraient un peu longues � d�crire. De mani�re simplifi�e, les espaces sont
    utilis�s � l'int�rieur des expressions, mais pas aux extr�mit�s.
    @code
    U32 aValue = (aN / 10) + 1;
    if (Test()) ...
    while(!Fini) ...
    for(U32 aI = 0; aI < 256; ++aI) ...
    F32 Atan(F32 _X, F32 _Y);
    @endcode

    @par Documentation
    Liste des fichiers de chaque module :
    - Const.hh
    - Enum.hh
    - ErrMsg.hh
    - MODule.hh<br>
    Pour chaque [groupe de] classe(s) :
    - XXX.hh
    - XXX.inl
    - XXX.cc

    @par Espaces de noms
    Tous les types, classes, fonctions, constantes,.. doivent �tre encapsul�s dans des espaces de
    noms.<br>
    Ces espaces de noms doivent �tre compos�s de 2 � 4 lettres majuscules (SH, MEM, MATH,...).<br>
    Les using-directives et les using-d�clarations sont formellement interdites dans les en-t�tes (et pas
    sp�cialement encourag�es dans les sources).<br>
    Les espaces de noms anonymes ne sont autoris�s qu'afin de rendre "priv�s" certains composants
    d'espaces de noms particuliers.<br>

    Dans la documentation, tous les noms de classes, de variables, de m�thodes,... doivent �tre
    renseign�s au maximum en indiquant leur port�e. Ex MATH::V3::x
    
    @par Remarques particuli�res
    - Les blocs <b>switch</b> doivent <b><i>TOUJOURS</i></b> comporter un cas par d�faut.
    - La pr�-incr�mentation des variables doit �tre pr�f�r�e � leur post-incr�mentation � chaque fois que
    cela est possible.
    - A part les destructeurs, �viter, autant que faire se peut, les m�thodes publiques virtuelles.
    - Sauf cas tr�s particuliers, bannir les attributs prot�g�s.
 */
