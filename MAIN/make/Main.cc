/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file MAIN/make/Main.cc
    @brief Lancement du moteur.
    @author @ref Guillaume_Terrissol
    @date 6 D�cembre 2002 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.

    Le seul int�r�t de ce fichier est de permettre l'utilisation de la SDL sous mingw avec libtool (ar
    fait n'importe quoi et perd SDL_main en chemin).
 */

#include <OSGi/BundleMgr.hh>
#include <OSGi/ApplicationService.hh>

/*! main() classique.
    @param pArgC Nombre d'arguments pass�s sur la ligne de commande
    @param pArgV Liste des arguments pass�s sur la ligne de commande
    @return 0 si l'application s'est correctement termin�e, un code d'erreur sinon
*/
int main(int pArgC, char* pArgV[])
{
    // Configuration d'OSGi.
    OSGi::ApplicationService::Args lArgs
    {
        "bundleDir=../bundles",
        "cacheDir=cache",
        "tempDir=tmp",
        "permDir=sav",
        "configDir=cfg",
        "cleanCache",
        "verbose"
    };
    // Analyse les arguments.
    const std::string   kAll    = "--all";
    const std::string   kMain   = "--main=";

    std::string lService = BUNDLE_NAME;
    std::string lBundle  = lService;

    for(int a = 1; a < pArgC; ++a)
    {
        std::string lArgV = pArgV[a];
        if      (lArgV == kAll)
        {
            lBundle.clear();
        }
        else if (lArgV.compare(0, kMain.length(), kMain) == 0)
        {
            lService = lArgV.substr(kMain.length());
        }
        else
        {
            lArgs.push_back(lArgV);
        }
    }

    // On charge ce qu'il faut.
    OSGi::BundleMgr::Ptr    lMgr = OSGi::BundleMgr::make(lArgs);
    lMgr->loadBundles();
    if (!lBundle.empty())
    {
        OSGi::Bundle::Ptr   lBndl = lMgr->context()->findBundle(lService);
        lBndl->act(OSGi::kResolve, lMgr->context());
        lBndl->act(OSGi::kStart, lMgr->context());
    }
    else
    {
        lMgr->startBundles();
    }

    // Et c'est parti !
    return lMgr->services()->findByTypeAndName<OSGi::ApplicationService>(lService)->main(lArgs, lMgr->context());
}
