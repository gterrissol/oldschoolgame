/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file MAIN/make/fr.osg.main.cc
    @brief Programme principal (moteur).
    @author @ref Guillaume_Terrissol
    @date 13 Ao�t 2002 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <iostream>
#include <memory>

#include "ERRor/ErrMsg.hh"
#include "PAcKage/PakFileMgr.hh"
#include "PROGram/All.hh"
#include "STL/String.hh"

#include "UTIlity/Task.tcc"

#include <OSGi/Activator.hh>
#include <OSGi/ApplicationService.hh>
#include <OSGi/Context.hh>


    class OSGService : public OSGi::ApplicationService
    {
    public:
        OSGService() { }
        virtual ~OSGService() { }

    private:
        /*! Fonction principale du programme <b>osg</b>, lanc�e par main().
            @param pArgC Nombre d'arguments pass�s sur la ligne de commande
            @param pArgV Liste des arguments pass�s sur la ligne de commande
            @return 0 si l'application s'est correctement termin�e, un code d'erreur sinon
         */
        int process(const Args& pArgs, OSGi::Context*) override final
        {
            const std::string   kPak    = "pak=";
            std::string lPakFileName    = "./pak/osg_000.pak";

            for(auto lArgV : pArgs)
            {
                if (lArgV.compare(0, kPak.length(), kPak) == 0)
                {
                    lPakFileName = lArgV.substr(kPak.length());
                }
            }

            try
            {
#   if   defined(FRANCAIS)
                SET_ERR_MSG_LANGUAGE(eFr)
#   elif defined(ENGLISH)
                SET_ERR_MSG_LANGUAGE(eEn)
#   endif

                PROG::Hardware  lHardware{lPakFileName};
                PROG::Game::Ptr lGame = lHardware.boot();

                while(lGame.lock()->step())
                {
                    lHardware.tick();
                }
            }
            catch(ERR::Exception& pE)
            {
                std::cerr << "Exception captur�e : " << pE.what() << std::endl;

                return 1;
            }
            catch(std::exception& pE)
            {
                std::cerr << "Exception standard captur�e (" << typeid(pE).name() << ") : " << pE.what() << std::endl;

                return 1;
            }
            catch(...)
            {
                std::cerr << "Exception inconnue." << std::endl;
                throw;
            }

            return 0;
        }
    };

namespace OSG
{
    class Activator : public OSGi::Activator
    {
    public:
                Activator() : OSGi::Activator() { }
virtual         ~Activator() { }
    private:
        void    doStart(OSGi::Context* pContext) override final
                {
                    pContext->out() << "Starting OSG" << std::endl;
                    OSGi::ApplicationService::Ptr   lApp{new OSGService()};
                    pContext->services()->checkIn(BUNDLE_NAME, lApp);
                    pContext->out() << "OSG started" << std::endl;
                }
        void    doStop(OSGi::Context* pContext) override final
                {
                    pContext->out() << "Stopping OSG" << std::endl;
                    pContext->services()->checkOut(BUNDLE_NAME);
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(OSG::Activator)
OSGI_END_REGISTER_ACTIVATORS()
