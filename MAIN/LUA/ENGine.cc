/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "Bindings.hh"

/*! @file MAIN/LUA/ENGine.cc
    @brief Bindings des classes de la biblioth�que ENGine.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2009 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SLB/SLB.hpp>

#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "OBJect/GameObject.hh"
#include "OUTside/World.hh"
#include "UNIverse/GameUniverse.hh"

namespace LUA
{
    // OBJect
    namespace
    {
        void move(OBJ::GameObject* pObject, float pX, float pY)
        {
//            OUT::World*    pOutside    = dynamic_cast<OUT::World*>(UNI::GameUniverse::get()->activeWorlds()[0].get());
            MATH::V3       lPosition   = pObject->position();
            lPosition.x += F32(pX);
            lPosition.y += F32(pY);
//            lPosition.z  = F32(pOutside->height(MATH::V2(lPosition.x, lPosition.y)) + 0.5F);

            pObject->setPosition(lPosition);
        }

        float getX(OBJ::GameObject* pObject)
        {
            return pObject->position().x;
        }

        float getY(OBJ::GameObject* pObject)
        {
            return pObject->position().y;
        }

        float getZ(OBJ::GameObject* pObject)
        {
            return pObject->position().z;
        }

        void initOBJect(SLB::Manager* pManager)
        {
            SLB::Class<OBJ::GameObject, SLB::Instance::NoCopyNoDestroy>("OBJ::GameObject", pManager)
                .set("move",    move)
                .set("x",       getX)
                .set("y",       getY)
                .set("z",       getZ)
            ;
        }
    }


    /*!
     */
    void initENGine(SLB::Manager* pManager)
    {
        initOBJect(pManager);
    }
}
