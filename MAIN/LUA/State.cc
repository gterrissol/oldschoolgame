/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "State.hh"

/*! @file MAIN/LUA/State.cc
    @brief M�thodes (non-inline) de la classe LUA::State.
    @author @ref Guillaume_Terrissol
    @date 23 Avril 2009 - 18 Septembre 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SLB/SLB.hpp>

#include "CAMera/BaseCamera.hh"
#include "INPut/Controller.hh"
#include "OBJect/GameObject.hh"
#include "STL/SharedPtr.hh"

#include "Allocator.hh"
#include "Bindings.hh"

namespace LUA
{
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

    /*! @brief Custom script handler.
        @version 0.2

        La d�rivation depuis SLB::Script est n�cessaire pour sp�cialiser l'allocateur lua.
     */
    class Script : public SLB::Script
    {
    public:

        Script(SLB::Manager* pMgr, lua_Alloc pFunc = nullptr);
virtual ~Script();
    };


    /*!
     */
    Script::Script(SLB::Manager* pMgr, lua_Alloc pFunc )
        : SLB::Script(pMgr)
    {
        if (pFunc != nullptr)
        {
            setAllocator(pFunc, nullptr);
        }
        doString("SLB.using(SLB)\n");
    }


    /*!
     */
    Script::~Script() { }

//------------------------------------------------------------------------------
//                                P-Impl de State
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PROG::Game
        @version 0.2
     */
    class State::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        Private();                          //!< Constructeur.
        ~Private();                         //!< Destructeur.
        //@}

        UniquePtr<SLB::Manager> mManager;   //!< 
        UniquePtr<SLB::Script>  mScript;    //!< 
        lua_State*              mLuaState;  //!< 
        String                  mMain;      //!< 
    };


//------------------------------------------------------------------------------
//                 P-Impl de State : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    State::Private::Private()
        : mManager(new SLB::Manager())
        , mScript(nullptr)
        , mLuaState(nullptr)
        , mMain("")
    {
        initGDK(mManager.get());
        initGraphX(mManager.get());
        //initANImation(mLuaState);
        initENGine(mManager.get());

        mScript.reset(new Script(mManager.get(), allocate));
    }


    /*! Destructeur.
     */
    State::Private::~Private() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    State::State()
        : pthis()
    { }


    /*! Destructeur.
     */
    State::~State() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @todo Contr�les sur la validit� de la fonction.
     */
    void State::main(const char* pBody)
    {
        pthis->mMain = pBody;
    }


    /*!
     */
    void State::define(const char* pBody)
    {
        pthis->mScript->doString(pBody);
    }


    /*!
     */
    void State::define(const char* pName, CAM::Camera* pCamera)
    {
        pthis->mScript->set(pName, pCamera);
    }


    /*!
     */
    void State::define(const char* pName, OBJ::GameObject* pObject)
    {
        pthis->mScript->set(pName, pObject);
    }


    /*!
     */
    void State::define(const char* pName, INP::Controller* pController)
    {
        pthis->mScript->set(pName, pController);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void State::run()
    {
        pthis->mScript->doString(pthis->mMain.c_str());
    }
}
