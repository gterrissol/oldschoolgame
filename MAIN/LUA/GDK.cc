/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "Bindings.hh"

/*! @file MAIN/LUA/GDK.cc
    @brief M�thodes (non-inline) des classes ....
    @author @ref Guillaume_Terrissol
    @date 15 Avril 2009 - 6 Juin 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SLB/SLB.hpp>

#include "TIME/MainTimer.hh"

namespace LUA
{
    // TIME
    namespace
    {
        /*! @return Le temps �coul� lors de la derni�re frame, en ms
         */
        F32::TType frameDelta()
        {
            return TimeMgr->delta() / 1000.0F;
        }

        void initTime(SLB::Manager* pManager)
        {
            SLB::Class<TIME::MainTimer, SLB::Instance::NoCopyNoDestroy>("TIME", pManager)
                .set("frameDelta", frameDelta);
        }
    }

    /*!
     */
    void initGDK(SLB::Manager* pManager)
    {
        initTime(pManager);
    }
}
