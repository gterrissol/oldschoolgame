/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#pragma GCC diagnostic ignored "-Wunused-parameter"

#include "Bindings.hh"

/*! @file MAIN/LUA/GraphX.cc
    @brief M�thodes (non-inline) des classes ....
    @author @ref Guillaume_Terrissol
    @date 15 Avril 2009 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SLB/SLB.hpp>

#include "CAMera/BaseCamera.hh"
#include "INPut/Controller.hh"
#include "INPut/InputMgr.hh"
#include "MATH/V2.hh"
#include "MATH/V3.hh"

namespace LUA
{
    // CAMera
    namespace
    {
        void rotate(CAM::Camera* pCamera, F32::TType pAlpha)
        {
            pCamera->rotate(F32(pAlpha));
        }

        F32::TType getAngle(CAM::Camera* pCamera)
        {
            return pCamera->viewAngle();
        }

        F32::TType getZoom(CAM::Camera* pCamera)
        {
            return pCamera->zoom();
        }

        void zoom(CAM::Camera* pCamera, F32::TType pF)
        {
            pCamera->zoom(F32(pF));
        }

        void setZoom(CAM::Camera* pCamera, F32::TType pZ)
        {
            return pCamera->setZoom(F32(pZ));
        }

        void move(CAM::Camera* pCamera, F32::TType pX, F32::TType pY)
        {
            pCamera->move(MATH::V3(F32(pX), F32(pY), k0F));
        }

        void moveTo(CAM::Camera* pCamera, F32::TType pX, F32::TType pY, F32::TType pZ)
        {
            pCamera->moveTo(MATH::V3(F32(pX), F32(pY), F32(pZ)));
        }

        float getX(CAM::Camera* pCamera)
        {
            return pCamera->position().x;
        }

        float getY(CAM::Camera* pCamera)
        {
            return pCamera->position().y;
        }

        float getZ(CAM::Camera* pCamera)
        {
            return pCamera->position().z;
        }

        void initCAMera(SLB::Manager* pManager)
        {
            SLB::Class<CAM::Camera, SLB::Instance::NoCopyNoDestroy>("CAM::Camera", pManager)
                .set("rotate",  rotate)
                .set("yaw",     getAngle)
                .set("getZoom", getZoom)
                .set("zoom",    zoom)
                .set("setZoom", setZoom)
                .set("move",    move)
                .set("moveTo",  moveTo)
                .set("x",       getX)
                .set("y",       getY)
                .set("z",       getZ)
            ;
        }
    }


    // INPut
    namespace
    {
        F32::TType getX1(INP::Controller* pController)
        {
            return pController->movement().x;
        }

        F32::TType getY1(INP::Controller* pController)
        {
            return pController->movement().y;
        }

        F32::TType getX2(INP::Controller* pController)
        {
            return pController->movement().x;
        }

        F32::TType getY2(INP::Controller* pController)
        {
            return pController->movement().y;
        }

        I32::TType pressed(INP::Controller* pController, INP::EButtons pButton)
        {
            return pController->pressed(INP::EButtons(pButton));
        }

        I32::TType released(INP::Controller* pController, INP::EButtons pButton)
        {
            return pController->released(INP::EButtons(pButton));
        }

        void initINPut(SLB::Manager* pManager)
        {
            SLB::Class<INP::InputMgr, SLB::Instance::NoCopyNoDestroy>("INP", pManager)
                .enumValue("EButtons::eB1",  INP::eButton1)
                .enumValue("EButtons::eB2",  INP::eButton2)
                .enumValue("EButtons::eB3",  INP::eButton3)
                .enumValue("EButtons::eB4",  INP::eButton4)
                .enumValue("EButtons::eB5",  INP::eButton5)
                .enumValue("EButtons::eB6",  INP::eButton6)
                .enumValue("EButtons::eB7",  INP::eButton7)
                .enumValue("EButtons::eB8",  INP::eButton8)
                .enumValue("EButtons::eB9",  INP::eButton9)
                .enumValue("EButtons::eB10", INP::eButtonA)
            ;

            SLB::Class<INP::Controller, SLB::Instance::NoCopyNoDestroy>("INP::Controller", pManager)
                .set("getX1",       getX1)
                .set("getY1",       getY1)
                .set("getX2",       getX2)
                .set("getY2",       getY2)
                .set("pressed",     pressed)
                .set("released",    released)
            ;
        }
    }


    /*!
     */
    void initGraphX(SLB::Manager* pManager)
    {
        initCAMera(pManager);
        initINPut(pManager);
    }
}
