/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Allocator.hh"

/*! @file MAIN/LUA/Allocator.cc
    @brief M�thodes (non-inline) de la classe LUA::Allocator.
    @author @ref Guillaume_Terrissol
    @date 28 Novembre 2008 - 8 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstring>

#include "MEMory/MemoryMgr.hh"

namespace LUA
{
//------------------------------------------------------------------------------
//                       Allocatation de M�moire pour LUA
//------------------------------------------------------------------------------

    /*! Alloue, r�alloue ou d�salloue un bloc de m�moire pour une machine Lua.<br>
        @param pPtr     Pointeur sur le bloc m�moire � r�allouer ou d�sallouer
        @param pOldSize Taille actuelle du bloc m�moire <i>pPtr</i>
        @param pNewSize Nouvelle taille souhait�e pour <i>pPtr</i> (0 pour d�sallouer)
        @internal Cette fonction suit les recommandations � suivre pour impl�menter un allocateur Lua, et
        n'est pas destin�e � �tre appel�e autrement.
     */
    void* allocate(void*, void* pPtr, std::size_t pOldSize, std::size_t pNewSize)
    {
        if (pNewSize == 0)
        {
            MEM::MemoryMgr::get()->deallocate(pPtr);
            return nullptr;
        }

        void*   lNewBlock = MEM::MemoryMgr::get()->allocate(pNewSize);

        if (lNewBlock != nullptr)
        {
            if (pOldSize != 0)
            {
                if (pPtr != nullptr)
                {
                    memcpy(lNewBlock, pPtr, (pOldSize < pNewSize) ? pOldSize : pNewSize);

                    MEM::MemoryMgr::get()->deallocate(pPtr);
                }
            }

            return lNewBlock;
        }
        else
        {
            return nullptr;
        }
    }
}
