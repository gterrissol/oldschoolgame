/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LUA_ALL_HH
#define LUA_ALL_HH

/*! @file MAIN/LUA/All.hh
    @brief Interface publique du module @ref LUA.
    @author @ref Guillaume_Terrissol
    @date 28 Novembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace LUA   //! D�finition des liaisons avec Lua.
{
    /*! @namespace LUA
        @version 0.2

     */

    /*! @defgroup LUA LUA : Scripts LUA
        <b>namespace</b> LUA et @ref LUA_API_Page.
     */

}

#include "Allocator.hh"

#endif  // De LUA_ALL_HH
