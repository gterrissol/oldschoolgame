    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "LUA.hh"

/*! @file MAIN/LUA/LUA.cc
    @brief D�finitions diverses du module @ref LUA.
    @author @ref Guillaume_Terrissol
    @date 28 Novembre 2008 - 29 Avril 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace LUA
{
    /*! @page LUA_API_Page API lua
        @note Coming soon...
     */
}
