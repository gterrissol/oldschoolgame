/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LUA_STATE_HH
#define LUA_STATE_HH

#include "LUA.hh"

/*! @file MAIN/LUA/State.hh
    @brief En-t�te de la classe LUA::State.
    @author @ref Guillaume_Terrissol
    @date 22 Avril 2009 - 18 Septembre 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CAMera/CAMera.hh"
#include "INPut/INPut.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "OBJect/OBJect.hh"

namespace LUA
{
    class State : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        State();    //!< Constructeur.
        ~State();   //!< Destructeur.
        //@}
        //! @name D�finitions de variables et de fonctions
        //@{
        void    main(const char* pBody);
        void    define(const char* pBody);
        void    define(const char* pName, CAM::Camera* pCamera);
        void    define(const char* pName, OBJ::GameObject* pObject);
        void    define(const char* pName, INP::Controller* pController);
        //@}
        void    run();
        //void    off();

    private:

        FORBID_COPY(State)
        PIMPL()
    };
}

#endif  // De LUA_STATE_HH
