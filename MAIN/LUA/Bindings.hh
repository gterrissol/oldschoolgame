/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LUA_BINDINGS_HH
#define LUA_BINDINGS_HH

#include "LUA.hh"

/*! @file MAIN/LUA/Bindings.hh
    @brief D�clarations priv�es du module LUA
    @author @ref Guillaume_Terrissol
    @date 23 Avril 2009 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace SLB
{
    class Manager;
}

namespace LUA
{
    //! @name Cr�ation de bindings lua pour les biblioth�ques
    //@{
    void    initGDK(SLB::Manager* pManager);        //!< ... GDK.
    void    initGraphX(SLB::Manager* pManager);     //!< ... GraphX.
    void    initANImation(SLB::Manager* pManager);  //!< ... ANImation.
    void    initENGine(SLB::Manager* pManager);     //!< ... ENGine.
    //@}
}

#endif  // De LUA_BINDINGS_HH
