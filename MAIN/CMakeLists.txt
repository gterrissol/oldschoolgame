set(MAIN_MODULES LUA PROGram)
foreach(module ${MAIN_MODULES})
    add_subdirectory(${module})
endforeach()

add_bundle(fr.osg.main SOURCES make/fr.osg.main.cc VERSION 1.3.1 DEPENDS SDL fr.osg.gdk fr.osg.gx fr.osg.ani fr.osg.eng ARCHIVES ${MAIN_MODULES})

use_externals(fr.osg.main SDL)
