/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROG_PROGRAM_HH
#define PROG_PROGRAM_HH

/*! @file MAIN/PROGram/PROGram.hh
    @brief Pr�-d�clarations du module @ref PROGram.
    @author @ref Guillaume_Terrissol
    @date 24 Avril 2008 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PROG
{
    class Game;
    class GameStep;
    class Hardware;
    class MBR;
}

#endif  // De PROG_PROGRAM_HH
