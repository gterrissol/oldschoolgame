/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROG_ALL_HH
#define PROG_ALL_HH

/*! @file MAIN/PROGram/All.hh
    @brief Interface publique du module @ref PROGram.
    @author @ref Guillaume_Terrissol
    @date 2 Janvier 2003 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PROG   //! Programme.
{
    /*! @namespace PROG
        @version 0.66

        Ce module est le coeur du moteur.
     */

    /*! @defgroup PROGram PROGram : Application principale
        <b>namespace</b> PROG.
     */
}

#include "GameSteps.hh"
#include "Hardware.hh"

#endif  // De PROG_ALL_HH
