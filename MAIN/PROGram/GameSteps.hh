/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROG_GAME_HH
#define PROG_GAME_HH

#include "PROGram.hh"

/*! @file MAIN/PROGram/GameSteps.hh
    @brief En-t�te des classes PROG::GameStep & d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2008 - 13 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GEOmetry/BaseGeometry.hh"
#include "GraphXView/Viewport.hh"
#include "MATerial/Image.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/Handle.hh"
#include "PAcKage/LoaderSaver.hh"
#include "UNIverse/GameUniverse.hh"

#include "Enum.hh"

namespace PROG
{
//------------------------------------------------------------------------------
//                                Machine � Etats
//------------------------------------------------------------------------------

    /*! @brief Machine � �tats du moteur de jeu.
        @version 0.3
     */
    class Game : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        using Ptr = WeakPtr<Game>;                              //!< Pointeur sur moteur de jeu.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                                Game(GXV::Viewport::Ptr pViewport,
                                     PAK::Loader&       pLoader);  //!< Constructeur (chargement).
                                ~Game();                        //!< Destructeur.
        //@}
        bool                    step();                         //!< Avanc�e du moteur.
        //! @name Acc�s � certaines donn�es de jeu
        //@{
        UNI::Universe::Ptr      gameUniverse();                 //!< Univers de jeu.
        GEO::GeometryMgr::Ptr   geometries();                   //!< G�om�tries.
        MAT::ImageMgr::Ptr      images();                       //!< Images.
        SHD::Library::Ptr       shaders();                      //!< Biblioth�que de shaders.
        //@}
    private:

        FORBID_COPY(Game)
        PIMPL()
    };
}

#endif  // De PROG_GAME_HH
