/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameSteps.hh"

/*! @file MAIN/PROGram/GameSteps.cc
    @brief M�thodes (non-inline) des classes PROG::GameStep & d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2008 - 14 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CAMera/BaseCamera.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "GraphXView/EngineViewport.hh"
#include "INPut/Controller.hh"
#include "LUA/State.hh"
#include "MATH/M4.hh"
#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "PAcKage/LoaderSaver.hh"
#include "RenDeR/Renderer.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"
#include "TILE/BaseTile.hh"
#include "TIME/MainTimer.hh"


namespace OBJ
{
    /*!
     */
    class Renderer : public RDR::Renderer
    {
    public:
                Renderer(GameObject* pObject) : mObject(pObject) { }
virtual         ~Renderer() { }

    private:

virtual void    dumpAppearances()
        {
            GameObject::AppearancePtr lApp = mObject->appearance();

            MATH::M4        lTransform;
            lTransform.loadIdentity();
            const MATH::V3& lPos = mObject->position();
            lTransform.L.x = lPos.x;
            lTransform.L.y = lPos.y;
            lTransform.L.z = lPos.z;

            pushAppearance(lApp, lTransform);
        }
        GameObject* mObject;
    };
}


namespace PROG
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class GameData : public MEM::OnHeap
    {
    public:
        typedef SharedPtr<GameData> Ptr;

        GameData(GXV::Viewport::Ptr pViewport)
            : MEM::OnHeap{}
            , mViewport{pViewport}
            , mController{}
            , mPlayer{}
        { }
        void init()
        {
            mPlayer = STL::makeShared<OBJ::GameObject>();
            mController = viewport()->makeController();
            // Tente l'activation d'un joypad.
            if (!mController->bindToJoypad(I32(1)))
            {
                // Sinon, configuration du clavier.
                mController->bindToKeyboard();
            }
        }
        void fini()
        {
            mController.reset();
            mPlayer.reset();
        }
virtual ~GameData() { }

        GXV::Viewport::Ptr  viewport()
        {
            return mViewport.lock();
        }
virtual CAM::Camera::Ptr    camera() = 0;

        WeakPtr<GXV::Viewport::Ptr::element_type>   mViewport;
        SharedPtr<INP::Controller>  mController;    //!< 
        SharedPtr<OBJ::GameObject>  mPlayer;        //!< 
    };


//------------------------------------------------------------------------------
//                             Etat du Moteur de Jeu
//------------------------------------------------------------------------------

    /*! @brief Etat du moteur de jeu.
        @version 0.2
     */
    class GameStep : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                GameStep(String pName, GameData::Ptr pData, PAK::Loader&& pLoader); //!< Constructeur.
        void    init();
virtual         ~GameStep();                        //!< Destructeur.
 static void    saveDefault(PAK::Saver& pSaver);    //!< Cr�ation d'un �tat de jeu minimal.
        //@}
        bool    step();                             //!< Avanc�e du moteur.

    protected:

        void    save();                             //!< Sauvegarde.


    private:

        UniquePtr<LUA::State>       mLua;           //!< 
        PAK::FileHdl                mFileHdl;       //!< Handle du fichier de donn�es de l'�tape de jeu.
        WeakPtr<GameData>           mCommonData;    //!< 
        SharedPtr<OBJ::Renderer>    mRenderer;      //!< 
        String                      mName;          //!< 
    };


//------------------------------------------------------------------------------
//                    Game Step : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    GameStep::GameStep(String pName, GameData::Ptr pData, PAK::Loader&& pLoader)
        : mLua(new LUA::State())
        , mFileHdl(pLoader.fileHdl())
        , mCommonData(pData)
        , mRenderer{}
        , mName(pName)
    { }


    /*!
     */
    void GameStep::init()
    {
        mCommonData.lock()->init();
        mRenderer = STL::makeShared<OBJ::Renderer>(mCommonData.lock()->mPlayer.get());
        mCommonData.lock()->viewport()->attach(mRenderer);

        mLua->define("TheCamera",     mCommonData.lock()->camera().get());
        mLua->define("TheController", mCommonData.lock()->mController.get());
        mLua->define("ThePlayer",     mCommonData.lock()->mPlayer.get());

        // Initialisation des �tats de jeu (pour l'instant, seulement Run, � remanier...).
        if (mName == "Run")
        {
            const char* kDefs =
                "lCosYaw = 0.0\n"
                "lSinYaw = 0.0\n"
                "kCamDY  = 7.5\n"
                "kCamDZ  = 15.0\n";
            mLua->define(kDefs);

            const char* kCam =
                "function update_camera()\n"
                "    lDelta  = TIME.frameDelta()\n"
                "\n" // Contr�le cam�ra.
                "    if (0 < TheController:pressed(INP.EButtons.eB5)) then\n"
                "        TheCamera:rotate(- 0.0035 * lDelta)\n"
                "    end\n"
                "    if (0 < TheController:pressed(INP.EButtons.eB6)) then\n"
                "        TheCamera:rotate(  0.0035 * lDelta)\n"
                "    end\n"
                "end\n";
            mLua->define(kCam);

            const char* kChar =
                "function update_character()\n"
                "    lDelta  = TIME.frameDelta()\n"
                "    lCoeff  = lDelta / 60.0\n"
                "\n" // Contr�le perso.
                "    lMoveX  =   lCoeff * TheController:getX1()\n"
                "    lMoveY  = - lCoeff * TheController:getY1()\n"
                "    lYaw    = - TheCamera:yaw()\n"
                "\n"
                "    lCosYaw, lSinYaw = math.cos(lYaw), math.sin(lYaw)\n"
                "    ThePlayer:move(lCosYaw * lMoveX - lSinYaw * lMoveY, lSinYaw * lMoveX + lCosYaw * lMoveY)\n"
                "end\n";
            mLua->define(kChar);

            const char* kUpdate =
                "function adapt_camera()\n"
                "\n" // Adaptation automatique : zoom.
                "    lDeltaZ = TheCamera:getZoom() - ThePlayer:z()\n"
                "    lNewRatio = 0.02\n"
                "    lOldRatio = 1.0 - lNewRatio\n"
                "    TheCamera:setZoom(lOldRatio * TheCamera:getZoom() + lNewRatio * (ThePlayer:z() + kCamDZ))\n"
                "\n" // Adaptation automatique : position.
                "    lPX, lPY = ThePlayer:x(), ThePlayer:y()\n"
                "    lCX, lCY = TheCamera:x(), TheCamera:y()\n"
                "    lDeltaX = 0.02 * ((  lCosYaw * lPX + lSinYaw * lPY) - (  lCosYaw * lCX + lSinYaw * lCY))\n"
                "    lDeltaY = 0.02 * ((- lSinYaw * lPX + lCosYaw * lPY) - (- lSinYaw * lCX + lCosYaw * lCY) + kCamDY)\n"
                "    TheCamera:move(lDeltaX, lDeltaY)\n"
                "end\n";
            mLua->define(kUpdate);

            const char* kMain =
                "update_camera()\n"
                "update_character()\n"
                "adapt_camera()\n";
            mLua->main(kMain);
        }
    }


    /*! Destructeur.
     */
    GameStep::~GameStep() = default;


    /*!
     */
    void GameStep::saveDefault(PAK::Saver& pSaver)
    {
        pSaver << String("Undefined");
    }

 
//------------------------------------------------------------------------------
//                             Game Step : M�thodes
//------------------------------------------------------------------------------

    /*! Avanc�e du moteur.
     */
    bool GameStep::step()
    {
        //ImageMgr->deliverResources();

        GameData::Ptr   lGameData = mCommonData.lock();
        if (lGameData->mController->pressed(INP::eButtonA) == 0)
        {
            mLua->run();

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! Sauvegarde.
     */
    void GameStep::save()
    {
        PAK::Saver lSaver(mFileHdl);
        lSaver << mName;
    }


//------------------------------------------------------------------------------
//                                P-Impl de Game
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PROG::Game
        @version 0.2
     */
    class Game::Private
        : public GameData
        , public STL::enable_shared_from_this<Game::Private>
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<GameStep> GameStepPtr;                    //!< Pointeur sur �tat de jeu.
        //@}
                            Private(GXV::Viewport::Ptr pViewport,
                                    PAK::Loader&       pLoader);    //!< Constructeur.
virtual                     ~Private();

        CAM::Camera::Ptr    camera() override;

        GameStepPtr             mCurrent;                           //!< Etat de jeu actuel.
        CAM::Camera::Ptr        mCamera;                            //!< Camera de jeu.
        GEO::GeometryMgr::Ptr   mGeometries;
        MAT::ImageMgr::Ptr      mImages;
        UNI::Universe::Ptr      mUniverse;
        Vector<GameStepPtr>     mGameSteps;                         //!< Etats de jeu (apr�s les ressources).
    };


//------------------------------------------------------------------------------
//                         P-Impl de Game : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Game::Private::Private(GXV::Viewport::Ptr pViewport, PAK::Loader& pLoader)
        : GameData{pViewport}
        , mCurrent()
        , mCamera{}
        , mGeometries{}
        , mImages{}
        , mUniverse()
        , mGameSteps()
    {
        mGameSteps.resize(eGameStepCount);
        PAK::FileHdl    lHdl = PAK::eBadHdl;
        pLoader >> lHdl;    // Main.
        pLoader >> lHdl;    // Load.
        mGameSteps[eLoad]      = STL::makeShared<GameStep>("Load",      shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Menu.
        mGameSteps[eMenu]      = STL::makeShared<GameStep>("Menu",      shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Movie.
        mGameSteps[eMovie]     = STL::makeShared<GameStep>("Movie",     shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Pause.
        mGameSteps[ePause]     = STL::makeShared<GameStep>("Pause",     shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Run.
        mGameSteps[eRun]       = STL::makeShared<GameStep>("Run",       shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Save.
        mGameSteps[eSave]      = STL::makeShared<GameStep>("Save",      shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Start.
        mGameSteps[eStart]     = STL::makeShared<GameStep>("Start",     shared_from_this().lock(), PAK::Loader(lHdl));
        pLoader >> lHdl;    // Sysconfig.
        mGameSteps[eSysConfig] = STL::makeShared<GameStep>("SysConfig", shared_from_this().lock(), PAK::Loader(lHdl));

//=> cr�ation ou chargement : 2 constructeur pour Private.

        mCurrent = mGameSteps[eRun];

        {
            pLoader >> lHdl;
            PAK::Loader lUniverseLoader(lHdl);
            mUniverse = STL::makeShared<UNI::Universe>(lUniverseLoader);
            mUniverse->render(viewport());
        }
        {
            pLoader >> lHdl;
            PAK::Loader lGeometriesLoader(lHdl);
            mGeometries = STL::makeShared<GEO::GeometryMgr>();
            mGeometries->load(lGeometriesLoader);
        }
        {
            pLoader >> lHdl;
            PAK::Loader lImagesLoader(lHdl);
            mImages = STL::makeShared<MAT::ImageMgr>();
            mImages->load(lImagesLoader);
        }

        pLoader >> lHdl;
        mUniverse->viewport()->initShaders(lHdl);

        // Pour tester :
        mGameSteps[eRun]->init();
        //mPlayer->setAppearance(APP::Primitive::create(APP::eCube, F32(1.0F), CLR::kMagenta));
        //pGraphicDevice->grab(mPlayer->appearance());
    }

    //!
    Game::Private::~Private()
    {
        mUniverse->viewport()->freeShaders();
        mUniverse->render({});
        fini();
    }


    /*!
     */
    CAM::Camera::Ptr Game::Private::camera()
    {
        if (mCamera == nullptr)
        {
            mCamera   = viewport()->camera();
        }

        return mCamera;
    }


//------------------------------------------------------------------------------
//                                Game : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (chargement).
     */
    Game::Game(GXV::Viewport::Ptr pViewport, PAK::Loader& pLoader)
        : pthis{pViewport, pLoader}
    { }


    /*! Destructeur.
     */
    Game::~Game() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    bool Game::step()
    {
        pthis->mGeometries->deliverResources();

        return pthis->mCurrent->step();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return Le "singleton" univers
     */
    UNI::Universe::Ptr Game::gameUniverse()
    {
        return pthis->mUniverse;
    }


    /*! @return L'ensemble des ressources g�om�tries du jeu.
     */
    GEO::GeometryMgr::Ptr Game::geometries()
    {
        return pthis->mGeometries;
    }


    /*! @return L'ensemble des ressources g�om�tries du jeu.
     */
    MAT::ImageMgr::Ptr Game::images()
    {
        return pthis->mImages;
    }

    /*!
     */
    SHD::Library::Ptr Game::shaders()
    {
        if (auto lViewport = pthis->viewport())
        {
            return lViewport->shaders();
        }
        else
        {
            return {};
        }
    }
}
