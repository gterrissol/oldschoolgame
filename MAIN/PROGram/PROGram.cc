/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "PROGram.hh"

/*! @file MAIN/PROGram/PROGram.cc
    @brief D�finitions diverses du module PROG.
    @author @ref Guillaume_Terrissol
    @date 17 F�vrier 2004 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace PROG
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlreadyRegisteredExtension, "Extension d�j� enregistr�e.",            "Already registered extension.")
    REGISTER_ERR_MSG(kHaltExceptionMessage,       "Exception d'arr�t du programme.",        "Program halt exception")
    REGISTER_ERR_MSG(kMethodMustBeReimplemented,  "Cette m�thode doit �tre r�impl�ment�e.", "This method must be reimplemented.")
}
