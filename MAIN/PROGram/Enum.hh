/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROG_ENUM_HH
#define PROG_ENUM_HH

/*! @file MAIN/PROGram/Enum.hh
    @brief Enum�rations du module PROG.
    @author @ref Guillaume_Terrissol
    @date 8 Ao�t 2006 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PROG
{
    //! Types de ressources.
    enum EResources
    {
        eGeometry,          //!< G�om�trie.
        eImage,             //!< Image.
        eFragment,          //!< Fragment de texture de tuile.
        eTile,              //!< Tuile (de terrain).
        eScript,            //!< Script (Lua).
        eResourceTypeCount  //!< Nombre de types de ressources.
    };

    enum EGameSteps
    {
        eSysConfig,
        eStart,
        eRun,
        ePause,
        eLoad,
        eSave,
        eMenu,
        eMovie,
        eGameStepCount
    };
}

#endif // De PROG_ENUM_HH
