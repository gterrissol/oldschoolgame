/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROG_HARDWARE_HH
#define PROG_HARDWARE_HH

#include "PROGram.hh"

/*! @file MAIN/PROGram/Hardware.hh
    @brief En-t�te de la classe PROG::Hardware.
    @author @ref Guillaume_Terrissol
    @date 20 Septembre 2005 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include "ERRor/Exception.hh"
#include "STL/STL.hh"

#include "GameSteps.hh"

namespace PROG
{
//------------------------------------------------------------------------------
//                               Exception Ultime
//------------------------------------------------------------------------------

    /*! @brief Exception ultime.
        @version 0.3

        Cette exception est lanc�e, notamment, lorsque l'utilisateur demande l'arr�t du programme suite �
        l'�chec d'une assertion.
     */
    class HaltException : public ERR::Exception
    {
    public:
        HaltException();            //!< Constructeur par d�faut.
virtual ~HaltException() throw();   //!< Destructeur.
    };


//------------------------------------------------------------------------------
//                                Mat�riel
//------------------------------------------------------------------------------

    /*! @brief Mat�riel.
        @version 0.5

        
     */
    class Hardware
    {
    public:
        //! @name Type de pointeur
        //@{
        using   Ptr = std::shared_ptr<Hardware>;                //!< Pointeur sur mat�riel.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Hardware();                                 //!< Constructeur (�diteur).
                    Hardware(const std::string& pPakFileName);  //!< Constructeur (moteur).
                    ~Hardware();                                //!< Destructeur.
        //@}
        //! @name Cr�ation & chargement
        //@{
        Game::Ptr   boot(void* pWinId);                         //!< Lancement (�diteur).
        Game::Ptr   boot();                                     //!< Lancement.
        void        tick();                                     //!< Nouveau cycle.
        void        halt();                                     //!< Arr�t.
        //@}
        //! @name Contr�le des ressources cach�es
        //@{
        using Data = std::vector<I32>;                          //!< Liste de donn�es pour les ressources.
        //! Identifiants des requ�tes de contr�le des ressources cach�es
        enum ECmd
        {
            eResizeView,                                        //!< Redimensionnement du viewport.
            eGetViewSize                                        //!< R�cup�ration de la taille du viewport.
        };
        Data        rcntl(ECmd pId, const Data& pArgs);         //!< Acc�s aux "ressources cach�es".
        //@}

    private:

        class Private;
        std::unique_ptr<Private>    pthis;                      //!< P-Impl.
    };
}

#endif  // De PROG_HARDWARE_HH
