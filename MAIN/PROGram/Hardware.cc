/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Hardware.hh"

/*! @file MAIN/PROGram/Hardware.cc
    @brief M�thodes (non-inline) de la classe PROG::Hardware.
    @author @ref Guillaume_Terrissol
    @date 20 Septembre 2005 - 29 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SDL.h>

#include <thread>

#include "GraphXView/Viewport.hh"
#include "GraphXView/EngineViewport.hh"
#include "INPut/InputMgr.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/MemoryMgr.hh"
#include "PAcKage/Enum.hh"
#include "PAcKage/PakFileMgr.hh"
#include "ReSourCe/ObjectMgr.hh"
#include "ReSourCe/Thread.hh"
#include "STL/SharedPtr.hh"
#include "TILE/TileGeometry.hh"
#include "TIME/MainTimer.hh"
#include "VIEWport/Window.hh"

#include "ErrMsg.hh"

namespace PROG
{
//------------------------------------------------------------------------------
//                                   Exception
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    HaltException::HaltException()
#ifdef ASSERTIONS
        : ERR::Exception(kHaltExceptionMessage)
#else
        : ERR::Exception("")
#endif  // De ASSERTIONS
    { }


    /*! Destructeur.
     */
    HaltException::~HaltException() throw() { }


//------------------------------------------------------------------------------
//                              P-Impl de Hardware
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PROG::Hardware
        @version 0.2
     */
    class Hardware::Private
    {
    public:
        //! @name Types de pointeur
        //@{
        using MemoryMgrPtr  = SharedPtr<MEM::MemoryMgr>;    //!< Pointeur sur gestionnaire de m�moire.
        using MainTimerPtr  = SharedPtr<TIME::MainTimer>;   //!< Pointeur sur timer principal.
        using ObjectMgrPtr  = SharedPtr<RSC::ObjectMgr>;    //!< Pointeur sur gestionnaire d'objets.
        using PakFileMgrPtr = SharedPtr<PAK::PakFileMgr>;   //!< Pointeur sur gestionnaire de pak file.
//        using SoundMgrPtr   = SharedPtr<SND::SoundMgr>;     //!< Pointeur sur gestionnaire sonore.
        using InputMgrPtr   = INP::InputMgr::Ptr;           //!< Pointeur sur gestionnaire de p�riph�riques d'entr�e.
        using ThreadPtr     = SharedPtr<THR::Thread>;       //!< Pointeur sur thread.
        using ViewportPtr   = SharedPtr<GXV::Viewport>;     //!< Pointeur sur viewport.
        using WindowPtr     = SharedPtr<VIEW::Window>;      //!< Pointeur sur fen�tre.
        //@}
                Private();                                  //!< Constructeur.
        //! @name Gestion du syst�me
        //@{
        void    boot(bool pForENGine);                      //!< D�marrage du syst�me.
        void    switchOff();                                //!< Arr�t du syst�me.
        //@}

        //! @name Composants
        //@{
        MemoryMgrPtr    mMemoryMgr;                         //!< Gestionnaire m�moire.
        MainTimerPtr    mMainTimer;                         //!< Chronom�tre principal.
        PakFileMgrPtr   mFileMgr;                           //!< Gestionnaire de fichiers.
        ObjectMgrPtr    mObjectMgr;
        ThreadPtr       mLoadingThread;                     //!< Processus de chargement en t�che de fond.

        InputMgrPtr     mInputMgr;                          //!< Gestionnaire de p�riph�riques d'entr�e.
//        SoundMgrPtr     mSoundMgr;                          //!< Gestionnaire du son et de la musique.
        ViewportPtr     mViewport;                          //!< Support de vue.
        WindowPtr       mWindow;                            //!< Fen�tre.
        ThreadPtr       mWorldAIThread;                     //!< IA du monde.

        SharedPtr<Game> mGame;
        bool            mEngineMode;
    };


//------------------------------------------------------------------------------
//                       P-Impl de Hardware : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Hardware::Private::Private()
        : mMemoryMgr{}
        , mMainTimer{}
        , mFileMgr{}
        , mObjectMgr{}
        , mLoadingThread{}
//        , mSoundMgr()
        , mWorldAIThread{}
        , mGame{}
        , mEngineMode{true}
    { }


//------------------------------------------------------------------------------
//                          PImpl : Gestion du Syst�me
//------------------------------------------------------------------------------

    /*! D�marre les ressources mat�rielles virtuelles.
     */
    void Hardware::Private::boot(bool pForENGine)
    {
        mEngineMode = pForENGine;

        // Choix de la langue.
#if     defined(FRANCAIS)
        SET_ERR_MSG_LANGUAGE(eFr)
#elif   defined(ENGLISH)
        SET_ERR_MSG_LANGUAGE(eEn)
#else
#   error "Pas de langue par d�faut d�finie."
#endif  // Du langage.

        // Tout d'abord la SDL.
#ifdef  HAVE_WINDOWS_H
        U32 lFlags  = U32(SDL_INIT_NOPARACHUTE);
#else
        U32 lFlags  = U32(0/*SDL_INIT_NOPARACHUTE*/);
#endif  // HAVE_WINDOWS_H

        if (SDL_Init(lFlags) < 0)
        {
            throw PROG::HaltException();
        }

        // Au commencement (ou presque), �tait la RAM (rappel : MEM::MemoryMgr utilise l'op�rateur new par d�faut).
        mMemoryMgr      = std::make_shared<MEM::MemoryMgr>();
        // Puis vint le temps.
        mMainTimer      = STL::makeShared<TIME::MainTimer>();

        // Ensuite les fichiers.
        mFileMgr        = STL::makeShared<PAK::PakFileMgr>();
        // Et leur chargement.
        mObjectMgr      = STL::makeShared<RSC::ObjectMgr>(U32(pForENGine ? 0 : PAK::eMaxFileCount));
        mLoadingThread  = STL::makeShared<RSC::Thread>();

        // Un peu de contr�le pour limiter le bordel.
        mInputMgr       = STL::makeShared<INP::InputMgr>();
        // Et en musique.
//      mSoundMgr = STL::makeShared<SND::SoundMgr>();
        // En jetant un coup d'oeil.
        mViewport       = STL::makeShared<GXV::EngineViewport>(mInputMgr, pForENGine);
//#ifdef HAVE_WINDOWS_H
        mWindow         = STL::makeShared<VIEW::Window>(GXV::Viewport::Ptr{});
        mInputMgr->set(INP::InputMgr::ResizeCallback{ [=](I32 pW, I32 pH) { mWindow->resize(pW, pH); } });

        /*if (pForENGine)
        {
            mWindow->activate();
        }*/
/*#else
        mWindow->activate();
#endif  // HAVE_WINDOWS_H*/
//        mViewport->initialize();

//        mWorldAIThread.reset(   new ...);
        // Sur l'avanc�e du bouzin.
//        Game = new Game ?

        // Autres composants moteur (cach�s).
        TILE::TileGeometry::buildTriangleList();
    }


    /*!
     */
    void Hardware::Private::switchOff()
    {
        // Composants moteur (cach�s).
        TILE::TileGeometry::destroyTriangleList();

        // Composants g�n�raux.
        mWorldAIThread.reset();

        mWindow.reset();
        mViewport.reset();
        mInputMgr.reset();
//        mSoundMgr.reset();

        mObjectMgr.reset();
        mLoadingThread.reset();
        mFileMgr.reset();
        mMainTimer.reset();
        mMemoryMgr.reset();

        SDL_Quit();
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Hardware::Hardware()
        : pthis( new Private())
    {
        pthis->boot(false);
    }


    /*! Constructeur.
     */
    Hardware::Hardware(const std::string& pPakFileName)
        : pthis(new Private())
    {
        pthis->boot(true);
        PAK::PakFileMgr::get()->openFile(pPakFileName.c_str());
    }


    /*! Destructeur.
     */
    Hardware::~Hardware()
    {
        halt();
        pthis->switchOff();
    }


//------------------------------------------------------------------------------
//                              Cr�ation & chargement
//------------------------------------------------------------------------------

    /*! @param pWinId Id de la fen�tre IDE pour le rendu du moteur
     */
    Game::Ptr Hardware::boot(void* pWinId)
    {
        PAK::Loader lLoader(PAK::eRoot);

        pthis->mGame = STL::makeShared<Game>(pthis->mViewport, lLoader);
        pthis->mWindow->activate(pWinId);
        pthis->mWindow->bind(pthis->mViewport);

        RSC::Thread::get()->signal(THR::eStart);

        return pthis->mGame;
    }


    /*!
     */
    Game::Ptr Hardware::boot()
    {
        return boot(nullptr);
    }


    /*!
     */
    void Hardware::tick()
    {
        pthis->mInputMgr->processEvents();
        pthis->mWindow->update();

        const I64   kFreq       = I64{100};             // Hz.
        const I64   kDelay      = I64{1000000 / kFreq}; // �s.
        I64         lElapsed    = TimeMgr->delta();

        if ((100 < lElapsed) && (lElapsed < kDelay))
        {
            std::chrono::microseconds   lSpare = std::chrono::microseconds(kDelay - lElapsed);
            std::this_thread::sleep_for(lSpare);
        }

        pthis->mMainTimer->tick();
    }


    /*!
     */
    void Hardware::halt()
    {
        RSC::Thread::get()->signal(THR::eStop);

        pthis->mGame.reset();
    }


    /*! @param pId   Identifie la commande d'acc�s aux ressources.
        @param pArgs Arguments de la commande (si besoin).
        @return La r�ponse de la commande si d�finie; une liste vide sinon.
     */
    Hardware::Data Hardware::rcntl(ECmd pId, const Data& pArgs)
    {
        switch(pId)
        {
            case eResizeView:
            {
                ASSERT(pArgs.size() == 2, "Invalid argument for eResizeView")
                pthis->mWindow->resize(pArgs[0], pArgs[1]);
            } break;
            case eGetViewSize:
            {
                return Data{pthis->mWindow->width(), pthis->mWindow->height()};
            }
            default:
            {
                ASSERT(false, "Invalid id")
            } break;
        }

        return {};
    }
}
