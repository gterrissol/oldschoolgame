/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "World.hh"

/*! @file ENGine/UNDerground/World.cc
    @brief M�thodes (non-inline) de la classe UND::World.
    @author @ref Guillaume_Terrissol
    @date 24 Novembre 2008 - 15 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/LoaderSaver.hh"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "UTIlity/Rect.hh"

namespace UND
{
//------------------------------------------------------------------------------
//                                P-Impl de World
//------------------------------------------------------------------------------

    /*! @brief P-Impl de UT::World.
        @version 0.4
     */
    class World::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
        Private();                      //!< Constructeur par d�faut.
        Private(PAK::Loader& pLoader);  //!< Constructeur.
        ~Private();                     //!< Destructeur.
        //@}

        UTI::Rect   mArea;              //!< Zone couverte par le monde.
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::Private::Private()
        : mArea{}
    { }


    /*! Constructeur par d�faut.
     */
    World::Private::Private(PAK::Loader& pLoader)
    {
        // Zone.
        pLoader >> mArea;
    }


    /*! Destructeur.
     */
    World::Private::~Private() { }


//------------------------------------------------------------------------------
//                      World : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::World(PAK::Loader& pLoader)
        : WOR::World(pLoader)
        , pthis(pLoader)
    { }


    /*! Destructeur.
     */
    World::~World() { }


//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
        
     */
    void World::store(PAK::Saver& pSaver) const
    {
        WOR::World::store(pSaver);

        pSaver << pthis->mArea;
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    void World::setArea(const UTI::Rect& pArea)
    {
        pthis->mArea = pArea;
    }


//------------------------------------------------------------------------------
//                          Parcours des Lieux � Rendre
//------------------------------------------------------------------------------

    /*! Etablit la liste des apparences visibles et retourne la premi�re d'entre elles.
        @param pCamera Camera d�finissant le cadre de rendu du monde
        @return La premi�re apparence visible, ou nullptr s'il n'y en a pas
     */
    World::PlacePtr World::first(ConstCameraPtr /*pCamera*/)
    {
        return nullptr;
    }


    /*! 
     */
    World::PlacePtr World::next()
    {
        return nullptr;
    }


    /*!
     */
    World::PlacePtr World::current()
    {
        return nullptr;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    WOR::EType World::getWorldType() const
    {
        return WOR::eOutside;
    }


    /*!
     */
    UTI::Rect World::getArea() const
    {
        return pthis->mArea;
    }
}
