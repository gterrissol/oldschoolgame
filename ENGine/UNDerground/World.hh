/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UND_WORLD_HH
#define UND_WORLD_HH

#include "UNDerground.hh"

/*! @file ENGine/UNDerground/World.hh
    @brief En-t�te de la classe UND::World.
    @author @ref Guillaume_Terrissol
    @date 24 Novembre 2008 - 15 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "WORld/BaseWorld.hh"

namespace UND
{
    /*! @brief Monde sous-terrain
        @version 0.2

        
     */
    class World : public WOR::World
    {
    public :
        //! @name Classe amie
        //@{
 friend class Controller;                               //!< Pour les m�thodes d'�dition.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit    World(PAK::Loader& pLoader);        //!< Constructeur par d�faut.
virtual             ~World();                           //!< Destructeur.
        //@}

    protected:
        //! @name "Permanence" des donn�es
        //@{
virtual void        store(PAK::Saver& pSaver) const;    //!< Sauvegarde des donn�es.
        //@}
        //! @name M�thodes d'�dition
        //@{
        void        setArea(const UTI::Rect& pArea);    //!< Zone du monde (coordonn�es globales).
        //@}
    private:
        //! @name Gestion du rendu
        //@{
virtual PlacePtr    first(ConstCameraPtr pCamera);      //!< Premier lieu visible.
virtual PlacePtr    next();                             //!< Lieu visible suivant.
virtual PlacePtr    current();                          //!< Lieu visible actuel.
        //@}
virtual WOR::EType  getWorldType() const;               //!< Type du monde.
virtual UTI::Rect   getArea() const;                    //!< Zone occup�e par le monde.
        FORBID_COPY(World)
        PIMPL()
    };
}

#endif  // De UND_WORLD_HH
