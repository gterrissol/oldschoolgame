/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UND_ERRMSG_HH
#define UND_ERRMSG_HH

/*! @file ENGine/UNDerground/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module UND.
    @author @ref Guillaume_Terrissol
    @date 24 Novembre 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace UND
{
#ifdef ASSERTIONS

    // ! @name Messages d'assertion
    // @{
    // Ins�rez ici vos messages d'assertions (cf. ERR module)
    // @}

#endif  // De ASSERTIONS
}

#endif  // De UND_ERRMSG_HH
