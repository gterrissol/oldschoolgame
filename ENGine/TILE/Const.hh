/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_CONST_HH
#define TILE_CONST_HH

/*! @file ENGine/TILE/Const.hh
    @brief Constantes du module TILE.
    @author @ref Guillaume_Terrissol
    @date 8 Ao�t 2006 - 13 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace TILE
{
    extern  const F32   kVerticalStep;      //!< Echelle verticale des tuiles.
    extern  const U16   kFamilyMaxSize;     //!< Nombre maximum d'�l�ments dans une famille.
    extern  const U16   kMaxFamilyCount;    //!< Nombre maximum de familles.
}

#endif // De TILE_CONST_HH
