/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_BASETILE_HH
#define TILE_BASETILE_HH

#include "TILE.hh"

/*! @file ENGine/TILE/BaseTile.hh
    @brief En-t�te de la classe TILE::Tile.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 29 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/MEMory.hh"
#include "OBJect/OBJect.hh"
#include "PLaCes/Place.hh"
#include "ReSourCe/ResourceMgr.hh"
#include "ReSourCe/ObjectMgr.hh"
#include "STL/STL.hh"

#include "Fragment.hh"
#include "HeightField.hh"

namespace TILE
{
    /*! @brief Tuile.
        @version 0.35

        Element de surface.
        @todo Chargement dynamique U complet d'une zone (� param�trer)
     */
    class Tile : public PLC::Place
    {
        //! @name Classe amie
        //@{
 friend class Controller;                                                       //!< Pour les m�thodes d'�dition.
        //@}
    public:
        //! @name Alias
        //@{
        using Encoding = TexelGroup;                                            //!< Encodage de la megatexture (pour la tuile).
        //@}
        //! @name Types de pointeurs
        //@{
        using GameObjectPtr         = SharedPtr<OBJ::GameObject>;               //!< Pointeur sur game object.
        using ConstHeightFieldPtr   = SharedPtr<HeightField const>;             //!< Pointeur (constant) sur champ d'altitudes.
        using HeightFieldPtr        = SharedPtr<HeightField>;                   //!< Pointeur sur champ d'altitudes.
        using MaterialPtr           = SharedPtr<MAT::Material>;                 //!< Pointeur sur mat�riau.
        using ObjectPtr             = RSC::ObjectMgr::ObjectPtr;                //!< Pointeur sur objet ressource.
        using TileGeometryPtr       = SharedPtr<TileGeometry>;                  //!< Pointeur sur g�om�trie de tuile.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit            Tile(PAK::Loader& pLoader);                         //!< Constructeur (chargement imm�diat).
 static void                saveDefault(PAK::Saver&        pSaver,
                                        const HeightGroup& pHeights,
                                        PAK::FileHdl       pOut);               //!< Cr�ation d'une tuile minimale.
virtual                     ~Tile();                                            //!< Destructeur.
        //@}
        //! @name Information sur la zone de la tuile
        //@{
        const UTI::Rect&    area() const;                                       //!< Zone occup�e par la tuile.
        F32                 height(const MATH::V2& pPosition) const;            //!< Altitude a une position donn�e.
        //@}

    protected:
        //! @name "Permanence" des donn�es
        //@{
virtual void                store(PAK::Saver& pSaver) const;                    //!< Sauvegarde des donn�es.
        //@}
        void                swap(Tile& pLoadedTile);                            //!< Permutation des donn�es.
        //! @name Autres constructeurs
        //@{
        explicit            Tile();                                             //!< Constructeur par d�faut.
        explicit            Tile(PAK::FileHdl pFileHdl);                        //!< Constructeur (chargement diff�r�).


    private:

        void                build(const UTI::Rect& pArea, PAK::FileHdl pOut);   //!< Cr�ation.
        //@}
        //! @name M�thodes d'�dition
        //@{
        void                addElement(GameObjectPtr pElement);                 //!< Ajout d'un �l�ment.
        void                removeElement(GameObjectPtr pElement);              //!< Retrait d'un �l�ment.
        //@}
        //! @name Acc�s aux donn�es (pour �dition)
        //@{
        ConstHeightFieldPtr heights() const;                                    //!< Sol (�l�vations constantes).
        HeightFieldPtr      heights();                                          //!< Sol (�l�vations).
        TileGeometryPtr     ground();                                           //!< Sol (g�om�trie).
        void                setTexture(const Encoding& pConf);                  //!< D�finition de la texture de la tuile.
        Encoding            texture() const;                                    //!< Texture (encod�e).
        ObjectPtr           outside() const;                                    //!< Monde ext�rieur.
        //@}
        //! @name Parcours des apparences
        //@{
virtual bool                first(AppearancePtr& pApp, MATH::M4& pFrame);       //!< Premi�re apparence visible.
virtual bool                next(AppearancePtr& pApp, MATH::M4& pFrame);        //!< Apparence visible suivante.
virtual bool                current(AppearancePtr& pApp, MATH::M4& pFrame);     //!< Apparence visible actuelle.
        //@}
        FORBID_COPY(Tile)
        PIMPL()
    };


    using   TileRsc = RSC::Resource<Tile>;          //!< Ressource tuile.
    using   TileMgr = RSC::ResourceMgr<TileRsc>;    //!< Gestionnaire de tuiles.
    using   Key     = TileMgr::TKey;                //!< Clef pour un TILE::Tile.
}

#endif  // De TILE_BASETILE_HH
