/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_ALL_hh
#define TILE_ALL_hh

/*! @file ENGine/TILE/All.hh
    @brief Interface publique du module @ref TILE.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 13 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TILE   //! Gestion des tuiles.
{
    /*! @namespace TILE
        @version 0.3

        Le moteur est essentiellement 2D (heightfield). Le terrain et les "dongeons" sont divis�s en
        petites sections : des tuiles (tiles), zones carr�es, qui g�rent elles-m�mes les objets plac�s
        dessus.
     */

    /*! @defgroup TILE TILE : Gestion des tuiles
        <b>namespace</b> TILE.
     */
}

#include "HeightField.hh"
#include "TileGeometry.hh"
#include "Fragment.hh"
#include "BaseTile.hh"

#endif  // De TILE_ALL_hh
