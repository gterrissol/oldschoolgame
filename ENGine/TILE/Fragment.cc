/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Fragment.hh"

/*! @file ENGine/TILE/Fragment.cc
    @brief M�thodes (non-inline) des classes TILE::Image, TILE::Fragment, TILE::FragmentMgr::Private
    & TILE::FragmentMgr.
    @author @ref Guillaume_Terrissol
    @date 7 Ao�t 2006 - 29 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cstring>

#include "CoLoR/Colors.hh"
#include "CoLoR/RGB.hh"
#include "CoLoR/RGBA.hh"
#include "MATH/Const.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/Map.hh"
#include "STL/String.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"
#include "UTIlity/Bits.hh"

#include "Const.hh"
#include "ErrMsg.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//                                    Tiling
//------------------------------------------------------------------------------

    //!
    Tiling::Tiling() = default;

    /*! @param pLoader Donn�es de la famille.
     */
    Tiling::Tiling(PAK::Loader& pLoader)
        : mData()
    {
        // 1 - Lecture par ligne des donn�es, et r�p�tition gauche/droite.
        U32 lWidth  = width();
        U32 lHeight = height();
        U32 lMargin = margin();
        U32 lTexel{};
        U32 lOffset = U32(lMargin * eTilingSize) + lMargin;
        for(auto lY = k0UL; lY < lHeight; ++lY, lOffset += lMargin + lMargin)
        {
            // Donn�es des fragments sur la ligne.
            for(auto lX = k0UL; lX < lWidth; ++lX, ++lOffset)
            {
                pLoader >> lTexel;
                mData[lOffset] = lTexel;
            }
            // R�p�tition
            std::copy_n(&mData[lOffset - lMargin], lMargin, &mData[lOffset - lWidth - lMargin]);    // ... � gauche, des donn�es droites des fragments.
            std::copy_n(&mData[lOffset - lWidth],  lMargin, &mData[lOffset]);                       // ... � droite, des donn�es gauches des fragments.
        }
        // 2 - R�p�tition
        for(auto lY = k0UL; lY < lMargin; ++lY)
        {
            std::copy_n(&mData[(lHeight + lY) * eTilingSize], eTilingSize, &mData[                     lY  * eTilingSize]);   // ... en haut des donn�es en bas des fragments.
            std::copy_n(&mData[(lMargin + lY) * eTilingSize], eTilingSize, &mData[(lMargin + lHeight + lY) * eTilingSize]);   // ... en bas des donn�es en haut des fragments.
        }
    }

    /*!
     */
    void Tiling::store(PAK::Saver& pSaver)
    {
        U32 lWidth  = width();
        U32 lHeight = height();
        U32 lMargin = margin();
        U32 lOffset = U32(lMargin * eTilingSize) + lMargin;
        for(auto lY = k0UL; lY < lHeight; ++lY, lOffset += lMargin + lMargin)
        {
            for(auto lX = k0UL; lX < lWidth; ++lX, ++lOffset)
            {
                pSaver << mData[lOffset];
            }
        }
    }

    /*!
     */
    const void* Tiling::data() const
    {
        return &mData[0];
    }

    /*!
     */
    const U32* Tiling::line(U32 pLine) const
    {
        ASSERT_EX(pLine < height(), kInvalidRowIndex, return nullptr;)

        return &mData[(margin() + pLine) * eTilingSize + margin()];
    }

    /*!
     */
    U32* Tiling::line(U32 pLine)
    {
        ASSERT_EX(pLine < height(), kInvalidRowIndex, return nullptr;)

        return &mData[(margin() + pLine) * eTilingSize + margin()];
    }

    /*!
     */
    U32 Tiling::computeTilingColor() const
    {
        // Comme les 4x4 fragmetns d'une famille sont les variations (normalement l�g�res) d'une m�me base,
        // la couleur g�n�rale est calcul�e � partir d'un seul fragment (le premier).
        U32         lChannels[4] = { k0UL, k0UL, k0UL, k0UL };
        const auto  lMargin = margin();

        for(auto lLi = lMargin; lLi < eFragmentSize + lMargin; ++lLi)
        {
            for(auto lCo = lMargin; lCo < eFragmentSize + lMargin; ++lCo)
            {
                U32 lTexel  = mData[lLi * eTilingSize + lCo];
                lChannels[0] += U32{(0x000000FF & lTexel) >>  0};
                lChannels[1] += U32{(0x0000FF00 & lTexel) >>  8};
                lChannels[2] += U32{(0x00FF0000 & lTexel) >> 16};
                lChannels[3] += U32{(0xFF000000 & lTexel) >> 24};
            }
        }

        return U32{(lChannels[0] / (eFragmentSize * eFragmentSize)) <<  0} |
               U32{(lChannels[1] / (eFragmentSize * eFragmentSize)) <<  8} |
               U32{(lChannels[2] / (eFragmentSize * eFragmentSize)) << 16} |
               U32{(lChannels[3] / (eFragmentSize * eFragmentSize)) << 24};
    }

    /*!
     */
    U32 Tiling::width()
    {
        return U32{eFragmentsInTilingSide * eFragmentSize};
    }

    /*
     */
    U32 Tiling::height()
    {
        return U32{eFragmentsInTilingSide * eFragmentSize};
    }

    /*!
     */
    U32 Tiling::margin()
    {
        return U32{(eTilingSize - width()) / 2};
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    TilingsTexture::TilingsTexture()
        : MAT::Image{}
    {
        setFormat(MAT::EFormat(MAT::e32Bits | MAT::eRGBA));
        setWidth(U16(eTilingsInTextureSide * eTilingSize));
        setHeight(U16(eTilingsInTextureSide * eTilingSize));
    }


    /*!
     */
    TilingsTexture::TilingsTexture(const Tilings& pList)
        : TilingsTexture{}
    {
        Vector<Tiling::Ptr> lTilings;
        for(auto t : pList)
        {
            lTilings.push_back(const_cast<const FragmentMgr*>(FragmentMgr::get())->tiling(t));
        }

        std::vector<U32>    lLine{4 * eTilingSize, U32{0}};
        for(U32 lG = k0UL; lG < 4; ++lG)
        {
            for(U32 lRow = k0UL; lRow < 4 * eTilingSize; ++lRow)
            {
                for(U32 lT = k0UL; lT < 4; ++lT)
                {
                    auto* lTilingLine = &static_cast<const U32::TType*>(lTilings[4 * lG +lT]->data())[lRow * eTilingSize];
                    std::memcpy(addressof(lLine[lT * eTilingSize]), lTilingLine, eTilingSize * sizeof(U32));
                }
                setLine(static_cast<U16>(lG * eTilingSize + lRow), lLine.data());
            }
        }
    }

    //!
    TilingsTexture::~TilingsTexture() = default;

    //!
    void TilingsTexture::update(const Tilings& /*pNewList*/) { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    class Masks : public MAT::Image
    {
    public:
        using Ptr = SharedPtr<Masks>;
        Masks(PAK::Loader& pLoader)
            : MAT::Image(pLoader)
        {
            keepDataAfterBind(true);
        }
        using MAT::Image::store;
    };

//------------------------------------------------------------------------------
//                          P-Impl de Fragment Manager
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::FragmentMgr.
        @version 0.7

        Impl�mentation de FragmentMgr (tout ce qu'il y a de plus standard).
     */
    class FragmentMgr::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
                Private();                                      //!< Constructeur par d�faut.
                Private(PAK::Loader& pLoader);                  //!< Constructeur (chargement).
        //@}
        //! @name Permanence
        //@{
        void    store(PAK::Saver& pSaver) const;                //!< Sauvegarde des donn�es.
        //@}
        //! name Familles de fragments.
        //@{
        enum
        {
            eFragments,
            eColor
        };
        Vector<std::tuple<Tiling::Ptr, U32>>    mTilings;           //!< Familles de fragments (+ couleurs).
        Map<U32, Tiling::Id>                    mTilingIdsByColor;  //!< Id de Tiling par couleur.
        TilingsTexture::Ptr                     mTexture;           // Temporaire.
        //@}
        Masks::Ptr                              mMasks;             //!< "Masques" de m�lange de fragments.
        PAK::FileHdl                            mFileHdl;           //!< Handle du fichier des donn�es.
    };


//------------------------------------------------------------------------------
//             Fragment Manager P-Impl : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    FragmentMgr::Private::Private()
        : mTilings{}
        , mTilingIdsByColor{}
        , mTexture{}
        , mMasks{}
        , mFileHdl(PAK::eBadHdl)
    { }


    /*! Constructeur (chargement).
        @param pLoader Donn�es du P-Impl
     */
    FragmentMgr::Private::Private(PAK::Loader& pLoader)
        : mTilings{}
        , mTilingIdsByColor{}
        , mTexture{}
        , mMasks{}
        , mFileHdl(pLoader.fileHdl())
    {
        U32 lTilingCount = k0UL;
        pLoader >> lTilingCount;
        mTilings.reserve(lTilingCount);
        bool    lIsTilingDefined;

        for(auto lT = k0UW; lT < lTilingCount; ++lT)
        {
            pLoader >> lIsTilingDefined;
            if (lIsTilingDefined)
            {
                mTilings.emplace_back(std::make_tuple(STL::makeShared<Tiling>(pLoader), U32(CLR::kWhite)));
                CLR::RGBA   lColor;
                pLoader >> lColor;
                std::get<eColor>(mTilings.back()) = lColor;
                mTilingIdsByColor[lColor] = I16(lT);
            }
            else
            {
                mTilings.emplace_back(std::make_tuple(Tiling::Ptr{}, U32(CLR::kWhite)));
            }
        }

        mMasks = STL::makeShared<Masks>(pLoader);
    }


//------------------------------------------------------------------------------
//                     Fragment Manager P-Impl : Permanence
//------------------------------------------------------------------------------

    /*! Sauvegarde les donn�es du gestionnaire.
        @param pSaver Fichier dans lequel sauvegarder les donn�es
     */
    void FragmentMgr::Private::store(PAK::Saver& pSaver) const
    {
        U32 lTilingCount = U32(mTilings.size());
        pSaver << lTilingCount;

        for(const auto& lT : mTilings)
        {
            if (const auto& lTiling = std::get<eFragments>(lT))
            {
                pSaver << true;
                lTiling->store(pSaver);
                pSaver << CLR::RGBA{std::get<eColor>(lT)};
            }
            else
            {
                pSaver << false;
            }
        }

        mMasks->store(pSaver);
    }


//------------------------------------------------------------------------------
//                 Fragment Manager : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    FragmentMgr::FragmentMgr()
        : pthis()
    { }


    /*! Destructeur.
     */
    FragmentMgr::~FragmentMgr() { }


//------------------------------------------------------------------------------
//          Fragment Manager : Sauvegarde & Restauration des Fragments
//------------------------------------------------------------------------------

    /*! Sauvegarde les donn�es du gestionnaire.
     */
    void FragmentMgr::save() const
    {
        if (pthis->mFileHdl != PAK::eBadHdl)
        {
            PAK::Saver lSaver(pthis->mFileHdl);
            pthis->store(lSaver);
        }
    }


    /*! Charge les donn�es du gestionnaire.
        @param pLoader Donn�es du gestionnaire
     */
    void FragmentMgr::load(PAK::Loader& pLoader)
    {
        MEM::PImpl<Private>(pLoader).swap(pthis);
        pthis->mFileHdl = pLoader.fileHdl();
    }


    /*! @return Le handle du fichier contenant les donn�es du gestionnaire.
     */
    PAK::FileHdl FragmentMgr::fileHdl() const
    {
        return pthis->mFileHdl;
    }


//------------------------------------------------------------------------------
//         Fragment Manager : Informations sur les Familles de Fragments
//------------------------------------------------------------------------------

    /*!
     */
    FragmentMgr::TilingCount FragmentMgr::tilingCount() const
    {
        return TilingCount(pthis->mTilings.size());
    }

    /*! @todo Ext�rieur, g�om�trie macro : les texels sont une pond�ration des couleurs des familles.
        @todo Plut�t retourner un U32 ?
     */
    CLR::RGBA FragmentMgr::tilingColor(Tiling::Id pId) const
    {
        auto&   lTiling = pthis->mTilings[pId];
        auto&   lColor  = std::get<Private::eColor>(lTiling);
        if (lColor == U32{CLR::kWhite})
        {
            auto fragments = std::get<Private::eFragments>(lTiling);
            if (fragments)
            {
                lColor = fragments->computeTilingColor();
                pthis->mTilingIdsByColor[lColor] = pId;
            }
        }

        return CLR::RGBA{lColor};
    }


    /*!
     */
    const Tiling::Ptr FragmentMgr::tiling(Tiling::Id pId) const
    {
        if (size_t(pId) < pthis->mTilings.size())
        {
            return std::get<Private::eFragments>(pthis->mTilings[pId]);
        }
        else
        {
            return std::get<Private::eFragments>(pthis->mTilings[pId % pthis->mTilings.size()]);
        }
    }


//------------------------------------------------------------------------------
//                Fragment Manager : Informations sur les Masques
//------------------------------------------------------------------------------

    /*!
     */
    const MAT::Image::Ptr FragmentMgr::masks() const
    {
        return pthis->mMasks;
    }


//------------------------------------------------------------------------------
//                    Fragment Manager : G�n�ration de Texture
//------------------------------------------------------------------------------

    /*! @todo Il faut en fait d�terminer les TilingsTextures pour une tuile.
     *        Pour le moment, cr�e une texture en recopiant les tilings par d�faut 4 lignes des 4 tilings c�te � c�te.
     */
    TilingsTexture::Ptr FragmentMgr::getFragments(Tiling::Id )//pId)
    {
        try
        {
            if (!pthis->mTexture)
            {
                pthis->mTexture = STL::makeShared<TilingsTexture>(
                    TilingsTexture::Tilings{
                        Tiling::Id{ 0}, Tiling::Id{ 1}, Tiling::Id{ 2}, Tiling::Id{ 3},
                        Tiling::Id{ 4}, Tiling::Id{ 5}, Tiling::Id{ 6}, Tiling::Id{ 7},
                        Tiling::Id{ 8}, Tiling::Id{ 9}, Tiling::Id{10}, Tiling::Id{11},
                        Tiling::Id{12}, Tiling::Id{13}, Tiling::Id{14}, Tiling::Id{15}
                    });
            }
            return pthis->mTexture;
        }
        catch(std::out_of_range&)
        {
            ASSERT(false, "Invalid Tiling id")
            return {};
        }
    }


//------------------------------------------------------------------------------
//           Fragment Manager : Encodage des Familles et des Fragments
//------------------------------------------------------------------------------

    /*!
     */
    bool FragmentMgr::isTexelAlreadySet(const TexelData& pTexel)
    {
        return (pTexel.defined == 1);
    }

    /*!
     */
    TexelData FragmentMgr::inactiveTexel()
    {
        return TexelData{};
    }

    /*!
     */
    bool FragmentMgr::isTexelInactive(const TexelData& pTexel)
    {
        return pTexel == inactiveTexel();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    Tiling::Ptr FragmentMgr::editTiling(Tiling::Id pId)
    {
        if (size_t(pId) < pthis->mTilings.size())
        {
            return std::get<Private::eFragments>(pthis->mTilings[pId]);
        }
        else
        {
            return std::get<Private::eFragments>(pthis->mTilings[pId % pthis->mTilings.size()]);
        }
    }

    //!
    Tiling::Id FragmentMgr::createTiling()
    {
        // Recherche un slot libre.
        auto    lFree = std::find_if(pthis->mTilings.begin(), pthis->mTilings.end(), [](decltype(*pthis->mTilings.cbegin()) pT)
        {
            return std::get<Private::eFragments>(pT) == nullptr;
        });
        if (lFree != pthis->mTilings.end())
        {
            *lFree = std::make_tuple(STL::makeShared<Tiling>(), U32{CLR::kWhite});
            return Tiling::Id(std::distance(pthis->mTilings.begin(), lFree));
        }
        else
        {
            pthis->mTilings.emplace_back(std::make_tuple(STL::makeShared<Tiling>(), U32{CLR::kWhite}));
            return Tiling::Id(pthis->mTilings.size() - 1);
        }
    }

    //!
    void FragmentMgr::removeTiling(Tiling::Id pId)
    {
        auto&   lT = pthis->mTilings[pId];
        std::get<Private::eFragments>(lT).reset();
        std::get<Private::eColor>(lT) = U32{CLR::kWhite};
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  FragmentMgr*    MEM::Singleton<FragmentMgr>::smThat = nullptr;
#endif
}
