/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_TILE_hh
#define TILE_TILE_hh

/*! @file ENGine/TILE/TILE.hh
    @brief Pr�-d�clarations du module @ref TILE.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2008 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TILE
{
    class Controller;
    class Fragment;
    class FragmentMgr;
    class Image;
    class Tile;
    class TileGeometry;
    class TileGeometryFX;

    template<class TData>                                       class Group;
    template<class TOff, class TT>                              class PieceOfData;
    template<typename TValue, unsigned int TLen, class TAlloc>  class MapHeight;
}

#endif  // De TILE_TILE_hh
