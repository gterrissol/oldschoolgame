/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_HEIGHTFIELD_HH
#define TILE_HEIGHTFIELD_HH

#include "TILE.hh"

/*! @file ENGine/TILE/HeightField.hh
    @brief En-t�te des classes TILE::PieceOfData, TILE::Group & TILE::HeightMap.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2003 - 11 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Allocator.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/PAcKage.hh"
#include "UTIlity/UTIlity.hh"

#include "Const.hh"
#include "Enum.hh"

namespace TILE
{
    /*! @brief Encapsulation d'une �l�vation.
        @version 0.5

        Les m�thodes d'�dition ne peuvent poss�der qu'un seul argument; il a donc fallu encapsuler toutes
        les informations d'�dition d'une �l�vation (mais aussi d'un texel, c'est pourquoi cette classe
        est finalement <b>template</b>) dans une classe, afin de transmettre des instances de cette
        classe pour modififer des cartes d'�l�vations (ou des textures).
     */
    template<class TOff, class TT>
    class PieceOfData : public MEM::Auto<PieceOfData<TOff, TT>>
    {
    public:
        //! @name Types
        //@{
        typedef TOff    TOffset;                        //!< Type utilis� pour d�finir les coordonn�es.
        typedef TT      TType;                          //!< Type de la donn�e port�e.
        //@}
        //! @name Constructeurs
        //@{
        inline                  PieceOfData(TOff pC,
                                            TOff pR,
                                            TT   pD);   //!< Constructeur.
        inline  PieceOfData&    operator=(TT pD);       //!< Op�rateur d'affectation.
        //@}
        //! @name Informations
        //@{
        inline  TOff            col() const;            //!< Colonne.
        inline  TOff            row() const;            //!< Ligne.
        inline  TT              data() const;           //!< Valeur de la donn�e.
        //@}

    private:

                                PieceOfData();          //!< Constructeur par d�faut.

        TOff    mCol;                                   //!< Colonne.
        TOff    mRow;                                   //!< Ligne.
        TT      mData;                                  //!< Donn�e proprement dite.
    };

    using   TilingId = int16_t;     //!< Type sous-jacent pour un Id de Tiling (2048 ids possibles).
    using   FragId   = uint16_t;    //!< Type sous-jacent pour un Id de Fragment.

    /*! Donn�es pour un texel de heightfield.
        @note L'ordre des attributs peut sembler �trange, mais c'�tait le seul moyen
        de conserver une taille de structure minimale, tout en restant portable.
     */
    struct TexelData
    {
        TilingId    tilingId0 : 12; //!< Identifiant du 1er Tiling.
        uint16_t    defined   :  1; //!< Bit de d�finition.
        uint16_t    enabled   :  1; //!< Bit d'activation.
        uint16_t              :  0;

        TilingId    tilingId1 : 12; //!< Identifiant du 2e Tiling.
        uint16_t    blending  :  2; //!< M�thode de m�lange.
        uint16_t              :  0;

        TilingId    tilingId2 : 12; //!< Identifiant du 3e Tiling.
        uint16_t    fragDef   :  1; //!< Fragment d�fini.
        uint16_t              :  0;

        TilingId    tilingId3 : 12; //!< Identifiant du 4e Tiling.
        FragId      fragId    :  4; //!< 16 fragments possibles.
    };


    using Height = PieceOfData<I16, I16>;       //!< El�vation (pour �dition).
    using Texel  = PieceOfData<I16, TexelData>; //!< Texel (pour �dition).


    /*! @brief Requ�te d'�dition des �l�vations.
        @version 0.2

        M�me s'il est possible d'�diter les �l�vations (ou les texels) une par une (resp. un par un), il
        arrive souvent qu'il faille les modifier par groupe (pour des surfaces), au mois d'un point de
        vue ergonomique. Pour la gestion des undo/redo, il est de m�me pr�f�rable de grouper d'office les
        modifications simultan�e. Il a dont �t� n�cessaire de grouper de telles requ�tes d'�dition
        d'�l�vations (ou de texel).
        @note Par design, il est pr�vu que TData soit une isntanciation de PieceOfData
     */
    template<class TData>
    class Group : public MEM::Auto<Group<TData>>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        explicit        Group();                            //!< Constructeur par d�faut.
                        ~Group();                           //!< Destructeur.
        //@}
        //! @name Acc�s aux �l�vations
        //@{
        using TElement = TData;                             //!< Donn�e �l�mentaire contenue dans le groupe.

        void            insert(const TData& pPiece);        //!< Ajout d'une donn�e.
        void            insertInArea(const TData& pPiece);  //!< Ajout d'une donn�e.
        const TData&    operator[](size_t pN) const;        //!< R�cup�ration d'une donn�e.
        TData&          operator[](size_t pN);              //!< R�cup�ration d'une donn�e.
        U32             count() const;                      //!< Nombre d'�lement.
        //@}
        UTI::Rect       area() const;                       //!< Zone couverte.


    private:

        PIMPL()
    };


    using HeightGroup = Group<Height>;  //!< Groupe d'�l�vations (pour �dition).
    using TexelGroup  = Group<Texel>;   //!< Groupe de texels (pour �dition).


    /*! @brief "Carte" d'�l�vations.
        @version 0.2

        Dim max : uInt16
     */
    template<typename TValue, unsigned int TLen, class TAlloc = MEM::Allocator<TValue>>
    class HeightMap : public MEM::OnHeap
    {
    public:
        //! @name Informations g�n�rales
        //@{
        using TValueType = TValue;                                  //!< Type de donn�e (alias).

        enum
        {
            eLength = TLen                                          //!< Longueur de c�t�.
        };
        //@}
        //! @name Constructeurs
        //@{
                explicit        HeightMap(TValue pRefValue = 0);    //!< Constructeur (par d�faut).
                explicit        HeightMap(PAK::Loader& pLoader);    //!< Constructeur (chargement).
                                ~HeightMap();                       //!< Destructeur.
        //@}
        //! @name Donn�es
        //@{
                void            store(PAK::Saver& pSaver) const;    //!< Sauvegarde.
        inline  const TValue*   operator[](size_t pLine) const;     //!< Acc�s (constant) � une ligne de donn�es.
        inline  TValue*         operator[](size_t pLine);           //!< Acc�s � une ligne de donn�es.
                void            clear();                            //!< R�initialisation des donn�es.
        //@}
        //! @name Autres informations
        //@{
                void            setReference(TValue pRefValue);     //!< D�finition de la valeur de r�f�rence.
        inline  TValue          reference() const;                  //!< Valeur de r�f�rence.
        //@}

    private:

        FORBID_COPY(HeightMap)
        PIMPL()
    };


    using HeightField = HeightMap<I16, 1 + (eEdgeSize + 1) + 1>;    //!< Altitudes d'une tuile.


    template<class TOff, class TT>
    inline bool operator<(const PieceOfData<TOff, TT>& pL, const PieceOfData<TOff, TT>& pR);    //!< Op�rateur inf�rieur-�.

    inline bool operator==(const TexelData& pL, const TexelData& pR);                           //!< Op�rateur d'�galit�.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "HeightField.inl"

#endif  // De TILE_HEIGHTFIELD_HH
