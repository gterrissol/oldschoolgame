/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseTile.hh"

/*! @file ENGine/TILE/BaseTile.cc
    @brief M�thodes (non-inline) de la classe TILE::Tile.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 11 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <cstdlib>

#include "APPearance/BaseAppearance.hh"
#include "GEOmetry/Fields.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATerial/Texture.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/V2.hh"
#include "PAcKage/LoaderSaver.hh"
#include "SHaDers/Const.hh"
#include "STL/Shared.tcc"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"
#include "UTIlity/Rect.hh"

#include "HeightField.tcc"
#include "TileGeometry.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Texture de tuile.
        @version 0.5
     */
    class TileTexture : public MAT::Texture
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        TileTexture(ImagePtr pImage);   //!< Constructeur.
virtual ~TileTexture();                 //!< Destructeur.
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Mat�riau de tuile.
        @version 0.5
     */
    class TileMaterial : public MAT::Material
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        TileMaterial();     //!< Constructeur.
virtual ~TileMaterial();    //!< Destructeur.
        //@}
    };


//------------------------------------------------------------------------------
//                                P-Impl de Tile
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::Tile.
        @version 0.5
     */
    class Tile::Private : public MEM::OnHeap
    {
    public:
        //! @name Alias
        //@{
        using IV4   = std::array<I32, 4>;                       //!< Tableau de 4 entiers.
        using V2    = std::array<F32, 2>;                       //!< Tableau de 2 flottants.
        //@}
        //! @name Constructeurs
        //@{
                    Private();                                  //!< Constructeur par d�faut.
                    Private(PAK::Loader& pLoader);              //!< Constructeur (chargement imm�diat).
        //@}
        //! @name Encodage de la texture
        //@{
        void        saveTexture(PAK::Saver& pSaver) const;      //!< Sauvegarde de la texture encod�e.
        void        loadTexture(PAK::Loader& pLoader);          //!< Chargement de la texture encod�e.
        void        setTexture(const Encoding& pConf);          //!< Encodage de la texture.
        void        apply(U16 pTexel, const TexelData& pData);  //!< Application des donn�es d'un texel sur la tuile.
        TexelData   texelData(U16 pTexel) const;                //!< Donn�es d'un texel de la tuile
        //! Info pour les tilings d'un texel de heightfield.
        struct TilingInfo
        {
            TilingId    tilingId : 12;                          //!< 2048 ids possibles.
            FragId      fragId   : 4;                           //!< 16 fragments possibles.
        };
 static TilingInfo  tilingInfoFromFragTxf(V2 pFrag);            //!< Conversion : coordonn�es de fragment vers infos de tiling.
 static V2          fragTxyFromTilingInfo(TilingId pTilingId,
                                          FragId   pFragId);    //!< Conversion : infos de tiling vers coordonn�es de fragment.
        //@}
        Vector<GameObjectPtr>   mElements;                      //!< El�ments plac�s sur le carreau.
        UTI::Rect               mArea;                          //!< Zone de la tuile.
        //! @name Apparence
        //@{
        AppearancePtr           mSurface;                       //!< Surface du carreau (g�om�trie & mat�riau).
        HeightFieldPtr          mHeights;                       //!< El�vations de la tuile.
        TileGeometryPtr         mGround;                        //!< G�om�trie de la tuile.
        MaterialPtr             mMaterial;                      //!< Mat�riau de la tuile.
        //@}
        //! @name Gestion de la MegaTexture
        //@{
        void        initMegaTexture();                          //!< Initialisation.
        Vector<V2>                  mTexCoords;                 //!< Coordonn�es pour le placage de la megatexture.
        std::array<Vector<V2>, 4>   mFragsTxy;                  //!< Identifie les fragments � utiliser pour les quartiers A, B, C et D.
        Vector<IV4>                 mTTs;                       //!< Identifie les TilingsTextures pour les quartiers A, B, C et D.
        Vector<I32>                 mIsEnabled;                 //!< Le fragment est-il activ� ?
        Vector<I32>                 mBlending;                  //!< Fonction de m�lange des fragments.
        enum
        {
            eTexelCount = HeightField::eLength * HeightField::eLength
        };
        std::bitset<eTexelCount>    mDefined;                   //!< �tat de d�finition des texels.
        //@}
        //! @name Monde ext�rieur
        //@{
        ObjectPtr               mOutside;           //!< Pointeur sur le monde ext�rieur.
        PAK::FileHdl            mOutsideHdl;        //!< Handle du monde ext�rieur.
        //@}
        I32                     mAppIndex;
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    TileTexture::TileTexture(ImagePtr pImage)
        : MAT::Texture()
    {
        setManualImage(pImage);
        setMipMapParams(MAT::eLinearNone);
    }


    /*! Destructeur.
     */
    TileTexture::~TileTexture() { }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    TileMaterial::TileMaterial()
        : MAT::Material()
    {
        setProgram(SHD::kMegaTexProgram);
        addTexture(STL::makeShared<TileTexture>(FragmentMgr::get()->getFragments(Tiling::Id(0))), "uTilings[0]");
        addTexture(STL::makeShared<TileTexture>(FragmentMgr::get()->getFragments(Tiling::Id(1))), "uTilings[1]");
        addTexture(STL::makeShared<TileTexture>(FragmentMgr::get()->getFragments(Tiling::Id(2))), "uTilings[2]");
        addTexture(STL::makeShared<TileTexture>(FragmentMgr::get()->getFragments(Tiling::Id(3))), "uTilings[3]");
        addTexture(STL::makeShared<TileTexture>(FragmentMgr::get()->masks()),                     "uMasks");
    }


    /*! Destructeur.
     */
    TileMaterial::~TileMaterial() = default;


//------------------------------------------------------------------------------
//                                 Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Tile::Private::Private()
        : mElements()
        , mArea()
        , mSurface()
        , mHeights()
        , mGround()
        , mMaterial()
        , mOutside()
        , mOutsideHdl()
        , mAppIndex(-1)
    {
        mHeights  = STL::makeShared<HeightField>(k0W);
        mGround   = STL::makeShared<TileGeometry>(mHeights);
        mMaterial = STL::makeShared<MAT::Material>();
        mSurface  = STL::makeShared<APP::Appearance>(mGround, mMaterial);
        initMegaTexture();
    }


    /*! Constructeur (chargement imm�diat).
        @param pLoader Donn�es de la tuile
     */
    Tile::Private::Private(PAK::Loader& pLoader)
        : mElements()
        , mArea()
        , mSurface()
        , mHeights()
        , mGround()
        , mMaterial()
        , mOutside()
        , mOutsideHdl()
        , mAppIndex(-1)
    {
        pLoader >> mArea;
        pLoader >> mOutsideHdl;

        mHeights  = STL::makeShared<HeightField>(pLoader);
        mGround   = STL::makeShared<TileGeometry>(mHeights);
        loadTexture(pLoader);
        mMaterial = STL::makeShared<TileMaterial>();
        mSurface  = STL::makeShared<APP::Appearance>(mGround, mMaterial);

        // 2 - Elements du d�cor.
        U32 lElementCount;
        pLoader >> lElementCount;

        for(U32 lE = k0UL; lE < lElementCount; ++lE)
        {
            mElements.push_back(STL::makeShared<OBJ::GameObject>(pLoader));
        }
    }


//------------------------------------------------------------------------------
//                            Encodage de la Texture
//------------------------------------------------------------------------------

    /*!
     */
    void Tile::Private::saveTexture(PAK::Saver& pSaver) const
    {
        U16 lTexelCount = mGround->vertexCount();
        pSaver << lTexelCount;
        for(U16 lT = k0UW; lT < lTexelCount; ++lT)
        {
            TexelData   lTexelData  = texelData(lT);
            const U16*  lData       = reinterpret_cast<const U16*>(&lTexelData);
            pSaver << lData[0];
            pSaver << lData[1];
            pSaver << lData[2];
            pSaver << lData[3];
        }
    }


    /*!
     */
    void Tile::Private::loadTexture(PAK::Loader& pLoader)
    {
        U16 lTexelCount{};
        pLoader >> lTexelCount;

        U16 lVertexCount = mGround->vertexCount();

        mTexCoords.reserve(lVertexCount);
        mTexCoords.resize(0);
        const auto& lTL = std::const_pointer_cast<const TileGeometry>(mGround)->vertices3()[0];

        for(const auto& lV : std::const_pointer_cast<const TileGeometry>(mGround)->vertices3())
        {   // (-1,-1) ->(33,33)
            mTexCoords.push_back(V2{lV[U32{0}] - lTL[U32{0}] - k1F, lTL[U32{1}] - lV[U32{1}] - k1F});
        }
        mGround->addField("iTexCoords", mTexCoords);

        for(auto & lFrags : mFragsTxy)
        {
            lFrags.resize(lVertexCount, fragTxyFromTilingInfo(0, 0));
        }

        mIsEnabled.resize(lVertexCount, I32{true});
        mBlending.resize(lVertexCount, I32{});

        mTTs.resize(lVertexCount, IV4());

        for(U16 lT = k0UW; lT < lTexelCount; ++lT)
        {
            TexelData   lTexelData  = texelData(lT);
            U16*        lData       = reinterpret_cast<U16*>(&lTexelData);
            pLoader >> lData[0];
            pLoader >> lData[1];
            pLoader >> lData[2];
            pLoader >> lData[3];
            apply(lT, lTexelData);
        }

        mGround->addField("iFragsTxy[0]", mFragsTxy[0]);
        mGround->addField("iFragsTxy[1]", mFragsTxy[1]);
        mGround->addField("iFragsTxy[2]", mFragsTxy[2]);
        mGround->addField("iFragsTxy[3]", mFragsTxy[3]);
        mGround->addField("iTTs",         mTTs);
        mGround->addField("iEnabled",     mIsEnabled);
        mGround->addField("iBlending",    mBlending);
    }


    /*!
     */
    Tile::Private::TilingInfo Tile::Private::tilingInfoFromFragTxf(V2 pFrag)
    {
        static const F32 kOffset{(eTilingSize - eFragmentsInTilingSide * eFragmentSize) / 2.0F};

        pFrag[0] *= F32(eTilingsInTextureSide * eTilingSize);
        pFrag[1] *= F32(eTilingsInTextureSide * eTilingSize);
        auto lTx{static_cast<uint16_t>(pFrag[0]                     - kOffset) / eTilingSize};
        auto lTy{static_cast<uint16_t>(pFrag[1]                     - kOffset) / eTilingSize};
        auto lFx{static_cast<uint16_t>(pFrag[0] - lTx * eTilingSize - kOffset) / eFragmentSize};
        auto lFy{static_cast<uint16_t>(pFrag[1] - lTy * eTilingSize - kOffset) / eFragmentSize};
        return TilingInfo{static_cast<TilingId>(eTilingsInTextureSide  * lTy + lTx),
                          static_cast<FragId>(  eFragmentsInTilingSide * lFy + lFx)};
    }


    /*!
     */
    Tile::Private::V2 Tile::Private::fragTxyFromTilingInfo(TilingId pTilingId, FragId pFragId)
    {
        static const F32 kOffset{(eTilingSize - eFragmentsInTilingSide * eFragmentSize) / 2.0F};

        return
        {
            F32{(kOffset + (pTilingId % 4) * eTilingSize + (pFragId % 4) * eFragmentSize) / (1.0F * eTilingsInTextureSide * eTilingSize)},
            F32{(kOffset + (pTilingId / 4) * eTilingSize + (pFragId / 4) * eFragmentSize) / (1.0F * eTilingsInTextureSide * eTilingSize)},
        };
    }


    /*!
     */
    void Tile::Private::setTexture(const Encoding& pConf)
    {
        if (pConf.count() != mGround->vertexCount())
        {
            return;
        }

        for(U16 lT = k0UW; lT < pConf.count(); ++lT)
        {
            apply(lT, pConf[lT].data());
        }
    }


    /*! @param pTexel Identifiant du texel (dans [0, (1 + (eFragmentCount + 1) + 1) ^ 2 ]).
        @param pData  Nouvelles donn�es du texel.
     */
    void Tile::Private::apply(U16 pTexel, const TexelData& pData)
    {
        // Il faut une correspondance entre info.tilingIdN et le num�ro de la TilingsTexture sur laquelle est le tiling, ainsi que sa position.
        // Pour l'instant : 1�re texture, et la position et le num�ro du tiling.
        // + rappel : les 4 TilingsTextures sont les m�mes; il faudra reprendre le shader.
        mFragsTxy[0][pTexel] = fragTxyFromTilingInfo(pData.tilingId0, pData.fragId);
        mFragsTxy[1][pTexel] = fragTxyFromTilingInfo(pData.tilingId1, pData.fragId);
        mFragsTxy[2][pTexel] = fragTxyFromTilingInfo(pData.tilingId2, pData.fragId);
        mFragsTxy[3][pTexel] = fragTxyFromTilingInfo(pData.tilingId3, pData.fragId);
        pData.defined ? mDefined.set(pTexel) : mDefined.reset(pTexel);
        mIsEnabled[pTexel] = I32(pData.enabled);
        mBlending[pTexel] = I32(pData.blending);
        mTTs[pTexel] = IV4
        {
            I32(0),
            I32(0),
            I32(0),
            I32(0),
        };
    }


    /*! @param pTexel Identifiant du texel.
        @return Les donn�es du texel @p pTexel.
     */
    TexelData Tile::Private::texelData(U16 pTexel) const
    {
        return TexelData{
                   tilingInfoFromFragTxf(mFragsTxy[0][pTexel]).tilingId,
                   static_cast<uint16_t>(mDefined.test(pTexel)),
                   static_cast<uint16_t>(mIsEnabled[pTexel]),

                   tilingInfoFromFragTxf(mFragsTxy[1][pTexel]).tilingId,
                   static_cast<uint16_t>(mBlending[pTexel]),

                   tilingInfoFromFragTxf(mFragsTxy[2][pTexel]).tilingId,
                   static_cast<uint16_t>(1),    // fragDef (il n'y a que lors de l'initialisation de la M�gaTexture que ce flag peut �tre nul).

                   tilingInfoFromFragTxf(mFragsTxy[3][pTexel]).tilingId,
                   tilingInfoFromFragTxf(mFragsTxy[0][pTexel]).fragId
               };
    }


//------------------------------------------------------------------------------
//                                  Megatexture
//------------------------------------------------------------------------------

    // Temporaire : pour la mise en place de la megatexture.
    namespace
    {
        F32 coord(U16 i)
        {
            F32 lMargin = F32((eTilingSize - eFragmentsInTilingSide * eFragmentSize) / 2.0F);
            U16 lX(i % 4);
            U16 lT((i / 4) % 4);
            return F32{(lMargin + lT * eTilingSize + lX * eFragmentSize) / (1.0F * eTilingsInTextureSide * eTilingSize)};
        }
    }


    //!
    void Tile::Private::initMegaTexture()
    {
        auto    lCount = mGround->vertexCount();

        mTexCoords.reserve(lCount);
        const auto& lTL = std::const_pointer_cast<const TileGeometry>(mGround)->vertices3()[0];

        for(const auto& lV : std::const_pointer_cast<const TileGeometry>(mGround)->vertices3())
        {   // (-1,-1) ->(33,33)
            mTexCoords.push_back(V2{lV[U32{0}] - lTL[U32{0}] - k1F, lTL[U32{1}] - lV[U32{1}] - k1F});
        }
        mGround->addField("iTexCoords", mTexCoords);

        for(auto& lFrags : mFragsTxy) lFrags.resize(lCount);

        for(decltype(lCount) i = decltype(lCount){0}; i < lCount; ++i)
        {
            int x = rand() % std::numeric_limits<U16::TType>::max();
            int y = rand() % std::numeric_limits<U16::TType>::max();

             mFragsTxy[0][i] = V2{coord(U16((x +  0) % 64)), coord(U16((y +  0) % 64))};
             mFragsTxy[1][i] = V2{coord(U16((x +  4) % 64)), coord(U16((y +  4) % 64))};
             mFragsTxy[2][i] = V2{coord(U16((x +  8) % 64)), coord(U16((y +  8) % 64))};
             mFragsTxy[3][i] = V2{coord(U16((x + 12) % 64)), coord(U16((y + 12) % 64))};
        }
        mGround->addField("iFragsTxy[0]", mFragsTxy[0]);
        mGround->addField("iFragsTxy[1]", mFragsTxy[1]);
        mGround->addField("iFragsTxy[2]", mFragsTxy[2]);
        mGround->addField("iFragsTxy[3]", mFragsTxy[3]);

        mTTs.resize(lCount, IV4{I32{0}, I32{0}, I32{0}, I32{0}});
        mGround->addField("iTTs", mTTs);

        mIsEnabled.resize(lCount, I32(true));
        mGround->addField("iEnabled", mIsEnabled);

        mBlending.resize(lCount, I32(0));
        mGround->addField("iBlending", mBlending);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Construit une instance � partir des donn�es d'un fichier.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Tile::Tile(PAK::Loader& pLoader)
        : PLC::Place(pLoader)
        , pthis(pLoader)
    {
        pthis->mSurface->bind(this->OBJ::GameEntity::shared_from_this());
    }


    /*! @param pSaver   Saver dans lequel placer les donn�es d'une tuile minimale valide
        @param pHeights Altitudes de la tuile
        @param pOut     Handle du fichier du monde ext�rieur
     */
    void Tile::saveDefault(PAK::Saver& pSaver, const HeightGroup& pHeights, PAK::FileHdl pOut)
    {
        pSaver << pHeights.area();
        pSaver << pOut;

        // Heightfield.
        pSaver << k0W;
        // NB: les �l�vations "sur les bords" (i.e. appartenant aux voisines) ne sont pas contenues
        // dans pHeights; elles dont donc ajout�es manuellement (je sais, c'est un peu lourd).
        for(U32 lH = k0UL; lH < HeightField::eLength; ++lH)
        {
            pSaver << HeightGroup::TElement::TType(0);
        }
        U32 lH = k0UL;
        for(U32 lLi = k0UL; lLi < HeightField::eLength - 2; ++lLi)
        {
            pSaver << HeightGroup::TElement::TType(0);
            for(U32 lCo = k0UL; lCo < HeightField::eLength - 2; ++lCo)
            {
                pSaver << pHeights[lH++].data();
            }
            pSaver << HeightGroup::TElement::TType(0);
        }
        for(U32 lH = k0UL; lH < HeightField::eLength; ++lH)
        {
            pSaver << HeightGroup::TElement::TType(0);
        }

        // Texture.
        pSaver << k0UW;
    }


    /*! Destructeur.
     */
    Tile::~Tile()
    {
        saveSelf();
    }


//------------------------------------------------------------------------------
//                      Information sur la Zone de la Tuile
//------------------------------------------------------------------------------

    /*!
     */
    const UTI::Rect& Tile::area() const
    {
        return pthis->mArea;
    }


    /*! @param pPosition Position, en coordonn�es, pour laquelle l'altitude la la tuile est recherch�e
        @note Code temporaire, en attendant l'int�gration d'DE
     */
    F32 Tile::height(const MATH::V2& pPosition) const
    {
        const UTI::Rect&    lArea   = area();
        MATH::V2            lLocal  = MATH::V2(pPosition.x - lArea.left(), lArea.top() - pPosition.y);

        F32 lFracX      = F32(1.0F + lLocal.x); // + 1 => bordure
        F32 lFracY      = F32(1.0F + lLocal.y); // + 1 => bordure
        I32 lRoundedCol = I32(floorf(lFracX));
        I32 lRoundedRow = I32(floorf(lFracY));
        lFracX          = lFracX - F32(lRoundedCol);
        lFracY          = lFracY - F32(lRoundedRow);

        HeightField*    lHeights    = pthis->mHeights.get();
        if (lHeights != nullptr)
        {
            Height::TType  lX_Y        = (*lHeights)[lRoundedRow]    [lRoundedCol];
            Height::TType  lX_1_Y      = (*lHeights)[lRoundedRow]    [lRoundedCol + 1];
            Height::TType  lX_Y_1      = (*lHeights)[lRoundedRow + 1][lRoundedCol];
            Height::TType  lX_1_Y_1    = (*lHeights)[lRoundedRow + 1][lRoundedCol + 1];

            F32 lHeight = F32(((lX_Y *   (1.0F - lFracX) + lX_Y_1   * lFracX) +
                               (lX_1_Y * (1.0F - lFracY) + lX_1_Y_1 * lFracY)) * 0.5F);

            return lHeight * TILE::kVerticalStep;
        }
        else
        {
            return k0F;
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void Tile::store(PAK::Saver& pSaver) const
    {
        RSC::Object::store(pSaver);

        pSaver << pthis->mArea;
        pSaver << pthis->mOutsideHdl;
        heights()->store(pSaver);
        pthis->saveTexture(pSaver);

        U32 lElementCount(pthis->mElements.size());
        pSaver << lElementCount;
        for(U32 lE = k0UL; lE < lElementCount; ++lE)
        {
            pthis->mElements[lE]->store(pSaver);
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
     */
    void Tile::swap(Tile& pLoadedTile)
    {
        pthis.swap(pLoadedTile.pthis);
        pthis->mSurface->bind(this->OBJ::GameEntity::shared_from_this());
    }


//------------------------------------------------------------------------------
//                             Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut (tuile temporaire).
     */
    Tile::Tile()
        : PLC::Place()
        , pthis()
    {
        pthis->mSurface->bind(this->OBJ::GameEntity::shared_from_this());
    }


    /*! Constructeur (chargement diff�r�).<br>
        L'instance doit �tre retourn�e valide (tout comme le constructeur par d�faut).
     */
    Tile::Tile(PAK::FileHdl pFileHdl)
        : PLC::Place(pFileHdl)
        , pthis()
    { }


    /*! Ce constructeur sera appel� par une classe d�riv�e (vraisemblablement DAT::EditableObj<Tile>) pour cr�er une nouvelle instance
        de tuile dans le moteur.
        @param pArea Zone "couverte" par cette tuile
        @param pOut  Handle du fichier du monde auquel appartient la tuile
     */
    void Tile::build(const UTI::Rect& pArea, PAK::FileHdl pOut)
    {
        if ( pthis->mArea.isNull()                &&
            (pthis->mOutsideHdl == PAK::eBadHdl) &&
            !pArea.isNull()                        &&
            (pOut != PAK::eBadHdl))
        {
            pthis->mArea = pArea;
            pthis->mOutsideHdl = pOut;
        }
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*! Place un �l�ment sur le carreau.
        @param pElement Element (d�cor) � placer sur la tuile
     */
    void Tile::addElement(GameObjectPtr pElement)
    {
        ASSERT(pElement != nullptr, kInvalidGameObject)

        MATH::V3    lPos    = pElement->position();
        U32         lLi     = U32(pthis->mArea.top() - lPos[1]);
        U32         lCo     = U32(lPos[0] - pthis->mArea.left());

        if ((lCo < eEdgeSize) && (lLi < eEdgeSize))
        {
            lPos[2] = F32((*pthis->mHeights)[lLi][lCo] * kVerticalStep);
            pElement->setPosition(lPos);
            //pElement->reparent(this);
            pthis->mElements.push_back(pElement);
        }
    }


    /*! Retire un �l�ment du carreau.
        @param pElement Element (d�cor) � retirer de la tuile
     */
    void Tile::removeElement(GameObjectPtr pElement)
    {
        auto lIter = std::find(pthis->mElements.begin(), pthis->mElements.end(), pElement);

        if (lIter != pthis->mElements.end())
        {
            std::swap(*lIter, pthis->mElements.back());
            pthis->mElements.pop_back();
            pElement->reparent(GameObjectPtr());
        }

    }


//------------------------------------------------------------------------------
//                       Acc�s aux Donn�es (pour Edition)
//------------------------------------------------------------------------------

    /*!
     */
    Tile::ConstHeightFieldPtr Tile::heights() const
    {
        return pthis->mHeights;
    }


    /*!
     */
    Tile::HeightFieldPtr Tile::heights()
    {
        return pthis->mHeights;
    }


    /*!
     */
    Tile::TileGeometryPtr Tile::ground()
    {
        return pthis->mGround;
    }


    /*! @todo Mettre � jour les donn�es de la megatexture (mTexCoords etc) � partir des donn�es de @p pConf.
     */
    void Tile::setTexture(const Encoding& pConf)
    {
        pthis->setTexture(pConf);
    }

    /*! @todo Reconstruire l'encodage � partir des donn�es de la megatexture (mTexCoords etc).
     */
    Tile::Encoding Tile::texture() const
    {
        // Coming soon...
        return Encoding{};
    }


    /*!
     */
    Tile::ObjectPtr Tile::outside() const
    {
        if (pthis->mOutside.expired())
        {
            const auto& lOutsides = RSC::ObjectMgr::get()->instances(pthis->mOutsideHdl);

            if (!lOutsides.empty())
            {
                pthis->mOutside = lOutsides.front();
            }
        }

        return pthis->mOutside;
    }


//------------------------------------------------------------------------------
//                            Parcours des Apparences
//------------------------------------------------------------------------------

    /*! Premi�re apparence visible.
     */
    bool Tile::first(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        pthis->mAppIndex = I32(-1);

        return current(pApp, pFrame);
    }


    /*! Apparence visible suivante.
     */
    bool Tile::next(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        if (pthis->mAppIndex < I32(pthis->mElements.size()))
        {
            ++pthis->mAppIndex;
        }

        return current(pApp, pFrame);
    }


    /*! Apparence visible actuelle.
     */
    bool Tile::current(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        if (pthis->mAppIndex == -1)
        {
            pApp = pthis->mSurface;
            pFrame.loadIdentity();
            pFrame.L.x = area().center().x;
            pFrame.L.y = area().center().y;

            return true;
        }
        else if (pthis->mAppIndex < I32(pthis->mElements.size()))
        {
            GameObjectPtr   lObject = pthis->mElements[pthis->mAppIndex];
            pApp    = lObject->appearance();
            pFrame  = lObject->frame();

            return true;
        }
        else
        {
            pApp = nullptr;
            pFrame.loadIdentity();

            return false;
        }
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  RSC::ResourceMgr<TileRsc>*  MEM::Singleton<RSC::ResourceMgr<TileRsc>>::smThat   = nullptr;
#endif
}
