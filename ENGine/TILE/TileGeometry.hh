/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_TILEGEOMETRY_HH
#define TILE_TILEGEOMETRY_HH

#include "TILE.hh"

/*! @file ENGine/TILE/TileGeometry.hh
    @brief En-t�te des classes TILE::TileGeometry & TILE::TileGeometryFX.
    @author @ref Guillaume_Terrissol
    @date 24 Avril 2003 - 28 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/CoLoR.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/STL.hh"

#include "HeightField.hh"

namespace TILE
{
    /*! @brief G�om�trie (surface) d'une tuile.
        @version 0.2

        Cette classe regroupe les altitudes d'une instance de Tile.<br>
        Elle permet �galement de construire une instance de GEO::Geometry � partir de ses donn�es.
     */
    class TileGeometry : public GEO::Geometry
    {
    public :
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<HeightField const>    ConstHeightFieldPtr;    //!< Pointeur (constant) sur champ d'altitudes.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit    TileGeometry(ConstHeightFieldPtr pElevations);      //!< Constructeur.
virtual             ~TileGeometry();                                    //!< Destructeur.
        //@}
        //! @name Liste de triangles
        //@{
 static void        buildTriangleList();                                //!< Construction de la liste de triangles type.
 static void        destroyTriangleList();                              //!< Destruction de la liste de triangles type.
        //@}
        //! @name M�thodes assistantes
        //@{
        void        update(ConstHeightFieldPtr pElevations);            //!< Mise � jour de la g�om�trie.

    protected:

        void        computeNormals();                                   //!< R�estimation de toutes les normales.
        //@}
        //! @name "Permanence" des donn�es
        //@{
virtual void        store(PAK::Saver& pSaver) const;                    //!< Sauvegarde des donn�es.
        //@}

    private:

        FORBID_COPY(TileGeometry)

        void        buildFirstTime(ConstHeightFieldPtr pElevations);    //!< Premi�re construction.
    };


    /*! @brief G�om�trie de tuile pour effets.
        @version 0.2

        Pour l'instant, seule la couleur des tuiles peut �tre modifi�e.
     */
    class TileGeometryFX : public GEO::Geometry
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        explicit    TileGeometryFX(const CLR::RGBA& pColor);    //!< Constructeur.
virtual             ~TileGeometryFX();                          //!< Destructeur.
        //@}
        void        setColor(const CLR::RGBA& pNewColor);       //!< D�finition de la couleur.
    };
}

#endif  // De TILE_TILEGEOMETRY_HH
