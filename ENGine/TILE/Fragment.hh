/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_FRAGMENT_HH
#define TILE_FRAGMENT_HH

#include "TILE.hh"

/*! @file ENGine/TILE/Fragment.hh
    @brief En-t�te des classes TILE::Image, TILE::Fragment & TILE::FragmentMgr.
    @author @ref Guillaume_Terrissol
    @date 7 Ao�t 2006 - 26 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <array>

#include "CoLoR/CoLoR.hh"
#include "MATerial/Image.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"

#include "Enum.hh"
#include "HeightField.hh"

namespace TILE
{
    /*! @brief "Famille" de fragments de texture de tuile.
        @version 0.7

        Les fragments d'une famille sont maintenent regroup�s dans une m�me image, � une seule r�solution.@n
        Les fragments font 100x100 texels (Cf eFragmentSize).@n
        L'image de la famille fait 512x512 (Cf eTilingSize).@n
        La bordure est une r�p�tition des bords des c�t�s, reconstitut�e au chargement.
     */
    class Tiling
    {
    public:
        //! @name Pointeur
        //@{
        using Ptr = SharedPtr<Tiling>;                      //!< Pointeur sur Tiling.
        using Id  = I16;                                    //!< Identifiant.
        //@}
        //! @name Constructeur
        //@{
                    Tiling();                               //!< ... par d�faut.
                    Tiling(PAK::Loader& pLoader);           //!< Chargement.
        //@}
        //! @name Donn�es
        //@{
        void        store(PAK::Saver& pSaver);              //!< Sauvegarde.
        const void* data() const;                           //!< Acc�s global.
        const U32*  line(U32 pLine) const;                  //!< Acc�s constant par ligne.
        U32*        line(U32 pLine);                        //!< Acc�s par ligne.

        U32         computeTilingColor() const;             //!< Couleur moyenne.

 static U32         width();                                //!< Largeur des fragments.
 static U32         height();                               //!< Hauteur des fragments.

    private:

 static U32         margin();                               //!< Marge autour des fragments.
        std::array<U32, eTilingSize * eTilingSize>  mData;  //!< D�finition.
        //@}
    };


    /*! @brief Texture de fragments (aggr�gat de Tiling).
        @version 0.5

        Composition de 4x4 instances de Tiling : ce sont ces textures qui seront utilis�es pour l'application de la M�gaTexture.

        @todo G�n�ration des mipmaps.
        @todo Possibilit� de mettre � jour partiellement dans OpenGL via glTexSubImage.
    */
    class TilingsTexture : public MAT::Image
    {
    public:

        using Ptr       = SharedPtr<TilingsTexture>;
        using Tilings   = std::array<Tiling::Id, eTilingsInTexture>;

        using MAT::Image::setData;
        using MAT::Image::setLine;

                TilingsTexture();
                TilingsTexture(const Tilings& pList);
virtual         ~TilingsTexture();

        void    update(const Tilings& pNewList);

    private:

        // A compl�ter...
    };


    /*! @brief Banque de fragments de texture de tuile.
        @version 0.8

        Les fragments de texture de tuile, bien qu'�tant des images, ne sont pas g�r�es par ImageMgr, car
        elles ont imp�rativement besoin d'�tre r�ellement disponibles lors de leur accession.<br>
        Je me suis bas� en partie sur l'interface de RSC::ResourceMgr afin de profiter de certains
        m�canismes dej� en place (notamment la gestion au niveau de PROG::PakFileLayout).
     */
    class FragmentMgr : public MEM::Singleton<FragmentMgr>, public MEM::OnHeap
    {
        //! @name Classe amie
        //@{
 friend class FragmentMgrController;                                                    //!< Pour les m�thodes d'�dition.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        using Ptr = SharedPtr<FragmentMgr>;                                             //!< Pointeur sur gestionnaire.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit                FragmentMgr();                                          //!< Constructeur (chargement).
                                ~FragmentMgr();                                         //!< Destructeur.
        //@}
        //! @name Sauvegarde & restauration des fragments
        //@{
        void                    save() const;                                           //!< Sauvegarde des ressources.
        void                    load(PAK::Loader& pLoader);                             //!< Chargement des ressources.
        PAK::FileHdl            fileHdl() const;                                        //!< Handle du fichier des donn�es.
        //@}
        //! @name Informations sur les groupes de fragments
        //@{
        using TilingCount = MEM::BuiltIn<std::make_unsigned<Tiling::Id::TType>::type>;  //!< Type du nombre de Tilings.
        TilingCount             tilingCount() const;                                    //!< Nombre de familles.
        CLR::RGBA               tilingColor(Tiling::Id pId) const;                      //!< Couleur associ�e � une famille.
        const Tiling::Ptr       tiling(Tiling::Id pId) const;                           //!< Fragments d'une famille.
        //@}
        //! @name Informations sur les masques
        //@{
        const MAT::Image::Ptr   masks() const;
        //@}
        TilingsTexture::Ptr     getFragments(Tiling::Id pid);
        //! @name Encodage des familles et des fragments
        //@{
 static bool                    isTexelAlreadySet(const TexelData& pTexel);             //!< Fragment modifi� par l'utilisateur ?
 static TexelData               inactiveTexel();                                        //!< Texel inactif (� ne pas modifier).
 static bool                    isTexelInactive(const TexelData& pTexel);               //!< Texel inactif ?
        //@}
    protected:
        //! @name M�thodes d'�dition
        //@{
        Tiling::Ptr             editTiling(Tiling::Id pId);                             //!< Groupe de fragments (pour �dition).
        Tiling::Id              createTiling();                                         //!< Cr�ation d'une famille.
        void                    removeTiling(Tiling::Id pId);                           //!< Suppression d'une famille.
        //@}
    private:

        FORBID_COPY(FragmentMgr)
        PIMPL()
    };
}

#endif  // De TILE_FRAGMENT_HH
