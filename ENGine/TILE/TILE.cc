/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "TILE.hh"

/*! @file ENGine/TILE/TILE.cc
    @brief D�finitions diverses du module TILE.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <limits>

#include "ERRor/ErrMsg.hh"

#include "Const.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//                           D�finition de Constantes
//------------------------------------------------------------------------------

    const F32   kVerticalStep   = F32(0.05F);
    const U16   kFamilyMaxSize  = U16(std::numeric_limits<U16::TType>::max());
    // La couleur par d�faut (0) est enregistr�e automatiquement, la couleur "vide" est inexploitable.
    const U16   kMaxFamilyCount = U16(256);


//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Message d'assertion.
    REGISTER_ERR_MSG(kColorAlreadyUsed,     "Couleur d�j� utilis�e.",                   "Color already used.")
    REGISTER_ERR_MSG(kHeigtfieldMustExist,  "Le champ d'�l�vations doit exister.",      "The heightfield must exist.")
    REGISTER_ERR_MSG(kInvalidColorIndex,    "Indice de couleur invalide.",              "Invalid color index.")
    REGISTER_ERR_MSG(kInvalidElementIndex,  "Indice d'�l�ment invalide.",               "Invalid element index.")
    REGISTER_ERR_MSG(kInvalidFamilyIndex,   "Indice de famille de fragments invalide.", "Invalid fragment family index.")
    REGISTER_ERR_MSG(kInvalidFamilyName,    "Nom de famille de fragments invalide.",    "Invalid fragment family name.")
    REGISTER_ERR_MSG(kInvalidFragmentIndex, "Indice de fragment de texture invalide.",  "Invalid fragment texture index.")
    REGISTER_ERR_MSG(kInvalidGameObject,    "Game Object invalide.",                    "Invalid Game Object.")
    REGISTER_ERR_MSG(kInvalidMaskIndex,     "Indice de masque invalide.",               "Invalid masl index.")
    REGISTER_ERR_MSG(kInvalidResolution,    "R�solution invalide.",                     "Invalid resolution.")
    REGISTER_ERR_MSG(kInvalidRowIndex,      "Indice de ligne invalide.",                "Invalid row index.")
    REGISTER_ERR_MSG(kTileEncodingTooLong,  "Encodage de texture de tuile trop long.",  "Tile encoding too long.")
    REGISTER_ERR_MSG(kTooManyFamilies,      "Trop de familles.",                        "Too many families.")
    REGISTER_ERR_MSG(kTooManyFragments,     "Trop de fragments.",                       "Too many fragments.")
    REGISTER_ERR_MSG(kTooManyMasks,         "Trop de masques.",                         "Too many masks.")
}
