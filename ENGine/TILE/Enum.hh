/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_ENUM_HH
#define TILE_ENUM_HH

/*! @file ENGine/TILE/Enum.hh
    @brief Enum�rations du module TILE.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 30 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TILE
{
    enum
    {
        eEdgeSize               = 32,   //!< Longueur du c�t� d'une tuile, en carr�s �l�mentaires (2 triangles).

        eFragmentCount          = 32,   //!< Nombre de fragments de texture pour [le c�t� d'] une tuile.
        eFragmentSize           = 100,  //!< Taille, en texels, [du c�t�] des fragments de texture de tuile.
        eFragmentsInTilingSide  = 4,    //!< Nombre de fragments par [c�t� de] famille.
        eFragmentsInTiling      = 16,   //!< Nombre de fragments par famille (eFragmentsInTilingSide ^ 2).

        eTilingSize             = 512,  //!< Taille, en texels, [du c�t�] de la texture d'un groupe de fragments.
        eTilingsInTextureSide   = 4,    //!< Nombre de tilings par [c�t� de] texture de tilings.
        eTilingsInTexture       = 16,   //!< Nombre de tilings par texture (eTilingsInTextureSide ^ 2).
        eMaxTilingCount         = 2048, //!< Nombre maximal de tilings.

        eMaskSize               = 32,   //!< Taille, en texels, [du c�t�] d'un masque de m�lange de fragment.
        eMaskSlotCount          = 4,    //!< Nombre de masques pour une variation.
        eMaskTypeCount          = 4     //!< Nombre de variations de m�langes de masques.
    };


    //! Voisins.
    enum ENeighbours
    {
        eNorthWest  = 0,        //!< Nord-Ouest.
        eNorth,                 //!< Nord.
        eNorthEast,             //!< Nord-Est.
        eEast,                  //!< Est.
        eSouthEast,             //!< Sud-Est.
        eSouth,                 //!< Sud.
        eSouthWest,             //!< Sud-Ouest.
        eWest,                  //!< Ouest.
        eNeighbourCount         //!< Nombre de voisins.
    };


    //! M�lange de tilings.
    enum EBlending
    {
        eHard,
        eSmooth,
        eSoft,
        eBlurry,
        eFull,
        eEmpty,
        eBlendingCount
    };
}

#endif // De TILE_ENUM_HH
