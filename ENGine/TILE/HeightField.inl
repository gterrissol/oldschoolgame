/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file ENGine/TILE/HeightField.inl
    @brief M�thodes inline des classes TILE::PieceOfData, TILE::Group & TILE::HeightMap.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2003 - 26 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TILE
{
//------------------------------------------------------------------------------
//                         Piece of Data : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pC Colonne de la donn�e
        @param pR Ligne de la donn�e
        @param pD Valeur de la donn�e
     */
    template<class TOff, class TT>
    inline PieceOfData<TOff, TT>::PieceOfData(TOff pC, TOff pR, TT pD)
        : mCol(pC)
        , mRow(pR)
        , mData(pD)
    { }


    /*! Cette m�thode permet de facilement "�diter" la "donn�e" (la position associ�e est fix�e une fois
        pour toute).
        @param pD Nouvelle valeur � porter
        @return L'instance
     */
    template<class TOff, class TT>
    inline PieceOfData<TOff, TT>& PieceOfData<TOff, TT>::operator=(TT pD)
    {
        mData = pD;

        return *this;
    }


//------------------------------------------------------------------------------
//                         Piece of Data : Informations
//------------------------------------------------------------------------------

    /*! @return La colonne de la donn�e
     */
    template<class TOff, class TT>
    inline TOff PieceOfData<TOff, TT>::col() const
    {
        return mCol;
    }


    /*! @return La Ligne de la donn�e
     */
    template<class TOff, class TT>
    inline TOff PieceOfData<TOff, TT>::row() const
    {
        return mRow;
    }


    /*! @return La valeur de la donn�e (pour une �l�vation, l'�chelle n'est pas appliqu�e : voir @sa
        HeightMap::verticalScale())
     */
    template<class TOff, class TT>
    inline TT PieceOfData<TOff, TT>::data() const
    {
        return mData;
    }


//------------------------------------------------------------------------------
//                                  Op�rateurs
//------------------------------------------------------------------------------

    /*! Afin de pouvoir trier les informations, l'op�ration inf�rieur � �tait indispensable : le voici
        donc.
        @param pL 1er �lement � comparer
        @param pR 2nd �lement � comparer
        @return Vrai si <i>pL</i> est strictement inf�rieur � <i>pR</i> (les composants sont compar�s
        selon l'ordre suivant : PieceOfData<TOff, TT>::data(), puis PieceOfData<TOff, TT>::row(), et
        enfin PieceOfData<TOff, TT>::col())
     */
    template<class TOff, class TT>
    inline bool operator<(const PieceOfData<TOff, TT>& pL, const PieceOfData<TOff, TT>& pR)
    {
        if  (pL.data() == pR.data())
        {
            if (pL.row() == pR.row())
            {
                return (pL.col() < pR.col());
            }
            else
            {
                return (pL.row() < pR.row());
            }
        }
        else 
        {
            return (pL.data() < pR.data());
        }
    }


    /*! @param pL Op�rande gauche.
        @param pR Op�rande droit.
        @return Vrai si @p pL == @p pR
     */
    inline bool operator==(const TexelData& pL, const TexelData& pR)
    {
        return (reinterpret_cast<const std::array<uint16_t, 5>&>(pL) == reinterpret_cast<const std::array<uint16_t, 5>&>(pR));
    }
}
