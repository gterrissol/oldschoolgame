/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "HeightField.hh"

/*! @file ENGine/TILE/HeightField.cc
    @brief M�thodes (non-inline) de la classe TILE::cl_HeightField.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "HeightField.tcc"

namespace TILE
{
    template    class Group<Height>;
    template    class Group<Texel>;
}
