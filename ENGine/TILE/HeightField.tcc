/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_HEIGHTFIELD_TCC
#define TILE_HEIGHTFIELD_TCC

#include "HeightField.hh"

/*! @file ENGine/TILE/HeightField.tcc
    @brief M�thodes inline des classes TILE::PieceOfData, TILE::Group & TILE::HeightMap.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2008 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Limits.hh"
#include "STL/Vector.hh"
#include "UTIlity/Rect.hh"

#include "ErrMsg.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//                           Elevation Group : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::Group<TData>.
        @version 0.4
     */
    template<class TData>
    class Group<TData>::Private : public MEM::OnHeap
    {
    public:

                        Private();                      //!< Constructeur.

        inline  void    checkArea(const TData& pPiece); //!< Extension de la zone

        typedef Vector<TData>   Area;                   //!< Conteneur d'�l�ments.
        Area                    mElements;              //!< Liste des �l�ments.
        UTI::Rect               mArea;                  //!< Zone couverte.
    };


//------------------------------------------------------------------------------
//                     Elevation Group P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*!
     */
    template<class TData>
    Group<TData>::Private::Private()
        : mElements()
        , mArea(F32(std::numeric_limits<typename TData::TOffset>::max()),
                F32(std::numeric_limits<typename TData::TOffset>::min()),
                F32(std::numeric_limits<typename TData::TOffset>::min()),
                F32(std::numeric_limits<typename TData::TOffset>::max()))
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    template<class TData>
    inline void Group<TData>::Private::checkArea(const TData& pPiece)
    {
        if (pPiece.col() < mArea.left())
        {
            mArea.setLeft(F32(pPiece.col()));
        }
        if (mArea.right() < pPiece.col())
        {
            mArea.setRight(F32(pPiece.col()));
        }
        if (pPiece.row() < mArea.bottom())
        {
            mArea.setBottom(F32(pPiece.row()));
        }
        if (mArea.top() < pPiece.row())
        {
            mArea.setTop(F32(pPiece.row()));
        }
    }


//------------------------------------------------------------------------------
//                 Elevation Group : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TData>
    Group<TData>::Group() : pthis() { }


    /*! Destructeur.
     */
    template<class TData>
    Group<TData>::~Group() { }


//------------------------------------------------------------------------------
//                    Elevation Group : Acc�s aux El�vations
//------------------------------------------------------------------------------

    /*! Ajoute une donn�e dans le groupe.
        @param pPiece Donn�e � ajouter
        @warning Aucun contr�le n'est effectu� sur la pr�sence pr�alable de la donn�e � ajouter
     */
    template<class TData>
    void Group<TData>::insert(const TData& pPiece)
    {
        pthis->mElements.push_back(pPiece);
    }


    /*! Ajoute une donn�e dans le groupe.
        @param pPiece Donn�e � ajouter
     */
    template<class TData>
    void Group<TData>::insertInArea(const TData& pPiece)
    {
        insert(pPiece);

        pthis->checkArea(pPiece);
    }


    /*! R�cup�re une donn�e.
        @param pN Index de la donn�e � r�cup�rer
        @return L'<i>pN</i>i�me donn�e
     */
    template<class TData>
    inline const TData& Group<TData>::operator[](size_t pN) const
    {
        return pthis->mElements[pN];
    }


    /*! R�cup�re une donn�e.
        @param pN Index de la donn�e � r�cup�rer
        @return L'<i>pN</i>i�me donn�e
     */
    template<class TData>
    inline TData& Group<TData>::operator[](size_t pN)
    {
        return pthis->mElements[pN];
    }


    /*! @return Le nombre d'�l�ments du groupe
     */
    template<class TData>
    inline U32 Group<TData>::count() const
    {
        return U32(pthis->mElements.size());
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return La zone couverte par les �l�vations
     */
    template<class TData>
    inline UTI::Rect Group<TData>::area() const
    {
        return pthis->mArea;
    }


//------------------------------------------------------------------------------
//                              HeightMap : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::HeightMap<TValue, TLen, TAlloc>.
        @version 0.2
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    class HeightMap<TValue, TLen, TAlloc>::Private : public MEM::OnHeap
    {
    public:

        Private(TValue pRef);                   //!< Constructeur.
        std::vector<TValue, TAlloc> mValues;    //!< El�vations.
        TValue                      mReference; //!< Altitude de r�f�rence.
    };


//------------------------------------------------------------------------------
//                        HeightMap P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pRef Valeur par d�faut pour les donn�es
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    HeightMap<TValue, TLen, TAlloc>::Private::Private(TValue pRef)
        : mValues()
        , mReference(pRef)
    {
        mValues.resize(eLength * eLength, mReference);
    }


//------------------------------------------------------------------------------
//                    HeightMap : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
        @param pRefValue Valeur par d�faut pour toutes les donn�es
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    HeightMap<TValue, TLen, TAlloc>::HeightMap(TValue pRefValue) : pthis(pRefValue) { }


    /*! Constructeur (chargement).
        @param pLoader Fichier � partir duquel lire les donn�es
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    HeightMap<TValue, TLen, TAlloc>::HeightMap(PAK::Loader& pLoader) : pthis(TValue(0))
    {
        pLoader >> pthis->mReference;

        pthis->mValues.resize(eLength * eLength, pthis->mReference);
        for(auto lValueI = pthis->mValues.begin(); lValueI != pthis->mValues.end(); ++lValueI)
        {
            pLoader >> (*lValueI);
        }
    }


    /*! Destructeur.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    HeightMap<TValue, TLen, TAlloc>::~HeightMap() { }


//------------------------------------------------------------------------------
//                              HeightMap : Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde les donn�es de la carte.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    inline void HeightMap<TValue, TLen, TAlloc>::store(PAK::Saver& pSaver) const
    {
        pSaver << pthis->mReference;

        for(auto lValueI = pthis->mValues.begin(); lValueI != pthis->mValues.end(); ++lValueI)
        {
            pSaver << (*lValueI);
        }
    }


    /*! Acc�s (constant) � une ligne d'�l�vations.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    inline const TValue* HeightMap<TValue, TLen, TAlloc>::operator[](size_t pLine) const
    {
        ASSERT(pLine < eLength, kInvalidRowIndex)

        return reinterpret_cast<const TValue*>(&pthis->mValues[pLine * eLength]);
    }


    /*! Acc�s � une ligne d'�l�vations.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    inline TValue* HeightMap<TValue, TLen, TAlloc>::operator[](size_t pLine)
    {
        ASSERT(pLine < eLength, kInvalidRowIndex)

        return reinterpret_cast<TValue*>(&pthis->mValues[pLine * eLength]);
    }


    /*! R�initialisation des donn�es.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    void HeightMap<TValue, TLen, TAlloc>::clear()
    {
        std::fill(pthis->mValues.begin(), pthis->mValues.end(), TValue(0));
    }


//------------------------------------------------------------------------------
//                        HeightMap : Autres Informations
//------------------------------------------------------------------------------

    /*! D�finition de l'altitude de r�f�rence.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    void HeightMap<TValue, TLen, TAlloc>::setReference(TValue pRefValue)
    {
        pthis->mReference = pRefValue;
    }


    /*! Altitude de r�f�rence.
     */
    template<typename TValue, unsigned int TLen, class TAlloc>
    inline TValue HeightMap<TValue, TLen, TAlloc>::reference() const
    {
        return pthis->mReference;
    }
}

#endif  // De TILE_HEIGHTFIELD_TCC
