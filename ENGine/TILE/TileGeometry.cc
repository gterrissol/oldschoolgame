/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "TileGeometry.hh"

/*! @file ENGine/TILE/TileGeometry.cc
    @brief M�thodes (non-inline) des classes TILE::TileGeometry & TILE::TileGeometryFX.
    @author @ref Guillaume_Terrissol
    @date 24 Avril 2003 - 15 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "CoLoR/RGBA.hh"
#include "GEOmetry/Fields.hh"
#include "GEOmetry/List.tcc"
#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "STL/SharedPtr.hh"

#include "ErrMsg.hh"
#include "HeightField.tcc"

namespace
{
    using TriangleListPtr = SharedPtr<GEO::TriangleList>;           //!< Pointeur sur liste de triangles.

    TriangleListPtr                         gTriangles;             //!< Liste de triangles des g�om�tries de tuiles.
    std::unique_ptr<Vector<GEO::Triangle>>  gTrianglesOnBorders;    //!< Liste de triangles sur les bords des g�om�tries de tuiles.
}

namespace TILE
{
//------------------------------------------------------------------------------
//                     Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    TileGeometry::TileGeometry(ConstHeightFieldPtr pElevations)
        : GEO::Geometry()
    {
        ASSERT(pElevations, kHeigtfieldMustExist)

        setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eTriangles));
        setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal));

        linkTriangleList(gTriangles);

        // Construit les vertex.
        buildFirstTime(pElevations);
        setVertexCount(U16(vertices3().size()));
    }


    /*! Destructeur.
     */
    TileGeometry::~TileGeometry() { }


    /*! Mise � jour de la g�om�trie.
     */
    void TileGeometry::update(ConstHeightFieldPtr pElevations)
    {
        Vector<GEO::Vertex3>&   lV3             = vertices3();

        F32                     lBaseElevation  = F32(pElevations->reference());
        U16                     lCurrentIndex   = k0UW;

        // Les vertex appartenant aux autres tuiles ne sont pas consid�r�s pour le moment.
        for(I8 lLine = k0B; lLine < HeightField::eLength; ++lLine)
        {
            for(I8 lColumn = k0B; lColumn < HeightField::eLength; ++lColumn, ++lCurrentIndex)
            {
                // Seule l'�l�vation peut avoir besoin d'�tre mise � jour (pour les coordonn�es de vertex).
                lV3[lCurrentIndex][2]            = lBaseElevation + kVerticalStep * F32((*pElevations)[lLine][lColumn]);
            }
        }

        // Enfin, calcule les normales.
        computeNormals();
    }


//------------------------------------------------------------------------------
//                             M�thodes Assistantes
//------------------------------------------------------------------------------

    /*! Recalcule toutes les normales.
        @todo A finir
     */
    void TileGeometry::computeNormals()
    {
        // Premi�re phase : utilise la liste de triangles pour calculer les normales.
        const Vector<GEO::Vertex3>&     lVertices       = vertices3();
        Vector<GEO::Normal>&            lNormals        = normals();
        const Vector<GEO::Triangle>&    lTriangles      = triangleList(k1F);
        const Vector<GEO::Triangle>&    lBorders        = *gTrianglesOnBorders;
        U16                             lVertexCount    = U16(lVertices.size());

        // Algorithme :
        // 1�re phase : Calcule les normales de tous les triangles,
        // et note pour chaque vertex, � quels triangles il appartient.
        // 2�me phase : Calcule les normales des triangles sur les bordures
        // appartenant aux tuiles voisines (les vertex sont mis au courant).
        // 3�me phase : Pour chaque vertex, somme des normales puis normalisation d'icelle.
        Vector<MATH::V3>    lTriangleNormals(lTriangles.size() + lBorders.size(), MATH::kNullV3);
        Vector<Vector<U16>> lVertexTriangles(lVertexCount);
        // D�finir la capacit� ici permet de doubler les performances (test� et approuv� !).
        std::for_each(lVertexTriangles.begin(),
                      lVertexTriangles.end(),
                      std::bind2nd(std::mem_fun_ref(&Vector<U16>::reserve), 8UL));

        auto computeNormals = [&lVertices, &lNormals, &lTriangleNormals, &lVertexTriangles]
                              (const Vector<GEO::Triangle>& pTriangles, U16 pTrOffset)
        {
            U16 lTriangleCount  = U16(pTriangles.size());

            for(U16 lTriangle = k0UW; lTriangle < lTriangleCount; ++lTriangle)
            {
                U16                 lV0 = pTriangles[lTriangle][0];
                const GEO::Vertex3& l0  = lVertices[lV0];
                U16                 lV1 = pTriangles[lTriangle][1];
                const GEO::Vertex3& l1  = lVertices[lV1];
                U16                 lV2 = pTriangles[lTriangle][2];
                const GEO::Vertex3& l2  = lVertices[lV2];

                MATH::V3    l01(l1[0] - l0[0], l1[1] - l0[1], l1[2] - l0[2]);
                MATH::V3    l02(l2[0] - l0[0], l2[1] - l0[1], l2[2] - l0[2]);

                MATH::V3    lNormal  = l01 ^ l02;
                U16         lTrIndex = pTrOffset + lTriangle;

                lTriangleNormals[lTrIndex] = lNormal.normalized();
                lVertexTriangles[lV0].push_back(lTrIndex);
                lVertexTriangles[lV1].push_back(lTrIndex);
                lVertexTriangles[lV2].push_back(lTrIndex);
            }

            return pTrOffset + U16(pTriangles.size());
        };

        U16 lTrOffset = computeNormals(triangleList(k1F), k0UW);        // 1ere pahse, triangles visible de la tuile.
                        computeNormals(lBorders,          lTrOffset);   // 2e phase bordures (triangles "partag�s").

        for(U16 lV = k0UW; lV < lVertexCount; ++lV)
        {
            MATH::V3    lNormal = MATH::kNullV3;
            U16         lTCount = U16(lVertexTriangles[lV].size());

            for(U16 lT = k0UW; lT < lTCount; ++lT)
            {
                lNormal += lTriangleNormals[lVertexTriangles[lV][lT]];
            }

            if (lTCount != 0)
            {
                lNormal.normalize();
                lNormals[lV][0] = lNormal[0];
                lNormals[lV][1] = lNormal[1];
                lNormals[lV][2] = lNormal[2];
            }
        }
    }


//------------------------------------------------------------------------------
//                           "Permanence" des donn�es
//------------------------------------------------------------------------------

    /*! Ne fait rien : une instance de cette classe est toujours cr��e dynamiquement (aucune sauvegarde).<br>
        Les donn�es sont "export�es" via un "height field" (@sa HeightField).
     */
    void TileGeometry::store(PAK::Saver&) const { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Construction de la g�om�trie.
     */
    void TileGeometry::buildFirstTime(ConstHeightFieldPtr pElevations)
    {
        F32         lBaseElevation  = F32(pElevations->reference());
        MATH::V2    lTopLeftCorner  = MATH::V2(F32(-0.5F * (HeightField::eLength - 1)),
                                               F32( 0.5F * (HeightField::eLength - 1)));
        F32         lInvEdgeSizeL   = F32(1.0F / (HeightField::eLength - 3));
        F32         lInvEdgeSizeW   = F32(1.0F / (HeightField::eLength - 3));

        Vector<GEO::Vertex3>&   lV3 = vertices3();
        Vector<GEO::Normal>&    lN3 = normals();

        F32 lDefaultNormal[] = { k0F, k0F, k1F };
        lN3.resize((HeightField::eLength + 1) * (HeightField::eLength + 1), lDefaultNormal);

        // Les vertex appartenant aux autres tuiles ne sont pas consid�r�s pour le moment.
        for(I8 lLine = k0B; lLine < HeightField::eLength; ++lLine)
        {
            for(I8 lColumn = k0B; lColumn < HeightField::eLength; ++lColumn)
            {
                GEO::Vertex3   lCurrentVertex;
                // X.
                lCurrentVertex[0] = lTopLeftCorner[0] + F32(lColumn);
                // Y.
                lCurrentVertex[1] = lTopLeftCorner[1] - F32(lLine);
                // Z.
                lCurrentVertex[2] = lBaseElevation + kVerticalStep * F32((*pElevations)[lLine][lColumn]);

                lV3.push_back(lCurrentVertex);
            }
        }

        // Enfin, calcule les normales.
        computeNormals();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Construit la liste des triangles part�g�s entre tuiles (sur les bordures).
     */
    static void buildTriangleListOnBorders()
    {   // 4 brods, 2 triangles pour un carr�, +1 pour compl�ter un bord.
        U16 lTriangleCount = U16(4 * 2 * (eEdgeSize + 1) * eEdgeSize);
        gTrianglesOnBorders = std::make_unique<Vector<GEO::Triangle>>();
        gTrianglesOnBorders->reserve(lTriangleCount);
        // NNNE
        // W  E
        // W  E
        // WSSS
        U16 lWidth  = U16(HeightField::eLength);
        for(U16 lS = U16(0); lS <= U16(eEdgeSize); ++lS)
        {
            {   // N
                U16 l0( 0        * lWidth + lS);        // 0--1
                U16 l1( 0        * lWidth + lS + 1);    // |\ |
                U16 l2((0   + 1) * lWidth + lS);        // | \|
                U16 l3((0   + 1) * lWidth + lS + 1);    // 2--3

                gTrianglesOnBorders->emplace_back(l2, l3, l0);
                gTrianglesOnBorders->emplace_back(l3, l1, l0);
            }
            {   // E
                U16 l0( (lS + 0) * lWidth + lWidth - 2);
                U16 l1( (lS + 0) * lWidth + lWidth - 1);
                U16 l2( (lS + 1) * lWidth + lWidth - 2);
                U16 l3( (lS + 1) * lWidth + lWidth - 1);

                gTrianglesOnBorders->emplace_back(l2, l3, l0);
                gTrianglesOnBorders->emplace_back(l3, l1, l0);
            }
            {   // S
                U16 l0((lWidth - 2) * lWidth + lWidth - lS - 2);
                U16 l1((lWidth - 2) * lWidth + lWidth - lS - 1);
                U16 l2((lWidth - 1) * lWidth + lWidth - lS - 2);
                U16 l3((lWidth - 1) * lWidth + lWidth - lS - 1);

                gTrianglesOnBorders->emplace_back(l2, l3, l0);
                gTrianglesOnBorders->emplace_back(l3, l1, l0);
            }
            {   // W
                U16 l0((lWidth - lS - 2) * lWidth);
                U16 l1((lWidth - lS - 2) * lWidth + 1);
                U16 l2((lWidth - lS - 1) * lWidth);
                U16 l3((lWidth - lS - 1) * lWidth + 1);

                gTrianglesOnBorders->emplace_back(l2, l3, l0);
                gTrianglesOnBorders->emplace_back(l3, l1, l0);
            }
        }
    }


    /*! Construit la liste de triangles type pour toutes les tuiles. La possibilit� de partager certaines
        donn�es entre plusieurs g�om�tries est ici pleinement exploit�e.
     */
    void TileGeometry::buildTriangleList()
    {
        U16 lTriangleCount = U16(2 * eEdgeSize * eEdgeSize);
        gTriangles = STL::makeShared<GEO::TriangleList>(lTriangleCount);

        U16 lWidth  = U16(HeightField::eLength);

        for(U16 lLi = U16(1); lLi <= U16(eEdgeSize); ++lLi)
            for(U16 lCo = U16(1); lCo <= U16(eEdgeSize); ++lCo)
            {
                U16 l0( lLi      * lWidth +     lCo);    // 0--1
                U16 l1( lLi      * lWidth + 1 + lCo);    // |\ |
                U16 l2((lLi + 1) * lWidth +     lCo);    // | \|
                U16 l3((lLi + 1) * lWidth + 1 + lCo);    // 2--3
                // Cet ordre doit �tre maintenu, afin de garantir la bonne application de la megatexture
                // (il faut terminer les *2* triangles par le coin sup�rieur gauche).
                // Ces triangles doivent �tre les m�mes que ceux des pinceaux de tuiles.
                gTriangles->push(GEO::Triangle(l2, l3, l0));
                gTriangles->push(GEO::Triangle(l3, l1, l0));
            }

        buildTriangleListOnBorders();
    }


    /*! D�truit la liste de triangles type pour les tuiles.
     */
    void TileGeometry::destroyTriangleList()
    {
        gTrianglesOnBorders = nullptr;
        gTriangles = nullptr;
    }


//------------------------------------------------------------------------------
//                 Tile Geometry FX : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    TileGeometryFX::TileGeometryFX(const CLR::RGBA& pColor)
        : GEO::Geometry()
    {
        // Champs : couleurs (port�es) et triangles (li�s).
        setFlags(I32(GEO::eRGBA | GEO::eTriangles));
        setAutoFlags(I32(GEO::eRGBA));

        Vector<GEO::RGBA>&  lRGBA = colors4();
        lRGBA.reserve((HeightField::eLength + 1) * (HeightField::eLength + 1));

        for(I8 lLine = k0B; lLine < HeightField::eLength; ++lLine)
        {
            for(I8 lColumn = k0B; lColumn < HeightField::eLength; ++lColumn)
            {
                lRGBA.push_back(pColor);
            }
        }
        setVertexCount(U16(lRGBA.size()));
        linkTriangleList(gTriangles);
    }


    /*! Destructeur.
     */
    TileGeometryFX::~TileGeometryFX() { }


//------------------------------------------------------------------------------
//                   Tile Geometry FX : Application de l'Effet
//------------------------------------------------------------------------------

    /*! D�finit la nouvelle couleur de la g�om�trie.
        @param pNewColor Nouvelle couleur pour la g�om�trie de tuile
     */
    void TileGeometryFX::setColor(const CLR::RGBA& pNewColor)
    {
        Vector<GEO::RGBA>&  lRGBA = colors4();

        if (lRGBA.front() != pNewColor)
        {
            for(auto lIter = lRGBA.begin(); lIter != lRGBA.end(); ++lIter)
            {
                *lIter = pNewColor;
            }
        }
    }
}
