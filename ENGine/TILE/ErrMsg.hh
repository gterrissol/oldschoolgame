/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TILE_ERRMSG_HH
#define TILE_ERRMSG_HH

/*! @file ENGine/TILE/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module TILE.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TILE
{
#ifdef ASSERTIONS

    //! @name Message d'assertion
    //@{
    extern  ERR::String kColorAlreadyUsed;
    extern  ERR::String kHeigtfieldMustExist;
    extern  ERR::String kInvalidColorIndex;
    extern  ERR::String kInvalidElementIndex;
    extern  ERR::String kInvalidFamilyIndex;
    extern  ERR::String kInvalidFamilyName;
    extern  ERR::String kInvalidFragmentIndex;
    extern  ERR::String kInvalidGameObject;
    extern  ERR::String kInvalidMaskIndex;
    extern  ERR::String kInvalidResolution;
    extern  ERR::String kInvalidRowIndex;
    extern  ERR::String kTileEncodingTooLong;
    extern  ERR::String kTooManyFamilies;
    extern  ERR::String kTooManyFragments;
    extern  ERR::String kTooManyMasks;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De TILE_ERRMSG_HH
