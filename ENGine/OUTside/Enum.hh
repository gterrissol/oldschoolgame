/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UT_ENUM_HH
#define UT_ENUM_HH

/*! @file ENGine/OUTside/Enum.hh
    @brief Enum�rations et constantes du module OUT.
    @author @ref Guillaume_Terrissol
    @date 26 Novembre 2008 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OUT
{
    enum
    {
        eMaxTileCount   = 256
    };
}

#endif // De UT_ENUM_HH
