/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UT_ALL_HH
#define UT_ALL_HH

/*! @file ENGine/OUTside/All.hh
    @brief Interface publique du module @ref OUTside.
    @author @ref Guillaume_Terrissol
    @date 26 Novembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OUT   //! Monde ext�rieur.
{
    /*! @namespace OUT
        @version 0.3
     */

    /*! @defgroup OUTside OUTside : Monde ext�rieur (surface).
        <b>namespace</b> OUT.
     */

}

#include "World.hh"

#endif  // De UT_ALL_HH
