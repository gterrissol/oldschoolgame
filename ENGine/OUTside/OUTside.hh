/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UT_OUTSIDE_HH
#define UT_OUTSIDE_HH

/*! @file ENGine/OUTside/OUTside.hh
    @brief Pr�-d�clarations du module @ref OUTside.
    @author @ref Guillaume_Terrissol
    @date 26 Novembre 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OUT
{
    class Controller;
    class World;
}

#endif  // De UT_OUTSIDE_HH
