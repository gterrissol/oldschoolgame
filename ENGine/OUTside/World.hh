/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OUT_WORLD_HH
#define OUT_WORLD_HH

#include "OUTside.hh"

/*! @file ENGine/OUTside/World.hh
    @brief En-t�te de la classe OUT::World.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "TILE/BaseTile.hh"
#include "WORld/BaseWorld.hh"

#include "Enum.hh"

namespace OUT
{
    /*! @brief Monde ext�rieur
        @version 0.3
     */
    class World : public WOR::World
    {
        //! @name Classe amie
        //@{
 friend class Controller;                                       //!< Pour les m�thodes d'�dition.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        using TileRscPtr    = SharedPtr<TILE::TileRsc>;         //!< Pointeur sur ressource tuile.
        using TilePtr       = SharedPtr<TILE::Tile>;            //!< Pointeur sur tuile.
        using TileMgrPtr    = TILE::TileMgr::Ptr;               //!< Pointeur sur gestionnaire de tuiles.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit    World(PAK::Loader& pLoader);                //!< Constructeur.
virtual             ~World();                                   //!< Destructeur.
        //@}
        //! @name Informations
        //@{
        U32         width() const;                              //!< Largeur du monde (est - ouest), en tuiles.
        U32         height() const;                             //!< Hauteur du monde (nord - sud), en tuiles.
        F32         height(const MATH::V2& pPosition) const;    //!< Altitude du monde.
        TileMgrPtr  tileMgr() const;                            //!< Gestionnaire de tuiles.
        //@}

    protected:
        //! @name "Permanence" des donn�es
        //@{
virtual void        store(PAK::Saver& pSaver) const;            //!< Sauvegarde des donn�es.
        //@}
    private:
        //! @name M�thodes d'�dition
        //@{
        void        setArea(const UTI::Rect& pArea);            //!< Zone du monde (coordonn�es globales).
        void        setTileKey(U16 pX, U16 pY, TILE::Key pKey); //!< D�finition des tuiles.
        //@}
        //! @name Gestion du rendu
        //@{
virtual PlacePtr    first(ConstCameraPtr pCamera);              //!< Premier lieu visible.
virtual PlacePtr    next();                                     //!< Lieu visible suivant.
virtual PlacePtr    current();                                  //!< Lieu visible actuel.
        //@}
virtual WOR::EType  getWorldType() const;                       //!< Type du monde.
virtual UTI::Rect   getArea() const;                            //!< Zone occup�e par le monde.
        FORBID_COPY(World)
        PIMPL()
    };
}

#endif  // De OUT_WORLD_HH
