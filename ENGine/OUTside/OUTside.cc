    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "OUTside.hh"

/*! @file ENGine/OUTside/OUTside.cc
    @brief D�finitions diverses du module @ref OUTside.
    @author @ref Guillaume_Terrissol
    @date 26 Novembre 2008 - 28 Novembre 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OUT
{
}
