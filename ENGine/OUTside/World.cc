/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "World.hh"

/*! @file ENGine/OUTside/World.cc
    @brief M�thodes (non-inline) de la classe OUT::World.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2003 - 31 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "CAMera/BaseCamera.hh"
#include "MATH/V2.hh"
#include "PAcKage/LoaderSaver.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"
#include "UTIlity/Rect.hh"

namespace OUT
{
//------------------------------------------------------------------------------
//                                P-Impl de World
//------------------------------------------------------------------------------

    /*! @brief P-Impl de OUT::World.
        @version 0.4
     */
    class World::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                                Private(World* pThat, PAK::Loader& pLoader);    //!< Constructeur.
                                ~Private();                                     //!< Destructeur.
        //@}
        //! @name Parcours des lieux � rendre
        //@{
        const Vector<TilePtr>&  visibleTiles(ConstCameraPtr pCamera);   //!< 
        const Vector<TilePtr>&  visibleTiles() const;                   //!< 
        //@}
        typedef std::pair<TILE::Key, TileRscPtr>    ReferencedTile;     //!< Couple { clef, tuile }.
        typedef Vector<ReferencedTile>              TileRow;            //!< "Ligne" de tuiles.
        typedef Vector<TileRow>                     TileArea;           //!< Zone tuil�e.
        World*                          pthat;                          //!< Monde.
        TILE::TileMgr::Ptr              mTileMgr;                       //!< Tuiles.
        TileArea                        mTiles;                         //!< Surface du monde.
        mutable Vector<TilePtr>         mVisibleTiles;                  //!< Tuiles visibles.
        Vector<TilePtr>::const_iterator mCurrentTile;                   //!< Tuile � rendre.
        UTI::Rect                       mArea;                          //!< Zone couverte par le monde.
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::Private::Private(World* pThat, PAK::Loader& pLoader)
        : pthat(pThat)
        , mTiles()
        , mVisibleTiles()
        , mCurrentTile()
    {
        PAK::FileHdl    lHdl;
        pLoader >> lHdl;
        PAK::Loader lTileData{lHdl};
        mTileMgr = STL::makeShared<TILE::TileMgr>();
        mTileMgr->load(lTileData);

        // Nombre de tuiles : hauteur.
        U32 lTileRowCount    = k0UL;
        pLoader >> lTileRowCount;

        // Nombre de tuiles : largeur.
        U32 lTileColumnCount = k0UL;
        pLoader >> lTileColumnCount;

        // Zone.
        pLoader >> mArea;

        // Cr�e les tuiles.
        mTiles.resize(lTileRowCount);

        for(auto lRowIter = mTiles.begin(); lRowIter != mTiles.end(); ++lRowIter)
        {
            TileRow&    lTileRow    = *lRowIter;

            lTileRow.resize(lTileColumnCount);

            for(auto lTileIter = lTileRow.begin(); lTileIter != lTileRow.end(); ++lTileIter)
            {
                TILE::Key  lTileKey;
                pLoader >> lTileKey;

                *lTileIter = std::make_pair(lTileKey, nullptr);
            }
        }
    }


    /*! Destructeur.
     */
    World::Private::~Private()
    {
        for(auto lRowIter = mTiles.begin(); lRowIter != mTiles.end(); ++lRowIter)
        {
            TileRow&    lTileRow    = *lRowIter;

            for(auto lTileIter = lTileRow.begin(); lTileIter != lTileRow.end(); ++lTileIter)
            {
                mTileMgr->release((*lTileIter).second);
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @todo Lier avec la cam�ra diff�rement ?
     */
    const Vector<World::TilePtr>& World::Private::visibleTiles(ConstCameraPtr pCamera)
    {
        // D�termine les tuiles visible en fonction de la cam�ra.
        mVisibleTiles.clear();

        I16         lHeight = I16(mTiles.size());
        I16         lWidth  = I16(mTiles.back().size());

        UTI::Pointf lTarget = pCamera->visibleArea().center();
        I16             lX      = I16((lTarget.x   - mArea.left()) / TILE::eEdgeSize - 2);
        if (lX < 0) lX = k0W;
        if (lWidth <= lX) lX = I16(lWidth - 1);
        I16             lY      = I16((mArea.top() - lTarget.y)    / TILE::eEdgeSize - 2);
        if (lY < 0) lY = k0W;
        if (lHeight <= lY) lY = I16(lHeight - 1);

        for(I16 lLi = k0W; lLi < lHeight; ++lLi)
        {
            if ((lY <= lLi) && (lLi < lY + 4))
            {
                for(I16 lCo = k0W; lCo < lWidth; ++lCo)
                {
                    if ((lX <= lCo) && (lCo < lX + 4))
                    {
                        ReferencedTile& lRefTile = mTiles[lLi][lCo];
                        if (!lRefTile.first.isNull())
                        {
                            if (lRefTile.second == nullptr)
                            {
                                lRefTile.second = mTileMgr->retrieve(lRefTile.first);
                            }
                            mVisibleTiles.push_back(lRefTile.second);
                        }
                        else
                        {
                            lRefTile.second = nullptr;
                        }
                    }
                    else
                    {
                        mTileMgr->release(mTiles[lLi][lCo].second);
                    }
                }
            }
            else
            {
                for(I16 lCo = k0W; lCo < lWidth; ++lCo)
                {
                    mTileMgr->release(mTiles[lLi][lCo].second);
                }
            }
        }
        //...

        return mVisibleTiles;
    }


    /*!
     */
    const Vector<World::TilePtr>& World::Private::visibleTiles() const
    {
        return mVisibleTiles;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::World(PAK::Loader& pLoader)
        : WOR::World(pLoader)
        , pthis(this, pLoader)
    { }


    /*! Destructeur.
     */
    World::~World()
    {
        saveSelf();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    U32 World::width() const
    {
        if (height() != 0)
        {
            return U32(pthis->mTiles.back().size());
        }
        else
        {
            return k0UL;
        }
    }


    /*!
     */
    U32 World::height() const
    {
        return U32(pthis->mTiles.size());
    }


    /*! @note Temporaire : mise au point de la d�mo
     */
    F32 World::height(const MATH::V2& pPosition) const
    {
        I16             lHeight = I16(pthis->mTiles.size());
        I16             lWidth  = I16(pthis->mTiles.back().size());

        I16             lX      = I16(floorf((pPosition.x - pthis->mArea.left()) / TILE::eEdgeSize));
        if (lX < 0) return k0F;
        if (lWidth <= lX) return k0F;
        I16             lY      = I16(floorf((pthis->mArea.top() - pPosition.y)  / TILE::eEdgeSize));
        if (lY < 0) return k0F;
        if (lHeight <= lY) return k0F;

        Private::ReferencedTile&    lRefTile = pthis->mTiles[lY][lX];

        if (lRefTile.second == nullptr)
        {
            return k0F;
        }

        return lRefTile.second->height(pPosition);
    }


    /*!
     */
    World::TileMgrPtr World::tileMgr() const
    {
        return pthis->mTileMgr;
    }


//------------------------------------------------------------------------------
//                               Gestion du Rendu
//------------------------------------------------------------------------------
#if 0
    /*!
     */
    void World::go()
    {
        static F32  sAlpha = 0.0F;
        const F32   kRay = 12.0F;
        mLight->setPosition(MATH::V4(kRay * cosf(sAlpha), kRay * sinf(sAlpha), F32(8.0F), k0F));
        sAlpha += F32((TimeMgr->delta() * 0.05F) / 60000.0F);

        pthis->mTileMgr->deliverResources();

        // Changement d'�clairage (c'est juste pour tester).
        for(U32 lT = k0UL; lT < pthis->mTiles.size(); ++lT)
        {
            TileRscPtr lTile = pthis->mTiles[lT];

            lTile->lock();
                APP::Appearance*   lSurface = lTile->surface();
                if (lSurface != AppearanceMgr->default())
                {
                    lSurface->ungetLit(mLight);
                }
            lTile->unlock();
        }

        // Eclaire les tuiles.
        for(U32 lT = 0; lT < pthis->mTiles.size(); ++lT)
        {
            TileRscPtr lTile = pthis->mTiles[lT];

            lTile->lock();
                APP::Appearance*   lSurface = lTile->surface();
                if (lSurface != AppearanceMgr->default())
                {
                    lSurface->getLit(mLight);
                }
            lTile->unlock();
        }
    }
#endif  // 0

//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
        
     */
    void World::store(PAK::Saver& pSaver) const
    {
        WOR::World::store(pSaver);

        pSaver << pthis->mTileMgr->fileHdl();

        // Nombre de tuiles : hauteur.
        U32 lTileRowCount    = height();
        pSaver << lTileRowCount;
        // Nombre de tuiles : largeur.
        U32 lTileColumnCount = width();
        pSaver << lTileColumnCount;

        pSaver << pthis->mArea;

        // Et clefs de ces derni�res.
        for(U32 lRow = k0UL; lRow < lTileRowCount; ++lRow)
        {
            const Private::TileRow& lTileRow = pthis->mTiles[lRow];

            for(U32 lCol = k0UL; lCol < lTileColumnCount; ++lCol)
            {
                TILE::Key   lTileKey    = lTileRow[lCol].first;

                pSaver << lTileKey;
            }
        }
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    void World::setArea(const UTI::Rect& pArea)
    {
        pthis->mArea = pArea;

        U32 lRowCount   = U32(pthis->mArea.roundedHeight() / TILE::eEdgeSize);
        U32 lColCount   = U32(pthis->mArea.roundedWidth()  / TILE::eEdgeSize);

        pthis->mTiles.resize(lRowCount);
        for(auto lRowIter = pthis->mTiles.begin(); lRowIter != pthis->mTiles.end(); ++lRowIter)
        {
            Private::TileRow&   lTileRow = *lRowIter;

            lTileRow.resize(lColCount);

            for(auto lTileIter = lTileRow.begin(); lTileIter != lTileRow.end(); ++lTileIter)
            {
                *lTileIter = std::make_pair(TILE::Key(), nullptr);
            }
        }
    }


    /*! @param pX   Abscisse de la tuile (0 est � gauche)
        @param pY   Ordonn�e de la tuile (0 est en "haut")
        @param pKey clef de la tuile � la position <i>pX</i>, <i>pY</i>
     */
    void World::setTileKey(U16 pX, U16 pY, TILE::Key pKey)
    {
        if ((pX < width()) && (pY < height()))
        {
            pthis->mTiles[pY][pX].first = pKey;
        }
    }


    /*!
     * /
    void World::setTileData(U32 pWidth, U32 pHeight, const PAK::FileHdl* pTileHdls)
    {
        pthis->mTiles.resize(pHeight);
        for(auto lRowIter = pthis->mTiles.begin(); lRowIter != pthis->mTiles.end(); ++lRowIter)
        {
            Private::TileRow& lTileRow    = *lRowIter;

            lTileRow.resize(pWidth);

            for(auto lTileIter = lTileRow.begin(); lTileIter != lTileRow.end(); ++lTileIter, ++pTileHdls)
            {
                PAK::FileHdl   lTileFileHdl    = *pTileHdls;
                TILE::Key      lTileKey        = mTileMgr->key(lTileFileHdl);

                *lTileIter = std::make_pair(lTileKey, STL::makeShared<TileRsc>());
            }
        }
    }*/


//------------------------------------------------------------------------------
//                          Parcours des Lieux � Rendre
//------------------------------------------------------------------------------

    /*! Etablit la liste des apparences visibles et retourne la premi�re d'entre elles.
        @param pCamera Camera d�finissant le cadre de rendu du monde
        @return La premi�re apparence visible, ou nullptr s'il n'y en a pas
     */
    World::PlacePtr World::first(ConstCameraPtr pCamera)
    {
pthis->mTileMgr->deliverResources();    // En attendant de r�activer go().

        pthis->mCurrentTile = pthis->visibleTiles(pCamera).begin();

        if (pthis->mCurrentTile != pthis->visibleTiles().end())
        {
            return *pthis->mCurrentTile;
        }
        else
        {
            return nullptr;
        }
    }


    /*! 
     */
    World::PlacePtr World::next()
    {
        if (current() != nullptr)
        {
            ++pthis->mCurrentTile;

            if (pthis->mCurrentTile != pthis->visibleTiles().end())
            {
                return *pthis->mCurrentTile;
            }
        }

        return nullptr;
    }


    /*!
     */
    World::PlacePtr World::current()
    {
        if (pthis->mCurrentTile != pthis->visibleTiles().end())
        {
            return *pthis->mCurrentTile;
        }
        else
        {
            return nullptr;
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    WOR::EType World::getWorldType() const
    {
        return WOR::eOutside;
    }


    /*!
     */
    UTI::Rect World::getArea() const
    {
        return pthis->mArea;
    }
}
