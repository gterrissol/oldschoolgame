/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Camera.hh"

/*! @file ENGine/OBJect/Camera.cc
    @brief M�thodes (non-inline) de la classe OBJ::Camera.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 26 Juin 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OBJ
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Camera::Camera(CAM::Camera::Ptr pCamera)
        : GameObject()
        , mBaseCamera(pCamera)
    { }


    /*! Destructeur.
     */    
    Camera::~Camera() { }
}
