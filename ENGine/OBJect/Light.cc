/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Light.hh"

/*! @file ENGine/OBJect/Light.cc
    @brief M�thodes (non-inline) de la classe OBJ::Light.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OBJ
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Light::Light()
        : GameObject()
    { }//BonePtr pParent) : Bone(Parent) { }


    /*! Destructeur.
     */    
    Light::~Light() { }
}
