/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameObject.hh"

/*! @file ENGine/OBJect/GameObject.cc
    @brief M�thodes (non-inline) de la classe OBJ::GameObject.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2002 - 4 Juillet 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <typeinfo>

#include "APPearance/Primitive.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "PAcKage/Handle.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/Function.hh"
#include "STL/Map.hh"
#include "STL/Shared.tcc"
#include "STL/SharedPtr.hh"

namespace OBJ
{
//------------------------------------------------------------------------------
//                                  Game Entity
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    GameEntity::GameEntity() { }


    /*! Destructeur.
     */
    GameEntity::~GameEntity() { }


    /*!
     */
    U32 GameEntity::getType() const
    {
        return U32(1);
    }


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de OBJ::GameObject.
        @version 0.2

     */
    class GameObject::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
        Private();
        //@}
        //! @name Attributs "visuels".
        //@{
        //Vector(ChildBonePtr)   mVpo_Bones;      //!< Ensemble d'os.
        AppearancePtr   mAppearance;            //!< Apparence de l'instance.
        MATH::V3        mReferencePosition;     //!< Position initiale.
        MATH::Q         mReferenceOrientation;  //!< Orientation initiale.
        MATH::V3        mReferenceScale;        //!< Echelle initiale.
        //@}
        //! @name Donn�es de jeu.
        //@{
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    GameObject::Private::Private()
        : mAppearance(APP::Primitive::create(APP::eCube, F32(0.5F), CLR::RGB(F32(0.5F), F32(0.5F), F32(0.5F))))
        , mReferencePosition(MATH::kNullV3)
        , mReferenceOrientation(MATH::kNullQ)
        , mReferenceScale(k1F, k1F, k1F)
    { }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    GameObject::GameObject(PAK::Loader& pLoader)
        : BONE::SimpleObject(BonePtr())
        , pthis()
    {
        // Squelette.
        U32 lBoneCount(0);
        pLoader >> lBoneCount;
        /*mVpo_Bones.resize(ul_BoneCount);
        for(...) { }*/

        // Apparence.
        pthis->mAppearance = STL::makeShared<APP::Appearance>(std::move(pLoader));
        pthis->mAppearance->bind(shared_from_this());

        // Localisation initiale (par rapport au parent).
        pLoader >> pthis->mReferencePosition;
        pLoader >> pthis->mReferenceOrientation;
        pLoader >> pthis->mReferenceScale;

        setPosition(pthis->mReferencePosition);
        setOrientation(pthis->mReferenceOrientation);
        setScale(pthis->mReferenceScale);

        // Donn�es de jeu.
        // ...
    }


    /*! Destructeur.
     */    
    GameObject::~GameObject() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameObject::setAppearance(AppearancePtr pApp)
    {
        pthis->mAppearance = pApp;
        pthis->mAppearance->bind(shared_from_this());
    }


    /*! Apparence.
     */
    GameObject::AppearancePtr GameObject::appearance()
    {
        return pthis->mAppearance;
    }


    /*! Animation.
     */
    void GameObject::update()
    {
        /*if (!po_Parent().expired()) // && bNeedUpdate()
        {
            SharedPtr<BonePtr::element_type>  po_ParentBone = po_Parent().lock();

            setPosition(mv_ReferencePosition + po_ParentBone->rv_Position());
            setOrientation(mq_ReferenceOrientation + po_ParentBone->rq_Orientation());
            setScale(mv_ReferenceScale + po_ParentBone->rv_Scale());
        }*/
    }


//------------------------------------------------------------------------------
//                              Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    GameObject::GameObject()
        : BONE::SimpleObject(BonePtr())
        , pthis()
    {
        pthis->mAppearance->bind(shared_from_this());
        setPosition(pthis->mReferencePosition);
        setOrientation(pthis->mReferenceOrientation);
        setScale(pthis->mReferenceScale);
    }


//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void GameObject::store(PAK::Saver& pSaver) const
    {
        // Squelette.
        pSaver << k0UL;/*mVpo_Bones.size();
        for(...) { }*/

        // Apparence.
        pthis->mAppearance->store(pSaver);

        // Localisation initiale (par rapport au parent).
        pSaver << position();//pthis->mReferencePosition;
        pSaver << orientation();//pthis->mReferenceOrientation;
        pSaver << scale();//pthis->mReferenceScale;

        // Donn�es de jeu.
        // ...
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    MATH::V3 GameObject::referencePosition() const
    {
        return pthis->mReferencePosition;
    }


    /*!
     */
    MATH::Q GameObject::referenceOrientation() const
    {
        return pthis->mReferenceOrientation;
    }


    /*!
     */
    MATH::V3 GameObject::referenceScale() const
    {
        return pthis->mReferenceScale;
    }


    /*!
     */
    MATH::M4 GameObject::referenceFrame() const
    {
        return MATH::M4(MATH::M3(referenceOrientation()), referencePosition(), referenceScale());
    }


    /*!
     */
    void GameObject::setReferencePosition(MATH::V3 pP)
    {
        pthis->mReferencePosition = pP;
        setPosition(pP);
        pthis->mAppearance->setPosition(pP);
    }


    /*!
     */
    void GameObject::setReferenceOrientation(MATH::Q pO)
    {
        pthis->mReferenceOrientation = pO;
        setOrientation(pO);
        pthis->mAppearance->setOrientation(pO);
    }


    /*!
     */
    void GameObject::setReferenceScale(MATH::V3 pS)
    {
        pthis->mReferenceScale = pS;
        setScale(pS);
        pthis->mAppearance->setScale(pS);
    }
}
