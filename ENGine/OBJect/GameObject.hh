/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OBJ_GAMEOBJECT_HH
#define OBJ_GAMEOBJECT_HH

#include "OBJect.hh"

/*! @file ENGine/OBJect/GameObject.hh
    @brief En-t�te des classes OBJ::GameEntity & OBJ::GameObject.
    @author @ref Guillaume_Terrissol
    @date 3 Juillet 2001 - 19 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "BONE/SimpleObject.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/Shared.hh"
#include "STL/STL.hh"

namespace OBJ
{
//------------------------------------------------------------------------------
//                                  Game Entity
//------------------------------------------------------------------------------

    /*! @brief Classe de base pour tous les objets du moteur.
        @version 0.2

        Avant l'introduction de cette classe, les objets du moteur se divisait principalement en deux
        cat�gories :
        - les Game Objects (OBJ::GameObject) : purs objets de jeu (persos, objets, waypoints,...),
        - les ressources (tuiles, mondes, lieux,...).
        Ce sont des objets �ditables dans l'IDE. Afin de faciliter le travail, il fallait leur donner une
        base commune : c'est le r�le de cette classe, qui sera enrichie au fur et � mesure des besoins.
     */
    class GameEntity : public APP::Pickable
    {
    public:
        //! @name Constructeur & destructeur
        //@{
            GameEntity();       //!< Constructeur.
virtual     ~GameEntity();      //!< Destructeur.
        //@}

    private:

virtual U32 getType() const;    //!< Type d'objet "pickable".
    };


//------------------------------------------------------------------------------
//                                  Game Object
//------------------------------------------------------------------------------

    /*! @brief Game Object.
        @version 0.26

        Tous les objet utilis�s dans le jeu sont des instances de cette classe, ou de ses d�riv�es.
        @todo Les objets sont syst�matiquement cr��s par chargement : impl�menter une m�thode de fa�on �
        profiter des instances d�j� cr��s (en effectuant une simple duplication, par exemple) pour �viter
        de trop jouer avec les fichiers
     */
    class GameObject : public BONE::SimpleObject, public GameEntity
    {
    public:
        //! @name Classe amie
        //@{
 friend class Controller;                                       //!< Pour les m�thodes d'�dition.
        //@}
        //! @name Types de pointeur
        //@{
        typedef SharedPtr<APP::Appearance>  AppearancePtr;      //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        GameObject(PAK::Loader& pLoader);       //!< Constructeur.
virtual                 ~GameObject();                          //!< Destructeur.
        //@}
        void            setAppearance(AppearancePtr pApp);      //!< D�finition de l'apparence.
        AppearancePtr   appearance();                           //!< Apparence.
virtual void            update();                               //!< Animation.
        //! @name Autre constructeur
        //@{
                        GameObject();                           //!< Constructeur par d�faut.
        //@}
        //! @name "Permanence" des donn�es
        //@{
virtual void            store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //@}
        //! @name 
        //@{
        MATH::V3        referencePosition() const;              //!< 
        MATH::Q         referenceOrientation() const;           //!< 
        MATH::V3        referenceScale() const;                 //!< 
        MATH::M4        referenceFrame() const;                 //!< 


//    private:

        void            setReferencePosition(MATH::V3 pP);      //!< 
        void            setReferenceOrientation(MATH::Q pO);    //!< 
        void            setReferenceScale(MATH::V3 pS);         //!< 
        //@}
        FORBID_COPY(GameObject)
        PIMPL()
    };
}

#endif // De OBJ_GAMEOBJECT_HH
