/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OBJ_CAMERA_HH
#define OBJ_CAMERA_HH

#include "OBJect.hh"

/*! @file ENGine/OBJect/Camera.hh
    @brief En-t�te de la classe OBJ::Camera.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 26 Juin 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GameObject.hh"

#include "CAMera/BaseCamera.hh"

namespace OBJ
{
    /*! @brief Camera.
        @version 0.21
     */
    class Camera : public GameObject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        Camera(CAM::Camera::Ptr pCamera);   //!< Constructeur.
virtual ~Camera();                          //!< Destructeur.
        //@}
        inline CAM::Camera* get() const
        {
            return mBaseCamera.get();
        }

    private:

        CAM::Camera::Ptr    mBaseCamera;
    };
}

#endif  // De OBJ_CAMERA_HH
