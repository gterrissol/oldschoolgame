/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OBJ_ALL_HH
#define OBJ_ALL_HH

/*! @file ENGine/OBJect/All.hh
    @brief Interface publique du module @ref OBJect.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 22 Juillet 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace OBJ   //! Game Objects.
{
    /*! @namespace OBJ
        @version 0.15

     */

    /*! @defgroup OBJect OBJect : Objets de jeu
        <b>namespace</b> OBJ.
     */
}

#include "Camera.hh"
#include "Light.hh"
#include "GameObject.hh"

#endif // De OBJ_ALL_HH
