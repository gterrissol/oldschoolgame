/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Memory.hh"

/*! @file ENGine/OBJect/Memory.cc
    @brief M�thodes (non-inline) de la classe OBJ::Memory.
    @author @ref Guillaume_Terrissol
    @date 12 Avril 2008 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace OBJ
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de OBJ::Memory.
        @version 0.2

     */
    class Memory::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
                        Private();                          //!< Constructeur par d�faut.
        //@}
        //! @name Gestion de l'�criture
        //@{
        inline  void    prepareWrite(U16 pOffsetInBits);    //!< Activation du mode �criture
        inline  void    setBit(bool pBit);                  //!< Ecriture d'un bit.
        inline  void    setByte(U8 pByte);                  //!< Ecriture d'un octet.
        //@}
        //! @name Gestion de la lecture
        //@{
        inline  void    prepareRead(U16 pOffsetInBits);     //!< Activation du mode lecture.
        inline  bool    getBit();                           //!< Lecture d'un bit.
        inline  U8      getByte();                          //!< Lecture d'un octet.
        //@}
        //! @name Attributs
        //@{
        Vector<U8>      mData;                              //!< Donn�es.
        U16             mBlockStart;                        //!< D�but du bloc activ�.
        U16             mCurrent;                           //!< Position du curseur dans le bloc activ�.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Memory::Private::Private()
        : mData()
        , mBlockStart(0)
        , mCurrent(0)
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Active l'�criture d'un bloc de donn�es.
        @param pOffsetInBits Position du bloc � �crire
     */
    void Memory::Private::prepareWrite(U16 pOffsetInBits)
    {
        mBlockStart = pOffsetInBits;
    }


    /*!
     */
    void Memory::Private::setBit(bool /*pBit*/)
    {
        // A venir.
    }


    /*!
     */
    void Memory::Private::setByte(U8 /*pByte*/)
    {
        // A venir.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Active la lecture d'un bloc de donn�es.
        @param pOffsetInBits Position du bloc � lire
     */
    void Memory::Private::prepareRead(U16 /*pOffsetInBits*/)
    {
        // A venir.
    }


    /*!
     */
    bool Memory::Private::getBit()
    {
        // A venir.
        return false;
    }


    /*!
     */
    U8 Memory::Private::getByte()
    {
        // A venir.
        return k0UB;
    }


//------------------------------------------------------------------------------
//                          Block : Ecriture de Donn�es
//------------------------------------------------------------------------------

    /*! Active le mode �criture pour ce bloc (les m�thodes push() peuvent �tre appel�es).<br>
        Un curseur d'�criture est plac� en d�but du bloc allou�.
     */
    void Memory::Block::write()
    {
        Memory::get()->pthis->prepareWrite(mOffset);
    }


    /*! Ecrit un bit en m�moire, � l'emplacement actuel du curseur.
        @param pBit Valeur du bit � �crire
     */
    void Memory::Block::push(bool pBit)
    {
        Memory::get()->pthis->setBit(pBit);
    }


    /*! Ecrit un octet en m�moire, � l'emplacement actuel du curseur.
        @param pByte Valeur de l'octet � �crire
     */
    void Memory::Block::push(U8 pByte)
    {
        Memory::get()->pthis->setByte(pByte);
    }


//------------------------------------------------------------------------------
//                          Block : Lecture de Donn�es
//------------------------------------------------------------------------------

    /*! Active le mode lecture pour ce bloc (les m�thodes pop() peuvent �tre appel�es).<br>
        Un curseur de lecture est plac� en d�but du bloc allou�.
     */
    void Memory::Block::read()
    {
        Memory::get()->pthis->prepareRead(mOffset);
    }


    /*! Lit un bit en m�moire, � l'emplacement actuel du curseur.
        @param pBit Variable � laquelle assigner la valeur du bit lu
     */
    void Memory::Block::pop(bool& pBit)
    {
        pBit = Memory::get()->pthis->getBit();
    }


    /*! Lit un octet en m�moire, � l'emplacement actuel du curseur.
        @param pByte Variable � laquelle assigner la valeur de l'octet lu
     */
    void Memory::Block::pop(U8& pByte)
    {
        pByte = Memory::get()->pthis->getByte();
    }


//------------------------------------------------------------------------------
//                             Block : Autre M�thode
//------------------------------------------------------------------------------

    /*! @return VRAI si le bloc est valide (i.e. r�f�rence des donn�es de m�moire de jeu), FAUX sinon
     */
    bool Memory::Block::isValid() const
    {
        return (mSizeInBits != 0);
    }


//------------------------------------------------------------------------------
//                             Block : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Memory::Block::Block()
        : mOffset(0)
        , mSizeInBits(0)
    { }


    /*! Cr�ation d'un bloc.
     */
    Memory::Block::Block(U16 pOffset, U16 pSize)
        : mOffset(pOffset)
        , mSizeInBits(pSize)
    {
        ASSERT(mSizeInBits != 0, kInvalidSize)
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Memory::Memory()
        : pthis()
    { }


    /*! Destructeur.
     */
    Memory::~Memory() { }


//------------------------------------------------------------------------------
//                           Cr�ation d'une sauvegarde
//------------------------------------------------------------------------------

    /*! Pr�paration d'une sauvegarde.
     */
    void Memory::beginDump(bool /*pThreaded*/)
    {
        // A venir.
    }


    /* Sauvegarde pr�te ?
     */
    bool Memory::isDumpDone() const
    {
        // A venir.
        return false;
    }


    /*! R�cup�ration de la sauvegarde.
     */
    void Memory::getDump(Vector<U8>& /*pData*/)
    {
        // A venir.
    }


//------------------------------------------------------------------------------
//                          Chargement d'une Sauvegarde
//------------------------------------------------------------------------------

    /*! Chargement d'une sauvegarde.
     */
    void Memory::beginLoad(Vector<U8>& /*pData*/)
    {
        // A venir.
    }


    /*! Sauvegarde charg�e ?
     */
    bool Memory::isLoadDone() const
    {
        // A venir.
        return false;
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  Memory* MEM::Singleton<Memory>::smThat  = nullptr;
#endif
}
