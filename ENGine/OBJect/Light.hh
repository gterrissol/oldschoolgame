/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OBJ_LIGHT_HH
#define OBJ_LIGHT_HH

#include "OBJect.hh"

/*! @file ENGine/OBJect/Light.hh
    @brief En-t�te de la classe OBJ::Light.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GameObject.hh"

namespace OBJ
{
    /*! @brief Lumi�re (moteur).
        @version 0.1

        Cet objet permet de manipuler des lumi�res au niveau du moteur, par opposition aux LIG::Light qui sont des lumi�res g�r�es au
        niveau du moteur graphiques.
     */
    class Light : public GameObject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        Light(/*BonePtr pParent*/); //!< Constructeur.
virtual ~Light();                   //!< Destructeur.
        //@}
    };
}

#endif  // De OBJ_LIGHT_HH
