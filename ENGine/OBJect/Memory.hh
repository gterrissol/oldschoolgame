/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef OBJ_MEMORY_HH
#define OBJ_MEMORY_HH

#include "OBJect.hh"

/*! @file ENGine/OBJect/Memory.hh
    @brief En-t�te de la classe OBJ::GameObject.
    @author @ref Guillaume_Terrissol
    @date 12 Avril 2008 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "STL/Vector.hh"

namespace OBJ
{
//------------------------------------------------------------------------------
//                                  M�moire de Jeu
//------------------------------------------------------------------------------

    /*! @brief M�moire de jeu.
        @version 0.2

        Cette classe le mod�le m�moire utilis� pour conserver "l'�tat de jeu" de tous les objets de
        l'univers.
     */
    class Memory : public MEM::Singleton<Memory>, public MEM::OnHeap
    {
        class Private;
    public:
        //! @name Constructeur & destructeur
        //@{
                        Memory();                   //!< Constructeur par d�faut.
                        ~Memory();                  //!< Destructeur.
        //@}
        //! @name Cr�ation d'une sauvegarde
        //@{
        void    beginDump(bool pThreaded = true);   //!< Pr�paration d'une sauvegarde.
        bool    isDumpDone() const;                 //!< Sauvegarde pr�te ?
        void    getDump(Vector<U8>& pData);         //!< R�cup�ration de la sauvegarde.
        //@}
        //! @name Chargement d'une sauvegarde
        //@{
        void    beginLoad(Vector<U8>& pData);       //!< Chargement d'une sauvegarde.
        bool    isLoadDone() const;                 //!< Sauvegarde charg�e ?
        //@}
        //! @name Acc�s � la m�moire
        //@{

        /*! @brief Proxy vers des donn�es de m�moire.
            @version 0.2
         */
        class Block : public MEM::Auto<Block>
        {
        //! @name Classes amies
        //@{
 friend class Memory::Private;                      //!< Le gestionnaire conna�t sa ressource.
        //@}
        public:
            //! @name Ecriture de donn�es
            //@{
            void    write();                        //!< Activation du mode �criture.
            void    push(bool pBit);                //!< Ecriture d'un bit.
            void    push(U8 pByte);                 //!< Ecriture d'un octet.
            //@}
            //! @name Lecture de donn�es
            //@{
            void    read();                         //!< Activation du mode lecture.
            void    pop(bool& pBit);                //!< Lecture d'un bit.
            void    pop(U8& pByte);                 //!< Lecture d'un octet.
            //@}
            bool    isValid() const;                //!< Validit� du bloc.
            //! @name Constructeurs
            //@{
            Block();                                //!< Constructeur par d�faut.


        private:

            Block(U16 pOffset, U16 pSize);          //!< Cr�ation d'un bloc.
            //@}
            //! @name
            //@{
            U16 mOffset;                            //!< Position du bloc (en bits).
            U16 mSizeInBits;                        //!< Taille du bloc (en bits).
            //@}
        };


    protected:

        Block  reserve(U16 pSizeInBits);            //!< Cr�ation d'un bloc de m�moire.
        void    clear(Block pBlock);                //!< Suppression d'un bloc de m�moire.
        //@}

    private:

        FORBID_COPY(Memory)
        PIMPL()
    };
}

#endif // De OBJ_MEMORY_HH
