/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_INSIDE_HH
#define WOR_INSIDE_HH

#include "WORld.hh"

/*! @file ENGine/WORld/Inside.hh
    @brief En-t�te de la classe WOR::Inside.
    @author @ref Guillaume_Terrissol
    @date 22 Mai 2005 - 19 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/PAcKage.hh"
#include "PLaCes/PLaCes.hh"

#include "BaseWorld.hh"

namespace WOR
{
    /*! @brief Monde int�rieur.
        @version 0.2
     */
    class Inside : public World
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef SharedPtr<PLC::Hall>    HallPtr;            //!< Pointeur sur pi�ce.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        Inside(PAK::Loader& pLoader);       //!< Constructeur (chargement imm�diat).
virtual                 ~Inside();                          //!< Destructeur.
        //@}
    protected:
        //! @name Autres constructeurs
        //@{
                        Inside();                           //!< Constructeur par d�faut.
                        Inside(PAK::FileHdl pFileHdl);      //!< Constructeur (chargement diff�r�).
        //@}
        //! @name M�thodes d'�dition
        //@{

        //@}
        //! @name Gestion des donn�es
        //@{
virtual void            swap(Inside& pLoadedInside);        //!< Permutation des donn�es.
virtual void            store(PAK::Saver& pSaver) const;    //!< Sauvegarde des donn�es.
        //@}

    private:

virtual void            updateAfterDeliver();               //!< Mise � jour apr�s le chargement.
virtual Vector<HallPtr> entrances();                        //!< Entr�es (pi�ces li�es � un autre monde).

        FORBID_COPY(Inside)
        PIMPL()
    };
}

#endif  // De WOR_INSIDE_HH
