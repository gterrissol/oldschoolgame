/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Inside.hh"

/*! @file ENGine/WORld/Inside.cc
    @brief M�thodes (non-inline) de la classe WOR::Inside.
    @author @ref Guillaume_Terrissol
    @date 22 Mai 2005 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace WOR
{
//------------------------------------------------------------------------------
//                               P-Impl de Inside
//------------------------------------------------------------------------------

    /*! @brief P-Impl de WOR::Inside.
        @version 0.2
     */
    class Inside::Private : public MEM::OnHeap
    {
    public:

        typedef Inside::HallPtr HallPtr;    //!< 
        //! @name Constructeurs
        //@{
        Private();                          //!< Constructeur.
        //@}
        Vector<HallPtr> mHalls;             //!< Pi�ces du monde.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Inside::Private::Private()
        : mHalls()
    { }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Inside::Inside(PAK::Loader& pLoader)
        : World(pLoader)
        , pthis()
    { }


    /*! Destructeur.
     */
    Inside::~Inside() { }


//------------------------------------------------------------------------------
//                              Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Inside::Inside()
        : pthis()
    { }


    /*! Constructeur (chargement diff�r�).
     */
    Inside::Inside(PAK::FileHdl pFileHdl)
        : World(pFileHdl)
        , pthis()
    { }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
        @todo Demander le chargement de certaines pi�ces en priorit� (les entr�es) ?
     */
    void Inside::swap(Inside& /*pLoadedInside*/) { }


    /*! Sauvegarde des donn�es.
     */
    void Inside::store(PAK::Saver& /*pSaver*/) const { }


//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

    /*! Mise � jour apr�s le chargement.
     */
    void Inside::updateAfterDeliver()
    {
        // ...
    }


    /*! Entr�es (pi�ces li�es � un autre monde).
     */
    Vector<Inside::HallPtr> Inside::entrances()
    {
        return Vector<HallPtr>();
    }
}
