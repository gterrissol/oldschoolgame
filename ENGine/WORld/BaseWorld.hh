/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_BASEWORLD_HH
#define WOR_BASEWORLD_HH

#include "WORld.hh"

/*! @file ENGine/WORld/BaseWorld.hh
    @brief En-t�te de la classe WOR::World.
    @author @ref Guillaume_Terrissol
    @date 31 Janvier 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CAMera/CAMera.hh"
#include "MEMory/MEMory.hh"
#include "PAcKage/PAcKage.hh"
#include "PLaCes/PLaCes.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Vector.hh"
#include "UTIlity/UTIlity.hh"

#include "Enum.hh"
#include "Scene.hh"

namespace WOR
{
    /*! @brief Monde.
        @version 0.4

        @note Un ensemble de mondes sont li�s :
        - un ext�rieur (OUT::World),
        - un souterrain (UND::World),
        - un ensemble de b�timents (HSE::World) et de "dongeons" (DNG::World) plac�s sur les deux premiers.<br>
        Un univers peut poss�der plusieurs de ces ensembles.<br>
        Chacun de ces ensemble de monde doit pouvoir se r�f�rer � des donn�es communes (notamment des �l�vations),
        afin de g�rer correctement les intersections (pour les �viter, en fait).<br>
        Si ces donn�es seront essentiellement exploit�es en mode �diteur, un r�f�rencement commun �tait in�vitable
        en mode �diteur (voir commonData()).
     */
    class World : public Scene
    {
    public:
        class Group;
        //! @name Types de pointeur
        //@{
        using Ptr            = SharedPtr<World>;                //!< Pointeur sur monde.
        using ConstCameraPtr = SharedPtr<CAM::Camera const>;    //!< Pointeur (constant) sur cam�ra.
        using PlacePtr       = SharedPtr<PLC::Place>;           //!< Pointeur sur lieu.
        using LightPtr       = WeakPtr<LIT::Light>;             //!< Pointeur sur lumi�re.
        using GroupPtr       = SharedPtr<Group>;                //!< Pointeur sur groupe de mondes.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit        World(PAK::Loader& pLoader);            //!< Constructeur.
virtual                 ~World();                               //!< Destructeur.
        //@}
        //! @name Gestion du rendu
        //@{
        PlacePtr        firstPlace(ConstCameraPtr pCamera);     //!< Premier lieu visible.
        PlacePtr        nextPlace();                            //!< Lieu visible suivant.
        PlacePtr        currentPlace();                         //!< Lieu visible actuel.
virtual void            go();                                   //!< Animation (une frame).
        void            shine(LightPtr pSun);                   //!< D�finition du soleil.
        LightPtr        sun() const;                            //!< Soleil.
        //@}
        bool            isActive() const;                       //!< Monde actif ?
        EType           worldType() const;                      //!< Type de monde.
        UTI::Rect       area() const;                           //!< Zone occup�e par le monde.
        //@}
        //! @name Gestion des groupes
        //@{
        /*! @brief Groupe de mondes.
            @version 0.2
         */
        class Group : public MEM::OnHeap
        {
        public:
            //! @name Constructeurs
            //@{
            explicit                Group(PAK::Loader& pLoader);        //!< Chargement.
            //@}
            enum
            {
                eOutside        = 0,
                eUnderground    = 1,
                eInsides        = 2
            };
            PAK::FileHdl            fileHdl() const;
            U32                     count() const;
            Vector<PAK::FileHdl>    worldHdls() const;

            void                    store(PAK::Saver& pSaver) const;    //!< Sauvegarde des donn�es.

        protected:
            //! @name Edition
            //@{
            void                    insert(World::Ptr pWorld);          //!< Ajout d'un monde.
            void                    remove(World::Ptr pWorld);          //!< Retrait d'un monde.
            //@}

        private:

            PAK::FileHdl            mCommon;                            //!< Donn�es d'�l�vations et de textures.
            PAK::FileHdl            mOut;                               //!< Monde ext�rieur.
            PAK::FileHdl            mUnd;                               //!< Monde souterrain.
            Vector<PAK::FileHdl>    mIns;                               //!< Mondes int�rieurs.
        };
        
        void            bind(PAK::FileHdl pGroup);              //!< Assignation � un groupe.
        PAK::FileHdl    group() const;                          //!< Groupe de mondes.
        //@}
    protected:
        //! @name Autres constructeurs
        //@{
        explicit        World();                                //!< Constructeur par d�faut.
        explicit        World(PAK::FileHdl pFileHdl);           //!< Constructeur (chargement diff�r�).
        //@}
        //! @name Gestion des donn�es
        //@{
virtual void            swap(World& pLoaded);                   //!< Permutation des donn�es.
virtual void            store(PAK::Saver& pSaver) const = 0;    //!< Sauvegarde des donn�es.
        //@}
    private:
        //! @name Parcours des lieux � rendre
        //@{
virtual PlacePtr        first(ConstCameraPtr pCamera) = 0;      //!< Premier lieu visible.
virtual PlacePtr        next() = 0;                             //!< Lieu visible suivant.
virtual PlacePtr        current() = 0;                          //!< Lieu visible actuel.
        //@}
virtual EType           getWorldType() const = 0;               //!< Type de monde.
virtual UTI::Rect       getArea() const = 0;                    //!< Zone occup�e par le monde.
        FORBID_COPY(World)
        PIMPL()
    };


    /*! @brief Afficheur de monde.
        @version 0.3
     */
    class WorldRenderer : public RDR::Renderer
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef WeakPtr<World>  WorldPtr;           //!< Pointeur sur monde.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit    WorldRenderer(WorldPtr pWorld); //!< Constructeur.
virtual             ~WorldRenderer();               //!< Destructeur.
        //@}

    private:

                    WorldRenderer();                //!< Constructeur par d�faut.
        //! @name Rendu
        //@{
virtual void        dumpAppearances();              //!< Pr�paration du rendu.
        //@}

        FORBID_COPY(WorldRenderer)
        PIMPL()
    };
}

#endif  // De WOR_BASEWORLD_HH
