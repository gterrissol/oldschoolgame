/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_DUNGEON_HH
#define WOR_DUNGEON_HH

#include "WORld.hh"

/*! @file ENGine/WORld/Dungeon.hh
    @brief En-t�te de la classe WOR::Dungeon.
    @author @ref Guillaume_Terrissol
    @date 17 Mai 2005 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/PAcKage.hh"

#include "Inside.hh"

namespace WOR
{
    /*! @brief Labyrinthe.
        @version 0.22
     */
    class Dungeon : public Inside
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        explicit    Dungeon(PAK::Loader& pLoader);      //!< Constructeur (chargement imm�diat).
virtual             ~Dungeon();                         //!< Destructeur.
        //@}

    protected:
        //! @name Autres constructeurs
        //@{
                    Dungeon();                          //!< Constructeur par d�faut.
                    Dungeon(PAK::FileHdl pFileHdl);     //!< Constructeur (chargement diff�r�).
        //@}
        //! @name Gestion des donn�es
        //@{
        void        swap(Dungeon& pLoaded);             //!< Permutation des donn�es.
virtual void        store(PAK::Saver& pSaver) const;    //!< Sauvegarde des donn�es.
        //@}

    private:
        //! @name Parcours des lieux � rendre
        //@{
virtual PlacePtr    first(ConstCameraPtr pCamera);      //!< Premier lieu visible.
virtual PlacePtr    next();                             //!< Lieu visible suivant.
virtual PlacePtr    current();                          //!< Lieu visible actuel.
        //@}
virtual EType       getWorldType() const;               //!< Type du monde.
virtual UTI::Rect   getArea() const;                    //!< Zone occup�e par le monde.
    };


//    typedef RSC::Resource<Dungeon>                DungeonRsc;    //!< Ressource labyrinthe.
//    typedef RSC::ResourceMgr<DungeonRsc>::TKey    Key;           //!< Clef pour un WOR::Dungeon.
}

#endif  // De WOR_DUNGEON_HH
