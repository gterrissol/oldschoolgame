/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseWorld.hh"

/*! @file ENGine/WORld/BaseWorld.cc
    @brief M�thodes (non-inline) de la classe WOR::World.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 14 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "APPearance/BaseAppearance.hh"
#include "CAMera/BaseCamera.hh"
#include "MATH/M4.hh"
#include "PAcKage/LoaderSaver.hh"
#include "PLaCes/Place.hh"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "UTIlity/Rect.hh"

#include "ErrMsg.hh"

namespace WOR
{
//------------------------------------------------------------------------------
//                                P-Impl de World
//------------------------------------------------------------------------------

    /*! @brief P-Impl de WOR::World.
        @version 0.41
     */
    class World::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
                Private();                  //!< Constructeur par d�faut.
        //@}
        LightPtr                mSun;       //!< Soleil.
        PAK::FileHdl            mGroupHdl;  //!< Donn�es communes.
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::Private::Private()
        : mSun()
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    World::Group::Group(PAK::Loader& pLoader)
        : mCommon{}
        , mOut{}
        , mUnd{}
        , mIns{}
    {
        pLoader >> mCommon;
        pLoader >> mOut;
        pLoader >> mUnd;
        U32 lInsideCount{};
        pLoader >> lInsideCount;
        mIns.reserve(lInsideCount);
        for(U32 lI = k0UL; lI < lInsideCount; ++lI)
        {
            PAK::FileHdl lHdl{};
            pLoader >> lHdl;
            mIns.push_back(lHdl);
        }
    }

    /*!
     */
    PAK::FileHdl World::Group::fileHdl() const
    {
        return mCommon;
    }

    /*!
     */
    U32 World::Group::count() const
    {
        return U32(mOut.isValid() + mUnd.isValid() + mIns.size());
    }

    /*!
     */
    Vector<PAK::FileHdl> World::Group::worldHdls() const
    {
        Vector<PAK::FileHdl>    lWorlds;
        lWorlds.reserve(1 + 1 + mIns.size());

        lWorlds.push_back(mOut);
        lWorlds.push_back(mUnd);
        std::copy(begin(mIns), end(mIns), std::back_inserter(lWorlds));

        return lWorlds;
    }

    /*!
     */
    void World::Group::store(PAK::Saver& pSaver) const
    {
        pSaver << (const_cast<Group*>(this)->mCommon = pSaver.fileHdl());
        pSaver << mOut;
        pSaver << mUnd;
        pSaver << U32(mIns.size());

        for(auto lHdl : mIns)
        {
            pSaver << lHdl;
        }
    }

    /*!
     */
    void World::Group::insert(World::Ptr pWorld)  // Conceptuellement, le retrait n'est l� que pour la gestion des undo/redo.
    {
        switch(pWorld->worldType())
        {
        case eOutside:
            ASSERT_EX(!mOut.isValid(), "Already an outside in this group.", return;)
            mOut = pWorld->fileHdl();
            break;
        case eUnderground:
            ASSERT_EX(!mUnd.isValid(), "Already an underground in this group.", return;)
            mUnd = pWorld->fileHdl();
            break;
        case eCave:
        case eDungeon:
        case eHouse:
            ASSERT_EX(std::find(mIns.begin(), mIns.end(), pWorld->fileHdl()) == mIns.end(), "World already in this group", return;)
            mIns.push_back(pWorld->fileHdl());
            break;
        default:
            ASSERT_EX(false, "Invalid world type.", return;)
        }
    }

    /*!
     */
    void World::Group::remove(World::Ptr pWorld)
    {
        switch(pWorld->worldType())
        {
        case eOutside:
            ASSERT_EX(mOut == pWorld->fileHdl(), "Another outside in this group.", return;)
            mOut = PAK::eBadHdl;
            break;
        case eUnderground:
            ASSERT_EX(mUnd == pWorld->fileHdl(), "Another underground in this group.", return;)
            mUnd = PAK::eBadHdl;
            break;
        case eCave:
        case eDungeon:
        case eHouse:
        {
            auto    lInside = std::find(mIns.begin(), mIns.end(), pWorld->fileHdl());
            ASSERT_EX(lInside != mIns.end(), "World not in this group", return;)
            mIns.erase(lInside);
            break;
        }
        default:
            ASSERT_EX(false, "Invalid world type.", return;)
        }
    }


//------------------------------------------------------------------------------
//                      World : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    World::World(PAK::Loader& pLoader)
        : Scene(pLoader)
        , pthis()
    {
        pLoader >> pthis->mGroupHdl;

        I32 lType(0);
        pLoader >> lType;
    }


    /*! Destructeur.
     */
    World::~World() { }


//------------------------------------------------------------------------------
//                               Gestion du Rendu
//------------------------------------------------------------------------------

    /*! Le rendu d'un monde ne peut �tre demand� que pour une partie de celui-ci, d�limit�e par un cadre
        (d�fini par une cam�ra).<br>
        Une fois un tel cadre "pos�", les lieux "marqu�s" comme visibles peuvent �tre parcourus pour �tre
        rendus. Cette m�thode est charg�e de recenser les Lieux visibles et de retourner le premier
        d'entre eux.<br>
        Un parcours des lieux visibles peut se faire par une boucle telle que la suivante :
        @code
    WOR::World*    lWorld  = ...;
    ConstCameraPtr lCamera = ...;

    for(auto lPlace = lWorld->firstPlace(lCamera); lPlace != nullptr; lPlace = lWorld->nextPlace())
    {
        // Utilisation de lPlace, ou lWorld->currentPlace().
    }
        @endcode
        @param pCamera Cette cam�ra d�finit le cadre de rendu : seules les lieux visibles seront
        accessibles
        @return La premier lieu � "rendre" ou nullptr si le monde ne contient aucun lieu visible
        @sa NextPlace & CurrentPlace
     */
    World::PlacePtr World::firstPlace(ConstCameraPtr pCamera)
    {
        return first(pCamera);
    }


    /*! Retourne le lieu � rendre suivant l'actuel (renvoy� par currentPlace()) et l'�tablit comme
        nouveau lieu actuel.
        @return Le lieu suivant l'actuel, dans la liste des lieux visibles ou nullptr s'il n'y en a plus
        @note Ne fait rien s'il n'y a plus de lieu visible (currentPlace() retourne nullptr)
        @sa FirstPlace & CurrentPlace
     */
    World::PlacePtr World::nextPlace()
    {
        return next();
    }


    /*! Permet de connapitre le lieu visible actuellement s�lectionn� pour le rendu.
        @return Le lieu actuellement selectionn� lors du parcours
        @sa FirstPlace & NextPlace
     */
    World::PlacePtr World::currentPlace()
    {
        return current();
    }


    /*!
     */
    void World::go() { }


    /*! D�finition du soleil.
     */
    void World::shine(LightPtr pSun)
    {
        pthis->mSun = pSun;
    }


    /*!
     */
    World::LightPtr World::sun() const
    {
        return pthis->mSun;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool World::isActive() const
    {
        return false;
    }


    /*!
     */
    EType World::worldType() const
    {
        return getWorldType();
    }


    /*!
     */
    UTI::Rect World::area() const
    {
        return getArea();
    }


    /*!
     */
    void World::bind(PAK::FileHdl pGroup)
    {
        pthis->mGroupHdl = pGroup;
    }


    /*! @return Le handle du fichier du groupe de mones auquel l'instance appartient.
     */
    PAK::FileHdl World::group() const
    {
        return pthis->mGroupHdl;
    }


//------------------------------------------------------------------------------
//                             Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    World::World()
        : pthis{}
    { }


    /*! Constructeur (chargement diff�r�).
     */
    World::World(PAK::FileHdl pFileHdl)
        : Scene(pFileHdl)
        , pthis()
    { }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
     */
    void World::swap(World& pLoadedWorld)
    {
        Scene::swap(pLoadedWorld);
    }


    /*! Sauvegarde des donn�es.
     */
    void World::store(PAK::Saver& pSaver) const
    {
        Scene::store(pSaver);

        pSaver << pthis->mGroupHdl;

        // Type de niveau.
        pSaver << I32(worldType());
    }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn World::PlacePtr World::first(ConstCameraPtr pCamera)
        Cette m�thode d�finit l'impl�mentation de firstPlace(), son comportement doit donc correspondre �
        ses exigences.
        @param pCamera Cam�ra d�finissant le cadre de rendu
        @return Le premier lieu visible
        @sa Design Patterns : patron de m�thode (template method)
     */


    /*! @fn World::PlacePtr World::next()
        Cette m�thode d�finit l'impl�mentation de nextPlace(), son comportement doit donc correspondre �
        ses exigences.
        @return Le lieu visible suivant l'actuel
        @sa Design Patterns : patron de m�thode (template method)
     */


    /*! @fn World::PlacePtr World::current()
        Cette m�thode d�finit l'impl�mentation de currentPlace(), son comportement doit donc correspondre
        � ses exigences.
        @return Le lieu visible actuel
        @sa Design Patterns : patron de m�thode (template method)
     */


//------------------------------------------------------------------------------
//                           P-Impl de World Renderer
//------------------------------------------------------------------------------

    /*! @brief P-Impl de WOR::WorldRenderer.
        @version 0.2
     */
    class WorldRenderer::Private : public MEM::OnHeap
    {
    public:

        typedef WorldRenderer::WorldPtr   WorldPtr;
        //! @name Constructeur
        //@{
        Private(WorldPtr pWorld);   //!< Constructeur.
        //@}
        WorldPtr    mWorld;         //!< Monde � afficher.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    WorldRenderer::Private::Private(WorldPtr pWorld)
        : mWorld(pWorld)
    { }


//------------------------------------------------------------------------------
//                  World Renderer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    WorldRenderer::WorldRenderer(WorldPtr pWorld)
        : pthis(pWorld)
    { }


    /*! Destructeur.
     */
    WorldRenderer::~WorldRenderer() { }


//------------------------------------------------------------------------------
//                            World Renderer : Rendu
//------------------------------------------------------------------------------

    /*! Pr�paration du rendu.
     */
    void WorldRenderer::dumpAppearances()
    {
        MATH::M4    lM1(MATH::V4(k1F,   k0F,  k0F, k0F),
                        MATH::V4(k0F,   k1F,  k0F, k0F),
                        MATH::V4(k0F,   k0F,  k1F, k0F),
                        MATH::V4(k0F, - k1F,  k0F, k1F));   // Attention : vecteurs colonnes !

        if (pthis->mWorld.expired())
        {
            return;
        }

        SharedPtr<WorldPtr::element_type>   lWorld = pthis->mWorld.lock();

        for(auto lPlaceI = lWorld->firstPlace(camera().lock()); lPlaceI != nullptr; lPlaceI = lWorld->nextPlace())
        {
            World::PlacePtr::element_type::AppearancePtr lApp;
            for(lPlaceI->firstApp(lApp, lM1); lApp != nullptr; lPlaceI->nextApp(lApp, lM1))
            {
                if (lApp->litingLights()->count() == 0)
                {
                    lApp->getLit(lWorld->sun().lock());
                }

                pushAppearance(lApp, lM1);
            }
        }
    }
}
