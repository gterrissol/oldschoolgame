/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "WORld.hh"

/*! @file ENGine/WORld/WORld.cc
    @brief D�finitions diverses du module WOR.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace WOR
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kCommonDataFileHandleMissing, "Handle de fichier de donn�es communes absent.", "Common data file handle missing.")
}
