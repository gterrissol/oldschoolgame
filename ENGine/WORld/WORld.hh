/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_WORLD_HH
#define WOR_WORLD_HH

/*! @file ENGine/WORld/WORld.hh
    @brief Pr�-d�clarations du module @ref WORld.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace WOR
{
    class Dungeon;
    class DungeonController;
    class Inside;
    class Scene;
    class World;
    class WorldRenderer;
}

#endif  // De WOR_WORLD_HH
