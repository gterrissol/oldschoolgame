/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Scene.hh"

/*! @file ENGine/WORld/Scene.cc
    @brief M�thodes (non-inline) de la classe WOR::Scene.
    @author @ref Guillaume_Terrissol
    @date 27 Avril 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace WOR
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Scene::Scene(PAK::Loader& pLoader)
        : RSC::Object(pLoader)
    { }


    /*! Destructeur.
     */
    Scene::~Scene() { }


//------------------------------------------------------------------------------
//                               Gestion du Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void Scene::setDisplayProperties(EDisplayProperty /*pProperties*/)
    {
    }


//------------------------------------------------------------------------------
//                             Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Scene::Scene() { }


    /*! Constructeur (chargement diff�r�).
     */
    Scene::Scene(PAK::FileHdl pFileHdl)
        : RSC::Object(pFileHdl)
    { }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
     */
    void Scene::swap(Scene& /*pLoadedScene*/) { }


    /*! Sauvegarde des donn�es.
     */
    void Scene::store(PAK::Saver& pSaver) const
    {
        RSC::Object::store(pSaver);
    }
}
