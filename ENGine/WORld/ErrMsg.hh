/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_ERRMSG_HH
#define WOR_ERRMSG_HH

/*! @file ENGine/WORld/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module WOR.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace WOR
{
#ifdef ASSERTIONS

    // ! @name Message d'assertion
    // @{
    extern  ERR::String kCommonDataFileHandleMissing;
    // @}

#endif  // De ASSERTIONS
}

#endif  // De WOR_ERRMSG_HH
