/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_ALL_HH
#define WOR_ALL_HH

/*! @file ENGine/WORld/All.hh
    @brief Interface publique du module @ref WORld.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 13 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace WOR   //! Monde (fragment d'univers).
{
    /*! @namespace WOR
        @version 0.26

     */

    /*! @defgroup WORld WORld : Gestion des mondes
        <b>namespace</b> WOR.
     */
}

#include "Scene.hh"
#include "BaseWorld.hh"
#include "Inside.hh"
#include "Outside.hh"
#include "Dungeon.hh"

#endif  // De WOR_ALL_HH
