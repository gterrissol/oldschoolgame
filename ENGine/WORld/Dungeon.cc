/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Dungeon.hh"

/*! @file ENGine/WORld/Dungeon.cc
    @brief M�thodes (non-inline) de la classe WOR::Dungeon.
    @author @ref Guillaume_Terrissol
    @date 17 Mai 2005 - 20 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/SharedPtr.hh"
#include "UTIlity/Rect.hh"

namespace WOR
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Dungeon::Dungeon(PAK::Loader& pLoader)
        : Inside(pLoader)
    { }


    /*! Destructeur.
     */
    Dungeon::~Dungeon() { }


//------------------------------------------------------------------------------
//                              Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Dungeon::Dungeon() { }


    /*! Constructeur (chargement diff�r�).
     */
    Dungeon::Dungeon(PAK::FileHdl pFileHdl)
        : Inside(pFileHdl)
    { }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
     */
    void Dungeon::swap(Dungeon& pLoaded)
    {
        swap(pLoaded);
    }

    /*! Sauvegarde des donn�es.
     */
    void Dungeon::store(PAK::Saver& pSaver) const
    {
        Inside::store(pSaver);
    }


//------------------------------------------------------------------------------
//                          Parcours des Lieux � Rendre
//------------------------------------------------------------------------------

    /*! Etablit la liste des apparences visibles et retourne la premi�re d'entre elles.
        @param pCamera Camera d�finissant le cadre de rendu du monde
        @return La premi�re apparence visible, ou nullptr s'il n'y en a pas
     */
    Dungeon::PlacePtr Dungeon::first(ConstCameraPtr)
    {
        return nullptr;
    }


    /*! 
     */
    Dungeon::PlacePtr Dungeon::next()
    {
        return nullptr;
    }


    /*!
     */
    Dungeon::PlacePtr Dungeon::current()
    {
        return nullptr;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    EType Dungeon::getWorldType() const
    {
        return eDungeon;
    }


    /*!
     */
    UTI::Rect Dungeon::getArea() const
    {
        // Temporaire...
        return UTI::Rect(F32(-64.0F), F32(64.0F), F32(64.0F), F32(-64.0F));
    }
}
