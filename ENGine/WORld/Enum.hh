/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_ENUM_HH
#define WOR_ENUM_HH

/*! @file ENGine/WORld/Enum.hh
    @brief Enum�rations du module WOR.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 4 D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace WOR
{
    //! Propri�t�s d'affichage.
    enum EDisplayProperty
    {
        eDirect     = 0x00,
        eForeGround = 0x01
    };

    //! Types de monde.
    enum EType
    {
        eNone,          //!< Non-initialis�.
        eMenu,          //!< Menu.
        eOutside,       //!< Ext�rieur.
        // Int�rieurs :
        eUnderground,   //!< Monde souterrain.
        eCave,          //!< Grotte (au sens large).
        eDungeon,       //!< Labyrinthe.
        eHouse          //!< Maison.
    };
}

#endif // De WOR_ENUM_HH
