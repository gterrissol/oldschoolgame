/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef WOR_SCENE_HH
#define WOR_SCENE_HH

#include "WORld.hh"

/*! @file ENGine/WORld/Scene.hh
    @brief En-t�te de la classe WOR::Scene.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "OBJect/GameObject.hh"
#include "PAcKage/PAcKage.hh"
#include "ReSourCe/Object.hh"

#include "Enum.hh"

namespace WOR
{
    /*! @brief Sc�ne.
        @version 0.2
     */
    class Scene : public RSC::Object, public OBJ::GameEntity
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Scene(PAK::Loader& pLoader);                        //!< Constructeur.
virtual         ~Scene();                                           //!< Destructeur.
        //@}
        //! @name Gestion du rendu
        //@{
virtual void    go() = 0;                                           //!< Progression.
        void    setDisplayProperties(EDisplayProperty pProperties); //!< 
        //@}
    protected:
        //! @name Autres constructeurs
        //@{
                Scene();                                            //!< Constructeur par d�faut.
                Scene(PAK::FileHdl pFileHdl);                       //!< Constructeur (chargement diff�r�).
        //@}
        //! @name Gestion des donn�es
        //@{
virtual void    swap(Scene& pLoadedScene);                          //!< Permutations des donn�es.
virtual void    store(PAK::Saver& pSaver) const = 0;                //!< Sauvegarde des donn�es.
        //@}
    };
}

#endif  // De WOR_SCENE_HH
