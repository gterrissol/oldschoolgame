/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "UNIverse.hh"

/*! @file ENGine/UNIverse/UNIverse.cc
    @brief D�finitions diverses du module UNI.
    @author @ref Guillaume_Terrissol
    @date 31 Janvier 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace UNI
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kUndefinedRootMenu, "Menu racine non-d�fini.", "Undefined root menu.")
    REGISTER_ERR_MSG(kNoWorldDefined,    "Aucun monde d�fini.",     "No world defined.")


//------------------------------------------------------------------------------
//                                  Constantes
//------------------------------------------------------------------------------

    const char* kUniverseDefaultName    = "No name";
}
