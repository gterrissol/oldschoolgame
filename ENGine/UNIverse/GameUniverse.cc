/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameUniverse.hh"

/*! @file ENGine/UNIverse/GameUniverse.cc
    @brief M�thodes (non-inline) des classes UNI::Universe & UNI::GameUniverse.
    @author @ref Guillaume_Terrissol
    @date 31 Janvier 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "PAcKage/LoaderSaver.hh"
#include "OUTside/World.hh"
#include "STL/Shared.tcc"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"
#include "UNDerground/World.hh"
#include "WORld/Dungeon.hh"

#include "Const.hh"
#include "ErrMsg.hh"

namespace UNI
{
//------------------------------------------------------------------------------
//                              P-Impl de Universe
//------------------------------------------------------------------------------

    /*! @brief P-Impl de UNI::Universe.
        @version 0.4
     */
    class Universe::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
                                    Private();                              //!< Constructeur par d�faut.
                                    Private(PAK::Loader& pLoader);          //!< Constructeur de chargement.
        //@}
        //! @name Gestion des mondes
        //@{
        WOR::World::Ptr             buildWorld(PAK::Loader& pLoader);       //!< Cr�ation d'un monde.
                bool                insertWorld(PAK::FileHdl pWorldHdl);    //!< 
                bool                removeWorld(PAK::FileHdl pWorldHdl);    //!< 
                void                insertWorld(WOR::World::Ptr pWorld);    //!< 
        //@}
        //! @name "Singletons"
        //@{
        inline  CAM::Camera::Ptr    camera();                               //!< Point de vue sur la sc�ne.
        inline  GXV::Viewport::Ptr  viewport();                             //!< "Fen�tre" sur la sc�ne.
        //@}
        //! @name Attributs
        //@{
        typedef SharedPtr<RDR::Renderer>    RendererPtr;                    //!< Pointeur sur Renderer.
        Vector<WOR::World::Ptr>             mWorlds;                        //!< Mondes de l'univers.
        Vector<WOR::World::GroupPtr>        mGroups;                        //!< Mondes de l'univers, par groupes.
        Vector<RendererPtr>                 mRenderers;                     //!< Syst�mes de rendu pour chaque monde.
        WorldContainer                      mActiveWorlds;                  //!< ? Mondes actuellement utilis�s.
        GXV::Viewport::Ptr                  mViewport;
        SharedPtr<LIT::DirectionalLight>    mSun;                           //!< Ph�bus.
        TILE::FragmentMgr::Ptr              mFragments;                     //!< 
        //@}
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Universe::Private::Private()
        : mWorlds{}
        , mGroups{}
        , mRenderers()
        , mActiveWorlds{}
        , mSun()
        , mFragments{}
    { }


    /*! Constructeur via chargement de donn�es.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Universe::Private::Private(PAK::Loader& pLoader)
        : mGroups{}
        , mRenderers()
        , mActiveWorlds()
        , mViewport{}
        , mSun()
        , mFragments{}
    {
        // Cr�ations.
        U32 lWorldGroupCount = k0UL;
        pLoader >> lWorldGroupCount;

        // Soleil.
        mSun = STL::makeShared<LIT::DirectionalLight>();
        mSun->setDirection(MATH::V4(+k1F, +k1F, +k1F, k0F).normalized());
        mSun->setAmbient(CLR::RGBA(F32(0.2F), F32(0.2F), F32(0.2F), k1F));
        mSun->setDiffuse(CLR::RGBA(F32(0.8F), F32(0.8F), F32(0.8F), k1F));
        mSun->setSpecular(CLR::RGBA(k0F, k0F, k0F, k1F));

        // Mondes.
        for(U32 lG = k0UL; lG < lWorldGroupCount; ++lG)
        {
            PAK::FileHdl    lGroupHdl{};
            pLoader >> lGroupHdl;
            PAK::Loader     lGroupLoader{lGroupHdl};
            mGroups.push_back(STL::makeShared<WOR::World::GroupPtr::element_type>(lGroupLoader));
            auto    lWorlds = mGroups.back()->worldHdls();

            for(auto lWorldFileHdl : lWorlds)
            {
if (!lWorldFileHdl.isValid()) continue; // Ligne � retirer quand les types de mondes seront revenus.
                // Cr�ation.
                PAK::Loader lWorldLoader(lWorldFileHdl);
                insertWorld(buildWorld(lWorldLoader));
            }
        }

        {
            PAK::FileHdl    lFragmentMgrHdl;
            pLoader >> lFragmentMgrHdl;

            PAK::Loader lFragMgrLoader{lFragmentMgrHdl};
            mFragments = STL::makeShared<TILE::FragmentMgr>();
            mFragments->load(lFragMgrLoader);
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Cr�� un monde.
     */
    WOR::World::Ptr Universe::Private::buildWorld(PAK::Loader& pLoader)
    {
        PAK::FileHdl    lWetData;
        pLoader >> lWetData;
        I32             lWorldType(WOR::eNone);
        pLoader >> lWorldType;
        pLoader.rollback(U32(sizeof(PAK::FileHdl) + sizeof(I32)));

        WOR::World::Ptr lWorld;

        switch(lWorldType)
        {
            case WOR::eOutside:
                lWorld = STL::makeShared<OUT::World>(pLoader);
                break;
            case WOR::eUnderground:
                lWorld = STL::makeShared<UND::World>(pLoader);
                break;
            case WOR::eCave:
//                lWorld = STL::makeShared<WOR::Cave>(pLoader);
                break;
            case WOR::eDungeon:
                lWorld = STL::makeShared<WOR::Dungeon>(pLoader);
                break;
            case WOR::eHouse:
//                lWorld = STL::makeShared<HSE::World>(pLoader);
                break;
            default:
                break;
        }

        return lWorld;
    }


    /*! Ajoute un monde � l'univers.
        @param pWorldHdl Handle du fichier du nouveau monde
        @return VRAI si <i>pWorldHdl</i> a �t� ins�r� dans l'univers, FAUX sinon (il en faisait peut-�tre
        d�j� partie)
     */
    bool Universe::Private::insertWorld(PAK::FileHdl pWorldHdl)
    {
        if (pWorldHdl == PAK::eBadHdl)
        {
            return false;
        }

        auto lIter = std::find_if(mWorlds.begin(), mWorlds.end(), RSC::HasHandle(pWorldHdl));

        if (lIter == mWorlds.end())
        {
            PAK::Loader lWorldLoader(pWorldHdl);
            auto    lWorld  = buildWorld(lWorldLoader);

            insertWorld(lWorld);

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! Retire un monde de l'univers.
        @param pWorldHdl Handle de fichier de l'ancien (bient�t) monde de l'univers
        @return VRAI si <i>pWorldHdl</i> a �t� retir� dans l'univers, FAUX sinon (il ne devait pas en
        faire partie)
        @note Le fichier associ� � <i>pWorldHdl</i> est conserv�
     */
    bool Universe::Private::removeWorld(PAK::FileHdl pWorldHdl)
    {
        if (pWorldHdl == PAK::eBadHdl)
        {
            return false;
        }

        auto lIter = std::find_if(mWorlds.begin(), mWorlds.end(), RSC::HasHandle(pWorldHdl));

        if (lIter != mWorlds.end())
        {
            U32 lWorldIndex = U32(lIter - mWorlds.begin());

            // Permute le monde � retirer (et son renderer) avec
            // le dernier �l�ment du tableau (dans les deux cas).
            std::swap(mWorlds[lWorldIndex], mWorlds.back());
            std::swap(mRenderers[lWorldIndex], mRenderers.back());

            // Un monde et son renderer ont toujours la m�me position, dans leur tableau respectif.

            // D�pile.
            mWorlds.pop_back();

            if (auto lViewport = viewport())
            {
                lViewport->detach(mRenderers.back());
            }
            mRenderers.pop_back();

            return true;
        }
        else
        {
            return false;
        }
    }


    /*!
     */
    void Universe::Private::insertWorld(WOR::World::Ptr pWorld)
    {
        // Ajoute le monde dans la liste.
        mWorlds.push_back(pWorld);
        // L'�claire.
        pWorld->shine(mSun);
        // Il ne faut pas oublier d'ajouter un renderer.
        mRenderers.push_back(STL::makeShared<WOR::WorldRenderer>(mWorlds.back()));
        // Ni d'attacher ce dernier au viewport.
        if (auto lViewport = viewport())
        {
            lViewport->attach(mRenderers.back());
        }
    }


//------------------------------------------------------------------------------
//                         Game Universe : "Singletons"
//------------------------------------------------------------------------------

    /*! Cam�ra "filmant" le jeu.
     */
    inline CAM::Camera::Ptr Universe::Private::camera()
    {
        return viewport()->camera();
    }


    /*! Vue sur le jeu.
     */
    inline GXV::Viewport::Ptr Universe::Private::viewport()
    {
        return mViewport;
    }


//------------------------------------------------------------------------------
//                  Game Universe : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Universe::Universe(PAK::Loader& pLoader)
        : RSC::Object{pLoader}
        , pthis(pLoader)
    { }


    /*! Destructeur.
     */
    Universe::~Universe()
    {
        if (auto lViewport = viewport())
        {
            for(auto lRendererI = pthis->mRenderers.rbegin(); lRendererI != pthis->mRenderers.rend(); ++lRendererI)
            {
                lViewport->detach(*lRendererI);
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    WOR::World::GroupPtr Universe::group(PAK::FileHdl pGroupHdl) const
    {
        auto lIter = std::find_if(pthis->mGroups.begin(), pthis->mGroups.end(), [pGroupHdl](decltype(*pthis->mGroups.cbegin()) pGroup) {
            return pGroup->fileHdl() == pGroupHdl;
        });

        if (lIter != pthis->mGroups.end())
        {
            return *lIter;
        }
        else
        {
            return nullptr;
        }
    }


    /*!
     */
    WOR::World::Ptr Universe::world(PAK::FileHdl pWorldHdl) const
    {
        auto lIter = std::find_if(pthis->mWorlds.begin(), pthis->mWorlds.end(), RSC::HasHandle(pWorldHdl));

        if (lIter != pthis->mWorlds.end())
        {
           return *lIter;
        }
        else
        {
            return nullptr;
        }
    }


    /*!
     */
    TILE::TileMgr::Ptr Universe::tileMgr(PAK::FileHdl pWorldHdl) const
    {
        auto lWorldIter = std::find_if(pthis->mWorlds.begin(), pthis->mWorlds.end(), RSC::HasHandle(pWorldHdl));

        if (lWorldIter != pthis->mWorlds.end())
        {
            if (auto lGroup = group((*lWorldIter)->group()))
            {
                if (auto lOutside = std::dynamic_pointer_cast<OUT::World>(world(lGroup->worldHdls()[WOR::World::Group::eOutside])))
                {
                    return lOutside->tileMgr();
                }
            }
        }

        return nullptr;
    }


    /*! @return Les mondes "actifs" de l'univers
        (anciennement rootWorlds : retournait overworlds & underworlds)
     */
    Universe::WorldContainer Universe::activeWorlds() const
    {
        return pthis->mWorlds;
    }


//------------------------------------------------------------------------------
//                                Boucle de Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void Universe::render(GXV::Viewport::Ptr pView)
    {
        if (pthis->mViewport)
        {
            for(auto lRendererI = pthis->mRenderers.rbegin(); lRendererI != pthis->mRenderers.rend(); ++lRendererI)
            {
                viewport()->detach(*lRendererI);
            }
        }
        pthis->mViewport = pView;
        if (pthis->mViewport)
        {
            for(auto lRendererI = pthis->mRenderers.rbegin(); lRendererI != pthis->mRenderers.rend(); ++lRendererI)
            {
                viewport()->attach(*lRendererI);
            }
        }
    }


    /*! Progression de l'univers.
     */
    void Universe::go()
    {
        for(auto lWorld : pthis->mWorlds)
        {
            lWorld->go();
        }
    }


//------------------------------------------------------------------------------
//                         Game Universe : "Singletons"
//------------------------------------------------------------------------------

    /*! Cam�ra "filmant" le jeu.
     */
    CAM::Camera::Ptr Universe::camera() const
    {
        return pthis->camera();
    }


    /*! Vue sur le jeu.
     */
    GXV::Viewport::Ptr Universe::viewport() const
    {
        return pthis->viewport();
    }


    /*! Soleil : lumi�re commune pour tous les objets situ�s � l'�xt�rieur.
     */
    LIT::Light::Ptr Universe::sun() const
    {
        return pthis->mSun;
    }


    /*!
     */
    TILE::FragmentMgr::Ptr Universe::fragments() const
    {
        return pthis->mFragments;
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    bool Universe::addGroup(PAK::FileHdl pGroupHdl)
    {
        if (group(pGroupHdl) == nullptr)
        {
            PAK::Loader lGroupLoader{pGroupHdl};
            pthis->mGroups.push_back(STL::makeShared<WOR::World::GroupPtr::element_type>(lGroupLoader));
            return true;
        }

        return false;
    }

    /*!
     */
    bool Universe::delGroup(PAK::FileHdl pGroupHdl)
    {
        auto lIter = std::find_if(pthis->mGroups.begin(), pthis->mGroups.end(), [pGroupHdl](decltype(*pthis->mGroups.cbegin()) pGroup) {
            return pGroup->fileHdl() == pGroupHdl;
        });

        if (lIter != pthis->mGroups.end())
        {
            pthis->mGroups.erase(lIter);
            return true;
        }

        return false;
    }


    /*! @copydoc UNI::Universe::Private::insertWorld(PAK::FileHdl pWorldHdl)
     */
    bool Universe::insertWorld(PAK::FileHdl pWorldHdl)
    {
        return pthis->insertWorld(pWorldHdl);
    }


    /*! @copydoc UNI::Universe::Private::removeWorld(PAK::FileHdl pWorldHdl)
     */
    bool Universe::removeWorld(PAK::FileHdl pWorldHdl)
    {
        return pthis->removeWorld(pWorldHdl);
    }


//------------------------------------------------------------------------------
//                              Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Universe::Universe()
        : pthis()
    { }


//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
        @param pSaver "Fichier" dans lequel �crire les donn�es de l'instance
     */
    void Universe::store(PAK::Saver& pSaver) const
    {
        RSC::Object::store(pSaver);

        pSaver << U32(pthis->mGroups.size());
        for(const auto& lGroup : pthis->mGroups)
        {
            PAK::Saver  lGroupSaver{lGroup->fileHdl()};
            pSaver << lGroupSaver.fileHdl();
            lGroup->store(lGroupSaver);
        }
/*
        // Nombre de mondes.
        U32 lWorldCount = U32(pthis->mWorlds.size());
        pSaver << lWorldCount;

        // Type et handle de fichier de chacun de ces mondes.
        for(U32 lW = k0UL; lW < lWorldCount; ++lW)
        {
            auto    lCurrentWorld = pthis->mWorlds[lW];

            pSaver << objectFileHdl(lCurrentWorld.get());
        }*/
        
        pSaver << pthis->mFragments->fileHdl();
        // + FragmentMgr
    }


//------------------------------------------------------------------------------
//                                 Game Universe
//------------------------------------------------------------------------------

    /*! Construteur.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    GameUniverse::GameUniverse(PAK::Loader& pLoader)
        : Universe(pLoader)
    { }


    /*! Destructeur.
     */
    GameUniverse::~GameUniverse() { }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  GameUniverse*   MEM::Singleton<GameUniverse>::smThat    = nullptr;
#endif
}
