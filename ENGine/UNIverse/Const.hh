/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UNI_CONST_HH
#define UNI_CONST_HH

/*! @file ENGine/UNIverse/Const.hh
    @brief Constantes du module UNI.
    @author @ref Guillaume_Terrissol
    @date 7 Ao�t 2006 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UNI
{
    extern  const char* kUniverseDefaultName;   //!< Nom d'univers par d�faut.
}

#endif // De UNI_CONST_HH
