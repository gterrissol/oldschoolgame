/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UNI_UNIVERSE_HH
#define UNI_UNIVERSE_HH

/*! @file ENGine/UNIverse/UNIverse.hh
    @brief Pr�-d�clarations du module @ref UNIverse.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UNI
{
    class Controller;
    class GameUniverse;
    class Universe;
}

#endif  // De UNI_UNIVERSE_HH
