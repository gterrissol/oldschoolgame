/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UNI_UNIVERSE_HH
#define UNI_UNIVERSE_HH

/*! @file ENGine/UNIverse/All.hh
    @brief Interface publique du module @ref UNIverse.
    @author @ref Guillaume_Terrissol
    @date 31 Janvier 2003 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UNI   //! Univers de jeu.
{
    /*! @namespace UNI
        @version 0.2

     */

    /*! @defgroup UNIverse UNIverse : Gestion des univers
        <b>namespace</b> UNI.
     */
}

#include "GameUniverse.hh"

#endif  // De UNI_UNIVERSE_HH
