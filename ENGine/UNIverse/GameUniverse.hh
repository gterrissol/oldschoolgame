/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UNI_GAMEUNIVERSE_HH
#define UNI_GAMEUNIVERSE_HH

#include "UNIverse.hh"

/*! @file ENGine/UNIverse/GameUniverse.hh
    @brief En-t�te des classes UNI::Universe & UNI::GameUniverse.
    @author @ref Guillaume_Terrissol
    @date 31 Janvier 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CAMera/BaseCamera.hh"
#include "GraphXView/Viewport.hh"
#include "LIghT/BaseLight.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "OBJect/GameObject.hh"
#include "PAcKage/PAcKage.hh"
#include "ReSourCe/Object.hh"
#include "STL/STL.hh"
#include "TILE/Fragment.hh"
#include "TILE/BaseTile.hh"
#include "WORld/BaseWorld.hh"

namespace UNI
{
    /*! @brief Univers.
        @version 0.25
     */
    class Universe : public RSC::Object, public OBJ::GameEntity
    {
        //! @name Classe amie
        //@{
 friend class Controller;                                               //!< Le datum conna�t sa donn�e.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        using Ptr               = SharedPtr<Universe>;                  //!< Pointeur sur univers.
        using WorldContainer    = Vector<WOR::World::Ptr>;              //!< Conteneur de mondes.
        //! @name Constructeurs & destructeur
        //@{
                                Universe(PAK::Loader& pLoader);         //!< Constructeur (chargement imm�diat).
virtual                         ~Universe();                            //!< Destructeur.
        //@}
        WOR::World::GroupPtr    group(PAK::FileHdl pGroupHdl) const;    //!< Acc�s � un groupe de mondes.
        WOR::World::Ptr         world(PAK::FileHdl pWorldHdl) const;    //!< Acc�s � un monde (charg�).
        TILE::TileMgr::Ptr      tileMgr(PAK::FileHdl pWorldHdl) const;  //!< Acc�s au gestionnaire de tuiles d'un groupe de mondes.
        WorldContainer          activeWorlds() const;                   //!< Mondes "actifs".
        //! @name Boucle de rendu
        //@{
        void                    render(GXV::Viewport::Ptr pView);       //!< Viewport de rendu.
        void                    go();                                   //!< Progression de l'univers.
        //@}
        //! @name "Singletons"
        //@{
        CAM::Camera::Ptr        camera() const;                         //!< Point de vue sur la sc�ne.
        GXV::Viewport::Ptr      viewport() const;                       //!< "Fen�tre" sur la sc�ne.
        LIT::Light::Ptr         sun() const;                            //!< Soleil.
        TILE::FragmentMgr::Ptr  fragments() const;                      //!< Fragments de texture de tuile.
        //@}
    protected:
        //! @name M�thodes d'�dition
        //@{
        bool                    addGroup(PAK::FileHdl pGroupHdl);       //!< Ajout d'un groupe de mondes.
        bool                    delGroup(PAK::FileHdl pGroupHdl);       //!< Suppression d'un groupe de mondes.

        bool                    insertWorld(PAK::FileHdl pWorldHdl);    //!< Insertion d'un monde.
        bool                    removeWorld(PAK::FileHdl pWorldHdl);    //!< Retrait d'un monde.
        //@}
        //! @name Autre constructeur
        //@{
                                Universe();                             //!< Constructeur par d�faut.
        //@}
        //! @name "Permanence" des donn�es
        //@{
virtual void                    store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //@}

    private:

        FORBID_COPY(Universe)
        PIMPL()
    };


    /*! @brief Univers lanc� par le moteur de jeu
        @version 0.2

        Le principal (seul ?) int�r�t de cette classe est d'�tre un singleton.
     */
    class GameUniverse : public MEM::Singleton<GameUniverse>, public Universe
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        GameUniverse(PAK::Loader& pLoader); //!< Constructeur.
virtual ~GameUniverse();                    //!< Destructeur.
        //@}
    };
}

#endif  // De UNI_GAMEUNIVERSE_HH
