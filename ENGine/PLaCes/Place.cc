/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Place.hh"

/*! @file ENGine/PLaCes/Place.cc
    @brief M�thodes (non-inline) de la classe PLC::Place.
    @author @ref Guillaume_Terrissol
    @date 6 mai 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "STL/SharedPtr.hh"

namespace PLC
{
//------------------------------------------------------------------------------
//                      Place : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (chargement imm�diat).
     */
    Place::Place(PAK::Loader& pLoader)
        : RSC::Object(pLoader)
    { }


    /*! Destructeur.
     */
    Place::~Place() { }


//------------------------------------------------------------------------------
//                               Gestion du Rendu
//------------------------------------------------------------------------------

    /*! Premi�re apparence visible.
     */
    bool Place::firstApp(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        return first(pApp, pFrame);
    }


    /*! Apparence visible suivante.
     */
    bool Place::nextApp(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        return next(pApp, pFrame);
    }


    /*! Apparence visible actuelle.
     */
    bool Place::currentApp(AppearancePtr& pApp, MATH::M4& pFrame)
    {
        return current(pApp, pFrame);
    }


//------------------------------------------------------------------------------
//                         Place : Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Place::Place() { }


    /*! Constructeur (chargement diff�r�).
     */
    Place::Place(PAK::FileHdl pHdl)
        : RSC::Object(pHdl)
    { }
}
