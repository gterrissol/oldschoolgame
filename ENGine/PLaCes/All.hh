/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_ALL_HH
#define PLC_ALL_HH

/*! @file ENGine/PLaCes/All.hh
    @brief Interface publique du module @ref PLaCes.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 11 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PLC   //! Lieux.
{
    /*! @namespace PLC
        @version 0.15

     */

    /*! @defgroup PLaCes PLaCes : Lieux
        <b>namespace</b> PLC.
     */
}

#include "Passage.hh"
#include "Gate.hh"
#include "Place.hh"
#include "Hall.hh"

#endif  // De PLC_ALL_HH
