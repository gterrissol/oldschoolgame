/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_CONST_HH
#define PLC_CONST_HH

/*! @file ENGine/PLaCes/Const.hh
    @brief Constantes du module PLC.
    @author @ref Guillaume_Terrissol
    @date 8 mai 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PLC
{
    //! @name Noms des passages
    //@{
    extern  const char* kFadingName;        //!< Fondu.
    extern  const char* kPreLoadingName;    //!< Pr�-chargement.
    //@}
}

#endif  // De PLC_CONST_HH
