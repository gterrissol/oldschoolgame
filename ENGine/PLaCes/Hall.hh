/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_HALL_HH
#define PLC_HALL_HH

#include "PLaCes.hh"

/*! @file ENGine/PLaCes/Hall.hh
    @brief En-t�te de la classe PLC::Hall.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ReSourCe/ResourceMgr.hh"

#include "Place.hh"

namespace PLC
{
    /*! @brief Lieu int�rieur.
        @version 0.3

     */
    class Hall : public Place
    {
    public :
        //! @name Constructeurs & destructeur
        //@{
                        Hall(PAK::Loader& pLoader);         //!< Constructeur (chargement imm�diat)..
virtual                 ~Hall();                            //!< Destructeur.


    protected:

                        Hall();                             //!< Constructeur par d�faut.
                        Hall(PAK::FileHdl pHdl);            //!< Constructeur (chargement diff�r�).
        //@}
        //! @name Gestion des donn�es
        //@{
        void            swap(Hall& pHall);                  //!< Permutation des donn�es.
virtual void            store(PAK::Saver& pSaver) const;    //!< Sauvegarde des donn�es.
        //@}

    private:
        //! @name Parcours des apparences
        //@{
virtual AppearancePtr   first();                            //!< Premi�re apparence visible.
virtual AppearancePtr   next();                             //!< Apparence visible suivante.
virtual AppearancePtr   current();                          //!< Apparence visible actuelle.
        //@}
    };


    typedef RSC::Resource<Hall>             HallRsc;        //!< Ressource pi�ce.
    typedef RSC::ResourceMgr<HallRsc>::TKey Key;            //!< Clef pour un RSC::Hall.
}

#endif  // De PLC_HALL_HH
