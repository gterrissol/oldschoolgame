/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Hall.hh"

/*! @file ENGine/PLaCes/Hall.cc
    @brief M�thodes (non-inline) de la classe PLC::Hall.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2003 - 20 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "STL/SharedPtr.hh"

namespace PLC
{
//------------------------------------------------------------------------------
//                          Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (chargement imm�diat).
     */
    Hall::Hall(PAK::Loader& pLoader)
        : Place(pLoader)
    { }


    /*! Destructeur.
     */
    Hall::~Hall() { }


    /*! Constructeur par d�faut.
     */
    Hall::Hall() { }


    /*! Constructeur (chargement diff�r�).
     */
    Hall::Hall(PAK::FileHdl pHdl)
        : Place(pHdl)
    { }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Permutation des donn�es.
     */
    void Hall::swap(Hall& /*pHall*/)
    {
        // A venir.
    }


    /*! Sauvegarde des donn�es.
     */
    void Hall::store(PAK::Saver& /*pSaver*/) const
    {
        // A venir.
    }


//------------------------------------------------------------------------------
//                            Parcours des Apparences
//------------------------------------------------------------------------------

    /*! Premi�re apparence visible.
     */
    Hall::AppearancePtr Hall::first()
    {
        // A venir.
        return nullptr;
    }


    /*! Apparence visible suivante.
     */
    Hall::AppearancePtr Hall::next()
    {
        // A venir.
        return nullptr;
    }


    /*! Apparence visible actuelle.
     */
    Hall::AppearancePtr Hall::current()
    {
        // A venir.
        return nullptr;
    }
}
