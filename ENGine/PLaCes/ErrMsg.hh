/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_ERRMSG_HH
#define PLC_ERRMSG_HH

/*! @file ENGine/PLaCes/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module PLC.
    @author @ref Guillaume_Terrissol
    @date 6 mai 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PLC
{
#ifdef ASSERTIONS

    // ! @name Message d'assertion
    // @{
    // Ins�rez ici vos messages d'assertions (cf. ERR module)
    // @}

#endif  // De ASSERTIONS
}

#endif  // De PLC_ERRMSG_HH
