/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file ENGine/PLaCes/Passage.inl
    @brief M�thodes inline de la classe PLC::Passage et de ses d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 6 mai 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PLC
{
//------------------------------------------------------------------------------
//                     Passage : Caract�ristiques du Passage
//------------------------------------------------------------------------------

    /*! Ex�cute l'action d�finie par cette classe.
        @param pFrom Lieu quitt�
        @param pTo   Lieu p�n�tr�
     */
    inline void Passage::process(Place* pFrom, Place* pTo)
    {
        doProcess(pFrom, pTo);
    }


//------------------------------------------------------------------------------
//                              Fading : Param�tres
//------------------------------------------------------------------------------

    /*! D�finit la dur�e du fondu au noir.
        @param pDelay Dur�e (en ms) d'assombrissement du lieu quitt�
     */
    inline void Fading::setFadeInDelay(U32 pDelay)
    {
        mFadeInDelay = pDelay;
    }


    /*! @return La dur�e (en ms) du fondu au noir
     */
    inline U32 Fading::fadeInDelay() const
    {
        return mFadeInDelay;
    }


    /*! D�finit la dur�e du fondu au clair.
        @param pDelay Dur�e (en ms) d'�claircissement du lieu p�n�tr�
     */
    inline void Fading::setFadeOutDelay(U32 pDelay)
    {
        mFadeOutDelay = pDelay;
    }


    /*! @return La dur�e (en ms) du fondu au clair
     */
    inline U32 Fading::fadeOutDelay() const
    {
        return mFadeOutDelay;
    }
}
