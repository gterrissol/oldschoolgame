/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_PLACE_HH
#define PLC_PLACE_HH

#include "PLaCes.hh"

/*! @file ENGine/PLaCes/Place.hh
    @brief En-t�te de la classe PLC::Place.
    @author @ref Guillaume_Terrissol
    @date 5 mai 2003 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "MATH/MATH.hh"
#include "OBJect/GameObject.hh"
#include "PAcKage/PAcKage.hh"
#include "ReSourCe/Object.hh"
#include "STL/STL.hh"

namespace PLC
{
    /*! @brief Lieu
        @version 0.25

        Un lieu sait � quel monde il appartient (son os parent)
        Lors du chargement d'un lieu, les portes (Gate) associ�es sont cr��es (au besoin) par le
        GateKeeper (GateMgr::get()).
        Ainsi, les portes sont r�f�renc�es directement via des clefs (aucun contr�le effectu� afin de ne
        pas perdre de temps) :
        - 1 : void    openGate(const GateKey& pKey);
        - 2 : Gate*  gate(const GateKey& pKey);
        - 3 : void    closeGate(const GateKey& pKey);
        De cette mani�re, une porte est d�sallou�e d�s lors qu'elle n'est plus utilis�e, il n'y a pas de duplication en m�moire, la coh�rence
        des passages � travers les portes est maintenue lors de l'�dition.
     */
    class Place : public RSC::Object, public OBJ::GameEntity
    {
    public:
        //! @name Types de pointeurs
        //@{
        typedef SharedPtr<APP::Appearance>  AppearancePtr;          //!< Pointeur sur apparence.
        typedef SharedPtr<Gate>             GatePtr;                //!< Pointeur sur porte.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Place(PAK::Loader& pLoader);                        //!< Constructeur (chargement imm�diat).
virtual         ~Place();                                           //!< Destructeur.
        //@}
        //! @name Gestion du rendu
        //@{
        bool    firstApp(AppearancePtr& pApp, MATH::M4& pFrame);    //!< Premi�re apparence visible.
        bool    nextApp(AppearancePtr& pApp, MATH::M4& pFrame);     //!< Apparence visible suivante.
        bool    currentApp(AppearancePtr& pApp, MATH::M4& pFrame);  //!< Apparence visible actuelle.
        //@}

    protected:
        //! @name Autres constructeurs
        //@{
                Place();                                            //!< Constructeur par d�faut.
                Place(PAK::FileHdl pHdl);                           //!< Constructeur (chargement diff�r�).
        //@}
    private:
        //! @name Parcours des apparences
        //@{
virtual bool    first(AppearancePtr& pApp, MATH::M4& pFrame) = 0;   //!< Premi�re apparence visible.
virtual bool    next(AppearancePtr& pApp, MATH::M4& pFrame) = 0;    //!< Apparence visible suivante.
virtual bool    current(AppearancePtr& pApp, MATH::M4& pFrame) = 0; //!< Apparence visible actuelle.
        //@}
//        Vector<GatePtr> mGates;                                     //!< Portes.
    };
}

#endif  // De PLC_PLACE_HH
