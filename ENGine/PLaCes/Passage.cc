/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Passage.hh"

/*! @file ENGine/PLaCes/Passage.cc
    @brief M�thodes (non-inline) de la classe PLC::Passage et de ses d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 8 mai 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/LoaderSaver.hh"
#include "STL/String.hh"

#include "Const.hh"

namespace PLC
{
//------------------------------------------------------------------------------
//                     Passage : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pLoader Ce param�tre n'est pas utilis� pour l'instant (le sera-t-il un jour ?)
     */
    Passage::Passage(PAK::Loader&) { }


    /*! Destructeur.
     */
    Passage::~Passage() { }


    /*! Constructeur par d�faut.
     */
    Passage::Passage() { }


//------------------------------------------------------------------------------
//                     Passage : Caract�ristiques du Passage
//------------------------------------------------------------------------------

    /*! Les diff�rentes actions pouvant �tre associ�es � l'emprunt d'une porte doivent �tre nomm�es afin
        d'�tre facilement r�f�renc�es dans les �diteurs.
        @return Le nom de l'action
     */
    String Passage::name() const
    {
        return actionName();
    }


//------------------------------------------------------------------------------
//                      Passage : "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Ecrit les donn�es de l'instance dans un fichier.
        @param pSaver Les donn�es de l'objet doivent �tre �crites dans ce "fichier"
        @note L'�criture des donn�es doit correspondre � lecture effectu� dans le constructeur
     */
    void Passage::store(PAK::Saver& /*pSaver*/) const
    {
        // Rien pour l'instant.
    }


//------------------------------------------------------------------------------
//                     Fading : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Fading::Fading(PAK::Loader& pLoader)
        : Passage(pLoader)
        , mFadeInDelay(0)
        , mFadeOutDelay(0)
    {
        // R�cup�ration des param�tres de fondu.
        pLoader >> mFadeInDelay;
        pLoader >> mFadeOutDelay;
    }


    /*! Destructeur.
     */
    Fading::~Fading() { }


    /*! Constructeur par d�faut.
     */
    Fading::Fading()
        : mFadeInDelay(0)
        , mFadeOutDelay(0)
    { }


//------------------------------------------------------------------------------
//                       Fading : "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Ecrit les donn�es de l'instance dans un fichier.
        @param pSaver Les donn�es de l'objet doivent �tre �crites dans ce "fichier"
     */
    void Fading::store(PAK::Saver& pSaver) const
    {
        Passage::store(pSaver);

        pSaver << fadeInDelay();
        pSaver << fadeOutDelay();
    }


//------------------------------------------------------------------------------
//                 Fading : Impl�mentation des Caract�ristiques
//------------------------------------------------------------------------------

    /*! Effectue un fondu sur les lieuxs <i>pFrom</i> et <i>pTo</i>.
        @param pFrom Lieu quitt�  : fondu au noir
        @param pTo   Lieu p�n�tr� : fondu au clair (�a se dit ?)
        @todo A impl�menter
     */
    void Fading::doProcess(Place* /*pFrom*/, Place* /*pTo*/)
    {
        // ...
    }


    /*! @return Le nom de l'action
     */
    String Fading::actionName() const
    {
        return kFadingName;
    }
}
