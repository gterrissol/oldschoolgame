/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_GATE_HH
#define PLC_GATE_HH

#include "PLaCes.hh"

/*! @file ENGine/PLaCes/Gate.hh
    @brief En-t�te de la classe PLC::Gate.
    @author @ref Guillaume_Terrissol
    @date 6 mai 2003 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"

namespace PLC
{
    /*! @brief Clef de porte.
        @version 0.2
     */
    class GateKey : public MEM::Auto<GateKey>
    {
    public:
        //! @name Constructeurs
        //@{
        GateKey();  //!< Constructeur par d�faut.
        //@}
    };


    /*! @brief Porte.
        @version 0.2

        Les instances de cette classe permettent le passage d'un lieu � un autre.<br>
        Le d�clenchement d'animations, la gestion correcte de la "r�action du sol", la demande de
        chargement d'autres lieux,... est r�alis�e gr�ce aux portes.
     */
    class Gate : public MEM::OnHeap
    {
    public :
        //! @name Constructeur & destructeur
        //@{
                                Gate();                         //!< Constructeur par d�faut.
virtual                         ~Gate();                        //!< Destructeur.
        //@}
        inline  const GateKey&  key() const;                    //!< Clef associ�e � la porte.
        //! @name Emprunt de la porte
        //@{
                void            exitFrom(Place* pLeftPlace);    //!< Sortie d'une pi�ce.
        //@}

    private:

        FORBID_COPY(Gate)
        PIMPL()
    };


    /*! @brief Gardien des portes.
        @version 0.21
     */
    class GateMgr : public MEM::Singleton<GateMgr>, public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{

        //@}
        //! @name Gestion des portes
        //@{
        void    openGate(const GateKey& pKey);  //!< Ouverture d'une porte.
        Gate*   gate(const GateKey& pKey);          //!< R�cup�ration d'une porte.
        void    closeGate(Gate* pGate);        //!< Fermeture d'une porte.
        //@}
    };


//------------------------------------------------------------------------------
//                      D�finition du "Cerb�re de la Porte"
//------------------------------------------------------------------------------

#define GateKeeper  PLC::GateMgr::get() //!< Gestionnaire des portes.
}

#endif  // De PLC_GATE_HH
