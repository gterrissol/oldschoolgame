/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Gate.hh"

/*! @file ENGine/PLaCes/Gate.cc
    @brief M�thodes (non-inline) de la classe PLC::Gate.
    @author @ref Guillaume_Terrissol
    @date 6 mai 2003 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Vector.hh"

#include "Passage.hh"

namespace PLC
{
//------------------------------------------------------------------------------
//                                P-Impl de Gate
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PLC::Gate.
        @version 0.4
     */
    class Gate::Private : public MEM::OnHeap
    {
    public:
        Private();
        
        Vector<Passage*>    mOnPassingActions;  //!< Actions � r�aliser lors de l'emprunt de la porte.
        Place*              mFirstPlace;        //!< Lieu.
        Place*              mSecondPlace;       //!< Lieu.
        GateKey             mKey;               //!< Clef.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Gate::Private::Private()
        : mOnPassingActions()
        , mFirstPlace(nullptr)
        , mSecondPlace(nullptr)
        , mKey()
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    GateKey::GateKey() { }

//------------------------------------------------------------------------------
//                     Gate : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Gate::Gate()
        : pthis()
    { }


    /*! Destructeur.
     */
    Gate::~Gate() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    const GateKey& Gate::key() const
    {
        return pthis->mKey;
    }


//------------------------------------------------------------------------------
//                          Gate : Emprunt de la Porte
//------------------------------------------------------------------------------

    /*! Sortie d'une pi�ce.
     */
    void Gate::exitFrom(Place* pLeftPlace)
    {
        if      (pLeftPlace == pthis->mFirstPlace)
        {
            for(auto lActionI = pthis->mOnPassingActions.begin();
                lActionI != pthis->mOnPassingActions.end();
                ++lActionI)
            {
                (*lActionI)->process(pthis->mFirstPlace, pthis->mSecondPlace);
            }
        }
        else if (pLeftPlace == pthis->mSecondPlace)
        {
            for(auto lActionI = pthis->mOnPassingActions.begin();
                lActionI != pthis->mOnPassingActions.end(); ++lActionI)
            {
                (*lActionI)->process(pthis->mSecondPlace, pthis->mFirstPlace);
            }
        }
        // Sinon : erreur (ne fait rien ?).
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  GateMgr*    MEM::Singleton<GateMgr>::smThat = nullptr;
#endif
}
