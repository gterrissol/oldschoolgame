/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PLC_PASSAGE_HH
#define PLC_PASSAGE_HH

#include "PLaCes.hh"

/*! @file ENGine/PLaCes/Passage.hh
    @brief En-t�te de classe PLC::Passage et de ses d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 8 mai 2003 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/String.hh"

namespace PLC
{
    /*! @brief Action de passage.
        @version 0.21

        Lors du passage � travers une porte (Gate), certaines actions peuvent �tre r�alis�es (fondu au
        noir du lieu quitt�, pr�paration du chargement d'autres lieux,...). De telles actions sont
        d�finies par les classes d�riv�es de celle-ci.
     */
    class Passage : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Passage(PAK::Loader& pLoader);              //!< Constructeur.
virtual                 ~Passage();                                 //!< Destructeur.
        //@}
        //! @name Caract�ristiques du passage
        //@{
                String  name() const;                               //!< Nom de l'action.
        inline  void    process(Place* pFrom, Place* pTo);          //!< Ex�cution de l'action.
        //@}

    protected:
        //! @name Autre constructeur
        //@{
                        Passage();                                  //!< Constructeur par d�faut.
        //@}
        //! @name "Permanence" des donn�es
        //@{
virtual         void    store(PAK::Saver& pSaver) const;            //!< Sauvegarde des donn�es.
        //@}

    private:
        //! @name Impl�mentation des caract�ristiques
        //@{
virtual         void    doProcess(Place* pPlace, Place* pTo) = 0;   //!< ... de l'action.
virtual         String  actionName() const = 0;                     //!< ... du nom de l'action.
        //@}
    };


    /*! @brief Passage : fondu sur des lieux.
        @version 0.2

        Cette action est surtout destin�e aux passages � travers d'une porte � partir (ou vers) un
        int�rieur.<br>
        Dans les faits, le lieu quitt� s'assombrit jusqu'� dispara�tre. Le lieu "p�n�tr�" subit l'effet
        inverse et appara�t depuis l'obscurit�.
     */
    class Fading : public Passage
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Fading(PAK::Loader& pLoader);           //!< Constructeur.
virtual                 ~Fading();                              //!< Destructeur.


    protected:
                        Fading();                               //!< Constructeur par d�faut.
        //@}
        //! @name Param�tres
        //@{
        inline  void    setFadeInDelay(U32 pDelay);             //!< Etablissement de la dur�e du fondu au noir.
        inline  U32     fadeInDelay() const;                    //!< R�cup�ration de la dur�e du fondu au noir.
        inline  void    setFadeOutDelay(U32 pDelay);            //!< Etablissement de la dur�e du fondu au clair.
        inline  U32     fadeOutDelay() const;                   //!< R�cup�ration de la dur�e du fondu au clair.
        //@}
        //! @name "Permanence" des donn�es
        //@{
virtual         void    store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //@}

    private:        
        //! @name Impl�mentation
        //@{
virtual         void    doProcess(Place* pFrom, Place* pTo);    //!< ... de l'action.
virtual         String  actionName() const;                     //!< ... du nom de l'action.
        //@}
        //! @name Propri�t�s du fondu
        //@{
        U32 mFadeInDelay;                                       //!< Dur�e du fondu au noir.
        U32 mFadeOutDelay;                                      //!< Dur�e du fondu au clair.
        //@}
    };
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Passage.inl"

#endif  // De PLC_PASSAGE_HH
