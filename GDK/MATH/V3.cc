/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "V3.hh"

/*! @file GDK/MATH/V3.cc
    @brief M�thodes (non-inline) de la classe MATH::V3.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 21 Mai 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include <cmath>

#include "ERRor/Log.hh"
#include "MEMory/Array.hh"

#include "ErrMsg.hh"
#include "Float.hh"
#include "V2.hh"
#include "Q.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const V3    kNullV3 = V3(k0F, k0F, k0F);


//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : vecteur null.
     */
    V3::V3()
        : x(k0F)
        , y(k0F)
        , z(k0F)
    { }


    /*! Construit un vecteur � partir de chacune de ses coordonn�es.
        @param pX coordonn�e X
        @param pY coordonn�e Y
        @param pZ coordonn�e Z
     */    
    V3::V3(F32 pX, F32 pY, F32 pZ)
        : x(pX)
        , y(pY)
        , z(pZ)
    { }


    /*! Construit un vecteur � partir d'un tableau de 3 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    V3::V3(const F32 pCoords[3])
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
    { }


    /*! Construit un vecteur � partir d'un tableau de 3 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    V3::V3(const MEM::Array<F32, 3> pCoords)
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
    { }


    /*! Construit un vecteur � partir d'un MATH::V2. La 3e coordonn�e vaut 1.0F par d�faut (pour des
        coordonn�es homog�nes).
        @param pV2 Vecteur dont les coordonn�es seront utilis�es pour initialiser les 2 premi�res coordonn�es de l'instance
     */
    V3::V3(const V2& pV2)
        : x(pV2.x)
        , y(pV2.y)
        , z(k1F)
    { }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z)
     */
    const F32& V3::operator[](size_t pN) const
    {
        ASSERT_EX(pN < 3, kRangeCheckError, return x;)

        return reinterpret_cast<const F32*>(&this->x)[pN];
    }


    /*! @param pN Indice de la coordonn�e demand�e.
        @return R�f�rence sur coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z)
        @note Une coordonn�e peut �tre modifi�e via cet opr�rateur
     */
    F32& V3::operator[](size_t pN)
    {
        ASSERT_EX(pN < 3, kRangeCheckError, return x;)

        return reinterpret_cast<F32 *>(&this->x)[pN];
    }


//------------------------------------------------------------------------------
//                                     Tests
//------------------------------------------------------------------------------

    /*! @return VRAI si le <b>vecteur</b> est �gal au vecteur nul, FAUX sinon
     */
    bool V3::isNull() const
    {
        return (*this == kNullV3);
    }


    /*! @param pE Pr�cision de la comparaison
        @return VRAI si le <b>vecteur</b> est �gal au vecteur nul (� <i>pEpsilon</i> pr�s sur chacune
        de ses coordonn�es), FAUX sinon
     */
    bool V3::isNullEpsilon(F32 pE) const
    {
        return (MATH::isNullEpsilon(x, pE) && MATH::isNullEpsilon(y, pE) && MATH::isNullEpsilon(z, pE));
    }


    /*! @return VRAI si les coordonn�es du <b>vecteur</b> sont valides, FAUX si l'une d'elles au moins
        est NaN
     */
    bool V3::isValid() const
    {
        return !(std::isnan(x) || std::isnan(y) || std::isnan(z));
    }


    /*! Teste si le <b>vecteur</b> est au-dessus d'un plan.
        @param pP "Origine" du plan � consid�rer
        @param pN Vecteur normal du plan � consid�rer
        @return VRAI le <b>vecteur</b> est au-dessus du plan d�fini par <i>pP</i> et <i>pN</i>
     */
    bool V3::isUpFromPlane(const V3& pP, const V3& pN) const
    {
        V3  lThis = *this - pP;

        return (k0F <= (pN * lThis));
    }


//------------------------------------------------------------------------------
//                   Op�rateurs d'Addition et de Soustraction
//------------------------------------------------------------------------------

    /*! @return Le m�me <b>vecteur</b>
     */
    const V3 V3::operator+() const
    {
        return *this;
    }


    /*! @param pV Vecteur � additionner
        @return Le <b>vecteur</b>, auquel on a ajout� <i>pV</i>
     */
    V3& V3::operator+=(const V3& pV)
    {
        x += pV.x;
        y += pV.y;
        z += pV.z;

        return *this;
    }


    /*! @return L'oppos� du <b>vecteur</b>
     */
    const V3 V3::operator-() const
    {
        return V3(- x, - y, - z);
    }


    /*! @param pV Vecteur � soustraire
        @return Le <b>vecteur</b>, auquel on a "soustrait" <i>pV</i> (ou ajout� - <i>pV</i>)
     */
    V3& V3::operator-=(const V3& pV)
    {
        x -= pV.x;
        y -= pV.y;
        z -= pV.z;

        return *this;
    }


//------------------------------------------------------------------------------
//                            Produits & "Divisions"
//------------------------------------------------------------------------------

    /*! @param pF Scalaire par lequel multiplier
        @return Le <b>vecteur</b>, que l'on a multipli� par <i>pF</i>
     */
    V3& V3::operator*=(F32 pF)
    {
        x *= pF;
        y *= pF;
        z *= pF;

        return *this;
    }


    /*! @param pV Second op�rande du produit vectoriel
        @return Le <b>vecteur</b> ^ <i>pV</i>
      */
    V3& V3::operator^=(const V3& pV)
    {
        V3  lT = *this;

        x = lT.y * pV.z - lT.z * pV.y;
        y = lT.z * pV.x - lT.x * pV.z;
        z = lT.x * pV.y - lT.y * pV.x;

        return *this;
    }


    /*! @param pF Scalaire par lequel "diviser"
        @return Le <b>vecteur</b>, que l'on a multipli� par 1.0F / <i>pF</i>
     */
    V3& V3::operator/=(F32 pF)
    {
        ASSERT_EX(pF != k0F, kDivideByZero, return *this;)

        x /= pF;
        y /= pF;
        z /= pF;

        return *this;
    }


//------------------------------------------------------------------------------
//                                     Norme
//------------------------------------------------------------------------------

    /*! Calcule la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |
     */
    F32 V3::norm() const
    {
        return F32(sqrtf(sqrNorm()));
    }


    /*! Calcule le carr� de la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |�
     */
    F32 V3::sqrNorm() const
    {
        return (*this) * (*this);
    }


    /*! @return Le <b>vecteur</b> normalis�
        @note Le <b>vecteur</b> n'est pas modifi�
     */
    V3 V3::normalized() const
    {
        return *this / norm();
    }


    /*! @return Le <b>vecteur</b>, apr�s l'avoir normalis�
     */
    void V3::normalize()
    {
        *this /= norm();
    }


    /*! @param pV Vecteur � normaliser
        @return Le <b>vecteur</b>, auquel on a affect� <i>pV</i> normalis�
     */
    V3& V3::normalize(const V3& pV)
    {
        *this = pV.normalized();

        return *this;
    }


    /*! @param pP Point 3D jusqu'auquel on va calculer la distance
        @return La distance entre le <b>point</b> source et <i>pP</i>
     */
    F32 V3::distTo(const V3& pP) const
    {
        return F32(sqrtf(sqrDistTo(pP)));
    }


    /*! @param pP Point3D jusqu'auquel on va calculer le carr� la distance
        @return La distance entre le <b>point</b> source et <i>pP</i>
     */
    F32 V3::sqrDistTo(const V3& pP) const
    {
        return (*this - pP).sqrNorm();
    }


    /*! Calcule la distance du <b>point</b> � une droite.
        @param pA Premier point d�finissant la droite
        @param pB Second point d�finissant la droite
        @return La distance du <b>point</b> � la droite (<i>pA pB</i>)
     */
    F32 V3::distToLine(const V3& pA, const V3& pB) const
    {
        V3  lU = (pB - pA).normalized();
        V3  lAP = *this - pA;

        // lU . lAP == AH avec H = projet� de P sur (AB).
        V3  lAH = (lU * lAP) * lU;
        V3  lH = lU + lAH;

        return (*this - lH).norm();
    }


    /*! Calcule la distance du <b>point</b> � un plan.
        @param pP "Origine" du plan � consid�rer
        @param pN Vecteur normal du plan � consid�rer
        @return La distance du <b>point</b> au plan (<i>pP</i>, <i>pN</i>)
     */
    F32 V3::distToPlane(const V3& pP, const V3& pN) const
    {
        return F32(fabsf(sidedDistToPlane(pP, pN)));
    }


    /*! Calcule la distance sign�e du <b>point</b> � un plan (i.e. n�gative si le <b>point</b> est
        au-dessous du plan, positive s'il est au-dessus).
        @param pP "Origine" du plan � consid�rer
        @param pN Vecteur normal du plan � consid�rer
        @return La distance sign�e du <b>point</b> au plan (<i>pP</i>, <i>pN</i>)
     */
    F32 V3::sidedDistToPlane(const V3& pP, const V3& pN) const
    {
        ASSERT(pN.norm() == k1F, kUnnormalizedVector)

        return pN * (*this - pP);
    }


//------------------------------------------------------------------------------
//                          Tests d'Appartenance
//------------------------------------------------------------------------------

    /*! @param pA Premier point du triangle
        @param pB Deuxi�me point du triangle
        @param pC Troisi�me point du triangle
        @return VRAI si le point d�fini par l'instance est � l'int�rieur du triangle ABC
     */
    bool V3::isInTriangle(const V3& pA, const V3& pB, const V3& pC) const
    {
        // M�thode optimis�e, n'utilisant pas acos, mais des produits vectoriels. 
        // Le principe est simple : soit M, le point repr�sent� par l'instance.
        // Pour chaque segment [P1 P2] du triangle, calcul des produits vectoriels suivants :
        // P3P1 x P3P2 & MP1 x MP2.
        // Si le produit scalaire des 2 vecteurs r�sultants est strictement n�gatif, le point est du
        // mauvais c�t� du segment.
        // S'il est nul, P est sur la droite (P1, P2).
        // S'il est strictement positif, P est du bon c�t� du segment [P1, P2] (i.e: du m�me c�t� que P3).
        // Si, pour chacun des 3 segments, les produits scalaires sont positifs, P est � l'int�rieur du
        // triangle P1P2P3.

        // M == "*this".
        V3  lMB(pB - *this),
            lMC(pC - *this);
        V3  lAB(pB - pA),
            lCA(pA - pC);

        // 1er Sommet : A.
        V3  lABxAC  = lAB ^ (- lCA);
        V3  lMBxMC  = lMB ^    lMC;
        // Produit scalaire (pour l'�valuation paresseuse : d�s que le point est du mauvais c�t� d'un
        // des segments, faux est retourn�).
        if (lABxAC * lMBxMC < k1F)
        {
            return false;
        }

        V3  lMA(pA - *this);
        V3  lBC(pC - pB);

        // 2e sommet : B.
        V3  lBCxBA  = lBC ^ (- lAB);
        V3  lMCxMA  = lMC ^    lMA;
        // Produit scalaire.
        if (lBCxBA * lMCxMA < k1F)
        {
            return false;
        }

        // 3e sommet : C.
        V3  lCAxCB  = lCA ^ (- lBC);
        V3  lMAxMB  = lMA ^    lMB;
        if (lCAxCB * lMAxMB < k1F)
        {
            return false;
        }

        return true;
    }


    /*! @param pOrigin Centre de la sph�re
        @param pRadius Rayon de la sph�re
        @return VRAI si le <b>point</b> est � l'int�rieur de la sph�re de centre <i>pOrigin</i> et de
        rayon <i>pRadius</i>, FAUX sinon
     */
    bool V3::isInSphere(const V3& pOrigin, F32 pRadius) const
    {
      return (distTo(pOrigin) <= pRadius);
    }


    /*! @param pOrigin  Centre de la sph�re
        @param pRadius  Rayon de la sph�re
        @param pE       Pr�cision utilis�e pour le test
        @return VRAI si le <b>point</b> est sur la surface de la sph�re de centre <i>pOrigin</i> et de
        rayon <i>pRadius</i> (estimation � <i>pEpsilon</i> pr�s), FAUX sinon
     */
    bool V3::isOnSphere(const V3& pOrigin, F32 pRadius, F32 pE) const
    {
        return areEqualEpsilon(distTo(pOrigin), pRadius, pE);
    }


//------------------------------------------------------------------------------
//                                  Projections
//------------------------------------------------------------------------------

    /*! @param pA Premi�re extr�mit� du segment
        @param pB Seconde extr�mit� du segment
        @return H, le projet� du <b>point</b> sur le segment [AB]
        @note Si H, le <b>point</b> projet� sur (AB), est en dehors de [AB], le point retourn� est, de A
        ou B, le plus proche de H
     */
    V3 V3::projectOnSegment(const V3& pA, const V3& pB) const
    {
        // M == "*this".
        V3 lAM(*this - pA),
            lAB(pB    - pA);
        
        F32 lNormAB = lAB.norm();
        lAB.normalize();

        // Coordon�e de H sur [AB].
        F32 lAH = lAB * lAM;

        // Contr�le sur les extr�mit�s.
        if (lAH < k0F)
        {
            return pA;
        }
        if (lNormAB < lAH)
        {
            return pB;
        }

        // vAB = vAH.
        lAB *= lAH;

        // H = A "+" lAH.
        return (pA + lAB);
    }


    /*!
     */
    V3 V3::projectOnTriangle(const V3& pA,const V3& pB, const V3& pC) const
    {
        // Projection de "M" sur les c�t�s du triangles.
        V3  lHAB = projectOnSegment(pA, pB),
            lHBC = projectOnSegment(pB, pC),
            lHCA = projectOnSegment(pC, pA);
        
        // Distances de "M" jusqu'� ses projet�s sur les c�t�s du triangles.
        F32 lDistances[3] =
        {
            distTo(lHAB),
            distTo(lHBC),
            distTo(lHCA)
        };

        // Retourne le projet� dont la distance � "M" est la plus petite.
        U32 lIndex = U32(std::min_element(lDistances, lDistances + 3) - lDistances);

        switch(lIndex)
        {
            case 0:     // AB
                return lHAB;
            case 1:     // BC
                return lHBC;
            case 2:     // CA
                return lHCA;
            default:    // ??
                return kNullV3;
        }
    }


//------------------------------------------------------------------------------
//                                   Rotation
//------------------------------------------------------------------------------

    /*! Transforme le <b>vecteur</b> par la rotation repr�sent�e par un quaternion.
        @param pQ Quaternion repr�sentant la rotation par laquelle transformer le <b>vecteur</b>
     */
    void V3::rotate(const Q& pQ)
    {
        Q  lRes = pQ * Q(x, y, z, k1F) * pQ.conjugated();

        x = lRes[0];
        y = lRes[1];
        z = lRes[2];
    }


//------------------------------------------------------------------------------
//                                   Min & Max
//------------------------------------------------------------------------------

    /*! @return L'index de la plus grande coordonn�e (en valeur absolue)
     */
    U32 V3::dominantAxis() const
    {
        F32 lCoords[3] =
        {
            F32(fabsf(x)),
            F32(fabsf(y)),
            F32(fabsf(z))
        };

        return U32(std::max_element(lCoords, lCoords + 3) - lCoords);
    }


    /*! @return La plus petite des coordonn�es du <b>vecteur</b>
     */
    F32 V3::min() const
    {
        const F32*  lCoords = &x;
        return *std::min_element(lCoords, lCoords + 3);
    }


    /*! @return La plus grande des coordonn�es du <b>vecteur</b>
     */
    F32 V3::max() const
    {
        const F32*  lCoords = &x;
        return *std::max_element(lCoords, lCoords + 3);
    }


    /*! @return La plus petite des coordonn�es du <b>vecteur</b> en valeur absolue
     */
    F32 V3::minAbs() const
    {
        F32 lCoords[3] =
        {
            F32(fabsf(x)),
            F32(fabsf(y)),
            F32(fabsf(z))
        };

        return *std::min_element(lCoords, lCoords + 3);
    }


    /*! @return La plus grande des coordonn�es du <b>vecteur</b> en valeur absolue
     */
    F32 V3::maxAbs() const
    {
        return this->operator[](dominantAxis());
    }


    /*! Applique un seuil bas sur les coordonn�es.
        @param pMin Valeur minimale pour les coordonn�es du <b>vecteur</b>
     */
    void V3::setMin(F32 pMin)
    {
        x = std::max(x, pMin);
        y = std::max(y, pMin);
        z = std::max(z, pMin);
    }


    /*! Applique un seuil haut sur les coordonn�es.
        @param pMax Valeur maximale pour les coordonn�es du <b>vecteur</b>
     */
    void V3::setMax(F32 pMax)
    {
        x = std::min(x, pMax);
        y = std::min(y, pMax);
        z = std::min(z, pMax);
    }


    /*! Applique un seuil bas (en valeur absolue) sur les coordonn�es.
        @param pMin Valeur absolue minimale pour les coordonn�es du <b>vecteur</b>
     */
    void V3::setMinAbs(F32 pMin)
    {
        if (k0F < pMin)
        {
            x = std::max(x, pMin);
            y = std::max(y, pMin);
            z = std::max(z, pMin);
        }
        else
        {
            x = std::max(x, - pMin);
            y = std::max(y, - pMin);
            z = std::max(z, - pMin);
        }
    }


    /*! Applique un seuil haut (en valeur absolue) sur les coordonn�es.
        @param pMax Valeur absolue maximale pour les coordonn�es du <b>vecteur</b>
        @note pMax doit �tre positif
     */
    void V3::setMaxAbs(F32 pMax)
    {
        ASSERT_EX(k0F < pMax, kInvalidRange, return;)

        x = std::min(x,   pMax);
        x = std::max(x, - pMax);
        y = std::min(y,   pMax);
        y = std::max(y, - pMax);
        z = std::min(z,   pMax);
        z = std::max(z, - pMax);
    }


//------------------------------------------------------------------------------
//                           Operateurs de Comparaison
//------------------------------------------------------------------------------

    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si les coordonn�es des 2 vecteurs sont identiques, FAUX sinon
     */
    bool operator==(const V3& pL, const V3& pR)
    {
        return ((pL.x == pR.x) && (pL.y == pR.y) && (pL.z == pR.z));
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return FAUX si les coordonn�es des 2 vecteurs sont identiques, VRAI sinon
     */
    bool operator!=(const V3& pL, const V3& pR)
    {
        return !(pL == pR);
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si <i>pL</i> est "inf�rieur" � <i>pR</i> (i.e. ses coordonn�es, de x � z, sont
        strictement inf�rieures - dans cet ordre - � celles de <i>pR</i>)
     */
    bool operator<(const V3& pL, const V3& pR)
    {
        if      (pL.x < pR.x)
        {
            return true;
        }
        else if (pR.x < pL.x)
        {
            return false;
        }
        else
        {
            if      (pL.y < pR.y)
            {
                return true;
            }
            else if (pR.y < pL.y)
            {
                return false;
            }
            else
            {
                return (pL.z < pR.z);
            }
        }
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @param pE Pr�cision avec laquelle comparer les coordonn�es de <i>pL</i> et <i>pR</i>
        @return VRAI si les vecteurs sont identiques, � <i>pE</i> pr�s sur chaque coordonn�e
     */
    bool areEqualEpsilon(const V3& pL, const V3& pR, F32 pE)
    {
        return areEqualEpsilon(pL.x, pR.x, pE) &&
               areEqualEpsilon(pL.y, pR.y, pE) &&
               areEqualEpsilon(pL.z, pR.z, pE);
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @param pE Pr�cision avec laquelle comparer les coordonn�es de <i>pL</i> et <i>pR</i>
     */
    bool areCollinearEpsilon(const V3& pL, const V3& pR, F32 pE)
    {
        V3 lU = pL ^ pR;
    
        return lU.isNullEpsilon(pE);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const V3 operator+(V3 pL, const V3& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const V3 operator-(V3 pL, const V3& pR)
    {
        return pL -= pR;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel multiplier le vecteur
        @return <i>pF</i> * <i>pV</i>
     */
    const V3 operator*(V3 pV, F32 pF)
    {
        return pV *= pF;
    }


    /*! @param pF Scalaire par lequel multiplier
        @param pV Vecteur � "scaler"
        @return <i>pF</i> * <i>pV</i>
     */
    const V3 operator*(F32 pF, const V3& pV)
    {
        return pV * pF;
    }


    /*! @param pL Premier op�rande du produit scalaire
        @param pR Second op�rande du produit scalaire
        @return Le <i>pL</i>.<i>pR</i>
     */
    const F32 operator*(const V3& pL, const V3& pR)
    {
        return pL.x * pR.x + pL.y * pR.y + pL.z * pR.z;
    }


    /*! @param pL Premier op�rande du produit vectoriel
        @param pR Second op�rande du produit vectoriel
        @return Le <i>pL</i> ^ <i>pR</i>
      */
    const V3 operator^(V3 pL, const V3& pR)
    {
        return pL ^= pR;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel "diviser" le vecteur
        @return (1 / <i>pF</i>) * <i>pV</i>
     */
    const V3 operator/(V3 pV, F32 pF)
    {
        return pV /= pF;
    }


//------------------------------------------------------------------------------
//                          Op�rateurs Trigonom�triques
//------------------------------------------------------------------------------

    /*! Calcule le cosinus de l'angle form� par deux vecteurs.
        @param pL Premier vecteur de l'angle
        @param pR Second vecteur de l'angle
        @return Le cosinus de l'angle { pL, pR }
     */
    F32 cos(const V3& pL, const V3& pR)
    {
        F32 lRes = pL.normalized() * pR.normalized();

        if      (lRes < - k1F)
        {
            return - k1F;
        }
        else if (k1F < lRes)
        {
            return k1F;
        }
        else
        {
            return lRes;
        }
    }


    /*! Calcule le sinus de l'angle form� par deux vecteurs.
        @param pL Premier vecteur de l'angle
        @param pR Second vecteur de l'angle
        @return Le sinus de l'angle { pL, pR }
     */
    F32 sin(const V3& pL, const V3& pR)
    {
        V3  lTmp            = pL ^ pR;
        F32 lSqrNormProduct = pL.sqrNorm() * pR.sqrNorm();

        return F32(sqrtf(lTmp.sqrNorm() / lSqrNormProduct));
    }


    /*! Calcule le sinus (sign�) de l'angle form� par deux vecteurs.
        @param pL Premier vecteur de l'angle
        @param pR Second vecteur de l'angle
        @param pU "Verticale" � consid�rer pour l'orientation de l'angle
        @return Le sinus (sign�) de l'angle { pL, pR }
     */
    F32 sin(const V3& pL, const V3& pR, const V3& pU)
    {
        V3  lTmp    = pL.normalized() ^ pR.normalized();
        F32 lN      = lTmp.norm();

        if (k0F < lTmp * pU)
        {
            return lN;
        }
        else
        {
            return -lN;
        }
    }


    /*! @param pL Premier vecteur de l'angle
        @param pR Second vecteur de l'angle
        @return La valeur, en radians, de l'angle { pL, pR }
     */
    F32 angle(const V3& pL, const V3& pR)
    {
        return F32(acosf(cos(pL, pR)));
    }


    /*! @param pL Premier vecteur de l'angle
        @param pR Second vecteur de l'angle
        @param pU "Verticale" � consid�rer pour l'orientation de l'angle
        @return La valeur (sign�e), en radians, de l'angle { pL, pR }
     */
    F32 angle(const V3& pL, const V3& pR, const V3& pU)
    {
        return (k0F <= sin(pL, pR, pU)) ? angle(pL, pR) : - angle(pL, pR);
    }


    /*! Calcule l'angle form� par la projection de 2 vecteurs sur le plan (xOy).
        @param pL Premier vecteur � projeter
        @param pR Second vecteur � projeter
        @return La valeur, en radians, de l'angle form� par les projections de <i>pL</i> et <i>pR</i>
     */
    F32 angleZ(const V3& pL, const V3& pR)
    {
        F32 lN = F32(sqrtf((pL.x * pL.x + pL.y * pL.y) * (pR.x * pR.x + pR.y * pR.y)));

        if (lN < kEpsLow)
        {
            return k0F;
        }

        F32 lDet    = pL.x * pR.y - pR.x * pL.y;
        F32 lDot    = pL.x * pR.x + pR.y * pL.y;

        if (k0F < lDet)
        {
            return F32(acosf(lDot / lN));
        }
        else
        {
            return F32(- acosf(lDot / lN));
        }
    }


//------------------------------------------------------------------------------
//                                   Min & Max
//------------------------------------------------------------------------------

    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return De <i>pL</i> ou <i>pR</i>, le vecteur ayant la plus petite norme
     */
    V3 min(const V3& pL, const V3 & pR)
    {
        if (pL.sqrNorm() < pR.sqrNorm())
        {
            return pL;
        }
        else
        {
            return pR;
        }
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return De <i>pL</i> ou <i>pR</i>, le vecteur ayant la plus petite norme
     */
    V3 max(const V3& pL, const V3 & pR)
    {
        if (pL.sqrNorm() < pR.sqrNorm())
        {
            return pR;
        }
        else
        {
            return pL;
        }
    }


//------------------------------------------------------------------------------
//                         Transformation de Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pV Coordonn�es cart�siennes (x, y, z)
        @return <i>pV</i> converti en coordonn�es sph�riques (rh�, th�ta, phi)
     */
    V3 cartesianToSpheric(const V3& pV)
    {
        F32 lTmp    = pV.x * pV.x + pV.y *pV.y;
        F32 lRho    = F32(sqrtf(lTmp + pV.z * pV.z));

        F32 lSign   = (pV.y < k0F) ? - k1F : k1F;

        F32 lTheta  = (pV.x == k0F) ? lSign * kPiBy2 : F32(atanf(pV.y / pV.x));
        F32 lPhi    = (pV.x == k0F) ? kPiBy2         : F32(atanf(sqrtf(lTmp) / pV.z));

        return V3(lRho, lTheta, lPhi);
    }


    /*! @param pV Coordonn�es sph�riques (rh�, th�ta, phi)
        @return <i>pV</i> converti en coordonn�es cart�siennes (x, y, z)
     */
    V3 sphericToCartesian(const V3& pV)
    {
        F32 lCos    = F32(cosf(pV.y));
        F32 lSin    = F32(sinf(pV.z));

        F32 x       = pV.x * lSin * lCos;
        F32 y       = pV.x * lSin * F32(sinf(pV.y));
        F32 z       = pV.x * lCos;

        return V3(x, y, z);
    }


//------------------------------------------------------------------------------
//                                Autre Op�rateur
//------------------------------------------------------------------------------

    /*! Calcule le milieu d'un segment.
        @param pL Premi�re extr�mit� du segment
        @param pR Seconde extr�mit� du segment
        @return Le milieu de [<i>pL</i>, <i>pR</i>]
     */
    V3 middle(const V3 &pL, const V3 &pR)
    {
        return V3(F32(0.5F * (pL.x + pR.x)), F32(0.5F * (pL.y + pR.y)), F32(0.5F * (pL.z + pR.z)));
    }


//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 3D
//------------------------------------------------------------------------------

    /*! Affichage d'un vecteur 3D.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Vecteur � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const V3& pToPrint)
    {
        pLog << '(' << pToPrint.x << ", " << pToPrint.y << ", " << pToPrint.z << ')';

        return pLog;
    }
}
