/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "V2.hh"

/*! @file GDK/MATH/V2.cc
    @brief M�thodes (non-inline) de la classe MATH::V2.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 23 Mars 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "ERRor/Log.hh"
#include "MEMory/Array.hh"

#include "ErrMsg.hh"
#include "Float.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : vecteur null.
     */
    V2::V2()
        : x(k0F)
        , y(k0F)
    { }


    /*! Construit un vecteur � partir de chacune de ses coordonn�es.
        @param pX coordonn�e X
        @param pY coordonn�e Y
     */    
    V2::V2(F32 pX, F32 pY)
        : x(pX)
        , y(pY)
    { }


    /*! Construit un vecteur � partir d'un tableau de 2 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y]
     */
    V2::V2(const F32 pCoords[2])
        : x(pCoords[0])
        , y(pCoords[1])
    { }


    /*! Construit un vecteur � partir d'un tableau de 2 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y]
     */
    V2::V2(const MEM::Array<F32, 2> pCoords)
        : x(pCoords[0])
        , y(pCoords[1])
    { }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y)
     */
    const F32& V2::operator[](size_t pN) const
    {
        ASSERT_EX(pN < 2, kRangeCheckError, return x;)

        return reinterpret_cast<const F32*>(&this->x)[pN];
    }


    /*! @param pN Indice de la coordonn�e demand�e.
        @return R�f�rence sur coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y)
        @note Une coordonn�e peut �tre modifi�e via cet opr�rateur
     */
    F32& V2::operator[](size_t pN)
    {
        ASSERT_EX(pN < 2, kRangeCheckError, return x;)

        return reinterpret_cast<F32 *>(&this->x)[pN];
    }


//------------------------------------------------------------------------------
//                   Op�rateurs d'Addition et de Soustraction
//------------------------------------------------------------------------------

    /*! @return Le m�me <b>vecteur</b>
     */
    const V2 V2::operator+() const
    {
        return *this;
    }


    /*! @param pV Vecteur � additionner
        @return Le <b>vecteur</b>, auquel on a ajout� <i>pV</i>
     */
    V2& V2::operator+=(const V2& pV)
    {
        x += pV.x;
        y += pV.y;

        return *this;
    }


    /*! @return L'oppos� du <b>vecteur</b>
     */
    const V2 V2::operator-() const
    {
        return V2(- x, - y);
    }


    /*! @param pV Vecteur � soustraire
        @return Le <b>vecteur</b>, auquel on a "soustrait" <i>pV</i> (ou ajout� - <i>pV</i>)
     */
    V2& V2::operator-=(const V2& pV)
    {
        x -= pV.x;
        y -= pV.y;

        return *this;
    }


//------------------------------------------------------------------------------
//                            Produits & "Divisions"
//------------------------------------------------------------------------------

    /*! @param pF Scalaire par lequel multiplier
        @return Le <b>vecteur</b>, que l'on a multipli� par <i>pF</i>
     */
    V2& V2::operator*=(F32 pF)
    {
        x *= pF;
        y *= pF;

        return *this;
    }


    /*! @param pF Scalaire par lequel "diviser"
        @return Le <b>vecteur</b>, que l'on a multipli� par 1.0F / <i>pF</i>
     */
    V2& V2::operator/=(F32 pF)
    {
        ASSERT_EX(pF != k0F, kDivideByZero, return *this;)

        x /= pF;
        y /= pF;

        return *this;
    }


//------------------------------------------------------------------------------
//                                     Norme
//------------------------------------------------------------------------------

    /*! Calcule la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |
     */
    F32 V2::norm() const
    {
        return F32(sqrtf(sqrNorm()));
    }


    /*! Calcule le carr� de la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |�
     */
    F32 V2::sqrNorm() const
    {
        return (*this) * (*this);
    }


    /*! @return Le <b>vecteur</b> normalis�
        @note Le <b>vecteur</b> n'est pas modifi�
     */
    V2 V2::normalized() const
    {
        return *this / norm();
    }


    /*! @return Le <b>vecteur</b>, apr�s l'avoir normalis�
     */
    void V2::normalize()
    {
        *this /= norm();
    }


    /*! @param pV Vecteur � normaliser
        @return Le <b>vecteur</b>, auquel on a affect� <i>pV</i> normalis�
     */
    V2& V2::normalize(const V2& pV)
    {
        *this = pV.normalized();

        return *this;
    }


    /*! @param pP Point 2D jusqu'auquel on va calculer la distance
        @return La distance entre le <b>point</b> source et <i>pP</i>
     */
    F32 V2::distTo(const V2& pP) const
    {
        return F32(sqrtf(sqrDistTo(pP)));
    }


    /*! @param pP Point3D jusqu'auquel on va calculer le carr� la distance
        @return La distance entre le <b>point</b> source et <i>pP</i>
     */
    F32 V2::sqrDistTo(const V2& pP) const
    {
        return (*this - pP).sqrNorm();
    }


//------------------------------------------------------------------------------
//                           Operateurs de Comparaison
//------------------------------------------------------------------------------

    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si les coordonn�es des 2 vecteurs sont identiques, FAUX sinon
     */
    bool operator==(const V2& pL, const V2& pR)
    {
        return ((pL.x == pR.x) && (pL.y == pR.y));
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return FAUX si les coordonn�es des 2 vecteurs sont identiques, VRAI sinon
     */
    bool operator!=(const V2& pL, const V2& pR)
    {
        return !(pL == pR);
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si <i>pL</i> est "inf�rieur" � <i>pR</i> (i.e. ses coordonn�es, de x � y, sont
        strictement inf�rieures - dans cet ordre - � celles de <i>pR</i>)
     */
    bool operator<(const V2& pL, const V2& pR)
    {
        if      (pL.x < pR.x)
        {
            return true;
        }
        else if (pR.x < pL.x)
        {
            return false;
        }
        else
        {
            return (pL.y < pR.y);
        }
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @param pE Pr�cision avec laquelle comparer les coordonn�es de <i>pL</i> et <i>pR</i>
        @return VRAI si les vecteurs sont identiques, � <i>pE</i> pr�s sur chaque coordonn�e
     */
    bool areEqualEpsilon(const V2& pL, const V2& pR, F32 pE)
    {
        return areEqualEpsilon(pL.x, pR.x, pE) && areEqualEpsilon(pL.y, pR.y, pE);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const V2 operator+(V2 pL, const V2& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const V2 operator-(V2 pL, const V2& pR)
    {
        return pL -= pR;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel multiplier le vecteur
        @return <i>pF</i> * <i>pV</i>
     */
    const V2 operator*(V2 pV, F32 pF)
    {
        return pV *= pF;
    }


    /*! @param pF Scalaire par lequel multiplier
        @param pV Vecteur � "scaler"
        @return <i>pF</i> * <i>pV</i>
     */
    const V2 operator*(F32 pF, const V2& pV)
    {
        return pV * pF;
    }


    /*! @param pL Premier op�rande du produit scalaire
        @param pR Second op�rande du produit scalaire
        @return Le <i>pL</i>.<i>pR</i>
     */
    const F32 operator*(const V2& pL, const V2& pR)
    {
        return pL.x * pR.x + pL.y * pR.y;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel "diviser" le vecteur
        @return (1 / <i>pF</i>) * <i>pV</i>
     */
    const V2 operator/(V2 pV, F32 pF)
    {
        return pV /= pF;
    }


//------------------------------------------------------------------------------
//                                Autre Op�rateur
//------------------------------------------------------------------------------

    /*! Calcule le milieu d'un segment.
        @param pL Premi�re extr�mit� du segment
        @param pR Seconde extr�mit� du segment
        @return Le milieu de [<i>pL</i>, <i>pR</i>]
     */
    V2 middle(const V2 &pL, const V2 &pR)
    {
        return V2(F32(0.5F * (pL.x + pR.x)), F32(0.5F * (pL.y + pR.y)));
    }


//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 3D
//------------------------------------------------------------------------------

    /*! Affichage d'un vecteur 3D.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Vecteur � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const MATH::V2& pToPrint)
    {
        pLog << '(' << pToPrint.x << ", " << pToPrint.y << ')';

        return pLog;
    }
}
