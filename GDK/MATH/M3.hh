/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_M3_HH
#define MATH_M3_HH

#include "MATH.hh"

/*! @file GDK/MATH/M3.hh
    @brief En-t�te de la classe MATH::M3.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 1er Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "V3.hh"

namespace MATH
{
    /*! @brief Matrice 3x3.
        @version 1.5
        @ingroup MATH

        @sa M4
     */
    class M3 : public MEM::Auto<M3>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    M3();                                   //!< ... par d�faut.
        explicit    M3(const V3& pV);                       //!< ... (matrice antisym�trique).
                    M3(const V3& pI,
                       const V3& pJ,
                       const V3& pK);                       //!< ... par vecteurs de coordonn�es.
        explicit    M3(const Q& pQ);                        //!< ... par quaternion �quivalent.
        void        init(const V3& pI,
                         const V3& pJ,
                         const V3& pK);                     //!< Nouvelle d�finition des �l�ments.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;            //!< Acc�s index� constant.
        F32&        operator[](size_t pN);                  //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Op�rateurs d'addition et de soustraction
        //@{
        const M3   operator+() const;                       //!< Plus unaire.
        M3&        operator+=(const M3& pM);                //!< Addition puis affectation.
        const M3   operator-() const;                       //!< Moins unaire.
        M3&        operator-=(const M3& pM);                //!< Soustraction puis affectation.
        //@}
        //! @name Produits & "divisions"
        //@{
        M3&        operator*=(const M3& pM);                //!< Produit par une matrice.
        M3&        operator*=(F32 pF);                      //!< Produit par un scalaire.
        M3&        operator/=(F32 pF);                      //!< "Division" par un scalaire.
        //@}
        //! @name M�thodes propres aux matrices.
        //@{
        void        invert();                               //!< Inversion.
        void        transpose();                            //!< Transposition.
        F32         trace() const;                          //!< Trace.
        F32         determinant();                          //!< D�terminant.
        void        normalize();                            //!< Normalisation.
        void        loadIdentity();                         //!< Mise � l'identit�.
        //@}
        //! @name Cr�ation de rotations
        //@{
        void        createRotationOnX(F32 pAngle);          //!< Cr�ation d'une rotation selon l'axe des X.
        void        createRotationOnY(F32 pAngle);          //!< Cr�ation d'une rotation selon l'axe des Y.
        void        createRotationOnZ(F32 pAngle);          //!< Cr�ation d'une rotation selon l'axe des Z.
        //@}
        //! \name Tensors
        //@{
        void        loadConeIT();                           //!< Cone.
        void        loadBlockIT(F32 pM, const V3& pS);      //!< Bloc.
        void        loadSphereIT(F32 pM, F32 pR);           //!< Sphere.
        void        loadCylinderIT(F32 pM, F32 pH, F32 pR); //!< Cylindre.
        //@}
        //! @name Coordonn�es
        //@{
        V3  I;                                              //!< 1�re colonne.
        V3  J;                                              //!< 2e   colonne.
        V3  K;                                              //!< 3e   colonne.
        //@}
    };


    /*! @defgroup MATH_M3_Operators Op�rateurs de matrices 3x3
        @ingroup MATH
     */
    //@{
    //! @name Operateurs d'(in)�galit�
    //@{
    bool        operator==(const M3& pL, const M3& pR);     //!< Egalit�.
    bool        operator!=(const M3& pL, const M3& pR);     //!< In�galit�.
    //@}
    //! @name Op�rateurs arithm�tiques
    //@{
    const M3    operator+(M3 pL, const M3& pR);             //!< Addition.
    const M3    operator-(M3 pL, const M3& pR);             //!< Soustraction.
    const M3    operator*(M3 pM, F32 pF);                   //!< Produit par un scalaire.
    const M3    operator*(F32 pF, const M3& pM);            //!< Produit par un scalaire.
    const M3    operator*(M3 pL, const M3& pR);             //!< Produit de matrices.
    const V3    operator*(const M3& pM, const V3& pV);      //!< Produit par un vecteur.
    const M3    operator/(M3 pM, F32 pF);                   //!< "Division" par un scalaire.
    //@}
    //@}


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    extern const M3 kIdM3;  //!< Matrice identit�.


//------------------------------------------------------------------------------
//                          Affichage d'une Matrice 3x3
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const M3& pToPrint); //!< Ecriture d'une matrice 3x3.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "M3.inl"

#endif  // De MATH_M3_HH
