/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_M4_HH
#define MATH_M4_HH

#include "MATH.hh"

/*! @file GDK/MATH/M4.hh
    @brief En-t�te de la classe MATH::M4.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 1er Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "V4.hh"

namespace MATH
{
    /*! @brief Matrice 4x4.
        @version 1.5
        @ingroup MATH

        @sa M3
     */
    class M4 : public MEM::Auto<M4>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    M4();                           //!< ... par d�faut.
                    M4(const V4& pI,
                       const V4& pJ,
                       const V4& pK,
                       const V4& pL);               //!< ... par vecteurs de coordonn�es.
                    M4(const M3& pM,
                       const V3& pT);               //!< ... � partir d'une rotation et d'une position.
                    M4(const M3& pM,
                       const V3& pT,
                       const V3& pS);               //!< ... � partir d'une rotation, d'une position et d'une �chelle.
        explicit    M4(const M3& pM);               //!< ... � partir d'une MATH::M3.
        void        init(const V4& pI,
                        const V4& pJ,
                        const V4& pK,
                        const V4& pL);              //!< Nouvelle d�finition des coordonn�es.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;    //!< Acc�s index� constant.
        F32&        operator[](size_t pN);          //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Op�rateurs d'addition et de soustraction
        //@{
        const M4    operator+() const;              //!< Plus unaire.
        M4&         operator+=(const M4& pM);       //!< Addition puis affectation.
        const M4    operator-() const;              //!< Moins unaire.
        M4&         operator-=(const M4& pM);       //!< Soustraction puis affectation.
        //@}
        //! @name Produits & "divisions"
        //@{
        M4&         operator*=(const M4& pM);       //!< Produit par une matrice.
        M4&         operator*=(F32 pF);             //!< Produit par un scalaire.
        M4&         operator/=(F32 pF);             //!< "Division" par un scalaire.
        //@}
        //! @name M�thodes propres aux matrices
        //@{
        void        invert();                       //!< Inversion.
        void        transpose();                    //!< Transposition.
        F32         trace() const;                  //!< Trace.
        F32         determinant();                  //!< D�terminant.
        void        normalize();                    //!< Normalisation.
        void        loadIdentity();                 //!< Mise � l'identit�.
        //@}
        //! @name D�composition
        //@{
        M3          rotation() const;               //!< Matrice de rotation.
        V3          translation() const;            //!< Vecteur translation.
        //@}
        //! @name Coordonn�es
        //@{
        V4  I;                                      //!< 1�re colonne.
        V4  J;                                      //!< 2�me colonne.
        V4  K;                                      //!< 3�me colonne.
        V4  L;                                      //!< 4�me colonne.
        //@}
    };


    /*! @defgroup MATH_M4_Operators Op�rateurs de matrices 4x4
        @ingroup MATH
     */
    //@{
    //! @name Operateurs d'(in)�galit�
    //@{
    bool        operator==(const M4& pL, const M4& pR); //!< Egalit�.
    bool        operator!=(const M4& pL, const M4& pR); //!< In�galit�.
    //@}
    //! @name Op�rateurs arithm�tiques
    //@{
    const M4   operator+(M4 pL, const M4& pR);          //!< Addition.
    const M4   operator-(M4 pL, const M4& pR);          //!< Soustraction.
    const M4   operator*(M4 pM, F32 pF);                //!< Produit par un scalaire.
    const M4   operator*(F32 pF, const M4& pM);         //!< Produit par un scalaire.
    const M4   operator*(M4 pL, const M4& pR);          //!< Produit de matrices.
    const V4   operator*(const M4& pM, const V4& pV);   //!< Produit par un vecteur.
    const M4   operator/(M4 pM, F32 pF);                //!< "Division" par un scalaire.
    //@}
    //@}


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    extern const M4 kIdM4;  //!< Matrice identit�.


//------------------------------------------------------------------------------
//                          Affichage d'une Matrice 4x4
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const M4& pToPrint); //!< Ecriture d'une matrice 4x4.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "M4.inl"

#endif // De MATH_M4_HH
