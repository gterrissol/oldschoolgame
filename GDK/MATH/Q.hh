/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_Q_HH
#define MATH_Q_HH

#include "MATH.hh"

/*! @file GDK/MATH/Q.hh
    @brief En-t�te de la classe MATH::Q.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "Const.hh"

namespace MATH
{
    /*! @brief Quaternion.
        @version 1.5
     */
    class Q : public MEM::Auto<Q>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    Q();                                        //!< ... par d�faut.
                    Q(F32 pX, F32 pY, F32 pZ, F32 pW);          //!< ... par des coordonn�es s�par�es.
        explicit    Q(const F32 pCoords[4]);                    //!< ... � partir d'un tableau int�gr�.
        explicit    Q(const MEM::Array<F32, 4> pCoords);        //!< ... � partir d'un tableau int�gr�.
                    Q(const V3& pAxis, F32 pAngle);             //!< ... par un couple (axe, angle).
                    Q(V3 pSrc, V3 pDst);                        //!< ... d'une rotation d'un vecteur vers un autre.
        explicit    Q(const M3& pM);                            //!< ... par �quivalence � une matrice.
        void        init(F32 pX, F32 pY, F32 pZ, F32 pW);       //!< Nouvelle d�finition des coordonn�es.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;                //!< Acc�s index� constant.
        F32&        operator[](size_t pN);                      //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Addition, soustraction, produits et quotient
        //@{
        const Q     operator+() const;                          //!< Plus unaire.
        Q&          operator+=(const Q& pQ);                    //!< Addition.
        const Q     operator-() const;                          //!< Moins unaire.
        Q&          operator-=(const Q& pQ);                    //!< Soustraction.

        Q&          operator*=(F32 pF);                         //!< Produit par un scalaire.
        Q&          operator*=(const Q& pQ);                    //!< Produit.
        Q&          operator/=(const Q& pQ);                    //!< Quotient.
        //@}
        //! @name M�thodes propres aux quaternions.
        //@{
        void        lerp(const Q& pSrc, const Q& pDst, F32 pF); //!< Interpolation lin�aire.
        void        slerp(const Q& pSrc, Q pDst, F32 pF);       //!< Interpolation sph�rique.
        void        rotationFromZTo(const V3& pV);              //!< Initialisation d'une rotation du vecteur Z vers un autre vecteur.
        V4          getAxisAngle() const;                       //!< R�cup�ration de l'axe et de l'angle.
        F32         norm() const;                               //!< Norme.
        F32         sqrNorm() const;                            //!< Carr� de la norme.
        void        normalize();                                //!< Normalisation.
        Q           normalized() const;                         //!< Normalisation.
        void        invert();                                   //!< Inversion.
        Q           inverted() const;                           //!< Inversion.
        void        conjugate();                                //!< Conjugu�.
        Q           conjugated() const;                         //!< Conjugu�.
        //@}
        //! @name Coordonn�es
        //@{
        F32 x;                                                  //!< Coordonn�e X.
        F32 y;                                                  //!< Coordonn�e Y.
        F32 z;                                                  //!< Coordonn�e Z.
        F32 w;                                                  //!< Coordonn�e W.
        //@}

    private:
        //! \name Conversion
        //@{
        inline      operator V4() const;                        //!< Conversion en V4.
        //@}
    };


    /*! @defgroup MATH_Q_Operators Op�rateurs de quaternions
        @ingroup MATH
     */
    //@{
    //! @name Operateurs d'(in)�galit� et d'ordre
    //@{
    bool        operator==(const Q& pL, const Q& pR);   //!< Egalit�.
    bool        operator!=(const Q& pL, const Q& pR);   //!< In�galit�.
    bool        operator<(const Q& pL, const Q& pR);    //!< 'Less'.
    bool        areEqualEpsilon(const Q& pL, const Q& pR,
                                F32 pE = kEpsilon);     //!< Egalit�, avec pr�cision.
    //@}
    //! @name Op�rateurs arithm�tiques
    //@{
    const Q     operator+(Q pL, const Q& pR);           //!< Addition.
    const Q     operator-(Q pL, const Q& pR);           //!< Soustraction.
    const Q     operator*(Q pL, const Q& pR);           //!< Produit.
    const Q     operator*(Q pQ, F32 pF);                //!< Produit par un scalaire.
    const Q     operator*(F32 pF, const Q& pQ);         //!< Produit par un scalaire.
    const Q     operator/(Q pL, const Q& pR);           //!< Quotient.
    const Q     operator/(Q pQ, F32 pF);                //!< Quotient par un scalaire.
    //@}
    //@}


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    extern const Q  kNullQ;                                     //!< Quaternion Nul.


//------------------------------------------------------------------------------
//                           Affichage d'un Quaternion
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const Q& pToPrint);  //!< Ecriture d'un quaternion.
}



//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Q.inl"

#endif  // De MATH_Q_HH
