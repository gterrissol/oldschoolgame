/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "M4.hh"

/*! @file GDK/MATH/M4.cc
    @brief M�thodes (non-inline) de la classe MATH::M4.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 1er Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cmath>

#include "ERRor/Log.hh"

#include "ErrMsg.hh"
#include "M3.hh"
#include "V3.hh"
#include "V4.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const M4    kIdM4{V4{k1F, k0F, k0F, k0F},
                      V4{k0F, k1F, k0F, k0F},
                      V4{k0F, k0F, k1F, k0F},
                      V4{k0F, k0F, k0F, k1F}};


//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    M4::M4()
        : I()
        , J()
        , K()
        , L()
    { }


    /*! Construit une matrice � partir de chacune de ses colonnes (V4).
        @param pI 1er vecteur colonne
        @param pJ 2e  vecteur colonne
        @param pK 3e  vecteur colonne
        @param pL 4e  vecteur colonne
     */
    M4::M4(const V4& pI, const V4& pJ, const V4& pK, const V4& pL)
        : I(pI)
        , J(pJ)
        , K(pK)
        , L(pL)
    { }


    /*! Construit une matrice de la forme :@code
        | R t |
        | 0 1 | @endcode
        @param pM Matrice <b>R</b>
        @param pT Vecteur <b>t</b>
     */
    M4::M4(const M3& pM, const V3& pT)
        : I(pM.I)
        , J(pM.J)
        , K(pM.K)
        , L(pT)
    {
        I.w = J.w = K.w = k0F;
    }


    /*! Construit une matrice de la forme :@code
        | Rs t |
        | 0  1 | @endcode
        @param pM Matrice <b>R</b>
        @param pT Vecteur <b>t</b>
        @param pS Vecteur <b>s</b>
     */
    M4::M4(const M3& pM, const V3& pT, const V3& pS)
        : I(pM.I)
        , J(pM.J)
        , K(pM.K)
        , L(pT)
    {
        I *= pS.x;
        J *= pS.y;
        K *= pS.z;
        I.w = J.w = K.w = k0F;
    }

    /*! Construit une matrice de rotation, de la forme :@code
        | R 0 |
        | 0 1 | @endcode
        @param pM Matrice <b>R</b>
     */
    M4::M4(const M3& pM)
        : I(pM.I)
        , J(pM.J)
        , K(pM.K)
        , L(kNullV3)
    {
        I.w = J.w = K.w = k0F;
    }


    /*! Etablit de nouvelles colonnes pour la matrice
        @param pI Nouvelle valeur pour la 1�re colonne
        @param pJ Nouvelle valeur pour la 2�me colonne
        @param pK Nouvelle valeur pour la 3�me colonne
        @param pL Nouvelle valeur pour la 4�me colonne
     */
    void M4::init(const V4& pI, const V4& pJ, const V4& pK, const V4& pL)
    {
        I = pI;
        J = pJ;
        K = pK;
        L = pL;
    }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @note Le code n'est pas tr�s propre mais marche bien
     */
    const F32& M4::operator[](size_t pN) const
    {
        ASSERT(pN < 4 * 4, kRangeCheckError)

        switch(pN / 4)
        {
            case 0: return I[pN % 4];
            case 1: return J[pN % 4];
            case 2: return K[pN % 4];
            case 3: return L[pN % 4];
            default:return I[0];
        }
    }


    /*! @note Le code n'est pas tr�s propre mais marche bien
     */
    F32& M4::operator[](size_t pN)
    {
        ASSERT(pN < 4 * 4, kRangeCheckError)

        switch(pN / 4)
        {
            case 0: return I[pN % 4];
            case 1: return J[pN % 4];
            case 2: return K[pN % 4];
            case 3: return L[pN % 4];
            default:return I[0];
        }
    }


//------------------------------------------------------------------------------
//                   Op�rateurs d'Addition et de Soustraction
//------------------------------------------------------------------------------

    /*! @return La m�me <b>matrice</b>
     */
    const M4 M4::operator+() const
    {
        return *this;
    }


    /*! @param pM Matrice � additionner
        @return La <b>matrice</b> � laquelle on a ajout� <i>pM</i>
     */
    M4& M4::operator+=(const M4& pM)
    {
        I += pM.I;
        J += pM.J;
        K += pM.K;
        L += pM.L;

        return *this;
    }


    /*! @return L'oppos� de la <b>matrice</b>
     */
    const M4 M4::operator-() const
    {
        return M4(- I, - J, - K, - L);
    }


    /*! @param pM Matrice � soustraire
        @return La <b>matrice</b>, � laquelle on a "soustrait" <i>pM</i> (ou ajout� - <i>pM</i>)
     */
    M4& M4::operator-=(const M4& pM)
    {
        I -= pM.I;
        J -= pM.J;
        K -= pM.K;
        L -= pM.L;

        return *this;
    }


//------------------------------------------------------------------------------
//                            Produits & "Divisions"
//------------------------------------------------------------------------------

    /*! @param pM Matrice par laquelle multiplier la <b>matrice</b> (� droite)
        @return La <b>matrice</b>, que l'on a multipli�e par <i>pM</i> (� droite)
     */
    M4& M4::operator*=(const M4& pM)
    {
        init(V4(I.x * pM.I.x + J.x * pM.I.y + K.x * pM.I.z + L.x * pM.I.w,
                I.y * pM.I.x + J.y * pM.I.y + K.y * pM.I.z + L.y * pM.I.w,
                I.z * pM.I.x + J.z * pM.I.y + K.z * pM.I.z + L.z * pM.I.w,
                I.w * pM.I.x + J.w * pM.I.y + K.w * pM.I.z + L.w * pM.I.w),
             V4(I.x * pM.J.x + J.x * pM.J.y + K.x * pM.J.z + L.x * pM.J.w,
                I.y * pM.J.x + J.y * pM.J.y + K.y * pM.J.z + L.y * pM.J.w,
                I.z * pM.J.x + J.z * pM.J.y + K.z * pM.J.z + L.z * pM.J.w,
                I.w * pM.J.x + J.w * pM.J.y + K.w * pM.J.z + L.w * pM.J.w),
             V4(I.x * pM.K.x + J.x * pM.K.y + K.x * pM.K.z + L.x * pM.K.w,
                I.y * pM.K.x + J.y * pM.K.y + K.y * pM.K.z + L.y * pM.K.w,
                I.z * pM.K.x + J.z * pM.K.y + K.z * pM.K.z + L.z * pM.K.w,
                I.w * pM.K.x + J.w * pM.K.y + K.w * pM.K.z + L.w * pM.K.w),
             V4(I.x * pM.L.x + J.x * pM.L.y + K.x * pM.L.z + L.x * pM.L.w,
                I.y * pM.L.x + J.y * pM.L.y + K.y * pM.L.z + L.y * pM.L.w,
                I.z * pM.L.x + J.z * pM.L.y + K.z * pM.L.z + L.z * pM.L.w,
                I.w * pM.L.x + J.w * pM.L.y + K.w * pM.L.z + L.w * pM.L.w));

        return *this;

    }


    /*! @param pF Scalaire par lequel multiplier
        @return La <b>matrice</b>, que l'on a multipli�e par <i>pF</i>
     */
    M4& M4::operator*=(F32 pF)
    {
        I *= pF;
        J *= pF;
        K *= pF;
        L *= pF;

        return *this;
    }


    /*! @param pF Scalaire par lequel "diviser"
        @return Le <b>vecteur</b>, que l'on a multipli� par 1.0F / <i>pF</i>
     */
    M4& M4::operator/=(F32 pF)
    {
        ASSERT_EX(pF != k0F, kDivideByZero, return *this;)

        I /= pF;
        J /= pF;
        K /= pF;
        L /= pF;

        return *this;
    }


//------------------------------------------------------------------------------
//                         M�thodes Propres aux Matrices
//------------------------------------------------------------------------------

    /*! Inverse la matrice.
     */
    void M4::invert()
    {
        // Pr�pare les donn�es de la matrice pour un pivot de Gauss.
        F32 lTmpWork[4][8] =
        {
            { I.x, J.x, K.x, L.x, k1F, k0F, k0F, k0F },
            { I.y, J.y, K.y, L.y, k0F, k1F, k0F, k0F },
            { I.z, J.z, K.z, L.z, k0F, k0F, k1F, k0F },
            { I.w, J.w, K.w, L.w, k0F, k0F, k0F, k1F }
        };
        F32*    lRow0 = lTmpWork[0];
        F32*    lRow1 = lTmpWork[1];
        F32*    lRow2 = lTmpWork[2];
        F32*    lRow3 = lTmpWork[3];

        // Choix d'un pivot.
        if (fabsf(lRow2[0]) < fabsf(lRow3[0]))
        {
            std::swap(lRow2, lRow3);
        }
        if (fabsf(lRow1[0]) < fabsf(lRow2[0]))
        {
            std::swap(lRow1, lRow2);
        }
        if (fabsf(lRow0[0]) < fabsf(lRow1[0]))
        {
            std::swap(lRow0, lRow1);
        }
        if (k0F == lRow0[0])
        {
            ASSERT(false, kMatrixInversionFailed)
            return;
        }

        // Premi�re it�ration de l'algorithme.
        {
            F32 lM1 = lRow1[0] / lRow0[0];
            F32 lM2 = lRow2[0] / lRow0[0];
            F32 lM3 = lRow3[0] / lRow0[0];

            F32 lS  = lRow0[1];
            lRow1[1] -= lM1 * lS;
            lRow2[1] -= lM2 * lS;
            lRow3[1] -= lM3 * lS;
                lS  = lRow0[2];
            lRow1[2] -= lM1 * lS;
            lRow2[2] -= lM2 * lS;
            lRow3[2] -= lM3 * lS;
                lS  = lRow0[3];
            lRow1[3] -= lM1 * lS;
            lRow2[3] -= lM2 * lS;
            lRow3[3] -= lM3 * lS;
                lS  = lRow0[4];
            if (lS != k0F)
            {
                lRow1[4] -= lM1 * lS;
                lRow2[4] -= lM2 * lS;
                lRow3[4] -= lM3 * lS;
            }
                lS  = lRow0[5];
            if (lS != k0F)
            {
                lRow1[5] -= lM1 * lS;
                lRow2[5] -= lM2 * lS;
                lRow3[5] -= lM3 * lS;
            }
                lS  = lRow0[6];
            if (lS != k0F)
            {
                lRow1[6] -= lM1 * lS;
                lRow2[6] -= lM2 * lS;
                lRow3[6] -= lM3 * lS;
            }
                lS  = lRow0[7];
            if (lS != k0F)
            {
                lRow1[7] -= lM1 * lS;
                lRow2[7] -= lM2 * lS;
                lRow3[7] -= lM3 * lS;
            }
        }

        // Choix d'un pivot.
        if (fabsf(lRow2[1]) < fabsf(lRow3[1]))
        {
            std::swap(lRow2, lRow3);
        }
        if (fabsf(lRow1[1]) < fabsf(lRow2[1]))
        {
            std::swap(lRow1, lRow2);
        }
        if (k0F == lRow1[1])
        {
            ASSERT(false, kMatrixInversionFailed)
            return;
        }

        // Deuxi�me it�ration de l'algorithme.
        {
            F32 lM2 = lRow2[1] / lRow1[1];
            F32 lM3 = lRow3[1] / lRow1[1];

            F32 lS  = lRow1[2];
            lRow2[2] -= lM2 * lS;
            lRow3[2] -= lM3 * lS;
                lS  = lRow1[3];
            lRow2[3] -= lM2 * lS;
            lRow3[3] -= lM3 * lS;
                lS  = lRow1[4];
            if (lS != k0F)
            {
                lRow2[4] -= lM2 * lS;
                lRow3[4] -= lM3 * lS;
            }
                lS  = lRow1[5];
            if (lS != k0F)
            {
                lRow2[5] -= lM2 * lS;
                lRow3[5] -= lM3 * lS;
            }
                lS  = lRow1[6];
            if (lS != k0F)
            {
                lRow2[6] -= lM2 * lS;
                lRow3[6] -= lM3 * lS;
            }
                lS  = lRow1[7];
            if (lS != k0F)
            {
                lRow2[7] -= lM2 * lS;
                lRow3[7] -= lM3 * lS;
            }
        }

        // Choix d'un pivot.
        if (fabsf(lRow2[2]) < fabsf(lRow3[2]))
        {
            std::swap(lRow2, lRow3);
        }
        if (k0F == lRow2[2])
        {
            ASSERT(false, kMatrixInversionFailed)
            return;
        }

        // Troisi�me it�ration de l'algorithme.
        {
            F32 lM3 = lRow3[2] / lRow2[2];

            lRow3[3] -= lM3 * lRow2[3];
            lRow3[4] -= lM3 * lRow2[4];
            lRow3[5] -= lM3 * lRow2[5];
            lRow3[6] -= lM3 * lRow2[6];
            lRow3[7] -= lM3 * lRow2[7];
        }

        // Dernier contr�le.
        if (k0F == lRow3[3])
        {
            ASSERT(false, kMatrixInversionFailed)
            return;
        }

        // Ligne 3.
        F32 lS = k1F / lRow3[3];
        lRow3[4] *= lS;
        lRow3[5] *= lS;
        lRow3[6] *= lS;
        lRow3[7] *= lS;
        // Ligne 2.
        F32 lM2 = lRow2[3];
            lS  = k1F / lRow2[2];
        lRow2[4] = lS * (lRow2[4] - lRow3[4] * lM2);
        lRow2[5] = lS * (lRow2[5] - lRow3[5] * lM2);
        lRow2[6] = lS * (lRow2[6] - lRow3[6] * lM2);
        lRow2[7] = lS * (lRow2[7] - lRow3[7] * lM2);
        F32 lM1 = lRow1[3];
        lRow1[4] -= lRow3[4] * lM1;
        lRow1[5] -= lRow3[5] * lM1;
        lRow1[6] -= lRow3[6] * lM1;
        lRow1[7] -= lRow3[7] * lM1;
        F32 lM0 = lRow0[3];
        lRow0[4] -= lRow3[4] * lM0;
        lRow0[5] -= lRow3[5] * lM0;
        lRow0[6] -= lRow3[6] * lM0;
        lRow0[7] -= lRow3[7] * lM0;
        // Ligne 1.
            lM1 = lRow1[2];
            lS = k1F / lRow1[1];
        lRow1[4] = lS * (lRow1[4] - lRow2[4] * lM1);
        lRow1[5] = lS * (lRow1[5] - lRow2[5] * lM1);
        lRow1[6] = lS * (lRow1[6] - lRow2[6] * lM1);
        lRow1[7] = lS * (lRow1[7] - lRow2[7] * lM1);
            lM0 = lRow0[2];
        lRow0[4] -= lRow2[4] * lM0;
        lRow0[5] -= lRow2[5] * lM0;
        lRow0[6] -= lRow2[6] * lM0;
        lRow0[7] -= lRow2[7] * lM0;
        // Ligne 0.
            lM0 = lRow0[1];
            lS  = k1F / lRow0[0];
        lRow0[4] = lS * (lRow0[4] - lRow1[4] * lM0);
        lRow0[5] = lS * (lRow0[5] - lRow1[5] * lM0);
        lRow0[6] = lS * (lRow0[6] - lRow1[6] * lM0);
        lRow0[7] = lS * (lRow0[7] - lRow1[7] * lM0);

        init(V4(lRow0[4], lRow1[4], lRow2[4], lRow3[4]),
             V4(lRow0[5], lRow1[5], lRow2[5], lRow3[5]),
             V4(lRow0[6], lRow1[6], lRow2[6], lRow3[6]),
             V4(lRow0[7], lRow1[7], lRow2[7], lRow3[7]));
    }


    /*! Transpose la <b>matrice</b>.
     */
    void M4::transpose()
    {
        init(V4(I.x, J.x, K.x, L.x),
             V4(I.y, J.y, K.y, L.y),
             V4(I.z, J.z, K.z, L.z),
             V4(I.w, J.w, K.w, L.w));
    }


    /*! @return La trace de la <b>matrice</b>
     */
    F32 M4::trace() const
    {
        return I.x + J.y + K.z + L.w;
    }



    /*! @return Le d�terminant de la <b>matrice</b>
     */
    F32 M4::determinant()
    {
        return I.x * M3(V3(J.y, J.z, J.w),
                        V3(K.y, K.z, K.w),
                        V3(L.y, L.z, L.w)).determinant()
             - I.y * M3(V3(J.x, J.z, J.w),
                        V3(K.x, K.z, K.w),
                        V3(L.x, L.z, L.w)).determinant()
             + I.z * M3(V3(J.x, J.y, J.w),
                        V3(K.x, K.y, K.w),
                        V3(L.x, L.y, L.w)).determinant()
             - I.w * M3(V3(J.x, J.y, J.z),
                        V3(K.x, K.y, K.z),
                        V3(L.x, L.y, L.z)).determinant();
    }


    /*! Orthonormalise la <b>matrice</b>.
     */
    void M4::normalize()
    {
        /*I.Normalize();

        // Recalcule K � partir de I et J, puis normalise.
        K = I ^ J;
        K.Normalize();

        // Recalcule J � partir de K et I, puis normalise.
        J = K ^ I;
        J.Normalize();*/
    }


    /*! Charge l'identit� dans la matrice.
     */  
    void M4::loadIdentity()
    {
        init(V4(k1F, k0F, k0F, k0F),
             V4(k0F, k1F, k0F, k0F),
             V4(k0F, k0F, k1F, k0F),
             V4(k0F, k0F, k0F, k1F));
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @param pL Premi�re matrice � comparer
        @param pR Seconde matrice � comparer
        @return VRAI si les �l�ments des 2 matrices sont identiques, FAUX sinon
     */
    bool operator==(const M4& pL, const M4& pR)
    {
        return (pL.I == pR.I) && (pL.J == pR.J) && (pL.K == pR.K) && (pL.L == pR.L);
    }


    /*! @param pL Premi�re matrice � comparer
        @param pR Seconde matrice � comparer
        @return FAUX si les �l�ments des 2 matrices sont identiques, VRAI sinon
     */
    bool operator!=(const M4& pL, const M4& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const M4 operator+(M4 pL, const M4& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const M4 operator-(M4 pL, const M4& pR)
    {
        return pL -= pR;
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel multiplier
        @return <i>pF</i> * la <b>matrice</b>
     */
    const M4 operator*(M4 pM, F32 pF)
    {
        return pM *= pF;
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel multiplier la matrice
        @return <i>pF</i> * <i>pM</i>
     */
    const M4 operator*(F32 pF, const M4& pM)
    {
        return pM * pF;
    }


    /*! @param pL Premier op�rande du produit
        @param pR Second op�rande du produit
        @return <i>pL</i> * <i>pR</i>
     */
    const M4 operator*(M4 pL, const M4& pR)
    {
        return pL *= pR;
    }


    /*! @param pM Matrice � multiplier
        @param pV Vecteur par lequel multiplier (� droite)
        @return La <i>pM</i> . <i>pV</i>
     */
    const V4 operator*(const M4& pM, const V4& pV)
    {
        return V4(pM.I.x * pV.x + pM.J.x * pV.y + pM.K.x * pV.z + pM.L.x * pV.w,
                  pM.I.y * pV.x + pM.J.y * pV.y + pM.K.y * pV.z + pM.L.y * pV.w,
                  pM.I.z * pV.x + pM.J.z * pV.y + pM.K.z * pV.z + pM.L.z * pV.w,
                  pM.I.w * pV.x + pM.J.w * pV.y + pM.K.w * pV.z + pM.L.w * pV.w);
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel "diviser" la matrice
        @return (1 / <i>pF</i>) * <i>pV</i>
     */
    const M4 operator/(M4 pM, F32 pF)
    {
        return pM /= pF;
    }


//------------------------------------------------------------------------------
//                                 D�composition
//------------------------------------------------------------------------------

    /*! @return La composante rotation de la matrice
     */
    M3 M4::rotation() const
    {
        return M3(V3(I), V3(J), V3(K));
    }


    /*! @return La translation port�e par la matrice
     */
    V3 M4::translation() const
    {
        return V3(L);
    }


//------------------------------------------------------------------------------
//                          Affichage d'une Matrice 4x4
//------------------------------------------------------------------------------

    /*! Affichage d'un matrice 4x4.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Matrice � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const M4& pToPrint)
    {
        pLog << "[ " << pToPrint.I << " ]\n";
        pLog << "[ " << pToPrint.J << " ]\n";
        pLog << "[ " << pToPrint.K << " ]\n";
        pLog << "[ " << pToPrint.L << " ]\n";

        return pLog;
    }
}
