/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MATH.hh"

/*! @file GDK/MATH/MATH.cc
    @brief D�finitions diverses du module MATH.
    @author @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "ERRor/ErrMsg.hh"

#include "Float.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kDivideByZero,          "Division par z�ro.",                   "Divide by 0.")
    REGISTER_ERR_MSG(kInvalidRange,          "Intervalle de d�finition incorrect.",  "Invalid range.")
    REGISTER_ERR_MSG(kMatrixInversionFailed, "L'inversion de matrice a �chou�.",     "Matrix inversion failed.")
    REGISTER_ERR_MSG(kRangeCheckError,       "Valeur hors de l'intervale autoris�.", "Range check error.")
    REGISTER_ERR_MSG(kUnnormalizedVector,    "Vector non-normalis�.",                "Unnormalized vector.")


//------------------------------------------------------------------------------
//                            Fonctions El�mentaires
//------------------------------------------------------------------------------

    /*! @param pL Premier nombre � comparer
        @param pR Second nombre � comparer
        @param pE Pr�cision de la comparaison
        @return VRAI si <i>pL</i> == <i>pR</i>, � <i>pE</i> pr�s
     */
    bool areEqualEpsilon(F32 pL, F32 pR, F32 pE)
    {
        if (fabsf(pL - pR) < pE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! @param pF Nombre � comparer � 0
        @param pE Pr�cision de la comparaison
        @return VRAI si <i>pL</i> == 0.0F, � <i>pE</i> pr�s
     */
    bool isNullEpsilon(F32 pF, F32 pE)
    {
        if (fabsf(pF) < pE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! @param pF   Valeur � contraindre dans un intervalle
        @param pMin Borne inf�rieure de l'intervalle
        @param pMax Borne sup�rieure de l'intervalle
        @return La valeur de <i>pF</i>, contrainte dans l'intervalle [<i>pMin</i>, <i>pMax</i>]
     */
    F32 clamp(F32 pF, F32 pMin, F32 pMax)
    {
        ASSERT_EX((pMin < pMax), kInvalidRange, return pF;)

        return ((pMax < pF) ? pMax : ((pF < pMin) ? pMin : pF));
    }


//------------------------------------------------------------------------------
//                                 Trigonom�trie
//------------------------------------------------------------------------------

    /*! @param pD Angle (deg) qu'on veut exprimer en radians
        @return L'angle <i>pD</i> converti en radians
     */
    F32 degToRad(F32 pD)
    {
        return pD * kDegToRad;
    }


    /*! @param pR Angle (rad) qu'on veut exprimer en degr�s
        @return L'angle <i>pR</i> converti en degr�s
     */
    F32 radToDeg(F32 pR)
    {
        return pR * kRadToDeg;
    }
}
