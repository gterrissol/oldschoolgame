/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_V3_HH
#define MATH_V3_HH

#include "MATH.hh"

/*! @file GDK/MATH/V3.hh
    @brief En-t�te de la classe MATH::V3.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "Const.hh"

namespace MATH
{
    /*! @brief Vecteur g�om�trique 3D.
        @version 1.9

        Un simple vecteur g�om�trique 3D. Cette classe (et son "interface") d�finit la plupart des
        op�rateurs requis pour un usage courant.
        @sa V2, V4
     */
    class V3 : public MEM::Auto<V3>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    V3();                                   //!< ... par d�faut.
                    V3(F32 pX, F32 pY, F32 pZ);             //!< ... par des coordonn�es s�par�es.
        explicit    V3(const F32 pCoords[3]);               //!< ... � partir d'un tableau int�gr�.
        explicit    V3(const MEM::Array<F32, 3> pCoords);   //!< ... � partir d'un tableau int�gr�.
        explicit    V3(const V2& pV2);                      //!< ... � partir d'un MATH::V2.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;            //!< Acc�s index� constant.
        F32&        operator[](size_t pN);                  //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Tests
        //@{
        bool        isNull() const;                         //!< Test d'�galit� � 0.
        bool        isNullEpsilon(F32 pE = kEpsilon) const; //!< Test d'�galit� � 0, avec pr�cision.
        bool        isValid() const;                        //!< Test de validit� des coordonn�es.
        bool        isUpFromPlane(const V3& pP,
                                  const V3& pN) const;      //!< Position par rapport � un plan.
        //@}
        //! @name Op�rateurs d'addition et de soustraction
        //@{
        const V3    operator+() const;                      //!< Plus unaire.
        V3&         operator+=(const V3& pV);               //!< Addition.
        const V3    operator-() const;                      //!< Moins unaire.
        V3&         operator-=(const V3& pV);               //!< Soustraction.
        //@}
        //! @name Produits & "divisions"
        //@{
        V3&         operator*=(F32 pF);                     //!< Produit par un scalaire.
        V3&         operator^=(const V3& pV);               //!< Produit vectoriel.
        V3&         operator/=(F32 pF);                     //!< "Division" par un scalaire.
        //@}
        //! @name Norme & Distance
        //@{
        F32         norm() const;                           //!< Norme.
        F32         sqrNorm() const;                        //!< Carr� de la norme.
        V3          normalized() const;                     //!< Vecteur normalis�.
        void        normalize();                            //!< Vecteur normalis�.
        V3&         normalize(const V3& pV);                //!< Normalisation puis affectation.
        F32         distTo(const V3& pP) const;             //!< Distance � un point 2D.
        F32         sqrDistTo(const V3& pP) const;          //!< Carr� de la distance � un point 2D.
        F32         distToLine(const V3& pA,
                               const V3& pB) const;         //!< Distance � un point 2D.
        F32         distToPlane(const V3& pP,
                                const V3& pN) const;        //!< Distance � un plan.
        F32         sidedDistToPlane(const V3& pP,
                                     const V3& pN) const;   //!< 0> : below, 0< : above.
        //@}
        //! @name Tests d'appartenance
        //@{
        bool        isInTriangle(const V3& pA,
                                 const V3& pB,
                                 const V3& pC) const;       //!< ... � un triangle.
        bool        isInSphere(const V3& pOrigin,
                               F32       pRadius) const;    //!< ... � une sph�re.
        bool        isOnSphere(const V3& pOrigin,
                               F32 pRadius,
                               F32 pE = kEpsHigh) const;    //!< ... � la surface d'une sph�re.
        //@}
        //! @name Projections
        //@{
        V3          projectOnSegment(const V3& pA,
                                     const V3& pB) const;   //!< ... sur un segment de droite.

        V3          projectOnTriangle(const V3& pA,
                                      const V3& pB,
                                      const V3& pC) const;  //!< ... sur un triangle.
        //@}
        //! @name Rotation
        //@{
        void        rotate(const Q& pQ);                    //!< Rotation par un quaternion.
        //@}
        //! @name Min & Max
        //@{
        U32         dominantAxis() const;                   //!< Index de la plus grande coordonn�e.
        F32         min() const;                            //!< Plus petite coordonn�e.
        F32         max() const;                            //!< Plus grande coordonn�e.
        F32         minAbs() const;                         //!< Plus petite coordonn�e (en valeur absolue).
        F32         maxAbs() const;                         //!< Plus grande coordonn�e (en valeur absolue).
        void        setMin(F32 pMin);                       //!< Contrainte d'une valeur minimale.
        void        setMax(F32 pMax);                       //!< Contrainte d'une valeur maximale.
        void        setMinAbs(F32 pMin);                    //!< Contrainte d'une valeur minimale (en valeur absolue).
        void        setMaxAbs(F32 pMax);                    //!< Contrainte d'une valeur maximale (en valeur absolue).
        //@}
        //! @name Coordonn�es
        //@{
        F32 x;                                              //!< Coordonn�e X.
        F32 y;                                              //!< Coordonn�e Y.
        F32 z;                                              //!< Coordonn�e Z.
        //@}
    };


    /*! @defgroup MATH_V3_Operators Op�rateurs de vecteur 3D
        @ingroup MATH
     */
    //@{
    //! @name Op�rateurs de comparaison
    //@{
    bool        operator==(const V3& pL, const V3& pR);             //!< Egalit�.
    bool        operator!=(const V3& pL, const V3& pR);             //!< In�galit�.
    bool        operator<(const V3& pL, const V3& pR);              //!< 'Less'.
    bool        areEqualEpsilon(const V3& pL, const V3& pR,
                                F32 pE = kEpsilon);                 //!< Egalit�, avec pr�cision.
    bool        areCollinearEpsilon(const V3& pL, const V3& pR,
                                    F32 pE = kEpsilon);             //!< Collin�arit� de vecteurs, avec pr�cision.
    //@}
    //! @name Op�rateurs arithm�tiques
    //@{
    const V3    operator+(V3 pL, const V3& pR);                     //!< Addition.
    const V3    operator-(V3 pL, const V3& pR);                     //!< Soustraction.
    const V3    operator*(V3 pV, F32 pF);                           //!< Produit par un scalaire.
    const V3    operator*(F32 pF, const V3& pV);                    //!< Produit par un scalaire.
    const F32   operator*(const V3& pL, const V3& pR);              //!< Produit scalaire.
    const V3    operator^(V3 pL, const V3& pR);                     //!< Produit vectoriel.
    const V3    operator/(V3 pV, F32 pF);                           //!< "Division" par un scalaire.
    //@}
    //! @name Op�rateurs trigonom�triques
    //@{
    F32         cos(const V3& pL, const V3& pR);                    //!< Cosinus de l'angle entre 2 vecteurs.
    F32         sin(const V3& pL, const V3& pR);                    //!< Sinus de l'angle entre 2 vecteurs.
    F32         sin(const V3& pL, const V3& pR, const V3& pU);      //!< Sinus sign� de l'angle entre 2 vecteurs.
    F32         angle(const V3& pL, const V3& pR);                  //!< Angle entre 2 vecteurs.
    F32         angle(const V3& pL, const V3& pR, const V3& pU);    //!< Angle sign� entre 2 vecteurs.
    F32         angleZ(const V3& pL, const V3& pR);                 //!< Angle entre 2 vecteurs projet�s sur le plan (xOy).
    //@}
    // @name Min & max
    //@{
    V3          min(const V3& pL, const V3 & pR);                   //!< Plus petit vecteur (en norme).
    V3          max(const V3& pL, const V3 & pR);                   //!< Plus grand vecteur (en norme).
    //@}
    //! @name Transformation de coordonn�es
    //@{
    V3          cartesianToSpheric(const V3& pV);                   //!< ... cart�siennes en sph�riques.
    V3          sphericToCartesian(const V3& pV);                   //!< ... sph�riques en cart�siennes.
    //@}
    //! @name Autre op�rateur
    //@{
    V3          middle(const V3 &pL, const V3 &pR);                 //!< Centre d'un segment.
    //@}
    //@}


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    extern const V3 kNullV3;   //!< Vecteur Nul.


//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 3D
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const V3& pToPrint);     //!< Ecriture d'un vecteur 3D.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "V3.inl"

#endif  // De MATH_V3_HH
