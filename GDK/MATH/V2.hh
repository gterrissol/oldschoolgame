/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_V2_HH
#define MATH_V2_HH

#include "MATH.hh"

/*! @file GDK/MATH/V2.hh
    @brief En-t�te de la classe MATH::V2.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

#include "Const.hh"

namespace MATH
{
    /*! @brief Vecteur g�om�trique 2D.
        @version 1.8

        Un simple vecteur g�om�trique 2D. Cette classe d�finit la plupart des op�rateurs requis pour un
        usage courant.
        @note Aucune m�thode trigonom�trique n'est impl�ment�e
        @sa V3, V4
     */
    class V2 : public MEM::Auto<V2>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    V2();                                   //!< ... par d�faut.
                    V2(F32 pX, F32 pY);                     //!< ... par des coordonn�es s�par�es.
        explicit    V2(const F32 pCoords[2]);               //!< ... � partir d'un tableau int�gr�.
        explicit    V2(const MEM::Array<F32, 2> pCoords);   //!< ... � partir d'un tableau int�gr�.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;            //!< Acc�s index� constant.
        F32&        operator[](size_t pN);                  //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Op�rateurs d'addition et de soustraction
        //@{
        const V2    operator+() const;                      //!< Plus unaire.
        V2&         operator+=(const V2& pV);               //!< Addition.

        const V2    operator-() const;                      //!< Moins unaire.
        V2&         operator-=(const V2& pV);               //!< Soustraction.
        //@}
        //! @name Produits & "divisions"
        //@{
        V2&         operator*=(F32 pF);                     //!< Produit par un scalaire.
        V2&         operator/=(F32 pF);                     //!< "Division" par un scalaire.
        //@}
        //! @name Norme & Distance
        //@{
        F32         norm() const;                           //!< Norme.
        F32         sqrNorm() const;                        //!< Carr� de la norme.
        V2          normalized() const;                     //!< Vecteur normalis�.
        void        normalize();                            //!< Vecteur normalis�.
        V2&         normalize(const V2& pV);                //!< Normalisation puis affectation.
        F32         distTo(const V2& pP) const;             //!< Distance � un point 2D.
        F32         sqrDistTo(const V2& pP) const;          //!< Carr� de la distance � un point 2D.
        //@}
        //! @name Coordonn�es.
        //@{
        F32 x;                                              //!< Coordonn�e X.
        F32 y;                                              //!< Coordonn�e Y.
        //@}
    };


    /*! @defgroup MATH_V2_Operators Op�rateurs de vecteur 2D
        @ingroup MATH
     */
    //@{
    //! @name Op�rateurs de comparaison
    //@{
    bool        operator==(const V2& pL, const V2& pR);     //!< Egalit�.
    bool        operator!=(const V2& pL, const V2& pR);     //!< In�galit�.
    bool        operator<(const V2& pL, const V2& pR);      //!< 'Less'.
    bool        areEqualEpsilon(const V2& pL, const V2& pR,
                                F32 pE = kEpsilon);         //!< Egalit�, avec pr�cision.
    //@}
    //! @name Op�rateurs arithm�tiques
    //@{
    const V2    operator+(V2 pL, const V2& pR);             //!< Addition.
    const V2    operator-(V2 pL, const V2& pR);             //!< Soustraction.
    const V2    operator*(V2 pV, F32 pF);                   //!< Produit par un scalaire.
    const V2    operator*(F32 pF, const V2& pV);            //!< Produit par un scalaire.
    const F32   operator*(const V2& pL, const V2& pR);      //!< Produit scalaire.
    const V2    operator/(V2 pV, F32 pF);                   //!< "Division" par un scalaire.
    //@}
    //! @name Autre op�rateur
    //@{
    V2          middle(const V2 &pL, const V2 &pR);         //!< Milieu d'un segment.
    //@}
    //@}

//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 2D
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const V2& pToPrint); //!< Ecriture d'un vecteur 2D.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "V2.inl"

#endif  // De MATH_V2_HH
