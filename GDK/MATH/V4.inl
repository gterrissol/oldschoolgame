/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MATH/V4.inl
    @brief M�thodes inline de la classe MATH::V4.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Mars 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MATH
{
    // Pour l'instant, aucune m�thode inline.
    // Apr�s profiling, certaines devraient faire leur apparition ici.
}
