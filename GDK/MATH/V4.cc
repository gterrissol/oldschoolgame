/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "V4.hh"

/*! @file GDK/MATH/V4.cc
    @brief M�thodes (non-inline) de la classe MATH::V4.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 30 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "ERRor/Log.hh"
#include "MEMory/Array.hh"

#include "ErrMsg.hh"
#include "Float.hh"
#include "V3.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const V4    kNullV4 = V4{k0F, k0F, k0F, k0F};


//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : vecteur null.
     */
    V4::V4()
        : x(k0F)
        , y(k0F)
        , z(k0F)
        , w(k0F)
    { }


    /*! Construit un vecteur � partir de chacune de ses coordonn�es.
        @param pX coordonn�e X
        @param pY coordonn�e Y
        @param pZ coordonn�e Z
        @param pW coordonn�e W
     */    
    V4::V4(F32 pX, F32 pY, F32 pZ, F32 pW)
        : x(pX)
        , y(pY)
        , z(pZ)
        , w(pW)
    { }


    /*! Construit un vecteur � partir d'un tableau de 4 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    V4::V4(const F32 pCoords[4])
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
        , w(pCoords[3])
    { }


    /*! Construit un vecteur � partir d'un tableau de 4 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    V4::V4(const MEM::Array<F32, 4> pCoords)
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
        , w(pCoords[3])
    { }


    /*! Construit un vecteur � partir d'un MATH::V3. La 4e coordonn�e vaut 1.0F par d�faut (pour des
        coordonn�es homog�nes).
        @param pV3 Vecteur dont les coordonn�es seront utilis�es pour initialiser les 2 premi�res coordonn�es de l'instance
     */
    V4::V4(const V3& pV3)
        : x(pV3.x)
        , y(pV3.y)
        , z(pV3.z)
        , w(k1F)
    { }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z, 3 -> W)
     */
    const F32& V4::operator[](size_t pN) const
    {
        ASSERT_EX(pN < 4, kRangeCheckError, return x;)

        return reinterpret_cast<const F32*>(&this->x)[pN];
    }


    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z, 3 -> W)
        @note Une coordonn�e peut �tre modifi�e via cet opr�rateur
     */
    F32& V4::operator[](size_t pN)
    {
        ASSERT_EX(pN < 4, kRangeCheckError, return x;)

        return reinterpret_cast<F32*>(&this->x)[pN];
    }


//------------------------------------------------------------------------------
//                   Op�rateurs d'Addition et de Soustraction
//------------------------------------------------------------------------------

    /*! @return Le m�me <b>vecteur</b>
     */
    const V4 V4::operator+() const
    {
        return *this;
    }


    /*! @param pV Vecteur � additionner
        @return Le <b>vecteur</b>, auquel on a ajout� <i>pV</i>
     */
    V4& V4::operator+=(const V4& pV)
    {
        x += pV.x;
        y += pV.y;
        z += pV.z;
        w += pV.w;

        return *this;
    }


    /*! @return L'oppos� du <b>vecteur</b>
     */
    const V4 V4::operator-() const
    {
        return V4(- x, - y, - z, - w);
    }


    /*! @param pV Vecteur � soustraire
        @return Le <b>vecteur</b>, auquel on a "soustrait" <i>pV</i> (ou ajout� - <i>pV</i>)
     */
    V4& V4::operator-=(const V4& pV)
    {
        x -= pV.x;
        y -= pV.y;
        z -= pV.z;
        w -= pV.w;

        return *this;
    }


//------------------------------------------------------------------------------
//                            Produits & "Divisions"
//------------------------------------------------------------------------------

    /*! @param pF Scalaire par lequel multiplier
        @return Le <b>vecteur</b>, que l'on a multipli� par <i>pF</i>
     */
    V4& V4::operator*=(F32 pF)
    {
        x *= pF;
        y *= pF;
        z *= pF;
        w *= pF;

        return *this;
    }


    /*! @param pF Scalaire par lequel "diviser"
        @return Le <b>vecteur</b>, que l'on a multipli� par 1.0F / <i>pF</i>
     */
    V4& V4::operator/=(F32 pF)
    {
        ASSERT_EX(pF != k0F, kDivideByZero, return *this;)

        x /= pF;
        y /= pF;
        z /= pF;
        w /= pF;

        return *this;
    }


//------------------------------------------------------------------------------
//                                     Norme
//------------------------------------------------------------------------------

    /*! Calcule la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |
     */
    F32 V4::norm() const
    {
        return F32(sqrtf(sqrNorm()));
    }


    /*! Calcule le carr� de la norme du <b>vecteur</b>.
        @return | <b>vecteur</b> |�
     */
    F32 V4::sqrNorm() const
    {
        return (*this) * (*this);
    }


    /*! @return Le <b>vecteur</b> normalis�
        @note Le <b>vecteur</b> n'est pas modifi�
     */
    V4 V4::normalized() const
    {
        return *this / norm();
    }


    /*! @return Le <b>vecteur</b>, apr�s l'avoir normalis�
     */
    void V4::normalize()
    {
        *this /= norm();
    }


    /*! @param pV Vecteur � normaliser
        @return Le <b>vecteur</b>, auquel on a affect� <i>pV</i> normalis�
     */
    V4& V4::normalize(const V4& pV)
    {
        *this = pV.normalized();

        return *this;
    }


//------------------------------------------------------------------------------
//                                  Conversion
//------------------------------------------------------------------------------

    /*! @return Un V3 dont les coordonn�es (x, y, z) sont les m�mes que le <b>vecteur</b>
     */
    V4::operator V3() const
    {
        return V3(x, y, z);
    }


//------------------------------------------------------------------------------
//                      Operateurs d'(In)Egalit� et d'Ordre
//------------------------------------------------------------------------------


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si les coordonn�es des 2 vecteurs sont identiques, FAUX sinon
     */
    bool operator==(const V4& pL, const V4& pR)
    {
        return ((pL.x == pR.x) && (pL.y == pR.y) && (pL.z == pR.z) && (pL.w == pR.w));
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return FAUX si les coordonn�es des 2 vecteurs sont identiques, VRAI sinon
     */
    bool operator!=(const V4& pL, const V4& pR)
    {
        return !(pL == pR);
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @return VRAI si <i>pL</i> est "inf�rieur" � <i>pR</i> (i.e. ses coordonn�es, de x � w, sont
        strictement inf�rieures - dans cet ordre - � celles de <i>pR</i>)
     */
    bool operator<(const V4& pL, const V4& pR)
    {
        if      (pL.x < pR.x)
        {
            return true;
        }
        else if (pR.x < pL.x)
        {
            return false;
        }
        else
        {
            if      (pL.y < pR.y)
            {
                return true;
            }
            else if (pR.y < pL.y)
            {
                return false;
            }
            else
            {
                if      (pL.z < pR.z)
                {
                    return true;
                }
                else if (pR.z < pL.z)
                {
                    return false;
                }
                else
                {
                    return (pL.w < pR.w);
                }
            }
        }
    }


    /*! @param pL Premier vecteur � comparer
        @param pR Second vecteur � comparer
        @param pE Pr�cision avec laquelle comparer les coordonn�es de <i>pL</i> et <i>pR</i>
        @return VRAI si les vecteurs sont identiques, � <i>pE</i> pr�s sur chaque coordonn�e
     */
    bool areEqualEpsilon(const V4& pL, const V4& pR, F32 pE)
    {
        return areEqualEpsilon(pL.x, pR.x, pE) &&
               areEqualEpsilon(pL.y, pR.y, pE) &&
               areEqualEpsilon(pL.z, pR.z, pE) &&
               areEqualEpsilon(pL.w, pR.w, pE);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const V4 operator+(V4 pL, const V4& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const V4 operator-(V4 pL, const V4& pR)
    {
        return pL -= pR;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel multiplier le vecteur
        @return <i>pF</i> * <i>pV</i>
     */
    const V4 operator*(V4 pV, F32 pF)
    {
        return pV *= pF;
    }


    /*! @param pF Scalaire par lequel multiplier
        @param pV Vecteur � "scaler"
        @return <i>pF</i> * <i>pV</i>
     */
    const V4 operator*(F32 pF, const V4& pV)
    {
        return pV * pF;
    }


    /*! @param pL Premier op�rande du produit scalaire
        @param pR Second op�rande du produit scalaire
        @return Le <i>pL</i>.<i>pR</i>
     */
    const F32 operator*(const V4& pL, const V4& pR)
    {
        return pL.x * pR.x + pL.y * pR.y + pL.z * pR.z + pL.w * pR.w;
    }


    /*! @param pV Vecteur � "scaler"
        @param pF Scalaire par lequel "diviser" le vecteur
        @return (1 / <i>pF</i>) * <i>pV</i>
     */
    const V4 operator/(V4 pV, F32 pF)
    {
        return pV /= pF;
    }


//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 4D
//------------------------------------------------------------------------------

    /*! Affichage d'un vecteur 4D.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Vecteur � afficher
        @return <i>pro_Log</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const V4& pToPrint)
    {
        pLog << '(' << pToPrint.x << ", " << pToPrint.y << ", " << pToPrint.z << ", " << pToPrint.w << ')';

        return pLog;
    }
}
