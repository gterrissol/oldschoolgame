/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_V4_HH
#define MATH_V4_HH

#include "MATH.hh"

/*! @file GDK/MATH/V4.hh
    @brief En-t�te de la classe MATH::V4.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 30 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "Const.hh"

namespace MATH
{
    /*! @brief Vecteur g�om�trique 4D.
        @version 1.8

        Un simple vecteur g�om�trique 4D. Cette classe d�finit la plupart des op�rateurs requis pour un
        usage courant.
        @sa V2, V3
     */
    class V4 : public MEM::Auto<V4>
    {
    public :
        //! @name Constructeurs
        //@{
        explicit    V4();                                   //!< ... par d�faut.
                    V4(F32 pX, F32 pY, F32 pZ, F32 pW);     //!< ... par des coordonn�es s�par�es.
        explicit    V4(const F32 pCoords[4]);               //!< ... � partir d'un tableau int�gr�.
        explicit    V4(const MEM::Array<F32, 4> pCoords);   //!< ... � partir d'un tableau int�gr�.
        explicit    V4(const V3& pV3);                      //!< ... � partir d'un MATH::V3.
        //@}
        //! @name Acc�s direct aux coordonn�es
        //@{
        const F32&  operator[](size_t pN) const;            //!< Acc�s index� constant.
        F32&        operator[](size_t pN);                  //!< Acc�s index� par r�f�rence.
        //@}
        //! @name Op�rateurs d'addition et de soustraction
        //@{
        const V4    operator+() const;                      //!< Plus unaire.
        V4&         operator+=(const V4& pV);               //!< Addition puis affectation.
        const V4    operator-() const;                      //!< Moins unaire.
        V4&         operator-=(const V4& pV);               //!< Soustraction puis affectation.
        //@}
        //! @name Produits & "divisions"
        //@{
        V4&         operator*=(F32 pF);                     //!< Produit par un scalaire.
        V4&         operator/=(F32 pF);                     //!< "Division" par un scalaire.
        //@}
        //! @name Norme & Distance
        //@{
        F32         norm() const;                           //!< Norme.
        F32         sqrNorm() const;                        //!< Carr� de la norme.
        void        normalize();                            //!< Normalisation.
        V4          normalized() const;                     //!< Vecteur normalis�.
        V4&         normalize(const V4& pV);                //!< Normalisation puis affectation.
        //@}
        //! @name Conversion
        //@{
                    operator V3() const;                    //!< Conversion en V3.
        //@}
        //!@name
        //@{
        F32 x;                                              //!< Coordonn�e X.
        F32 y;                                              //!< Coordonn�e Y.
        F32 z;                                              //!< Coordonn�e Z.
        F32 w;                                              //!< Coordonn�e W.
        //@}
    };


    /*! @defgroup MATH_V4_Operators Op�rateurs de vecteur 4D
        @ingroup MATH
     */
    //@{
    //! @name Operateurs d'(in)�galit� et d'ordre
    //@{
    bool        operator==(const V4& pL, const V4& pR);     //!< Egalit�.
    bool        operator!=(const V4& pL, const V4& pR);     //!< In�galit�.
    bool        operator< (const V4& pL, const V4& pR);     //!< 'Less'.
    bool        areEqualEpsilon(const V4& pL, const V4& pR,
                                F32 pE = kEpsilon);         //!< Egalit�, avec pr�cision.
    //@}

    //! @name Op�rateurs arithm�tiques
    //@{
    const V4    operator+(V4 pL, const V4& pR);             //!< Addition.
    const V4    operator-(V4 pL, const V4& pR);             //!< Soustraction.
    const V4    operator*(V4 pV, F32 pF);                   //!< Produit par un scalaire.
    const V4    operator*(F32 pF, const V4& pV);            //!< Produit par un scalaire.
    const F32   operator*(const V4& pL, const V4& pR);      //!< Produit scalaire.
    const V4    operator/(V4 pV, F32 pF);                   //!< "Division" par un scalaire.
    //@}
    //@}


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    extern const V4 kNullV4;   //!< Vecteur Nul.


//------------------------------------------------------------------------------
//                           Affichage d'un Vecteur 4D
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const V4& pToPrint); //!< Ecriture d'un vecteur 4D.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "V4.inl"

#endif  // De MATH_V4_HH
