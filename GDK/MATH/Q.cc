/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Q.hh"

/*! @file GDK/MATH/Q.cc
    @brief M�thodes (non-inline) de la classe MATH::Q.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 21 Mai 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "ERRor/Log.hh"
#include "MEMory/Array.hh"

#include "ErrMsg.hh"
#include "Float.hh"
#include "M3.hh"
#include "V3.hh"
#include "V4.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const Q kNullQ  = Q(k0F, k0F, k0F, k1F);


//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : quaternion null.
     */
    Q::Q()
        : x(k0F)
        , y(k0F)
        , z(k0F)
        , w(k1F)
    { }


    /*! Construit un quaternion � partir de chacune de ses coordonn�es. <br>
        q = x.i + y.j + z.k + w
        @param pX coordonn�e X
        @param pY coordonn�e Y
        @param pZ coordonn�e Z
        @param pW coordonn�e W
     */    
    Q::Q(F32 pX, F32 pY, F32 pZ, F32 pW)
        : x(pX)
        , y(pY)
        , z(pZ)
        , w(pW)
    { }


    /*! Construit un quaternion � partir d'un tableau de 4 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    Q::Q(const F32 pCoords[4])
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
        , w(pCoords[3])
    { }


    /*! Construit un quaternion � partir d'un tableau de 4 coordonn�es.
        @param pCoords Tableau de coodonn�es [X, Y, Z]
     */
    Q::Q(const MEM::Array<F32, 4> pCoords)
        : x(pCoords[0])
        , y(pCoords[1])
        , z(pCoords[2])
        , w(pCoords[3])
    { }


    /*! Cr�e le quaternion �quivalent � une rotation arbitraire.
        @param pAxis  Axe de la rotation
        @param pAngle Angle de la rotation (rad)
     */
    Q::Q(const V3& pAxis, F32 pAngle)
        : x(k0F)
        , y(k0F)
        , z(k0F)
        , w(k1F)
    {
        F32 lSqrNorm = pAxis.sqrNorm();

        if (!isNullEpsilon(lSqrNorm))
        {
            F32 lHalfAngle  = F32(pAngle * 0.5F);
            F32 lSine       = F32(sinf(lHalfAngle));

            init(lSine * pAxis.x, lSine * pAxis.y, lSine * pAxis.z, F32(cosf(lHalfAngle)));

            normalize();
        }
        // Sinon, d�j� initialis� avec le quaternion nul.
    }


    /*! Cr�e le quaternion �quivalent � la rotation transformant un vecteur arbitraire en un autre.
        @param pSrc Vecteur source
        @param pDst Vecteur r�sultat de la transformation par la rotation de <i>pSrc</i>
     */
    Q::Q(V3 pSrc, V3 pDst)
        : x(k0F)
        , y(k0F)
        , z(k0F)
        , w(k1F)
    {
        pSrc.normalize();
        pDst.normalize();

        pDst = (pSrc + pDst) * F32(0.5F);
        pDst.normalize();

        V3  lAxis = pSrc ^ pDst;

        init(lAxis.x, lAxis.y, lAxis.z, pSrc * pDst);
    }


    /*! Cr�e le quaternion �quivalent � une matrice arbitraire.
        @param pM Matrice dont il faut calculer le quaternion �quivalent
     */
    Q::Q(const M3& pM)
        : x(k0F)
        , y(k0F)
        , z(k0F)
        , w(k1F)
    {
        F32 lTrace = pM.trace();

        if (k0F < lTrace)
        {
            F32 lS  = k1F / F32(2.0F * sqrtf(lTrace));

            init((pM.K.y - pM.J.z) * lS,
                 (pM.I.z - pM.K.x) * lS,
                 (pM.J.x - pM.I.y) * lS,
                 k1F / F32(4.0F * lS));
        }
        else
        {
            U32 lI = k0UL;

            if (pM.I.x < pM.J.y)
            {
                lI = U32(1);
            }

            if (pM[lI * lI] < pM.K.z)
            {
                lI = U32(2);
            }

            U32 lJ      = U32((lI + 1) % 3);
            U32 lK      = U32((lJ + 1) % 3);
            F32 lV[3]   = { k0F, k0F, k0F };
            lV[lI] = (pM[lI * lI] - pM[lJ * lJ] - pM[lK * lK] + k1F) * F32(0.5F);

            F32 lBy4VaI = k1F / F32(4.0F * lV[lI]);
            lV[lJ] = (pM[lI * 3 + lJ] + pM[lJ * 3 + lI]) * lBy4VaI;
            lV[lK] = (pM[lI * 3 + lK] + pM[lK * 3 + lI]) * lBy4VaI;

            init(lV[0], lV[1], lV[2], (pM[lJ * 3 + lK] - pM[lK * 3 + lJ]) * lBy4VaI);
        }
    }


   /*! Etablit de nouvelles coordonn�es pour le quaternion
        @param pX Nouvelle valeur pour x
        @param pY Nouvelle valeur pour y
        @param pZ Nouvelle valeur pour z
        @param pW Nouvelle valeur pour w
     */
    void Q::init(F32 pX, F32 pY, F32 pZ, F32 pW)
    {
        x = pX;
        y = pY;
        z = pZ;
        w = pW;
    }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z, 3 -> W)
     */
    const F32& Q::operator[](size_t pN) const
    {
        ASSERT_EX(pN < 4, kRangeCheckError, return x;)

        return reinterpret_cast<const F32*>(&this->x)[pN];
    }


    /*! @param pN Indice de la coordonn�e demand�e.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> X, 1 -> Y, 2 -> Z, 3 -> W)
        @note Une coordonn�e peut �tre modifi�e via cet opr�rateur
     */
    F32& Q::operator[](size_t pN)
    {
        ASSERT_EX(pN < 4, kRangeCheckError, return x;)

        return reinterpret_cast<F32*>(&this->x)[pN];
    }


//------------------------------------------------------------------------------
//                  Addition, Soustraction, Produit & Quotient
//------------------------------------------------------------------------------

    /*! @return Le m�me <b>quaternion</b>
     */
    const Q Q::operator+() const
    {
        return *this;
    }


    /*! @param pQ Quaternion � additionner
        @return Le <b>quaternion</b>, auquel on a ajout� <i>pQ</i>
     */
    Q& Q::operator+=(const Q& pQ)
    {
        init(x + pQ.x, y + pQ.y, z + pQ.z, w + pQ.w);

        return *this;
    }


    /*! @return L'oppos� du <b>quaternion</b>
     */
    const Q Q::operator-() const
    {
        return Q(- x, - y, - z, - w);
    }


    /*! @param pQ Vecteur � soustraire
        @return Le <b>quaternion</b>, auquel on a "soustrait" <i>pQ</i> (ou ajout� - <i>pQ</i>)
     */
    Q& Q::operator-=(const Q& pQ)
    {
        init(x - pQ.x, y - pQ.y, z - pQ.z, w - pQ.w);

        return *this;
    }


    /*! @param pF Scalaire par lequel multiplier le <b>quaternion</b>
        @return Le produit du <b>quaternion</b> par <i>pF</i>
     */
    Q& Q::operator*=(F32 pF)
    {
        init(x * pF, y * pF, z * pF, w * pF);

        return *this;
    }


    /*! @param pQ Quaternion par lequel multiplier
        @return Le produit du <b>quaternion</b> et de <i>pQ</i> (dans cet ordre)
     */
    Q& Q::operator*=(const Q& pQ)
    {
        init(w * pQ.x + x * pQ.w + y * pQ.z - z * pQ.y,
             w * pQ.y + y * pQ.w + z * pQ.x - x * pQ.z,
             w * pQ.z + z * pQ.w + x * pQ.y - y * pQ.x,
             w * pQ.w - x * pQ.x - y * pQ.y - z * pQ.z);

        return *this;
    }


//------------------------------------------------------------------------------
//                       M�thodes Propres aux Quaternions
//------------------------------------------------------------------------------

     /*! Interpole lin�airement un quaternion.
        @param pSrc Quaternion source
        @param pDst Quaternion destination
        @param pF   Param�tre d'interpolation
     */
    void Q::lerp(const Q& pSrc, const Q& pDst, F32 pF)
    {
        V4  lSrc = pSrc;
        V4  lDst = pDst;
        V4  lRes = lSrc + pF * (lDst - lSrc);

        init(lRes.x, lRes.y, lRes.z, lRes.w);
    }


    /*! Interpole sph�riquement un quaternion.
        @param pSrc Quaternion source
        @param pDst Quaternion destination
        @param pF Param�tre d'interpolation
     */
    void Q::slerp(const Q& pSrc, Q pDst, F32 pF)
    {
        // Angle entre les 2 quaternions.
        F32 lCosTheta = V4(pSrc).normalized() * V4(pDst).normalized();

        // Gestion des signes.
        pDst = (lCosTheta < k0F) ? - pDst : pDst;

        if (areEqualEpsilon(lCosTheta, k1F, kEpsilon))
        {
            // Quaternions proches : interpolation sph�rique approxim�e par une interpolation lin�aire.
            lerp(pSrc, pDst, pF);
        }
        else
        {
            F32 lTheta        = F32(acosf(lCosTheta));
            F32 lInvSinTheta  = F32(k1F / sinf(lTheta));

            *this = pSrc * F32(sinf(lTheta * (k1F - pF))) + pDst * F32(sinf(lTheta * pF)) * lInvSinTheta;
        }
    }


    /*! Cr�e le quaternion �quivalent � la rotation transformant le vecteur Z en un vecteur arbitraire.
        @param pV Vecteur r�sultat de la transformation par la rotation de <b>Z</b>
     */
    void Q::rotationFromZTo(const V3& pV)
    {
        *this = Q(V3(k0F, k0F, k1F), pV);
    }


    /*! @return Un vecteur 4 :
        - les 3 premiers �l�ments sont la direction de la rotation repr�sent�e par le <b>quaternion</b>
        - le dernier �l�ment est l'angle (en degr�s) de cette rotation
     */
    V4 Q::getAxisAngle() const
    {
        Q  lNormalized     = normalized();

        F32 lCosHalfAngle   = lNormalized.w;
        F32 lSinHalfAngle   = k1F - lCosHalfAngle * lCosHalfAngle;

        if (isNullEpsilon(lSinHalfAngle))
        {
            return V4(k0F, k0F, k0F, k0F);
        }
        else
        {
            F32 lAngle  = F32(2.0F * atan2f(lSinHalfAngle, lCosHalfAngle));
            F32 lScale = k1F / lSinHalfAngle;

            return V4(lNormalized.x * lScale, lNormalized.y * lScale, lNormalized.z * lScale, lAngle * kRadToDeg);
        }
    }


    /*! Normalise de <b>quaternion</b>
     */
    void Q::normalize()
    {
        F32 lInvNorm = k1F / norm();

        init(x * lInvNorm, y * lInvNorm, z * lInvNorm, w * lInvNorm);
    }


    /*! @return La norme du <b>quaternion</b>
     */
    F32 Q::norm() const
    {
        return F32(sqrtf(sqrNorm()));
    }


    /*! @return Le carr� de la norme du <b>quaternion</b>
     */
    F32 Q::sqrNorm() const
    {
        return F32(x * x + y * y + z * z + w * w);
    }


    /*! @return Le <b>quaternion</b> normalis�
     */
    Q Q::normalized() const
    {
        Q   lQ = *this;

        lQ.normalize();

        return lQ;
    }


    /*! Inverse le <b>quaternion</b>
     */
    void Q::invert()
    {
        *this = inverted();
    }


    /*! @return Le <b>quaternion</b> invers�
     */
    Q Q::inverted() const
    {
        return conjugated() / sqrNorm();
    }


    /*! Affecte au <b>quaternion</b> son conjugu� 
     */
    void Q::conjugate()
    {
        x = -x;
        y = -y;
        z = -z;
    }


    /*! @return Le conjugu� du <b>quaternion</b>
     */
    Q Q::conjugated() const
    {
        return Q(- x, - y, - z, w);
    }


//------------------------------------------------------------------------------
//                                  Conversion
//------------------------------------------------------------------------------

    /*! Convertir le <b>quaternion</b> en MATH::V4
        @return Un V4 dont les coordonn�es sont les m�mes que celles du <b>quaternion</b>
     */
    Q::operator V4() const
    {
        return V4(x, y, z, w);
    }


//------------------------------------------------------------------------------
//                      Operateurs d'(In)Egalit� et d'Ordre
//------------------------------------------------------------------------------


    /*! @param pL Premier quaternion � comparer
        @param pR Second quaternion � comparer
        @return VRAI si les coordonn�es des 2 quaternions sont identiques, FAUX sinon
     */
    bool operator==(const Q& pL, const Q& pR)
    {
        return ((pL.x == pR.x) && (pL.y == pR.y) && (pL.z == pR.z) && (pL.w == pR.w));
    }


    /*! @param pL Premier quaternion � comparer
        @param pR Second quaternion � comparer
        @return FAUX si les coordonn�es des 2 quaternions sont identiques, VRAI sinon
     */
    bool operator!=(const Q& pL, const Q& pR)
    {
        return !(pL == pR);
    }


    /*! @param pL Premier quaternion � comparer
        @param pR Second quaternion � comparer
        @return VRAI si <i>pL</i> est "inf�rieur" � <i>pR</i> (i.e. ses coordonn�es, de x � w, sont
        strictement inf�rieures - dans cet ordre - � celles de <i>pR</i>)
     */
    bool operator<(const Q& pL, const Q& pR)
    {
        if      (pL.x < pR.x)
        {
            return true;
        }
        else if (pR.x < pL.x)
        {
            return false;
        }
        else
        {
            if      (pL.y < pR.y)
            {
                return true;
            }
            else if (pR.y < pL.y)
            {
                return false;
            }
            else
            {
                if      (pL.z < pR.z)
                {
                    return true;
                }
                else if (pR.z < pL.z)
                {
                    return false;
                }
                else
                {
                    return (pL.w < pR.w);
                }
            }
        }
    }


    /*! @param pL Premier quaternion � comparer
        @param pR Second quaternion � comparer
        @param pE Pr�cision avec laquelle comparer les coordonn�es de <i>pL</i> et <i>pR</i>
        @return VRAI si les quaternions sont identiques, � <i>pE</i> pr�s sur chaque coordonn�e
     */
    bool areEqualEpsilon(const Q& pL, const Q& pR, F32 pE)
    {
        return areEqualEpsilon(pL.x, pR.x, pE) &&
               areEqualEpsilon(pL.y, pR.y, pE) &&
               areEqualEpsilon(pL.z, pR.z, pE) &&
               areEqualEpsilon(pL.w, pR.w, pE);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const Q operator+(Q pL, const Q& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const Q operator-(Q pL, const Q& pR)
    {
        return pL -= pR;
    }


    /*! @param pL Premier op�rande du produit scalaire
        @param pR Second op�rande du produit scalaire
        @return Le <i>pL</i>.<i>pR</i>
     */
    const Q operator*(Q pL, const Q& pR)
    {
        return pL *= pR;
    }


    /*! @param pQ Quaternion � "scaler"
        @param pF Scalaire par lequel multiplier le quaternion
        @return <i>pF</i> * <i>pQ</i>
     */
    const Q operator*(Q pQ, F32 pF)
    {
        return pQ *= pF;
    }


    /*! @param pF Scalaire par lequel multiplier
        @param pQ Quaternion � "scaler"
        @return <i>pF</i> * <i>pQ</i>
     */
    const Q operator*(F32 pF, const Q& pQ)
    {
        return pQ * pF;
    }


    /*! @param pL Premier op�rande du quotient
        @param pR Second op�rande du quotient
        @return Le <i>pL</i> / <i>pR</i>
     */
    const Q operator/(Q pL, const Q& pR)
    {
        return pL *= pR.inverted();
    }


    /*! @param pQ Quaternion � "scaler"
        @param pF Scalaire par lequel "diviser" le quaternion
        @return (1 / <i>pF</i>) * <i>pQ</i>
     */
    const Q operator/(Q pQ, F32 pF)
    {
        return pQ *= (k1F / pF);
    }


//------------------------------------------------------------------------------
//                           Affichage d'un Quaternion
//------------------------------------------------------------------------------

    /*! Affichage d'un quaternion.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Quaternion � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const Q& pToPrint)
    {
        pLog << '(' << pToPrint.x << ", " << pToPrint.y << ", " << pToPrint.z << ", " << pToPrint.w << ')';

        return pLog;
    }
}
