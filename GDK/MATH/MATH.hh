/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_MATH_HH
#define MATH_MATH_HH

/*! @file GDK/MATH/MATH.hh
    @brief Pr�-d�clarations du module @ref MATH.
    @author @ref Guillaume_Terrissol
    @date 23 Mars 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MATH
{
    class M3;
    class M4;
    class Q;
    class V2;
    class V3;
    class V4;
}

#endif  // De MATH_MATH_HH
