/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "M3.hh"

/*! @file GDK/MATH/M3.cc
    @brief M�thodes (non-inline) de la classe MATH::M3.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 1er Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "ERRor/Log.hh"

#include "Const.hh"
#include "ErrMsg.hh"
#include "Float.hh"
#include "Q.hh"
#include "V3.hh"

namespace MATH
{
//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const M3    kIdM3{V3{k1F, k0F, k0F},
                      V3{k0F, k1F, k0F},
                      V3{k0F, k0F, k1F}};


//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    M3::M3()
        : I()
        , J()
        , K()
    { }


    /*! Cr�e une matrice antisym�trique � partir de 3 valeurs.
        @code
    |  0       V.z  - V.y|
    |- V.z     0      V.x|
    |  V.y   - V.x    0  |
        @endcode
        @param pV Vecteur des 3 param�tres
     */
    M3::M3(const V3& pV)
        : I(  k0F,    pV.z, - pV.y)
        , J(- pV.z,   k0F,    pV.x)
        , K(  pV.y, - pV.x,   k0F)
    { }


    /*! Construit une matrice � partir de chacune de ses colonnes (V3).
        @param pI 1er vecteur colonne
        @param pJ 2e  vecteur colonne
        @param pK 3e  vecteur colonne
     */
    M3::M3(const V3& pI, const V3& pJ, const V3& pK)
        : I(pI)
        , J(pJ)
        , K(pK)
    { }


    /*! Construit une matrice �quivalente � un quaternion.
        @param pQ Quaternion dont la matrice �quivalente sera cr��e
     */
    M3::M3(const Q& pQ)
        : I()
        , J()
        , K()
    {
        F32 lW2     = pQ.w * pQ.w,
            lX2     = pQ.x * pQ.x,
            lY2     = pQ.y * pQ.y,
            lZ2     = pQ.z * pQ.z,
            lWX     = pQ.w * pQ.x,
            lWY     = pQ.w * pQ.y,
            lWZ     = pQ.w * pQ.z,
            lXY     = pQ.x * pQ.y,
            lXZ     = pQ.x * pQ.z,
            lYZ     = pQ.y * pQ.z,
            lScale  = F32(2.0F / (lW2 + lX2 + lY2 + lZ2));

        I = V3(k1F - lScale * (lY2 + lZ2),       lScale * (lXY + lWZ),       lScale * (lXZ - lWY));
        J = V3(      lScale * (lXY - lWZ), k1F - lScale * (lX2 + lZ2),       lScale * (lYZ + lWX)); 
        K = V3(      lScale * (lXZ + lWY),       lScale * (lYZ - lWX), k1F - lScale * (lX2 - lY2));

        normalize();
    }


   /*! Etablit de nouvelles colonnes pour la matrice
        @param pI Nouvelle valeur pour la 1�re colonne
        @param pJ Nouvelle valeur pour la 2e   colonne
        @param pK Nouvelle valeur pour la 3e   colonne
     */
    void M3::init(const V3& pI, const V3& pJ,const V3& pK)
    {
        I = pI;
        J = pJ;
        K = pK;
    }


//------------------------------------------------------------------------------
//                         Acc�s Direct aux Coordonn�es
//------------------------------------------------------------------------------

    /*! @param pN Indice de l'�l�ment demand�.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> 2 : I; 3 -> 5 : J; 6 -> 8 : K)
     */
    const F32& M3::operator[](size_t pN) const
    {
        ASSERT(pN < 3 * 3, kRangeCheckError)

        switch(pN / 3)
        {
            case 0: return I[pN % 3];
            case 1: return J[pN % 3];
            case 2: return K[pN % 3];
            default:return I[0];
        }
    }


    /*! @param pN Indice de l'�l�ment demand�.
        @return Coordonn�e[<i>pN</i>] (i.e : 0 -> 2 : I; 3 -> 5 : J; 6 -> 8 : K)
     */
    F32& M3::operator[](size_t pN)
    {
        ASSERT(pN < 3 * 3, kRangeCheckError)

        switch(pN / 3)
        {
            case 0: return I[pN % 3];
            case 1: return J[pN % 3];
            case 2: return K[pN % 3];
            default:return I[0];
        }
    }


//------------------------------------------------------------------------------
//                   Op�rateurs d'Addition et de Soustraction
//------------------------------------------------------------------------------

    /*! @return La m�me <b>matrice</b>
     */
    const M3 M3::operator+() const
    {
        return *this;
    }


    /*! @param pM Matrice � additionner
        @return La <b>matrice</b> � laquelle on a ajout� <i>pM</i>
     */
    M3& M3::operator+=(const M3& pM)
    {
        I += pM.I;
        J += pM.J;
        K += pM.K;

        return *this;
    }


    /*! @return L'oppos� de la <b>matrice</b>
     */
    const M3 M3::operator-() const
    {
        return M3(- I, - J, - K);
    }


    /*! @param pM Matrice � soustraire
        @return La <b>matrice</b>, � laquelle on a "soustrait" <i>pM</i> (ou ajout� - <i>pM</i>)
     */
    M3& M3::operator-=(const M3& pM)
    {
        I -= pM.I;
        J -= pM.J;
        K -= pM.K;

        return *this;
    }


//------------------------------------------------------------------------------
//                            Produits & "Divisions"
//------------------------------------------------------------------------------

    /*! @param pM Matrice par laquelle multiplier la <b>matrice</b> (� droite)
        @return La <b>matrice</b>, que l'on a multipli�e par <i>pM</i> (� droite)
     */
    M3& M3::operator*=(const M3& pM)
    {
        init(V3(I.x * pM.I.x + J.x * pM.I.y + K.x * pM.I.z,
                I.y * pM.I.x + J.y * pM.I.y + K.y * pM.I.z,
                I.z * pM.I.x + J.z * pM.I.y + K.z * pM.I.z),
             V3(I.x * pM.J.x + J.x * pM.J.y + K.x * pM.J.z,
                I.y * pM.J.x + J.y * pM.J.y + K.y * pM.J.z,
                I.z * pM.J.x + J.z * pM.J.y + K.z * pM.J.z),
             V3(I.x * pM.K.x + J.x * pM.K.y + K.x * pM.K.z,
                I.y * pM.K.x + J.y * pM.K.y + K.y * pM.K.z,
                I.z * pM.K.x + J.z * pM.K.y + K.z * pM.K.z));

        return *this;
    }


    /*! @param pF Scalaire par lequel multiplier
        @return La <b>matrice</b>, que l'on a multipli�e par <i>pF</i>
     */
    M3& M3::operator*=(F32 pF)
    {
        I *= pF;
        J *= pF;
        K *= pF;

        return *this;
    }


    /*! @param pF Scalaire par lequel "diviser"
        @return Le <b>vecteur</b>, que l'on a multipli� par 1.0F / <i>pF</i>
     */
    M3& M3::operator/=(F32 pF)
    {
        ASSERT_EX(pF != k0F, kDivideByZero, return *this;)

        I /= pF;
        J /= pF;
        K /= pF;

        return *this;
    }


//------------------------------------------------------------------------------
//                         M�thodes Propres aux Matrices
//------------------------------------------------------------------------------

    /*! Inverse la matrice.
     */
    void M3::invert()
    {
        F32 lDet = determinant();

        if (isNullEpsilon(lDet))
        {
            I = J = K = kNullV3;
        }
        else
        {
            init(V3(   J.y * K.z - J.z * K.y,  - (I.y * K.z - I.z * K.y),    I.y * J.z * I.z * J.y),
                 V3(- (J.x * K.z - J.z * K.x),    I.x * K.z - I.z * K.x,  - (I.x * J.z * I.z * J.x)),
                 V3(   J.x * K.y - J.y * K.x,  - (I.x * K.y - I.y * K.x),    I.x * J.y * I.y * J.x));

            *this /= lDet;
        }
    }


    /*! Transpose la <b>matrice</b>.
     */
    void M3::transpose()
    {
        init(V3(I.x, J.x, K.x),
             V3(I.y, J.y, K.y),
             V3(I.z, J.z, K.z));
    }


    /*! @return La trace de la <b>matrice</b>
     */
    F32 M3::trace() const
    {
        return I.x + J.y + K.z;
    }


    /*! @return Le d�terminant de la <b>matrice</b>
     */
    F32 M3::determinant()
    {
        return (I.x * (J.y * K.z - J.z * K.y) -
                J.x * (I.y * K.z - I.z * K.y) +
                K.x * (I.y * J.z - I.z * J.y));
    }


    /*! Orthonormalise la <b>matrice</b>.
     */
    void M3::normalize()
    {
        I.normalize();

        // Recalcule K � partir de I et J, puis normalise.
        K = I ^ J;
        K.normalize();

        // Recalcule J � partir de K et I, puis normalise.
        J = K ^ I;
        J.normalize();
    }


    /*! Charge l'identit� dans la matrice.
     */  
    void M3::loadIdentity()
    {
        I = V3(k1F, k0F, k0F),
        J = V3(k0F, k1F, k0F),
        K = V3(k0F, k0F, k1F);
    }


//------------------------------------------------------------------------------
//                             Cr�ation de Rotations
//------------------------------------------------------------------------------

    /*! (R�)Initialise en tant que matrice de rotation d'angle <i>pAngle</i> selon l'axe des X.
        @param pAngle Angle (en radians) de la rotation � cr�er
     */
    void M3::createRotationOnX(F32 pAngle)
    {
        F32 lCosA = F32(cosf(pAngle));
        F32 lSinA = F32(sinf(pAngle));

        // Rappel : la matrice est organis�e en vecteurs colonnes.
        I = V3(k1F,   k0F,   k0F);
        J = V3(k0F,   lCosA, lSinA);
        K = V3(k0F, - lSinA, lCosA);
    }


    /*! (R�)Initialise en tant que matrice de rotation d'angle <i>pAngle</i> selon l'axe des Y.
        @param pAngle Angle (en radians) de la rotation � cr�er
     */
    void M3::createRotationOnY(F32 pAngle)
    {
        F32 lCosA = F32(cosf(pAngle));
        F32 lSinA = F32(sinf(pAngle));

        // Rappel : la matrice est organis�e en vecteurs colonnes.
         I = V3(  lCosA, k0F, lSinA);
         J = V3(  k0F,   k1F, k0F);
         K = V3(- lSinA, k0F, lCosA);
    }


    /*! (R�)Initialise en tant que matrice de rotation d'angle <i>pAngle</i> selon l'axe des Z.
        @param pAngle Angle (en radians) de la rotation � cr�er
     */
    void M3::createRotationOnZ(F32 pAngle)
    {
        F32 lCosA = F32(cosf(pAngle));
        F32 lSinA = F32(sinf(pAngle));

        // Rappel : la matrice est organis�e en vecteurs colonnes.
        I = V3(  lCosA, lSinA, k0F);
        J = V3(- lSinA, lCosA, k0F);
        K = V3(  k0F,   k0F,   k1F);
    }


//------------------------------------------------------------------------------
//                                    Tensors
//------------------------------------------------------------------------------

    /*! @todo A coder
     */
    void M3::loadConeIT()
    {
        // ...
    }


    /*! @param pM Masse
        @param pS Taille
     */
    void M3::loadBlockIT(F32 pM, const V3& pS)
    {
        F32 lCoef = F32(3.0F / pM),
            lX2   = F32(pS.x * pS.x),
            lY2   = F32(pS.y * pS.y),
            lZ2   = F32(pS.z * pS.z);
        
        I.x = lCoef * (lY2 + lZ2);
        J.y = lCoef * (lZ2 + lX2);
        K.z = lCoef * (lX2 + lY2);
    }


    /*! @param pM Masse
        @param pR Rayon
     */
    void M3::loadSphereIT(F32 pM, F32 pR)
    {
        F32 lCoef = F32(0.4F * pM * pR * pR);   // 2.0 / 5.0 * mass * radius�

        I.x = J.y = K.z = lCoef;
    }


    /*! @param pM Masse
        @param pH Hauteur
        @param pR Rayon
     */
    void M3::loadCylinderIT(F32 pM, F32 pH, F32 pR)
    {
        F32 lH2 = pH * pH,
            lR2 = pR * pR;

        I.x = F32(k1F / (pM * (lR2 * 0.25F + lH2 * k1By12)));
        J.y = F32(k1F / (pM *  lR2 * 0.5F));
        K.z = I.x;
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @param pL Premi�re matrice � comparer
        @param pR Seconde matrice � comparer
        @return VRAI si les �l�ments des 2 matrices sont identiques, FAUX sinon
     */
    bool operator==(const M3& pL, const M3& pR)
    {
        return (pL.I == pR.I) && (pL.J == pR.J) && (pL.K == pR.K);
    }


    /*! @param pL Premi�re matrice � comparer
        @param pR Seconde matrice � comparer
        @return FAUX si les �l�ments des 2 matrices sont identiques, VRAI sinon
     */
    bool operator!=(const M3& pL, const M3& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! @param pL Premier op�rande de la somme
        @param pR Second op�rande de la somme
        @return <i>pL</i> + <i>pR</i>
     */
    const M3 operator+(M3 pL, const M3& pR)
    {
        return pL += pR;
    }


    /*! @param pL Premier op�rande de la diff�rence
        @param pR Second op�rande de la diff�rence
        @return <i>pL</i> - <i>pR</i>
     */
    const M3 operator-(M3 pL, const M3& pR)
    {
        return pL -= pR;
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel multiplier
        @return <i>pF</i> * la <b>matrice</b>
     */
    const M3 operator*(M3 pM, F32 pF)
    {
        return pM *= pF;
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel multiplier la matrice
        @return <i>pF</i> * <i>pM</i>
     */
    const M3 operator*(F32 pF, const M3& pM)
    {
        return pM * pF;
    }


    /*! @param pL Premier op�rande du produit
        @param pR Second op�rande du produit
        @return <i>pL</i> * <i>pR</i>
     */
    const M3 operator*(M3 pL, const M3& pR)
    {
        return pL *= pR;
    }


    /*! @param pM Matrice � multiplier
        @param pV Vecteur par lequel multiplier (� droite)
        @return La <b>matrice</b> * <i>pV</i>
     */
    const V3 operator*(const M3& pM, const V3& pV)
    {
        return V3(pM.I.x * pV.x + pM.J.x * pV.y + pM.K.x * pV.z,
                   pM.I.y * pV.x + pM.J.y * pV.y + pM.K.y * pV.z,
                   pM.I.z * pV.x + pM.J.z * pV.y + pM.K.z * pV.z);
    }


    /*! @param pM Matrice � "scaler"
        @param pF Scalaire par lequel "diviser" la matrice
        @return (1 / <i>pF</i>) * <i>pV</i>
     */
    const M3 operator/(M3 pM, F32 pF)
    {
        return pM /= pF;
    }


//------------------------------------------------------------------------------
//                          Affichage d'une Matrice 3x3
//------------------------------------------------------------------------------

    /*! Affichage d'un matrice 3x3.
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Matrice � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const M3& pToPrint)
    {
        pLog << "[ " << pToPrint.I << " ]\n";
        pLog << "[ " << pToPrint.J << " ]\n";
        pLog << "[ " << pToPrint.K << " ]\n";

        return pLog;
    }
}
