/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_FLOAT_HH
#define MATH_FLOAT_HH

/*! @file GDK/MATH/Float.hh
    @brief Fonctions math�matiques.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Const.hh"

namespace MATH
{
    //! @name Fonctions �l�mmentaires
    //@{
    bool    areEqualEpsilon(F32 pL, F32 pR, F32 pE = kEpsilon); //!< Test d'�galit�, avec pr�cision.
    bool    isNullEpsilon(F32 pF, F32 pE = kEpsilon);           //!< Test de nullit�, avec pr�cision.
    F32     slamp(F32 pF, F32 pMin, F32 pMax);                  //!< Restriction d'une valeur � un intervalle.
    //@}
    //! @name Trigonom�trie
    //@{
    F32     degToRad(F32 pD);                                   //!< Conversion en radians.
    F32     radToDeg(F32 pR);                                   //!< Conversion en degr�s.
    //@}
}

#endif // De MATH_FLOAT_HH
