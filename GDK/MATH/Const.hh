/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_CONST_HH
#define MATH_CONST_HH

/*! @file GDK/MATH/Const.hh
    @brief Constantes math�matiques.
    @author @ref Nicolas_Peri & @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 20 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

    /*! @defgroup MATH_Const Constantes math�matiques
        @ingroup MATH
     */
    //@{
    //! @name Constantes g�n�rales
    //@{
    const F32   k1F         = F32(1.0F);                  //!< 1.0F.
    const F32   kE          = F32(2.71828182846F);        //!< e (nombre d'euler)
    const F32   kFltMin     = F32(3.402823466e-38F);      //!< Plus petit r�el (sur 32 bits).
    const F32   kFltMax     = F32(3.402823466e+38F);      //!< Plus grand r�el (sur 32 bits).
    const F32   kIntMax     = F32(2147483647.0F);         //!< Plus grand entier sign� sur 32bits.
    const F32   kEpsHigh    = F32(0.0001F);               //!< Pr�cision large.
    const F32   kEpsilon    = F32(0.000001F);             //!< Pr�cision standard.
    const F32   kEpsLow     = F32(0.000000001F);          //!< Pr�cision fine.
    //@}    
    //! @name Constantes "inverses"
    //@{
    const F32   k1By65536   = F32(0.152587890625e-4F);  //!< 1 / 65536.
    const F32   k1By256     = F32(0.00390625F);         //!< 1 / 256.
    const F32   k1By128     = F32(0.0078125F);          //!< 1 / 128.
    const F32   k1By64      = F32(0.015625F);           //!< 1 / 64.
    const F32   k1By32      = F32(0.03125F);            //!< 1 / 32.
    const F32   k1By16      = F32(0.0625F);             //!< 1 / 16.
    const F32   k1By12      = F32(0.08333333333F);      //!< 1 / 12.
    const F32   k1By8       = F32(0.125F);              //!< 1 / 8.
    const F32   k1By6       = F32(0.16666666667F);      //!< 1 / 6.
    const F32   k1By3       = F32(0.33333333333F);      //!< 1 / 3.
    //@}
    //! @name Racines carr�es    
    //@{
    const F32   k1BySqrt3   = F32(0.57735026919F);      //!< 1 / sqrt(3).
    const F32   k1BySqrt2   = F32(0.70710678119F);      //!< 1 / sqrt(2).
    const F32   kSqrt3By2   = F32(0.86602540378F);      //!< sqrt(3) / 2.
    const F32   kSqrt2      = F32(1.41421356237F);      //!< sqrt(2).
    const F32   kSqrt3      = F32(1.73205080757F);      //!< sqrt(3).
    //@}
    //! @name Conversion degr� - radian
    //@{
    const F32   kPiBy180    = F32(0.01745329252F);      //!< Pi / 180.
    const F32   kDegToRad   = kPiBy180;                 //!< Conversion en radians.
    const F32   k180ByPi    = F32(57.2957795131F);      //!< 180 / Pi.
    const F32   kRadToDeg   = k180ByPi;                 //!< Conversion en degr�s.
    //@}
    //! @name Pi
    //@{
    const F32   kPi         = F32(3.14159265359F);      //!<   Pi.
    const F32   k2Pi        = F32(6.28318530718F);      //!< 2 Pi.
    const F32   k3PiBy2     = F32(4.71238898038F);      //!< 3 Pi / 2.
    const F32   k3PiBy4     = F32(2.35619449019F);      //!< 3 Pi / 4
    const F32   kPiBy2      = F32(1.57079632977F);      //!<   Pi / 2.
    const F32   kPiBy3      = F32(1.04719755120F);      //!<   Pi / 3.
    const F32   kPiBy4      = F32(0.78539816340F);      //!<   Pi / 4.
    const F32   kPiBy6      = F32(0.52359877560F);      //!<   Pi / 6.
    const F32   kPiBy8      = F32(0.39269908170F);      //!<   Pi / 8.
    const F32   kPiBy12     = F32(0.26179938780F);      //!<   Pi / 12.

    //@}
    //! @name Sinus - Cosinus
    //@{
    const F32   kSin15      = F32(0.25881904510F);      //!< sin 15�
    const F32   kCos15      = F32(0.96592582629F);      //!< cos 15�
    const F32   kSin30      = F32(0.5F);                //!< sin 30�.
    const F32   kCos30      = kSqrt3By2;                //!< cos 30�.
    const F32   kSin45      = k1BySqrt2;                //!< sin 45�.
    const F32   kCos45      = k1BySqrt2;                //!< cos 45�.
    const F32   kSin60      = kSqrt3By2;                //!< sin 60�.
    const F32   kCos60      = F32(0.5F);                //!< cos 60�.
    //@}
    //@}

#endif  // De MATH_CONST_HH
