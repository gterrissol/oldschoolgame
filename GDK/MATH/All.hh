/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MATH_ALL_HH
#define MATH_ALL_HH

/*! @file GDK/MATH/All.hh
    @brief Interface publique du module @ref MATH.
    @author @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 11 Avril 2009
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MATH  //! Espace de noms mathématiques.
{
    /*! @namespace MATH
        @version 0.95
     */

    /*! @defgroup MATH MATH : classes mathématiques
        <b>namespace</b> MATH, MATH_Const, MATH_V2_Operators, MATH_V3_Operators, MATH_V4_Operators,
        MATH_M3_Operators, MATH_M4_Operators & MATH_Q_Operators
     */
}

#include "Float.hh"
#include "V2.hh"
#include "V3.hh"
#include "V4.hh"
#include "M3.hh"
#include "M4.hh"
#include "Q.hh"

#endif  // De MATH_ALL_HH
