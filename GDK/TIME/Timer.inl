/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/TIME/Timer.inl
    @brief M�thodes inline de la classe TIME::Timer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 27 Mars 2002 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Date.hh"

namespace TIME
{
//------------------------------------------------------------------------------
//                        Gestion de la Suspension Locale
//------------------------------------------------------------------------------

    /*! V�rifie si le timer est en activit�.
        @return VRAI si le timer est suspendu (localement ou globalement), FAUX sinon
        @note Un timer est en activit� s'il n'est ni suspendu localement ni suspendu globalement
     */
    inline bool Timer::isSuspended() const
    {
        return (mIsSuspended                ||  // Suspendu localement.
                (isGloballySuspended() &&       // Suspendu globalement.
                (mIndex != maxTimerCount())));  // R�ceptif � la suspension globale,
    }                                           // car cr�� dans les limites autoris�es.


//------------------------------------------------------------------------------
//                       Gestion de la Suspension Globale
//------------------------------------------------------------------------------

    /*! V�rifie si tous les timers sont suspendus globalement.
        @return VRAI si les timers sont suspendus de mani�re globales, FAUX sinon
        @warning Si les timers ne sont pas suspendus globalement, cela ne signifie pas qu'il sont tous
        en activit�.
        Utilisez la m�thode TIME::Timer::bSuspended() individuellement � chaque timer pour savoir si
        celui-ci est en activit�
     */
    inline bool Timer::isGloballySuspended()
    {
        return smIsGloballySuspended;
    }


//------------------------------------------------------------------------------
//                              Limites du Syst�me
//------------------------------------------------------------------------------

    /*! Compte le nombre de timers courants.
        @return Le nombre de timers repertori�s
        @warning Ce nombre n'est pas n�cessairement repr�sentatif de la quantit� totale de timers jamais
        cr��s. En effet, si trop de timers sont cr��s, il ne seront pas compt�s comme repertori�s
     */
    inline U16 Timer::timerCount()
    {
        return smTimerCount;
    }


    /*! R�cup�re le nombre maximal de timers instanciables
        @return Le nombre maximal de timers instanciables
        @note Cette limite est une constante au syst�me et devrait �tre utilis�e pour v�rifier que trop
        de timers ne sont pas cr��s.<br>
        Au del� de cette limite, les timers cr��s ne seront pas d�truits mais leur comportement n'est
        plus garanti.<br>
        Toutefois une production excessive de timers n'alt�re en aucune fa�on le comportement des timers
        d�j� cr�es
     */
    inline U16 Timer::maxTimerCount()
    {
        return U16(eMaxTimers);
    }


//------------------------------------------------------------------------------
//                               Capture de Temps
//------------------------------------------------------------------------------

    /*! @return Le temps absolu, en �s
     */
    inline I64 Timer::absoluteTime()
    {
        return timeInMicroseconds();
    }


    /*! @return Le temps absolu de r�ference, en �s
     */
    inline I64 Timer::referenceTime() const
    {
        return (mParent == nullptr) ? absoluteTime() : mParent->time();
    }


//------------------------------------------------------------------------------
//                          Ajout / Retrait d'un Timer
//------------------------------------------------------------------------------

    /*! Incr�mente le compteur de timers.
        @return La valeur du compteur de timer, <b>avant</b> l'incr�mentation
     */
    inline U16 Timer::oneTimerMore()
    {
        return smTimerCount++;
    }


    /*! D�cr�mente le compteur de timers.
     */
    inline void Timer::oneTimerLess()
    {
        --smTimerCount;
    }
}
