/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TIME_ENUM_HH
#define TIME_ENUM_HH

/*! @file GDK/TIME/Enum.hh
    @brief Enum�ration (et constante) du module TIME.
    @author @ref Guillaume_Terrissol
    @date 12 Avril 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace TIME
{
    extern  const I64   kFromSecondToMicro; //!< Conversion de s en �s.

    enum
    {
        eMaxTimers = 256                    //!< Nombre maximum de timer autoris�s simultan�ment.
    };
}

#endif // De TIME_ENUM_HH
