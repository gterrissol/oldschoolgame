/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/TIME/Date.inl
    @brief M�thodes inline de la classe TIME::Date.
    @author @ref Guillaume_Terrissol
    @date 30 Juillet 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TIME
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut (la date est, par d�faut, la date courante).
     */
    inline Date::Date()
        : mTime(timeInSeconds())
    { }


    /*! Constructeur � partir d'un entier (valeur de temps).
        @param pTime Temps de la date � construire
     */
    inline Date::Date(I32 pTime)
        : mTime(pTime)
    { }


//------------------------------------------------------------------------------
//                                Autres M�thodes
//------------------------------------------------------------------------------

    /*! Acc�s � la valeur de la date.
        @return La valeur de la date
     */
    inline I32 Date::time() const
    {
        return mTime;
    }


    /*! La date prend comme nouvelle valeur la date courante.
     */
    inline void Date::clock()
    {
        mTime = timeInSeconds();
    }


//------------------------------------------------------------------------------
//                           Op�rateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! Test d'�galit�.
        @param pL Premi�re date � comparer
        @param pR Seconde date � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques, FAUX sinon
     */
    inline bool operator==(Date pL, Date pR)
    {
        return (pL.time() == pR.time());
    }


    /*! Test d'in�galit�.
        @param pL Premi�re date � comparer
        @param pR Seconde date � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont distinctes, FAUX sinon
     */
    inline bool operator!=(Date pL, Date pR)
    {
        return !(pL == pR);
    }
}
