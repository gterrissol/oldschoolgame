/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Timer.hh"

/*! @file GDK/TIME/Timer.cc
    @brief M�thodes (non-inline) de la classe TIME::Timer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 27 Mars 2002 - 7 F�vrier 2018
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ErrMsg.hh"

namespace TIME
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur [par d�faut].
        @param pParent Chornom�tre parent : le temps d'i-celui sera utilis� comme r�f�rence (tout comme
        ses pauses)
        @note R�initialise le timer et le d�marre en tant que non suspendu localement.<br>
        Si la limite du nombre de timers est d�pass�e, le bon fonctionnement du timer n'est pas
        assur�.<br>
        Si tous les timers sont suspendus globalement, ce timer d�marre suspendu globalement sauf si ce
        timer d�passe la limite du nombre de timers autoris�s.<br>
     */
    Timer::Timer(Timer* pParent)
        : mTimeAtRestart(absoluteTime())
        , mTimeOffset(0)
        , mParent(pParent)
        , mIndex(0)
        , mIsSuspended(false)
    {
        // V�rifie si le timer est cr�� dans les limites autoris�es.
        if (timerCount() < maxTimerCount() - 1)
        {
            // Ce timer est ajout� dans la liste.
            mIndex = oneTimerMore();
            smTimerList[mIndex] = this;
        }
        else
        {
            // Ce timer est consid�r� comme excessif et son comportement n'est pas garanti.
            mIndex = maxTimerCount();
            LAUNCH_EXCEPTION(kTooManyTimers)
        }
    }


    /*! Destructeur.
        @note Lib�re de la place dans l'espace limit� des timers dans le cas o� ce timer �tait cr�e sans
        exc�s. Pour autant, si un timer excessif existe il ne profitera pas de la place laiss�e par cette
        destruction
     */
    Timer::~Timer()
    {
        // Retire le timer de la liste des timers, s'il n'est pas excessif.
        if (mIndex != maxTimerCount())
        {
            ASSERT_NOTHROW(0 < timerCount(), kInvalidTimerCounter)
            ASSERT_NOTHROW(smTimerList[mIndex] == this, kInvalidTimerReference)

            // Suppresion : �change avec le dernier �l�ment (au besoin)...
            if (mIndex != timerCount() - 1)
            {
                Timer*  lSwap   = smTimerList[timerCount() - 1];
                lSwap->mIndex   = mIndex;
                smTimerList[mIndex] = lSwap;
            }
            // Puis d�pilement.
            oneTimerLess();
        }
    }


//------------------------------------------------------------------------------
//                  Fonctions El�mentaires du Compteur de Temps
//------------------------------------------------------------------------------

    /*! R�initialise le timer.
        @note Si le timer �tait globalement ou localement suspendu, apr�s la r�initialisation il reste
        globalement ou localement suspendu, respectivement
     */
    void Timer::reset()
    {
        mTimeOffset    = k0LL;
        mTimeAtRestart = referenceTime();
    }


    /*! R�cup�re le temps �coul�, en �s.
        @return Le temps �coul� depuis la derni�re r�initialisation (ou depuis la cr�ation si le timer
        n'a jamais �t� r�initialis�)
        @note Le temps calcul� tient compte des �ventuelles suspensions, locales comme globales
     */
    I64 Timer::time() const
    {
        if (isSuspended())
        {
            return mTimeOffset;
        }
        else
        {
            return mTimeOffset + (referenceTime() - mTimeAtRestart);
        }
    }


//------------------------------------------------------------------------------
//                        Gestion de la Suspension Locale
//------------------------------------------------------------------------------

    /*! Suspend la mesure de l'�coulement du temps localement, ie pour ce timer uniquement.<br>
        @note Suspendre localement un timer d�j� suspendu localement n'a aucun effet
     */
    void Timer::suspend()
    {
        if (mIsSuspended)
        {
            return;
        }

        if (!isSuspended())
        {
            mTimeOffset += referenceTime() - mTimeAtRestart;
        }

        mIsSuspended = true;
    }


    /*! Poursuit la mesure de l'�coulement du temps, localement.<br>
        @note Si le timer n'est pas suspendu localement, cette m�thode n'a aucun effet.
        Les timers suspendus globalement le resteront
     */
    void Timer::resume()
    {
        if (!mIsSuspended)
        {
            return;
        }

        mIsSuspended = false;

        if (!isSuspended())
        {
            mTimeAtRestart = referenceTime();
        }
    }


//------------------------------------------------------------------------------
//                       Gestion de la suspension globale
//------------------------------------------------------------------------------

    /*! Suspension de tous les timers, ie globalement.
        @note Suspendre les timers lorsqu'ils sont d�j� globalement suspendus n'a aucun effet
        @warning Les timers cr��s au del� des limites autoris�es ne peuvent pas �tre suspendus
        globalement
     */
    void Timer::globalSuspend()
    {
        if (smIsGloballySuspended)
        {
            return;
        }

        I64 lCurrentAbsoluteTime = absoluteTime();

        for(U16 lT = k0UW; lT < timerCount(); ++lT)
        {
            if (!smTimerList[lT]->isSuspended())
            {
                smTimerList[lT]->mTimeOffset += lCurrentAbsoluteTime - smTimerList[lT]->mTimeAtRestart;
            }
        }

        smIsGloballySuspended = true;
    }


    /*! Reprise globale de tous les timers.
        @note Les timers suspendus localement le resteront
     */
    void Timer::globalResume()
    {
        if (!smIsGloballySuspended)
        {
            return;
        }

        smIsGloballySuspended = false;

        I64 lCurrentAbsoluteTime = absoluteTime();

        for(U16 lT = k0UW; lT < timerCount(); ++lT)
        {
            if (!smTimerList[lT]->isSuspended())
            {
                smTimerList[lT]->mTimeAtRestart = lCurrentAbsoluteTime;
            }
        }
    }


//------------------------------------------------------------------------------
//                              Variables de Classe
//------------------------------------------------------------------------------

    Timer*  Timer::smTimerList[eMaxTimers] = { nullptr };
    U16     Timer::smTimerCount            = k0UW;
    bool    Timer::smIsGloballySuspended   = false;


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var Timer::mTimeAtRestart
        Temps absolu mesur� � la cr�ation du timer.
     */
    /*! @var Timer::mTimeOffset
        Addition de temps � effectuer, qui correspond au temps "suppos�" �coul� entre les appels � 'Suspend' et � 'Resume'.
     */
    /*! @var Timer::mParent
        Chronom�tre parent servant de r�f�rence temporelle � l'instance (temps absolu, pauses,...).
     */
    /*! @var Timer::mIndex
        Pour chaque timer, indice du pointeur dans Timer::smTimerList
     */
    /*! @var Timer::mIsSuspended
        Pour chaque timer, indique l'�tat d'activation (ou, plus exactement, de suspension).
     */
    /*! @var Timer::smTimerCount
        Nombre de timers actuellement instanci�s.
     */
    /*! @var Timer::smTimerList
        Regroupe tous les timers instanci�s via un tableau de pointeurs.
     */
    /*! @var Timer::smIsGloballySuspended
        Indique l'�tat de suspension globale.
     */


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page TIME_Timer_Page Utilisation des Timers
        @version 0.9

        La m�thode TIME::Timer::Time() r�cup�re le <b>temps �coul�</b> depuis la cr�ation ou la derni�re
        r�initialisation du timer.<br>
        <br>
        Tout timer peut �tre <b>r�initialis�</b> gr�ce � la m�thode TIME::Timer::Reset() ind�pendamment
        de tout autre timer.<br>
        <br>
        Chaque timer peut �tre suspendu ind�pendamment de tout autre timer (autre que son parent, si
        d�fini). Ce m�canisme s'appelle la <b>suspension locale</b>.<br>
        Pendant qu'un timer est suspendu, c'est comme s'il faisait un saut dans le temps : imaginez un
        chronom�tre mis en pause pendant quelques instants, puis r�activ� apr�s cette pause, sans avoir
        �t� r�initialis� entre temps.<br>
        Lors de sa cr�ation, un timer n'est pas suspendu localement.<br>
        <br>
        De mani�re analogue, il est possible de suspendre tous les timers avec une seule commande. Ce
        m�canisme s'appelle la <b>suspension globale</b>.<br>
        Les suspensions locale et globale sont d�corr�l�es : suspendre globalement tous les timers ne
        suspend pas localement chaque timer.<br>
        Un timer est consid�r� comme suspendu s'il est suspendu globalement U (<i>ou</i> inclusif)
        localement.<br>
        Si un timer est r�initialis� alors qu'il est suspendu localement ou globalement, apr�s la
        r�initialisation ce timer reste suspendu localement ou globalement, respectivement.<br>
        A la cr�ation d'un timer, celui-ci commence suspendu globalement si tous les timers sont
        actuellement suspendus globalement.<br>
        <br>
        Le syst�me impose une limite sur le nombre de timers � cr�er. Cette limite est constante et
        d�pendante de l'impl�mentation.<br>
        La limite du syst�me peut �tre consult�e via la fonction TIME::Timer::MaxTimerCount() et le
        nombre de timers r�pertori�s peut �tre consult� via la fonction TIME::Timer::TimerCount() afin
        de s'assurer que la cr�ation d'un nouveau timer se fasse en toute s�curit�. Tout timer cr�� au
        del� de cette limite n'est plus garanti de fonctionner correctement; en particulier le m�canisme
        de suspension globale n'est pas assur� pour les timers excessifs. Cependant il est garanti que la
        cr�ation excessive d'un timer n'alt�re en aucune mani�re le bon fonctionnement de timers d�j�
        cr��s.<br>
    */
}
