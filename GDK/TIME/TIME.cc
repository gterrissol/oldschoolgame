/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "TIME.hh"

/*! @file GDK/TIME/TIME.cc
    @brief D�finitions diverses du module @ref TIME.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

#include "Enum.hh"

namespace TIME
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidTimerCounter,   "Compteur de timers invalide.",  "Invalid timer counter.")
    REGISTER_ERR_MSG(kInvalidTimerReference, "R�f�rence sur timer invalide.", "Invalide timer reference.")
    REGISTER_ERR_MSG(kTooManyTimers,         "Trop de timers.",               "Too many timers.")

//------------------------------------------------------------------------------
//                          D�finition d'une Constante
//------------------------------------------------------------------------------

    /*! Multiplier par cette constante permet de convertir des secondes en microsecondes.
     */
    const I64   kFromSecondToMicro  = I64(1000000);
}
