/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TIME_ALL_HH
#define TIME_ALL_HH

/*! @file GDK/TIME/All.hh
    @brief Interface publique du module @ref TIME.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2001 - 30 D�cembre 2007
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TIME   //! Gestion du temps.
{
    /*! @namespace TIME
        @version 0.85

        Ce module fournit quelques classes et fonctions relatives � la gestion du temps.
     */

    /*! @defgroup TIME TIME : Gestion du temps et chronom�tres.
        <b>namespace</b> TIME.
     */
}

#include "Date.hh"
#include "Timer.hh"
#include "MainTimer.hh"

#endif  // De TIME_ALL_HH
