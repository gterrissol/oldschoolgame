/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/TIME/MainTimer.inl
    @brief M�thodes inline de la classe TIME::MainTimer.
    @author @ref Guillaume_Terrissol
    @date 5 Janvier 2003 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TIME
{
//------------------------------------------------------------------------------
//                               Gestion du Temps
//------------------------------------------------------------------------------

    /*! Il est indispensable de conna�tre l'intervalle de temps �coul� entre deux frames cons�cutives
        (notamment pour les animations).<br>
        Cette information est fournie par cette m�thode.
        @return Le temps �coul� entre les d�buts de la frame actuelle, et de la pr�c�dente
     */
    inline I64 MainTimer::delta()
    {
        return mDelta;
    }


    /*! La m�me date (en �s) doit �tre utilis�e par l'ensemble du moteur, pour chaque frame.
        @return La date � utiliser pour la frame actuelle
     */
    inline I64 MainTimer::frameTime() const
    {
        return mTickedTime;
    }


    /*! @return Le temps exact �coul� depuis le  La date � utiliser pour la frame actuelle
     */
    inline I64 MainTimer::getTime() const
    {
        return mTimer.time();
    }
}
