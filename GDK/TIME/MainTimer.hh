/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TIME_MAINTIMER_HH
#define TIME_MAINTIMER_HH

#include "TIME.hh"

/*! @file GDK/TIME/MainTimer.hh
    @brief En-t�te de la classe TIME::MainTimer.
    @author @ref Guillaume_Terrissol
    @date 5 Janvier 2003 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/Singleton.hh"

#include "Timer.hh"

namespace TIME
{
    /*! @brief Timer principal.
        @version 0.36
        @ingroup TIME_Timers

        L'application dispose d'un timer principal, qui permet d'utiliser la m�me base de temps au niveau
        du programme. En effet, la boucle principale du programme appelle la fonction de mise � jour de
        chaque biblioth�que : celles-ci doivent travailler sur le m�me instant.<br>
        Une instance - unique - permettra donc, pour chaque frame, de travailler avec la m�me date (en
        �s), ainsi que de conna�tre l'intervalle de temps �coul� depuis la frame pr�c�dente.
        @note Toute suspension ou r�initialisation doit se faire via les m�thodes statiques de
        TIME::Timer
    */
    class MainTimer : public MEM::Singleton<MainTimer>, public MEM::OnHeap
    {
    public:

                        MainTimer();        //!< Constructeur par d�faut.
        //! @name Gestion du temps
        //@{
                void    tick();             //!< Capture du temps.
        inline  I64     delta();            //!< Intervalle de temps entre 2 frames.
        inline  I64     frameTime() const;  //!< Date de la frame actuelle.
        inline  I64     getTime() const;    //!< Temps continu.
                void    reset();            //!< R�initialisation du timer.


    private:

                void    tick(I64 pDelta);   //!< Avancement manuel du temps.
        //@}
        Timer   mTimer;                     //!< Timer proprement dit.

        //! @name Mesures pour les frames
        //@{
        I64     mTickedTime;                //!< Temps lors de la capture.
        I64     mDelta;                     //!< Intervalle de temps �coul� entre 2 frames.
        //@}
    };


//------------------------------------------------------------------------------
//                    D�finition du "Singleton Time Manager"
//------------------------------------------------------------------------------

#define TimeMgr TIME::MainTimer::get()      //!< Gestionnaire du temps.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "MainTimer.inl"

#endif  // De TIME_MAINTIMER_HH
