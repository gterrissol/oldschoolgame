/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MainTimer.hh"

/*! @file GDK/TIME/MainTimer.cc
    @brief M�thodes (non-inline) de la classe TIME::MainTimer.
    @author @ref Guillaume_Terrissol
    @date 5 Janvier 2003 - 27 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace TIME
{
//------------------------------------------------------------------------------
//                                 Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    MainTimer::MainTimer()
        : mTickedTime(0)
        , mDelta(0)
    { }


//------------------------------------------------------------------------------
//                               Gestion du Temps
//------------------------------------------------------------------------------

    /*! Capture le temps courant : les prochains appels � iTime() retourneront le temps mesur� par cette
        m�thode.
        @note Cette m�thode est pr�vue pour �tre appel�e par le moteur, au d�but de chaque frame, pour
        offrir � tout le moteur une base de temps commune
     */
    void MainTimer::tick()
    {
        I64 lNewTichedTime = getTime();

        // Mise � jour des attributs.
        mDelta      = lNewTichedTime - mTickedTime;
        mTickedTime = lNewTichedTime;
    }


    /*! R�initialise le timer.
        @note A n'utiliser que pour red�marrer le moteur
     */
    void MainTimer::reset()
    {
        // R�initialisation du timer.
        mTimer.reset();

        // Puis des attributs de l'instance.
        mTickedTime = mDelta = k0LL;
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  MainTimer*  MEM::Singleton<MainTimer>::smThat   = nullptr;
#endif
}
