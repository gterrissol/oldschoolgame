/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TIME_TIMER_HH
#define TIME_TIMER_HH

#include "TIME.hh"

/*! @file GDK/TIME/Timer.hh
    @brief En-t�te de la classe TIME::Timer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 27 Mars 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "Enum.hh"

namespace TIME
{
    /*! @defgroup TIME_Timers Chronom�tres
        @ingroup TIME
     */

    /*! @brief Chronom�tre (pr�cis � la �s).
        @version 0.95
        @ingroup TIME_Timers

        Cette classe permet de mesurer l'�coulement du temps de mani�re simple, automatis�e.<br>
        @sa @ref TIME_Timer_Page
        @todo Impl�menter l'�coulement manuel du temps (�ventuellement � partir TIME::MainTimer)
    */
    class Timer : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Timer(Timer* pParent = 0);  //!< Constructeur.
                        ~Timer();                   //!< Destruteur.
        //@}
        //! @name Fonctions �l�mentaires du compteur de temps
        //@{
                void    reset();                    //!< R�initialisation.
                I64     time() const;               //!< Valeur du timer en �s.
        //@}
        //! @name Gestion de la suspension locale
        //@{
                void    suspend();                  //!< Suspension de la mesure.
                void    resume();                   //!< Reprise de la mesure.
        inline  bool    isSuspended() const;        //!< L'instance est-elle suspendue ?
        //@}
        //! @name Gestion de la suspension globale
        //@{
 static         void    globalSuspend();            //!< Suspension de tous les timers.
 static         void    globalResume();             //!< Reprise de tous les timers.
 static inline  bool    isGloballySuspended();      //!< Les timers sont-ils tous suspendus ?
        //@}
        //! @name Limites du syst�me
        //@{
 static inline  U16     timerCount();               //!< Nombre de timers courants.
 static inline  U16     maxTimerCount();            //!< Nombre maximal de timers autoris�s.
        //@}

        //! @name Capture de temps
        //@{
 static inline  I64     absoluteTime();             //!< R�cup�ration du temps absolu en �s.
        inline  I64     referenceTime() const;      //!< R�cup�ration du temps absolu en �s.
        //@}

    private:
        //! @name Ajout / retrait d'un timer
        //@{
 static inline  U16     oneTimerMore();             //!< Compte d'un timer de plus.
 static inline  void    oneTimerLess();             //!< Compte d'un timer de moins.
        //@}
        //! @name Compteur de temps
        //@{
        I64     mTimeAtRestart;                     //!< R�f�rence de temps (absolu) pour ce timer.
        I64     mTimeOffset;                        //!< D�calage temporel d� aux pauses.
        //@}
        //! @name Gestion de la liste des timers
        //@{
 static Timer*  smTimerList[eMaxTimers];            //!< Liste de tous les timers.
 static U16     smTimerCount;                       //!< Nombre de timers.
        Timer*  mParent;                            //!< Chronom�tre parent.
        U16     mIndex;                             //!< Indice du timer.
        //@}
        //! @name Informations de suspension
        //@{
        bool    mIsSuspended;                       //!< Etat suspendu localement � un timer.
 static bool    smIsGloballySuspended;              //!< Etat suspendu globalement � tous les timers.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Timer.inl"

#endif  // De TIME_TIMER_HH
