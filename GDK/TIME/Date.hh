/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TIME_DATE_HH
#define TIME_DATE_HH

#include "TIME.hh"

/*! @file GDK/TIME/Date.hh
    @brief R�cup�ration de la date et du temps courants & en-t�te de la classe TIME::Date.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2001 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace TIME
{
    /*! @name TIME_Time Acc�s au temps
        @ingroup TIME
     */
    //@{
    I32         timeInSeconds();                    //!< Temps courant (format UNIX : en secondes).
    I64         timeInMicroseconds();               //!< Temps courant (en microsecondes).
    const char* date(U32 pTime);                    //!< Conversion d'une date en un format lisible.
    //@}


    /*! @defgroup TIME_Date_Interface Date
        @ingroup TIME
     */

    /*! @brief Date.
        @version 2.0
        @ingroup TIME_Date_Interface

        Cette classe permet de manipuler (tr�s simplement) des dates.
     */
    class Date : public MEM::Auto<Date>
    {
    public:
        //! @name Constructeurs
        //@{
        inline          Date();                     //!< ... par d�faut.
        inline          Date(I32 pTime);            //!< ... � partir d'un entier.
        //@}
        //! @name Autres m�thodes
        //@{
        inline  I32     time() const;               //!< Valeur de la date.
        inline  void    clock();                    //!< Prend la date courante.
        //@}

    private:

        I32 mTime;                                  //!< Date.
    };


    /*! @ingroup TIME_Date_Interface
     */
    //@{
    inline  bool    operator==(Date pL, Date pR);   //!< Test d'�galit�.
    inline  bool    operator!=(Date pL, Date pR);   //!< Test d'in�galit�.
    //@}
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Date.inl"

#endif  // De TIME_DATE_HH
