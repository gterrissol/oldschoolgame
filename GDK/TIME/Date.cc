/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Date.hh"

/*! @file GDK/TIME/Date.cc
    @brief Fonctions (non-inline) de r�cup�ration de la date et du temps courants.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2001 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <ctime>
#if     defined(LINUX)
#   include <sys/time.h>
#elif   defined(MINGW)
#   include <windows.h>
#else
#   error "Aucune fonction syst�me n'est disponible pour impl�menter les timers."
#endif

#include "Enum.hh"

namespace TIME
{
//------------------------------------------------------------------------------
//                                Acc�s au temps
//------------------------------------------------------------------------------

    /*! Appelle la fonction C time()
        @return Le nombre de secondes �coul�s depuis le 1er Janvier 1970, 0h00 (GMT)
     */
    I32 timeInSeconds()
    {
        time_t  lTime;
        time(&lTime);

        return I32(lTime);
    }


    /*! R�cup�ration du temps courant.
        @return Le temps actuel, en microsecondes
     */
    I64 timeInMicroseconds()
    {
#if     defined(LINUX)
        timeval lTimeVal;

        gettimeofday(&lTimeVal, nullptr);

        return I64(lTimeVal.tv_sec * kFromSecondToMicro + lTimeVal.tv_usec);
#elif   defined(MINGW)
        static I64  sFreq = k0LL;

        if (sFreq == 0)
        {
            LARGE_INTEGER   lFrequency;
            QueryPerformanceFrequency(&lFrequency);
            sFreq = I64(lFrequency.QuadPart);
        }

        LARGE_INTEGER   lTime;
        QueryPerformanceCounter(&lTime);

        return I64(kFromSecondToMicro * lTime.QuadPart) / sFreq;
#endif
    }


    /*! Convertit une date exprim�e en secondes en son �quivalent plus lisible : <b>Jour Mois Quanti�me
        Heure Ann�e</b>.
        @param pTime Date � exprimer dans un autre format
        @return La date <i>pTime</i> exprim�e en un format lisible
     */
    const char* date(U32 pTime)
    {
        time_t  lTime(pTime);

        return ctime(&lTime);
    }


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var TIME::Date::mTime
        La date est stock�e sous la forme d'un entier 32 bits (format unix : nombre de secondes depuis
        le 1er janvier 1970, � 0:00).
     */
}
