/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "InputMgr.hh"

/*! @file GDK/INPut/InputMgr.cc
    @brief M�thodes (non-inline) des classes INP::InputMgr, INP::Joypad & INP::Keyboard.
    @author @ref Guillaume_Terrissol
    @date 17 Mars 2002 - 29 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <SDL.h>

#include "ERRor/Log.hh"
#include "STL/Shared.tcc"
#include "STL/String.hh"

#include "ErrMsg.hh"
#include "InputDevice.hh"

namespace INP
{
//------------------------------------------------------------------------------
//                                    Joypad
//------------------------------------------------------------------------------

    /*! @brief Encapsulation d'un joypad.
        @version 1.0
     */
    class Joypad : public InputDevice
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Joypad(I32 pID);    //!< Constructeur.
virtual         ~Joypad();          //!< Destructeur.
        //@}
        //! @name Gestion SDL des joypads
        //@{
        void    open();             //!< Ouverture du joypad pour la r�ception des �v�nements.
        void    close();            //!< Fermeture du joypad.
        //@}
        //! @name Information sur les joypads
        //@{
 static I32     joypadCount();      //!< Nombre de joypads.
virtual String  name() const;       //!< Nom du joypad (+ caract�ristiques).
        //@}

    private:

        FORBID_COPY(Joypad)
        //! @name Donn�es propres aux joypads
        //@{
        I32             mID;        //!< Identifiant du joypad.
        SDL_Joystick*   mSDLJoypad; //!< Pointeur sur la structure SDLJoystick.
        //@}
    };


//------------------------------------------------------------------------------
//                                    Clavier
//------------------------------------------------------------------------------

    /*! @brief Gestion du clavier.
        @version 1.0
     */
    class Keyboard : public InputDevice
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Keyboard();     //!< Constructeur.
virtual         ~Keyboard();    //!< Destructeur.
        //@}
        //! @name Informations
        //@{
virtual String  name() const;   //!< Nom du contr�leur.
        //@}

    private:

        FORBID_COPY(Keyboard)
    };


//------------------------------------------------------------------------------
//                               P-Impl d'InputMgr
//------------------------------------------------------------------------------

    /*! @brief P-Impl de INP::InputMgr.
        @version 0.2
        @internal
     */
    class InputMgr::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
        Private();                                      //!< Constructeur.
        ResizeCallback              mResizeViewport;    //!< Redimensionnement du viewport.
        //! @name Attributs relatifs aux joypads
        //@{
        Vector<SharedPtr<Joypad>>   mJoypads;           //!< Tableau de joypads.
        Vector<bool>                mBoundJoypads;      //!< Utilisation des joypads.
        I16                         mLowerBound;        //!< Tol�rance minimale (sign�e).
        I16                         mUpperBound;        //!< Tol�rance maximale (sign�e)
        //@}
        //! @name Attribut relatif au clavier
        //@{
        SharedPtr<Keyboard>         mKeyboard;          //!< Clavier.
        Vector<I32>                 mKeyBindings;       //!< Touches d'action.
        Vector<I32>                 mMoveKeys;          //!< Touches de direction.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    InputMgr::Private::Private()
        : mResizeViewport{[](I32, I32){}}
        , mJoypads()
        , mBoundJoypads()
        , mLowerBound(static_cast<I16::TType>(- kJoypadAmplitude * 0.1F))
        , mUpperBound(- mLowerBound)
        , mKeyboard()
        , mKeyBindings()
        , mMoveKeys()
    { }


//------------------------------------------------------------------------------
//                      Joypad : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pID Identificateur du joypad (pour la SDL)
     */
    Joypad::Joypad(I32 pID)
        : mID(pID)
        , mSDLJoypad(nullptr)
    { }


    /*! Destructeur.
     */
    Joypad::~Joypad()
    {
        close();
    }


//------------------------------------------------------------------------------
//                       Joypad : Gestion SDL des Joypads
//------------------------------------------------------------------------------

    /*! Ouvre le joypad et effectue les initialisations n�cessaires.
     */
    void Joypad::open()
    {
        // Ouvre le joypad.
        mSDLJoypad = SDL_JoystickOpen(mID);

        // V�rifie la pr�sence d'un nombre suffisant de boutons.
        I32 lAxisCount = I32(SDL_JoystickNumAxes(mSDLJoypad));
        if (lAxisCount < eMinAxisCount)
        {
            LAUNCH_EXCEPTION(kTooFewAxis)
        }

        I32 lButtonCount = I32(SDL_JoystickNumButtons(mSDLJoypad));

        if (lButtonCount < eMinButtonCount)
        {
            LAUNCH_EXCEPTION(kTooFewButtons)
        }

        // (re)Cr�e les axes.
        initAxis(lAxisCount, Axis());

        // (re)Cr�e les boutons.
        initButtons(lButtonCount, Button());
    }


    /*! Ferme le joypad (les �v�nements ne seront plus captur�s).
     */
    void Joypad::close()
    {
        if (mSDLJoypad != nullptr)
        {
            SDL_JoystickClose(mSDLJoypad);

            mSDLJoypad = nullptr;
        }
    }


//------------------------------------------------------------------------------
//                     Joypad : Information sur les Joypads
//------------------------------------------------------------------------------

    /*! @return Le nombre total de joypads disponibles
     */
    I32 Joypad::joypadCount()
    {
        return I32(SDL_NumJoysticks());
    }


    /*! @return Le nom du joypad
     */
    String Joypad::name() const
    {
        return SDL_JoystickNameForIndex(mID);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Keyboard::Keyboard()
    {
        // (re)Cr�e les axes.
        initAxis(I32(eMinAxisCount), Axis());

        // (re)Cr�e les boutons.
        initButtons(I32(eMaxButtonCount), Button());
    }


    /*! Destructeur.
     */
    Keyboard::~Keyboard() { }


//------------------------------------------------------------------------------
//                                 Informations
//------------------------------------------------------------------------------

    /*! @return Le nom du p�riph�rique : <b>Clavier</b>
     */
    String Keyboard::name() const
    {
        return kKeyboardName;
    }


//------------------------------------------------------------------------------
//                            InputMgr : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    InputMgr::InputMgr()
        : pthis{}
    {
        // En premier lieu, initialise le sous-syst�me joystick de la SDL.
        if (SDL_InitSubSystem(SDL_INIT_JOYSTICK) < 0)
        {
            EXECUTE_ERROR_MANAGEMENT_CODE(LOG::Cerr << kJoypadInitialisationFailed << SDL_GetError() << LOG::Endl;)
        }

        // Initialisation des joypads.
        I32 lJoypadCount    = I32(SDL_NumJoysticks());

        if (0 < lJoypadCount)
        {
            // Active les �v�nements sur le(s) joypad(s).
            SDL_JoystickEventState(SDL_ENABLE);

            for(I32 lJ = k0L; lJ < lJoypadCount; ++lJ)
            {
                pthis->mJoypads.push_back(STL::makeShared<Joypad>(lJ));

                try
                {
                    pthis->mJoypads.back()->open();
                }
                catch(ERR::Exception& pE)
                {
                    LOG::Cerr << pE.what() << LOG::Endl;

                    // D�pile le joypad : il n'a pas �t� ouvert correctement.
                    pthis->mJoypads.pop_back();
                }
                catch(...)
                {
                    // D�pile le joypad : il n'a pas �t� ouvert correctement.
                    pthis->mJoypads.pop_back();

                    throw;
                }
            }

            pthis->mBoundJoypads.resize(lJoypadCount, false);
        }
        else
        {
            EXECUTE_ERROR_MANAGEMENT_CODE(LOG::Cerr << kNoJoypadDetected << LOG::Endl;)
        }

        // Initialisation de la partie clavier.
        pthis->mKeyboard = STL::makeShared<Keyboard>();
        pthis->mKeyBindings.resize(eMaxButtonCount, I32(eBad));
        pthis->mMoveKeys.resize(eMaxAxisCount, I32(eBad));
    }


    /*! Destructeur.
     */
    InputMgr::~InputMgr() { }


//------------------------------------------------------------------------------
//                        InputMgr : �v�nements Sp�ciaux
//------------------------------------------------------------------------------

    /*! @param pViewportResizer Cette callback doit permettre de redimensionner le viewport du moteur.
     */
    void InputMgr::set(ResizeCallback pViewportResizer)
    {
        pthis->mResizeViewport = pViewportResizer;
    }


//------------------------------------------------------------------------------
//                             InputMgr : Ev�nements
//------------------------------------------------------------------------------

    /*! Traitement des �v�nements.
        @return VRAI si au moins un �v�nement a �t� trait�, FAUX si le pool d'�v�nements exploitables
        �tait vide
     */
    bool InputMgr::processEvents()
    {
        bool        lEvent = false;
        SDL_Event   lSDLEvent;

        while(SDL_PollEvent(&lSDLEvent))
        {
            switch(lSDLEvent.type)
            {
                case SDL_KEYDOWN:
                    for(U32 lB = k0UL; lB < pthis->mKeyBindings.size(); ++lB)
                    {
                        if (pthis->mKeyBindings[lB] == lSDLEvent.key.keysym.sym)
                        {
                            pthis->mKeyboard->setButtonDown(EButtons(lB + eButton1));
                            lEvent = true;
                            break;
                        }
                    }
                    for(U32 lA = U32(eUp); lA <= eRight; ++lA)
                    {
                        if (pthis->mMoveKeys[lA - eUp] == lSDLEvent.key.keysym.sym)
                        {
                            switch(lA)
                            {
                                case eUp:    pthis->mKeyboard->setYValue(I16(- kJoypadAmplitude)); break;
                                case eDown:  pthis->mKeyboard->setYValue(I16(+ kJoypadAmplitude)); break;
                                case eLeft:  pthis->mKeyboard->setXValue(I16(- kJoypadAmplitude)); break;
                                case eRight: pthis->mKeyboard->setXValue(I16(+ kJoypadAmplitude)); break;
                                default:    break;
                            }
                            lEvent = true;
                        }
                    }
                    break;
                case SDL_KEYUP:
                    if (lSDLEvent.key.keysym.sym == SDLK_ESCAPE)
                    {
                        LOG::Cout << "ESC" << LOG::Endl;
                        lEvent = true;
                    }
                    else
                    {
                        for(U32 lB = k0UL; lB < pthis->mKeyBindings.size(); ++lB)
                        {
                            if (pthis->mKeyBindings[lB] == lSDLEvent.key.keysym.sym)
                            {
                                pthis->mKeyboard->setButtonUp(EButtons(lB + eButton1));
                                lEvent = true;
                                break;
                            }
                        }
                        for(U32 lA = U32(eUp); lA <= eRight; ++lA)
                        {
                            if (pthis->mMoveKeys[lA - eUp] == lSDLEvent.key.keysym.sym)
                            {
                                switch(lA)
                                {
                                    case eUp:    pthis->mKeyboard->setYValue(k0W); break;
                                    case eDown:  pthis->mKeyboard->setYValue(k0W); break;
                                    case eLeft:  pthis->mKeyboard->setXValue(k0W); break;
                                    case eRight: pthis->mKeyboard->setXValue(k0W); break;
                                    default:    break;
                                }
                                lEvent = true;
                            }
                        }
                    }
                    break;
                case SDL_JOYAXISMOTION:
                    {
                        I16 lAxisValue = I16(lSDLEvent.jaxis.value);

                        if ((lAxisValue <= pthis->mLowerBound) || (pthis->mUpperBound <= lAxisValue))
                        {
                            if (lSDLEvent.jaxis.axis == eXAxis)
                            {
                                pthis->mJoypads[lSDLEvent.jaxis.which]->setXValue(lAxisValue);
                            }
                            else if (lSDLEvent.jaxis.axis == eYAxis)
                            {
                                pthis->mJoypads[lSDLEvent.jaxis.which]->setYValue(lAxisValue);
                            }
                        }
                        else
                        {
                            if (lSDLEvent.jaxis.axis == eXAxis)
                            {
                                pthis->mJoypads[lSDLEvent.jaxis.which]->setXValue(k0W);
                            }
                            else if (lSDLEvent.jaxis.axis == eYAxis)
                            {
                                pthis->mJoypads[lSDLEvent.jaxis.which]->setYValue(k0W);
                            }
                        }
                        lEvent = true;
                    }
                    break;
                case SDL_JOYBUTTONDOWN:
                    pthis->mJoypads[lSDLEvent.jbutton.which]->setButtonDown(EButtons(lSDLEvent.jbutton.button + eButton1));
                    lEvent = true;
                    break;
                case SDL_JOYBUTTONUP:
                    pthis->mJoypads[lSDLEvent.jbutton.which]->setButtonUp(EButtons(lSDLEvent.jbutton.button + eButton1));
                    lEvent = true;
                    break;
                case SDL_WINDOWEVENT:
                    // Un peu con de mettre �a ici, mais la SDL est ainsi faite que c'�tait la solution la plus simple.
                    if (lSDLEvent.window.event == SDL_WINDOWEVENT_RESIZED)
                    {
                        if (pthis->mResizeViewport)
                        {
                            pthis->mResizeViewport(I32(lSDLEvent.window.data1), I32(lSDLEvent.window.data2));
                        }
                    }
                    break;
                case SDL_QUIT:
                    lEvent = true;
                    break;
                default:
                    break;
            }
        }

        return lEvent;
    }


//------------------------------------------------------------------------------
//                        InputMgr : Gestion des Joypads
//------------------------------------------------------------------------------

    /*! La position des axes d'un joypad subit en permanence des petites variations. Afin de les
        att�nuer (� d�faut de les supprimer), on peut fixer une zone dans laquelle les variations des
        axes ne seront pas prises en compte (carr� centr� � l'origine).
        @param pTolerance Les variations des mouvements seront prises en compte si elles sont sup�rieures,
        en valeur absolue, � <i>pTolerance</i>% de l'amplitude maximale de mouvement
     */
    void InputMgr::setJoypadTolerance(F32 pTolerance)
    {
        pthis->mUpperBound = I16(kJoypadAmplitude * pTolerance);
        pthis->mLowerBound = - pthis->mUpperBound;
    }


    /*! @return Le nombre de joypads disponibles
     */
    I32 InputMgr::joypadCount() const
    {
        return I32(pthis->mJoypads.size());
    }


//------------------------------------------------------------------------------
//                      InputMgr : Acc�s aux P�riph�riques
//------------------------------------------------------------------------------

    /*! @param pJoypadID Indice du joypad � retourner (� partir de 1)
        @return Le <i>pJoypadID</i>i�me joypad
     */
    InputMgr::ConstInputDevicePtr InputMgr::joypad(I32 pJoypadID) const
    {
        U32 lJoypadID(pJoypadID - 1);

        ASSERT_EX(lJoypadID < U32(joypadCount()), kUndefinedJoypad, return ConstInputDevicePtr();)

        return pthis->mJoypads[lJoypadID];
    }


    /*! @return Le clavier
     */
    InputMgr::ConstInputDevicePtr InputMgr::keyboard() const
    {
        return pthis->mKeyboard;
    }


//------------------------------------------------------------------------------
//                      InputMgr : Gestion des Contraintes
//------------------------------------------------------------------------------

    /*! @param pKeyID Identifie la touche du clavier dont on cherche � savoir si elle est li� � un bouton
        ou un axe
        @return VRAI si la touche <i>pKeyID</i> est d�j� utilis�e, FAUX sinon
     */
    bool InputMgr::usedKey(I32 pKeyID) const
    {
        // Cherche la touche parmi celles d'action.
        for(U32 lB = k0UL; lB < pthis->mKeyBindings.size(); ++lB)
        {
            if (pthis->mKeyBindings[lB] == pKeyID)
            {
                return true;
            }
        }

        // Cherche la touche parmi celles de mouvement.
        for(U32 lB = k0UL; lB < eMaxAxisCount; ++lB)
        {
            if (pthis->mMoveKeys[lB] == pKeyID)
            {
                return true;
            }
        }

        // Elle n'est pas utilis�e.
        return false;
    }


    /*! @param pButton Bouton � lier avec une touche
        @param pKeyID  Identifie la touche du clavier � lier avec le bouton <i>pButton</i>
        @return VRAI si la touche a bien �t� li�e (elle ne l'�tait pas), FAUX si elle �tait d�j� li�e
     */
    bool InputMgr::bindKey(EButtons pButton, I32 pKeyID)
    {
        ASSERT_EX(U32::TType(pButton - eButton1) < pthis->mKeyBindings.size(), kInvalidButton, return false;)

        if (!usedKey(pKeyID))
        {
            pthis->mKeyBindings[pButton - eButton1] = I32(pKeyID);

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! @param pAxis  Axe � lier avec une touche
        @param pKeyID Identifie la touche du clavier � lier avec l'axe <i>pAxis</i>
        @return VRAI si la touche a bien �t� li�e (elle ne l'�tait pas), FAUX si elle �tait d�j� li�e
     */
    bool InputMgr::bindKey(EAxis pAxis, I32 pKeyID)
    {
        ASSERT_EX((pAxis - eUp) < eMaxAxisCount, kInvalidAxis, return false;)

        if (!usedKey(pKeyID))
        {
            pthis->mMoveKeys[pAxis - eUp] = pKeyID;

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! D�lie une touche.
        @param pKeyID Identifie la touche du clavier � d�lier
        @note Ne fait rien si la touche n'�tait pas li�e
     */
    void InputMgr::unbindKey(I32 pKeyID)
    {
        for(U32 lB = k0UL; lB < pthis->mKeyBindings.size(); ++lB)
        {
            if (pthis->mKeyBindings[lB] == pKeyID)
            {
                pthis->mKeyBindings[lB] = I32(eBad);
                break;
            }
        }
    }


    /*! Active l'utilisation d'un joystick.
        @param pJoypadID Identifie le joystick � activer (index � partir de 1)
     */
    bool InputMgr::bindJoypad(I32 pJoypadID)
    {
        U32 lJoypadID(pJoypadID - 1);

        if ((lJoypadID < pthis->mBoundJoypads.size()) && (!pthis->mBoundJoypads[lJoypadID]))
        {
            pthis->mBoundJoypads[lJoypadID] = true;

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! D�sactive l'utilisation d'un joystick.
        @param pJoypadID Identifie le joystick � d�sactiver (index � partir de 1)
     */
    void InputMgr::unbindJoypad(I32 pJoypadID)
    {
        U32 lJoypadID(pJoypadID - 1);

        if (U32(pJoypadID) < pthis->mBoundJoypads.size())
        {
            pthis->mBoundJoypads[pJoypadID] = false;
        }
    }
}
