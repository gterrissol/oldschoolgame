/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_INPUTDEVICE_HH
#define INP_INPUTDEVICE_HH

#include "INPut.hh"

/*! @file GDK/INPut/InputDevice.hh
    @brief En-t�te de la classe INP::InputDevice et des PODs Axis & Button.
    @author @ref Guillaume_Terrissol
    @date 1 Mai 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/V2.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"

#include "Enum.hh"

namespace INP
{
    /*! @brief Donn�es d'un axe.
        @version 1.0
        @internal

        Cette structure regroupe des informations sur les �tats d'un "axe" (vertical ou horizontal) pour
        un p�riph�rique d'entr�e.
     */
    struct Axis
    {
        Axis();     //!< Constructeur par d�faut.
        I16 mStart; //!< Valeur initiale.
        I16 mValue; //!< Valeur courante de position de l'axe.
    };


    /*! @brief Donn�es d'un bouton.
        @version 1.0
        @internal

        Cette structure regroupe des informations sur les �tats d'un bouton (�tat appuy� iou rel�ch�, moment du dernier appui,...)
     */
    struct Button
    {
        Button();               //!< Constructeur par d�faut.
        I32     mStart;         //!< D�but du dernier appui.
        I32     mEnd;           //!< Fin du dernier appui.
        I32     mPressedTime;   //!< Dur�e de la derni�re pression.
        I32     mReleasedTime;  //!< Derni�re dur�e entre 2 pressions.
        bool    mPressed;       //!< Etat d'appui.
        bool    mReleased;      //!< Etat de rel�chement.
    };


    /*! @brief P�riph�rique d'entr�es.
        @version 0.6
        @internal

        Encapsulation du comportement d'un p�riph�rique d'entr�e.
     */
    class InputDevice : public MEM::OnHeap
    {
        //! @name Classe amie
        //@{
 friend class InputMgr;                                                             //!< Pour que l'Input Manager puisse acc�der aux m�thodes priv�es.
        //@}
    public:
        //! @name Constructeur & destructeur
        //@{
                                    InputDevice();                                  //!< Constructeur.
virtual                             ~InputDevice();                                 //!< Destructeur.
        //@}
        //! @name Commandes de base : mouvements
        //@{
                const MATH::V2& movement() const;                                   //!< Sens de mouvement.
                bool            circle(bool pCCW) const;                            //!< Cercle.
                const MATH::V2& turnOver(I32 pElapsed) const;                       //!< Demi-tour brusque.
        //@}
        //! @name Gestion des boutons
        //@{
        inline  const Button&   getButton(EButtons pID) const;                      //!< Donn�es d'un bouton.
        inline  I32             buttonCount() const;                                //!< Nombre de boutons.
        //@}
        //! @name Informations
        //@{
virtual         String          name() const = 0;                                   //!< Nom du contr�leur.
        //@}

    protected:
        //! @name Initialisation
        //@{
        inline  void            initAxis(I32 pCount, const Axis& pDefault);         //!< Initialisations des axes.
        inline  void            initButtons(I32 pCount, const Button& pDefault);    //!< Initialisations des boutons.
        //@}

    private:

        FORBID_COPY(InputDevice)
        //! @name Entr�es courantes
        //@{
        inline  void            setXValue(I16 pX);                                  //!< Fixe la valeur X courante.
        inline  void            setYValue(I16 pY);                                  //!< Fixe la valeur Y courante.
                void            setButtonUp(EButtons pButton);                      //!< Signale la pression d'un bouton.
                void            setButtonDown(EButtons pButton);                    //!< Signale le rel�chement d'un bouton.
        //@}
        //! @name M�morisation des �v�nements
        //@{
        Vector<Axis>   mAxis;                                                       //!< Axes du p�riph�riques.
        MATH::V2       mDirection;                                                  //!< Sens de mouvement.
        Vector<Button> mButtons;                                                    //!< Boutons du p�riph�riques.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "InputDevice.inl"

#endif  // De INP_INPUTDEVICE_HH
