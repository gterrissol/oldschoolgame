/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_ENUM_HH
#define INP_ENUM_HH

/*! @file GDK/INPut/Enum.hh
    @brief Enumérations du module INP.
    @author @ref Guillaume_Terrissol
    @date 4 Avril 2002 - 13 Mai 2012
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace INP
{
    //! Boutons.
    enum EButtons
    {
        eButton1 = 0,
        eButton2,
        eButton3,
        eButton4,
        eButton5,
        eButton6,
        eButton7,
        eButton8,
        eButton9,
        eButtonA,
        eMaxButtonCount
    };


    //! Axes.
    enum EAxis
    {
        eUp = 0,
        eDown,
        eLeft,
        eRight,
        eMaxAxisCount,

        eXAxis = 0,
        eYAxis = 1
    };


    //! Autres énumérations
    enum
    {
        eBad            = - 1,
        eMinAxisCount   = 2,    //!< Nombre minimal d'axes requis.
        eMinButtonCount = 6     //!< Nombre minimal de bouton requis.
    };
}

#endif // De INP_ENUM_HH
