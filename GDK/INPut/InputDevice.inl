/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/INPut/InputDevice.inl
    @brief M�thodes inline de la classe INP::InputDevice.
    @author @ref Guillaume_Terrissol
    @date 1 Mai 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Const.hh"
#include "ErrMsg.hh"

namespace INP
{
//------------------------------------------------------------------------------
//                              Gestion des Boutons
//------------------------------------------------------------------------------

    /*! @param pID Identifiant du bouton dont on veut obtenir les donn�es
        @return Les donn�es du bouton <i>pID</i>
     */
    inline const Button& InputDevice::getButton(EButtons pID) const
    {
        U32 lButton = U32(pID - eButton1);

        ASSERT(lButton < mButtons.size(), kInvalidButton)

        return mButtons[lButton];
    }


    /*! @return Le nombre total de boutons du p�riph�rique si c'est un joypad, INP::E_Bad sinon
     */
    inline I32 InputDevice::buttonCount() const
    {
        return I32(mButtons.size());
    }


//------------------------------------------------------------------------------
//                                Initialisation
//------------------------------------------------------------------------------

    /*! @param pCount   Nombre d'axes � cr�er
        @param pDefault Valeur par d�faut des axes
     */
    inline void InputDevice::initAxis(I32 pCount, const Axis& pDefault)
    {
        mAxis.resize(pCount, pDefault);
    }


    /*! @param pCount   Nombre d'axes � cr�er
        @param pDefault Valeur par d�faut des boutons
     */
    inline void InputDevice::initButtons(I32 pCount, const Button& pDefault)
    {
        mButtons.resize(pCount, pDefault);
    }


//------------------------------------------------------------------------------
//                               Entr�es Courantes
//------------------------------------------------------------------------------

    /*! Fixe la valeur X courante.
        @param pX Nouvelle valeur de X
     */
    inline void InputDevice::setXValue(I16 pX)
    {
        mDirection.x = F32(pX / kJoypadAmplitude);
    }


    /*! Fixe la valeur Y courante.
        @param pY Nouvelle valeur de Y
     */
    inline void InputDevice::setYValue(I16 pY)
    {
        mDirection.y = F32(pY / kJoypadAmplitude);
    }
}
