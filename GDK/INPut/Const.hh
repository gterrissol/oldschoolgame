/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_CONST_HH
#define INP_CONST_HH

/*! @file GDK/INPut/Const.hh
    @brief Enum�rations du module INP.
    @author @ref Guillaume_Terrissol
    @date 9 Mai 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace INP
{
    extern  const char* kKeyboardName;                          //!< Nom du clavier en tant que p�riph�rique.
    extern  const char* kNone;                                  //!< "Nom" d'un p�riph�rique anonyme.

    const   I32         kTimeThreshold      = I32(300);         //!< D�lai de prise en compte des �v�nements.
    const   F32         kJoypadAmplitude    = F32(32767.0F);    //!< Amplitude des axes des joypads.
}

#endif // De INP_CONST_HH
