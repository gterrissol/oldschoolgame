/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_CONTROLLER_HH
#define INP_CONTROLLER_HH

#include "INPut.hh"

/*! @file GDK/INPut/Controller.hh
    @brief En-t�te de la classe INP::Controller.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/MATH.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/String.hh"

#include "Enum.hh"
#include "InputMgr.hh"

namespace INP
{
    /*! @brief Contr�leur.
        @version 0.7

        Cette classe joue le r�le d'interface entre le gestionnaire des entr�es (qui capture tous les
        �v�nements) et l'utilisateur.
     */
    class Controller : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        using Ptr = SharedPtr<Controller>;                          //!< Pointeur sur contr�leur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Controller(InputMgr::Ptr pInputMgr);            //!< Constructeur.
                    ~Controller();                                  //!< Destructeur.
        //@}
        //! @name Commandes : mouvements
        //@{
        MATH::V2    movement() const;                               //!< Mouvement.
        bool        circle(bool pCCW) const;                        //!< Cercle ?
        MATH::V2    turnOver(I32 pElapsed) const;                   //!< Revirement ?
        //@}
        //! @name Commandes : boutons
        //@{
        I32         pressed(EButtons pButton) const;                //!< Appui sur un bouton ?
        I32         released(EButtons pButton) const;               //!< Rel�chement d'un bouton ?
        bool        chain(EButtons pFirst, EButtons pThen) const;   //!< Encha�nement de 2 boutons ?
        bool        repeated(EButtons pButton) const;               //!< Appui � r�p�tition sur un bouton ?
        //@}
        //! @name Liaisons entr�es/commandes
        //@{
        bool        bindToJoypad(I32 pJoypadID);                    //!< 
        bool        bindToKeyboard();                               //!< 
        bool        bindKey(EButtons pButton, I32 pKeyID);          //!< 
        bool        bindMoveKeys(I32 pKeyUp,   I32 pKeyDown,
                                 I32 pKeyLeft, I32 pKeyRight);      //!< Touches de direction.
        //@}
        //! @name Informations
        //@{
        String      name() const;                                   //!< Nom du contr�leur.
        I32         buttonCount() const;                            //!< Nombre de boutons.
        //@}

    private:

        FORBID_COPY(Controller)
        PIMPL()
    };
}

#endif  // De INP_CONTROLLER_HH
