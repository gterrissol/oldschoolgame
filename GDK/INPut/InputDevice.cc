/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "InputDevice.hh"

/*! @file GDK/INPut/InputDevice.cc
    @brief M�thodes (non-inline) de la classe INP::InputDevice et des PODs Axis & Button.
    @author @ref Guillaume_Terrissol
    @date 1 Mai 2002 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "TIME/MainTimer.hh"

namespace INP
{
//------------------------------------------------------------------------------
//                                     PODs
//------------------------------------------------------------------------------

    /*! Axe : constructeur par d�faut.
     */
    Axis::Axis()
        : mStart(0)
        , mValue(0)
    { }


    /*! Bouton : constructeur par d�faut.
     */
    Button::Button()
        : mStart(0)
        , mEnd(0)
        , mPressedTime(0)
        , mReleasedTime(0)
        , mPressed(false)
        , mReleased(false)
    { }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    InputDevice::InputDevice()
        : mAxis()
        , mDirection(k0F, k0F)
        , mButtons()
    { }


    /*! Destructeur.
     */
    InputDevice::~InputDevice() { }


//------------------------------------------------------------------------------
//                        Commandes de Base : Mouvements
//------------------------------------------------------------------------------

    /*!
     */
    const MATH::V2& InputDevice::movement() const
    {
        return mDirection;
    }


    /*! @todo Partitionner la zone de mouvement du pad en 4 ou 8, et m�moriser les passages de l'une � l'autre part.
     */
    bool InputDevice::circle(bool) const
    {
        return false;
    }


    /*! D�termine la direction apr�s un demi-tour brusque (s'il y a eu un), avec une tol�rance de
        <i>pElapsed</i> millisecondes.
        @param pElapsed Tol�rance de la d�tection d'un demi-tour brusque
        @return La direction apr�s un demi-tour brusque, le vecteur null sinon
     */
    const MATH::V2& InputDevice::turnOver(I32) const
    {
        return mDirection;
    }


//------------------------------------------------------------------------------
//                               Entr�es Courantes
//------------------------------------------------------------------------------

    /*! Signale le rel�chement d'un bouton.
        @param pButton Code du bouton rel�ch�
     */
    void InputDevice::setButtonUp(EButtons pButton)
    {
        U32 lButton = U32(pButton - eButton1);

        ASSERT_EX(lButton < mButtons.size(), kInvalidButton, return;)

        mButtons[lButton].mEnd          = I32(TimeMgr->frameTime() / 1000); // De �s en ms.
        mButtons[lButton].mReleased     = true;
        mButtons[lButton].mPressed      = false;

        mButtons[lButton].mPressedTime  = (mButtons[lButton].mEnd - mButtons[lButton].mStart);
    }


    /*! Signale l'enfoncement d'un bouton.
        @param pButton Code du bouton enfonc�
     */
    void InputDevice::setButtonDown(EButtons pButton)
    {
        U32 lButton = U32(pButton - eButton1);

        ASSERT_EX(lButton < mButtons.size(), kInvalidButton, return;)

        mButtons[lButton].mStart        = I32(TimeMgr->frameTime() / 1000); // De �s en ms.
        mButtons[lButton].mPressed      = true;
        mButtons[lButton].mReleased     = false;

        mButtons[lButton].mReleasedTime = (mButtons[lButton].mStart - mButtons[lButton].mEnd);
    }
}
