/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_ALL_HH
#define INP_ALL_HH

/*! @file GDK/INPut/All.hh
    @brief Interface publique du module @ref INPut.
    @author @ref Guillaume_Terrissol
    @date 17 Mars 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace INP  //! Gestion des p�riph�riques d'entr�es.
{
    /*! @namespace INP
        @version 0.8

     */

    /*! @defgroup INPut INPut : P�riph�riques d'entr�e
        <b>namespace</b> INP.
     */
}

#include "InputMgr.hh"
#include "Controller.hh"

#endif  // De INP_ALL_HH
