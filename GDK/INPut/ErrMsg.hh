/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_ERRMSG_HH
#define INP_ERRMSG_HH

/*! @file GDK/INPut/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module INP.
    @author @ref Guillaume_Terrissol
    @date 2 Avril 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace INP
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kINPModuleTimerAlreadyExists;
    extern  ERR::String kInvalidAxis;
    extern  ERR::String kInvalidButton;
    extern  ERR::String kJoypadInitialisationFailed;
    extern  ERR::String kNoJoypadDetected;
    extern  ERR::String kTooFewAxis;
    extern  ERR::String kTooFewButtons;
    extern  ERR::String kUndefinedJoypad;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De INP_ERRMSG_HH
