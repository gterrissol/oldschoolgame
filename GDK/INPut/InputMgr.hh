/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_INPUTMGR_HH
#define INP_INPUTMGR_HH

#include "INPut.hh"

/*! @file GDK/INPut/InputMgr.hh
    @brief En-t�te de la classe INP::InputMgr.
    @author @ref Guillaume_Terrissol
    @date 17 Mars 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "STL/Function.hh"
#include "STL/Shared.hh"
#include "STL/SharedPtr.hh"
#include "VIEWport/VIEWport.hh"

#include "Enum.hh"

namespace INP
{
    /*! @brief Gestionnaire des p�riph�riques d'entr�es
        @version 0.7

        Le gestionnaire d'entr�e capture tous les �v�nements et se charge de les distribuer aux
        sous-gestionnaires ad�quats.
     */
    class InputMgr :
        public MEM::OnHeap,
        public STL::enable_shared_from_this<InputMgr>
    {
    public:
        //! @name Types de pointeur
        //@{
        using Ptr                   = SharedPtr<InputMgr>;          //!< 
        using ConstInputDevicePtr   = WeakPtr<InputDevice const>;   //!< Pointeur (constant) sur p�riph�rique.
        using InputDevicePtr        = WeakPtr<InputDevice>;         //!< Pointeur sur p�riph�rique.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            InputMgr();                             //!< Constructeur par d�faut.
                            ~InputMgr();                            //!< Destructeur.
        //@}
        //! @name �v�nements sp�ciaux
        //@{
        using ResizeCallback    = std::function<void (I32, I32)>;   //!< Redimensionnement.
        void                set(ResizeCallback pViewportResizer);   //!< Redimensionnement du viewport.
        //@}
        //! @name Gestion g�n�rale
        //@{
        bool                processEvents();                        //!< G�re tous les �v�nements encore non-trait�s.
        //@}
        //! @name Gestion des joypads
        //@{
        void                setJoypadTolerance(F32 pTolerance);     //!< Etablissement de la tol�rance des joypads.
        I32                 joypadCount() const;                    //!< Nombre de joypads disponibles.
        //@}
        //! @name Acc�s aux p�riph�riques
        //@{
        ConstInputDevicePtr joypad(I32 pJoypadID) const;            //!< Acc�s � un joypad.
        ConstInputDevicePtr keyboard() const;                       //!< Acc�s au clavier.
        //@}
        //! @name Gestion des contraintes
        //@{
        bool                usedKey(I32 pKeyID) const;              //!< Une touche est-elle utilis�e ?
        bool                bindKey(EButtons pButton, I32 pKeyID);  //!< Lie une touche.
        bool                bindKey(EAxis pAxis, I32 pKeyID);       //!< Lie une touche.
        void                unbindKey(I32 pKeyID);                  //!< "D�lie" une touche.
        bool                bindJoypad(I32 pJoypadID);              //!< Lie un joypad.
        void                unbindJoypad(I32 pJoypadID);            //!< "D�lie" un joypad.
        //@}

    private:

        FORBID_COPY(InputMgr)
        PIMPL()
    };
}

#endif  // De INP_INPUTMGR_HH
