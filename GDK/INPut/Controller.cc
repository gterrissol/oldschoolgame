/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file GDK/INPut/Controller.cc
    @brief M�thodes (non-inline) de la class INP::Controller.
    @author @ref Guillaume_Terrissol
    @date 17 Mars 2002 - 1er Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>

#include <SDL.h>

#include "STL/String.hh"
#include "TIME/MainTimer.hh"

#include "InputDevice.hh"
#include "InputMgr.hh"

namespace INP
{
//------------------------------------------------------------------------------
//                             P-Impl de Controller
//------------------------------------------------------------------------------

    /*! @brief P-Impl de INP::Controller.
        @version 0.9
        @internal
     */
    class Controller::Private : public OnHeap
    {
    public:
        //! @name Constructeur
        //@{
                Private(InputMgr::Ptr pInputMgr);                   //!< Constructeur.
        //@}
        void    unbindKeys(Vector<I32>& pBoundKeys);                //!< "D�liaison" de touches.
        //! @name Type de pointeur
        //@{
        typedef WeakPtr<InputDevice const>  ConstInputDevicePtr;    //!< Pointeur (constant) sur p�riph�rique.
        typedef WeakPtr<InputDevice>        InputDevicePtr;         //!< Pointeur sur p�riph�rique.
        //@}
        WeakPtr<InputMgr::Ptr::element_type>    mInputMgr;          //!< Gestionnaire de p�riph�riques d'entr�e.
        //! @name Quel p�riph�rique ?
        //@{
        Vector<I32>                             mBoundKeys;         //!< Touches clavier li�es.
        I32                                     mJoyID;             //!< Index du joypad utilis�.
        ConstInputDevicePtr                     mDevice;            //!< P�riph�rique utilis�.
        bool                                    mUseKeyboard;       //!< Utilisation du clavier ?
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pInputMgr Gestionnaire de p�riph�riques d'entr�e, source des �v�nements pour ce contr�leur.
     */
    Controller::Private::Private(InputMgr::Ptr pInputMgr)
        : mInputMgr{pInputMgr}
        , mBoundKeys()
        , mJoyID(0)
        , mDevice()
        , mUseKeyboard(false)
    { }


//------------------------------------------------------------------------------
//                            P-Impl : Autre M�thode
//------------------------------------------------------------------------------

    /*! D�lie toutes les touches que ce contr�leur avait li�.
     */
    void Controller::Private::unbindKeys(Vector<I32>& pBoundKeys)
    {
        while(0 < pBoundKeys.size())
        {
            mInputMgr.lock()->unbindKey(pBoundKeys.back());
            pBoundKeys.pop_back();
        }
    }


//------------------------------------------------------------------------------
//                                 Constructeur
//------------------------------------------------------------------------------

    /*! @copydoc Controller::Private::Private(InputMgr::Ptr pInputMgr)
     */
    Controller::Controller(InputMgr::Ptr pInputMgr)
        : pthis{pInputMgr}
    { }


    /*! Destructeur.
     */
    Controller::~Controller()
    {
        pthis->mInputMgr.lock()->unbindJoypad(pthis->mJoyID);
        pthis->unbindKeys(pthis->mBoundKeys);
    }


//------------------------------------------------------------------------------
//                            Commandes : Mouvements
//------------------------------------------------------------------------------

    /*!
     */
    MATH::V2 Controller::movement() const
    {
        return pthis->mDevice.lock()->movement();
    }


    /*!
     */
    bool Controller::circle(bool pCCW) const
    {
        return pthis->mDevice.lock()->circle(pCCW);
    }


    /*!
     */
    MATH::V2 Controller::turnOver(I32 pElapsed) const
    {
        return pthis->mDevice.lock()->turnOver(pElapsed);
    }


//------------------------------------------------------------------------------
//                              Commandes : Boutons
//------------------------------------------------------------------------------

    /*! @param pButton 
        @return Le temps d'appui sur <i>pButton</i>, 0 s'il est rel�ch�
     */
    I32 Controller::pressed(EButtons pButton) const
    {
        const Button&   lCurrentButton  = pthis->mDevice.lock()->getButton(pButton);

        return ((lCurrentButton.mPressed) ? lCurrentButton.mStart : k0L);
    }


    /*! @param pButton
        @return Le temps de rel�chement de <i>pButton</i>, 0 s'il est enfonc�
     */
    I32 Controller::released(EButtons pButton) const
    {
        const Button&   lCurrentButton  = pthis->mDevice.lock()->getButton(pButton);

        return ((lCurrentButton.mReleased) ? lCurrentButton.mEnd : k0L);
    }


    /*! @param pFirst
        @param pThen
        @return 
     */
    bool Controller::chain(EButtons pFirst, EButtons pThen) const
    {
        const Button&   lFirst  = pthis->mDevice.lock()->getButton(pFirst);
        const Button&   lThen   = pthis->mDevice.lock()->getButton(pThen);

        // 2 cas : les commandes sont distinctes...
        if (pFirst != pThen)
        {
            if ((lFirst.mReleased) && (lThen.mPressed))
            {
                I32 lDelta = lThen.mStart - lFirst.mEnd;

                return ((- (kTimeThreshold / 2) < lDelta) && (lDelta < kTimeThreshold));
            }
        }
        else    // Ou pas.
        {
            if (lThen.mPressed)
            {
                // Condition : pas trop de temps entre le rel�chement et le nouvel appui.
                I32 lDelta = lThen.mStart - lFirst.mEnd;

                return (lDelta < kTimeThreshold);
            }
        }

        return false;
    }


    /*! @param pButton
        @return
     */
    bool Controller::repeated(EButtons pButton) const
    {
        const Button&   lRepeatButton   = pthis->mDevice.lock()->getButton(pButton);

        I32 lCurrent    = I32(TimeMgr->frameTime() / 1000); // De �s en ms.

        if (lRepeatButton.mPressed)
        {
            if ((abs(lRepeatButton.mPressedTime - lRepeatButton.mReleasedTime) < (kTimeThreshold / 2)) &&
                ((lCurrent - lRepeatButton.mStart) < (5 * lRepeatButton.mReleasedTime / 4))            &&
                ((lCurrent - lRepeatButton.mStart) < kTimeThreshold))
            {
                return true;
            }
        }
        else if (lRepeatButton.mReleased)
        {
            if ((abs(lRepeatButton.mReleasedTime - lRepeatButton.mPressedTime) < (kTimeThreshold / 2)) &&
                ((lCurrent - lRepeatButton.mEnd) < (5 * lRepeatButton.mPressedTime / 4))               &&
                 (lCurrent - lRepeatButton.mEnd) < kTimeThreshold)
            {
                return true;
            }
        }

        return false;
    }


//------------------------------------------------------------------------------
//                          Liaisons Entr�es/Commandes
//------------------------------------------------------------------------------

    /*! @param pJoypadID Un indice valide de joypad (voir INP::InputMgr::JoypadCount), ou INP::eBAD pour ne lier ce contr�leur qu'au
        clavier
        @return VRAI si le joypad a �t� li�, FAUX sinon (joypad inexistant ou d�j� li�)
     */
    bool Controller::bindToJoypad(I32 pJoypadID)
    {
        if (pthis->mInputMgr.lock()->bindJoypad(pJoypadID))
        {
            pthis->mJoyID          = pJoypadID;
            pthis->mUseKeyboard    = false;
            pthis->mDevice         = pthis->mInputMgr.lock()->joypad(pthis->mJoyID);

            return true;
        }

        return false;
    }


    /*!
     */
    bool Controller::bindToKeyboard()
    {
        if (!pthis->mUseKeyboard)
        {
            pthis->mInputMgr.lock()->unbindJoypad(pthis->mJoyID);
            pthis->mJoyID          = k0L;
            pthis->mUseKeyboard    = true;
            pthis->mDevice         = pthis->mInputMgr.lock()->keyboard();

            // Configuration par d�faut pour le clavier (voir plus tard comment mieux g�rer tout �a).
            bindMoveKeys(I32(SDLK_UP), I32(SDLK_DOWN), I32(SDLK_LEFT), I32(SDLK_RIGHT));
            bindKey(INP::eButton1, I32(SDLK_a));
            bindKey(INP::eButton2, I32(SDLK_q));
            bindKey(INP::eButton3, I32(SDLK_z));
            bindKey(INP::eButton4, I32(SDLK_s));
            bindKey(INP::eButton5, I32(SDLK_e));
            bindKey(INP::eButton6, I32(SDLK_d));
            bindKey(INP::eButton7, I32(SDLK_LCTRL));
            bindKey(INP::eButton8, I32(SDLK_RCTRL));
            bindKey(INP::eButton9, I32(SDLK_RSHIFT));
            bindKey(INP::eButtonA, I32(SDLK_RETURN));

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! D�finit quelle touche du clavier permet d'�muler un bouton du joypad donn� (si cette touche n'est
        pas d�j� affect�e).
        @param pButton Bouton du joypad � �muler
        @param pKeyID  Touche du clavier � lier � <i>pButton</i>
        @return VRAI si la liaison a r�ussi, FAUX sinon
     */
    bool Controller::bindKey(EButtons pButton, I32 pKeyID)
    {
        if (!pthis->mUseKeyboard)
        {
            return false;
        }

        if (pthis->mInputMgr.lock()->bindKey(pButton, pKeyID))
        {
            pthis->mBoundKeys.push_back(pKeyID);
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! Associe des touches du clavier aux directions.
        @param pKeyUp    Touche associ�e � la direction "haut"
        @param pKeyDown  Touche associ�e � la direction "bas"
        @param pKeyLeft  Touche associ�e � la direction "gauche"
        @param pKeyRight Touche associ�e � la direction "droite"
        @return VRAI si la liaison a �t� effectu�e pour chaque touche, FAUX sinon
     */
    bool Controller::bindMoveKeys(I32 pKeyUp, I32 pKeyDown, I32 pKeyLeft, I32 pKeyRight)
    {
        if (!pthis->mUseKeyboard)
        {
            return false;
        }

        Vector<I32> lBoundKeys;

        try
        {
            if (!pthis->mInputMgr.lock()->bindKey(eUp,    pKeyUp))
            {
                throw ERR::Exception("");
            }
            else
            {
                lBoundKeys.push_back(pKeyUp);
            }
            if (!pthis->mInputMgr.lock()->bindKey(eDown,  pKeyDown))
            {
                throw ERR::Exception("");
            }
            else
            {
                lBoundKeys.push_back(pKeyDown);
            }
            if (!pthis->mInputMgr.lock()->bindKey(eLeft,  pKeyLeft))
            {
                throw ERR::Exception("");
            }
            else
            {
                lBoundKeys.push_back(pKeyLeft);
            }
            if (!pthis->mInputMgr.lock()->bindKey(eRight, pKeyRight))
            {
                throw ERR::Exception("");
            }
            else
            {
                lBoundKeys.push_back(pKeyRight);
            }

            // La liaison a r�ussi : garde une r�f�rence sur les touches li�es.
            std::copy(lBoundKeys.begin(),
                      lBoundKeys.end(),
                      std::back_insert_iterator<Vector<I32>>(pthis->mBoundKeys));

            return true;
        }
        catch(...)
        {
            // D�lie toutes les touches.
            pthis->unbindKeys(lBoundKeys);

            return false;
        }
    }


//------------------------------------------------------------------------------
//                                 Informations
//------------------------------------------------------------------------------

    /*! @return INP::kKeyboard si le contr�leur est associ� au clavier, le nom du joypad s'il est li�
        � un joypad, "Aucun" sinon
     */
    String Controller::name() const
    {
        if (!pthis->mDevice.expired())
        {
            return pthis->mDevice.lock()->name();
        }
        else
        {
            return String(kNone);
        }        
    }


    /*! @return Le nombre de bouton du joypad auquel ce contr�leur est associ�, INP::eBad s'il n'utilise
        que le clavier
     */
    I32 Controller::buttonCount() const
    {
        if (!pthis->mDevice.expired())
        {
            return pthis->mDevice.lock()->buttonCount();
        }
        else
        {
            return I32(eBad);
        }
    }
}
