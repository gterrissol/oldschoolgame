/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "INPut.hh"

/*! @file GDK/INPut/INPut.cc
    @brief D�finitions diverses du module INP.
    @author @ref Guillaume_Terrissol
    @date 17 Mars 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace INP
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kINPModuleTimerAlreadyExists, "Le Timer du module INP existe d�j�.",  "INP module Timer already exists")
    REGISTER_ERR_MSG(kInvalidAxis,                 "Axe invalide.",                        "Invalid axis")
    REGISTER_ERR_MSG(kInvalidButton,               "Bouton invalide.",                     "Invalid button")
    REGISTER_ERR_MSG(kJoypadInitialisationFailed,  "L'initialisation du joypad a �chou�.", "Joypad initialisation failed.")
    REGISTER_ERR_MSG(kNoJoypadDetected,            "Aucun joypad d�tect�.",                "No joypad detected")
    REGISTER_ERR_MSG(kTooFewAxis,                  "Nombre d'axes insuffisant.",           "Too few axis")
    REGISTER_ERR_MSG(kTooFewButtons,               "Nombre de boutons insuffisant.",       "Too fex buttons")
    REGISTER_ERR_MSG(kUndefinedJoypad,             "Joypad non-d�fini.",                   "Undefined joypad")


//------------------------------------------------------------------------------
//                                  Constantes
//------------------------------------------------------------------------------

#if     defined(FRANCAIS)
    const char* kKeyboardName   = "Clavier";
    const char* kNone           = "Aucun";
#elif   defined(ENGLISH)
    const char* kKeyboardName   = "Keyboard";
    const char* kNone           = "None";
#endif  // Du langage.
}
