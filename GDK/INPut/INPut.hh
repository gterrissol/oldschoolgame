/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef INP_INPUT_HH
#define INP_INPUT_HH

/*! @file GDK/INPut/INPut.hh
    @brief Pr�-d�clarations du module @ref INPut.
    @author @ref Guillaume_Terrissol
    @date 26 Avril 2008 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace INP
{
    class Controller;
    class InputDevice;
    class InputMgr;
}

#endif  // De INP_INPUT_HH
