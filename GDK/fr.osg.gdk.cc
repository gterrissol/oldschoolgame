/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/fr.osg.gdk.cc
    @brief D�finition de la classe GDK::Activator.
    @author @ref Guillaume_Terrissol
    @date 23 Septembre 2011 - 25 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "EXTernals/Define.hh"
#include "MEMory/MemoryMgr.hh"

MINIMAL_ACTIVATOR(GDK)
