/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MEMory/PImpl.inl
    @brief M�thodes inline de la classe MEM::PImpl.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2007 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <utility>

namespace MEM
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TPrivate, class TCheck>
    PImpl<TPrivate, TCheck>::PImpl()
        : mData(new TPrivate())
    { }


    /*! Constructeur � un argument.
        @param p1 Param�tre unique pass� au constructeur de TPrivate le mieux adapt�
     */
    template<class TPrivate, class TCheck>
    template<class TT>
    PImpl<TPrivate, TCheck>::PImpl(TT&& p1)
        : mData(new TPrivate(std::forward<TT>(p1)))
    { }


    /*! Constructeur � deux arguments.<br>
        Le constructeur de TPrivate le mieux adapt� (aux param�tres fournis) sera appel�.
        @param p1 Premier param�tre
        @param p2 Second param�tre
     */
    template<class TPrivate, class TCheck>
    template<class TT1, class TT2>
    PImpl<TPrivate, TCheck>::PImpl(TT1&& p1, TT2&& p2)
        : mData(new TPrivate(std::forward<TT1>(p1), std::forward<TT2>(p2)))
    { }


    /*! Constructeur � trois arguments.<br>
        Le constructeur de TPrivate le mieux adapt� (aux param�tres fournis) sera appel�.
        @param p1 Premier param�tre
        @param p2 Deuxi�me param�tre
        @param p3 Troisi�me param�tre
     */
    template<class TPrivate, class TCheck>
    template<class TT1, class TT2, class TT3>
    PImpl<TPrivate, TCheck>::PImpl(TT1&& p1, TT2&& p2, TT3&& p3)
        : mData(new TPrivate(std::forward<TT1>(p1), std::forward<TT2>(p2), std::forward<TT3>(p3)))
    { }


    /*! Constructeur par copie.
        @param pOther Instance � copier
     */
    template<class TPrivate, class TCheck>
    PImpl<TPrivate, TCheck>::PImpl(const PImpl& pOther)
        : mData(new TPrivate(*pOther.mData))
    { }


    /*! Op�rateur d'affectation.
        @param pOther Instance � assigner � *<b>this</b>
        @return *<b>this</b>
     */
    template<class TPrivate, class TCheck>
    PImpl<TPrivate, TCheck>& PImpl<TPrivate, TCheck>::operator=(const PImpl& pOther)
    {
        PImpl<TPrivate, TCheck> lTmp(pOther);
        swap(lTmp);

        return *this;
    }


//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    template<class TPrivate, class TCheck>
    PImpl<TPrivate, TCheck>::~PImpl()
    {
        TCheck();
        delete mData;
    }


//------------------------------------------------------------------------------
//                                  Permutation
//------------------------------------------------------------------------------

    /*! Permute le pointeur port� par l'instance avec celui d'une autre instance.
        @param pPImpl P-Impl avec lequel permuter le pointeur de donn�es
     */
    template<class TPrivate, class TCheck>
    void PImpl<TPrivate, TCheck>::swap(PImpl& pPImpl)
    {
        TPrivate*   lTmp = mData;
        mData            = pPImpl.mData;
        pPImpl.mData     = lTmp;
    }


//------------------------------------------------------------------------------
//                               Acc�s aux Donn�es
//------------------------------------------------------------------------------

    /*! @return Un pointeur vers les donn�es de l'impl�mentation
     */
    template<class TPrivate, class TCheck>
    TPrivate* PImpl<TPrivate, TCheck>::operator->() const
    {
        return mData;
    }


    /*! @return Une r�f�rence vers les donn�es de l'impl�mentation
     */
    template<class TPrivate, class TCheck>
    TPrivate& PImpl<TPrivate, TCheck>::operator*() const
    {
        ASSERT(mData != nullptr, kInvalidPointer)

        return *mData;
    }
}
