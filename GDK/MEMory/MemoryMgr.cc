/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MemoryMgr.hh"

/*! @file GDK/MEMory/MemoryMgr.cc
    @brief M�thodes (non-inline) de la classe MEM::MemoryMgr.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 21 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <functional>
#include <mutex>
#include <vector>

#include "ERRor/Log.hh"

#include "BuiltIn.hh"

#undef CHECK_FOR_CORRUPTION

namespace MEM
{
//------------------------------------------------------------------------------
//                                 Enum�rations
//------------------------------------------------------------------------------

#ifdef  CHECK_FOR_CORRUPTION
    //! Valeurs remarquables pour l'initialisation de la m�moire.
    enum
    {
        eBaadFood   = 0xBAADF00D,                       //!< M�moire lib�r�e.
        eDeadBeef   = 0xDEADBEEF                        //!< M�moire initialis�e.
    };
#endif  // De CHECK_FOR_CORRUPTION

    //! Valeurs pour la gestion des blocs de m�moire globale.
    enum
    {
        eAlignment          = sizeof(uint32_t),         //!< Alignement des donn�es.
        eChunkSize          = 512,                      //!< Taille d'un bloc, en octets.
        eChunkSizeInHeap    = eChunkSize / eAlignment,  //!< Taille d'un bloc dans le tas.
        eChunkCount         = 1048576                   //!< Nombre total de blocs.
    };


//------------------------------------------------------------------------------
//                              P-Impl de MemoryMgr
//------------------------------------------------------------------------------

    /*! @brief P-Impl de MEM::MemoryMgr.
        @version 0.8
        @internal

        La @ref MEM_Memory_Page est fragment�e en blocs de eChunkSize octets.
     */
    class MemoryMgr::Private
    {
    public:
        //! @name Constructeur et destructeur
        //@{
                Private();                          //!< Constructeur par d�faut.
                ~Private();                         //!< Destructeur.
        //@}
        //! @name Gestion interne de la m�moire
        //@{
        void*   customAllocate(std::size_t pSize);  //!< Fonction <b>malloc</b>() propri�taire.
        void    customDeallocate(void* pPointer);   //!< Fonction <b>free</b>() propri�taire.
        //@}
        //! @name Gestion des blocs
        //@{
        bool    trimMemory();                       //!< D�fragmente la m�moire.
        void    findFreeChunk(uint32_t pIndex);     //!< Trouve le nouveau "premier" chunk libre.
        //@}
        //! @name Attributs
        //@{
        std::vector<uint32_t>   mHeapMemory;        //!< M�moire globale.
        std::vector<uint16_t>   mSizes;             //!< Tailles des blocs allou�s
        std::vector<uint16_t>   mAvailable;         //!< Nombre de blocs libres contigus.
#ifdef MEMORY_CHECK
        std::vector<uint16_t>   mSpareBlocks;       //!< M�moire allou�e par bloc et non-utilis�e.
        uint32_t                mSpareMemory;       //!< M�moire allou�e mais non-utilis�e.
#endif  // De MEMORY_CHECK
        uint32_t                mFreeChunkCount;    //!< Nombre de chunks libres.
        uint32_t                mFreeChunk;         //!< "Premier" chunk libre.
        std::mutex              mMutex;             //!< Mutex.
        //@}
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    MemoryMgr::Private::Private()
        : mHeapMemory(eChunkCount * eChunkSizeInHeap)
        , mSizes(eChunkCount, 0)
        , mAvailable(eChunkCount)
#ifdef MEMORY_CHECK
        , mSpareBlocks(eChunkCount, 0)
        , mSpareMemory(0)
#endif  // De MEMORY_CHECK
        , mFreeChunkCount(eChunkCount)
        , mFreeChunk(0)
        , mMutex()
    {
//        LOG::Cout << "Memory : [" << &mHeapMemory[0] << ", " << (&mHeapMemory.back() + 1) << "[\n";
#ifdef CHECK_FOR_CORRUPTION
        std::fill(mHeapMemory.begin(), mHeapMemory.end(), uint32_t{eDeadBeef});
#endif  // De CHECK_FOR_CORRUPTION

        uint16_t    lCounter = 0;
        for(auto lI = mAvailable.rbegin(); lI != mAvailable.rend(); ++lI)
        {
            *lI = (++lCounter != 0) ? lCounter : ++lCounter;    // L'overflow est voulu.
        }
    }


    /*! Destructeur.
     */
    MemoryMgr::Private::~Private()
    {
#ifdef  MEMORY_CHECK
        // V�rification de la m�moire.
        if (mFreeChunkCount < eChunkCount)
        {
            WARNING(kDetectingMemoryLeaks);
            LOG::Cerr << (eChunkCount - mFreeChunkCount) << " :\n";
            for(auto lSzI = mSizes.begin(); lSzI != mSizes.end(); ++lSzI)
            {
                if (*lSzI != 0)
                {
                    uint32_t    lIndex  = lSzI - mSizes.begin();
                    LOG::Cerr << &mHeapMemory[lIndex * eChunkSizeInHeap] << " : " << *lSzI << '\n';
                }
            }
        }
#endif  // De MEMORY_CHECK
    }


//------------------------------------------------------------------------------
//                         Gestion Interne de la M�moire
//------------------------------------------------------------------------------

    /*! @param pSize Nombre d'octets � allouer.
        @return Un pointeur sur la m�moire allou�e, un pointeur nul en cas d'erreur
     */
    void* MemoryMgr::Private::customAllocate(std::size_t pSize)
    {
        void*   lPointerToReturn = nullptr;

        std::lock_guard<std::mutex> lLock(mMutex);

        // V�rifie s'il reste de la m�moire disponible.
        uint32_t    lNeededL = (pSize + eChunkSize - 1) / eChunkSize;

        if (lNeededL <= mFreeChunkCount)
        {
            ASSERT(pSize <= lNeededL * static_cast<uint32_t>(eChunkSize), kOverflowError)
            uint16_t    lNeededW    = static_cast<uint16_t>(lNeededL);
            ASSERT(lNeededW == lNeededL, kOverflowError)
            uint16_t*   lHi         = &mAvailable[mFreeChunk];
            uint16_t*   lLo         = lHi - 1;
            uint16_t*   lHiBound    = &mAvailable.back() + 1;
            uint16_t*   lStart      = &mAvailable.front();
            uint16_t*   lLoBound    = lStart - 1;
            uint16_t*   lChunk      = nullptr;

            if (lLo == lLoBound)
            {
                lLo = nullptr;
            }

            while(true)
            {
                // Algo : � partir du premier chunk libre, cherche un nombre de chunks
                // contigus suffisant pour l'allocation (en montant et en descendant).
                if (lHi != nullptr)
                {
                    if (lNeededW <= *lHi)
                    {
                        lChunk = lHi;
                        break;
                    }
                    if (lHi == lHiBound)
                    {
                        lHi = nullptr;
                        if (lLo == nullptr)
                        {
                            break;
                        }
                    }
                    else
                    {
                        ++lHi;
                    }
                }

                if (lLo != nullptr)
                {
                    if (lNeededW <= *lLo)
                    {
                        lChunk = lLo;
                        break;
                    }
                    if (--lLo == lLoBound)
                    {
                        lLo = nullptr;
                        if (lHi == nullptr)
                        {
                            break;
                        }
                    }
                }
            }

            // Une s�rie de blocs en nombre suffisant a �t� trouv�e.
            if (lChunk != nullptr)
            {
                uint32_t    lIndex      = lChunk - lStart;
                ASSERT(lIndex < eChunkCount, kInvalidIndex)

                mFreeChunkCount    -= lNeededL;
                mSizes[lIndex]      = lNeededW;

                lPointerToReturn    = &mHeapMemory[lIndex * eChunkSizeInHeap];

#ifdef  CHECK_FOR_CORRUPTION
                // V�rifie que toute la m�moire � allouer est bien libre.
                uint32_t*   lBegin      = &mHeapMemory[lIndex * eChunkSizeInHeap];
                uint32_t*   lEnd        = lBegin + (lNeededL * eChunkSizeInHeap);
                ASSERT(std::find_if(lBegin,
                                    lEnd,
                                    std::bind2nd
                                        (std::not_equal_to<uint32_t>(), uint32_t{eDeadBeef})
                                    ) == lEnd, kBufferOverflow)
                std::fill(lBegin, lEnd, uint32_t{eBaadFood});
#endif  // De CHECK_FOR_CORRUPTION

                // Marque les blocs utilis�s.
                for(uint16_t* lChk = lChunk; lChk < lChunk + lNeededW; ++lChk)
                {
#ifdef CHECK_FOR_CORRUPTION
                    ASSERT(*lChk != 0, kCorruptData)
#endif  // De CHECK_FOR_CORRUPTION
                    *lChk = 0;
                }
                if (lChunk == lLo)
                {
                    // Il faut s'occuper de tous les blocs libres pr�c�dents
                    //          disponibilit� moindre blocs allou�s
                    // (... 0 0 [ M M-1 M-2 ... N+1 ] N N-1 ... 2 1 0 0 ...).
                    for(uint16_t* lChk = lChunk - 1; (lChk != lLoBound) && (lNeededW < *lChk); --lChk)
                    {
                        // Bloc inutilis� / m�me s�rie de blocs libres.
                        if ((*lChk != 0) && (lNeededW < *lChk))
                        {
                            *lChk -= lNeededW;
                        }
                    }
                }
                if (lIndex == mFreeChunk)
                {
                    findFreeChunk(lIndex);
                }

#ifdef MEMORY_CHECK
                // Memoire inutilis�e : part du dernier bloc allou� non-demand�e.
                uint32_t    lSpare = lNeededL * static_cast<uint32_t>(eChunkSize) - pSize;
                mSpareBlocks[lIndex + lNeededL - 1] = lSpare;
                mSpareMemory += lSpare;
#endif  // De MEMORY_CHECK
            }
        }

#ifdef MEMORY_CHECK
        ASSERT_EX(reinterpret_cast<uint32_t*>(static_cast<char*>(lPointerToReturn) + pSize) <= (&mHeapMemory.back() + 1),
                  "Out of bounds",
                  return nullptr;)
#endif  // De MEMORY_CHECK

        return lPointerToReturn;
    }


    /*! Lib�re un bloc de m�moire
        @param pPointer Pointeur sur le bloc de m�moire � d�sallouer
     */
    void MemoryMgr::Private::customDeallocate(void* pPointer)
    {
        std::lock_guard<std::mutex> lLock(mMutex);

        uint32_t*   lPointer    = static_cast<uint32_t*>(pPointer);
        ASSERT((&mHeapMemory.front() <= lPointer) && (lPointer <= &mHeapMemory.back()), kInvalidPointer)
        uint32_t    lIndex      = (lPointer - &mHeapMemory.front()) / eChunkSizeInHeap;
        ASSERT(lIndex < eChunkCount, kInvalidIndex)
        uint16_t    lSize       = mSizes[lIndex];
        ASSERT(lSize != 0, kCorruptData)

#ifdef MEMORY_CHECK
        // Retour de la m�moire inutilis�e.
        uint16_t&   lSpare  = mSpareBlocks[lIndex + lSize - 1];
        mSpareMemory   -= lSpare;
        lSpare = 0;
        ASSERT(mFreeChunkCount + static_cast<uint32_t>(lSize) <= eChunkCount, kOverflowError)
#endif  // De MEMORY_CHECK

        mFreeChunkCount += static_cast<uint32_t>(lSize);
        mSizes[lIndex] = 0;

#ifdef  CHECK_FOR_CORRUPTION
        ASSERT(std::find_if(&mSizes[lIndex + 1],
                            &mSizes[lIndex] + lSize,
                            std::bind2nd
                                (std::not_equal_to<uint16_t>(), 0)
                            ) - &mSizes[lIndex] == lSize, kCorruptData)
        std::fill_n(&mHeapMemory[lIndex * eChunkSizeInHeap], lSize * eChunkSizeInHeap, uint32_t{eDeadBeef});
#endif  // De CHECK_FOR_CORRUPTION

        for(uint16_t* lAvail = &mAvailable[lIndex]; 0 < lSize; ++lAvail, --lSize)
        {
#ifdef  CHECK_FOR_CORRUPTION
            ASSERT(*lAvail == 0, kCorruptData)
#endif  // De CHECK_FOR_CORRUPTION
            *lAvail = lSize;
        }

        // Le bloc libre doit �tre le plus gros disponible.
        if (mAvailable[mFreeChunk] < lSize)
        {
            mFreeChunk = lIndex;
        }
    }


//------------------------------------------------------------------------------
//                              Gestion des Chunks
//------------------------------------------------------------------------------

    /*! Recalcule les s�ries de blocs libres contigus.
     */
    bool MemoryMgr::Private::trimMemory()
    {
        std::lock_guard<std::mutex> lLock(mMutex);

        uint16_t*   lLoBound    = &mAvailable.front() - 1;
        uint16_t*   lHiBound    = &mAvailable.back();
        uint16_t    lSize       = 0;
        bool        lTrimed     = false;

        for(uint16_t* lAvail = lHiBound; lAvail != lLoBound; --lAvail)
        {
            if (*lAvail != 0)
            {
#ifdef CHECK_FOR_CORRUPTION
                ASSERT(*lAvail <= lSize, kCorruptData)
#endif  // De CHECK_FOR_CORRUPTION

                if      (*lAvail == 1)
                {
                    *lAvail = ++lSize;
                }
                else if (*lAvail < lSize)
                {
                    *lAvail = ++lSize;
                    lTrimed = true;
                }
                else // *lAvail == lSize
                {
                    ++lSize;
                }
            }
            else
            {
                lSize = 0;
            }
        }

        return lTrimed;
    }


    /*! Recherche une s�rie de blocs libres pour les futures allocations, � partir d'un emplacement
        donn�.
        @param pIndex Indice de bloc � partir duquel effectuer la recherche bidirectionnelle
     */
    void MemoryMgr::Private::findFreeChunk(uint32_t pIndex)
    {
        // Parcours montant et descendant pour trouver un bloc libre.
        uint16_t*   lHi         = &mAvailable[pIndex];
        uint16_t*   lLo         = lHi - 1;
        uint16_t*   lHiBound    = &mAvailable.back() + 1;
        uint16_t*   lBegin      = &mAvailable.front();
        uint16_t*   lLoBound    = lBegin - 1;

        if (lLo == lLoBound)
        {
            lLo = nullptr;
        }

        while(true)
        {
            if (lHi != nullptr)
            {
                if (0 < *lHi)
                {
                    mFreeChunk = lHi - lBegin;
                    ASSERT(mFreeChunk < eChunkCount, kInvalidIndex)
                    return;
                }
                if (lHi == lHiBound)
                {
                    lHi = nullptr;
                    if (lLo == nullptr)
                    {
                        break;
                    }
                }
                else
                {
                    ++lHi;
                }
            }

            if (lLo != nullptr)
            {
                if      ((lLo - 1) == lLoBound)
                {
                    lLo = nullptr;
                    if (lHi == nullptr)
                    {
                        break;
                    }
                }
                else if ((*lLo != 0) && (*(lLo - 1) == 1))
                {
                    // lLo est au d�but d'une s�rie de blocs disponibles.
                    mFreeChunk = lLo - lBegin;
                    ASSERT(mFreeChunk < eChunkCount, kInvalidIndex)
                    return;
                }
                else
                {
                    --lLo;
                }
            }
        }

        // S'il n'y a plus de bloc libre, c'est que le nombre �gale 0 (comme dirait La Palice).
        ASSERT(mFreeChunkCount == 0, kCorruptData)
        mFreeChunk = eChunkCount;
    }

//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    MemoryMgr::MemoryMgr()
        : pthis()
    {
        new SmallObjAllocator(8 * eChunkSize, eChunkSize / 2, eAlignment);
    }


    /*! Destructeur.
     */
    MemoryMgr::~MemoryMgr()
    {
        delete SmallObjAllocator::get();
    }


//------------------------------------------------------------------------------
//                           Allocation, D�sallocation
//------------------------------------------------------------------------------

    /*! Alloue un bloc de m�moire.
        @param pSize Nombre d'octets du bloc de m�moire � allouer
        @return Un bloc de m�moire de <i>pSize</i> octets
     */
    void* MemoryMgr::allocate(std::size_t pSize)
    {
        void*   lResult = pthis->customAllocate(pSize);

        if (lResult == nullptr)
        {
            if (pthis->trimMemory())
            {
                lResult = pthis->customAllocate(pSize);

                if (lResult == nullptr)
                {
                    throw std::bad_alloc();
                }
            }
            else
            {
                throw std::bad_alloc();
            }
        }

        return lResult;
    }


    /*! Lib�re un bloc de m�moire.
        @param pPointer Pointeur sur le bloc m�moire � d�sallouer
     */
    void MemoryMgr::deallocate(void* pPointer)
    {
        if (pPointer != nullptr)
        {
            pthis->customDeallocate(pPointer);
        }
    }


//------------------------------------------------------------------------------
//                                 Informations
//------------------------------------------------------------------------------

#ifdef MEMORY_CHECK

    /*! @return La m�moire totale accessible � l'utilisateur
     */
    std::size_t MemoryMgr::totalMemorySize() const
    {
        return eChunkCount * eChunkSize;
    }


    /*! @return La quantit� de m�moire allou�e par l'utilisateur
     */
    std::size_t MemoryMgr::allocatedMemorySize() const
    {
        return usedMemorySize() - spareMemorySize();
    }


    /*! @return La quantit� de m�moire allou�e r�ellement (avec les pertes induites par l'allocateur de blocs)
     */
    std::size_t MemoryMgr::usedMemorySize() const
    {
        return (eChunkCount - pthis->mFreeChunkCount) * eChunkSize;
    }


    /*! @return La quantit� de m�moire allou�e dans des blocs et non-utilis�e
     */
    std::size_t MemoryMgr::spareMemorySize() const
    {
        return pthis->mSpareMemory;
    }

#endif  // De MEMORY_CHECK

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  MemoryMgr*  Singleton<MemoryMgr>::smThat    = nullptr;
#endif


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var MemoryMgr::Private::mHeapMemory
        "Tas" de l'application : gros bloc de m�moire allou� � l'initialisation, et dans lequel se feront
        les allocations du moteur.
     */
    /*! @var MemoryMgr::Private::mSizes
        Ce tableau contient des informations sur chaque bloc allou� (une case = un bloc).<br>
        Pour chaque allocation, le nombre de blocs est conserv� � l'indice correspondant au premier bloc
        allou�. Les cases correspondant aux blocs suivants (s'il y en a), sont mises � 0.
     */
    /*! @var MemoryMgr::Private::mAvailable
        Ce tableau contient des informations sur chaque bloc allou� (une case = un bloc).<br>
        Chaque case, � n'importe quel moment, contient :
        - 0, si le bloc correspondant est allou�,
        - un nombre strictement positif, �gal � un nombre de blocs disponibles contigus, � partir de ce
        bloc.

        Exemple : [ 0 0 4 3 2 1 2 1 0 0 1 0 2 1 0 0 ]
     */
    /*! @var MemoryMgr::Private::mFreeChunk
        Indice du premier chunk disponible � consid�rer pour une allocation (une recherche sera lanc�
        s'il ne donne pas acc�s � une s�rie de blocs contigus suffisante pour une allocation).
     */
}
