/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_CLASS_HH
#define MEM_CLASS_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/Class.hh
    @brief En-t�te des classes MEM::OnHeap & MEM::Auto.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <new>

namespace MEM
{
//------------------------------------------------------------------------------
//                             Objet M�moire de Base
//------------------------------------------------------------------------------

    /*! @brief Objet m�moire de base
        @version 1.5
        @ingroup MEM_Models

        Cette classe doit �tre la classe de base de toute hi�rachie d'objet dans le moteur, afin
        d'assurer la r�impl�mentation des op�rateurs new & delete.
     */
    class OnHeap
    {
    public:
        //! @name Op�rateurs new
        //@{
 static inline  void*   operator new(std::size_t pSize);                            //!< ... plain.
 static inline  void*   operator new[](std::size_t pSize);                          //!< ... [].
 static inline  void*   operator new(std::size_t pSize, void* pPlace);              //!< ... "in place".
        //@}
        //! @name Op�rateurs delete
        //@{
 static inline  void    operator delete(void* pPointer, std::size_t pSize);         //!< ... plain.
 static inline  void    operator delete[](void* pPointer, std::size_t pSize);       //!< ... [].
 static inline  void    operator delete(void* pPointer, void* pPlace);              //!< ... "in place".
        //@}

    protected:

        inline          ~OnHeap();                                                  //!< Destructeur.


    private:
        //! @name Versions d�sactiv�s d'operator new & delete
        //@{
 static         void*   operator new(std::size_t, const std::nothrow_t&) throw();   //!< ... nothrow.
 static         void*   operator new[](std::size_t, const std::nothrow_t&) throw(); //!< ... [] nothrow.
 static         void    operator delete(void*, const std::nothrow_t&) throw();      //!< ... nothrow.
 static         void    operator delete[](void*, const std::nothrow_t&) throw();    //!< ... [] nothrow.
        //@}
    };


    /*! @brief Classe de base pour les objets "automatiques".
        @version 0.6
        @ingroup MEM_Models

        Initialement, cette classe n'�tait pas template, mais comme il y avait des probl�mes avec l'EBO
        (qui ne jouait plus lorsqu'une classe d�rivant de MEM::Auto avait comme attribut une instance
        d'une autre classe d�rivant de MEM::Auto). Avec une MEM::Auto template (id�alement, l'argument
        doit �tre la classe d�riv�e), plus de probl�me.
     */
    template<typename TT>
    class Auto
    {
    public:
        //! @name Versions activ�es d'operator new & delete
        //@{
 static inline  void*   operator new(std::size_t pSize, void* pPlace);              //!< ... "in place".
 static inline  void    operator delete(void* pPointer, void* pPlace);              //!< ... "in place".
        //@}

    protected:

        inline          ~Auto();                                                    //!< Destructeur.


    private:
        //! @name Versions d�sactiv�s d'operator new & delete
        //@{
 static         void*   operator new(std::size_t);                                  //!< ... plain.
 static         void*   operator new[](std::size_t);                                //!< ... [].
 static         void*   operator new(std::size_t, const std::nothrow_t&) throw();   //!< ... nothrow.
 static         void*   operator new[](std::size_t, const std::nothrow_t&) throw(); //!< ... [] nothrow.
 static         void    operator delete(void*, const std::nothrow_t&) throw();      //!< ... nothrow.
 static         void    operator delete[](void*, const std::nothrow_t&) throw();    //!< ... [] nothrow.
 static         void    operator delete(void*, std::size_t);                        //!< ... plain.
 static         void    operator delete[](void*, std::size_t);                      //!< ... [].
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Class.inl"

#endif  // De MEM_CLASS_HH
