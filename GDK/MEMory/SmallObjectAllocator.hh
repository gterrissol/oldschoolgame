/* The Loki Library
   Copyright (c) 2001 by Andrei Alexandrescu
   This code accompanies the book:
   Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design 
       Patterns Applied". Copyright (c) 2001. Addison-Wesley.
   Permission to use, copy, modify, distribute and sell this software for any 
       purpose is hereby granted without fee, provided that the above  copyright 
       notice appear in all copies and that both that copyright notice and this 
       permission notice appear in supporting documentation.
   The author or Addison-Wesley Longman make no representations about the 
       suitability of this software for any purpose. It is provided "as is" 
       without express or implied warranty.
 */

#ifndef SMALLOBJECTALLOCATOR_HH
#define SMALLOBJECTALLOCATOR_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/SmallObjectAllocator.hh
    @brief En-t�te de la classe MEM::SmallObjectAllocator.
    @author Code original : Andrei Alexandrescu
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 30 Avril 2007 - 11 Septembre 2011
    @note "Portions Copyright (c) 2001 by Andrei Alexandrescu"
 */

#include <cstddef>

#include "Singleton.hh"

namespace MEM
{
    class FixedAllocator;

    /*! @brief Allocateur de petits objets
        @version 0.8
        @ingroup MEM_Allocation

        G�re un ensemble d'allocateur de taille fixe.
     */
    class SmallObjAllocator : public Singleton<SmallObjAllocator>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                            SmallObjAllocator(std::size_t pPageSize,
                                              std::size_t pMaxObjectSize,
                                              std::size_t pObjectAlignSize);    //!< Constructeur.
                            ~SmallObjAllocator();                               //!< Destructeur.
        //@}
        //! @name Allocation & d�sallocation
        //@{
                void*       allocate(std::size_t pByteCount);                   //!< Allocation.
                void        deallocate(void* pPointer, std::size_t pByteCount); //!< D�sallocation.
        //@}
        //! @name Informations
        //@{
        inline  std::size_t maxObjectSize() const;                              //!< Taille d'objet maximale.
        inline  std::size_t alignment() const;                                  //!< Alignement.
                bool        isCorrupt() const;                                  //!< Test de corruption.
        //@}

    private:

        FORBID_COPY(SmallObjAllocator)
                bool        trimExcessMemory();                                 //!< Lib�ration de la m�moire superflue.
        //! @name Attributs
        //@{
        FixedAllocator*     mPool;                                              //!< R�servoir d'allocateurs.
        const std::size_t   mMaxSmallObjectSize;                                //!< Taille d'objet maximale.
        const std::size_t   mObjectAlignSize;                                   //!< Alignement.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "SmallObjectAllocator.inl"

#endif  //De  SMALLOBJECTALLOCATOR_HH

