/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MEMory/Class.inl
    @brief M�thodes inline des classes MEM::OnHeap & MEM::Auto.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 19 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "SmallObjectAllocator.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                                 Operator New
//------------------------------------------------------------------------------

    /*! Gestion propri�taire de l'allocation m�moire : operator<b> new</b>.
        @param pSize Taille de la m�moire � allouer (param�tre automatique)
        @return Un pointeur sur la m�moire allou�e
     */
    inline void* OnHeap::operator new(std::size_t pSize)
    {
        return SmallObjAllocator::get()->allocate(pSize);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator<b> new[]</b>.
        @param pSize Taille de la m�moire � allouer (param�tre automatique)
        @return Un pointeur sur la m�moire allou�e
     */
    inline void* OnHeap::operator new[](std::size_t pSize)
    {
        return SmallObjAllocator::get()->allocate(pSize);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator<b> new()</b>.
        @param pSize Taille de la m�moire � allouer (param�tre automatique)
        @param pPlace  Emplacement m�moire o� placer l'objet � construire
        @return Un pointeur sur la m�moire allou�e
     */
    inline void* OnHeap::operator new(std::size_t pSize, void* pPlace)
    {
        return ::operator new(pSize, pPlace);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator<b> new()</b>.
        @param pSize Taille de la m�moire � allouer (param�tre automatique)
        @param pPlace  Emplacement m�moire o� placer l'objet � construire
        @return Un pointeur sur la m�moire allou�e
     */
    template<typename TT>
    inline void* Auto<TT>::operator new(std::size_t pSize, void* pPlace)
    {
        return ::operator new(pSize, pPlace);
    }


//------------------------------------------------------------------------------
//                                Operator Delete
//------------------------------------------------------------------------------

    /*! Gestion propri�taire de l'allocation m�moire : operator <b>delete</b>.
        @param pPointer Pointeur sur l'objet � effacer
        @param pSize  Taille de l'objet � effacer
     */
    inline void OnHeap::operator delete(void* pPointer, std::size_t pSize)
    {
        SmallObjAllocator::get()->deallocate(pPointer, pSize);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator <b>delete[]</b>.
        @param pPointer Pointeur sur l'objet � effacer
        @param pSize  Taille de l'objet � effacer
     */
    inline void OnHeap::operator delete[](void* pPointer, std::size_t pSize)
    {
        SmallObjAllocator::get()->deallocate(pPointer, pSize);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator <b>delete()</b>.
        @param pPointer Pointeur sur l'objet � effacer
        @param pPlace   Emplacement m�moire o� se trouve l'objet � d�truire
     */
    inline void OnHeap::operator delete(void* pPointer, void* pPlace)
    {
        ::operator delete(pPointer, pPlace);
    }


    /*! Gestion propri�taire de l'allocation m�moire : operator <b>delete()</b>.
        @param pPointer Pointeur sur l'objet � effacer
        @param pPlace   Emplacement m�moire o� se trouve l'objet � d�truire
     */
    template<typename TT>
    inline void Auto<TT>::operator delete(void* pPointer, void* pPlace)
    {
        ::operator delete(pPointer, pPlace);
    }


//------------------------------------------------------------------------------
//                              Destructeurs
//------------------------------------------------------------------------------

    /*! Destructeur prot�g� pour �viter un effacement � partir d'un OnHeap* (le destructeur n'�tant pas
        virtuel).
     */
    inline OnHeap::~OnHeap() = default;


    /*! Destructeur prot�g� : Auto n'a pas vocation a �tre instanci�e directement.
     */
    template<typename TT>
    inline Auto<TT>::~Auto() = default;
}
