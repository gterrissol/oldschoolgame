/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_ALLOCATOR_HH
#define MEM_ALLOCATOR_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/Allocator.hh
    @brief En-t�te de la classe MEM::Allocator.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstddef>

namespace MEM
{
    /*! @brief Allocateur personnalis�.
        @version 1.5
        @ingroup MEM_Allocation

        Ce nouvel allocateur permet l'emploi des conteneurs de la STL avec une gestion de la m�moire de
        bas niveau propri�taire.<br>
        Il reprend l'ensemble des m�thodes et des d�clarations de type d'un allocateur standard (ainsi
        que les r�gles de nommage).
     */
    template<class TT>
    class Allocator
    {
    public:
        //! @name Types li�s au type d'objet allou�
        //@{
        typedef TT              value_type;                                 //!< Type allou�.
        typedef std::size_t     size_type;                                  //!< R�sultat de sizeof().
        typedef std::ptrdiff_t  difference_type;                            //!< Diff�rence de pointeurs.

        typedef TT*             pointer;                                    //!< Pointeur sur un objet.
        typedef const TT*       const_pointer;                              //!< Pointeur constant.

        typedef TT&             reference;                                  //!< R�f�rence sur objet.
        typedef const TT&       const_reference;                            //!< R�f�rence constante.
        //@}
        //! @name Constructeurs & destructeur
        //@{
        inline                  Allocator() throw();                       //!< Constructeur par d�faut.
        inline                  Allocator(const Allocator&) throw();        //!< Constructeur par copie.
        template<class TU>
        inline                  Allocator(const Allocator<TU>&) throw();    //!< Constructeur par copie.
        inline                  ~Allocator() throw();                      //!< Destructeur.
        //@}
        //! @name Adresses
        //@{
        inline  pointer         address(reference pX) const;                //!< ... d'un objet.
        inline  const_pointer   address(const_reference pX) const;          //!< ... d'un objet constant.
        //@}
        //! @name [D�s]Allocation m�moire d'un objet
        //@{
 static inline  pointer         allocate(size_type pN, const void* = 0);    //!< Allocation.
 static inline  void            deallocate(void* pP, size_type pN);         //!< D�sallocation.
        //@}
        //! @name [D�s]Initialisation d'un objet
        //@{
        template<class TU, class... TArgs>
        inline  void            construct(TU* pP, TArgs&&... pArgs);        //!< Initialisation.
        template<class TU>
        inline  void            destroy(TU* pP);                            //!< Destruction.
        inline  void            construct(pointer pP, const_reference pV);  //!< Initialisation.
        inline  void            destroy(pointer pP);                        //!< Destruction.
        //@}
        inline  size_type       max_size() const throw();                   //!< Nombre maximum d'objets.

        //! Consultez "The C++ Programming Language, 3rd Edition" (�19.4.1).
        template<class TU> struct rebind
        {
            //! Allocateur pour types internes.
            typedef Allocator<TU>   other;
        };
    };
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Allocator.inl"

#endif  // De MEM_ALLOCATOR_HH
