/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MEMory.hh"

/*! @file GDK/MEMory/MEMory.cc
    @brief D�finitions diverses du module @ref MEMory.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

#include "BuiltIn.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlignmentError,              "Erreur d'alignement.",              "Alignment error.")
    REGISTER_ERR_MSG(kAlreadyCheckedIndex,         "Index d�j� contr�l�.",              "Already checked index.")
    REGISTER_ERR_MSG(kAlreadyConstructedSingleton, "Le singleton est d�j� construit.",  "Already constructed singleton.")
    REGISTER_ERR_MSG(kAlreadyDestructedSingleton,  "Le singleton est d�j� d�truit.",    "Singleton already destructed.")
    REGISTER_ERR_MSG(kAlreadyFilledChunk,          "Chunk d�j� rempli.",                "Already filled chunk.")
    REGISTER_ERR_MSG(kBufferOverflow,              "Ecrasement m�moire.",               "Buffer overflow.")
    REGISTER_ERR_MSG(kChunkNotFound,               "Chunk non trouv�.",                 "Chunk not found.")
    REGISTER_ERR_MSG(kCorruptChunk,                "Chunk corrompu.",                   "Corrupt chunk.")
    REGISTER_ERR_MSG(kCorruptData,                 "Donn�es corrompues.",               "Corrupt data.")
    REGISTER_ERR_MSG(kDoubleDeleteAttempt,         "Tentative de double effacement.",   "Double delete attempt.")
    REGISTER_ERR_MSG(kEmptyChunk,                  "Chunk vide.",                       "Empty chunk.")
    REGISTER_ERR_MSG(kEmptyChunkNotEmpty,          "Le chunk vide ne l'est pas.",       "Empty chunk not empty.")
    REGISTER_ERR_MSG(kInvalidAlignment,            "Alignement invalide.",              "Invalid alignment.")
    REGISTER_ERR_MSG(kInvalidBlockSize,            "Taille de bloc invalide.",          "Invalid block size.")
    REGISTER_ERR_MSG(kInvalidChunk,                "Chunk invalide.",                   "Invalid chunk.")
    REGISTER_ERR_MSG(kInvalidIndex,                "Index invalide.",                   "Invalid index.")
    REGISTER_ERR_MSG(kInvalidPageSize,             "Taille de page invalide.",          "Invalid page size.")
    REGISTER_ERR_MSG(kInvalidPool,                 "R�servoir d'allocateurs invalide.", "Invalid allocator pool.")
    REGISTER_ERR_MSG(kInvalidPointer,              "Pointeur invalide.",                "Invalid pointer.")
    REGISTER_ERR_MSG(kMoreThanOneEmptyChunk,       "Plus d'un chunk vide.",             "More than one empty chunk.")
    REGISTER_ERR_MSG(kNotOneEmptyChunk,            "Pas un chunk vide.",                "Not one empty chunk.")
    REGISTER_ERR_MSG(kNoChunkHasBeenPushed,        "Aucun bloc n'a �t� empil�.",        "No chunk has been pushed.")
    REGISTER_ERR_MSG(kNoSingletonIsDefined,        "Aucun singleton n'est d�fini.",     "No singleton is defined.")
    REGISTER_ERR_MSG(kNullBlockSize,               "Taille de bloc nulle.",             "Null block size.")
    REGISTER_ERR_MSG(kOverflowError,               "Erreur de d�passement.",            "Overflow error.")
    REGISTER_ERR_MSG(kRangeCheckError,             "Valeur d'indice incorrecte.",       "Range check error.")
    REGISTER_ERR_MSG(kUninitializedChunk,          "Chunk non-initialis�.",             "Uninitialized chunk.")
    REGISTER_ERR_MSG(kWrongChunk,                  "Mauvais chunk.",                    "Wrong chunk.")

    // Message d'avertissement.
    REGISTER_ERR_MSG(kDetectingMemoryLeaks,        "D�tection de 'fuites' de m�moire.", "Detecting memory leaks.")

//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page MEM_Memory_Page M�moire globale
        @version 1.5

        @par Introduction
        Le moteur d'Old School Game utilise sa propre gestion m�moire. Initialement, toutes les classes
        du moteur d�rivaient d'une m�me base qui d�finissait ses propres versions des operator <b>new</b>
        et <b>delete</b> permettant d'enregistrer les lieux d'appel et de d�tecter les memory leaks. Les
        op�rateurs globaux �taient ensuite utilis�es pour l'allocation m�moire proprement dite.

        @par Tas
        L'objectif premier �tait de cr�er un tas au d�marrage de l'application dans lequel aller piocher
        pour les allocations m�moire. Ce tas a fini par arriver. Ses avantages multiples :
        - contiguit� des donn�es,
        - contr�le de la taille de la m�moire,
        - optimisation des allocations/d�sallocations.
        Deux gestionnaires sont utilis�s. Le premier g�re uniquement de gros blocs du "tas". Le second
        (repris de Loki), g�re les petites allocations (il agit comme un vrai procurateur vis-�-vis du
        premier gestionnaire).
        
        @par Exploitation
        Afin de b�n�ficier de cette gestion m�moire, il faut :<ul>
        <li> utiliser MEM::Allocator pour les conteneurs de la STL,
        <li> utiliser les instanciations de MEM::BuiltIn en lieu et place des types int�gr�s,
        <li> faire d�river de MEM::OnHeap toutes les classes instanci�es par <b>new</b>,
        <li> faire d�river de MEM::Auto autres classes (qui ne pourront pas �tre allou�es sur le tas).
        </ul>
    */

    /*! @typedef Allocator<TT>::rebind<TU>::other
        Ce type est n�cessaire notamment pour les conteneurs qui ne manipulent pas directement en m�moire
        les types de donn�e qu'ils contiennent (e.g. une std::map<Key, Value) n'alloue pas directement
        des instances de Key et de Value, mais un type interm�diaire (un noeud d'arbre) qui pourra �tre
        allou� gr�ce � ce <b>typedef</b>).
     */

    /*! @def FORBID_COPY(Class)
        Permet d'interdire l'utilisation des constructeur et op�rateur de copie.<br>
        A utiliser dans la partie des d�clarations priv�es de la classe concern�e.
     */
}
