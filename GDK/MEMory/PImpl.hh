/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_PIMPL_HH
#define MEM_PIMPL_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/PImpl.hh
    @brief En-t�te de la classe MEM::PImpl.
    @author @ref Guillaume_Terrissol
    @date 29 D�cembre 2007 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Class.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                             Impl�mentation Priv�e
//------------------------------------------------------------------------------

    struct NoCheck { };                                     //!< @brief Absence de contr�le sur TPrivate.


    /*! @brief Idiome "impl�mentation priv�e".
        @version 1.1
        @ingroup MEM_Models

        Afin d'apporter une "impl�mentation priv�e" (p-impl) � une classe <b>C</b>, il suffit de :
        - utiliser la macro PIMPL() dans la partie des d�clarations priv�es de <b>C</b>,
        - d�finir l'impl�mentation (class <b>C</b> :: Private : public MEM::OnHeap { ... } ) dans le
        fichier source de <b>C</b>,
        - initialiser l'attribut <b>pthis</b> (qui est l'instanciation de l'impl�mentation priv�e) dans
        le constructeur de <b>C</b> (jusqu'� 3 param�tres sont autoris�s).

        @par Exemple
        @code
        class MyClass
        {
           ...
        private:

            PIMPL()
        };

        class MyClass::Private
        {
        public:
            typedef void* Pointee;
            Private(U32 pIndex);
            ...
        };

        MyClass::MyClass(U32 pIndex)
            : pthis(pIndex)
        {
            ...
        }

        void MyClass::foo()
        {
            Private::Pointee   lPointer = fee();
            ...
        }
        @endcode
        @warning Une classe poss�dant un P-Impl doit d�finir explicitement un constructeur et son
        destructeur dans le m�me fichier que la classe jouant le r�le de P-Impl (afin que la construction
        et la destruction de son p-impl soit possible)
     */
    template<class TPrivate, class TCheck = NoCheck>
    class PImpl : public Auto<PImpl<TPrivate, TCheck>>
    {
    public:
        //! @name Constructeurs
        //@{
        explicit    PImpl();                               //!< ... par d�faut.
        template<class TT1>
        explicit    PImpl(TT1&& p1);                       //!< ... � 1 argument.
        template<class TT1, class TT2>
        explicit    PImpl(TT1&& p1, TT2&& p2);             //!< ... � 2 argument.
        template<class TT1, class TT2, class TT3>
        explicit    PImpl(TT1&& p1, TT2&& p2, TT3&& p3);   //!< ... � 3 arguments.

                    PImpl(const PImpl& pOther);            //!< ... par copie.
        PImpl&      operator=(const PImpl& pOther);         //!< Op�rateur d'affectation
        //@}
                    ~PImpl();                              //!< Destructeur.
        void        swap(PImpl& pPImpl);                   //!< Permutation.
        //! @name Donn�es
        //@{
        TPrivate*   operator->() const;                    //!< Acc�s aux donn�es.
        TPrivate&   operator*() const;                     //!< D�r�f�rencement.


    private:

        TPrivate*   mData;                                  //!< Donn�es.
        //@}
    };


    /*! Permet de d�clarer un p-impl dans une classe (� utiliser dans la partie des d�clarations priv�es
        de cette classe).
        @ingroup MEM_Models
     */
#define PIMPL()                     \
        class Private;              \
        MEM::PImpl<Private> pthis;


    /*! Permet de d�clarer un p-impl (ind�pendant de MemoryMgr) dans une classe (� utiliser dans la
        partie des d�clarations priv�es de cette classe).
        @ingroup MEM_Models
     */
#define FREE_PIMPL()                            \
        class Private;                          \
        MEM::PImpl<Private, MEM::NoCheck> pthis;
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "PImpl.inl"

#endif  // De MEM_PIMPL_HH
