/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_MEMORYMGR_HH
#define MEM_MEMORYMGR_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/MemoryMgr.hh
    @brief En-t�te de la classe MEM::MemoryMgr.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstddef>

#include "PImpl.hh"
#include "Singleton.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                             Gestionnaire M�moire
//------------------------------------------------------------------------------

    /*! @brief Gestionnaire m�moire.
        @version 1.0
        @ingroup MEM_Allocation

        Le singleton de cette classe donne acc�s � la @ref MEM_Memory_Page de l'application : un tas
        contigu allou� � l'initialisation.
     */
    class MemoryMgr : public Singleton<MemoryMgr>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    MemoryMgr();                    //!< Constructeur par d�faut.
                    ~MemoryMgr();                   //!< Destructeur.
        //@}
        //! @name Allocation & d�sallocation g�n�rique
        //@{
        void*       allocate(std::size_t pSize);    //!< Allocation.
        void        deallocate(void* pPointer);     //!< D�sallocation.
        //@}
#ifdef MEMORY_CHECK
        //! @name Informations
        //@{
        std::size_t totalMemorySize() const;        //!< M�moire totale disponible.
        std::size_t allocatedMemorySize() const;    //!< M�moire allou�e demand�e.
        std::size_t usedMemorySize() const;         //!< M�moire allou�e totale.
        std::size_t spareMemorySize() const;        //!< M�moire "perdue".
        //@}
#endif  // De MEMORY_CHECK

    private:

        FORBID_COPY(MemoryMgr)
        FREE_PIMPL()
    };


//------------------------------------------------------------------------------
//                   D�finition du "Singleton Memory Manager"
//------------------------------------------------------------------------------

#define Memory  MEM::MemoryMgr::get()               //!< Gestionnaire m�moire.
}

#endif  // De MEM_MEMORYMGR_HH
