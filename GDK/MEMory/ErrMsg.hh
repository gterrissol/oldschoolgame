/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_ERRMSG_HH
#define MEM_ERRMSG_HH

/*! @file GDK/MEMory/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module @ref MEMory.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace MEM
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kAlignmentError;
    extern  ERR::String kAlreadyCheckedIndex;
    extern  ERR::String kAlreadyConstructedSingleton;
    extern  ERR::String kAlreadyDestructedSingleton;
    extern  ERR::String kAlreadyFilledChunk;
    extern  ERR::String kBufferOverflow;
    extern  ERR::String kChunkNotFound;
    extern  ERR::String kCorruptChunk;
    extern  ERR::String kCorruptData;
    extern  ERR::String kDoubleDeleteAttempt;
    extern  ERR::String kEmptyChunk;
    extern  ERR::String kEmptyChunkNotEmpty;
    extern  ERR::String kInvalidAlignment;
    extern  ERR::String kInvalidBlockSize;
    extern  ERR::String kInvalidChunk;
    extern  ERR::String kInvalidIndex;
    extern  ERR::String kInvalidPageSize;
    extern  ERR::String kInvalidPool;
    extern  ERR::String kInvalidPointer;
    extern  ERR::String kMoreThanOneEmptyChunk;
    extern  ERR::String kNotOneEmptyChunk;
    extern  ERR::String kNoChunkHasBeenPushed;
    extern  ERR::String kNoSingletonIsDefined;
    extern  ERR::String kNullBlockSize;
    extern  ERR::String kOverflowError;
    extern  ERR::String kRangeCheckError;
    extern  ERR::String kUninitializedChunk;
    extern  ERR::String kWrongChunk;
    //@}
    //! @name Message d'avertissement
    //@{
    extern ERR::String kDetectingMemoryLeaks;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De MEM_ERRMSG_HH
