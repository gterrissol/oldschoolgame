/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_ARRAY_HH
#define MEM_ARRAY_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/Array.hh
    @brief En-t�te de la classe MEM::Array.
    @author @ref Guillaume_Terrissol
    @date 23 Mars 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Class.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                                   Tableaux
//------------------------------------------------------------------------------

    /*! @brief Pseudo-tableau int�gr�.
        @version 1.0
        @ingroup MEM_Models

        <i>TT</i> : Type des �l�ments du tableau.<br>
        <i>TC</i> : Nombre d'�l�ments dans le tableau (param�tre d�fini uniquement dans la classe
        g�n�rique : seules les sp�cialisation d'Array seront utilis�es, d�finies pour des tailles de 1,
        2, 3 et 4 �l�ments).
     */
    template<class TT, int TC>
    class Array;


    /*! @brief Pseudo-tableau int�gr� de taille 1.
        @version 1.0
        @ingroup MEM_Models
     */
    template<class TT>
    class Array<TT, 1> : Auto<Array<TT, 1>>
    {
    public:
        //! @name Constructeurs
        //@{
                    Array();                        //!< ... par d�faut.
                    Array(TT p1);                   //!< ... "complet".
                    Array(const TT pD[1]);          //!< ... via un tableau int�gr�.
        //@}
        //! @name Acc�s aux �l�ments
        //@{
        const TT&   operator[](size_t pN) const;    //!< ... constant
        TT&         operator[](size_t pN);          //!< ... non-constant.
        //@}

    private:
        //! @name El�ment du tableau
        //@{
        TT  m1;                                     //!< Unique �l�ment.
        //@}
    };


    /*! @brief Pseudo-tableau int�gr� de taille 2.
        @version 1.0
        @ingroup MEM_Models
     */
    template<class TT>
    class Array<TT, 2> : Auto<Array<TT, 2>>
    {
    public:
        //! @name Constructeurs
        //@{
                    Array();                        //!< ... par d�faut.
                    Array(TT p1, TT p2);            //!< ... "complet".
                    Array(const TT pD[2]);          //!< ... via un tableau int�gr�.
        //@}
        //! @name Acc�s aux �l�ments
        //@{
        const TT&   operator[](size_t pN) const;    //!< ... constant
        TT&         operator[](size_t pN);          //!< ... non-constant.
        //@}

    private:
        //! @name El�ments du tableau
        //@{
        TT  m1;                                     //!< 1er �l�ment.
        TT  m2;                                     //!< 2nd �l�ment.
        //@}
    };


    /*! @brief Pseudo-tableau int�gr� de taille 3.
        @version 1.0
        @ingroup MEM_Models
     */
    template<class TT>
    class Array<TT, 3> : Auto<Array<TT, 3>>
    {
    public:
        //! @name Constructeurs
        //@{
                    Array();                        //!< ... par d�faut.
                    Array(TT p1, TT p2, TT p3);     //!< ... "complet".
                    Array(const TT pD[3]);          //!< ... via un tableau int�gr�.
        //@}
        //! @name Acc�s aux �l�ments
        //@{
        const TT&   operator[](size_t pN) const;     //!< ... constant
        TT&         operator[](size_t pN);           //!< ... non-constant.
        //@}

    private:
        //! @name El�ments du tableau
        //@{
        TT  m1;                                     //!< 1er �l�ment.
        TT  m2;                                     //!< 2�me �l�ment.
        TT  m3;                                     //!< 3�me �l�ment.
        //@}
    };


    /*! @brief Pseudo-tableau int�gr� de taille 4.
        @version 1.0
        @ingroup MEM_Models
     */
    template<class TT>
    class Array<TT, 4> : Auto<Array<TT, 4>>
    {
    public:
        //! @name Constructeurs
        //@{
                    Array();                            //!< ... par d�faut.
                    Array(TT p1, TT p2, TT p3, TT p4);  //!< ... "complet".
                    Array(const TT pD[4]);              //!< ... via un tableau int�gr�.
        //@}
        //! @name Acc�s aux �l�ments
        //@{
        const TT&   operator[](size_t pN) const;        //!< ... constant
        TT&         operator[](size_t pN);              //!< ... non-constant.
        //@}

    private:
        //! @name El�ments du tableau
        //@{
        TT  m1;                                         //!< 1er �l�ment.
        TT  m2;                                         //!< 2�me �l�ment.
        TT  m3;                                         //!< 3�me �l�ment.
        TT  m4;                                         //!< 4�me �l�ment.
        //@}
    };


    //! @name Operateurs
    //@{
    template<class TT, int TC>
    inline  bool    operator==(const Array<TT, TC>& pL, Array<TT, TC>& pR); //!< ... d'�galit�.
    template<class TT, int TC>
    inline  bool    operator!=(const Array<TT, TC>& pL, Array<TT, TC>& pR); //!< ... d'in�galit�.
    //@}
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Array.inl"

#endif  // De MEM_ARRAY_HH
