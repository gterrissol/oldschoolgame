/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MEMory/Allocator.inl
    @brief M�thodes inline de la classe MEM::Allocator.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 1er Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include "SmallObjectAllocator.hh"

namespace MEM
{
    /*! Teste l'�galit� de 2 allocateurs.
        @return Toujours VRAI
     */
    template<class TT>
    inline bool operator==(const Allocator<TT>&, const Allocator<TT>&)
    {
        return true;
    }


    /*! Teste la non-�galit� de 2 allocateurs.
        @return Toujours FAUX
     */
    template<class TT>
    inline bool operator!=(const Allocator<TT>&, const Allocator<TT>&)
    {
        return false;
    }

        
    /*! Constructeur par d�faut, vide.
     */
    template<class TT>
    inline Allocator<TT>::Allocator() throw() {}


    /*! Constructeur par copie, vide.
     */
    template<class TT>
    inline Allocator<TT>::Allocator(const Allocator&) throw() {}
    

    /*! Constructeur par copie d'un allocateur instanci� avec un autre param�tre de mod�le, vide.
     */
    template<class TT> template<class TU> 
    inline Allocator<TT>::Allocator(const Allocator<TU>&) throw() {}


    /*! Destructeur, vide.
     */
    template<class TT>
    inline Allocator<TT>::~Allocator() throw() {}


    /*! @param pX Variable dont on cherche l'adresse en m�moire
        @return L'adresse en m�moire de <i>pX</i>
     */
    template<class TT>
    inline typename Allocator<TT>::pointer Allocator<TT>::address(reference pX) const
    {
        return std::addressof(pX);
    }


    /*! @param pX Variable "constante" dont on cherche l'adresse en m�moire
        @return L'adresse en m�moire de <i>pX</i>
     */
    template<class TT>
    inline typename Allocator<TT>::const_pointer Allocator<TT>::address(const_reference pX) const
    {
        return std::addressof(pX);
    }


    /*! Alloue de la m�moire.
        @param pN Nombre d'�l�ments � allouer
        @return Un pointeur sur bloc m�moire allou� pour <i>pN</i> <i>TT</i>
     */
    template<class TT>
    inline typename Allocator<TT>::pointer Allocator<TT>::allocate(size_type pN, const void*)
    {
        if (pN != 0)
        {
            return static_cast<pointer>(SmallObjAllocator::get()->allocate(pN * sizeof(value_type)));
        }
        else
        {
            return nullptr;
        }
    }


    /*! Lib�re de la m�moire.
        @param pP Pointeur sur la zone m�moire � lib�rer.
        @param pN Nombre de <i>TT</i> � lib�rer.
     */
    template<class TT>
    inline void Allocator<TT>::deallocate(void* pP, size_type pN)
    {
        SmallObjAllocator::get()->deallocate(pP, pN * sizeof(value_type));
    }


    /*! Initialise *<i>pP</i> par des arguments.
        @param pP    Pointeur sur la zone m�moire o� cr�er un nouvel objet
        @param pArgs Arguments � passer au constructeur de l'objet � construire
     */
    template<class TT> template<class TU, class... TArgs>
    inline void Allocator<TT>::construct(TU* pP, TArgs&&... pArgs)
    {
        ::new(pP) TU(std::forward<TArgs>(pArgs)...);
    }


    /*! D�truit *<i>pP</i>, mais ne lib�re pas la m�moire.
        @param pP Pointer sur l'objet � d�truire
     */
    template<class TT> template<class TU>
    inline void Allocator<TT>::destroy(TU* pP)
    {
        pP->~TU();
    }

    /*! Initialise *<i>pP</i> par <i>pV</i>.
        @param pP Pointeur sur la zone m�moire o� placer un nouvel objet
        @param pV Valeur � utiliser pour l'initialisation de l'objet nouvellement cr��
     */
    template<class TT>
    inline void Allocator<TT>::construct(pointer pP, const_reference pV)
    {
        new(pP) value_type(pV);
    }


    /*! D�truit *<i>pP</i>, mais ne lib�re pas la m�moire.
        @param pP Pointer sur l'objet � d�truire
     */
    template<class TT>
    inline void Allocator<TT>::destroy(pointer pP)
    {
        pP->~TT();
    }


    /*! @return Le nombre maximum d'objets de type <i>value_type</i> pouvant �tre allou�s par
        l'allocateur.
     */
    template<class TT>
    inline typename Allocator<TT>::size_type Allocator<TT>::max_size() const throw()
    {
        return size_type(- 1) / sizeof(value_type);
    }
}
