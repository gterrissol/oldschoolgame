/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_BUILTIN_HH
#define MEM_BUILTIN_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/BuiltIn.hh
    @brief En-t�te de la classe MEM::BuiltIn.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 1er F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdint>

#include "Class.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                                  Type Int�gr�
//------------------------------------------------------------------------------

    /*! @defgroup MEM_Builtin Types int�gr�s
        @ingroup MEMory
     */

    /*! @brief Type int�gr�.
        @version 2.0
        @ingroup MEM_Builtin

        Cette classe permet l'allocation dynamique des types int�gr�s de base par le gestionnaire m�moire
        du module, sans avoir � red�finir les fonctions gobales <b>new</b> et <b>delete</b>.
     */
    template<typename TBaseType>
    class BuiltIn : public Auto<BuiltIn<TBaseType>>
    {
    public:
        //! @name Type public
        //@{
        typedef TBaseType   TType;                          //!< Param�tre de mod�le.
        //@}
        //! @name Constructeur
        //@{
        inline  explicit    BuiltIn(TBaseType pValue = 0);  //!< ... par valeur.
        //@}
        //! @name Op�rateurs
        //@{
        inline              operator TType() const;         //!< ... de conversion implicite.
        inline  BuiltIn     operator+() const;              //!< ... + unaire.
        inline  BuiltIn     operator-() const;              //!< ... - unaire.
        inline  BuiltIn&    operator++();                   //!< ... de pr�-incr�mentation.
        inline  BuiltIn     operator++(int);                //!< ... de post-incr�mentation.
        inline  BuiltIn&    operator--();                   //!< ... de pr�-d�cr�mentation.
        inline  BuiltIn     operator--(int);                //!< ... de post-incr�mentation.
        inline  BuiltIn&    operator+=(BuiltIn pOperand);   //!< ... +=.
        inline  BuiltIn&    operator-=(BuiltIn pOperand);   //!< ... -=.
        inline  BuiltIn&    operator*=(BuiltIn pOperand);   //!< ... *=.
        inline  BuiltIn&    operator/=(BuiltIn pOperand);   //!< ... /=.
        inline  BuiltIn&    operator&=(BuiltIn pOperand);   //!< ... &=.
        inline  BuiltIn&    operator|=(BuiltIn pOperand);   //!< ... |=.
        //@}

    private:

        TBaseType   mValue;                                 //!< Valeur.
    };


    /*! @defgroup MEM_Builtin_Arithmetic Op�rateurs arithm�tiques
        @ingroup MEM_Builtin
     */
    //@{
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator+(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Addition.
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator-(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Soustraction.
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator*(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Multiplication.
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator/(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Division.
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator&(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Et binaire.
    template<class TBaseType>
    inline BuiltIn<TBaseType>   operator|(BuiltIn<TBaseType> pL,
                                          BuiltIn<TBaseType> pR);       //!< Ou binaire.
    //@}
    /*! @defgroup MEM_Builtin_Ordering Op�rateurs d'ordre
        @ingroup MEM_Builtin
     */
    //@{
    template<class TBaseType, class>
    inline  bool            operator==(BuiltIn<TBaseType> pL,
                                       BuiltIn<TBaseType> pR);          //!< ... d'�galit�.
    template<class TBaseType>
    inline  bool            operator!=(BuiltIn<TBaseType> pL,
                                       BuiltIn<TBaseType> pR);          //!< ... d'in�galit�.
    template<class TBaseType>
    inline  bool            operator<=(BuiltIn<TBaseType> pL,
                                       BuiltIn<TBaseType> pR);          //!< Inf�rieur �.
    template<class TBaseType>
    inline  bool            operator<(BuiltIn<TBaseType> pL,
                                      BuiltIn<TBaseType> pR);           //!< Strictement inf�rieur �.
    //@}
    /*! @defgroup MEM_Builtin_Operators Autres op�rateurs
        @ingroup MEM_Builtin
     */
    //@}
    template<class TBaseType>
    inline const TBaseType* addressof(const BuiltIn<TBaseType>& pB);    //!< ... address-of (constant).
    template<class TBaseType>
    inline TBaseType*       addressof(BuiltIn<TBaseType>& pB);          //!< ... address-of.
    template<class TBaseType>
    inline  LOG::Log&       operator<<(LOG::Log&          pLog,
                                       BuiltIn<TBaseType> pB);          //!< Ecriture d'un nombre.
    //@}
}


//------------------------------------------------------------------------------
//                       Instanciation des Types Int�gr�s
//------------------------------------------------------------------------------

    /*! @defgroup MEM_Builtin_Types Typedefs
        @ingroup MEM_Builtin
     */
    //@{
    typedef MEM::BuiltIn<int8_t>    I8;     //!< Entier 8  bits.
    typedef MEM::BuiltIn<int16_t>   I16;    //!< Entier 16 bits.
    typedef MEM::BuiltIn<int32_t>   I32;    //!< Entier 32 bits.
    typedef MEM::BuiltIn<int64_t>   I64;    //!< Entier 64 bits.
    typedef MEM::BuiltIn<uint8_t>   U8;     //!< Entier 8  bits non-sign�.
    typedef MEM::BuiltIn<uint16_t>  U16;    //!< Entier 16 bits non-sign�.
    typedef MEM::BuiltIn<uint32_t>  U32;    //!< Entier 32 bits non-sign�.
    typedef MEM::BuiltIn<uint64_t>  U64;    //!< Entier 64 bits non-sign�.
    typedef MEM::BuiltIn<float>     F32;    //!< R�el 32 bits.
    typedef MEM::BuiltIn<double>    F64;    //!< R�el 64 bits.
    //@}


//------------------------------------------------------------------------------
//                                     Z�ro
//------------------------------------------------------------------------------

    const I8    k0B     = I8(0);        //!< 0
    const U8    k0UB    = U8(0);        //!< 0
    const I16   k0W     = I16(0);       //!< 0
    const U16   k0UW    = U16(0);       //!< 0
    const I32   k0L     = I32(0);       //!< 0
    const U32   k0UL    = U32(0);       //!< 0
    const I64   k0LL    = I64(0);       //!< 0
    const U64   k0ULL   = U64(0);       //!< 0
    const F32   k0F     = F32(0.0F);    //!< 0.0F
    const F32   k0D     = F32(0.0);     //!< 0.0

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "BuiltIn.inl"

#endif  // De MEM_BUILTIN_HH
