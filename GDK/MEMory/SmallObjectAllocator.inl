/* The Loki Library
   Copyright (c) 2001 by Andrei Alexandrescu
   This code accompanies the book:
   Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design 
       Patterns Applied". Copyright (c) 2001. Addison-Wesley.
   Permission to use, copy, modify, distribute and sell this software for any 
       purpose is hereby granted without fee, provided that the above  copyright 
       notice appear in all copies and that both that copyright notice and this 
       permission notice appear in supporting documentation.
   The author or Addison-Wesley Longman make no representations about the 
       suitability of this software for any purpose. It is provided "as is" 
       without express or implied warranty.
 */

/*! @file GDK/MEMory/SmallObjectAllocator.inl
    @brief M�thodes inline de la classe MEM::SmallObjAllocator.
    @author Code original : Andrei Alexandrescu
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 1er Mai 2007 - 11 Septembre 2011
    @note "Portions Copyright (c) 2001 by Andrei Alexandrescu"
 */

namespace MEM
{
//------------------------------------------------------------------------------
//                                 Informations
//------------------------------------------------------------------------------

    /*! @return La taille, en octets, du plus gros objet que cette instance peut allouer
     */
    inline std::size_t SmallObjAllocator::maxObjectSize() const
    {
        return mMaxSmallObjectSize;
    }


    /*! @return Le nombre d'octets s�parant deux allocations cons�cutives
     */
    inline std::size_t SmallObjAllocator::alignment() const
    {
        return mObjectAlignSize;
    }
}
