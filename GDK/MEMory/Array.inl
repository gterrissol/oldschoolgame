/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MEMory/Array.inl
    @brief M�thodes inline de la classe MEM::Array.
    @author @ref Guillaume_Terrissol
    @date 23 Mars 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MEM
{
//------------------------------------------------------------------------------
//                                   Tableaux
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TT>
    Array<TT, 1>::Array()
        : m1(0)
    { }


    /*! Constructeur initialisant ses �l�ments.
        @param p1 Valeur pour l'�l�ment
     */
    template<class TT>
    Array<TT, 1>::Array(TT p1)
        : m1(p1)
    { }


    /*! Constructeur initialisant ses �l�ments via un tableau int�gr�.
        @param pD Tableau d'�l�ments
     */
    template<class TT>
    Array<TT, 1>::Array(const TT pD[1])
        : m1(pD[0])
    { }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    const TT& Array<TT, 1>::operator[](size_t pN) const
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
        }
    }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    TT& Array<TT, 1>::operator[](size_t pN)
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
        }
    }


    /*! Constructeur par d�faut.
     */
    template<class TT>
    Array<TT, 2>::Array()
        : m1(0)
        , m2(0)
    { }


    /*! Constructeur initialisant ses �l�ments.
        @param p1 Valeur pour le 1er �l�ment
        @param p2 Valeur pour le 2nd �l�ment
     */
    template<class TT>
    Array<TT, 2>::Array(TT p1, TT p2)
        : m1(p1)
        , m2(p2)
    { }


    /*! Constructeur initialisant ses �l�ments via un tableau int�gr�.
        @param pD Tableau d'�l�ments
     */
    template<class TT>
    Array<TT, 2>::Array(const TT pD[2])
        : m1(pD[0])
        , m2(pD[1])
    { }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    const TT& Array<TT, 2>::operator[](size_t pN) const
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
        }
    }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    TT& Array<TT, 2>::operator[](size_t pN)
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
        }
    }


    /*! Constructeur par d�faut.
     */
    template<class TT>
    Array<TT, 3>::Array()
        : m1(0)
        , m2(0)
        , m3(0)
    { }


    /*! Constructeur initialisant ses �l�ments.
        @param p1 Valeur pour le 1er �l�ment
        @param p2 Valeur pour le 2�me �l�ment
        @param p3 Valeur pour le 3�me �l�ment
     */
    template<class TT>
    Array<TT, 3>::Array(TT p1, TT p2, TT p3)
        : m1(p1)
        , m2(p2)
        , m3(p3)
    { }


    /*! Constructeur initialisant ses �l�ments via un tableau int�gr�.
        @param pD Tableau d'�l�ments
     */
    template<class TT>
    Array<TT, 3>::Array(const TT pD[3])
        : m1(pD[0])
        , m2(pD[1])
        , m3(pD[2])
    { }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    const TT& Array<TT, 3>::operator[](size_t pN) const
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
            case 2: return m3;
        }
    }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    TT& Array<TT, 3>::operator[](size_t pN)
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
            case 2: return m3;
        }
    }


    /*! Constructeur par d�faut.
     */
    template<class TT>
    Array<TT, 4>::Array()
        : m1(0)
        , m2(0)
        , m3(0)
        , m4(0)
    { }


    /*! Constructeur initialisant ses �l�ments.
        @param p1 Valeur pour le 1er �l�ment
        @param p2 Valeur pour le 2�me �l�ment
        @param p3 Valeur pour le 3�me �l�ment
        @param p4 Valeur pour le 4�me �l�ment
     */
    template<class TT>
    Array<TT, 4>::Array(TT p1, TT p2, TT p3, TT p4)
        : m1(p1)
        , m2(p2)
        , m3(p3)
        , m4(p4)
    { }


    /*! Constructeur initialisant ses �l�ments via un tableau int�gr�.
        @param pD Tableau d'�l�ments
     */
    template<class TT>
    Array<TT, 4>::Array(const TT pD[4])
        : m1(pD[0])
        , m2(pD[1])
        , m3(pD[2])
        , m4(pD[3])
    { }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    const TT& Array<TT, 4>::operator[](size_t pN) const
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
            case 2: return m3;
            case 3: return m4;
        }
    }


    /*! @param pN Indice de l'�l�ment souhait�
        @return Le <i>pN</i>i�me �l�ment du tableau
     */
    template<class TT>
    TT& Array<TT, 4>::operator[](size_t pN)
    {
        switch(pN)
        {
            default: ASSERT(false, kRangeCheckError);
            case 0: return m1;
            case 1: return m2;
            case 2: return m3;
            case 3: return m4;
        }
    }


    /*! @brief Comparateur d'instances de Array\<<i>TT</i>, <i>TC</i>\>
        @version 1.0
        @ingroup MEM_Models
        Compare r�cursivement (par m�ta-programmation) deux tableau de <i>TT</i>, � partir du sommet.
        @param pRefArray Premier tableau de valeurs � comparer
        @param pArray    Second tableau de valeurs � comparer
        @note Cette fonction n'est �videmment utile que pour d'autres templates
        @note Une version identique de DO() existe dans UTIlity (c'est la version originale). Je l'ai
        dupliqu�e ici (dans une classe, car il est impossible de sp�cialiser partiellement une fonction)
        pour l'impl�mentation de inline bool operator==(const Array<TT, TC>& pL, Array<TT, TC>& pR)
     */
    template<class TT, int TC>
    struct CmpN
    {
 static inline bool perform(const TT pRefArray[], const TT pArray[])
        {
            return ((pRefArray[TC - 1] == pArray[TC - 1]) && CmpN<TT, TC - 1>::perform(pRefArray, pArray));
        }
    };


    /*! @brief Sp�cialisation CmpN (arr�t de la r�cursivit�).
        @version 1.0
        @ingroup MEM_Models
     */
    template<class TT>
    struct CmpN<TT, 0>
    {
 static inline bool perform(const TT[], const TT[])
        {
            return true;
        }
    };


    /*! @ingroup MEM_Models
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si les 2 tableaux sont identiques, FAUX sinon
     */
    template<class TT, int TC>
    inline bool operator==(const Array<TT, TC>& pL, Array<TT, TC>& pR)
    {
        return CmpN<TT, TC>::perform(&pL[0], &pR[0]);
    }


    /*! @ingroup MEM_Models
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return FAUX si les 2 tableaux sont identiques, VRAI sinon
     */
    template<class TT, int TC>
    inline bool operator!=(const Array<TT, TC>& pL, Array<TT, TC>& pR)
    {
        return !(pL == pR);
    }
}
