/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

#ifndef MEM_SINGLETON_HH
#define MEM_SINGLETON_HH

#include "MEMory.hh"

/*! @file GDK/MEMory/Singleton.hh
    @brief En-t�te de la classe MEM::Singleton.
    @author Code original : Scott Bilas
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 11 Septembre 2011
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

namespace MEM
{
//------------------------------------------------------------------------------
//                               Classe Singleton
//------------------------------------------------------------------------------

    /*! @brief Singleton.
        @version 2.0
        @ingroup MEM_Models

        Cette classe permet de contraindre une classe � n'avoir qu'une seule instance.<br>
        Pour l'utiliser, il suffit d'en d�river une classe :<br>
        @code
    class SingletonClass : public MEM::Singleton<SingletonClass>
    {
        ...
    private:

        FORBID_COPY(SingletonClass)
        ...
    };
        @endcode
        Au d�but du programme, il suffit d'allouer cette instance unique.
        @code
        SingletonClass* lMySingleton    = new SingletonClass(...);
        @endcode
        On peut ensuite y acc�der via la m�thode MEM::Singleton::get().<br>
        N'oubliez pas de d�truire l'instance � la fin du programme (pas de garbage collector).
        @note <b>Attention</b> : Singleton ne d�rive pas de MEM::OnHeap : n'oubliez pas de d�river
        votre singleton de MEM::OnHeap, ou d'une de ses classes d�riv�es au besoin
     */
    template<typename TType>
    class Singleton
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Singleton();    //!< Constructeur par d�faut.
                        ~Singleton();   //!< Destructeur.
        //@}
        //! @name M�thode d'acc�s
        //@{
 static inline TType*   get();          //!< Acc�s au singleton via un pointeur.
        //@}

    private:

        FORBID_COPY(Singleton)

        static TType*   smThat;         //!< Instance unique de la classe singleton.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Singleton.inl"

#endif  // De MEM_SINGLETON_HH
