/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

/*! @file GDK/MEMory/Singleton.inl
    @brief M�thodes inline de la classe MEM::Singleton.
    @author Code original : Scott Bilas
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 7 F�vrier 2018
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

#include "ERRor/Assert.hh"

#include "ErrMsg.hh"

namespace MEM
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : cr�e l'instance unique de la classe.
        @note Ce constructeur ne doit �tre appel� qu'une fois (ou apr�s avoir d�truit l'instance)
     */
    template<typename TType>
    Singleton<TType>::Singleton()
    {
        ASSERT_EX(smThat == nullptr, kAlreadyConstructedSingleton, return;)

        smThat = static_cast<TType*>(this);
    }


    /*! Destructeur : d�truit l'instance unique de la classe.
        @note Ce destructeur ne doit �tre appel� qu'une fois (pour chaque construction d'instance)
     */
    template<typename TType>
    Singleton<TType>::~Singleton()
    {
        ASSERT_NOTHROW(smThat != nullptr, kAlreadyDestructedSingleton)

        smThat = nullptr;
    }


//------------------------------------------------------------------------------
//                                M�thode d'Acc�s
//------------------------------------------------------------------------------

    /*! @return L'instance unique de la classe (via un pointeur)
     */
    template<typename TType>
    inline TType* Singleton<TType>::get()
    {
        ASSERT(smThat != nullptr, kNoSingletonIsDefined)

        return smThat;
    }


//------------------------------------------------------------------------------
//                                   Singleton
//------------------------------------------------------------------------------

#ifndef NOT_FOR_DOXYGEN
#   if !defined(_WIN32)
    template<typename TType>    TType*  Singleton<TType>::smThat    = nullptr;
#   endif
#endif  // De NOT_FOR_DOXYGEN
}
