/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_MEMORY_HH
#define MEM_MEMORY_HH

/*! @file MEMory/MEMory.hh
    @brief Pr�-d�clarations du module @ref MEMory.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2007 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MEM
{
    class MemoryMgr;
    class OnHeap;

    template<class TT>                      class Allocator;
    template<class TT, int TC>              class Array;
    template<typename TT>                   class Auto;
    template<typename TBaseType>            class BuiltIn;
    template<class TPrivate, class TCheck>  class PImpl;
    template<typename TType>                class Singleton;
    template<class TP>                      class UniquePtr;

    /*! Permet d'interdire l'utilisation des constructeur et op�rateur de copie.
        @ingroup MEM_Models
     */
#   define FORBID_COPY(Class)                   \
                Class(const Class&) = delete;   \
        Class&  operator=(const Class&) = delete;
}

#endif  // De MEM_MEMORY_HH
