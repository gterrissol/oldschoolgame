/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEM_ALL_HH
#define MEM_ALL_HH

/*! @file GDK/MEMory/All.hh
    @brief Interface publique du module @ref MEMory.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MEM   //! Gestion de la m�moire.
{
    /*! @namespace MEM
        @version 1.0
        @ingroup MEMory

        Pour plus d'informations, consultez la page @ref MEM_Memory_Page.
     */

    /*! @defgroup MEMory MEMory : Gestion m�moire
        <b>namespace</b> MEM, @ref MEM_Allocation, @ref MEM_Builtin & @ref MEM_Models.
     */

    /*! @defgroup MEM_Allocation Allocation m�moire
        @ingroup MEMory
     */

    /*! @defgroup MEM_Models Mod�les m�moire pour les instances de classes
        @ingroup MEMory
      */
}

#include "Singleton.hh"
#include "MemoryMgr.hh"
#include "Allocator.hh"
#include "Class.hh"
#include "Array.hh"
#include "PImpl.hh"
#include "BuiltIn.hh"

#endif  // De MEM_ALL_HH
