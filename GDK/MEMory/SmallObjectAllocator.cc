/* The Loki Library
   Copyright (c) 2001 by Andrei Alexandrescu
   This code accompanies the book:
   Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design 
       Patterns Applied". Copyright (c) 2001. Addison-Wesley.
   Permission to use, copy, modify, distribute and sell this software for any 
       purpose is hereby granted without fee, provided that the above  copyright 
       notice appear in all copies and that both that copyright notice and this 
       permission notice appear in supporting documentation.
   The author or Addison-Wesley Longman make no representations about the 
       suitability of this software for any purpose. It is provided "as is" 
       without express or implied warranty.
 */

#include "SmallObjectAllocator.hh"

/*! @file GDK/MEMory/SmallObjectAllocator.cc
    @brief D�claration et m�thodes des MEM::Chunk.
    @author Code original : Andrei Alexandrescu
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 29 Avril 2007 - 1er F�vrier 2015
    @note "Portions Copyright (c) 2001 by Andrei Alexandrescu"

    J'ai repris le code, plut�t qu'utilis� Loki directement pour pouvoir utiliser la @ref MEM_Memory_Page
    du moteur.
 */

#include <bitset>
#include <climits>
#include <mutex>
#include <vector>

#include "ERRor/Assert.hh"

#include "BuiltIn.hh"
#include "MemoryMgr.hh"


#undef CHECK_FOR_CORRUPTION

namespace MEM
{
    const uint8_t   kMaxBlockCount  = 255;  //!< Nombre maximal de bloc dans une instance de MEM::Chunk.

//------------------------------------------------------------------------------
//                              Classes Assistantes
//------------------------------------------------------------------------------

    /*! @brief Collection de blocs de m�moire contigus.
        @version 1.0
        @internal

        Contient des informations sur chaque chunk allou�. Chaque bloc est de m�me taille, telle que
        sp�cifi�e par FixedAllocator. Le nombre de blocs dans un chunk d�pend de la taille de page.
        C'est une structure de type POD.
        
        @par Interface minimale
        A des fins de performance � l'ex�cution, aucun constructeur, destructeur ou op�rateur de copie ou
        affectation n'est d�fini. Les versions inline g�n�r�es par le compliateur devraient �tre
        suffisantes, et peut-�tre plus rapides que des m�thodes cod�es � la main.
        L'absence de ces m�thodes permet � std::vector de cr�er et copier des chunks sans surco�t. Les
        m�thodes init() et release() font ce que feraient un constructeur par d�faut et un destructeur.
        Un chunk n'est dans un �tat exploitable apr�s sa construction qu'apr�s avoir appel� init(). De
        m�me, il ne l'est plus apr�s que release() a �t� appel�e.

        @par Efficience
        Pr�s du plus bas niveau de l'allocateur, les performances surpassent toute autre consid�ration.
        Chaque m�thode fait le minimum attendu.

        @par Indices "furtifs"
        Le premier caract�re de chaque bloc contient l'index du prochain bloc vide. Ces indices forment
        une liste simplement cha�n�e � l'int�rieur des blocs. A chunk est corrompu si cette liste a une
        boucle ou est plus courte que mAvailableBlockCount. Une bonne part de l'efficience de
        l'allocateur, en temps et en espace, vient de la fa�on dont ces indices sont impl�ment�s.
     */
    class Chunk
    {
    public:
        //! @name "Constructeurs" & "destructeur"
        //@{
                bool    init(std::size_t pBlockSize,
                             uint8_t     pBlockCount);                      //!< Initialisation.
                void    reset(std::size_t pBlockSize,
                              uint8_t     pBlockCount);                     //!< R�initialisation (litt�ralement).
                void    release();                                          //!< Lib�ration.
        //@}
        //! @name Allocation & d�sallocation
        //@{
                void*   allocate(std::size_t pBlockSize);                   //!< Allocation.
                void    deallocate(void*       pPointer,
                                   std::size_t pBlockSize);                 //!< D�sallocation.
        //@}
        //! @name Tests
        //@{
                bool    isCorrupt(std::size_t pBlockSize,
                                  uint8_t     pBlockCount,
                                  bool        pCheckIndices) const;         //!< Test de corruption.
                bool    isBlockAvailable(void*       pPointer,
                                         std::size_t pBlockSize,
                                         uint8_t     pBlockCount) const;    //!< Disponibilit� de bloc.
        inline  bool    hasBlock(void* pPointer,
                                 std::size_t pChunkLength) const;           //!< Possession de bloc.
        inline  bool    hasAvailable(uint8_t pBlockCount) const;            //!< Disponibilit� m�moire.
        inline  bool    isFilled() const;                                   //!< Rempli ?
        //@}
        //! @name Attributs
        //@{
        uint8_t*    mData;                                                  //!< Tableau de blocs allou�s.
        uint8_t     mFirstAvailableBlock;                                   //!< Index du 1er bloc vide.
        uint8_t     mAvailableBlockCount;                                   //!< Nombre de blocs vides.
        //@}
    };


    /*! @brief Allocateur de "taille fixe".
        @version 1.0
        @internal

        Offre des services pour allouer des objets d'une taille fixe. Une instance de cette classe
        poss�de un conteneur de "conteneurs" de blocs de taille fixe. Le conteneur "externe" porte tous
        les chunks. Le conteneur "interne" est un chunk qui poss�de quelques blocs.

        @par Invariants de classe
        - Il y a toujours soit z�ro, soit un chunk vide,
        - S'il n'a aucun chunk vide, alors mEmptyChunk est nul,
        - S'il y a un chunk vide, alors mEmptyChunk pointe dessus,
        - Si le conteneur de chunks est vide, alors mDeallocChunk et mAllocChunk sont nuls,
        - Si le conteneur de chunks n'est pas vide, alors soit mDeallocChunk et mAllocChunk sont
        nuls, soit ils pointent � l'int�rieur du conteneur,
        - mAllocChunk pointera souvent sur le dernier chunk dans le conteneur, puisqu'il sera
        vraisemblablement le plus r�cemment allou�, et devrait donc contenir un bloc disponible.
     */
    class FixedAllocator
    {

    public:
        //! @name "Constructeurs" et destructeur
        //@{
                                FixedAllocator();                   //!< Constructeur par d�faut.
                                ~FixedAllocator();                  //!< Destructeur.
                void            initialize(std::size_t pBlockSize,
                                           std::size_t pPageSize);  //!< Initialisation.
        //@}
        //! @name Allocation & d�sallocation
        //@{
                void*           allocate();                         //!< Allocation.
                bool            deallocate(void*   pPointer,
                                               Chunk* pHint);          //!< D�sallocation.
        //@}
        //! @name Informations
        //@{
        inline  std::size_t     blockSize() const;                  //!< Taille de bloc.
        inline  std::size_t     countEmptyChunks() const;           //!< Nombre de chunks vide.
        //@}
        //! @name Lib�ration m�moire
        //@{
                bool            trimEmptyChunk();                   //!< ... des chunks vides.
                bool            trimChunkList();                    //!< ... des emplacements de liste inutilis�s.
        //@}
        //! @name Tests
        //@{
                bool            isCorrupt() const;                  //!< ... de corruption.
                const Chunk*    hasBlock(void* pPointer) const;     //!< ... de possession d'un bloc.
        inline  Chunk*          hasBlock(void* pPointer);           //!< ... de possession d'un bloc.
        //@}

    private:

        FORBID_COPY(FixedAllocator)

                void            doDeallocate(void* pPointer);       //!< D�sallocation effective.
                bool            makeNewChunk();                     //!< Cr�ation d'un nouveau chunk.
                Chunk*          vicinityFind(void* pPointer) const; //!< Recherche d'un chunk par voisinage.
        //! @name D�finition de types
        //@{
        typedef std::vector<Chunk>     Chunks;                      //!< Type de conteneur utilis� pour "porter" les chunks.
        typedef Chunks::iterator       ChunkIter;                   //!< It�rateur sur Chunks.
        typedef Chunks::const_iterator ChunkCIter;                  //!< It�rateur constant sur Chunks.
        //@}
        //! @name Attributs
        //@{
        Chunks      mChunks;                                        //!< Conteneur de chunks.
        Chunk*      mAllocChunk;                                    //!< Pointeur sur le chunk utilis� pour la derni�re ou la prochaine allocation.
        Chunk*      mDeallocChunk;                                  //!< Pointeur sur le chunk utilis� pour la derni�re ou la prochaine d�sallocation.
        Chunk*      mEmptyChunk;                                    //!< Pointeur sur le seul chunk vide, s'il existe, nullptr sinon.
        std::size_t mBlockSize;                                     //!< Nombre d'octets dans un seul bloc de chunk
        uint8_t     mBlockCount;                                    //!< Nombre de blocs g�r�s par chaque chunk.
        //@}
    };


    namespace
    {
        const uint8_t   kMinObjectsPerChunk = 8;            //!< Plus petit nombre d'objets port�s par un chunk.
        const uint8_t   kMaxObjectsPerChunk = UCHAR_MAX;    //!< Plus grand nombre d'objets port�s par un chunk.

        std::mutex  gMutex; //!< Mutex de MEM::SmallObjectAllocator.
    }


//------------------------------------------------------------------------------
//                    Chunk : "Constructeurs" & "Destructeur"
//------------------------------------------------------------------------------

    /*! Initialise un Chunk tout juste construit.
        @param pBlockSize  Nombre d'octets par bloc
        @param pBlockCount Nombre de blocs � rendre disponibles dans le chunk
        @return VRAI en cas de succ�s, FAUX en cas d'�chec
     */
    bool Chunk::init(std::size_t pBlockSize, uint8_t pBlockCount)
    {
        ASSERT(0 < pBlockSize, kNullBlockSize)
        ASSERT(0 < pBlockCount, kEmptyChunk)

        // Contr�le de non-d�passement.
        const std::size_t   lAllocSize  = pBlockSize * pBlockCount;
        ASSERT(lAllocSize / pBlockSize == pBlockCount, kOverflowError)

        if ((mData = static_cast<uint8_t*>(Memory->allocate(lAllocSize))) != nullptr)
        {
            reset(pBlockSize, pBlockCount);
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! R�inialise le chunk � son �tat par d�faut :
        - nombre de blocs disponibles fix�s au maximum (<i>pBlockCount</i>),
        - premier bloc disponible est celui d'indice 0,
        - les index de recherche dans chaque bloc "pointent" vers le bloc suivant.
        @param pBlockSize  Nombre d'octets par bloc
        @param pBlockCount Nombre de blocs � rendre disponibles dans le chunk
     */
    void Chunk::reset(std::size_t pBlockSize, uint8_t pBlockCount)
    {
        ASSERT(0 < pBlockSize, kNullBlockSize)
        ASSERT(0 < pBlockCount, kEmptyChunk)

        // Contr�le de non-d�passement.
        ASSERT((pBlockSize * pBlockCount) / pBlockSize == pBlockCount, kOverflowError)

        mFirstAvailableBlock    = 0;
        mAvailableBlockCount    = pBlockCount;

        uint8_t lInitValue = 0;
        for(uint8_t* lPieceOfData = mData; lInitValue != pBlockCount; lPieceOfData += pBlockSize)
        {
            *lPieceOfData = ++lInitValue;
        }
    }


    /*! Lib�re la m�moire allou�e pour le chunk.
     */
    void Chunk::release()
    {
        ASSERT(nullptr != mData, kUninitializedChunk)

        Memory->deallocate(mData);
    }


//------------------------------------------------------------------------------
//                      Chunk : Allocation & D�sallocation
//------------------------------------------------------------------------------

    /*! Alloue un bloc � l'int�rieur du chunk. La complexit� est toujours de O(1), et aucune exception ne
        sera jamais lanc�e.<br>
        Alloue en utilisant la @ref MEM_Memory_Page. N'alloue pas en appelant <b>new</b>, <b>malloc</b>
        ou une autre fonction, mais juste en manipulant des indices pour indiquer qu'un bloc allou� n'est
        plus disponible.
        @param pBlockSize Nombre d'octets par bloc
        @return Un pointeur vers un bloc de ce chunk, 0 si plus aucun bloc n'�tait disponible
     */
    void* Chunk::allocate(std::size_t pBlockSize)
    {
        if (isFilled())
        {
            return nullptr;
        }

        ASSERT_EX((std::size_t(mFirstAvailableBlock) * pBlockSize) / pBlockSize == std::size_t(mFirstAvailableBlock),
                  kOverflowError,
                  return nullptr;)

        uint8_t*    lResult = mData + (mFirstAvailableBlock * pBlockSize);
        mFirstAvailableBlock = *lResult;
        --mAvailableBlockCount;

        return lResult;
    }


    /*! D�salloue un bloc � l'int�rieur du chunk. La complexit� est toujours O(1), et aucune exception ne
        sera jamais lanc�e. Pour les performances, l'adresse <i>pPointer</i> est suppos�e appartenir au
        bloc, et correctement align�e. Ne d�salloue pas r�ellement la m�moire, mais ajuste des indices
        inernes pour indiquer qu'un bloc est disponible.
        @param pPointer   Pointeur vers le bloc � "d�sallouer"
        @param pBlockSize Nombre d'octets par bloc
     */
    void Chunk::deallocate(void* pPointer, std::size_t pBlockSize)
    {
        ASSERT(mData <= pPointer, kInvalidPointer)

        uint8_t*    lToRelease   = static_cast<uint8_t*>(pPointer);
        // Contr�le de l'alignment.
        ASSERT((lToRelease - mData) % pBlockSize == 0, kAlignmentError)

        uint8_t lIndex  = static_cast<uint8_t>((lToRelease - mData) / pBlockSize);

#if defined(DEBUG)
        // V�rifie si le bloc a d�j� �t� effac�. Une tentative d'effacement multiple du m�me bloc
        // corrompt la liste d'indices "furtifs". Et rend faux le nombre de blocs disponibles
        // (mAvailableBlockCount).
        if (0 < mAvailableBlockCount)
        {
            ASSERT(mFirstAvailableBlock != lIndex, kDoubleDeleteAttempt)
        }
#endif  // De DEBUG

        *lToRelease = mFirstAvailableBlock;
        mFirstAvailableBlock = lIndex;
        // V�rification de tronquage.
        ASSERT(mFirstAvailableBlock == (lToRelease - mData) / pBlockSize, kAlignmentError)

        ++mAvailableBlockCount;
    }


//------------------------------------------------------------------------------
//                                 Chunk : Tests
//------------------------------------------------------------------------------

    /*! D�termine si le chunk a �t� corrompu.
        @param pBlockSize    Nombre d'octets par bloc
        @param pBlockCount   Nombre de blocs disponibles dans le chunk
        @param pCheckIndices VRAI si l'appelant veut aussi contr�ler les indices de blocs disponibles
               pour une �ventuelle corruption. FAUX si l'appelant veut passer quelques tests pour aller
               plus vite
        @return VRAI si le chunk est corrompu, FAUX sinon
     */
    bool Chunk::isCorrupt(std::size_t pBlockSize, uint8_t pBlockCount, bool pCheckIndices) const
    {
        if (pBlockCount < mAvailableBlockCount)
        {
            // Le contenu de ce chunk est corrompu. Cela peut vouloir dire que quelque chose a �cras� la
            // m�moire du conteneur de chunks.
            ASSERT(false, kCorruptChunk)
            return true;
        }
        if (isFilled())
        {
            // Inutile de faire davantage de v�rifications si tous les blocs sont allou�s
            return false;
        }
        uint8_t lIndex    = mFirstAvailableBlock;
        if (pBlockCount <= lIndex)
        {
            // Le contenu de ce chunk est corrompu. Cela peut vouloir dire que
            // quelque chose a �cras� la m�moire du conteneur de chunks.
            ASSERT(false, kCorruptChunk)
            return true;
        }
        if (!pCheckIndices)
        {
            // L'appelant a choisi d'�viter des tests de corruption plus complexes.
            return false;
        }
        // Si le bit � lIndex dans lFoundBlocks est � 1,
        // c'est que cet index a �t� trouv� dans la liste cha�n�e.
        std::bitset<UCHAR_MAX>  lFoundBlocks;
        uint8_t*                lNextBlock  = nullptr;

        /*  La boucle parcourt la liste simplement cha�n�e d'indices "furtifs", s'assure que chaque index
            est bien born� (0 <= index < pBlockCount) et n'a pas d�j� �t� trouv� dans la liste. Elle
            devrait avoir exactement mAvailableBlockCount noeuds, la boucle n'it�rera pas davantage de
            fois. Cette boucle ne v�rifie pas l'int�rieur des blocs allou�s puisque ces blocs ne sont pas
            dans la liste cha�n�e, et leur contenu n'est pas modifi� par le chunk.

            Voici les types de liste corrompues qui peuvent �tre v�rifi�es. L'index corrompu est �crit
            entre ast�risques dans chaque exemple.

            Type 1 : l'index est trop grand.
            pBlockCount == 64
            mAvailableBlockCount == 7
            mFirstAvailableBlock -> 17 -> 29 -> *101*
            Il ne devrait y avoir aucun index sup�rieur au nombre total de blocs. Un tel index ferait
            r�f�rence � un bloc au-del� du domaine d'allocation du chunk.
            
            Type 2 : l'index est redondant.
            pBlockCount == 64
            mAvailableBlockCount == 5
            mFirstAvailableBlock -> 17 -> 29 -> 53 -> *17* -> 29 -> 53 ...
            Aucun index ne devrait �tre r�p�t� dans la liste puisque cel� indiquerait la pr�sence d'une
            boucle dans la liste.
         */
        for(uint8_t lCounter = 0; ;)
        {
            lNextBlock = mData + (lIndex * pBlockSize);
            lFoundBlocks.set(lIndex, true);
            ++lCounter;
            if (mAvailableBlockCount <= lCounter)
            {
                // A d�compt� avec succ�s le nombre de noeuds dans la liste.
                break;
            }
            lIndex = *lNextBlock;
            if (pBlockCount <= lIndex)
            {
                // Corruption de type 1 : cel� implique qu'un bloc a �t� corrompu � cause d'un
                // pointeur "�gar�" ou d'une op�ration sur un bloc proche qui a d�pass� sa taille.
                ASSERT(false, kCorruptChunk)
                return true;
            }
            if (lFoundBlocks.test(lIndex))
            {
                // Corruption de type 2 : cel� implique qu'un bloc a �t� corrompu � cause d'un pointeur
                // "�gar�" ou d'une op�ration sur un bloc proche qui a d�pass� sa taille. Ou peut-�tre
                // parce que le programme a essay� d'effacer un bloc plusieurs fois.
                ASSERT(false, kCorruptChunk)
                return true;
            }
        }
        if (lFoundBlocks.count() != mAvailableBlockCount)
        {
            // Cel� implique que la lsite d'indices a �t� corrompue. Id�alement cel� devrait avoir �t�
            // d�tect� � l'int�rieur de la boucle.
            ASSERT(false, kCorruptChunk)
            return true;
        }

        return false;
    }


    /*! D�termine si un bloc est disponible.
        @param pPointer    Adresse d'un bloc g�r� par le chunk
        @param pBlockSize  Nombre d'octets par bloc
        @param pBlockCount Nombre de blocs disponibles dans le chunk
        @return VRAI si le bloc est disponible, FAUX sinon
     */
    bool Chunk::isBlockAvailable(void* pPointer, std::size_t pBlockSize, uint8_t pBlockCount) const
    {
        if (isFilled())
        {
            return false;
        }

        uint8_t*    lPlace      = static_cast<uint8_t*>(pPointer);
        // Contr�le de l'alignment.
        ASSERT((lPlace - mData) % pBlockSize == 0, kAlignmentError)

        uint8_t lBlockIndex = static_cast<uint8_t>((lPlace - mData) / pBlockSize);

        uint8_t lIndex      = mFirstAvailableBlock;
        ASSERT(lIndex < pBlockCount, kInvalidIndex)

        if (lIndex == lBlockIndex)
        {
            return true;
        }

        // Si le bit � lIndex dans lFoundBlocks est � 1,
        // c'est que cet index a �t� trouv� dans la liste cha�n�e.
        std::bitset<UCHAR_MAX>  lFoundBlocks;
        uint8_t*                lNextBlock  = nullptr;
        for(uint8_t lCounter = 0; ;)
        {
            lNextBlock  = mData + (lIndex * pBlockSize);
            lFoundBlocks.set(lIndex, true);
            ++lCounter;
            if (mAvailableBlockCount <= lCounter)
            {
                // A d�compt� avec succ�s le nombre de noeuds dans la liste.
                break;
            }
            lIndex = *lNextBlock;
            if (lIndex == lBlockIndex)
            {
                return true;
            }
            ASSERT(lIndex < pBlockCount, kInvalidIndex)
            ASSERT(!lFoundBlocks.test(lIndex), kAlreadyCheckedIndex)
        }

        return false;
    }


    /*! @param pPointer     Adresse d'un bloc de m�moire dont on cherche � savoir s'il est g�r� par ce
                chunk
        @param pChunkLength Taille du chunk, en octets
        @return VRAI si le bloc � l'adresse <i>pPointer</i> est � l'int�rieur du chunk
     */
    inline bool Chunk::hasBlock(void* pPointer, std::size_t pChunkLength) const
    {
        uint8_t*    lPointer = static_cast<uint8_t*>(pPointer);
        return (mData <= lPointer) && (lPointer < mData + pChunkLength);
    }


    /*! @param pBlockCount Nombre de blocs dont on cherche � �valuer la disponibilit�
        @return VRAI si le chunk poss�de <i>pBlockCount</i> blocs disponibles
     */
    inline bool Chunk::hasAvailable(uint8_t pBlockCount) const
    {
        return (mAvailableBlockCount == pBlockCount);
    }


    /*! @return VRAI si tous les blocs du chunk sont utilis�s, FAUX s'il en reste au moins un disponible
     */
    inline bool Chunk::isFilled() const
    {
        return (0 == mAvailableBlockCount);
    }


//------------------------------------------------------------------------------
//                 Fixed Allocator : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    FixedAllocator::FixedAllocator()
        : mChunks()
        , mAllocChunk(nullptr)
        , mDeallocChunk(nullptr)
        , mEmptyChunk(nullptr)
        , mBlockSize(0)
        , mBlockCount(0)
    { }


    /*! Destructeur. D�truit tous les chunks.
     */
    FixedAllocator::~FixedAllocator()
    {
        for(auto lChunkI = mChunks.begin(); lChunkI != mChunks.end(); ++lChunkI)
        {
            lChunkI->release();
        }
    }


    /*! Initialise l'instance en calculant le nombre de blocs par chunk.
        @param pBlockSize Taille de bloc allouable dans un chunk
        @param pPageSize  Taille maximale de la m�moire allou�e dans un chunk (en octets)
     */
    void FixedAllocator::initialize(std::size_t pBlockSize, std::size_t pPageSize)
    {
        ASSERT(0 < pBlockSize, kNullBlockSize)
        ASSERT(pBlockSize <= pPageSize, kInvalidPageSize)

        mBlockSize = pBlockSize;

        std::size_t lBlockCount = pPageSize / pBlockSize;
        if      (kMaxObjectsPerChunk < lBlockCount)
        {
            lBlockCount = kMaxObjectsPerChunk;
        }
        else if (lBlockCount < kMinObjectsPerChunk)
        {
            lBlockCount = kMinObjectsPerChunk;
        }

        mBlockCount = static_cast<uint8_t>(lBlockCount);
        ASSERT(mBlockCount == lBlockCount, kOverflowError)
    }


//------------------------------------------------------------------------------
//                 Fixed Allocator : Allocation & D�sallocation
//------------------------------------------------------------------------------

    /*! @return Un pointeur vers bloc de m�moire allou�e de taille fixe, ou nullptr si l'allocation a �chou�
     */
    void* FixedAllocator::allocate()
    {
        // Prouve que mEmptyChunk soit ne pointe nulle part, soit pointe vers un chunk vraiment vide.
        ASSERT((nullptr == mEmptyChunk) || (mEmptyChunk->hasAvailable(mBlockCount)), kEmptyChunkNotEmpty)
        ASSERT(countEmptyChunks() < 2, kMoreThanOneEmptyChunk)

        if ((nullptr == mAllocChunk) || mAllocChunk->isFilled())
        {
            if (nullptr != mEmptyChunk)
            {
                mAllocChunk = mEmptyChunk;
                mEmptyChunk = nullptr;
            }
            else
            {
                for(auto lChunkI = mChunks.begin(); ; ++lChunkI)
                {
                    if (mChunks.end() == lChunkI)
                    {
                        if (!makeNewChunk())
                        {
                            return nullptr;
                        }
                        break;
                    }
                    if (!lChunkI->isFilled())
                    {
                        mAllocChunk = &*lChunkI;
                        break;
                    }
                }
            }
        }
        else if (mAllocChunk == mEmptyChunk)
        {
            // D�tache mEmptyChunk de mAllocChunk, car apr�s avoir appel�
            // mAllocChunk->Allocate(mBlockSize_); le chunk ne sera plus vide.
            mEmptyChunk = nullptr;
        }

        ASSERT(mAllocChunk != nullptr, kInvalidChunk)
        ASSERT(!mAllocChunk->isFilled(), kAlreadyFilledChunk)

        void* lPlace = mAllocChunk->allocate(mBlockSize);

        // Prouve que mEmptyChunk soit ne pointe nulle part, soit pointe vers un chunk vraiment vide.
        ASSERT((nullptr == mEmptyChunk) || (mEmptyChunk->hasAvailable(mBlockCount)), kEmptyChunkNotEmpty)
        ASSERT(countEmptyChunks() < 2, kMoreThanOneEmptyChunk)

        return lPlace;
    }


    /*! D�salloue un bloc de m�moire pr�alablement allou� avec pAllocate. Si le bloc n'appartient pas �
        cette instance, retourne FAUX, pour que l'appelant puisse s'en occuper autrement.
        @param pPointer Pointeur vers le bloc m�moire � d�sallouer
        @param pHint    Chunk � partir duquel effectuer la recherche du "propri�taire" de <i>pPointer</i>
        @return VRAI si <i>pPointer</i> a �t� trouv� et d�sallou�, FAUX sinon
     */
    bool FixedAllocator::deallocate(void* pPointer, Chunk* pHint)
    {
        ASSERT(!mChunks.empty(), kNoChunkHasBeenPushed)

        ASSERT(&mChunks.front() <= mDeallocChunk,   kInvalidChunk)
        ASSERT(mDeallocChunk    <= &mChunks.back(), kInvalidChunk)
        ASSERT(&mChunks.front() <= mAllocChunk,     kInvalidChunk)
        ASSERT(mAllocChunk      <= &mChunks.back(), kInvalidChunk)

        ASSERT(countEmptyChunks() < 2, kMoreThanOneEmptyChunk)

        Chunk*  lFoundChunk = (nullptr == pHint) ? vicinityFind(pPointer) : pHint;
        if (nullptr == lFoundChunk)
        {
            return false;
        }

        ASSERT(lFoundChunk->hasBlock(pPointer, mBlockSize * mBlockCount), kChunkNotFound)

#ifdef CHECK_FOR_CORRUPTION
        if (lFoundChunk->isCorrupt(mBlockSize, mBlockCount, true))
        {
            ASSERT(false, kCorruptData)
            return false;
        }
        if (lFoundChunk->isBlockAvailable(pPointer, mBlockSize, mBlockCount))
        {
            ASSERT(false, kCorruptData)
            return false;
        }
#endif  // CHECK_FOR_CORRUPTION

        mDeallocChunk = lFoundChunk;
        doDeallocate(pPointer);

        ASSERT(countEmptyChunks() < 2, kMoreThanOneEmptyChunk)

        return true;
    }


//------------------------------------------------------------------------------
//                        Fixed Allocator : Informations
//------------------------------------------------------------------------------

    /*! @return La taille de bloc avec laquell l'instance a �t� initialis�e
     */
    inline std::size_t FixedAllocator::blockSize() const
    {
        return mBlockSize;
    }


    /*! @return Le nombre de chunks vides port�s par cet allocateur
     */
    inline std::size_t FixedAllocator::countEmptyChunks() const
    {
        return (nullptr == mEmptyChunk) ? 0 : 1;
    }


//------------------------------------------------------------------------------
//                     Fixed Allocator : Lib�ration M�moire
//------------------------------------------------------------------------------

    /*! Lib�re la m�moire utilis�e par le chunk vide. Cela prendra un temps constant, quelle que soit la
        situation.
        @return VRAI si un chunk vide a �t� trouv� et lib�r�, FAUX sinon
     */
    bool FixedAllocator::trimEmptyChunk()
    {
        // Prouve que mEmptyChunk soit ne pointe nulle part, soit pointe vers un chunk vraiment vide.
        ASSERT((nullptr == mEmptyChunk) || (mEmptyChunk->hasAvailable(mBlockCount)), kEmptyChunkNotEmpty)
        if (nullptr == mEmptyChunk)
        {
            return false;
        }

        // Si mEmptyChunk pointe vers un chunk valide, alors la liste de chunks n'est pas vide.
        ASSERT(!mChunks.empty(), kNoChunkHasBeenPushed)
        // Et il devrait y avoir exactement un chunk vide.
        ASSERT(1 == countEmptyChunks(), kNotOneEmptyChunk)

        Chunk*  lLastChunk  = &mChunks.back();
        if (lLastChunk != mEmptyChunk)
        {
            std::swap(*mEmptyChunk, *lLastChunk);
        }
        ASSERT(lLastChunk->hasAvailable(mBlockCount), kEmptyChunkNotEmpty)
        lLastChunk->release();
        mChunks.pop_back();

        if (mChunks.empty())
        {
            mAllocChunk     = nullptr;
            mDeallocChunk   = nullptr;
        }
        else
        {
            if (mDeallocChunk == mEmptyChunk)
            {
                mDeallocChunk = &mChunks.front();
                ASSERT(mDeallocChunk->mAvailableBlockCount < mBlockCount, kAlreadyFilledChunk)
            }
            if (mAllocChunk == mEmptyChunk)
            {
                mAllocChunk = &mChunks.back();
                ASSERT(mAllocChunk->mAvailableBlockCount < mBlockCount, kAlreadyFilledChunk)
            }
        }

        mEmptyChunk = nullptr;

        return true;
    }


    /*! Lib�re les emplacement de chunks non-utilis�s de la liste. Cela prendra un temps constant quel
        que soit le nombre de chunks, mais le temps r�el d�pend de l'allocateur sous-jacent.
        @return FAUX s'il n'y avait pas d'emplacement inutilis�, VRAI si quelques uns ont �t� trouv�s
     */
    bool FixedAllocator::trimChunkList()
    {
        if (mChunks.empty())
        {
            ASSERT(nullptr == mAllocChunk,   kInvalidChunk)
            ASSERT(nullptr == mDeallocChunk, kInvalidChunk)
        }

        if (mChunks.size() == mChunks.capacity())
        {
            return false;
        }

        Chunks().swap(mChunks);

        return true;
    }


//------------------------------------------------------------------------------
//                            Fixed Allocator : Tests
//------------------------------------------------------------------------------

    /*! D�termine si l'instance est corrompue.<br>
        Verifie les attributs pour voir si l'un d'eux poss�de une valeur erron�e ou viole un invariant.
        V�rifie aussi si un chunk est corrompu.
        @return VRAI si l'instance est corrompue (et d�clenche une assertion), FAUX si tout va bien
     */
    bool FixedAllocator::isCorrupt() const
    {
        const bool          lIsEmpty            = mChunks.empty();
        ChunkCIter          lStartI             = mChunks.begin();
        ChunkCIter          lLastI              = mChunks.end();
        const std::size_t   lEmptyChunkCount    = countEmptyChunks();

        if (lIsEmpty)
        {
            if (lStartI != lLastI)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (0 < lEmptyChunkCount)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (nullptr != mDeallocChunk)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (nullptr != mAllocChunk)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (nullptr != mEmptyChunk)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
        }
        else
        {
            const Chunk*    lFront  = &mChunks.front();
            const Chunk*    lBack   = &mChunks.back();
            if (lLastI <= lStartI)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (lBack < mDeallocChunk)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (lBack < mAllocChunk)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (mDeallocChunk < lFront)
            {
                ASSERT(false, kCorruptData)
                return true;
            }
            if (mAllocChunk < lFront)
            {
                ASSERT(false, kCorruptData)
                return true;
            }

            switch(lEmptyChunkCount)
            {
                case 0:
                    if (mEmptyChunk != nullptr)
                    {
                        ASSERT(false, kCorruptData)
                        return true;
                    }
                    break;
                case 1:
                    if (mEmptyChunk == nullptr)
                    {
                        ASSERT(false, kCorruptData)
                        return true;
                    }
                    if (lBack < mEmptyChunk)
                    {
                        ASSERT(false, kCorruptData)
                        return true;
                    }
                    if (mEmptyChunk < lFront)
                    {
                        ASSERT(false, kCorruptData)
                        return true;
                    }
                    if (!mEmptyChunk->hasAvailable(mBlockCount))
                    {
                        // Cela peut vouloir dire que quelqu'un a essay� d'effacer un bloc deux fois.
                        ASSERT(false, kCorruptData)
                        return true;
                    }
                    break;
                default:
                    ASSERT(false, kCorruptData)
                    return true;
            }
            for(auto lChunkI = lStartI; lChunkI != lLastI; ++lChunkI)
            {
                const Chunk&    lChunk  = *lChunkI;
                if (lChunk.isCorrupt(mBlockSize, mBlockCount, true))
                {
                    return true;
                }
            }
        }

        return false;
    }


    /*! @param pPointer Pointeur dont on recherche le chunk propri�taire
        @return Le chunk auquel "appartient" <i>pPointer</i> s'il a �t� trouv�, FAUX sinon
        @note La complexit� est O(C), o� C est le nombre total de chunk (vides ou utilis�s)
     */
    const Chunk* FixedAllocator::hasBlock(void* pPointer) const
    {
        const std::size_t   lChunkLength = mBlockSize * mBlockCount;

        for(auto lChunkI = mChunks.begin(); lChunkI != mChunks.end(); ++lChunkI)
        {
            const Chunk&    lChunk  = *lChunkI;
            if (lChunk.hasBlock(pPointer, lChunkLength))
            {
                return &lChunk;
            }
        }

        return nullptr;
    }


    /*! @param pPointer Pointeur dont on recherche le chunk propri�taire
        @return Le chunk auquel "appartient" <i>pPointer</i> s'il a �t� trouv�, nullptr sinon
        @note La complexit� est O(C), o� C est le nombre total de chunk (vides ou utilis�s)
     */
    inline Chunk* FixedAllocator::hasBlock(void* pPointer)
    {
        return const_cast<Chunk*>(const_cast<const FixedAllocator*>(this)->hasBlock(pPointer));
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! D�salloue le bloc de m�moire � l'adresse pPointer, et g�re la "comptabilit�" interne n�cessaire
        pour maintenir les invariants de classe.
        @param pPointer Pointeur � d�sallouer
        @warning mDeallocChunk doit pointer vers le "bon" chunk le "propri�taire" de <i>pPointer</i>
     */
    void FixedAllocator::doDeallocate(void* pPointer)
    {
        // V�rifie que mDeallocChunk poss�de vraiment le bloc � l'adresse pPointer.
        ASSERT(mDeallocChunk->hasBlock(pPointer, mBlockSize * mBlockCount), kWrongChunk)
        // L'une ou l'autre des deux assertions suivante pourra �chouer
        // si quelqu'un essaie de supprimer le m�me bloc deux fois.
        ASSERT(mEmptyChunk != mDeallocChunk, kWrongChunk)
        ASSERT(!mDeallocChunk->hasAvailable(mBlockCount), kInvalidChunk)
        // Prouve que mEmptyChunk soit ne pointe nulle part, soit pointe vers un chunk vraiment vide.
        ASSERT((nullptr == mEmptyChunk) || (mEmptyChunk->hasAvailable(mBlockCount)), kEmptyChunkNotEmpty)

        // Appel transmis au chunk, la liste interne sera mise � jour,
        // mais la m�moire ne sera pas lib�r�e.
        mDeallocChunk->deallocate(pPointer, mBlockSize);

        if (mDeallocChunk->hasAvailable(mBlockCount))
        {
            ASSERT(mEmptyChunk != mDeallocChunk, kWrongChunk)
            // mDeallocChunk  est vide, mais un chunk n'est lib�r� que s'il y en a deux vides. Puisque
            // mEmptyChunk ne peut pointer que vers un seul chunk vide, s'il pointe d�j� vers quelque
            // chose, c'est que cette instance poss�de maintenant 2 chunks vides.
            if (nullptr != mEmptyChunk)
            {
                // Si le dernier chunk est vide, change simplement ce vers quoi pointe mDeallocChunk,
                // et lib�re le dernier chunk. Autrement, permute un chunk vide avec le dernier, et le
                // lib�re.
                Chunk*  lLastChunk  = &mChunks.back();
                if      (lLastChunk == mDeallocChunk)
                {
                    mDeallocChunk   = mEmptyChunk;
                }
                else if (lLastChunk != mEmptyChunk)
                {
                    std::swap(*mEmptyChunk, *lLastChunk);
                }
                ASSERT(lLastChunk->hasAvailable(mBlockCount), kInvalidChunk)
                lLastChunk->release();
                mChunks.pop_back();
                if ((mAllocChunk == lLastChunk) || mAllocChunk->isFilled())
                {
                    mAllocChunk = mDeallocChunk;
                }
            }
            mEmptyChunk = mDeallocChunk;
        }

        // Prouve que mEmptyChunk soit ne pointe nulle part, soit pointe vers un chunk vraiment vide.
        ASSERT((nullptr == mEmptyChunk) || (mEmptyChunk->hasAvailable(mBlockCount)), kEmptyChunkNotEmpty)
    }


    /*! Cr�e un chunk vide, et l'ajoute � la fin de la liste de chunks.<br>
        Tous les appels aux fonctions d'allocation m�moire de plus bas niveau s'effectuent dans cette
        m�thode, c'est pourquoi le seul bloc try-catch s'y trouve.
        @return VRAI en cas de succ�s, FAUX en cas d'�chec
     */
    bool FixedAllocator::makeNewChunk()
    {
        bool    lAllocated  = false;

        try
        {
            std::size_t lSize   = mChunks.size();
            // Appeler mChunks.reserve() *avant* de cr�er et d'initialiser le nouveau chunk veut
            // dire qu'il n'y a aucune fuite en cas de lancement de 'exception depuis reserve().
            if (mChunks.capacity() == lSize)
            {
                if (0 == lSize)
                {
                    lSize = 4;
                }
                mChunks.reserve(lSize * 2);
            }
            Chunk   lNewChunk;
            lAllocated = lNewChunk.init(mBlockSize, mBlockCount);
            if (lAllocated)
            {
                mChunks.push_back(lNewChunk);
            }
        }
        catch(...)
        {
            lAllocated = false;
        }
        if (!lAllocated)
        {
            return false;
        }

        mAllocChunk     = &mChunks.back();
        mDeallocChunk   = &mChunks.front();

        return true;
    }


    /*! Cherche le chunk poss�dant le bloc � l'adresse <i>pPointer</i>. D�marre � partir de mDeallocChunk
        et cherche � la fois dans les sens montant et descendant jusqu'� trouver le chunk poss�dant
        <i>pPointer</i>. Cet algorithme devrai trouver le chunk rapidement s'il s'agit de mDeallocChunk
        ou s'il en est proche dans le conteneur de chunks. La recherche s'effectue dans les deux sens car
        une telle recherche est efficace pour les d�sallocations � la fois dans le m�me ordre que leur
        allocation, et dans l'ordre inverse.<br>
        La complexit� est de O(C) o� C est le nombre total de chunks.<br>
        Cette m�thode ne lance jamais d'exception.
        @param pPointer Point dont on recherche le chunk propri�taire
        @return Le chunk contenant le bloc point� par <i>pPointer</i> s'il a �t� trouv�, nullptr sinon
     */
    Chunk* FixedAllocator::vicinityFind(void* pPointer) const
    {
        if (mChunks.empty())
        {
            return nullptr;
        }
        ASSERT(mDeallocChunk != nullptr, kInvalidChunk)

        const std::size_t   lChunkLength    = mBlockSize * mBlockCount;
        Chunk*              lLo             = mDeallocChunk;
        Chunk*              lHi             = mDeallocChunk + 1;
        const Chunk*        lLoBound        = &mChunks.front();
        const Chunk*        lHiBound        = &mChunks.back() + 1;

        // Cas sp�cial : mDeallocChunk est � la fin du tableau.
        if (lHi == lHiBound)
        {
            lHi = nullptr;
        }

        while(true)
        {
            if (lLo != nullptr)
            {
                if (lLo->hasBlock(pPointer, lChunkLength))
                {
                    return lLo;
                }
                if (lLo == lLoBound)
                {
                    lLo = nullptr;
                    if (nullptr == lHi)
                    {
                        break;
                    }
                }
                else
                {
                    --lLo;
                }
            }

            if (lHi != nullptr)
            {
                if (lHi->hasBlock(pPointer, lChunkLength))
                {
                    return lHi;
                }
                if (++lHi == lHiBound)
                {
                    lHi = nullptr;
                    if (nullptr == lLo)
                    {
                        break;
                    }
                }
            }
        }

        return nullptr;
    }


//------------------------------------------------------------------------------
//                             Fonctions Assistantes
//------------------------------------------------------------------------------

    /*! Calcule l'index dans le r�servoir d'allocateurs o� trouver celui g�rant les blocs de
        <i>pByteCount</i> octets.
        @internal
        @param pByteCount Taille de bloc g�r�e par l'allocateur recherch�
        @param pAlignment Alignement utilis� pour calculer les tailles de blocs
        @return L'index de l'allocateur g�rant les blocs de <i>pByteCount</i> octets
     */
    inline std::size_t getOffset(std::size_t pByteCount, std::size_t pAlignment)
    {
        const std::size_t lAlignExtra = pAlignment - 1;

        return (pByteCount + lAlignExtra) / pAlignment;
    }


    /*! Appelle l'allocateur par d�faut lorsque SmallObjAllocator d�cide de ne pas g�rer une requ�te
        (e.g. si le nombre d'octets � allouer est sup�rieur � la taille maximale que peuvent g�rer les
        instances FixedAllocator.
        @internal
        @param pByteCount Nombre d'octets � allouer.
        @return Un pointeur vers un bloc m�moire de <i>pByteCount</i> octets, nullptr en cas d'erreur
        d'allocation
     */
    void* defaultAllocator(std::size_t pByteCount)
    {
        return Memory->allocate(pByteCount);
    }


    /*! Appelle le d�sallocateur par d�faut lorsque SmallObjAllocator d�cide de ne pas g�rer une requ�te
        (e.g si une adresse n'a pas �t� trouv�e parmi ses propres blocs.
        @internal
        @param pPointer Pointeur vers le bloc de m�moire � d�sallouer
     */
    void defaultDeallocator(void* pPointer)
    {
        Memory->deallocate(pPointer);
    }


//------------------------------------------------------------------------------
//              Small Object Allocator : Constructeur & destructeur
//------------------------------------------------------------------------------

    /*! Le seul constructeur disponible n�cessite certains param�tre afin de pouvoir initialiser toutes
        les instances de FixedAllocator.
        @param pPageSize        Nombre maximal d'octets dans une page de m�moire
        @param pMaxObjectSize   Taille maximale d'un objet que cette instance pourra allouer
        @param pObjectAlignSize Nombre d'octets sur lesquels aligner les allocations
     */
    SmallObjAllocator::SmallObjAllocator(std::size_t pPageSize,
                                         std::size_t pMaxObjectSize,
                                         std::size_t pObjectAlignSize)
        : mPool(nullptr)
        , mMaxSmallObjectSize(pMaxObjectSize)
        , mObjectAlignSize(pObjectAlignSize)
    {
        ASSERT(0 != pObjectAlignSize, kInvalidAlignment)

        const std::size_t   lAllocCount = getOffset(pMaxObjectSize, pObjectAlignSize);
        mPool = new FixedAllocator[lAllocCount];

        for(std::size_t lAlloc = 0; lAlloc < lAllocCount; ++lAlloc)
        {
            mPool[lAlloc].initialize((lAlloc + 1) * pObjectAlignSize, pPageSize);
        }
    }


    /*! Le destructeur lib�re tous les blocs, chunks et instances de FixedAllocators.
     */
    SmallObjAllocator::~SmallObjAllocator()
    {
        delete[] mPool;
    }


//------------------------------------------------------------------------------
//              Small Object Allocator : Allocation & D�sallocation
//------------------------------------------------------------------------------

    /*! Alloue un bloc de m�moire de la taille requise. La complexit� est souvent constante, mais peut
        �tre O(C) o� C est le nombre de chunks dans une instance de FixedAllocator.<br>
        En cas d'�chec, trimExcessMemory() avant une nouvelle tentative d'allocation et, le cas �ch�ant,
        un lancement d'exception. Contrairement � beaucoup de <b>new_handler</b>, cette m�thode n'essaie
        qu'une seconde fois.
        @param pByteCount Nombre d'octets requis pour l'allocation
        @return Un pointeur vers le bloc de m�moire allou�e
        @exception std::bad_alloc En cas d'�chec d'allocation
     */
    void* SmallObjAllocator::allocate(std::size_t pByteCount)
    {
        std::lock_guard<std::mutex> lLock(gMutex);

        if (maxObjectSize() < pByteCount)
        {
            return defaultAllocator(pByteCount);
        }

        ASSERT(nullptr != mPool, kInvalidPool)

        if (0 == pByteCount)
        {
            pByteCount = 1;
        }

        const std::size_t   lIndex      = getOffset(pByteCount, alignment()) - 1;
        ASSERT(lIndex < getOffset(maxObjectSize(), alignment()), kInvalidIndex)

        FixedAllocator&     lAllocator  = mPool[lIndex];
        ASSERT(pByteCount <= lAllocator.blockSize(),                            kInvalidBlockSize)
        ASSERT(              lAllocator.blockSize() < pByteCount + alignment(), kInvalidBlockSize)

        void*               lPlace      = lAllocator.allocate();

        if ((nullptr == lPlace) && trimExcessMemory())
        {
            lPlace = lAllocator.allocate();
        }

        if ((nullptr == lPlace))
        {
            throw std::bad_alloc();
        }

        return lPlace;
    }


    /*! D�salloue un bloc de m�moire � une adresse donn�e et d'une taille sp�cifique. La complexit� est
        constante, mais O(C) seulement s'il faut rechercher pour quel chunk d�sallouer.
        @note Ne lance aucune exception
     */
    void SmallObjAllocator::deallocate(void* pPointer, std::size_t pByteCount)
    {
        std::lock_guard<std::mutex> lLock(gMutex);

        if (nullptr == pPointer)
        {
            return;
        }
        if (maxObjectSize() < pByteCount)
        {
            defaultDeallocator(pPointer);
            return;
        }

        ASSERT(nullptr != mPool, kInvalidPool)

        if (0 == pByteCount)
        {
            pByteCount = 1;
        }

        const std::size_t   lIndex      = getOffset(pByteCount, alignment()) - 1;
        ASSERT(lIndex < getOffset(maxObjectSize(), alignment()), kInvalidIndex)

        FixedAllocator&     lAllocator  = mPool[lIndex];
        ASSERT(pByteCount <= lAllocator.blockSize(),                            kInvalidBlockSize)
        ASSERT(              lAllocator.blockSize() < pByteCount + alignment(), kInvalidBlockSize)

        lAllocator.deallocate(pPointer, nullptr);
    }


//------------------------------------------------------------------------------
//                 Small Object Allocator : Recherche de M�moire
//------------------------------------------------------------------------------

    /*! Lib�re les chunks vides de la m�moire. La complexit� est de O(F + C) o� F est le nombre
        d'instances de FixedAllocator dans le r�servoir, et C le nombre de chunks dans toutes ces
        instances.
        @return VRAI si une lib�ration m�moire a eu lieu, FAUX sinon
     */
    bool SmallObjAllocator::trimExcessMemory()
    {
        // NB: le seul appel possible � cette m�thode a d�j� verrouill� le mutex.

        const std::size_t   lAllocCount = getOffset(maxObjectSize(), alignment());
        bool                lFound      = false;

        for(std::size_t lAlloc = 0; lAlloc < lAllocCount; ++lAlloc)
        {
            if (mPool[lAlloc].trimEmptyChunk())
            {
                lFound = true;
            }
        }
        for(std::size_t lAlloc = 0; lAlloc < lAllocCount; ++lAlloc)
        {
            if (mPool[lAlloc].trimChunkList())
            {
                lFound = true;
            }
        }

        return lFound;
    }


//------------------------------------------------------------------------------
//                     Small Object Allocator : Informations
//------------------------------------------------------------------------------

    /*! Test de corruption. La complexit� est O(F + C + B) o� F est le nombre d'instances de
        FixedAllocator dans le r�servoir, C le nombre de chunks dans toutes ces instances, et B le
        nombre de bloc dans tous les chunks. Les tests seront abr�g�s en cas de d�tection pr�coce d'une
        donn�e corrompue.
        @return VRAI si quoi que ce soit dans l'impl�mentation est corrompu
     */
    bool SmallObjAllocator::isCorrupt() const
    {
        std::lock_guard<std::mutex> lLock(gMutex);

        if (nullptr == mPool)
        {
            ASSERT(false, kInvalidPool)
            return true;
        }
        if (0 == alignment())
        {
            ASSERT(false, kCorruptData)
            return true;
        }
        if (0 == maxObjectSize())
        {
            ASSERT(false, kCorruptData)
            return true;
        }

        const std::size_t   lAllocCount = getOffset(maxObjectSize(), alignment());
        for(std::size_t lAlloc = 0; lAlloc < lAllocCount; ++lAlloc)
        {
            if (mPool[lAlloc].isCorrupt())
            {
                return true;
            }
        }

        return false;
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  SmallObjAllocator*  Singleton<SmallObjAllocator>::smThat    = nullptr;
#endif
}
