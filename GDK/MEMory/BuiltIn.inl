/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/MEMory/BuiltIn.inl
    @brief M�thodes inline de la classe MEM::BuiltIn.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MEM
{
//------------------------------------------------------------------------------
//                                 Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par valeur.
        @param pValue Valeur d'initialisation
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>::BuiltIn(TBaseType pValue)
        : mValue(pValue)
    { }


//------------------------------------------------------------------------------
//                                  Op�rateurs
//------------------------------------------------------------------------------

    /*! Conversion implicite vers le type de donn�e port�e.
        @return La valeur embarqu�e
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>::operator TType() const
    {
        return mValue;
    }


    /*! @return L'instance
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> BuiltIn<TBaseType>::operator+() const
    {
        return *this;
    }


    /*! @return L'oppos� de l'instance
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> BuiltIn<TBaseType>::operator-() const
    {
        BuiltIn<TBaseType> lTmp(- mValue);

        return lTmp;
    }


    /*! Op�rateur de pr�-incr�mentation.
     */    
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator++()
    {
        ++mValue;
        return *this;
    }


    /*! Op�rateur de post-incr�mentation.
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> BuiltIn<TBaseType>::operator++(int)
    {
        BuiltIn<TBaseType>   lTmp = *this;
        ++(*this);
        return lTmp;
    }


    /*! Op�rateur de pr�-d�cr�mentation.
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator--()
    {
        --mValue;
        return *this;
    }


    /*! Op�rateur de post-d�cr�mentation.
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> BuiltIn<TBaseType>::operator--(int)
    {
        BuiltIn<TBaseType>   lTmp = *this;
        --(*this);
        return lTmp;
    }


    /*! Additione, puis affecte le r�sultat.
        @param pOperand Second op�rande de l'addition
        @return Le type int�gr�, auquel la valeur d'<i>pOperand</i> a �t� ajout�e
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator+=(BuiltIn<TBaseType> pOperand)
    {
        mValue += pOperand.mValue;

        return *this;
    }


    /*! Soustrait, puis affecte le r�sultat.
        @param pOperand Second op�rande de la soustraction
        @return Le type int�gr�, auquel la valeur d'<i>pOperand</i> a �t� soustraite
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator-=(BuiltIn<TBaseType> pOperand)
    {
        mValue -= pOperand.mValue;

        return *this;
    }


    /*! Multiplie, puis affecte le r�sultat.
        @param pOperand Second op�rande de la multiplication
        @return Le type int�gr�, multipli� par la valeur d'<i>pOperand</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator*=(BuiltIn<TBaseType> pOperand)
    {
        mValue *= pOperand.mValue;

        return *this;
    }


    /*! Divise, puis affecte le r�sultat.
        @param pOperand Second op�rande de la division
        @return Le type int�gr�, divis� par la valeur d'<i>pOperand</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator/=(BuiltIn<TBaseType> pOperand)
    {
        mValue /= pOperand.mValue;

        return *this;
    }


    /*! Combine avec un <b>et</b> logique (bit � bit), puis affecte le r�sultat.
        @param pOperand Second op�rande du <b>et</b> logique
        @return Le type int�gr�, r�sultat du <b>et</b>logique de l'instance avec <i>pOperand</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator&=(BuiltIn<TBaseType> pOperand)
    {
        mValue &= pOperand.mValue;

        return *this;
    }


    /*! Combine avec un <b>ou</b> logique (bit � bit), puis affecte le r�sultat.
        @param pOperand Second op�rande du <b>ou</b> logique
        @return Le type int�gr�, r�sultat du <b>ou</b>logique de l'instance avec <i>pOperand</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType>& BuiltIn<TBaseType>::operator|=(BuiltIn<TBaseType> pOperand)
    {
        mValue |= pOperand.mValue;

        return *this;
    }


//------------------------------------------------------------------------------
//                           Op�rateurs Arithm�tiques
//------------------------------------------------------------------------------

    /*! Addition.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> + <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator+(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL += pR;
        return pL;
    }


    /*! Soustraction.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> - <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator-(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL -= pR;
        return pL;
    }


    /*! Multiplication.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> * <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator*(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL *= pR;
        return pL;
    }


    /*! Division.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> * <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator/(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL /= pR;
        return pL;
    }


    /*! Et binaire.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> & <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator&(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL &= pR;
        return pL;
    }


    /*! Ou binaire.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return <i>pL</i> | <i>pR</i>
     */
    template<typename TBaseType>
    inline BuiltIn<TBaseType> operator|(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        pL |= pR;
        return pL;
    }


//------------------------------------------------------------------------------
//                              Op�rateurs d'Ordre
//------------------------------------------------------------------------------

    /*! Op�rateur d'�galit�.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> portent la m�me valeur, FAUX sinon
     */
    template<typename TBaseType>
    inline bool operator==(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        return TBaseType(pL) == TBaseType(pR);
    }


    /*! Op�rateur d'in�galit�.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> portent des valeurs distinctes, FAUX sinon
     */
    template<typename TBaseType>
    inline bool operator!=(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        return TBaseType(pL) != TBaseType(pR);
    }


    /*! Op�rateur inf�rieur �.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return VRAI si <i>pL</i> est inf�rieur � <i>pR</i> , FAUX sinon
     */
    template<typename TBaseType>
    inline bool operator<=(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        return TBaseType(pL) <= TBaseType(pR);
    }


    /*! Op�rateur strictement inf�rieur �.
        @param pL Premier op�rande
        @param pR Second op�rande
        @return VRAI si <i>pL</i> est strictement inf�rieur � <i>pR</i> , FAUX sinon
     */
    template<typename TBaseType>
    inline bool operator<(BuiltIn<TBaseType> pL, BuiltIn<TBaseType> pR)
    {
        return TBaseType(pL) < TBaseType(pR);
    }


//------------------------------------------------------------------------------
//                               Autres Op�rateurs
//------------------------------------------------------------------------------

    /*! Apr�s la suppression des op�rateur d'adresse de la classe MEM::BuiltIn, il fallait trouver un
        autre moyen de retrouver facilement l'adresse de la valeur encapsul�e (certaines fonctions de
        biblioth�ques externes attendent comme param�tres des pointeurs vers des types de base, pas des
        instanciations de MEM::BuiltIn. J'ai donc introduit cet op�rateur, un peu � la mani�re de
        <b>sizeof</b> (raison pour laquelle sa d�claration ne respecte pas les r�gles de codage). Il
        existe deux versions, constante et non-constante.
        @param pB Instance doit on souhaite obtenir l'adresse (sous forme d'un pointeur constant vers le
                type de donn�e port�e)
        @return Un pointeur constant vers la donn�e port�e
     */
    template<typename TBaseType>
    inline const TBaseType* addressof(const BuiltIn<TBaseType>& pB)
    {
        return reinterpret_cast<const TBaseType*>(&pB);
    }


    /*! @overload
     */
    template<typename TBaseType>
    inline TBaseType* addressof(BuiltIn<TBaseType>& pB)
    {
        return reinterpret_cast<TBaseType*>(&pB);
    }


    /*! Affiche de la valeur d'un nombre sur un log.
        @param pLog Log sur lequel afficher le message de <i>pE</i>
        @param pB   Nombre int�gr� "� afficher"
        @return <i>pLog</i>
     */
    template<typename TBaseType>
    inline LOG::Log& operator<<(LOG::Log& pLog, BuiltIn<TBaseType> pB)
    {
        pLog << TBaseType(pB);

        return pLog;
    }
}
