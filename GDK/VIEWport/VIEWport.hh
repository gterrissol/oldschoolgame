/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef VIEW_VIEWPORT_HH
#define VIEW_VIEWPORT_HH

/*! @file GDK/VIEWport/VIEWport.hh
    @brief Pr�-d�clarations du module @ref VIEWport.
    @author @ref Guillaume_Terrissol
    @date 27 Mars 2008 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace VIEW
{
    class Viewport;
    class Window;
}

#endif  // De VIEW_VIEWPORT_HH
