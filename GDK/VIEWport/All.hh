/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef VIEW_VIEWPORT_HH
#define VIEW_VIEWPORT_HH

/*! @file GDK/VIEWport/All.hh
    @brief Interface publique du module @ref VIEWport.
    @author @ref Guillaume_Terrissol
    @date 4 Mars 2002 - 29 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace VIEW  //! Gestion du viewport.
{
    /*! @namespace VIEW
        @version 0.95
     */

    /*! @defgroup VIEWport VIEWport : Gestion bas niveau des fen�tre et des viewports
        <b>namespace</b> VIEW
     */

}

#include "Window.hh"
#include "BaseViewport.hh"

#endif  // De VIEW_VIEWPORT_HH
