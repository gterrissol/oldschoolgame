/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef VIEW_ERRMSG_HH
#define VIEW_ERRMSG_HH

/*! @file GDK/VIEWport/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module VIEW.
    @author @ref Guillaume_Terrissol
    @date 4 Mars 2002 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace VIEW
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kNoSDLSurface;
    extern  ERR::String kSDLFault;
    extern  ERR::String kVideoModeOpeningFailed;
    extern  ERR::String kVideoRequestFailed;
    extern  ERR::String kViewportMustExist;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De VIEW_ERRMSG_HH
