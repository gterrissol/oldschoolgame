/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "VIEWport.hh"

/*! @file GDK/VIEWport/VIEWport.cc
    @brief D�finitions diverses du module VIEW.
    @author @ref Guillaume_Terrissol
    @date 4 Mars 2002 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace VIEW
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kNoSDLSurface,           "Aucune surface SDL n'existe.",          "No SDL surface exists.")
    REGISTER_ERR_MSG(kSDLFault,               "Erreur SDL.",                           "SDL Fault.")
    REGISTER_ERR_MSG(kVideoModeOpeningFailed, "L'ouverture du mode vid�o a �chou� : ", "Video opening failed : ")
    REGISTER_ERR_MSG(kVideoRequestFailed,     "La requ�te vid�o a �chou� : ",          "Video request failed : ")
    REGISTER_ERR_MSG(kViewportMustExist,      "Le viewport doit exister",              "The viewport must exist.")
}
