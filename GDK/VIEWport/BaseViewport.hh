/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef VIEW_BASEVIEWPORT_HH
#define VIEW_BASEVIEWPORT_HH

#include "VIEWport.hh"

/*! @file GDK/VIEWport/BaseViewport.hh
    @brief En-t�te de la classe VIEW::Viewport.
    @author @ref Guillaume_Terrissol
    @date 4 Mars 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace VIEW
{
    /*! @brief Viewport pour le rendu 3D
        @version 1.5

        Interface pour l'impl�mentation d'un viewport OpenGL.
     */
    class Viewport : public MEM::OnHeap
    {
    public:
        //! @name Destructeur
        //@{
virtual         ~Viewport();                            //!< Destructeur.
        //@}
        //! @name Interface publique
        //@{
        void    initialize();                           //!< Initialisation du viewport.
        void    resize(I32 pWidth, I32 pHeight);        //!< Redimensionnement du viewport.
        F32     ratio() const;                          //!< Ratio d'affichage.
        void    clear();                                //!< "Nettoyage" du viewport.
        void    paint();                                //!< Rafra�chissement
        //@}

    private:
        //! @name Affichage
        //@{
virtual void    doClearing() = 0;                       //!< "Nettoyage" du viewport.
virtual void    preparePainting() = 0;                  //!< Pr�paration du rendu.
virtual void    doPainting() = 0;                       //!< Impl�mentation du rendu.
        //@}
        //! @name Gestion avanc�e
        //@{
virtual void    doInitialize() = 0;                     //!< Initialisation du viewport.
virtual void    doResize(I32 pWidth, I32 pHeight) = 0;  //!< Redimensionnement du viewport.
virtual F32     getRatio() const = 0;                   //!< Ratio d'affichage.
        //@}
    };
}

#endif  // De VIEW_BASEVIEWPORT_HH
