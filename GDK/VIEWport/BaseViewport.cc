/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseViewport.hh"

/*! @file GDK/VIEWport/BaseViewport.cc
    @brief M�thodes (non-inline) de la class VIEW::Viewport.
    @author @ref Guillaume_Terrissol
    @date 4 Mars 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace VIEW
{
//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Viewport::~Viewport() { }


//------------------------------------------------------------------------------
//                              Interface Publique
//------------------------------------------------------------------------------

    /*! Initialise le viewport.
        @sa doInitialize()
     */
    void Viewport::initialize()
    {
        doInitialize();
    }


    /*! Redimensionne le viewport.
        @param pWidth  Nouvelle largeur du viewport
        @param pHeight Nouvelle hauteur du viewport
     */
    void Viewport::resize(I32 pWidth, I32 pHeight)
    {
        doResize(pWidth, pHeight);
    }


    /*! @return Le ratio d'affichage
        @sa getRatio()
     */
    F32 Viewport::ratio() const
    {
        return getRatio();
    }


    /*! Nettoie l'�cran.<br>
        L'appel � cette m�thode avant paint() est inutile.
     */
    void Viewport::clear()
    {
        doClearing();
    }


    /*! Rafra�chit le viewport (affiche la sc�ne, si celle-ci a �t� d�finie).
     */
    void Viewport::paint()
    {
        // 1�re �tape : efface de l'�cran.
        doClearing();
        // 2�me �tape : pr�pare le rendu.
        preparePainting();
        // 3�me �tape : rend les donn�es pr�par�es.
        doPainting();
    }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn void VIEW::Viewport::doClearing()
        Effectue un nettoyage de la vue (effacement des pixels pr�c�dents).
        @sa preparePainting(), doPainting()
     */


    /*! @fn void VIEW::Viewport::preparePainting()
        Effectue toutes les op�rations en vue d'un nouveau rendu.
        @sa doClearing(), doPainting()
     */


    /*! @fn void VIEW::Viewport::doPainting()
        Effectue le rendu des donn�es pr�par�es par preparePainting(). Cette d�composition permet
        d'effectuer plusieurs rendu � partir de donn�es pr�par�es de la m�me mani�re.
        @sa doClearing(), preparePainting()
     */


    /*! @fn void VIEW::Viewport::doInitialize()
        Etablit les param�tres initiaux du viewport.
     */


    /*! @fn void VIEW::Viewport::doResize(I32 pWidth, I32 pHeight)
        Redimensionne le viewport.
        @param pWidth  Nouvelle largeur du viewport
        @param pHeight Nouvelle hauteur du viewport
     */


    /*! @fn void VIEW::Viewport::getRatio() const
        @return Le ratio d'affichage
     */
}
