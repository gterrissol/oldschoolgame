/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Window.hh"

/*! @file GDK/VIEWport/Window.cc
    @brief M�thodes (non-inline) de la classe VIEW::Window.
    @author @ref Guillaume_Terrissol
    @date 26 Septembre 2005 - 8 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>
#include <cstring>
#include <SDL.h>

#include "ERRor/Log.hh"
#include "STL/SharedPtr.hh"

#include "BaseViewport.hh"
#include "ErrMsg.hh"


namespace VIEW
{
//------------------------------------------------------------------------------
//                              Enum�ration Priv�e
//------------------------------------------------------------------------------

    enum
    {
        eDefaultWidth   = 640,
        eDefaultHeight  = 480
    };


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de VIEW::Window.
        @version 1.0
     */
    class Window::Private : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef Window::ViewportPtr   ViewportPtr;                      //!< Pointeur sur fen�tre.
        //@}
        //! @name M�thodes
        //@{
                Private(ViewportPtr pView, I32 pWidth, I32 pHeight);    //!< Constructeur.
        //@}
        ViewportPtr                             mViewport;              //!< Viewport de rendu.
        //! @name Informations pour la libSDL
        //@{
        SDL_Window*                             mSdlWindow;
        SDL_GLContext                           mSdlContext;
        //@}
        //! @name Information sur la fen�tre
        //@{
        I32                                     mWidth;                 //!< Largeur.
        I32                                     mHeight;                //!< Hauteur.
        bool                                    mIsFullScreen;          //!< Plein �cran ?
        //@}
    };


//------------------------------------------------------------------------------
//                               P-Impl : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Window::Private::Private(ViewportPtr pView, I32 pWidth, I32 pHeight)
        : mViewport(pView)
        , mSdlWindow{}
        , mSdlContext{}
        , mWidth(pWidth)
        , mHeight(pHeight)
        , mIsFullScreen(false)
    {
//        ASSERT(!mViewport.expired(), kViewportMustExist)
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Window::Window(ViewportPtr pView)
        : pthis(pView, I32(eDefaultWidth), I32(eDefaultHeight))
    { }


    /*! Destructeur.
     */
    Window::~Window()
    {
        deactivate();
    }


//------------------------------------------------------------------------------
//                                  Dimensions
//------------------------------------------------------------------------------

    /*! Redimensionne la fen�tre.
        @param pWidth  Nouvelle largeur de la fen�tre
        @param pHeight Nouvelle hauteur de la fen�tre
        @note En cas d'�chec, les anciennes dimensions sont conserv�es
     */
    void Window::resize(I32 pWidth, I32 pHeight)
    {
#ifndef HAVE_WINDOWS_H
        if (pthis->mSdlWindow != nullptr)
        {
            // Essaie de changer la taille (les autres arguments restent les m�mes).
            SDL_SetWindowSize(pthis->mSdlWindow, pWidth, pHeight);
#endif  // HAVE_WINDOWS_H
            pthis->mWidth  = pWidth;
            pthis->mHeight = pHeight;

            if (auto lViewport = pthis->mViewport.lock())
            {
                lViewport->resize(pthis->mWidth, pthis->mHeight);
            }
#ifndef HAVE_WINDOWS_H
        }
#endif  // HAVE_WINDOWS_H
    }


    /*! @return Le ratio de la fen�tre
     */
    F32 Window::ratio() const
    {
        return F32(width()) / F32(height());
    }


    /*! @return La largeur de la fen�tre
     */
    I32 Window::width() const
    {
        return pthis->mWidth;
    }


    /*! @return la hauteur de la fen�tre
     */
    I32 Window::height() const
    {
        return pthis->mHeight;
    }


//------------------------------------------------------------------------------
//                                Gestion avanc�e
//------------------------------------------------------------------------------

    /*!
     */
    void Window::bind(ViewportPtr pView)
    {
        pthis->mViewport = pView;
        if (auto lViewport = pthis->mViewport.lock())
        {
            lViewport->initialize();
            resize(width(), height());
        }
    }


    /*! Rafra�chit le contenu de la fen�tre en commandant un affichage du viewport.
     */
    void Window::update()
    {
        if (pthis->mSdlWindow != nullptr)
        {
            ASSERT(!pthis->mViewport.expired(), kViewportMustExist)

            pthis->mViewport.lock()->paint();

            // Permutte les buffers de rendu.
            SDL_GL_SwapWindow(pthis->mSdlWindow);
        }
    }


    /*! Suite au passage � Qt4, et aux tests sous windows, de nouveaux probl�mes sont apparus (plus
        d'�v�nements captur�s par Qt sur la vue 3D). En cons�quence, le mode vid�o de la SDL ne sera plus
        activ� qu'au lancement du moteur, par cette m�thode, puis stopp� gr�ce � l'autre m�thode ajout�e
        (forc�ment), deactivate().
     */
    void Window::activate(void* pWinId)
    {
        if (pthis->mSdlWindow == nullptr)
        {
            // Ouvre la vid�o.
            SDL_InitSubSystem(SDL_INIT_VIDEO);

            // Fixe le mode vid�o.
            try
            {
                if (pWinId == nullptr)
                {
                    // Requ�tes sur l'utilisation des bits des pixels.
                    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     8);
                    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   8);
                    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    8);
                    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   8);
                    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,  24);
                    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
                    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

                    if ((pthis->mSdlWindow = SDL_CreateWindow(
                            "OSG",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            width(),
                            height(),
                            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE)) == nullptr)
                    {
                        LAUNCH_EXCEPTION(kSDLFault)
                    }

                    if ((pthis->mSdlContext = SDL_GL_CreateContext(pthis->mSdlWindow)) == nullptr)
                    {
                        LAUNCH_EXCEPTION(kSDLFault)
                    }

                    resize(width(), height());
                }
                else
                {
                    if ((pthis->mSdlWindow = SDL_CreateWindowFrom(pWinId)) == nullptr)
                    {
                        LAUNCH_EXCEPTION(kSDLFault)
                    }
                }
            }
            catch(ERR::Exception& pE)
            {
                EXECUTE_ERROR_MANAGEMENT_CODE(LOG::Cerr << kVideoModeOpeningFailed << SDL_GetError() << LOG::Endl;)

                throw;
            }
        }
    }


    /*!
     */
    void Window::deactivate()
    {
        if (pthis->mSdlWindow != 0)
        {
            if (pthis->mSdlContext)
            {
                SDL_GL_DeleteContext(pthis->mSdlContext);
                pthis->mSdlContext = {};
            }
            SDL_DestroyWindow(pthis->mSdlWindow);
            pthis->mSdlWindow = nullptr;
            // Ferme le sous-syst�me vid�o.
            SDL_QuitSubSystem(SDL_INIT_VIDEO);
        }
    }


    /*! Fait passer la fen�tre du mode plein �cran au mode normal
        @param pOn Vrai pour le passage en plein �cran, Faux pour le mode fen�tr�
     */
    void Window::toggleFullScreen(bool pOn)
    {
        if ((pthis->mIsFullScreen != pOn) && (pthis->mSdlWindow != nullptr))
        {
            Uint32  lFlags = pOn ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0;

            if (SDL_SetWindowFullscreen(pthis->mSdlWindow, lFlags) == 0)
            {
                pthis->mIsFullScreen = pOn;
            }
            else
            {
                EXECUTE_ERROR_MANAGEMENT_CODE(LOG::Cerr << "Toggle fullscreen failed" << SDL_GetError() << LOG::Endl;)
            }
        }
    }
}
