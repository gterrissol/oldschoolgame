/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef VIEW_WINDOW_HH
#define VIEW_WINDOW_HH

#include "VIEWport.hh"

/*! @file GDK/VIEWport/Window.hh
    @brief En-t�te de la classe VIEW::Window.
    @author @ref Guillaume_Terrissol
    @date 26 Septembre 2005 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/SharedPtr.hh"

namespace VIEW
{
    /*! @brief Fen�tre.
        @version 0.95

        Pas mal d'�volutions au niveau de VIEW, GXV & QT3D. Cette classe n'est plus destin�e � �tre une
        base de QT3D::GLWidget, qui manipulera son viewport comme elle l'entendra.
        Cette classe utilise directement la SDL pour la gestion de fen�tre.
     */
    class Window : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        using Ptr           = SharedPtr<Window>;    //!< Pointeur sur fen�tre.
        using ViewportPtr   = WeakPtr<Viewport>;    //!< Pointeur sur viewport.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Window(ViewportPtr pView);          //!< Constructeur.
virtual         ~Window();                          //!< Destructeur.
        //@}
        //! @name Dimensions
        //@{
        void    resize(I32 pWidth, I32 pHeight);    //!< Redimensionnement de la fen�tre.
        F32     ratio() const;                      //!< Ratio de la fen�tre.
        I32     width() const;                      //!< Largeur de la fen�tre.
        I32     height() const;                     //!< Hauteur de la fen�tre.
        //@}
         //! @name Gestion avanc�e
        //@{
        void    bind(ViewportPtr pView);            //!< Changement de viewport.
        void    update();                           //!< Mise � jour du contenu.
        void    activate(void* pWinId = nullptr);   //!< Activation.
        void    deactivate();                       //!< D�sactivation.
        void    toggleFullScreen(bool pOn);         //!< Mode plein �cran activ�/d�sactiv�.
        //@}

    private:

        FORBID_COPY(Window)
        PIMPL()
    };
}

#endif  // De VIEW_WINDOW_HH
