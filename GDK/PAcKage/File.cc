/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "File.hh"

/*! @file GDK/PAcKage/File.cc
    @brief M�thodes (non-inline) de la classe PAK::File.
    @author @ref Guillaume_Terrissol
    @date 19 Janvier 2003 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

namespace PAK
{
//------------------------------------------------------------------------------
//                            Fichier : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    File::File()
        : mDataHdl(eBadHdl)
        , mSize(0)
    { }


    /*! Constructeur "utile".
     */
    File::File(U32 pSize, BlockHdl pHdl)
        : mDataHdl(pHdl)
        , mSize(pSize)
    { }


//------------------------------------------------------------------------------
//                          Fichier : Acc�s aux Donn�es
//------------------------------------------------------------------------------

    /*! @return Le handle permettant l'acc�s aux donn�es
        @sa SetDataHdl
     */
    BlockHdl File::dataHdl() const
    {
        return mDataHdl;
    }


    /*! @return La taille du fichier
        @sa Resize
     */
    U32 File::size() const
    {
        return mSize;
    }


//------------------------------------------------------------------------------
//                              Fichier : Validit�
//------------------------------------------------------------------------------

    /*! @return VRAI si le fihcier est valide, FAUX sinon
     */
    bool File::isValid() const
    {
        return (mDataHdl != eBadHdl) && (mSize != 0);
    }


    /*! Invalide le fichier
        @sa DataHdl()
     */
    void File::invalidate()
    {
        mDataHdl = BlockHdl(eBadHdl);
        mSize    = k0UL;
    }
}
