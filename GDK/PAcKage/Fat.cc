/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Fat.hh"

/*! @file GDK/PAcKage/Fat.cc
    @brief M�thodes (non-inline) de la classe PAK::Fat.
    @author @ref Guillaume_Terrissol
    @date 24 Juin 2002 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "File.hh"

namespace PAK
{
    /*! @brief P-Impl de PAK::Fat.
        @version 0.5

        Le seul but de cette classe �tait d'�viter l'inclusion de "STL/Vector.hh" directement dans
        l'en-t�te de PAK::Fat.
     */
    struct Fat::Private : public MEM::OnHeap
    {
        Vector<File>    mFiles; //!< Fichiers.
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Fat::Fat() { }


    /*! Destructeur.
     */
    Fat::~Fat() { }


    /*! Charge les descripteurs de fichiers.
        @param pFiles Tableau de fichiers � charger
        @note Les donn�es de <i>pFiles</i> seront modifi�es par l'appel de cette m�thode
     */
    void Fat::loadFiles(Vector<File>& pFiles)
    {
        // Supprime les anciennes donn�es (au besoin).
        pthis->mFiles.clear();
        // Charge les "nouveaux" fichiers.
        pthis->mFiles.swap(pFiles);
    }


//------------------------------------------------------------------------------
//                       EngineFat : Gestion des Fichiers
//------------------------------------------------------------------------------

    /*! Edite les donn�es de FAT d'un fichier.
        @param pHdl Handle du fichier dont modifier les donn�es de FAT
        @param pFile Donn�es de FAT du fichier identifi� par <i>pHdl</i>
        @note Les modifications possibles sont :
        - R�initialisation d'un fichier valide
        - D�finition d'un fichier pr�alablement invalide
     */
    void Fat::setFile(FileHdl pHdl, File pFile)
    {
        ASSERT_EX(pHdl.value() < pthis->mFiles.size(), kInvalidFileHandle, return;)

        File&   lFile = pthis->mFiles[pHdl.value()];

        if (lFile.dataHdl() == eBadHdl)
        {
            // Fichier pr�c�demment invalide : il doit �tre d�fini maintenant.
            ASSERT_EX(pFile.dataHdl() != eBadHdl, kInvalidFile, return;)

            lFile = pFile;
        }
        else
        {
            // Fichier pr�c�demment valide : il va �tre invalid�.
            ASSERT_EX(pFile.dataHdl() != eBadHdl, kInvalidFile, return;)
            ASSERT_EX(pFile.size() != 0UL, kInvalidSize, return;)

            lFile = pFile;
        }
    }


    /*! Acc�s au descripteur d'un fichier.
     */
    File Fat::file(FileHdl pHdl) const
    {
        ASSERT_EX(pHdl.value() < pthis->mFiles.size(), kInvalidFileHandle, return File();)

        return pthis->mFiles[pHdl.value()];
    }


    /*! @param pHdl Handle di fichier dont on veut conna�tre la taille
        @return La taille du fichier r�f�renc� par <i>proFileHdl</i>
     */
    U32 Fat::size(FileHdl pHdl) const
    {
        ASSERT(pHdl.value() < pthis->mFiles.size(), kInvalidFileHandle)

        return pthis->mFiles[pHdl.value()].size();
    }


    /*! @param pHdl Handle du fichier dont on veut acc�der aux donn�es
        @return Une r�f�rence vers les donn�es du fichier r�f�renc� par <i>proFileHdl</i>
     */
    BlockHdl Fat::dataHdl(FileHdl pHdl) const
    {
        ASSERT(pHdl.value() < pthis->mFiles.size(), kInvalidFileHandle)

        return pthis->mFiles[pHdl.value()].dataHdl();
    }
}
