/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_PACKAGE_HH
#define PAK_PACKAGE_HH

/*! @file GDK/PAcKage/PAcKage.hh
    @brief Pr�-d�clarations du moduke PAK.
    @author @ref Guillaume_Terrissol
    @date 4 Janvier 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PAK
{
    class Loader;
    class Saver;
    class PakFile;
    class File;
    class PakFileMgr;

    template<class TType, int TI>   class Hdl;
}

#endif  // De PAK_PACKAGE_HH
