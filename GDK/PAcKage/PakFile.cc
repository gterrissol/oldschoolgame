/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "PakFile.hh"

/*! @file GDK/PAcKage/PakFile.cc
    @brief M�thodes (non-inline) de la classe PAK::PakFile.
    @author @ref Guillaume_Terrissol
    @date 8 Juin 2002 - 30 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdio>

#include "STL/String.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "File.hh"

namespace
{
    /*! Modes d'ouverture d'un fichier.
        @internal
     */
    const char* kOpenModes[PAK::eOpenModeCount] =
    {
        "rb",   //!< ... en lecture.
        "rb+"   //!< ... en �criture.
    };
}

namespace PAK
{
//------------------------------------------------------------------------------
//                              P-Impl de Pak File
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PAK::PakFile.
        @version 0.5
        @internal
     */
    class PakFile::Private : public MEM::OnHeap
    {
    public:
        //! \name Constructeurs
        //@{
                        Private(const String& pFileName,
                                EOpenMode pOpenMode);       //!< Constructeur (moteur & �diteur).
                        Private(const String& pFileName,
                                I32 pMaxFileCount);         //!< Constructeur (cr�ation).
                        ~Private();
        //@}
        //! @name Ouverture / fermeture
        //@{
                void    open(const String& pFileName,
                             const String& pOpenMode);      //!< Ouverture du fichier.
                void    close();                            //!< Fermeture du fichier.
        //@}
        //! @name Op�rateurs d'acc�s
        //@{
        inline  void    read(void* pPtr, U32 pSize);        //!< C++ read()-like.
        inline  void    write(const void* pPtr, U32 pSize); //!< C++ write()-like.
        inline  void    seek(I32 pOff);                     //!< C fseek()-like.
        inline  void    seek(I32 pOff, I32 pWhence);        //!< C fseek()-like.
        //! @name Informations
        //@{
        inline  bool    isOpen() const;                     //!< Teste si le fichier est ouvert.
        inline  I32     tell() const;                       //!< C ftell()-like.
                U32     size() const;                       //!< R�cup�ration de la taille du fichier.
        //@}
        //! @name Lecture/�criture de l'en-t�te
        //@{
                void    writeHeader();                      //!< Ecriture de l'en-t�te.
                void    readHeader();                       //!< Lecture de l'en-t�te.
        //@}
        //! @name Attributs
        //@{
        FILE*       mPakFile;                               //!< Fichier syst�me.
        BlockHdl    mFatStart;                              //!< D�but des donn�es de la FAT.
        BlockHdl    mDataStart;                             //!< D�but des donn�es du <b>pak file</b>.
        I32         mMaxFileCount;                          //!< Nombre maximal de fichiers.
        const bool  mEditorMode;                            //!< Pak file ouvert en mode moteur ou �diteur ?
 static const I32   msHeaderSize;                           //!< Taille de l'en-t�te du <b>pak file</b>.
        //@}
    };


//------------------------------------------------------------------------------
//                     P-Impl : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pFileName Nom du fichier � ouvrir
        @param pOpenMode Mode d'ouverture du fichier
     */
    PakFile::Private::Private(const String& pFileName, EOpenMode pOpenMode)
        : mPakFile(nullptr)
        , mFatStart()
        , mDataStart()
        , mMaxFileCount(0)
        , mEditorMode(pOpenMode == eEditor)
    {
        open(pFileName, kOpenModes[pOpenMode]);
    }


    /*! Cr�ation d'un pak file.
        @param pFileName     Nom du fichier <b>pak</b> file
        @param pMaxFileCount Nombre maximum de fichiers dans le <b>pak</b> file � cr�er
     */
    PakFile::Private::Private(const String& pFileName, I32 pMaxFileCount)
        : mPakFile(nullptr)
        , mFatStart()
        , mDataStart()
        , mMaxFileCount(pMaxFileCount)
        , mEditorMode(true)
    {
        open(pFileName, kOpenModes[eEditor]);
    }


    /*! Destructeur.
     */
    PakFile::Private::~Private()
    {
        close();
    }


//------------------------------------------------------------------------------
//                        P-Impl : Ouverture / Fermeture
//------------------------------------------------------------------------------

    /*! Ouverture d'un fichier.
        @param pFileName Nom du fichier � ouvrir
        @param pOpenMode   Mode d'ouverture du fichier
        @note Un fichier d�j� ouvert sera ferm�, puis r�ouvert dans le nouveau mode
        @sa PAK::e_OpenMode
     */
    void PakFile::Private::open(const String& pFileName, const String& pOpenMode)
    {
        // Fermeture, au besoin.
        close();

        if (pOpenMode.find('+') != String::npos)   // + => out.
        {
            // Il faut s'assurer que le fichier existe : ouverture temporaire en �criture, en mode texte...
            mPakFile = fopen(pFileName.c_str(), "a+");

            // Puis fermeture.
            close();
        }

        // Ouverture du fichier.
        mPakFile = fopen(pFileName.c_str(), pOpenMode.c_str());

        // Un petit test pour la route...
        if (!isOpen())
        {
            LAUNCH_EXCEPTION(kOpenFileFailed)
        }
    }


    /*! Fermeture du fichier.
     */
    void PakFile::Private::close()
    {
        if (isOpen())
        {
            fclose(mPakFile);
        }
    }


//------------------------------------------------------------------------------
//                          P-Impl : Op�rateurs d'Acc�s
//------------------------------------------------------------------------------

    /*! Lit des donn�es � partir du fichier en utilisant la syntaxe de la fonction C fread().
        @param pPtr     Pointer vers lequel les donn�es seront charg�es depuis le fichier
        @param pSize  Nombre d'octets � lire dans le fichier
        @sa bFailed pour d�tecter une quelconque erreur
     */
    void PakFile::Private::read(void* pPtr, U32 pSize)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        fread(pPtr, 1, pSize, mPakFile);
#pragma GCC diagnostic pop
    }


    /*! Ecrit des donn�es dans le fichier en utilisant la syntaxe de la fonction C fwrite().
        @param pPtr     Pointer � partir duquel les donn�es seront enregistr�es dans le fichier
        @param pSize  Nombre d'octets � � �crire dans le fichier
        @sa bFailed pour d�tecter une quelconque erreur
     */
    void PakFile::Private::write(const void* pPtr, U32 pSize)
    {
        fwrite(pPtr, 1, pSize, mPakFile);
    }


    /*! D�place le curseur de lecture.
        @param pOff D�placement dans le fichier (� partir du d�but)
     */
    inline void PakFile::Private::seek(I32 pOff)
    {
        seek(pOff, I32(SEEK_SET));
    }


    /*! D�place le curseur d'�criture.
        @param pOff    D�placement relatif dans le fichier (par rapport � <i>pWhence</i>)
        @param pWhence Point de d�part du d�placement (SEEK_SET, SEEK_CUR ou SEEK_END)
     */
    inline void PakFile::Private::seek(I32 pOff, I32 pWhence)
    {
        fseek(mPakFile, pOff, pWhence);
    }


//------------------------------------------------------------------------------
//                             P-Impl : Informations
//------------------------------------------------------------------------------

    /*! @return VRAI si le fichier est ouvert, FAUX sinon
     */
    inline bool PakFile::Private::isOpen() const
    {
        return (mPakFile != nullptr);
    }


    /*! @return La position du pointeur de lecture/�criture � partir du d�but du fichier
     */
    inline I32 PakFile::Private::tell() const
    {
        return I32(ftell(mPakFile));
    }


    /*! @return La taille du fichier, en octets
     */
    U32 PakFile::Private::size() const
    {
        I32 lCurrent    = tell();
        const_cast<Private*>(this)->seek(k0L, I32(SEEK_END));
        I32 lSize       = tell();
        if (lCurrent != lSize)
        {
            const_cast<Private*>(this)->seek(lCurrent);
        }

        return U32(lSize);
    }


//------------------------------------------------------------------------------
//                    P-Impl : Lecture/Ecriture de l'En-T�te
//------------------------------------------------------------------------------

    /*! Ecrit les donn�es de l'en-t�te du pak file.
     */
    void PakFile::Private::writeHeader()
    {
        if (mEditorMode)
        {
            I32   lPos = tell();
            seek(k0L);

            // Nombres de fichiers.
            write(&mMaxFileCount, U32(sizeof(mMaxFileCount)));

            I32   lHeaderSize = tell();
            // Je ne vois pas comment �a pourrait arriver, mais bon...
            if (Private::msHeaderSize < lHeaderSize)
            {
                LAUNCH_EXCEPTION(kPakFileHeaderSizeOverflow)
            }

            // Repositionne le curseur.
            seek(lPos);
        }
    }


    /*! Lit les donn�es de l'en-t�te du pak file.
     */
    void PakFile::Private::readHeader()
    {
        I32   lPos = tell();
        seek(k0L);

        // Nombres de fichiers et de r�pertoires.
        read(&mMaxFileCount, U32(sizeof(mMaxFileCount)));

        // Repositionne le curseur.
        seek(lPos);

        // Les offsets des donn�es sont retrouv�s � partir des informations pr�sentes dans l'en-t�te.
        mFatStart  = BlockHdl(Private::msHeaderSize);
        mDataStart = BlockHdl(Private::msHeaderSize + I32(mMaxFileCount * sizeof(File)));
    }


//------------------------------------------------------------------------------
//                     PakFile : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Ouvre un pak file existant (mode moteur ou �diteur).
        @param pFileName Nom du fichier � ouvrir
        @param pOpenMode Mode d'ouverture du fichier; l'appel de ce constructeur devrait se faire avec eIn | eOut (mode �diteur)
     */
    PakFile::PakFile(const String& pFileName, EOpenMode pOpenMode)
        : pthis(pFileName, pOpenMode)
    {
        if (pthis->isOpen())
        {
            pthis->readHeader();
        }
        else
        {
            LAUNCH_EXCEPTION(kOpenFileFailed)
        }
    }


    /*! Cr�e un nouveau pak file et l'ouvre.
        @param pFileName     Nom du fichier � ouvrir
        @param pMaxFileCount Nombre maximal de fichiers pour le nouveau pak file.
     */
    PakFile::PakFile(const String& pFileName, I32 pMaxFileCount)
        : pthis(pFileName, pMaxFileCount)
    {
        // Quelques contr�les...
        if (eMaxFileCount < pthis->mMaxFileCount)
        {
            LAUNCH_EXCEPTION(kTooManyFiles)
        }

        // Le fichier vient d'�tre cr��. Un en-t�te va y �tre �crit : il contient les informations de base du pak file.
        pthis->mFatStart  = BlockHdl(Private::msHeaderSize);
        pthis->mDataStart = BlockHdl(Private::msHeaderSize + I32(pthis->mMaxFileCount * sizeof(File)));

        // L'en-t�te est ajout� au pak file.
        pthis->writeHeader();
    }


    /*! Destructeur.
     */
    PakFile::~PakFile()
    {
        if (pthis->mEditorMode)
        {
            pthis->writeHeader();
        }
    }


//------------------------------------------------------------------------------
//                         PakFile : Op�rateurs d'Acc�s
//------------------------------------------------------------------------------

    /*! @copydoc PakFile::Private::read(void* pPtr, U32 pSize)
     */
    void PakFile::read(void* pPtr, U32 pSize)
    {
        pthis->read(pPtr, pSize);
    }


    /*! @copydoc PakFile::Private::write(const void* pPtr, U32 pSize)
     */
    void PakFile::write(const void* pPtr, U32 pSize)
    {
        pthis->write(pPtr, pSize);
    }


    /*! @copydoc PakFile::Private::seek(I32 pOff)
     */
    void PakFile::seek(I32 pOff)
    {
        pthis->seek(pOff);
    }


//------------------------------------------------------------------------------
//                          PakFile : M�thodes d'acc�s
//------------------------------------------------------------------------------

    /*! @return La handle du d�but des donn�es de la FAT
     */
    BlockHdl PakFile::fatStart() const
    {
        return pthis->mFatStart;
    }


    /*! @return Le handle du premier bloc de donn�es des fichiers du pak file
     */
    BlockHdl PakFile::dataStart() const
    {
        return pthis->mDataStart;
    }


    /*! @return Le nombre maximal de fichiers du pak file
        @note Toujours inf�rieur � eMaxFileCount
     */
    I32 PakFile::maxFileCount() const
    {
        return pthis->mMaxFileCount;
    }


//------------------------------------------------------------------------------
//                         PakFile : Attribut de Classe
//------------------------------------------------------------------------------

    const I32   PakFile::Private::msHeaderSize  = I32(sizeof(U32));
}
