/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_LOADERSAVER_HH
#define PAK_LOADERSAVER_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/LoaderSaver.hh
    @brief En-t�te des classes PAK::Loader & PAK::Saver.
    @author @ref Guillaume_Terrissol
    @date 30 Juillet 2002 - 7 Juillet 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/STL.hh"

#include "Handle.hh"

namespace PAK
{
    //! @name Op�rateurs de lecture/�criture
    //@{
    template<class TType>
    inline  void    operator<<(Saver& pSaver, const TType& pDataToStore);   //!< Ecriture d'une donn�e.
    template<class TType>
    inline  void    operator>>(Loader& pLoader, TType& pDataToLoad);        //!< Lecture d'une donn�e.
    //@}


    /*! @brief Objet pour la sauvegarde de donn�es.
        @version 0.99

        Cette classe fait partie, avec PAK::Loader & PAK::PakFileMgr, de l'interface "publique" du
        module PAK.<br>
        Toutes les sauvegardes de donn�es s'effectuent � travers ses instances.
        @note <b>Important</b> : Un PAK::Saver ne peut �tre construit que sur un fichier d�j� cr��
     */
    class Saver : public MEM::Auto<Saver>
    {
        //! @name Classe et fonction amies
        //@{
        template<class TType>
 friend void    operator<<(Saver&, const TType&);               //!< Pour l'acc�s � Store().
        //@}
    public:
        //! @name Constructeur & destructeur
        //@{
                Saver(FileHdl pHdl);                            //!< Constructeur.
                ~Saver();                                       //!< Destructeur.
        //@}
        //! @name Connaissance du fichier
        //@{
        FileHdl fileHdl() const;                                //!< Handle du fichier ouvert.
        //@}
        //! @name Taille des donn�es
        //@{
        U32     size() const;                                   //!< Taille des donn�es.
        //@}

    private:
        // Constructeurs priv�s.
        FORBID_COPY(Saver)
        //! @name Sauvegarde d'une donn�e
        //@{
        void    store(const void* pData, std::size_t pSize);    //!< Ecriture d'une donn�e.
        //@}
        PIMPL()
    };


    /*! @brief Objet pour le chargement de donn�es.
        @version 0.99

        Cette classe fait partie, avec PAK::Saver & PAK::PakFileMgr, de l'interface "publique" du
        module PAK.<br>
        Tous les chargements de donn�es s'effectuent � travers ses instances.
        @note <b>Important</b> : Un PAK::Loader ne peut �tre construit que sur un fichier d�j� cr��
     */
    class Loader : public MEM::Auto<Loader>
    {
        //! @name Classe et fonction amies
        //@{
        template<class TType>
 friend void    operator>>(Loader&, TType&);                        //!< Pour permettre [l�galement] la sp�cialisation.
        //@}
    public:
        //! @name Constructeurs & destructeur
        //@{
                Loader(FileHdl pHdl);                               //!< Constructeur.
                Loader(const U8* pData, U32 pCount, FileHdl pHdl);  //!< Constructeur (sur donn�es existantes).
                ~Loader();                                          //!< Destructeur.
        //@}
        //! @name Connaissance du fichier
        //@{
        FileHdl fileHdl() const;                                    //!< Handle du fichier ouvert.
        //@}
        //! @name Taille des donn�es
        //@{
        U32     size() const;                                       //!< Taille des donn�es.
        U32     remaining() const;                                  //!< Nombre d'octet non-lus.
        //@}
        //! @name Autre op�ration
        //@{
        void    rollback(U32 pByteCount);                           //!< Annule une lecture.
        //@}

    private:
        // Constructeurs priv�s.
        FORBID_COPY(Loader)
        //! @name Chargement d'une donn�e
        //@{
        void    load(void* pData, std::size_t pSize);   //!< Lecture d'une donn�e.
        //@}
        PIMPL()
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "LoaderSaver.inl"

#endif  // De PAK_LOADERSAVER_HH
