/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_FILE_HH
#define PAK_FILE_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/File.hh
    @brief En-t�te des classes PAK::cl_EngineFile, PAK::cl_EditorFile & PAK::cl_DiskFile.
    @author @ref Guillaume_Terrissol
    @date 5 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"

#include "Handle.hh"

namespace PAK
{
    /*! @brief Fichier moteur.
        @version 1.5

        Les classes fichiers du module PAK (PAK::cl_EngineFile, PAK::EditorFile) ne sont en fait que des
        descripteurs, sans aucune connaissance des m�thodes de stockage des donn�es.
     */
    class File : public MEM::Auto<File>
    {
    public:
        //! @name Constructeurs
        //@{
                    File();                         //!< ... par d�faut.
                    File(U32 pSize, BlockHdl pHdl); //!< ... "utile".
        //@}
        //! @name Acc�s aux donn�es
        //@{
        BlockHdl    dataHdl() const;                //!< Handle des donn�es du fichier.
        U32         size() const;                   //!< Taille du fichier.
        //@}    
        //! @name Validit�  
        //@{    
        bool        isValid() const;                //!< Valide ?
        void        invalidate();                   //!< Invalidation.
        //@}    
    
    private:    
    
        BlockHdl    mDataHdl;                       //!< R�f�rence sur le premier octet de donn�es.
        U32         mSize;                          //!< Taille du fichier (des donn�es).
    };
}

#endif  // De PAK_FILE_HH
