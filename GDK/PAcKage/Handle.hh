/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_HANDLE_HH
#define PAK_HANDLE_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/Handle.hh
    @brief Handles du module PAK.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

#include "Enum.hh"

namespace PAK
{
    /*! @defgroup PAK_Handles Handles
        @ingroup PAcKage
        @{
     */

    /*! @brief Handle g�n�rique.
        @version 1.5

        Un handle permet de r�f�rencer de mani�re unique un objet, sans se pr�occuper de sa place en
        m�moire.<br>
        Ici, la g�n�ricit� permet de distinguer diff�rents types de handles pour r�soudre, notamment, la
        surcharge de m�thodes.
     */
    template<class TT, int TI>
    class Hdl : public MEM::Auto<Hdl<TT, TI>>
    {
    public:
        //! @name Constructeurs
        //@{
        inline  explicit    Hdl();              //!< ... par d�faut.
        inline  explicit    Hdl(TT pHdl);       //!< ... � partir d'un entier.
        inline              Hdl(EHdls pHdl);    //!< ... � partir d'une �num�ration.
        //@}
        //! @name Autres m�thodes
        //@{
        inline  TT          value() const;      //!< Valeur du handle.
        inline  bool        isValid() const;    //!< Validit� du handle.
        //@}

    private:

        TT  mHandle;                            //!< Handle.
    };


    //! @name Op�rateurs
    //@{
    template<typename TT, int TI>
    inline  bool    operator==(Hdl<TT, TI> pL, Hdl<TT, TI> pR); //!< ... d'�galit�.
    template<typename TT, int TI>
    inline  bool    operator==(EHdls pL, Hdl<TT, TI> pR);       //!< ... d'�galit�.
    template<typename TT, int TI>
    inline  bool    operator==(Hdl<TT, TI> pL, EHdls pR);       //!< ... d'�galit�.
    template<typename TT, int TI>
    inline  bool    operator!=(Hdl<TT, TI> pL, Hdl<TT, TI> pR); //!< ... d'in�galit�.
    template<typename TT, int TI>
    inline  bool    operator!=(EHdls pL, Hdl<TT, TI> pR);       //!< ... d'in�galit�.
    template<typename TT, int TI>
    inline  bool    operator!=(Hdl<TT, TI> pL, EHdls pR);       //!< ... d'in�galit�.
    template<typename TT, int TI>
    inline  bool    operator<(Hdl<TT, TI> pL, Hdl<TT, TI> pR);  //!< ... strictement inf�rieur �.
    //@}

    /*! @internal
     */
    enum
    {
        eFileHdl = 0,
        eBlockHdl
    };

    typedef Hdl<U16, eFileHdl>  FileHdl;    //!< Handle de fichier.
    typedef Hdl<I32, eBlockHdl> BlockHdl;   //!< Handle de block.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Handle.inl"

#endif  // De PAK_HANDLE_HH
