/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_ALL_HH
#define PAK_ALL_HH

/*! @file GDK/PAcKage/All.hh
    @brief Interface publique du module @ref PAcKage
    @author @ref Guillaume_Terrissol
    @date 11 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PAK   //! Gestion des fichiers.
{
    /*! @namespace PAK
        @version 0.94

        L'interface de ce module est (vraiment) minimale et compl�te (du moins, je l'esp�re). Les classes
        utiles sont : PAK::PakFileMgr (et ses d�riv�es, mais j'y reviendrai), PAK::Loader, PAK::Saver.
        <br>
        Consultez la page @ref PAK_Package_Page pour plus d'informations
        @todo Gestion Big/Little Endian
     */

    /*! @defgroup PAcKage PAcKAge : Gestion des fichiers.
        <b>namespace</b> PAK.
     */
}

#include "Handle.hh"
#include "LoaderSaver.hh"
#include "PakFileMgr.hh"

#endif  // De PAK_ALL_HH
