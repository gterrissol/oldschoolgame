/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_FAT_HH
#define PAK_FAT_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/Fat.hh
    @brief En-t�te de la classe PAK::Fat.
    @author @ref Guillaume_Terrissol
    @date 11 Juin 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/Vector.hh"

#include "Handle.hh"

namespace PAK
{
    /*! @brief FAT moteur.
        @version 1.5

        En mode moteur, la fat doit �tre la plus l�g�re possible : toutes les donn�es des fichiers ne
        sont pas utiles. Mais, comme les m�thodes de cette classe sont aussi utilis�es en mode �diteur
        (avec la m�me s�mantique), il fallait �viter de les red�finir. J'ai donc opt� pour une classe
        template (<i>TFile</i> = type de fichier � utiliser).
     */
    class Fat : public MEM::OnHeap
    {
    public:
        //! @name Constructeur(s) et destructeur
        //@{
                    Fat();                              //!< Constructeur.
virtual             ~Fat();                             //!< Destructeur.
        //@}
        //! @name Permanence des donn�es de FAT
        //@{
        void        loadFiles(Vector<File>& pFiles);    //!< Chargement (initialisation).
        //@}
        //! @name Gestion des fichiers
        //@{
        void        setFile(FileHdl pHdl, File pFile);  //!< D�finition des donn�es de FAT d'un fichier.
        File        file(FileHdl pHdl) const;           //!< Acc�s au descripteur d'un fichier.
        U32         size(FileHdl pHdl) const;           //!< Taille d'un fichier.
        BlockHdl    dataHdl(FileHdl pHdl) const;        //!< Donn�es d'un fichier.
        //@}

    private:

        FORBID_COPY(Fat)
        PIMPL()
    };
}

#endif  // De PAK_FAT_HH
