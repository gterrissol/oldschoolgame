/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "PakFileMgr.hh"

/*! @file GDK/PAcKage/PakFileMgr.cc
    @brief M�thodes (non-inline) des classes PAK::PakFileMgr, PAK::EnginePakFileMgr.
    @author @ref Guillaume_Terrissol
    @date 1 Juillet 2002 - 27 Avril 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cctype>
#include <functional>
#include <ios>
#include <memory>

#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "Fat.hh"
#include "File.hh"
#include "PakFile.hh"

namespace PAK
{
//------------------------------------------------------------------------------
//                             P-Impl de PakFileMgr
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PAK::PakFileMgr.
        @version 0.5
     */
    struct PakFileMgr::Private : public MEM::OnHeap
    {
                Private();              //!< Constructeur par d�faut.
        void    createFat();            //!< Cr�ation de la FAT.
        void    loadFat();              //!< Chargement de la FAT.

        UniquePtr<PakFile>  mPakFile;   //!< Fichier de donn�es.
        UniquePtr<Fat>      mFat;       //!< Fat.
        ManagerPtr          mManager;   //!< Gestionnaire de fichiers de <b>pak</b> file.
        mutable THR::Mutex  mMutex;     //!< Mutex.
    };


//------------------------------------------------------------------------------
//                    PakFileMgr Manager : Interface Publique
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    PakFileMgr::Manager::~Manager() { }


    /*! @copydoc PAK::PakFileMgr::Manager::manage(FileHdl pHdl, const U8* pData, U32 pCount)
        @note D�clar�e <b>inline</b> pour emp�cher son utilisation en dehors de ce fichier
     */
    inline void PakFileMgr::Manager::operator()(FileHdl pHdl, const U8* pData, U32 pCount)
    {
        manage(pHdl, pData, pCount);
    }


    /*! Donn�es initiales des fichiers.
        @param pFiles Donn�es (Tailles et handles de bloc) des fichiers au chargement d'un <b>pak</b>
        file
        @note D�clar�e <b>inline</b> pour emp�cher son utilisation en dehors de ce fichier
     */
    inline void PakFileMgr::Manager::initialize(const Vector<File>& pFiles)
    {
        setFileData(pFiles);
    }


//------------------------------------------------------------------------------
//                      PakFileMgr Manager : Impl�mentation
//------------------------------------------------------------------------------

    /*! M�thode � appeler afin d'effectuer toute op�ration pr�alable � l'�criture des donn�es d'un
        fichier.
        @param pHdl   Handle du fichier qui va �tre [r�]�crit
        @param pData  Donn�es du fichier
        @param pCount Taille de <i>pData</i>
        @sa operator()(FileHdl pHdl, const U8* pData, U32 pCount)
     */
    void PakFileMgr::Manager::manage(FileHdl, const U8*, U32)
    {
        // Par d�faut, ne fait rien.
    }


    /*! Donn�es initiales des fichiers.
        @param pFiles Donn�es (Tailles et handles de bloc) des fichiers au chargement d'un <b>pak</b>
        file
     */
    void PakFileMgr::Manager::setFileData(const Vector<File>&)
    {
        // Par d�faut, ne fait rien.
    }


//------------------------------------------------------------------------------
//           PakFileMgr Manager : Acc�s aux Informations des Fichiers
//------------------------------------------------------------------------------

    /*! @copydoc PAK::PakFile::dataStart() const
     */
    BlockHdl PakFileMgr::Manager::dataStart() const
    {
        return PakFileMgr::get()->pthis->mPakFile->dataStart();
    }


    /*! @copydoc PAK::PakFileMgr::defineFile(FileHdl pHdl, File pFile)
     */
    void PakFileMgr::Manager::defineFile(FileHdl pHdl, File pFile)
    {
        PakFileMgr::get()->defineFile(pHdl, pFile);
    }


    /*! @copydoc PAK::PakFileMgr::blockHdl(FileHdl pHdl) const
     */
    BlockHdl PakFileMgr::Manager::blockHdl(FileHdl pHdl) const
    {
        return PakFileMgr::get()->blockHdl(pHdl);
    }


    /*! @copydoc PAK::PakFileMgr::fileSize(FileHdl pHdl) const
     */
    U32 PakFileMgr::Manager::fileSize(FileHdl pHdl) const
    {
        return PakFileMgr::get()->fileSize(pHdl);
    }


//------------------------------------------------------------------------------
//                               P-Impl : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    PakFileMgr::Private::Private()
        : mPakFile()
        , mFat()
        , mManager(STL::makeShared<Manager>())
        , mMutex()
    { }


    /*! A la cr�ation d'un <b>pak</b> file, cr�e les donn�es initiales de la FAT.
     */
    void PakFileMgr::Private::createFat()
    {
        THR::Lock   lLock(mMutex);

        ASSERT_EX(mPakFile != nullptr, kUndefinedPakFile, return;)

        // Chargement des fichiers.
        Vector<File>    lFiles;
        lFiles.reserve(mPakFile->maxFileCount());

        BlockHdl        lFatDataStart = mPakFile->fatStart();
        File            lCurrentFile;   // Petite optimisation : variable d�clar�e en dehors de boucle.

        mPakFile->seek(lFatDataStart.value());

        for(I32 lF = k0L; lF < mPakFile->maxFileCount(); ++lF)
        {
            if (lF != eRoot)
            {
                // Cr�e un fichier invalide.
                mPakFile->write(&lCurrentFile, U32(sizeof(File)));
                lFiles.push_back(lCurrentFile);
            }
            else
            {
                // Cr�e le MBR.
                File    lMBR(U32(eRoot), mPakFile->dataStart());
                mPakFile->write(&lMBR, U32(sizeof(File)));
                lFiles.push_back(lMBR);
            }
        }

        mManager->initialize(lFiles);
        mFat->loadFiles(lFiles);
    }


    /*! Chargement de la FAT.
     */
    void PakFileMgr::Private::loadFat()
    {
        THR::Lock   lLock(mMutex);

        ASSERT_EX(mPakFile != nullptr, kUndefinedPakFile, return;)

        // Chargement des fichiers.
        Vector<File>    lFiles;
        lFiles.reserve(mPakFile->maxFileCount());

        BlockHdl    lFatDataStart = mPakFile->fatStart();
        File        lCurrentFile;   // Petite optimisation : variable d�clar�e en dehors de boucle.

        mPakFile->seek(lFatDataStart.value());

        for(I32 lF = k0L; lF < mPakFile->maxFileCount(); ++lF)
        {
            // R�cup�re le fichier courant.
            mPakFile->read(&lCurrentFile, U32(sizeof(File)));
            lFiles.push_back(lCurrentFile);
        }

        mManager->initialize(lFiles);
        mFat->loadFiles(lFiles);
    }


//------------------------------------------------------------------------------
//                    PakFileMgr : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    PakFileMgr::PakFileMgr()
        : pthis()
    { }


    /*! Destructeur.
     */
    PakFileMgr::~PakFileMgr()
    {
        closeFile();
    }


//------------------------------------------------------------------------------
//                PakFileMgr :
//------------------------------------------------------------------------------

    /*! Cr�ation, puis ouverture, d'un <b>pak</b> file.
        @param pName         Nom (et chemin) du fichier
        @param pMaxFileCount Nombre maximum de fichiers dans le <b>pak</b> file
     */
    void PakFileMgr::createFile(const String& pName, I32 pMaxFileCount)
    {
        try
        {
            THR::Lock   lLock(pthis->mMutex);

            pthis->mPakFile.reset(new PakFile(pName, pMaxFileCount));
            pthis->mFat.reset(new Fat());
        }
        catch(...)
        {
            closeFile();

            throw;
        }

        pthis->createFat();
    }


    /*! Ouverture d'un pak file.
        @param pName Nom du <b>pak</b> file � ouvrir
     */
    void PakFileMgr::openFile(const String& pName)
    {
        try
        {
            THR::Lock   lLock(pthis->mMutex);

            pthis->mPakFile.reset(new PakFile(pName, eEngine));
            pthis->mFat.reset(new Fat());
        }
        catch(...)
        {
            closeFile();

            throw;
        }

        pthis->loadFat();
    }


    /*! Ouverture d'un pak file pour �dition.
        @param pName Nom du <b>pak</b> file � ouvrir
     */
    void PakFileMgr::editFile(const String& pName)
    {
        try
        {
            THR::Lock   lLock(pthis->mMutex);

            pthis->mPakFile.reset(new PakFile(pName, eEditor));
            pthis->mFat.reset(new Fat());
        }
        catch(...)
        {
            closeFile();

            throw;
        }

        pthis->loadFat();
    }


    /*! Fermeture d'un pak file.
     */
    void PakFileMgr::closeFile()
    {
        THR::Lock   lLock(pthis->mMutex);

        // Lib�ration compl�te des ressources.
        pthis->mFat.reset();
        pthis->mPakFile.reset();
    }


//------------------------------------------------------------------------------
//                           PakFileMgr : "Contr�leur"
//------------------------------------------------------------------------------

    /*! D�finit le "gestionnaire" de fichier � utiliser.
        @param pManager Nouveau contr�leur
     */
    void PakFileMgr::setManager(ManagerPtr pManager)
    {
        pthis->mManager = pManager;
    }


//------------------------------------------------------------------------------
//                  PakFileMgr : Acc�s aux Donn�es des Fichiers
//------------------------------------------------------------------------------

    /*! R�cup�re les donn�es d'un fichier.
        @param pHdl   Handle du fichier dont r�cup�rer les donn�es
        @param pData  Ce tableau va recevoir les donn�es
        @param pCount Taille du tableau <i>pData</i>
     */
    void PakFileMgr::getData(FileHdl pHdl, U8* pData, U32 pCount)
    {
        THR::Lock   lLock(pthis->mMutex);

        // Position du curseur (lecture).
        pthis->mPakFile->seek(blockHdl(pHdl).value());

        // Lecture des donn�es.
        pthis->mPakFile->read(pData, pCount);
    }


    /*! Ecrit des donn�es dans un fichier.
        @param pHdl   Handle du fichier dans lequel �crire les donn�es
        @param pData  Ce tableau contient les donn�es
        @param pCount Taille du tableau <i>pData</i>
     */
    void PakFileMgr::putData(FileHdl pHdl, const U8* pData, U32 pCount)
    {
        THR::Lock   lLock(pthis->mMutex);

        (*pthis->mManager)(pHdl, pData, pCount);

        // Position du curseur (�criture).
        pthis->mPakFile->seek(blockHdl(pHdl).value());

        // Ecriture des donn�es.
        pthis->mPakFile->write(pData, pCount);
    }


    /*! Fournit au <b>pak</b> file les informations conernant un fichier cr�� ou modifi�.
        @param pHdl  Handle du fichier
        @param pFile Informations associ�es au fichier
     */
    void PakFileMgr::defineFile(FileHdl pHdl, File pFile)
    {
        THR::Lock   lLock(pthis->mMutex);

        ASSERT(pthis->mFat != nullptr, kUndefinedFAT)

        File    lPreviousFile   = pthis->mFat->file(pHdl);
        pthis->mFat->setFile(pHdl, pFile);

        try
        {
            // Ecrit la r�f�rence du fichier dans la fat du pak file.
            BlockHdl    lFatDataStart = pthis->mPakFile->fatStart();
            pthis->mPakFile->seek(I32(lFatDataStart.value() + pHdl.value() * sizeof(File)));
            pthis->mPakFile->write(&pFile, U32(sizeof(File)));
        }
        catch(std::ios_base::failure& pFailure)
        {
            // En cas de probl�me d'�criture, restaure la FAT dans son �tat pr�c�dent.
            pthis->mFat->setFile(pHdl, lPreviousFile);
            throw;
        }
    }


    /*! Les donn�es d'un fichier sont "identif�es" par un handle, retrouv� gr�ce � cette m�thode.
        @param pHdl Handle du fichier dont on veut acc�der aux donn�es
        @return Un handle vers les donn�es du fichier identifi� par <i>pHdl</i>
     */
    BlockHdl PakFileMgr::blockHdl(FileHdl pHdl) const
    {
        THR::Lock   lLock(pthis->mMutex);

        ASSERT(pthis->mFat != nullptr, kUndefinedFAT)

        return pthis->mFat->dataHdl(pHdl);
    }


    /*! @return La taille du fichier identifi� par <i>pHdl</i>
     */
    U32 PakFileMgr::fileSize(FileHdl pHdl) const
    {
        THR::Lock   lLock(pthis->mMutex);

        ASSERT_EX(pthis->mFat != nullptr, kUndefinedFAT, return k0UL;)

        return pthis->mFat->size(pHdl);
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  PakFileMgr* MEM::Singleton<PakFileMgr>::smThat  = nullptr;
#endif
}
