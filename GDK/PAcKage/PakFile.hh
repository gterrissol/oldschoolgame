/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_PAKFILE_HH
#define PAK_PAKFILE_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/PakFile.hh
    @brief Classe fichier <b>pak</b>.
    @author @ref Guillaume_Terrissol
    @date 8 Juin 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/String.hh"

#include "Handle.hh"

namespace PAK
{
    //! Ouverture d'un fichier
    enum EOpenMode
    {
        eEngine,
        eEditor,
        eOpenModeCount
    };


    /*! @brief Fichier empaquet�.
        @version 0.9
     */
    class PakFile : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                    PakFile(const String& pFileName, EOpenMode pMode);      //!< Constructeur (�diteur).
                    PakFile(const String& pFileName, I32 pMaxFileCount);    //!< Constructeur (cr�ation).
                    ~PakFile();                                             //!< Destructeur.
        //@}
        //! @name M�thodes d'acc�s
        //@{
        BlockHdl    fatStart() const;                                       //!< D�but des donn�es de la FAT.
        BlockHdl    dataStart() const;                                      //!< D�but des donn�es du <b>pak file</b>.
        I32         maxFileCount() const;                                   //!< Nombre maximal de fichiers.
        //@}
        //! @name Op�rateurs d'acc�s
        //@{
        void        read(void* pPtr, U32 pSize);                            //!< C++ read()-like.
        void        write(const void* pPtr, U32 pSize);                     //!< C++ write()-like.
        void        seek(I32 pOff);                                         //!< C seek()-like.
        //@}

    private:

        FORBID_COPY(PakFile)
        PIMPL()
    };
}

#endif  // De PAK_PAKFILE_HH
