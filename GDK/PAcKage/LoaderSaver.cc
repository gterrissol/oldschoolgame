/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "LoaderSaver.hh"

/*! @file GDK/PAcKage/LoaderSaver.cc
    @brief M�thodes (non-inline) des classes PAK::Loader & PAK::Saver.
    @author @ref Guillaume_Terrissol
    @date 30 Juillet 2002 - 14 Juillet 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/String.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "PakFileMgr.hh"

namespace PAK
{
//------------------------------------------------------------------------------
//                                P-Impl de Saver
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PAK::Saver.
        @version 0.5
        @internal
     */
    class Saver::Private : public MEM::OnHeap
    {
    public:

        Private(FileHdl pHdl);      //!< Constructeur.
        //! @name Attributs
        //@{
        std::vector<U8> mData;      //!< Donn�es � �crire.
        FileHdl         mFileHdl;   //!< Handle du fichier dans lequel �crire les donn�es.
        //@}
    };


    /*! @copydoc Saver::Saver(FileHdl pHdl)
     */
    Saver::Private::Private(FileHdl pHdl)
        : mData()
        , mFileHdl(pHdl)
    {
        // Afin de minimiser un tant soit peu les multiples r�allocations des donn�es du vecteur mData,
        // quelques octets sont r�serv�s d'office (64 octets devraient permettre de r�pondre au mieux �
        // la plupart des cas; � r�estimer, au besoin).
        mData.reserve(64);
    }


//------------------------------------------------------------------------------
//                      Saver : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pHdl Handle du fichier dans lequel �crire des donn�es.
        @note <b>Important</b> : le fichier sera �cras� (si les donn�es sont mises � jour, elles devront
        toutes �tre r��crites)
     */
    Saver::Saver(FileHdl pHdl)
        : pthis(pHdl)
    { }


    /*! Destructeur : les donn�es sont �crites � ce moment.
     */
    Saver::~Saver()
    {
        try
        {
            // Envoi des donn�es au gestionnaire de pak file.
            if (pthis->mFileHdl != eBadHdl)
            {
                PakFileMgr::get()->putData(pthis->mFileHdl, &pthis->mData[0], size());
            }
        }
        catch(...)
        {
            // Si tout a �t� bien cod�, la FAT n'aura pas �t� modifi�e, ni les donn�es �crites sur disque.
            WARNING(kFileWriteFailed)
        }
    }


//------------------------------------------------------------------------------
//                        Saver : Connaissance du Fichier
//------------------------------------------------------------------------------

    /*! @return Le handle du fichier ouvert en �criture
     */
    FileHdl Saver::fileHdl() const
    {
        return pthis->mFileHdl;
    }


//------------------------------------------------------------------------------
//                          Saver : Taille des Donn�es
//------------------------------------------------------------------------------

    /*! Cette m�thode est le pendant de PAK::Loader::ul_Size. Son utilit� est moindre, mais elle permet
        d'offrir � PAK::Loader & PAK::Saver une interface similaire.
        @return La taille des donn�es
        @note Contrairement � PAK::Loader::ul_Size, la valeur retourn�e varie au fur et � mesure qu'on
        empile des donn�es
     */
    U32 Saver::size() const
    {
        return U32(pthis->mData.size());
    }


//------------------------------------------------------------------------------
//                        Saver : Sauvegarde d'une Donn�e
//------------------------------------------------------------------------------

    /*! Ecriture d'une donn�e.
        @param pData Pointeur sur la donn�e � �crire
        @param pSize Taille (en octets) de la donn�e � �crire
     */
    void Saver::store(const void* pData, std::size_t pSize)
    {
        const U8*   lData   = static_cast<const U8*>(pData);

        for(U32 lD = k0UL; lD < pSize; ++lD)
        {
            pthis->mData.push_back(lData[lD]);
        }
    }


//------------------------------------------------------------------------------
//                               P-Impl de Loader
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PAK::Loader.
        @version 0.5
        @internal
     */
    class Loader::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
        Private(FileHdl pHdl);                              //!< Constructeur (sur fichier).
        Private(const U8* pData, U32 pCount, FileHdl pHdl); //!< Constructeur (sur donn�es m�moire).
        //@}
        //! @name Attributs
        //@{
        Vector<U8>      mData;                              //!< Donn�es lues.
        std::vector<U8> mEditorData;                        //!< Donn�es lues (d�j� charg�es en m�moire, en mode �diteur).
        U8*             mLoaderData;                        //!< Alias vers les donn�es.
        U32             mSize;                              //!< Alias vers la taille des donn�es.
        FileHdl         mFileHdl;                           //!< Handle du fichier � partir duquel lire les donn�es.
        U32             mCurrentByte;                       //!< Indice du prochain octet � lire dans le tableau.
        //@}
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur. Les donn�es y sont lues.
        @param pHdl Handle du fichier depuis lequel lire les donn�es.
        @note <b>Important</b> : le fichier doit exister, sinon une exception sera lanc�e
     */
    Loader::Private::Private(FileHdl pHdl)
        : mData()
        , mEditorData()
        , mLoaderData(nullptr)
        , mSize(0)
        , mFileHdl(pHdl)
        , mCurrentByte(0)
    {
        mSize = PakFileMgr::get()->fileSize(mFileHdl);
        if (mSize <= 16777216)  // Ce nombre "magique" doit dispara�tre (un jour).
        {
            // Allocation m�moire.
            mData.resize(mSize, k0UB);
            // R�ception des donn�es depuis le gestionnaire de pak file.
            PakFileMgr::get()->getData(mFileHdl, &mData[0], mSize);
            mLoaderData = &mData[0];
        }
        else
        {
            mEditorData.resize(mSize, k0UB);
            PakFileMgr::get()->getData(mFileHdl, &mEditorData[0], mSize);
            mLoaderData = &mEditorData[0];
        }
    }


    /*! Constructeur sur donn�es existantes.
        @param pData  Donn�es m�moire � partir desquelles construire l'instance
        @param pCount Taille du tableau <i>pData</i>
        @param pHdl   Handle du fichier dont les donn�es ont� �t� charg�es dans @p pData
        @note Le principal int�r�t de ce constructeur est de permettre � Loader de formater des donn�es
        depuis une zone m�moire sans d�clarer une nouvelle classe ni les accesseurs qui iraient bien
     */
    Loader::Private::Private(const U8* pData, U32 pCount, FileHdl pHdl)
        : mData()
        , mEditorData(pData, pData + pCount)
        , mLoaderData(&mEditorData[0])
        , mSize(pCount)
        , mFileHdl(pHdl)
        , mCurrentByte(0)
    { }


//------------------------------------------------------------------------------
//                     Loader : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @copydoc Loader::Private::Private(FileHdl pHdl)
     */
    Loader::Loader(FileHdl pHdl)
        : pthis(pHdl)
    { }


    /*! Constructeur sur donn�es existantes.
        @param pData  Donn�es m�moire � partir desquelles construire l'instance
        @param pCount Taille du tableau <i>pData</i>
        @param pHdl   Handle du fichier dont les donn�es ont� �t� charg�es dans @p pData
        @note Le principal int�r�t de ce constructeur est de permettre � Loader de formater des donn�es
        depuis une zone m�moire sans d�clarer une nouvelle classe ni les accesseurs qui iraient bien
     */
    Loader::Loader(const U8* pData, U32 pCount, FileHdl pHdl)
        : pthis(pData, pCount, pHdl)
    { }


    /*! Destructeur.
     */
    Loader::~Loader() { }


//------------------------------------------------------------------------------
//                       Loader : Connaissance du Fichier
//------------------------------------------------------------------------------

    /*! @return Le handle du fichier ouvert en lecture
     */
    FileHdl Loader::fileHdl() const
    {
        return pthis->mFileHdl;
    }


//------------------------------------------------------------------------------
//                       Loader : Chargement d'une Donn�e
//------------------------------------------------------------------------------

    /*! Lecture d'une donn�e.
        @param pData Pointeur sur la donn�e � lire
        @param pSize Taille (en octets) de la donn�e � lire
        @todo Effectuer un contr�le sur la quantit� de donn�es lues
     */
    void Loader::load(void* pData, std::size_t pSize)
    {
        U8*     lData       = static_cast<U8*>(pData);
        U32&    lCurrent    = pthis->mCurrentByte;

        for(U32 lD = k0UL; (lD < pSize) && (lCurrent < pthis->mSize); ++lD, ++lCurrent)
        {
            lData[lD] = pthis->mLoaderData[lCurrent];
        }
    }


//------------------------------------------------------------------------------
//                          Loader : Taille des Donn�es
//------------------------------------------------------------------------------

    /*! Parfois, les donn�es d'un loader elles-m�mes ne contiennent leur taille totale (ex: fichier texte). Cette m�thode permet donc de
        retrouver la taille du fichier de donn�es auquel ce loader est li�
        @return La taille des donn�es
     */
    U32 Loader::size() const
    {
        return pthis->mSize;
    }


    /*! @return Le nombre d'octets de ce loader qui n'ont pas encore �t� lus
     */
    U32 Loader::remaining() const
    {
        return pthis->mSize - pthis->mCurrentByte;
    }


//------------------------------------------------------------------------------
//                           Loader : Autre Op�ration
//------------------------------------------------------------------------------

    /*! Permet de "rembobiner" le loader. Cel� permet, entre autres, de lire une donn�e, puis de la
        rendre � nouvea disponible � la lecture pour un autre objet.
        @param pByteCount Nombre d'octets � "rembobiner"
     */
    void Loader::rollback(U32 pByteCount)
    {
        if (pByteCount <= pthis->mCurrentByte)
        {
            pthis->mCurrentByte -= pByteCount;
        }
        else
        {
            pthis->mCurrentByte = k0UL;
        }
    }


//------------------------------------------------------------------------------
//                Op�rateurs de Lecture/Ecriture : Sp�cialisation
//------------------------------------------------------------------------------

    /*! Sauvegarde d'une cha�ne C++.
        @param pSaver         Saver dans lequel �crire la cha�ne <i>pStringToStore</i>
        @param pStringToStore Cha�ne � sauvegarder
     */
    template<>
    void operator<<(Saver& pSaver, const String& pStringToStore)
    {
        U32 lLength = U32(pStringToStore.length());

        pSaver << lLength;
        pSaver.store(&pStringToStore[0], lLength);
    }


    /*! Chargement d'une cha�ne C++.
        @param pLoader       Loader � partir duquel lire la cha�ne � assigner � <i>pStringToLoad</i>
        @param pStringToLoad La cha�ne lue sera assign�e � cette variable
     */
    template<>
    void operator>>(Loader& pLoader, String& pStringToLoad)
    {
        U32 lLength = k0UL;
        pLoader >> lLength;

        pStringToLoad.resize(lLength);
        pLoader.load(&pStringToLoad[0], lLength);
    }
}
