/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/PAcKage/Handle.inl
    @brief M�thodes inline des classes PAK::Hdl.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PAK
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : handle invalide par d�faut.
     */
    template<class TType, int TI>
    inline Hdl<TType, TI>::Hdl()
        : mHandle(eBadHdl)
    { }


    /*! Constructeur � partir d'un entier (valeur de handle).
        @param pHdl Valeur du handle � construire
     */
    template<class TType, int TI>
    inline Hdl<TType, TI>::Hdl(TType pHdl)
        : mHandle(pHdl)
    { }


    /*! Constructeur � partir d'une �num�ration (de handle).
        @param pHdl Valeur du handle � construire
     */
    template<class TType, int TI>
    inline Hdl<TType, TI>::Hdl(EHdls pHdl)
        : mHandle(pHdl)
    { }


//------------------------------------------------------------------------------
//                                Autres M�thodes
//------------------------------------------------------------------------------

    /*! @return La valeur du handle
     */
    template<class TType, int TI>
    inline TType Hdl<TType, TI>::value() const
    {
        return mHandle;
    }


    /*! @return VRAI si le handle est diff�rent de PAK::eBadHdl, FAUX sinon
     */
    template<class TType, int TI>
    inline bool Hdl<TType, TI>::isValid() const
    {
        return mHandle != eBadHdl;
    }


//------------------------------------------------------------------------------
//                                  Op�rateurs
//------------------------------------------------------------------------------

    /*! Test d'�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator==(Hdl<TType, TI> pL, Hdl<TType, TI> pR)
    {
        return (pL.value() == pR.value());
    }


    /*! Test d'�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator==(EHdls pL, Hdl<TType, TI> pR)
    {
        return (TType(pL) == pR.value());
    }


    /*! Test d'�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator==(Hdl<TType, TI> pL, EHdls pR)
    {
        return (pL.value() == TType(pR));
    }


    /*! Test d'in�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont distincts, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator!=(Hdl<TType, TI> pL, Hdl<TType, TI> pR)
    {
        return !(pL == pR);
    }


    /*! Test d'in�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont distincts, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator!=(EHdls pL, Hdl<TType, TI> pR)
    {
        return !(pL == pR);
    }


    /*! Test d'in�galit�.
        @param pL Premier handle � comparer
        @param pR Second handle � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont distincts, FAUX sinon
     */
    template<class TType, int TI>
    inline bool operator!=(Hdl<TType, TI> pL, EHdls pR)
    {
        return !(pL == pR);
    }


    /*! Op�rateur strictement inf�rieur �.
        @param pL Premier op�rande de l'op�rateur
        @param pR Second op�rande de l'op�rateur
        @return VRAI si <i>pL</i> est <b>strictement</b> inf�rieur � <i>pR</i>
     */
    template<class TType, int TI>
    inline bool operator<(Hdl<TType, TI> pL, Hdl<TType, TI> pR)
    {
        return (pL.value() < pR.value());
    }
}
