/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "PAcKage.hh"

/*! @file GDK/PAcKage/PAcKage.cc
    @brief D�finitions diverses du module @ref PAcKage.
    @author @ref Guillaume_Terrissol
    @date 27 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace PAK
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kFileWriteFailed,           "L'�criture d'un fichier a �chou�.",                                                "File write failed.")
    REGISTER_ERR_MSG(kInvalidFile,               "Fichier invalide.",                                                                "Invalid handle.")
    REGISTER_ERR_MSG(kInvalidFileHandle,         "Handle de fichier invalide.",                                                      "Invalid file handle.")
    REGISTER_ERR_MSG(kInvalidSize,               "Taille invalide.",                                                                 "Invalid size.")
    REGISTER_ERR_MSG(kOpenFileFailed,            "Echec de l'ouverture du fichier.",                                                 "Open file failed.")
    REGISTER_ERR_MSG(kPakFileHeaderSizeOverflow, "Taille de l'en-t�te du Pak File trop important : risque d'�crasement de donn�es.", "Pak File header size too big : possible data overwriting.")
    REGISTER_ERR_MSG(kTooManyFiles,              "Cr�ation d'un pak file : trop de fichiers potentiels.",                            "Pak File creation : too many potential files.")
    REGISTER_ERR_MSG(kUndefinedFAT,              "FAT non-d�finie.",                                                                 "Undefined FAT.")
    REGISTER_ERR_MSG(kUndefinedPakFile,          "Pak File non-d�fini.",                                                             "Undefined Pak File.")


//------------------------------------------------------------------------------
//                       Cha�nes de Caract�res Constantes
//------------------------------------------------------------------------------

    const char* kPakFileExtension = ".pak";


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page PAK_Package_Page Module PAK
        @version 0.9
        @warning Obsol�te

        @section PAK_PakFileMgr Le gestionnaire de fichiers
        Il fallait garder � l'esprit que le gestionnaire de fichiers devait �tre un singleton (pour
        garder coh�rent LE pak file ouvert), et que l'instance pouvait �tre acc�d�e indiff�remment en mode
        moteur et �diteur (alors que, dans ce dernier mode, de nombreuses op�rations viennent s'ajouter �
        celles du mode moteur). Je ne voulais pas utiliser de m�thodes virtuelles (vides pour la version
        moteur) afin de ne pas allourdir l'interface. En outre, la liaison avec une ligne de commande pour
        appeler les m�thodes devait �tre possible.<br><br>
        J'ai opt� pour une solution qui peut sembler un peu lourde (et lente), mais qui reste relativement
        raisonnable (et finalement tr�s souple) : toutes les commandes qu'il serait possible d'appeler via
        une ligne de commande (ouverture / fermeture de pak file, cr�ation de r�pertoire, d�placement de
        fichier,...) sont appel�es via une seul m�thode : PAK::OPakFileMgr::Do. Un seul param�tre : un
        tableau de cha�ne de caract�res (en fait, les mots de la ligne de commande). Ainsi, on peut
        ajouter une commande sans pour autant modifier l'interface publique. L'acc�s aux fichiers
        proprement dit se fait gr�ce aux classes PAK::OLoader, PAK::OSaver (voir chapitre suivant : @ref
        PAK_Loaders_And_Savers).<br><br>

        @section PAK_Loaders_And_Savers Loaders & Savers
        Ce sont les 2 classes � utiliser pour lire et �crire des donn�es dans le pak file : elles servent
        d'interface s�curis�e. Elles ont �t� �crites de telle sorte que leur emploi se rapproche de celui
        des flux : �criture avec PAK::OSaver::operator<<, lecture avec PAK::OLoader::operator>>. Les
        utilisateurs n'ont pas � se d�placer dans le fichier, les donn�es sont lues s�quentiellement,
        sans possibilit� de retour (si une donn�e est requise plusieurs fois, elle doit �tre m�moris�e
        dans une variable).<br>
        Le constructeur de chacune de ces classes prend en param�tre un handle sur un fichier (d�j�
        cr��). Vous pouvez utiliser la m�thode PAK::OEditorPakFileMgr::FileHdlFromName, en mode �diteur,
        pour obtenir un handle de fichier (en mode moteur, un tel handle doit �tre connu par le contenu
        d'un fichier). La taille des donn�es (surtout utile pour un PAK::OLoader) est consultable
        (PAK::OLoader::Size et PAK::OSaver::Size). Le destructeur de PAK::OSaver se charge d'�crire
        toutes les donn�es dans le pak file (pour �viter les risques, n'allouez pas dynamiquement de
        PAK::OSaver).
        @code
    {
        PAK::OFileHdl   aHdl = ...
        PAK::OSaver     aSaver(aHdl);
        OClass          aInstance;
        // ...
        aSaver << aInstance;
        // ...
    }   // Le saver �crira les donn�es � la sortie du bloc.
        @endcode
    */
}
