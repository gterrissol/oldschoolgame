/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_PAKFILEMGR_HH
#define PAK_PAKFILEMGR_HH

#include "PAcKage.hh"

/*! @file GDK/PAcKage/PakFileMgr.hh
    @brief En-t�te de la classe PAK::PakFileMgr.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"
#include "THRead/Mutex.hh"

#include "Handle.hh"

namespace PAK
{
    /*! @brief Acc�s au fichier syst�me (interface).
        @version 1.2

        Un ensemble de commandes (m�thodes priv�es) permet de manipuler le pak file. Celles-ci sont
        introduites au fur et � mesure qu'elles sont disponibles. Leur impl�mentation peut varier dans
        les classes d�riv�es (ex: ouverture et fermeture de fichiers pak), c'est pourquoi elle est
        d�finie sous forme de m�thodes virtuelles prot�g�es.<br>
        Il peut sembler bizarre que la pseudo-interface (les commandes) soit priv�e alors que
        l'impl�mentation (les m�thodes virtuelles) est prot�g�e, mais cela emp�che les classes d�riv�es
        d'utiliser directement les commandes, tandis que, d'un point de vue conceptuel, il para�t normal
        qu'elles puissent modifier les m�thodes d'impl�mentation.
        @sa PAK::EnginePakFileMgr & EDIT::PakFileMgr
     */
    class PakFileMgr : public MEM::Singleton<PakFileMgr>, public MEM::OnHeap
    {
        //! @name Classes amies
        //@{
 friend class Loader;                                                          //!< Pour blockHdl(), fileSize() & getData().
 friend class Saver;                                                           //!< Pour blockHdl(), fileSize() & putData().
        //@}
    public:
        //! @name Type de pointeur
        //@{
        class Manager;
        typedef SharedPtr<Manager>  ManagerPtr;                                 //!< Pointeur sur "gestionnaire".
        //@}
        //! @name Constructeur & destructeur
        //@{
                    PakFileMgr();                                               //!< Constructeur par d�faut.
virtual             ~PakFileMgr();                                              //!< Destructeur.
        //@}
        //! @name Gestion du pak file
        //@{
        void        createFile(const String& pName, I32 pMaxFileCount);         //!< Cr�ation d'un <b>pak</b> file.
        void        openFile(const String& pName);                              //!< Ouverture d'un <b>pak</b> file.
        void        editFile(const String& pName);                              //!< Edition d'un <b>pak</b> file.
        void        closeFile();                                                //!< Fermeture d'un <b>pak</b> file.
        //@}
        //! @name Acc�s aux donn�es des fichiers
        //@{
        /*! @brief Gestionnaire de fichier.
            @version 0.8

            Afin de limiter le nombre de m�thodes en mode moteur, une bonne partie du comportement cod�
            pr�c�demment a �t� d�port� dans l'IDE, notamment la gestion des blocs de <b>pak</b> file. Il
            a donc fallu trouver un moyen de permettre cette gestion sans passer par la d�rivation de
            PAK::PakFileMgr (singleton construit de mani�re unique par PROG::Hardware). J'ai donc
            choisi de passer par un objet fonction qui doit effectuer toute t�che suppl�mentaire requise
            pour pr�parer l'�criture des donn�es d'un fichier dans le <b>pak</b> file (actuellement, le
            contr�le de la taille du fichier, et l'export des donn�es).
         */
        class Manager : public MEM::OnHeap
        {
        public:
            //! @name Interface publique
            //@{
    virtual             ~Manager();                                             //!< Destructeur.
            void        operator()(FileHdl pHdl, const U8* pData, U32 pCount);  //!< Op�rateur fonction (patron de m�thode).
            void        initialize(const Vector<File>& pFiles);                 //!< Donn�es initiales des fichiers.
            //@}

        protected:
            //! @name Acc�s aux informations des fichiers
            //@{
            BlockHdl    dataStart() const;                                      //!< D�but des donn�es du <b>pak file</b>.
            void        defineFile(FileHdl pHdl, File pFile);                   //!< Ajout d'un fichier au <b>pak</b> file.
            BlockHdl    blockHdl(FileHdl pHdl) const;                           //!< Handle vers les donn�es d'un fichier.
            U32         fileSize(FileHdl pHdl) const;                           //!< Taille d'un fichier.
            //@}

        private:
            //! @name Impl�mentation
            //@{
    virtual void        manage(FileHdl pHdl, const U8* pData, U32 pCount);      //!< Contr�le de la taille d'un fichier.
    virtual void        setFileData(const Vector<File>& pFiles);                //!< Initialisation avec les donn�es des fichiers.
            //@}
        };

        void        setManager(ManagerPtr pManager);                            //!< D�finition d'un "gestionnaire".


    private:

        void        getData(FileHdl pHdl, U8* pData, U32 pCount);               //!< R�cup�ration de donn�es.
        void        putData(FileHdl pHdl, const U8* pData, U32 pCount);         //!< Sauvegarde de donn�es.
        void        defineFile(FileHdl pHdl, File pFile);                       //!< Ajout d'un fichier au <b>pak</b> file.
        BlockHdl    blockHdl(FileHdl pHdl) const;                               //!< Handle vers les donn�es d'un fichier.
        U32         fileSize(FileHdl pHdl) const;                               //!< Taille d'un fichier.
        //@}

        FORBID_COPY(PakFileMgr)
        PIMPL()
    };
}

#endif  // De PAK_PAKFILEMGR_HH
