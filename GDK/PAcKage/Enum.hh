/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_ENUM_HH
#define PAK_ENUM_HH

/*! @file GDK/PAcKage/Enum.hh
    @brief Enum�rations du module PAK.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace PAK
{
//------------------------------------------------------------------------------
//                                 Enum�rations
//------------------------------------------------------------------------------

    //! Constantes pour la gestion de la FAT
    enum EMax
    {
        eMaxFileCount   = 0xFFFF,       //!< Nombre maximum de fichiers dans un Pak File
        eMaxPakFileSize = 0x7FFFFFFF    //!< Taille maximale d'un <b>pak file</b> (sans la FAT).
    };


    //! Valeurs de handles remarquables
    enum EHdls
    {
        eRoot           = 0,            //!< Index du r�pertoire racine dans la FAT.
        eBadHdl         = 0xFFFF,       //!< Handle de fichier invalide.
        eBadBlockHdl    = 0xFFFFFFFF    //!< Handle de bloc invalide.
    };


    //! R�pertoires pr�sents par d�faut.
    enum EDirs
    {
        eWD             = 0,            //!< R�pertoire de travail.
        eUp             = 1             //!< R�pertoire parent.
    };


    //! Autre �num�ration.
    enum
    {
        eBootFileHdl    = 0             //!< Handle du fichier boot.bin.
    };


//------------------------------------------------------------------------------
//                       Cha�nes de Caract�res Constantes
//------------------------------------------------------------------------------

    extern const char*  kPakFileExtension;
}

#endif // De PAK_ENUM_HH
