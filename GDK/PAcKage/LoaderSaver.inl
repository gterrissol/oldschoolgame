/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/PAcKage/LoaderSaver.inl
    @brief M�thodes inline des classes PAK::Loader & PAK::Saver.
    @author @ref Guillaume_Terrissol
    @date 30 Juillet 2002 - 31 Mai 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/String.hh"

namespace PAK
{
//------------------------------------------------------------------------------
//                        Op�rateurs de Lecture/Ecriture
//------------------------------------------------------------------------------

    /*! Sauvegarde d'une donn�e.
        @param pSaver       Saver dans lequel �crire les donn�es contenues dans <i>pDataToStore</i>
        @param pDataToStore Donn�e � sauvegarder
        @note Cette m�thode est un membre template afin d'offir un maximum de souplesse � l'utilisateur (la gestion de la taille des donn�es
        � �crire est interne)
     */
    template<class TType>
    inline void operator<<(Saver& pSaver, const TType& pDataToStore)
    {
        pSaver.store(&pDataToStore, sizeof(TType));
    }


    /*! Chargement d'une donn�e.
        @param pLoader     Loader � partir duquel lire les donn�es � envoyer dans <i>pDataToLoad</i>
        @param pDataToLoad La donn�e � charger sera plac�e dans cette variable
        @note Cette m�thode est un membre template afin d'offir un maximum de souplesse � l'utilisateur (la gestion de la taille des donn�es
        � lire est interne)
     */
    template<class TType>
    inline void operator>>(Loader& pLoader, TType& pDataToLoad)
    {
        pLoader.load(&pDataToLoad, sizeof(TType));
    }


//------------------------------------------------------------------------------
//                Op�rateurs de Lecture/Ecriture : Sp�cialisation
//------------------------------------------------------------------------------

#ifndef NOT_FOR_DOXYGEN

    template<>
    void operator<<(Saver& pSaver, const String& pStringToStore);
    template<>
    void operator>>(Loader& pLoader, String& pStringToLoad);

#endif  // De NOT_FOR_DOXYGEN
}
