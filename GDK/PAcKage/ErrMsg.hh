/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PAK_ERRMSG_HH
#define PAK_ERRMSG_HH

/*! @file GDK/PAcKage/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module @ref PAcKage.
    @author @ref Guillaume_Terrissol
    @date 2 Ao�t 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace PAK
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kFileWriteFailed;
    extern  ERR::String kInvalidFile;
    extern  ERR::String kInvalidFileHandle;
    extern  ERR::String kInvalidSize;
    extern  ERR::String kOpenFileFailed;
    extern  ERR::String kPakFileHeaderSizeOverflow;
    extern  ERR::String kTooManyFiles;
    extern  ERR::String kUndefinedFAT;
    extern  ERR::String kUndefinedPakFile;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De PAK_ERRMSG_HH
