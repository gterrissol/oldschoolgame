/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_RESOURCEMGR_TCC
#define RSC_RESOURCEMGR_TCC

#include "ResourceMgr.hh"

/*! @file GDK/ReSourCe/ResourceMgr.tcc
    @brief M�thodes inline de la classe RSC::ResourceMgr.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2008 - 28 D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Deque.hh"
#include "STL/Function.hh"
#include "STL/Map.hh"
#include "STL/SharedPtr.hh"

#include "BaseResource.tcc"
#include "HandleMgr.tcc"
#include "Thread.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RSC::ResourceMgr.
        @version 1.0
     */
    template<class TRes>
    class ResourceMgr<TRes>::Private : public MEM::OnHeap
    {
    public:

        typedef typename Map<PAK::FileHdl, TKey>::iterator          Iterator;       //!< Int�rateur pour le dictionnaire de clefs.
        typedef typename Map<PAK::FileHdl, TKey>::const_iterator    ConstIterator;  //!< Int�rateur pour le dictionnaire de clefs.
        typedef std::pair<Iterator, bool>                           InsertResult;   //!< R�sultat d'une insertion dans le dictionnaire de clefs.

        Private();                                                                  //!< Constructeur par d�faut.

        /*! @brief Interface pour l'acc�s � une ressource.
            @version 0.91

            Le gestionnaire de ressources ne manipule pas directement les ressources : il utilise des
            instances de cette classe pour allouer et d�sallouer les instances ressources uniquement
            lorsque cela est n�cessaire.
         */
        class DataPtr : public MEM::OnHeap
        {
        public:
            //! @name Constructeur
            //@{
            inline                  DataPtr();                          //!< Constructeur par d�faut.
            //@}
            //! @name Gestion "m�moire"
            //@{
                    TResPtr         allocate();                         //!< Alloue la ressource.
            inline  bool            isAllocated() const;                //!< La ressource est-elle allou�e ?
                    void            release();                          //!< Lib�re la ressource.
            //@}
            //! @name Acc�s aux attributs
            //@{
            inline  TResPtr         resource();                         //!< Acc�s � la ressource.
            inline  void            setFileHdl(PAK::FileHdl pFileHdl);  //!< Lie une ressource � un fichier.
            inline  PAK::FileHdl    fileHdl() const;                    //!< Handle sur le fichier auquel est li�e la ressource.
            inline  void            setKey(const TKey& pKey);           //!< Assigne sa clef � la [future] ressource.
            inline  const TKey&     key() const;                        //!< Clef de la ressource.
            //@}

        private:
            //! @name Attributs
            //@{
            TResPtr         mResource;                                  //!< Pointeur sur la ressource.
            PAK::FileHdl    mFileHdl;                                   //!< Handle sur le fichier de la ressource.
            TKey            mKey;                                       //!< Clef de la ressource.
            I32             mCounter;                                   //!< Compteur de r�f�rences.
            //@}
        };
        //! @name Ensembles des ressources et de leur clefs
        //@{
        Map<PAK::FileHdl, TKey>             mKeys;              //!< Dictionnaire de clefs.
        SharedPtr<HandleMgr<DataPtr, TKey>> mHandleMgr;         //!< Gestionnaire de handles.
        TResPtr                             mDefaultResource;   //!< Ressource par d�faut (juste construite).
        //@}
        //! @name Gestion des livraisons de ressources
        //@{
        Deque<std::function<void ()>>       mLoadedResources;   //!< Ressources charg�es (mais pas encore livr�es).
        //@}
        PAK::FileHdl                        mFileHdl;           //!< Handle du fichier des donn�es.
        THR::Mutex                          mMutex;             //!< Mutex.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TRes> 
    ResourceMgr<TRes>::Private::Private()
        : mKeys()
        , mHandleMgr(STL::makeShared<HandleMgr<DataPtr, TKey>>())
        , mDefaultResource()
        , mLoadedResources()
        , mFileHdl()
        , mMutex()
    { }


//------------------------------------------------------------------------------
//                  ResourceMgr : Constructeur & Destructeur(s)
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TRes> 
    ResourceMgr<TRes>::ResourceMgr()
        : pthis()
    { }


    /*! Destructeur.
     */
    template<class TRes> 
    ResourceMgr<TRes>::~ResourceMgr()
    {
        // Finit le chargement en cours (s'il y en a).
        Thread::get()->signal(THR::eJoin);
        // Et livre.
        deliverResources();
    }


    /*! Supprime toutes les ressources du gestionnaires. Les copies des clefs deviennent invalides.
        @note Les ressources encore utilis�es ne seront pas supprim�es
        @return Vrai si toutes les ressources ont �t� supprim�e, Faux si certaines d'entre elles �taient
        toujours utilis�es
     */
    template<class TRes> 
    bool ResourceMgr<TRes>::clear()
    {
        bool    lResourcesDeleted = true;

        for(auto lKeyI = pthis->mKeys.begin(); lKeyI != pthis->mKeys.end(); ++lKeyI)
        {
            lResourcesDeleted = lResourcesDeleted && remove(lKeyI->second);
        }

        return lResourcesDeleted;
    }


//------------------------------------------------------------------------------
//            ResourceMgr : Sauvegarde & Restauration des Ressources
//------------------------------------------------------------------------------

    /*! Sauvegarde des ressources.
        @note Le saver sur le fichier dans lequel �crire les correspondances fichiers - ressources,
        ainsi que les clefs sur les ressources est d�termin� � partir du load()
     */
    template<class TRes>
    void ResourceMgr<TRes>::save() const
    {
        if (pthis->mFileHdl != PAK::eBadHdl)
        {
            PAK::Saver  lSaver(pthis->mFileHdl);

            // Nombre d'�l�ments.
            lSaver << U32(pthis->mKeys.size());
            // Paires (handle, clef).
            for(auto lKeyI = pthis->mKeys.begin(); lKeyI != pthis->mKeys.end(); ++lKeyI)
            {
                lSaver << lKeyI->first;     // FileHdl.
                lSaver << lKeyI->second;    // Key.
            }
            // Donn�es du gestionnaires de handles.
            pthis->mHandleMgr->save(lSaver);
        }
    }


    /*! Chargement des ressources.
        @param pLoader Loader sur le fichier contenant les correspondances fichiers - ressources, ainsi
        que les clefs sur les ressources
     */
    template<class TRes> 
    void ResourceMgr<TRes>::load(PAK::Loader& pLoader)
    {
        ASSERT_EX(pthis->mKeys.size() == 0, kAlreadyDefinedData, return;)

        // Nombre d'�l�m�nts.
        U32 lKeyCount = k0UL;
        pLoader >> lKeyCount;
        // Paires (handle, clef).
        for(U32 lK = k0UL; lK < lKeyCount; ++lK)
        {
            PAK::FileHdl    lFileHdl;
            TKey            lKey;

            // Charge le handle de fichier et la clef.
            pLoader >> lFileHdl;
            pLoader >> lKey;

            // Les ins�re dans le dictionnaire de clefs.
            pthis->mKeys[lFileHdl] = lKey;
        }
        // Donn�es du gestionnaires de handles.
        pthis->mHandleMgr->load(pLoader);
        // R�ins�re les donn�es.
        for(auto lKeyI = pthis->mKeys.begin(); lKeyI != pthis->mKeys.end(); ++lKeyI)
        {
            // Chaque donn�e.
            typename Private::DataPtr*  lData = pthis->mHandleMgr->dereference(lKeyI->second);
            // Est reli�e au handle de fichier.
            lData->setFileHdl(lKeyI->first);
            // Et � sa clef.
            lData->setKey(lKeyI->second);
        }

        pthis->mFileHdl = pLoader.fileHdl();
    }

    /*! @return Le handle du fichier contenant les donn�es du gestionnaire.
     */
    template<class TRes>
    PAK::FileHdl ResourceMgr<TRes>::fileHdl() const
    {
        return pthis->mFileHdl;
    }


//------------------------------------------------------------------------------
//                     ResourceMgr : Gestion des Ressources
//------------------------------------------------------------------------------

    /*! Cr�ation d'une ressource.
        @param pFileHdl Handle du fichier contenant les donn�es de la ressource � cr�er
        @return La clef de la ressource cr�e
        @sa Remove
     */
    template<class TRes>
    typename ResourceMgr<TRes>::TKey ResourceMgr<TRes>::insert(PAK::FileHdl pFileHdl)
    {
        // Tentative d'insertion.
        typename Private::InsertResult  lResult = pthis->mKeys.insert(std::make_pair(pFileHdl, TKey()));
        TKey&                           lKey    = lResult.first->second;

        if (lResult.second)
        {
            // Le handle a effectivement �t� ins�r� : une instance de
            // DataPtr est cr��e par le gestionnaire de handles.
            typename Private::DataPtr*  lData = pthis->mHandleMgr->acquire(lKey);
            // L'instance de TRes n'est toutefois pas cr��e : elle le
            // sera lors de sa premi�re "r�cup�ration", via retrieve().

            // Initialise (partiellement) l'instance.
            lData->setFileHdl(pFileHdl);
            lData->setKey(lKey);
        }

        return lKey;
    }


    /*! Acquisition d'une ressource (celle-ci est allou�e si elle ne l'�tait pas encore).
        @param pKey Clef sur la ressource � acqu�rir
        @param pNow VRAI pour charger la ressource imm�diatement, FAUX pour la laisser se charger en t�che de fond
        @return Un pointeur sur la ressource
        @note <b>Important</b> : A chaque acquisition doit correspondre une lib�ration
        (RSC::ResourceMgr<>::release())
        @sa Release
     */
    template<class TRes>
    typename ResourceMgr<TRes>::TResPtr ResourceMgr<TRes>::retrieve(const TKey& pKey, bool pNow)
    {
        if (pKey.isNull())
        {
            return defaulted();
        }

        typename Private::DataPtr*  lData = pthis->mHandleMgr->dereference(pKey);

        if (lData != nullptr)
        {
            if (lData->isAllocated())
            {
                return lData->resource();
            }
            else
            {
                // L'objet doit �tre allou�...
                TResPtr lNewResource = lData->allocate();
                if (pNow)
                {
                    // Puis charg� imm�diatement...
                    lNewResource->load();
                    deliverResources();
                }
                else
                {
                    // Ou en t�che de fond.
                    registerLoad(lData->resource(), &TRes::load);   // Une r�f�rence de plus pour �viter un release() effectif pr�matur�.
                }
                // Avant d'�tre retourn�.
                return lNewResource;
            }
        }
        else
        {
            LAUNCH_EXCEPTION(kInvalidHandle)
        }
    }


    /*! Lib�ration d'une ressource (qui n'est plus utilis�e par un objet qui l'avait acquise).
        @param pResource Ressource � lib�rer
        @note Le pointeur sur la ressource est pass� par r�f�rence et deviendra nul apr�s la lib�ration
        [correcte] de la ressource
     */
    template<class TRes> 
    void ResourceMgr<TRes>::release(TResPtr& pResource)
    {
        if (pResource == nullptr)
        {
            return;
        }

        const TKey& lResKey = pResource->key();

        // Ressource par d�faut ou ressource charg�e ?
        if (lResKey.isNull())
        {
            // La lib�ration de la ressource par d�faut du gestionnaire
            // n'a lieu qu'� la destruction de celui-ci.
            return;
        }

        typename Private::DataPtr*  lData   = pthis->mHandleMgr->dereference(lResKey);

        if (lData != nullptr)
        {
            lData->release();
        }
        else
        {
            LAUNCH_EXCEPTION(kInvalidHandle)
        }

        // La ressource a �t� lib�r�e : le smart pointer (pass� par r�f�rence) est r�initialis�.
        pResource.reset();
    }


    /*! Suppression d'une ressource.
        @param pKey Clef sur la ressource � supprimer
        @return VRAI si la ressource a �t� supprim�e, FAUX si elle �tait encore utilis�e
        @warning <i>pKey</i> (et ses copies) sont invalid�es : il ne faut plus les utiliser
        @note Cette m�thode supprime d�finitivement une ressource
     */
    template<class TRes>
    bool ResourceMgr<TRes>::remove(const TKey& pKey)
    {
        // Ressource par d�faut ou ressource charg�e ?
        if (pKey.isNull())
        {
            // La ressource par d�faut du gestionnaire ne peut pas �tre supprim�e.
            return false;
        }

        // La ressource ne peut pas �tre supprim�e si elle est encore utilis�e.
        typename Private::DataPtr*  lDataToRemove   = pthis->mHandleMgr->dereference(pKey);

        if (!lDataToRemove->isAllocated())
        {
            // Premi�re �tape : retirer lDataToRemove du gestionnaire.
            pthis->mHandleMgr->release(pKey);
            // Deuxi�me �tape : retirer la paire (handle de fichier, clef) du dictionnaire.
            for(auto lKeyI = pthis->mKeys.begin(); lKeyI != pthis->mKeys.end(); ++lKeyI)
            {
                if (lKeyI->second == pKey)
                {
                    pthis->mKeys.erase(lKeyI);
                    // Pas la peine de poursuivre la boucle.
                    break;
                }
            }

            return true;
        }
        else
        {
            // La donn�e est toujours utilis�e (donc toujours allou�e).
            return false;
        }
    }


    /*! Une fois qu'une ressource a �t� charg�e, elle doit �tre livr�e (ses donn�es doivent �tre
        permut�es avec celles de l'instance la portant (voir m�thodes swap())). A la fin de leur
        chargement, la RSC::Resource::load() des ressources enregistrent les callbacks de livraison
        aupr�s de ce gestionnaire. Cette m�thode, appel�e depuis le thread principal, permet de proc�der
        � leur livraison.
     */
    template<class TRes>
    void ResourceMgr<TRes>::deliverResources()
    {
        // La synchronisation n'est utile que dans cette m�thode et registerDeliver().
        THR::Lock   lLock(pthis->mMutex);

        while(!pthis->mLoadedResources.empty())
        {
            // Beaucoup de parenth�ses, mais �a n'a rien de compliqu� (la callback de
            // livraison est simplement appel�e, pour le premier �l�ment de l'ensemble).
            pthis->mLoadedResources.front()();

            pthis->mLoadedResources.pop_front();
        }
    }


    /*! R�cup�re une clef de ressource � partir d'un handle de fichier
        @param pFileHdl Handle de fichier associ� � la ressource recherch�e
        @return La clef de la ressource associ�e � <i>poFileHdl</i> en cas de pr�sence, une clef de
        valeur nulle (invalide) en cas d'absence de la ressource
        @note Le m�me r�sultat pourrait �tre obtenu avec ResourceMgr::oInsert(), si la ressource �tait
        d�j� ins�r�e. L'avantage de cette m�thode est qu'elle n'essaie pas par d�faut d'ajouter une
        nouvelle ressource au gestionnaire
     */
    template<class TRes>
    typename ResourceMgr<TRes>::TKey ResourceMgr<TRes>::key(PAK::FileHdl pFileHdl) const
    {
        // Cherche la paire (poFileHdl, oKey).
        typename Private::ConstIterator lIter   = pthis->mKeys.find(pFileHdl);
        TKey                            lKey;

        if (lIter != pthis->mKeys.end())
        {
            // Trouv�e.
            lKey = lIter->second;
        }

        return lKey;
    }


    /*! Lorsque des ressources en utilisent d'autres (en tant qu'attributs), ces premi�res doivent
        pouvoir utiliser ces sous-ressources en attendant que les d�finitives aient �t� charg�es. Jusqu'�
        ce moment, ce sont des ressources par d�faut qui devront �tre employ�es). Plut�t que d'ajouter,
        pour chaque gestionnaire, des ressources par d�faut "� la main", cette m�thode permet d'obtenir
        simplement une ressource valide, sur laquelle toutes les op�rations standard des ressources
        peuvent s'appliquer.
        @return Une instance de <i>TRes</i>, initialis�e par son seul constructeur
     */
    template<class TRes>
    typename ResourceMgr<TRes>::TResPtr ResourceMgr<TRes>::defaulted()
    {
        if (pthis->mDefaultResource == nullptr)
        {
            // Le constructeur par d�faut de la ressource (mais aussi celui de l'objet) sera appel�.
            pthis->mDefaultResource = STL::makeShared<TRes>();
            setResourceFileHdl(pthis->mDefaultResource, PAK::eBadHdl);
            // En outre, c'est la clef par d�faut (donc invalide) qui est utilis�e.
        }

        return pthis->mDefaultResource;
    }


//------------------------------------------------------------------------------
//             ResourceMgr : Chargement & validation d'une ressource
//------------------------------------------------------------------------------

    /*! Enregistre une callback de chargement pour une ressource.
        @param pRes    Instance dont une m�thode de chargement doit �tre appel�e
        @param pMember M�thode de chargement de <i>ppt_Res</i>
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::registerLoad(TResPtr pRes, void (TRes::*pMember)())
    {
        Thread::get()->checkIn(std::bind(std::mem_fn(pMember), pRes));
    }


    /*! Enregistre une callback de livraison d'une ressource.
        @param pRes    Instance dont une m�thode de livraison doit �tre appel�e
        @param pMember M�thode de livraison de <i>pRes</i>
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::registerDeliver(TResPtr pRes, void (TRes::*pMember)())
    {
        // La synchronisation n'est utile que dans cette m�thode et deliverResources().
        THR::Lock   lLock(pthis->mMutex);
        pthis->mLoadedResources.push_back(std::bind(std::mem_fn(pMember), pRes));
    }


//------------------------------------------------------------------------------
//                            DataPtr : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TRes>
    inline ResourceMgr<TRes>::Private::DataPtr::DataPtr()
        : mResource()
        , mFileHdl(PAK::eBadHdl)
        , mCounter(0)
    { }


//------------------------------------------------------------------------------
//                          DataPtr : Gestion "m�moire"
//------------------------------------------------------------------------------

    /*! Alloue la ressource lorsque celle-ci ne l'a pas encore �t�.
        @return Un pointeur sur la ressource allou�e (mais pas charg�e !)
     */
    template<class TRes>
    typename ResourceMgr<TRes>::TResPtr ResourceMgr<TRes>::Private::DataPtr::allocate()
    {
        ASSERT_EX(!isAllocated(), kAlreadyAllocatedObject, return nullptr;)

        mResource = STL::makeShared<TRes>(fileHdl());
        ResourceMgr<TRes>::setResourceKey(mResource, key());

        // Une r�f�rence sur l'instance.
        mCounter = I32(1);

        return mResource;
    }


    /*! @return VRAI si la ressource a �t� allou�e, FAUX sinon
     */
    template<class TRes>
    inline bool ResourceMgr<TRes>::Private::DataPtr::isAllocated() const
    {
        return (mResource != nullptr);
    }


    /*! Lib�re la ressource (une r�f�rence de moins).<br>
        Lorsque la ressource n'est plus r�f�renc�e, elle est d�sallou�e.
     */
    template<class TRes>
    void ResourceMgr<TRes>::Private::DataPtr::release()
    {
        // Reste-t-il des r�f�rences sur la ressource.
        if (mCounter > 0)
        {
            // Une de moins.
            --mCounter;

            if (mCounter == 0)
            {
                // Il n'y en a plus : d�sallocation.
                mResource.reset();
            }
        }
    }


//------------------------------------------------------------------------------
//                         DataPtr : Acc�s aux Attributs
//------------------------------------------------------------------------------

    /*! Acquiert la ressource (et comptabilise une r�f�rence de plus).
        @return Un pointeur sur la ressource
        @note Cette m�thode ne doit pas �tre utilis�e � la l�g�re, car elle incr�mente le compteur de
        r�f�rences
     */
    template<class TRes>
    inline typename ResourceMgr<TRes>::TResPtr ResourceMgr<TRes>::Private::DataPtr::resource()
    {
        // Une r�f�rence de plus.
        ++mCounter;

        return mResource;
    }


    /*! Indique � quel fichier est li�e la ressource.
        @param pFileHdl Handle sur le fichier contenant les donn�es de la ressources
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::Private::DataPtr::setFileHdl(PAK::FileHdl pFileHdl)
    {
        mFileHdl = pFileHdl;
    }


    /*! @return Le handle du fichier contenant les donn�es de la ressource
     */
    template<class TRes>
    inline PAK::FileHdl ResourceMgr<TRes>::Private::DataPtr::fileHdl() const
    {
        return mFileHdl;
    }


    /*! Indique quelle clef a �t� associ�e � la ressource.
        @param pKey Clef associ�e � la ressource
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::Private::DataPtr::setKey(const TKey& pKey)
    {
        mKey = pKey;
    }


    /*! @return La clef de la ressource
     */
    template<class TRes>
    inline const typename ResourceMgr<TRes>::TKey& ResourceMgr<TRes>::Private::DataPtr::key() const
    {
        return mKey;
    }
}

#endif  // De RSC_RESOURCEMGR_TCC
