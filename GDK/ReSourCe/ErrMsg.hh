/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_ERRMSG_HH
#define RSC_ERRMSG_HH

/*! @file GDK/ReSourCe/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module RSC.
    @author @ref Guillaume_Terrissol
    @date 18 Septembre 2002 - 27 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace RSC
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kAFewHandlesAreStillUsed;
    extern  ERR::String kAlreadyAllocatedObject;
    extern  ERR::String kAlreadyDefinedData;
    extern  ERR::String kHandleAlreadyInitialized;
    extern  ERR::String kIndexOutOfRange;
    extern  ERR::String kInvalidHandle;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De RSC_ERRMSG_HH
