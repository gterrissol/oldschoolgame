/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_THREAD_HH
#define RSC_THREAD_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/Thread.hh
    @brief En-t�te de la classe RSC::LoadingThread.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2008 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Singleton.hh"
#include "THRead/BaseThread.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//              Thread Associ� au Chargement Dynamique des Donn�es
//------------------------------------------------------------------------------

    /*! @brief Thread d�di� au chargement des ressources.
        @version 1.1
        @ingroup ReSourCe

        Un seul thread est utilis� pour charger les donn�es : cette classe est donc un singleton.
        @note Aucune macro n'est d�finie pour simplifier l'utilisation de ce singleton, car il n'a pas
        � �tre utilis� depuis l'ext�rieur du module
     */
    class Thread : public MEM::Singleton<Thread>, public THR::Thread
    {
    public:

virtual ~Thread();  //!< Destructeur.
    };
}

#endif  // De RSC_THREAD_HH
