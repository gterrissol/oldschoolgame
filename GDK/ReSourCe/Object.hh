/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_OBJECT_HH
#define RSC_OBJECT_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/Object.hh
    @brief En-t�te des classes RSC::Object & RSC::HasHandle.
    @author @ref Guillaume_Terrissol
    @date 14 Avril 2003 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "PAcKage/Handle.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/Shared.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                Objet du Moteur
//------------------------------------------------------------------------------

    /*! @brief Objet (cr�� � partir d'un fichier)
        @version 0.75
        @ingroup ReSourCe

        Tout objet constructible � partir d'un fichier d'un <b>pak</b> file doit d�river de cette classe.<br>
     */
    class Object : public STL::enable_shared_from_this<Object>, public MEM::OnHeap
    {
        //! @name Classes et fonctions amies
        //@{
 friend class HasHandle;                                                //!< Pour l'acc�s au handle de fichier.
 friend class ObjectMgr;                                                //!< Pour l'acc�s au handle de fichier.
        template<class TRes>
 friend class ResourceMgr;                                              //!< Pour l'acc�s � setFileHdl().
 friend class Storer;                                                   //!< Pour l'acc�s au handle de fichier & � store().
        //@}
    public:
        //! @name Constructeurs & destructeur
        //@{
explicit                        Object();                               //!< Constructeur par d�faut.
                                Object(const PAK::Loader& pLoader);     //!< Constructeur (chargement imm�diat).
virtual                         ~Object();                              //!< Destructeur.


    protected:

                                Object(PAK::FileHdl pFileHdl);          //!< Constructeur (chargement diff�r� : par ressource)..
        //@}
        //! @name "Permanence" des donn�es
        //@{
                void            saveSelf() const;                       //!< Demande de sauvegarde des donn�es.
        inline  void            activateSaveSelf(bool pOn);             //!< Activation de la sauvegarde des donn�es.
virtual         void            store(PAK::Saver& pSaver) const = 0;    //!< Sauvegarde des donn�es.
        //@}
        //! @name Handle des fichiers de donn�es
        //@{
        inline  PAK::FileHdl    fileHdl() const;                        //!< R�cup�ration du handle de fichier associ�.
 static inline  PAK::FileHdl    objectFileHdl(const Object* pObject);   //!< R�cup�ration du handle de fichier associ� � un objet.


    private:

                void            setFileHdl(PAK::FileHdl pFileHdl);      //!< D�finition du handle de fichier associ�.

        PAK::FileHdl    mFileHdl;                                       //!< Handle de fichier associ�.
        bool            mSaveActivated;                                 //!< Activation de la sauvegarde.
        //@}
    };


//------------------------------------------------------------------------------
//                              Classes Assistantes
//------------------------------------------------------------------------------

    /*! @brief Recherche d'objet par handle.
        @version 0.5

        Ce foncteur est destin� � �tre utilis� dans un algorithme (de la STL, par exemple) pour trouver, dans un ensemble d'objets, une
        instance poss�dant un handle de fichier donn�.
     */
    class HasHandle : public MEM::Auto<HasHandle>
    {
    public:

        inline          HasHandle(PAK::FileHdl pFileHdl);                   //!< Constructeur.

        inline  bool    operator()(const RSC::Object& pObject) const;       //!< Op�rateur fonction.
        inline  bool    operator()(const RSC::Object* pObject) const;       //!< Op�rateur fonction.
        template<class TObj>
        inline  bool    operator()(const SharedPtr<TObj>& pObject) const;   //!< Op�rateur fonction.

    private:

        PAK::FileHdl mFileHdlToFind;                                        //!< Handle de fichier recherch�.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Object.inl"

#endif  // De RSC_OBJECT_HH

