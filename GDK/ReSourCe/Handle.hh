/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

#ifndef RSC_HANDLE_HH
#define RSC_HANDLE_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/Handle.hh
    @brief En-t�te de la classe RSC::Handle.
    @author Code original : Scott Bilas
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 13 Septembre 2011
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

#include "MEMory/BuiltIn.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                            Classe Handle G�n�rique
//------------------------------------------------------------------------------

    /*! @brief Handle g�n�rique.
        @version 1.5
        @ingroup RSC_Handles

     */
    template<typename TTag>
    class Handle : public MEM::Auto<Handle<TTag>>
    {
    public:
        //! @name Contructeurs
        //@{
        inline          Handle();                       //!< Constructeur par d�faut.
        inline  void    init(U32 pIndex);               //!< Initialisation.
        //@}
        //! @name M�thodes d'acc�s
        //@{
        inline  U32     getIndex() const;               //!< R�cup�re l'index.
        inline  U32     getMagic() const;               //!< R�cup�re le nombre magique.
        inline  I32     value() const;                  //!< R�cup�re le handle.
        inline  bool    isNull() const;                 //!< Teste si le handle est nul.
        inline          operator I32() const;           //!< Convertit le handle en entier 32 bits.
        //@}

    private:

        enum
        {
            // Nombre de bits utilis�s pour (resp.) l'index et le nombre magique.
            eMaxBitsIndex   = 19,                       //!< Champ de bits de l'index.
            eMaxBitsMagic   = 13,                       //!< Champ de bits du nombre magique.
            // Valeurs maximales pour l'index (resp.) et le nombre magique.
            eMaxIndex       = (1 << eMaxBitsIndex) - 1, //!< D�r�f�rence pour l'index.
            eMaxMagic       = (1 << eMaxBitsMagic) - 1  //!< D�r�f�rence pour le nombre magique.
        };
        //! Composition de la valeur du handle.
        union
        {
            //! Index & nombre magique.
            struct 
            {
                U32::TType  Index : eMaxBitsIndex;      //!< Index dans le tableau de ressources.
                U32::TType  Magic : eMaxBitsMagic;      //!< Nombre magique pour contr�ler la structure.
            } m;
            I32::TType      mValue;                     //!< Handle proprement dit.
        };

 static U32  smAutoMagic;                               //! Nombre magique de la classe.
    };


    //! @name Op�rateurs de comparaison
    //@{
    template<class TTag>
    inline  bool    operator==(const Handle<TTag>& pL, const Handle<TTag>& pR); //!< Test d'�galit�.
    template<class TTag>
    inline  bool    operator!=(const Handle<TTag>& pL, const Handle<TTag>& pR); //!< Test d'in�galit�.
    //@}
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Handle.inl"

#endif  // De RSC_HANDLE_HH
