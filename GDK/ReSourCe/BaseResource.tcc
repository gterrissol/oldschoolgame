/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_BASERESOURCE_TCC
#define RSC_BASERESOURCE_TCC

#include "BaseResource.hh"

/*! @file GDK/ReSourCe/BaseResource.tcc
    @brief M�thodes inline des classes RSC::Resource & RSC::ResourceHelper.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2008 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "STL/Shared.tcc"

#include "ResourceMgr.hh"

namespace RSC
{
    /*! @brief D�l�gu� pour une ressource.
        @version 1.3
        @ingroup ReSourCe

        Cette classe permet aux instances de type Resource@<<i>TObj</i>@> d'acc�der � certaines m�thodes
        priv�es de leur manager :<br>
        enregistrement des callbacks de chargement et de livraison.<br>
        @note Le param�tre template, <i>TRes</i>, doit �tre une instance d'une classe d�riv�e d'une
        sp�cialisation de RSC::Resource
        @note Le seul int�r�t de cette classe est de permettre � RSC::ResourceMgr de conserver ses
        m�thodes d'enregistrement de callbacks priv�es (seules les ressources peuvent utiliser ces 2
        m�thodes, mais les ressources <i>ne doivent pas</i> �tre amies de leur gestionnaire, il a donc
        fallu introduire une classe qui limite aux seules ressources l'acc�s � ces m�thodes)
     */
    template<class TRes>
    class ResourceHelper : public MEM::Auto<ResourceHelper<TRes>>
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<TRes> TResPtr;                    //!< Pointeur sur ressource.
        //@}
                ResourceHelper(TResPtr pResource);          //!< Constructeur.
        //! @name Enregistrement de callbacks
        //@{
        void    registerDeliver(void(TRes::* pMember)());   //!< Enregistre une callback de livraison pour une ressource.
        //@}

    private:

        TResPtr mResource;                                  //!< Instance dont des m�thodes seront enregistr�es aupr�s du gestionnaire.
    };


//------------------------------------------------------------------------------
//                         ResourceHelper : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pResource Instance dont des m�thodes (Resource::Load & Resource::deliver()) seront
        enregistr�es pour ex�cution aupr�s de son gestionnaire
     */
    template<class TRes>
    ResourceHelper<TRes>::ResourceHelper(TResPtr pResource)
        : mResource(pResource)
    {
        // Il faut v�rifier que TRes est bien une ressource.
    }


//------------------------------------------------------------------------------
//                 ResourceHelper : Enregistrement de Callbacks
//------------------------------------------------------------------------------

    /*! Enregistre une callback de livraison pour une ressource.
        @param pMember Cette m�thode (pour l'instance d�finie lors de la construction de l'instance) sera
        appel�e par un thread : elle doit permettre de livrer une donn�e charg�e (au moins partiellement)
     */
    template<class TRes>
    void ResourceHelper<TRes>::registerDeliver(void(TRes::* pMember)())
    {
        ResourceMgr<TRes>::get()->registerDeliver(mResource, pMember);
        ResourceMgr<TRes>::get()->release(mResource);   // Pour ResourceMgr::retrieve()
        ResourceMgr<TRes>::get()->release(mResource);   // Pour Resource::load()
    }


//------------------------------------------------------------------------------
//                      Resource : Cr�ation de la ressource
//------------------------------------------------------------------------------

    /*! Cette m�thode est appel�e afin de charger une ressource.
        @note Le fichier contenant les donn�es � charger est connue par la ressource
        (RSC::Resource::fileHdl())
        @sa Deliver, RSC::ResourceHelper
        @note Cette m�thode est appel�e dans le thread de chargement
     */
    template<class TObj>
    void Resource<TObj>::load()
    {
        // Charge les donn�es du fichier.
        PAK::Loader lLoader(this->fileHdl());

        // Cr�e la resource (l'objet).
        mDynamicallyLoadedInstance.reset(new TObj(lLoader));

        // Puis, demande sa livraison.
        typedef typename RSC::ResourceHelper<Resource<TObj>>::TResPtr::element_type  TRes;
        // NB: la ressource doit continuer � �tre port� par le "m�me" shared pointer
        // (i.e. shared_from_this() n'est pas suffisant : la ressource pourrait �tre
        // supprim�e entre la fin du load() et l'appel � deliver().
        RSC::ResourceHelper<Resource<TObj>> lHelper(ResourceMgr<TRes>::get()->retrieve(this->key()));
        lHelper.registerDeliver(&Resource<TObj>::deliver);
    }
}

#endif  // De RSC_BASERESOURCE_TCC
