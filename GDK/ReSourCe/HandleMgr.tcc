/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

#ifndef RSC_HANDLEMGR_TCC
#define RSC_HANDLEMGR_TCC

#include "HandleMgr.hh"

/*! @file GDK/ReSourCe/HandleMgr.tcc
    @brief M�thodes inline de la classe RSC::HandleMgr.
    @author Code original : Scott Bilas    
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 30 Juin 2013
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

#include "MEMory/PImpl.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "HandleMgr.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RSC::HandleMgr.
        @version 1.0
     */
    template<class TData, class THdl>
    class HandleMgr<TData, THdl>::Private : public MEM::OnHeap
    {
    public:

                        Private();                                              //!< Constructeur.
        const TData*    getNextHelp(Vector<U32>::const_iterator pBegin) const;  //!< Acc�s � la ressource suivante.

        //! @name Base de donn�es
        //@{
        Vector<TData>   mUserData;                                              //!< Tableau des ressources.
        Vector<U32>     mMagicNumbers;                                          //!< Tableau des nombres magiques.
        Vector<U32>     mFreeSlots;                                             //!< Tableau des indices libres.
        //@}
    };


//------------------------------------------------------------------------------
//                               P-Impl : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TData, class THdl>
    HandleMgr<TData, THdl>::Private::Private()
        : mUserData()
        , mMagicNumbers()
        , mFreeSlots()
    { }


    /*! Cherche la premi�re ressource utilis�e apr�s celle r�f�renc�e par l'it�rateur <i>pBegin</i>.
        @param pBegin It�rateur permettant d'identifier la premi�re ressource � consid�rer pour
        commencer la recherche
        @return Un pointeur sur la ressource (utilis�e) trouv�e � partir de <i>pBegin</i> si elle existe,
        nullptr sinon
     */
    template<class TData, class THdl>
    inline const TData* HandleMgr<TData, THdl>::Private::getNextHelp(Vector<U32>::const_iterator pBegin) const
    {
        for(auto lEnd = mMagicNumbers.end(); pBegin != lEnd; ++pBegin)
        {
            if (*pBegin != k0UL)
            {
                U32 lIndex = (pBegin - mMagicNumbers.begin());

                return (mUserData.begin() + lIndex);
            }
        }

        return nullptr;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut, vide.
     */
    template<class TData, class THdl>
    inline HandleMgr<TData, THdl>::HandleMgr()
        : pthis()
    { }
    

    /*! Destructeur. V�rifie que tous les handles ont �t� lib�r�s.
     */
    template<class TData, class THdl>
    inline HandleMgr<TData, THdl>::~HandleMgr()
    {
        WARNING_IF(hasUsedHandles(), kAFewHandlesAreStillUsed)
    }


//------------------------------------------------------------------------------
//                           Sauvegarde & Restauration
//------------------------------------------------------------------------------

    /*! Les handles, outre leur aspect s�curis� par rapport aux pointeurs, offrent �galement la
        possibilit� d'une r�utilisation d'une session � une autre. Ils sont simplement sauvegard�s dans
        un fichier du <b>pak</b> file.
        @param pSaver Saver vers le fichier de conservation des handles
        @sa Load
     */
    template<class TData, class THdl>
    void HandleMgr<TData, THdl>::save(PAK::Saver& pSaver) const
    {
        // Sont sauvegard�s, dans l'ordre :
        // La taille des tableaux de donn�es.
        pSaver << U32(pthis->mUserData.size());    // == pthis->mMagicNumbers.size.
        pSaver << U32(pthis->mFreeSlots.size());
        // Les nombres magiques.
        for(U32 lM = k0UL; lM < pthis->mMagicNumbers.size(); ++lM)
        {
            pSaver << pthis->mMagicNumbers[lM];
        }
        // Indices de donn�es libres.
        for(U32 lF = k0UL; lF < pthis->mFreeSlots.size(); ++lF)
        {
            pSaver << pthis->mFreeSlots[lF];
        }
    }


    /*! Charge des handles pr�alablement sauvegard�s par ce m�me gestionnaire.
        @param pLoader Loader vers le fichier de conservation des handles
        @note A charge de l'utilisateur de recr�er ses donn�es (elles devraient �tre sauvegard�es par
        l'objet dont ce gestionnaire est un attribut)
        @sa Save
     */
    template<class TData, class THdl>
    void HandleMgr<TData, THdl>::load(PAK::Loader& pLoader)
    {
        // Taille des tableaux de donn�es.
        U32 lDataSize       = k0UL;
        U32 lFreeSlotsSize  = k0UL;
        pLoader >> lDataSize;
        pLoader >> lFreeSlotsSize;

        // Redimensionnement.
        pthis->mUserData.resize(lDataSize);
        pthis->mMagicNumbers.resize(lDataSize, k0UL);
        pthis->mFreeSlots.resize(lFreeSlotsSize, k0UL);

        // Nombres magiques.
        for(U32 lM = k0UL; lM < pthis->mMagicNumbers.size(); ++lM)
        {
            pLoader >> pthis->mMagicNumbers[lM];
        }
        // Indices de donn�es libres.
        for(U32 lF = k0UL; lF < pthis->mFreeSlots.size(); ++lF)
        {
            pLoader >> pthis->mFreeSlots[lF];
        }
    }


//------------------------------------------------------------------------------
//                                  Acquisition
//------------------------------------------------------------------------------

    /*! Acquiert une ressource.
        @param pHdl Ce handle prendra comme valeur un handle vers la ressource allou�e
        @return Un pointeur vers la ressource allou�e
     */
    template<class TData, class THdl>
    TData* HandleMgr<TData, THdl>::acquire(THdl& pHdl)
    {
        // Si la liste de handles libres est vide, en cr�e un nouveau, sinon utilise le premier trouv�
        U32 lIndex  = k0UL;

        if (pthis->mFreeSlots.empty())
        {
            lIndex = U32(pthis->mMagicNumbers.size());
            pHdl.init(lIndex);
            pthis->mUserData.push_back(TData());
            pthis->mMagicNumbers.push_back(pHdl.getMagic());
        }
        else
        {
            lIndex = pthis->mFreeSlots.back();
            pHdl.init(lIndex);
            pthis->mFreeSlots.pop_back();
            pthis->mMagicNumbers[lIndex] = pHdl.getMagic();
        }

        return &pthis->mUserData[lIndex];
    }
    
    
    /*! Lib�re une ressource allou�e via ce gestionnaire.
        @param pHdl Handle vers la ressource � lib�rer
     */
    template<class TData, class THdl>
    void HandleMgr<TData, THdl>::release(const THdl& pHdl)
    {
        // Quel handle lib�rer ?
        U32 lIndex  = pHdl.getIndex();

        // V�rification de sa validit�.
        ASSERT_EX(lIndex < pthis->mUserData.size(), kIndexOutOfRange, return;)
        ASSERT_EX(pthis->mMagicNumbers[lIndex] == pHdl.getMagic(), kInvalidHandle, return;)

        // Retire le handle (le marque comme inutilis� et l'ajoute � la liste des handles libres).
        pthis->mMagicNumbers[lIndex] = k0UL;
        pthis->mFreeSlots.push_back(lIndex);
    }


//------------------------------------------------------------------------------
//                                D�r�f�rencement
//------------------------------------------------------------------------------

    /*! @param pHdl Handle vers une ressource
        @return Un pointeur vers la ressource identifi�e par <i>pHdl</i>
     */
    template<class TData, class THdl>
    TData* HandleMgr<TData, THdl>::dereference(const THdl& pHdl)
    {
        // V�rification rapide.
        if (pHdl.isNull())
        {
            return nullptr;
        }

        // get some vars
        U32 lIndex  = pHdl.getIndex();

        // V�rifie la validit� du handle.
        if ((pthis->mUserData.size() <= lIndex) || (pthis->mMagicNumbers[lIndex] != pHdl.getMagic()))
        {
            // Handle invalide.
            ASSERT(false, kInvalidHandle)
            return nullptr;
        }
        else
        {
            // Handle correct : r�cup�re la donn�e associ�e.
            return &(pthis->mUserData[lIndex]);
        }
    }
    
    
    /*! @param pHdl Handle vers une ressource
        @return Un pointeur vers la ressource identifi�e par <i>pHdl</i>
     */
    template<class TData, class THdl>
    inline const TData* HandleMgr<TData, THdl>::dereference(const THdl& pHdl) const
    {
        // L'appel est valide : Dereference ne modifie rien.
        return const_cast<HandleMgr<TData, THdl>*>(this)->dereference(pHdl);
    }


//------------------------------------------------------------------------------
//                                   Requ�tes
//------------------------------------------------------------------------------

    /*! @return Le nombre de handles utilis�s
     */
    template<class TData, class THdl>
    inline U32 HandleMgr<TData, THdl>::getUsedHandleCount() const
    {
        return U32(pthis->mMagicNumbers.size() - pthis->mFreeSlots.size());
    }


    /*! @return VRAI si au moins un handle est utilis�, FAUX sinon
     */
    template<class TData, class THdl>
    inline bool HandleMgr<TData, THdl>::hasUsedHandles() const
    {
        return (getUsedHandleCount() != k0UL);
    }


    /*! Cette m�thode et getNext() permettent de parcourir l'ensemble des ressources allou�es (� la
        mani�re d'un it�rateur).
        @return Un pointeur sur la premi�re ressource allou�e
     */
    template<class TData, class THdl>
    inline const TData* HandleMgr<TData, THdl>::getFirst() const
    {
        return pthis->getNextHelp(pthis->mMagicNumbers.begin());
    }


    /*! Cette m�thode et getFirst() permettent de parcourir l'ensemble des ressources allou�es (� la
        mani�re d'un it�rateur).
        @return La premi�re ressource utilis�e juste apr�s <i>pPrevious</i>
     */
    template<class TData, class THdl>
    inline const TData* HandleMgr<TData, THdl>::getNext(const TData* pPrevious) const
    {
        I32 lIndex = (pPrevious - pthis->mUserData.begin()) + 1;

        return pthis->getNextHelp(pthis->mMagicNumbers.begin() + lIndex);
    }
}

#endif  // De RSC_HANDLEMGR_TCC
