/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Object.hh"

/*! @file GDK/ReSourCe/Object.cc
    @brief M�thodes (non-inline) de la classe RSC::Object.
    @author @ref Guillaume_Terrissol
    @date 14 Avril 2003 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/LoaderSaver.hh"
#include "STL/Shared.tcc"

#include "ObjectMgr.hh"


namespace RSC
{
//------------------------------------------------------------------------------
//                          Constructeurs & destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut. Aucun handle de fichier n'�tant associ� � l'instance, le handle invalide
        lui est assign�.
     */
    Object::Object()
        : mFileHdl(PAK::eBadHdl)
        , mSaveActivated(false)
    { }


    /*! Ce constructeur est appel�e lorsqu'une classe d�riv�e est instanci�e via un loader.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Object::Object(const PAK::Loader& pLoader)
        : mFileHdl(pLoader.fileHdl())
        , mSaveActivated(false)
    {
        ObjectMgr::get()->checkIn(shared_from_this());
    }


    /*! Destructeur.
     */
    Object::~Object()
    {
        if (mFileHdl != PAK::eBadHdl)
        {
            ObjectMgr::get()->checkOut(shared_from_this());
        }
    }


    /*! Ce constructeur est appel� par Resource avec pour param�tre template une classe d�riv�e de
        celle-ci.
        @param pFileHdl Handle de fichier associ� � l'objet
     */
    Object::Object(PAK::FileHdl pFileHdl)
        : mFileHdl(pFileHdl)
        , mSaveActivated(false)
    {
        ObjectMgr::get()->checkIn(shared_from_this());
    }


//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Cette m�thode doit �tre appel�e dans le destructeur des classes dont sauvegarder le contenu.
     */
    void Object::saveSelf() const
    {
        if (mSaveActivated)
        {
            PAK::FileHdl    lHdl    = fileHdl();
            if (lHdl != PAK::eBadHdl)
            {
                PAK::Saver lSaver(lHdl);
                store(lSaver);
            }
        }
    }


    /*! Cette m�thode doit �tre r�impl�ment�e dans les classes d�riv�es afin de permettre la sauvegarde
        des donn�es des instances.<br>
        Le format des donn�es est identique � celui du constructeur correspondant au chargement depuis un
        fichier.
        @param pSaver Les donn�es de l'objet doivent �tre �crites dans ce "fichier"
        @note J'aurais pr�f�r� ne d�finir une telle m�thode qu'en mode �diteur, mais il m'a tout de m�me
        paru plus coh�rent de d�finir le format des donn�es d'un type (lecture/�criture) dans une seule
        classe
        @warning <b>Important</b> : Dans les r�impl�mentations de cette m�thode, commencez toujours par
        appeler la version de la classe m�re
     */
    void Object::store(PAK::Saver& pSaver) const
    {
        // NB: le handle n'est pas mutable, mais cette m�thode doit rester constante (et puis je n'avais
        // pas r�fl�chi au probl�me...).
        const_cast<Object*>(this)->setFileHdl(pSaver.fileHdl());
    }


//------------------------------------------------------------------------------
//                        Handle des Fichiers de Donn�es
//------------------------------------------------------------------------------

    /*! D�finition du handle de fichier associ�.
     */
    void Object::setFileHdl(PAK::FileHdl pFileHdl)
    {
        if (fileHdl() != pFileHdl)
        {
            // Radie l'objet avant de le modifier.
            ObjectMgr::get()->checkOut(shared_from_this());

            mFileHdl = pFileHdl;

            // Le nouveau handle a �t� �tabli, l'objet peut �tre r�enregistr�.
            ObjectMgr::get()->checkIn(shared_from_this());
        }
    }
}
