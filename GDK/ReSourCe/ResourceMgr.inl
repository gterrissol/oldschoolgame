/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/ReSourCe/ResourceMgr.inl
    @brief M�thodes inline de la classe RSC::ResourceMgr.
    @author @ref Guillaume_Terrissol
    @date 14 Ao�t 2002 - 13 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RSC
{
//------------------------------------------------------------------------------
//        Acc�s aux M�thodes non-publiques des ressources pour DataPtr
//------------------------------------------------------------------------------

    /*! Seuls les membres du gestionnaires peuvent modifier le handle de fichier d'une ressource.
        @param pRes     Ressource � laquelle il faut associer un handle de fichier
        @param pFileHdl Handle de fichier � associer � <i>pRes</i>
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::setResourceFileHdl(TResPtr pRes, PAK::FileHdl pFileHdl)
    {
        pRes->setFileHdl(pFileHdl);
    }


    /*! Seuls les membres du gestionnaires peuvent modifier la clef d'une ressource.
        @param pRes Ressource qu'il faut lier � un clef
        @param pKey Clef � lier avec <i>pRes</i>
     */
    template<class TRes>
    inline void ResourceMgr<TRes>::setResourceKey(TResPtr pRes, const TKey& pKey)
    {
        pRes->setKey(pKey);
    }
}
