/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_BASERESOURCE_HH
#define RSC_BASERESOURCE_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/BaseResource.hh
    @brief En-t�te de la classe RSC::Resource.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/Handle.hh"
#include "STL/SharedPtr.hh"

#include "Handle.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                   Ressource
//------------------------------------------------------------------------------

    /*! @brief Ressource (objet charg� � la vol�e).
        @version 1.0
        @ingroup ReSourCe

        La plupart des donn�es (essentiellement graphiques : textures, mod�les 3D,...) sont assez
        volumineuses. Attendre qu'elles soient charg�es avant de poursuivre l'ex�cution du programme
        serait trop p�nalisant. D'un autre c�t�, il est beaucoup plus simple de ne g�rer que des objets
        initialis�s et exploitables plut�t que des instances en cours de chargement (et le syst�me de
        communication � mettre en place pour indiquer la disponibilit� (enfin) de la donn�e).<br>
        Cette classe offre donc des m�canismes permttant de faciliter le chargement des donn�es en t�che
        de fond :
        - Notion de ressource par d�faut : instance valide construite ex nihilo
        - Chargement (�ventuellement par �tape) des donn�es
        - Livraison des donn�es charg�es
        Consultez la page @ref RSC_Resources_Page pour un exemple complet d'utilisation.
        @sa RSC::ResourceMgr
     */
    template<class TObj>
    class Resource : public TObj
    {
        //! @name Classes amies
        //@{
        template<class TRes>
 friend class ResourceMgr;                                      //!< Le gestionnaire conna�t sa ressource.
        //@}
    public:
        //! @name Contructeur & destructeur
        //@{
                            Resource();                         //!< Constructeur (pour une ressource) par d�faut.
                explicit    Resource(PAK::FileHdl pFileHdl);    //!< Constructeur.
virtual                     ~Resource();                        //!< Destructeur.
        //@}
        //! @name Alias
        //@{
        typedef TObj                        TObjectType;        //!< Type d'objet g�r� par la ressource.
        typedef ResourceMgr<Resource<TObj>> TMgr;               //!< Type du manager de la ressource.
        //@}
        //! @name Gestion du fichier de la ressource & de la clef
        //@{
        typedef Handle<Resource<TObj>>      TKey;               //!< Type de la clef pour l'acc�s � la ressource.
        inline  TKey        key() const;                        //!< Clef de la ressource.

    protected:

        inline  void        setKey(TKey pKey);                  //!< Assigne sa clef � la ressource.
        //@}
        //! @name Cr�ation de la ressource
        //@{
                void        load();                             //!< Chargement des donn�es.
                void        deliver();                          //!< Livraison des donn�es.


    private:

virtual         void        afterDeliver();                     //!< Travail � effectuer apr�s la livraison de la ressource.
        //@}
        //! @name Attributs
        //@{
        UniquePtr<TObj> mDynamicallyLoadedInstance;             //!< Instance charg�e dynamiquement.
        TKey            mKey;                                   //!< Clef de la ressource.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "BaseResource.inl"

#endif  // De RSC_BASERESOURCE_HH
