/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_RESOURCE_HH
#define RSC_RESOURCE_HH

/*! @file GDK/ReSourCe/ReSourCe.hh
    @brief Pr�-d�clarations du module @ref ReSourCe.
    @author @ref Guillaume_Terrissol
    @date 15 Mars 2008 - 18 Ao�t 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RSC
{
    class HasHandle;
    class LoadingThread;
    class Object;
    class ObjectMgr;

    template<typename TTag>             class Handle;
    template<class TData, class THdl>   class HandleMgr;
    template<class TObj, class TRes>    class Local;
    template<class TObj>                class Local<TObj, TObj>;    // Sp�cialisation.
    template<class TObj>                class Resource;
    template<class TRes>                class ResourceHelper;
    template<class TRes>                class ResourceMgr;
}

#endif  // De RSC_RESOURCE_HH
