/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ObjectMgr.hh"

/*! @file GDK/ReSourCe/ObjectMgr.cc
    @brief M�thodes (non-inline) des classes RSC::Object & RSC::ObjectMgr.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2003 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "PAcKage/ErrMsg.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "Object.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RSC::ObjectMgr.
        @version 1.0
     */
    class ObjectMgr::Private : public MEM::OnHeap
    {
    public:
        Private(U32 pMaxObjectCount);           //!< Constructeur.

        //! @name Ensemble d'objets
        //@{
        Vector<Vector<ObjectPtr>>   mInstances; //!< Objets allou�s.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    ObjectMgr::Private::Private(U32 pMaxObjectCount)
        : mInstances(pMaxObjectCount, Vector<ObjectPtr>())
    { }


//------------------------------------------------------------------------------
//                              Op�rateur d'Egalit�
//------------------------------------------------------------------------------

    /*! Compare deux weak pointers.
        @param pL Premier pointeur � comparer
        @param pR Second pointeur � comparer
        @return VRAI si les pA et pB sont �quivalents (i.e. partagent un m�me pointeur r�f�rence ou sont tous deux vides)
     */
    bool operator==(const ObjectMgr::ObjectPtr& pL, const ObjectMgr::ObjectPtr& pR)
    {
        return !std::owner_less<ObjectMgr::ObjectPtr>()(pL, pR) && !std::owner_less<ObjectMgr::ObjectPtr>()(pR, pL);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    ObjectMgr::ObjectMgr(U32 pMaxObjectCount)
        : pthis(pMaxObjectCount)
    { }


    /*! Destructeur.
     */
    ObjectMgr::~ObjectMgr() { }


//------------------------------------------------------------------------------
//                           Enregistrement des Objets
//------------------------------------------------------------------------------

    /*! Enregistrement d'un objet.
     */
    void ObjectMgr::checkIn(ObjectPtr pInstance)
    {
        U16 lIndex  = pInstance.lock()->fileHdl().value();

        if (lIndex < pthis->mInstances.size())
        {
            pthis->mInstances[lIndex].push_back(pInstance);
        }
        // Sinon, pas d'enregistrement.
    }


    /*!  Radiation d'un objet.
     */
    void ObjectMgr::checkOut(ObjectPtr pInstance)
    {
        U16 lIndex  = pInstance.lock()->fileHdl().value();

        if (lIndex < pthis->mInstances.size())
        {
            // Une r�f�rence sur le vecteur des instances candidates au retraits est cr��e, pour
            // optimiser un peu le code.
            Vector<ObjectPtr>&  lCandidates = pthis->mInstances[lIndex];
            auto                lIToRemove  = std::find(lCandidates.begin(), lCandidates.end(), pInstance);

            // Si l'instance n'a pas �t� trouv�e, une erreur a d� survenir en amont.
            if (lIToRemove != lCandidates.end())
            {
                // Index, dans rVpo_Candidates, de l'instance a supprimer.
                U32 lToRemove = U32(lIToRemove - lCandidates.begin());

                // Petite optimisation : pas de permutation si l'instance est d�j� au sommet du vecteur.
                if (lToRemove != lCandidates.size() - 1)
                {
                    std::swap(lCandidates[lToRemove], lCandidates.back());
                }

                // On d�pile : l'instance pInstance n'est plus r�f�renc�e.
                lCandidates.pop_back();
            }
        }
        // Sinon, rien.
    }



//------------------------------------------------------------------------------
//                               Acc�s aux objets
//------------------------------------------------------------------------------

    /*! Objets cr��s � partir d'un fichier donn�.
        @param pFileHdl Handle du fichier dont les donn�es ont permis la cr�ation des objets recherch�s
        @return La liste des objet construits � partir des donn�es du fichier <i>pFileHdl</i>
     */
    Vector<ObjectMgr::ObjectPtr> ObjectMgr::instances(PAK::FileHdl pFileHdl)
    {
        U16 lIndex  = pFileHdl.value();

        if (lIndex < pthis->mInstances.size())
        {
            return pthis->mInstances[lIndex];
        }
        else
        {
            LAUNCH_EXCEPTION(PAK::kInvalidFileHandle)
        }
    }


    /*! Clones d'un objet.
        @param pObject Objet dont on cherche les clones (i.e. les objets cr��s � partir des m�mes donn�es)
        @return Tous les objets cr��s � partir du m�me fichier que <i>pObject</i> (y compris ce dernier)
     */
    Vector<ObjectMgr::ObjectPtr> ObjectMgr::clones(ConstObjectPtr pObject)
    {
        PAK::FileHdl    lFileHdl    = pObject.lock()->fileHdl();

        return instances(lFileHdl);
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  ObjectMgr*  MEM::Singleton<ObjectMgr>::smThat   = nullptr;
#endif
}
