/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/ReSourCe/Object.inl
    @brief M�thodes inline de la classe RSC::Object.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2003 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RSC
{
//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Permet d'activer la sauvegarde de l'objet avant sa destruction.
        @param pOn VRAI pour activer la sauvegarde, FAUX pour la d�sactiver (�tat par d�faut)
     */
    inline void Object::activateSaveSelf(bool pOn)
    {
        mSaveActivated = pOn;
    }


//------------------------------------------------------------------------------
//                    Object : Handle des Fichiers de Donn�es
//------------------------------------------------------------------------------

    /*! @return Le handle de fichier associ�
     */
    inline PAK::FileHdl Object::fileHdl() const
    {
        return mFileHdl;
    }


    /*! Cette m�thode permet � une instance d'une classe d�riv�e de la hi�rarchie de conna�tre le handle
        de fichier associ� � une instance d'une autre classe de la hi�rachie de Object.
        @param pObject Objet duquel r�cup�rer le handle de fichier associ�
        @return Le handle de fichier associ� � l'instance <i>pObject</i>
     */
    inline PAK::FileHdl Object::objectFileHdl(const Object* pObject)
    {
        return pObject->fileHdl();
    }


//------------------------------------------------------------------------------
//                             Has Handle : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pFileHdl Handle de fichier dont l'instance propri�taire est recherch�e
     */
    inline HasHandle::HasHandle(PAK::FileHdl pFileHdl)
        : mFileHdlToFind(pFileHdl)
    { }


    /*! Op�rateur fonction (pour faire de cette classe un pr�dicat).
        @param pObject Objet dont on cherche � savoir s'il � pour handle de fichier la valeur fournie
        au constructeur de cette instance
        @return VRAI si <i>pObject</i> a pour handle de fichier la valeur fournie au constructeur de
        cette instance, FAUX sinon
     */
    inline bool HasHandle::operator()(const RSC::Object& pObject) const
    {
        return (RSC::Object::objectFileHdl(&pObject) == mFileHdlToFind);
    }


    /*! Op�rateur fonction (pour faire de cette classe un pr�dicat).
        @param pObject Objet dont on cherche � savoir s'il � pour handle de fichier la valeur fournie
        au constructeur de cette instance
        @return VRAI si <i>pObject</i> a pour handle de fichier la valeur fournie au constructeur de
        cette instance, FAUX sinon
     */
    inline bool HasHandle::operator()(const RSC::Object* pObject) const
    {
        return (RSC::Object::objectFileHdl(pObject) == mFileHdlToFind);
    }


    /*! Op�rateur fonction (pour faire de cette classe un pr�dicat).
        @param pObject Objet dont on cherche � savoir s'il � pour handle de fichier la valeur fournie
        au constructeur de cette instance
        @return VRAI si <i>pObject</i> a pour handle de fichier la valeur fournie au constructeur de
        cette instance, FAUX sinon
     */
    template<class TObj>
    inline bool HasHandle::operator()(const SharedPtr<TObj>& pObject) const
    {
        return (RSC::Object::objectFileHdl(pObject.get()) == mFileHdlToFind);
    }
}
