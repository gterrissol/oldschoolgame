/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_RESOURCEMGR_HH
#define RSC_RESOURCEMGR_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/ResourceMgr.hh
    @brief En-t�te de la classe RSC::ResourceMgr.
    @author @ref Guillaume_Terrissol
    @date 14 Ao�t 2002 - 28 D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "THRead/Mutex.hh"
#include "THRead/BaseThread.hh"
#include "STL/STL.hh"

#include "BaseResource.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                          Gestionnaire de Ressources
//------------------------------------------------------------------------------

    /*! @brief Gestionnaire de ressources.
        @version 0.99

        Un gestionnaire de ressources propose une interface capable de fournir les ressources demand�es
        (apr�s les avoir charg�es, si besoin est). Les ressources sont identifi�es de mani�re unique par
        une clef (une instanciation de HDL::GenericHdl, par le type de donn�e � g�rer : la ressource).
        @note Le param�tre template, <i>TRes</i>, doit �tre une instance d'une classe d�riv�e d'une
        sp�cialisation de RSC::Resource
        @note Les m�thodes registerLoad & registerDeliver ne doivent �tre acc�d�es que par les
        ressources (ou le gestionnaire lui-m�me) : il est, en effet, hors de question que
        l'enregistrement de callbacks de chargement ou de livraison puisse �tre demand� par n'importe
        qui. Ces m�thodes sont donc priv�es, et peuvent �tre utilis�es par les ressources via une
        instance de ResourceHelper (qui effectue un contr�le sur l'"appelant")

        Consultez �galement la page @ref RSC_Resources_Page
     */
    template<class TRes>
    class ResourceMgr : public MEM::Singleton<ResourceMgr<TRes>>
    {
        //! @name Classe amie
        //@{
 friend class ResourceHelper<TRes>;                                                         //!< Acc�s � Load et AfterDeliver.
        //@}
    public:
        //! @name Alias
        //@{
        using   TRsc = TRes;                                                                //!< Alias sur le type de la ressource.
        using   TKey = typename TRes::TKey;                                                 //!< Clef pour l'acc�s � une ressource.
        //@}
        //! @name Types de pointeur
        //@{
        using   Ptr     = SharedPtr<ResourceMgr<TRes>>;                                     //!< Pointeur sur gestionnaire.
        using   TResPtr = SharedPtr<TRes>;                                                  //!< Pointeur sur ressource.
        //@}
        //! @name Contructeur & destructeur
        //@{
                                ResourceMgr();                                              //!< Constructeur.
                                ~ResourceMgr();                                             //!< Destructeur.
                bool            clear();                                                    //!< Suppression de toutes les ressources.
        //@}
        //! @name Sauvegarde & restauration des ressources
        //@{
                void            save() const;                                               //!< Sauvegarde des ressources.
                void            load(PAK::Loader& pLoader);                                 //!< Chargement des ressources.
                PAK::FileHdl    fileHdl() const;                                            //!< Handle du fichier des donn�es.
        //@}
        //! @name Gestion des ressources
        //@{
                TKey            insert(PAK::FileHdl pFileHdl);                              //!< Cr�e une nouvelle ressource.
                TResPtr         retrieve(const TKey& pKey, bool pNow = false);              //!< Acquiert une ressource.
                void            release(TResPtr& pResource);                                //!< Lib�re une ressource.
                bool            remove(const TKey& pKey);                                   //!< Supprime une ressource.

                void            deliverResources();                                         //!< Livraisons des ressources charg�es.
                TKey            key(PAK::FileHdl pFileHdl) const;                           //!< Clef d'une ressource.
                TResPtr         defaulted();                                                //!< Ressource par d�faut.
        //@}

    private:
        //! @name Chargement & validation d'une ressource
        //@{
                void            registerLoad(TResPtr pRes, void(TRes::*pMember)());         //!< Enregistre une callback de chargement pour une ressource.
                void            registerDeliver(TResPtr pRes, void(TRes::*pMember)());      //!< Enregistre une callback de finalisation pour une ressource.
        //@}
        //! @name Acc�s aux m�thodes non-publiques des ressources pour DataPtr
        //@{
 static inline  void            setResourceFileHdl(TResPtr pRes, PAK::FileHdl pFileHdl);    //!< Association avec un handle de fichier.
 static inline  void            setResourceKey(TResPtr pRes, const TKey& pKey);             //!< Liaison avec une clef.
        //@}
        FORBID_COPY(ResourceMgr)
        PIMPL()
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "ResourceMgr.inl"

#endif  // De RSC_RESOURCEMGR_HH
