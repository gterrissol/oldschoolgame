/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_RESOURCE_HH
#define RSC_RESOURCE_HH

/*! @file GDK/ReSourCe/All.hh
    @brief Interface publique du module @ref ReSourCe.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RSC   //! Gestion des ressources.
{
    /*! @namespace RSC
        @version 0.98

        Dans ce moteur, la plupart des objets sont charg�s dynamiquement (� la vol�e). Le terme ressource
        d�signe de tels objets. Ce module fournit quelques classes permettant de d�finir et de manipuler
        les ressources. Consultez la page @ref RSC_Resources_Page pour une explication d�taill�e,
        illustr�e par 2 exemples concrets (et complets).
     */

    /*! @defgroup ReSourCe ReSourCe : Gestion des ressources
        <b>namespace</b> RSC et @ref RSC_Handles.
     */

    /*! @defgroup RSC_Handles Gestion de handles de ressource
        @ingroup ReSourCe
     */
}

#include "Object.hh"
#include "ObjectMgr.hh"
#include "BaseResource.hh"
#include "ResourceMgr.hh"
#include "ResourceMgr.tcc"

#endif  // De RSC_RESOURCE_HH
