/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RSC_OBJECTMGR_HH
#define RSC_OBJECTMGR_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/ObjectMgr.hh
    @brief En-t�te de la classe RSC::ObjectMgr.
    @author @ref Guillaume_Terrissol
    @date 13 Avril 2003 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "PAcKage/Handle.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                             Gestionnaire d'Objets
//------------------------------------------------------------------------------

    /*! @brief Gestionnaire d'objets
        @version 0.5
        @ingroup ReSourCe

        Tous les objets charg�s � partir de fichiers sont r�f�renc�s par ce gestionnaire.
     */
    class ObjectMgr : public MEM::Singleton<ObjectMgr>, public MEM::OnHeap
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef WeakPtr<RSC::Object const>  ConstObjectPtr;     //!< Pointeur (constant) sur objet ressource.
        typedef WeakPtr<RSC::Object>        ObjectPtr;          //!< Pointeur sur objet ressource.
        //@}
        //! @name Contructeur & destructeur.
        //@{
                            ObjectMgr(U32 pMaxObjectCount);     //!< Constructeur.
                            ~ObjectMgr();                       //!< Destructeur.
        //@}
        //! @name Enregistrement des objets
        //@{
        void                checkIn(ObjectPtr pInstance);       //!< Enregistrement d'un objet.
        void                checkOut(ObjectPtr pInstance);      //!< Radiation d'un objet.
        //@}
        //! @name Acc�s aux objets
        //@{
        Vector<ObjectPtr>   instances(PAK::FileHdl pFileHdl);   //!< Objets cr��s � partir d'un fichier donn�.
        Vector<ObjectPtr>   clones(ConstObjectPtr pObject);     //!< Clones d'un objet.
        //@}

    private:

        FORBID_COPY(ObjectMgr)
        PIMPL()
    };
}

#endif  // De RSC_OBJECTMGR_HH
