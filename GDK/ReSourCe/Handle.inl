/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

/*! @file GDK/ReSourCe/Handle.inl
    @brief M�thodes inline de la classe HDL::Handle.
    @author Code original : Scott Bilas    
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 17 Septembre 2011
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

#include "ErrMsg.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<typename TTag>
    inline Handle<TTag>::Handle()
        : mValue(0)
    { }


    /*! @param pIndex Valeur d'initialisation du handle
     */
    template<typename TTag>
    inline void Handle<TTag>::init(U32 pIndex)
    {
        ASSERT_EX(isNull(), kHandleAlreadyInitialized, return;)
        ASSERT_EX(pIndex <= eMaxIndex, kIndexOutOfRange, return;)

        if (eMaxMagic < ++smAutoMagic)
        {
            smAutoMagic = U32(1);   // Ne r�initialise pas � 0 (qui est utilis� pour le handle nul).
        }

        m.Index = pIndex;
        m.Magic = smAutoMagic;
    }


//------------------------------------------------------------------------------
//                               M�thodes d'Acc�s
//------------------------------------------------------------------------------

    /*! @return L'index du handle
     */
    template<typename TTag>
    U32 Handle<TTag>::getIndex() const
    {
        return U32(m.Index);
    }


    /*! @return Le nombre magique du handle
     */
    template<typename TTag>
    inline U32 Handle<TTag>::getMagic() const
    {
        return U32(m.Magic);
    }


    /*! @return La valeur du handle
     */
    template<typename TTag>
    inline I32 Handle<TTag>::value() const
    {
        return I32(mValue);
    }


    /*! @return VRAI si le handle est nul (non-initialis�), FAUX sinon
     */
    template<typename TTag>
    inline bool Handle<TTag>::isNull() const
    {
        return (mValue == 0);
    }
    

    /*! Convertit le handle en entier 32 bits (sign�).
        @return La valeur (num�rique) du handle
     */
    template<typename TTag>
    inline Handle<TTag>::operator I32() const
    {
        return I32(mValue);
    }


//------------------------------------------------------------------------------
//                           Op�rateurs de comparaison
//------------------------------------------------------------------------------

    /*! @return VRAI si les handles identifient la m�me ressource, FAUX sinon
     */
    template<typename TTag>
    inline bool operator==(const Handle<TTag>& pL, const Handle<TTag>& pR)
    {
        return (pL.value() == pR.value());
    }

    /*! @return VRAI si les handles identifient des ressource distinctes, FAUX sinon
     */
    template<typename TTag>
    inline bool operator!=(const Handle<TTag>& pL, const Handle<TTag>& pR)
    {
        return (pL.value() != pR.value());
    }


//------------------------------------------------------------------------------
//                                Nombre Magique
//------------------------------------------------------------------------------

#ifndef NOT_FOR_DOXYGEN
    template<typename TTag> U32 Handle<TTag>::smAutoMagic   = k0UL;
#endif  // De NOT_FOR_DOXYGEN
}
