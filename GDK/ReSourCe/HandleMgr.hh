/* Copyright (C) Scott Bilas, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

#ifndef RSC_HANDLEMGR_HH
#define RSC_HANDLEMGR_HH

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/HandleMgr.hh
    @brief En-t�te de la classe RSC::HandleMgr.
    @author Code original : Scott Bilas    
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 13 Juillet 2001 - 30 Juin 2013
    @note "Portions Copyright (C) Scott Bilas, 2000"
 */

#include "MEMory/Class.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                            Gestionnaire de Handles
//------------------------------------------------------------------------------

    /*! @brief Gestionnaire de handles.
        @version 1.5
        @ingroup RSC_Handles

        Ce gestionnaire permet de convertir des handles en <i>TData*</i> et vice-versa, en contr�lant la
        validit� des handles et en minimisant les ressources m�moires.
        Le type de ressource � g�rer (<i>TData</i>) doit poss�der un constructeur par d�faut.
     */
    template<class TData, class THdl>
    class HandleMgr : public MEM::OnHeap
    {
    public:
        //! @name Contructeur & destructeur
        //@{
                        HandleMgr();                            //!< Constructeur.
                        ~HandleMgr();                           //!< Destructeur.
        //@}
        //! @name Sauvegarde & restauration
        //@{
        void            save(PAK::Saver& pSaver) const;         //!< Sauvegarde des handles.
        void            load(PAK::Loader& pLoader);             //!< Chargement des handles.
        //@}
        //! @name Acquisition
        //@{
        TData*          acquire(THdl& pHdl);                    //!< Acquisition d'une ressource.
        void            release(const THdl& pHdl);              //!< Lib�ration d'une ressource.
        //@}
        //! @name D�r�f�rencement
        //@{
        TData*          dereference(const THdl& pHdl);          //!< D�r�f�rencement.
        const TData*    dereference(const THdl& pHdl) const;    //!< D�r�f�rencement.
        //@}
        //! @name Requ�tes
        //@{
        U32             getUsedHandleCount() const;             //!< Nombre de handles utilis�s.
        bool            hasUsedHandles() const;                 //!< Des handles sont-ils utilis�s ?
        //@}
        //! @name Parcours des ressources
        //@{
        const TData*    getFirst() const;                       //!< Acc�s � la premi�re ressource acquise.
        const TData*    getNext(const TData* pPrevious) const;  //!< Acc�s � la ressource suivante.
        //@}

    private:

        FORBID_COPY(HandleMgr)
        PIMPL()
    };
}

#endif  // De RSC_HANDLEMGR_HH

