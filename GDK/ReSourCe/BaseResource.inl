/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/ReSourCe/BaseResource.inl
    @brief M�thodes inline de la classe RSC::Resource.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ResourceMgr.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                            Resource : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.<br>
        La classe <i>TObj</i> doit fournir un constructeur par d�faut (qui sera appel�) : celui-ci doit
        construire une instance valide.
     */
    template<class TObj>
    inline Resource<TObj>::Resource()
        : TObj()
        , mDynamicallyLoadedInstance(nullptr)
    { }


    /*! Constructeur � partir d'un fichier. Les donn�es de ce fichier seront utilis�es pour construire
        l'instance (dans un thread).
        @param pFileHdl Handle du fichier dans lequel lire les donn�es de la ressource
     */
    template<class TObj>
    inline Resource<TObj>::Resource(PAK::FileHdl pFileHdl)
        : TObj(pFileHdl)
        , mDynamicallyLoadedInstance(nullptr)
    { }


    /*! Destructeur.
     */
    template<class TObj> inline Resource<TObj>::~Resource() { }


//------------------------------------------------------------------------------
//                         Resource : Gestion de la clef
//------------------------------------------------------------------------------

    /*! @return La clef de la ressource.
        @sa SetKey
     */
    template<class TObj>
    inline typename Resource<TObj>::TKey Resource<TObj>::key() const
    {
        return mKey;
    }


    /*! Chaque ressource a besoin de conna�tre sa clef afin de pouvoir demander � son gestionnaire de la
        lib�rer (i.e comptabiliser une r�f�rence de moins).
        @param pKey Clef de la ressource
     */
    template<class TObj>
    inline void Resource<TObj>::setKey(TKey pKey)
    {
        mKey = pKey;
    }


//------------------------------------------------------------------------------
//                      Resource : Cr�ation de la ressource
//------------------------------------------------------------------------------

    /*! Les donn�es charg�es dynamiquement sont stock�es dans des variables temporaires : les donn�es
        accessibles de la ressource sont celles par d�faut, ou des donn�es partiellement charg�es. Le
        r�le de cette m�thode est de rendre disponibles les donn�es qui viennent d'�tre charg�es.
        @sa Load
        @note Cette m�thode est appel�e dans le thread principal
     */
    template<class TObj>
    void Resource<TObj>::deliver()
    {
        // Echange des donn�es avec la resssource charg�e.
        this->swap(*mDynamicallyLoadedInstance);

        // Cette instance n'est plus utile.
        mDynamicallyLoadedInstance.reset();

        afterDeliver();
    }


    /*! Lorsqu'une ressource a enfin �t� charg�e, elle peut n�cessiter une mise � jour en fonction
        d'autres informations (exemple typique : une tuile (TIL::Tile) poss�de un �tat initial, stock�
        dans le <b>pak</b> file, mais cet �tat a pu changer depuis le d�but de la partie du joueur -
        objet d�plac�, d�formation du terrain (si on l'impl�mente), etc - il faut donc en tenir compte.
        R�cup�rer ces informations de mise � jour � partir du constructeur poserait des probl�mes de
        synchronisation.<br>
        Cette m�thode, � r�impl�menter au besoin, est appel�e apr�s la livraison de la ressource, par le
        thread principal. Elle permet d'effectuer un travail quelconque (dont la mise � jour �voqu� plus
        haut), sur une ressource compl�tement d�finie.
        @note J'insiste bien sur le fait que l'instance sur laquelle cette m�thode est appel�e est
        l'objet charg�, une fois qu'il a remplac� l'instance temporaire (valide, mais par d�faut) cr��e
        pr�c�demment
     */
    template<class TObj>
    void Resource<TObj>::afterDeliver() { }
}
