/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ReSourCe.hh"

/*! @file GDK/ReSourCe/ReSourCe.cc
    @brief D�finitions diverses du module RSC.
    @author @ref Guillaume_Terrissol
    @date 19 Juillet 2002 - 27 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

#include "Thread.hh"

namespace RSC
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAFewHandlesAreStillUsed,  "Quelques handles sont encore utilis�s.",             "A few handles are still used.")
    REGISTER_ERR_MSG(kAlreadyAllocatedObject,   "Objet d�j� allou�.",                                 "Already allocated object")
    REGISTER_ERR_MSG(kAlreadyDefinedData,       "Des donn�es sont d�j� pr�sentes.",                   "Already defined data.")
    REGISTER_ERR_MSG(kHandleAlreadyInitialized, "Handle d�j� initialis�.",                            "Handle already initialized")
    REGISTER_ERR_MSG(kIndexOutOfRange,          "Iindice en dehors de son intervalle de d�finition.", "Index out of range.")
    REGISTER_ERR_MSG(kInvalidHandle,            "Handle invalide.",                                   "Invalid handle.")


//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Thread::~Thread() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  Thread* MEM::Singleton<Thread>::smThat  = nullptr;
#endif


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var RSC::Handle<TTag>::Index
        Identifieur unique pour un d�r�f�rencement rapide dans le gestionnaire de handles.
     */
    /*! @var RSC::Handle<TTag>::Magic
        Nombre magique qui peut �tre utilis� pour v�rifier la validit� d'un handle.
     */
    /*! @var RSC::Handle<TTag>::mValue
        Le handle encapsule l'index et le nombre magique en un entier 32 bits.
     */
    /*! @var RSC::Handle<TTag>::smAutoMagic
        Cette variable, attribut de classe, est incr�ment�e � la cr�ation de toute nouvelle instance.
     */
    /*! @var RSC::HandleMgr<TData, THdl>::Private::mUserData
        @todo A �crire
     */
    /*! @var RSC::HandleMgr<TData, THdl>::Private::mMagicNumbers
        @todo A �crire
     */
    /*! @var RSC::HandleMgr<TData, THdl>::Private::mFreeSlots
        @todo A �crire
     */
    /*! @var RSC::ResourceMgr::Private::mHandleMgr
        M�morise les paires (clef - pointeur sur ressource). Les m�canismes d'allocation/lib�ration et
        d�r�f�rencement de ressources s'effectuent via ce gestionnaire de handles.
     */
    /*! @var RSC::ResourceMgr::Private::mKeys
        M�morise les paires (handle de fichier - clef). M�canisme de base : enregistrement d'un handle de
        fichier aupr�s du gestionnaire qui cr�e une ressource et retourne sa clef; la clef permet de
        retrouver la ressource; la ressource sait o� trouver ses donn�es gr�ce au handle de fichier.
     */
}

//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page RSC_Resources_Page Utilisation des ressources
        @section RSC_Resources_Page_Introduction Introduction
        Le moteur du jeu propose le chargement de donn�es en continu (streaming). Afin de parvenir � ce
        r�sultat, le m�canisme des ressources a �t� cr��. Un des objectifs principaux de ce m�canisme
        �tait la simplicit� d'utilisation, � la fois en tant que cr�ateur et en tant qu'utilsateur de
        ressources.<br>
        Pour l'utilisateur, une ressource est disponible et exploitable d�s sa cr�ation (m�me si elle n'a
        pas encore �t� charg�e). Ainsi, il n'y a pas besoin de contr�ler la validit� d'une ressource
        avant d'y acc�der : elle est valide, car allou�e. Pour le cr�ateur de ressources, le travail n'en
        est pas beaucoup plus compliqu� : l'essentiel du travail est r�alis� par les classes du module
        RSC.<br>
        Bien que les ressources soient charg�es dans un thread s�par�, il n'y a aucun besoin de mutex,
        puisque la seule op�ration n�cessitant une synchronisation est effectu�e en un point pr�cis de la
        boucle du moteur, o� un contr�le s'effectue (voir RSC::ResourceMgr::deliverResources()).

        @section RSC_Resources_Page_Principal Principe g�n�ral
        Pour donner � une classe un comportement de ressource, il faut :
        - d�river de RSC::Object
        - proposer 3 constructeurs : par d�faut, un avec comme param�tre PAK::Loader& (chargement
        imm�diat), et un dernier avec comme param�tre PAK::FileHdl (chargement diff�r�),
        - impl�menter une m�thode swap (qui fait ce qu'on attend d'elle, � savoir permuter tous les
        attributs avec ceux d'une autre instance),
        - d�clarer deux typedef (optionel),
        - d�finir une macro (d�sol�, mais si �a ne vous pla�t pas, ce n'est pas obligatoire) pour
        manipuler plus simplement le gestionnaire de ressources (optionel, donc), 
        - d�clarer une classe amie, dans certains cas.
        - ne pas oublier d'inclure "ReSourCe/ResourceMgr.tcc" dans le fichier source de l'objet ressource
        et d'instancier explicitement la classe ResourceMgr :
        @code
        namespace RSC
    {
        template class ResourceMgr<NS::MyResource>;
    }
        @endcode

        @section RSC_Resources_Page_Example Exemple concret
        Un exemple complet d'utilisation, comment� pas-�-pas.
        @subsection RSC_Resources_Page_Example_Interface Interface
        @code
namespace IMG
{
    // Image brute 8 bits.
    class Image : public RSC::Object
    {
        // ! @name Classe amie
        // @{
        template<class TObj>
 friend class RSC::Local;
        // @}
    public:
        // ! @name Constructeur & destructeur
        // @{
                Image(PAK::Loader& pLoader);        // !< Constructeur (chargement imm�diat).
virtual         ~Image();                           // !< Destructeur.
        // @}
        // ...

    protected:
        // ! @name Autres Constructeurs
        // @{
                Image();                            // !< Constructeur par d�faut.
                Image(PAK::FileHdl pFileHdl);       // !< Constructeur (chargement diff�r�).
        // @}
        // ! @name Gestion des donn�es
        // @{
        void    swap(Image& pImage);                // !< Permutation des donn�es.
virtual void    store(PAK::Saver& pSaver) const;    // !< Sauvegarde des donn�es.
        // @}
        // ...

    private:
        // Attributs...
    };

    typedef RSC::Resource<Image>                ImageRsc;
    typedef RSC::ResourceMgr<ImageRsc>::TKey    Key;

#define ImageMgr    RSC::ResourceMgr<MAT::ImageRsc>::get()
}
        @endcode
        @subsection RSC_Resources_Page_Example_Notes Notes
        @code
    class Image : public RSC::Object
        @endcode
        L'h�ritage multiple est autoris�, mais il est impossible de d�river deux fois de RSC::Object
        (pas d'h�ritage en diamant).
        @code
        template<class TObj>
 friend class RSC::Local;
        @endcode
        Si la ressource doit �tre utilis�e comme une r�f�rence (qu'il faudra animer, par exemple, avant
        de stocker le r�sultat dans une autre instance), il faut d�clarer comme amie la classe
        RSC::Local (voir @ref RSC_Resources_Page_Locals).
        @code
                Image(PAK::Loader& pLoader);
        @endcode
        Tout objet de type ressource peut �galement �tre construit imm�diatement : il suffit de lui
        fournir un fichier contenant ses donn�es.
        @code
virtual         ~Image();
        @endcode
        Ne pas oublier un destructeur virtuel...
        @code
                Image();
                Image(PAK::FileHdl pFileHdl);
        @endcode
        Le premier constructeur prot�g�, par d�faut, est appel� pour construire une instance par d�faut
        (justement : l'instance doit donc �tre valide et exploitable, comme une instance charg�e).<br>
        Le deuxi�me sera appel� par le gestionnaire de ressources pour construire une instance � partir
        d'une clef.
        @code
        void    swap(Image& pImage);
        @endcode
        Les donn�es de l'instance doivent pouvoir �tre �chang�es avec celles d'une autre (voir plus bas).
        <br>
        @code
virtual void    store(PAK::Saver& pSaver) const;
        @endcode
        Tout classe d�riv�e de RSC::Object doit, normalement, r�impl�menter cette m�thode, afin de
        sauvegarder ses propres attributs.
        @code
    typedef RSC::Resource<Image>                ImageRsc;
    typedef RSC::ResourceMgr<ImageRsc>::TKey    Key;
        @endcode
        Le type <i>ImageRsc</i> pourra �tre utilis� chaque fois qu'il sera indispensable de manipuler
        une ressource. Le reste du temps, utiliser un pointeur sur une <i>Image</i> sera suffisant
        (RSC::Resource d�rive de son param�tre template).<br>
        <i>Key</i> est un simple alias (justifi�, non ?).
        @code
#define ImageMgr    RSC::ResourceMgr<MAT::ImageRsc>::get()
        @endcode
        Ici aussi, un alias pour simplifier le code.
        @subsection RSC_Resources_Page_Example_Implementations Impl�mentation (et notes)
        @code
#include "..."
#include "ReSourCe/ResourceMgr.tcc"
        @endcode
        L'impl�mentation du gestionnaire de ressource n'est pas incluse dans les fichiers d'en-t�te (pour
        des raison d'optimisation des temps de compilation).
        @code
namespace IMG
{
    Image::Image(PAK::Loader& pLoader)
        : ... // Initialisation des attributs.
    {
        pLoader >> ...
    }
        @endcode
        Les donn�es de l'instance sont charg�es depuis le fichier (rien de bien compliqu�). Il est �
        noter qu'un objet ressource doit pouvoir sauvegarder ses donn�es (via une r�impl�mentation de
        RSC::Object::store(), voir plus bas); ce constructeur fait office de Load().
        @code
    Image::~Image()
    {
        // Grand nettoyage...
    }
        @endcode
        Comme n'importe quel destructeur...
        @code
    Image::Image()
        : ... // Initialisation par d�faut des attributs.
    {
        // ...
    }
        @endcode
        Rappel : le constructeur par d�faut doit produire un objet valide (ici, par exemple, une image,
        blanche, de 1x1 pixel).
        @code
    Image::Image(PAK::FileHdl pFileHdl)
        : RSC::Object(pFileHdl)
        , ... // Initialisation par d�faut des attributs.
    {
        // ...
    }
        @endcode
        Le passage de <i>pFileHdl</i> au constructeur de RSC::Object est le point le plus important.
        Pour le reste, initialiser les attributs de la m�me mani�re que dans le constructeur par d�faut
        conviendra parfaitement.
        @code
    void Image::swap(Image& pImage)
    {
        // Permutation des attributs...
    }
        @endcode
        L'instance sur laquelle est appel�e cette m�thode poss�de des valeurs par d�faut pour ses
        attributs, et <i>pImage</i> est l'instance charg�e dynamiquement. Leurs attributs doivent �tre
        simplement �chang�s (comme il devrait s'agir, la plupart du temps, de types int�gr�s, ou de
        pointeurs, ce ne devrait pas �tre tr�s lourd).
        @code
    void Image::store(PAK::Saver& pSaver) const
    {
        pSaver << ...
    }
        @endcode
        Pendant de Image(PAK::Loader&) : �criture des donn�es sur disque.
        @code
}   // Fin de IMG

namespace RSC
{
    template class ResourceMgr<NS::MyResource>;
}
        @endcode
        Ne pas oublier d'instancier explicitement le gestionnaire pour la ressource tout juste d�finie
        (il me semble qu'il s'agit l� du meilleur endroit pour proc�der � cette instanciation explicite -
        requise puisque, comme pr�cis� plus haut, l'impl�menation est disponible dans aucun en-t�te et ne
        pourra donc pas �tre instanci�e implicitement).

        @section RSC_Resources_Page_AfterDeliver Apr�s la livraison
        Certaines ressources, une fois charg�es, doivent synchroniser leurs donn�es avec l'�tat global du
        moteur. Pour ce faire, une m�thode peut �tre r�impl�ment�e : RSC::Resource<TObj>::afterDeliver().
        Il est garanti que cette m�thode sera appel�e sur une ressource, apr�s sa livraison, par le
        thread principal.<br>
        Bien entendu, n'importe quel autre travail peut �tre effectu� par cette m�thode.<br>
        Consultez sa documentation pour plus de pr�cisions.

        @section RSC_Resources_Page_Locals Copies locales
        A venir

        @section RSC_Resources_Page_Managers Gestionnaires de ressources
        A venir

        @section RSC_Resources_Page_Conclusion Conclusion
        J'ai essay� de rendre le syst�me de ressources le plus simple � utiliser, � codant dans RSC la
        plus grande partie du m�canisme.<br>
        Dans les faits, il ne reste � coder que le minimum :
        - 3 constructeurs (qu'il aurait fallu �crire de toute fa�on) et le destructeur,
        - store() (obligatoire, �galement),
        - swap() (impossible � automatiser simplement pour un cas g�n�ral - � moins de complexifier la
        d�finition des classes ressources),
        - afterDeliver() (� red�finir si n�cessaire),
        - quelques fioritures (alias facultatifs),
        - manipulation des gestionnaires (impossibles � automatiser. Pour l'instant).
    */
