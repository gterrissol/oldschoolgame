/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_RECT_HH
#define UTI_RECT_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/Rect.hh
    @brief En-t�te des classes UTI::PointT, UTI::SizeT & UTI::Rect.
    @author Guillaume_Terrissol
    @date 4 Avril 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace UTI
{
    /*! @brief Point 2D.
        @version 1.0
        @ingroup UTIlity

        Classe g�n�rique pour la repr�sentation d'un point 2D.
     */
    template<typename TT>
    struct PointT : MEM::Auto<PointT<TT>>
    {
    public:
        //! @name Constructeurs
        //@{
        inline  PointT();               //!< ... par d�faut.
        inline  PointT(TT pX, TT pY);   //!< ... � partir de coordonn�es.
        //@}
        //! @name Coordonn�es
        //@{
        TT  x;                          //!< Abscisse.
        TT  y;                          //!< Ordonn�e.
        //@}
    };


    /*! @brief "Taille" 2D.
        @version 1.0
        @ingroup UTIlity

        Classe g�n�rique pour exprimer la taille d'objets 2D (voir Rect).
     */
    template<typename TT>
    struct SizeT : MEM::Auto<SizeT<TT>>
    {
    public:
        //! @name Constructeurs
        //@}
        inline          SizeT();                //!< ... par d�faut.
        inline          SizeT(TT pW, TT pH);    //!< ... � partir de dimensions.
        //@}
        //! @name Informations
        //@{
        inline  bool    isNull() const;         //!< Taille nulle ?
        inline  bool    isValid() const;        //!< Taille valide ?
        //@}
        //! @name Dimensions
        //@{
        TT  width;                              //!< Largeur.
        TT  height;                             //!< Hauteur.
        //
    };


    /*! @name Points & tailles
        @ingroup UTIlity
     */
    //@{
    typedef PointT<I32>    Point;     //!< Point 2D en coordonn�es enti�res
    typedef PointT<F32>    Pointf;    //!< Point 2D en coordonn�es flottantes

    typedef SizeT<I32>     Size;      //!< Taille 2D de dimensions enti�res.
    typedef SizeT<F32>     Sizef;     //!< Taille 2D de dimensions flottantes.
    //@}

    /*! @defgroup UTI_Rectangle_Interface Rectangle
        @ingroup UTIlity
     */

    /*! @brief Rectangle.
        @version 0.82
        @ingroup UTI_Rectangle_Interface

        Un simple rectangle, d�finissant une zone dans le plan.<br>
        Les coordonn�es sont flottantes, le rectangle n'est donc pas discret (i.e. largeur == droite -
        gauche).<br>
        Les coordon�es hautes (fTop()) sont sup�rieures aux basses (fBottom()).
     */
    class Rect : public MEM::Auto<Rect>
    {
    public :
        //! @name Constructeurs
        //@{
                        Rect();                                     //!< ... par d�faut.
                        Rect(Pointf pPos, Sizef pSz);               //!< ... par composants.
                        Rect(F32 pL, F32 pT, F32 pR, F32 pB);       //!< ... par coordonn�es.
        //@}
        //! @name Etats du rectangle
        //@{
                bool    isEmpty() const;                            //!< Vide ?
                bool    isNull() const;                             //!< Nul ?
                bool    isValid() const;                            //!< Valide ?
        //@}
        //! @name Acc�s aux donn�es
        //@{
        inline  F32     left() const;                               //!< Abscisse gauche.
        inline  F32     top() const;                                //!< Ordonn�e sup�rieure.
        inline  F32     right() const;                              //!< Abscisse droite.
        inline  F32     bottom() const;                             //!< Ordonn�e inf�rieure.
                Pointf  topLeft() const;                            //!< Coin sup�rieur gauche.
                Pointf  bottomRight() const;                        //!< Coin inf�rieur droit.
                Pointf  topRight() const;                           //!< Coin sup�rieur droit.
                Pointf  bottomLeft() const;                         //!< Coin inf�rieur gauche.
                Pointf  center() const;                             //!< Centre du rectangle.
                F32     width() const;                              //!< Largeur.
                F32     height() const;                             //!< Hauteur.
                Sizef   size() const;                               //!< Taille.
        //@}
        //! @name Donn�es discr�tes
        //@{
                I32     roundedLeft() const;                        //!< Abscisse gauche.
                I32     roundedTop() const;                         //!< Ordonn�e sup�rieure.
                I32     roundedRight() const;                       //!< Abscisse droite.
                I32     roundedBottom() const;                      //!< Ordonn�e inf�rieure.
                U32     roundedWidth() const;                       //!< Largeur.
                U32     roundedHeight() const;                      //!< Hauteur.
        //@}
        //! @name Edition
        //@{
        inline  void    setLeft(F32 pL);                            //!< ... du c�t� gauche.
        inline  void    setTop(F32 pT);                             //!< ... du c�t� sup�rieur.
        inline  void    setRight(F32 pR);                           //!< ... du c�t� droit.
        inline  void    setBottom(F32 pB);                          //!< ... du c�t� inf�rieur.
                void    setTopLeft(Pointf pTL);                     //!< ... du coin sup�rieur gauche.
                void    setBottomRight(Pointf pBR);                 //!< ... du coin inf�rieur droit.
                void    setTopRight(Pointf pTR);                    //!< ... du coin sup�rieur droit.
                void    setBottomLeft(Pointf pBL);                  //!< ... du coin inf�rieur gauche.
                void    setRect(F32 pL, F32 pT, F32 pW, F32 pH);    //!< ... du rectangle complet.
                void    setWidth(F32 pW);                           //!< ... de la largeur.
                void    setHeight(F32 pH);                          //!< ... de la hauteur.
                void    setSize(Sizef pSz);                         //!< ... de la taille.
        //@}
        //! @name D�placements
        //@{
                void    moveLeft(F32 pOffset);                      //!< ... vers la gauche.
                void    moveTop(F32 pOffset);                       //!< ... vers le haut.
                void    moveRight(F32 pOffset);                     //!< ... vers la droite.
                void    moveBottom(F32 pOffset);                    //!< ... vers le bas.
        //@}

    private:
        //! @name Positions.
        //@{
        F32 mX1;                                                    //!< Abscisse gauche.
        F32 mY1;                                                    //!< Ordonn�e haute.
        F32 mX2;                                                    //!< Abscisse droite.
        F32 mY2;                                                    //!< Ordonn�e basse.
        //@}
    };


    /*! @defgroup UTI_Rectangle_Equality Operateurs d'(in)�galit�
        @ingroup UTI_Rectangle_Interface
     */
    //@{
    bool    operator==(const Rect& pL, const Rect& pR);             //!< Egalit�.
    bool    operator!=(const Rect& pL, const Rect& pR);             //!< In�galit�.
    //@}
    /*! @defgroup UTI_Rectangle_Operators Op�rations
        @ingroup UTI_Rectangle_Interface
     */
    //@{
    Rect    operator&(const Rect& pL, const Rect& pR);              //!< Intersection de deux rectangles.
    Rect    operator*(const Rect& pR, F32 pScale);                  //!< Etirement / r�tr�cissement.
    Rect    operator*(F32 pScale, const Rect& pR);                  //!< Etirement / r�tr�cissement.
    //@}
    /*! @defgroup UTI_Rectangle_Intersection Tests d'ntersection
        @ingroup UTI_Rectangle_Interface
     */
    //@{
    enum EIntersection
    {
        eInside = 0,
        eOverlaps,
        eOutside
    };
    bool            contains(const Rect& pRect, Pointf pPoint);     //!< ... avec un point.
    EIntersection   intersects(const Rect& pL, const Rect& pR);     //!< ... avec un autre rectangle.
    //@}


    LOG::Log&   operator<<(LOG::Log& pLog, const Rect& pRect);      //!< Ecriture d'un rectangle.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Rect.inl"

#endif  // De UTI_RECT_HH
