/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_TASK_TCC
#define UTI_TASK_TCC

#include "Task.hh"

/*! @file GDK/UTIlity/Task.tcc
    @brief M�thodes (non-inline) de la classe UTI::Task.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2008 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/SharedPtr.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                               Generic Callable
//------------------------------------------------------------------------------

    /*! @brief Objet sch�dul� g�n�rique.
        @version 1.0
        @internal
     */
    template<class TT>
    class Task::GenericCallable : public Task::Callable
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        typedef bool (TT::*Method)();                                   //!< Type de m�thode � appeler.
                GenericCallable(WeakPtr<TT>&& pObj, Method pMethod);    //!< Constructeur.
virtual         ~GenericCallable();                                     //!< Destructeur.
        //@}

    private:
        //! @name Impl�mentation
        //@{
virtual bool    validity() const;                                       //!< Validit�.
virtual bool    doProcess();                                            //!< Traitement.
        //@}
        //! @name Attributs
        //@{
        WeakPtr<TT> mThis;                                              //!< Objet � sch�duler.
        Method      mMethod;                                            //!< M�thode � appeler.
        //@}
    };


//------------------------------------------------------------------------------
//                 Generic Callable : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur
        @param pObj    Objet � sch�duler
        @param pMethod M�thode de pObj � appeler pour proc�der � la traitement
     */
    template<class TT>
    Task::GenericCallable<TT>::GenericCallable(WeakPtr<TT>&& pObj, Method pMethod)
        : mThis(pObj)
        , mMethod(pMethod)
    { }


    /*! Destructeur.
     */
    template<class TT>
    Task::GenericCallable<TT>::~GenericCallable() { }


//------------------------------------------------------------------------------
//                       Generic Callable : Impl�mentation
//------------------------------------------------------------------------------

    /*! @return VRAI si l'objet � sch�duler est toujours valide, FAUX sinon
     */
    template<class TT>
    bool Task::GenericCallable<TT>::validity() const
    {
        return !mThis.expired();
    }


    /*!
     */
    template<class TT>
    bool Task::GenericCallable<TT>::doProcess()
    {
        return ((*mThis.lock()).*mMethod)();
    }


//------------------------------------------------------------------------------
//                             Task : Enregistrement
//------------------------------------------------------------------------------

    /*! Enregistre un objet � sch�duler.
        @param pObject Objet � sch�duler
        @param pMethod M�thode � appeler p�riodiquement
     */
    template<class TT>
    void Task::checkIn(SharedPtr<TT>&& pObject, bool (TT::*pMethod)())
    {
        checkIn(STL::makeShared<GenericCallable<TT>>(pObject, pMethod));
    }
}

#endif  // De UTI_TASK_TCC
