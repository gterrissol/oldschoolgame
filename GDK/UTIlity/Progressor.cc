/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Progressor.hh"

/*! @file GDK/UTIlity/Progressor.cc
    @brief M�thodes (non-inline) de la classe UTI::Progressor.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "ERRor/Log.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Progressor::Progressor()
        : mLastStep(100)
        , mCurrent(0)
        , mStep(0.01F)
    {
        // Pour s'assurer que les valeurs sont bonnes...
        setStepCount(mLastStep);
    }


    /*! Destructeur.
     */
    Progressor::~Progressor() { }


//------------------------------------------------------------------------------
//                                  Avancement
//------------------------------------------------------------------------------

    /*! Permet de d�finir combien d'�tapes composent l'avancement.
        @param pCount Nombre d'�tapes
        @note Par d�faut, il y a 100 �tapes
        @warning Cette m�thode r�initialise le compteur d'�tapes
     */
    void Progressor::setStepCount(U32 pCount)
    {
        // On commence de 0.
        mLastStep = pCount;
        mStep     = F32(1.0F / mLastStep);
        setProgress(k0UL);
    }


    /*! Etablit l'�tape actuelle de l'avancement.
        @param pCurrentStep Etape courante
     */
    void Progressor::setProgress(U32 pCurrentStep)
    {
        mCurrent = std::min(pCurrentStep, mLastStep);
        progressChanged(currentPercentage());
    }


    /*! Passe � l'�tape suivante.
     */
    void Progressor::next()
    {
        if (mCurrent < mLastStep)
        {
            ++mCurrent;
            progressChanged(currentPercentage());
        }
    }


    /*! Affiche l'avancement sur la sortie standard.<br>
        Cette m�thode peut �tre r�impl�ment�e pour personnaliser le signalement de l'avancement de la
        t�che associ�e � cette instance.
     */
    void Progressor::progressChanged(F32 pNewProgress)
    {
        if ((0.0F <= pNewProgress) && (pNewProgress <= 100.0F))
        {
            LOG::Cout << 100.0F * pNewProgress << "%\n";
        }
    }
}
