/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_TASK_HH
#define UTI_TASK_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/Task.hh
    @brief En-t�te de la classe UTI::Task.
    @author @ref Guillaume_Terrissol
    @date 23 Avril 2008 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"

#include "STL/SharedPtr.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief T�che sch�dul�e.
        @version 0.9

        Une instance de cette classe permet d'ex�cuter r�guli�rement une m�thode d'objets pr�alablement
        enregistr�s. Il est possible d'enregistrer plusieurs m�thodes pour un m�me objet.
        @note Un objet d�truit est automatiquement d�senregistr�
        @note Aucun ordre n'est garanti concernant l'appel des m�thodes
     */
    class Task : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur.
        //@{
                Task();                                                         //!< Constructeur.
                ~Task();                                                        //!< Destructeur.
        //@}
        //! @name Activit�
        //@{
        void    setEnabled(bool pStatus);                                       //!< [D�s]Activation.
        bool    isEnabled() const;                                              //!< T�che activ�e ?
        //@}
        //! @name Enregistrement & traitement
        //@{
        template<class TT>
        void    checkIn(SharedPtr<TT>&& pObject, bool (TT::*pMethod)());    //!< Enregistrement.
        bool    process();                                                      //!< Traitement.
        //@}

    private:
        //! @name Gestion interne
        //@{
        class Callable;
        template<class TT>
        class GenericCallable;
        void    checkIn(SharedPtr<Callable>&& pCallable);                   //!< Enregistrement.
        //@}
        FORBID_COPY(Task)
        PIMPL()
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Task.inl"

#endif  // De UTI_TASK_HH
