/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_ALL_HH
#define UTI_ALL_HH

/*! @file GDK/UTIlity/All.hh
    @brief Interface publique du module @ref UTIlity.
    @author @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 25 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */
 
namespace UTI   //! Module utilitaire.
{
    /*! @namespace UTI
        @version 1.5
        @ingroup UTIlity

        Fournit quelques classes et fonctions utiles.
     */

    /*! @defgroup UTIlity UTIlity : Classes et fonctions utiles.
        <b>namespace</b> UTI.
     */
}

#include "Bits.hh"
#include "Rect.hh"
#include "Template.hh"
#include "FileMapping.hh"
#include "Progressor.hh"
#include "Task.hh"

#endif  // De UTI_ALL_HH
