/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Bits.hh"

/*! @file GDK/UTIlity/Bits.cc
    @brief D�finitions de fonctions de manipulation de bits.
    @author @ref Guillaume_Terrissol
    @date 25 Mai 2007 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
//------------------------------------------------------------------------------
//                               Inversion de Bits
//------------------------------------------------------------------------------

    /*! Inverse les bits d'un entier (8 bits).
        @param pBits Nombre dont inverser les bits
        @return L'entier obtenu en inversant ceux de <i>pBits</i>
        @note Algorithme obtenu ici : http://graphics.stanford.edu/~seander/bithacks.html#ReverseParallel
     */
    U8 reverseBits(U8 pBits)
    {
        U8::TType   lBits = pBits;

        // Permute les bits de rangs pair et impair.
        lBits = ((lBits >> 1) & 0x55) | ((lBits & 0x55) << 1);
        // Permute les paires cons�cutives.
        lBits = ((lBits >> 2) & 0x33) | ((lBits & 0x33) << 2);
        // Permute les demi-octets.
        lBits =  (lBits >> 4        ) |  (lBits         << 4);

        return U8(lBits);
    }


    /*! @overload
     */
    U16 reverseBits(U16 pBits)
    {
        U16::TType  lBits = pBits;

        // Permute les bits de rangs pair et impair.
        lBits = ((lBits >> 1) & 0x5555) | ((lBits & 0x5555) << 1);
        // Permute les paires cons�cutives.
        lBits = ((lBits >> 2) & 0x3333) | ((lBits & 0x3333) << 2);
        // Permute les demi-octets.
        lBits = ((lBits >> 4) & 0x0F0F) | ((lBits & 0x0F0F) << 4);
        // Permute les octets.
        lBits =  (lBits >> 8          ) |  (lBits           << 8);

        return U16(lBits);
   }


    /*! @overload
     */
    U32 reverseBits(U32 pBits)
    {
        U32::TType  lBits = pBits;

        // Permute les bits de rangs pair et impair.
        lBits = ((lBits >>  1) & 0x55555555) | ((lBits & 0x55555555) << 1);
        // Permute les paires cons�cutives.
        lBits = ((lBits >>  2) & 0x33333333) | ((lBits & 0x33333333) << 2);
        // Permute les demi-octets.
        lBits = ((lBits >>  4) & 0x0F0F0F0F) | ((lBits & 0x0F0F0F0F) << 4);
        // Permute les octets.
        lBits = ((lBits >>  8) & 0x00FF00FF) | ((lBits & 0x00FF00FF) << 8);
        // Permute les mots de 16 bits.
        lBits =  (lBits >> 16              ) |  (lBits               << 16);

        return U32(lBits);
    }


//------------------------------------------------------------------------------
//                                Arrondi � la puissance de 2 d'un nombre
//------------------------------------------------------------------------------

    /*! Calcule la puissance de 2 sup�rieure � un entier (8 bits).
        @param pN Nombre � arrondir � la puissance de 2 sup�rieure
        @return 2^n tel que 2^<sup>n-1</sup> <= pN <= 2^<sup>n</sup>
        @note Algorithme obtenu ici : http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
        @warning Attention aux overflows (et � 0)
     */
    U8 nextPowerOf2(U8 pN)
    {
        U8::TType   lN = pN;

        --lN;
        lN |= lN >> 1;
        lN |= lN >> 2;
        lN |= lN >> 4;
        ++lN;

        return U8(lN);
    }


    /*! @overload
     */
    U16 nextPowerOf2(U16 pN)
    {
        U16::TType  lN = pN;

        --lN;
        lN |= lN >> 1;
        lN |= lN >> 2;
        lN |= lN >> 4;
        lN |= lN >> 8;
        ++lN;

        return U16(lN);
    }


    /*! @overload
     */
    U32 nextPowerOf2(U32 pN)
    {
        U32::TType  lN = pN;

        --lN;
        lN |= lN >> 1;
        lN |= lN >> 2;
        lN |= lN >> 4;
        lN |= lN >> 8;
        lN |= lN >> 16;
        ++lN;

        return U32(lN);
    }
}
