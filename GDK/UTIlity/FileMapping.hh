/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_FILEMAPPING_HH
#define UTI_FILEMAPPING_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/FileMapping.hh
    @brief En-t�te des classes UTI::FileMappedMemoryBlock & UTI::Allocator.
    @author @ref Guillaume_Terrissol
    @date 16 Ao�t 2003 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Bloc de donn�es m�moire projet�es sur disque.
        @version 0.5
        @internal

        Une instance de cette classe permet d'allouer un gros bloc de m�moire via la projection en
        m�moire d'un fichier (cr�� pour l'occasion) d'une taille arbitraire.<br>
        @note L'acc�s � la m�moire ainsi allou�e est plus lent qu'avec la m�moire standard
        @note Si l'espace disque est insuffisant pour cr�er le fichier temporaire, aucune m�moire ne
        sera "allou�e"
     */
    class FileMappedMemoryBlock : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    FileMappedMemoryBlock(U32 pSize);   //!< Constructeur.
                    ~FileMappedMemoryBlock();           //!< Destructeur.
        //@}
        //! @name Acc�s aux donn�es projet�es
        //@{
        const void* data() const;                       //!< ... constant
        void*       data();                             //!< ... non-constant.
        U32         size() const;                       //!< Taille des donn�es.
        //@}

    private:

        FORBID_COPY(FileMappedMemoryBlock)
        PIMPL()
    };


    /*! @brief Allocateur personnalis� (via file-mapping)
        @version 0.7
        @ingroup MEM_Allocation

        Ce nouvel allocateur permet l'emploi des conteneurs de la STL avec une m�moire file-mapp�e.<br>
        Il reprend l'ensemble des m�thodes et des d�clarations de type d'un allocateur standard (ainsi que les r�gles de nommage).
     */
    template<class TT>
    class Allocator
    {
    public:
        //! @name Types li�s au type d'objet allou�
        //@{
        typedef TT              value_type;                                 //!< Type allou�.
        typedef std::size_t     size_type;                                  //!< R�sultat de sizeof().
        typedef std::ptrdiff_t  difference_type;                            //!< Diff�rence de pointeurs.

        typedef TT*             pointer;                                    //!< Pointeur sur un objet.
        typedef const TT*       const_pointer;                              //!< Pointeur constant.

        typedef TT&             reference;                                  //!< R�f�rence sur objet.
        typedef const TT&       const_reference;                            //!< R�f�rence constante.
        //@}
        //! @name Constructeurs & destructeur
        //@{
        inline                  Allocator() throw();                        //!< Constructeur par d�faut.
        inline                  Allocator(const Allocator&) throw();        //!< Constructeur par copie.
        template<class TU>
        inline                  Allocator(const Allocator<TU>&) throw();    //!< Constructeur par copie.
        inline                  ~Allocator() throw();                       //!< Destructeur.
        //@}
        //! @name Adresses
        //@{
        inline  pointer         address(reference pX) const;                //!< ... d'un objet.
        inline  const_pointer   address(const_reference pX) const;          //!< ... d'un objet constant.
        //@}
        //! @name [D�s]Allocation m�moire d'un objet
        //@{
 static inline  pointer         allocate(size_type pN, const void* = 0);    //!< Allocation.
 static inline  void            deallocate(void* pP, size_type pN);         //!< D�sallocation.
        //@}
        //! @name [D�s]Initialisation d'un objet
        //@{
        inline  void            construct(pointer pP, const_reference pV);  //!< Initialisation.
        inline  void            destroy(pointer pP);                        //!< Destruction.
        //@}
        inline  size_type       max_size() const throw();                   //!< Nombre maximum d'objets.

        //! Consultez "The C++ Programming Language, 3rd Edition" (�19.4.1).
        template<class TU> struct rebind
        {
            //! Allocateur pour types internes.
            typedef Allocator<TU>   other;
        };
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "FileMapping.inl"

#endif  // De UTI_FILEMAPPING_HH
