/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/UTIlity/Task.inl
    @brief M�thodes inline de la classe UTI::Task::Callable.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2008 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
//------------------------------------------------------------------------------
//                                   Callable
//------------------------------------------------------------------------------

    /*! @brief Objet sch�dul�.
        @version 1.0
        @internal

        Le seul int�r�t de cette classe est de fournir une classe de base non-template �
        TASK::GenericCallable<TT>.
     */
    class Task::Callable
    {
    public:

virtual                 ~Callable();            //!< Destructeur.
        //! @name Interface
        //@{
        inline  bool    isValid() const;        //!< Validit�.
        inline  bool    process();              //!< Traitement.
        //@}

    private:
        //! @name Impl�mentation
        //@
virtual         bool    validity() const = 0;   //!< Impl�mentation de la d�termination de la validit�.
virtual         bool    doProcess() = 0;        //!< Impl�mentation du traitement.
        //@}
    };


//------------------------------------------------------------------------------
//                             Callable : Interface
//------------------------------------------------------------------------------

    /*! @return VRAI si l'objet encapsul� est toujours valide, FAUX sinon
     */
    inline bool Task::Callable::isValid() const
    {
        return validity();
    }


    /*! @return VRAI si le traitement a r�ussi
     */
    inline bool Task::Callable::process()
    {
        return doProcess();
    }
}
