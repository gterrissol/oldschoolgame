/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Rect.hh"

/*! @file GDK/UTIlity/Rect.cc
    @brief M�thodes (non-inline) de la classe UTI::Rect.
    @author @ref Guillaume_Terrissol
    @date 4 Avril 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cmath>

#include "ERRor/Log.hh"
#include "MATH/Const.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.<br>
        Le rectangle est vide, centr� � l'origine.
     */
    Rect::Rect()
        : mX1(k0F)
        , mY1(k0F)
        , mX2(- k1F)
        , mY2(- k1F)
    { }


    /*! Initialise une instance � partir de la position et de la taille.
        @param pPos Position du coin sup�rieur gauche
        @param pSz  Taille
     */
    Rect::Rect(Pointf pPos, Sizef pSz)
        : mX1(pPos.x)
        , mY1(pPos.y)
        , mX2(mX1 + pSz.width)
        , mY2(mY1 - pSz.height)
    { }


    /*! Initialise une instance � partir des composantes s�par�es.
        @param pL Abscisse gauche
        @param pT Ordonn�e haute
        @param pR Abscisse droite
        @param pB Ordonn�e basse
     */
    Rect::Rect(F32 pL, F32 pT, F32 pR, F32 pB)
        : mX1(pL)
        , mY1(pT)
        , mX2(pR)
        , mY2(pB)
    { }


//------------------------------------------------------------------------------
//                              Etats du Rectangle
//------------------------------------------------------------------------------

    /*! Permet de savoir si le rectangle est vide, i.e. Left() >= Right() ou Bottom() >= Top().<br>
        Un rectangle vide n'est pas valide.
     */
    bool Rect::isEmpty() const
    {
        return (right() <= left()) || (top() <= bottom());
    }


    /*! Permet de savoir si le rectangle est nul, i.e. ses dimensions sont nulles (Right() == Left() et
        Bottom() == Top().<br>
        Un rectangle nul est �galement vide, et n'est pas valide.
     */
    bool Rect::isNull() const
    {
        return (fabsf(right() - left()) < kEpsilon) || (fabsf(top() - bottom()) < kEpsilon);
    }


    /*! Un rectangle est valide si Left() <= Right() et Bottom() <= Top().
        @return VRAI si le rectangle est valide, FAUX sinon
     */
    bool Rect::isValid() const
    {
        return (left() <= right()) && (bottom() <= top());
    }


//------------------------------------------------------------------------------
//                               Acc�s aux Donn�es
//------------------------------------------------------------------------------

    /*! @return Le coin sup�rieur gauche du rectangle
     */
    Pointf Rect::topLeft() const
    {
        return Pointf(left(), top());
    }


    /*! @return Le coin inf�rieur droit du rectangle
     */
    Pointf Rect::bottomRight() const
    {
        return Pointf(right(), bottom());
    }


    /*! @return Le coin sup�rieur droit du rectangle
     */
    Pointf Rect::topRight() const
    {
        return Pointf(right(), top());
    }


    /*! @return Le coin inf�rieur gauche du rectangle
     */
    Pointf Rect::bottomLeft() const
    {
        return Pointf(left(), bottom());
    }


    /*! @return Le centre du rectangle du rectangle
     */
    Pointf Rect::center() const
    {
        return Pointf(F32(0.5F) * (left() + right()), F32(0.5F) * (bottom() + top()));
    }


    /*! @return La largeur du rectangle
     */
    F32 Rect::width() const
    {
        return right() - left();
    }


    /*! @return La hauteur du rectangle
     */
    F32 Rect::height() const
    {
        return top() - bottom();
    }


    /*! @return La taille du rectangle
     */
    Sizef Rect::size() const
    {
        return Sizef(width(), height());
    }


//------------------------------------------------------------------------------
//                               Donn�es Discr�tes
//------------------------------------------------------------------------------

    /*! @return L'abscisse gauche du rectangle (discr�te; arrondie par d�faut)
     */
    I32 Rect::roundedLeft() const
    {
        return I32(static_cast<I32::TType>(rintf(left())));
    }


    /*! @return L'ordonn�e sup�rieure du rectangle (discr�te; arrondie par d�faut)
     */
    I32 Rect::roundedTop() const
    {
        return I32(static_cast<I32::TType>(rintf(top())));
    }


    /*! @return L'abscisse droite du rectangle (discr�te; arrondie par d�faut)
     */
    I32 Rect::roundedRight() const
    {
        return I32(static_cast<I32::TType>(rintf(right())));
    }


    /*! @return L'ordonn�e inf�rieure du rectangle (discr�te; arrondie par d�faut)
     */
    I32 Rect::roundedBottom() const
    {
        return I32(static_cast<I32::TType>(rintf(bottom())));
    }


    /*! @return La largeur du rectangle (discr�te; arrondie par d�faut)
     */
    U32 Rect::roundedWidth() const
    {
        return U32(static_cast<U32::TType>(rintf(width())));
    }


    /*! @return La hauteur du rectangle (discr�te; arrondie par d�faut)
     */
    U32 Rect::roundedHeight() const
    {
        return U32(static_cast<U32::TType>(rintf(height())));
    }


//------------------------------------------------------------------------------
//                                    Edition
//------------------------------------------------------------------------------

    /*! Modifie les coordonn�es du coin sup�rieur gauche.
        @param pPoint Nouvelles coordonn�es pour le coin sup�rieur gauche
        @note Les dimensions peuvent changer mais le coin inf�rieur droit ne sera pas modifi�
     */
    void Rect::setTopLeft(Pointf pPoint)
    {
        mX1 = pPoint.x;
        mY1 = pPoint.y;
    }


    /*! Modifie les coordonn�es du coin inf�rieur droit.
        @param pPoint Nouvelles coordonn�es pour le coin inf�rieur droit
        @note Les dimensions peuvent changer mais le coin sup�rieur gauche ne sera pas modifi�
     */
    void Rect::setBottomRight(Pointf pPoint)
    {
        mX2 = pPoint.x;
        mY2 = pPoint.y;
    }


    /*! Modifie les coordonn�es du coin sup�rieur droit.
        @param pPoint Nouvelles coordonn�es pour le coin sup�rieur droit
        @note Les dimensions peuvent changer mais le coin inf�rieur gauche ne sera pas modifi�
     */
    void Rect::setTopRight(Pointf pPoint)
    {
        mX2 = pPoint.x;
        mY1 = pPoint.y;
    }


    /*! Modifie les coordonn�es du coin inf�rieur gauche.
        @param pPoint Nouvelles coordonn�es pour le coin inf�rieur gauche
        @note Les dimensions peuvent changer mais le coin sup�rieur droit ne sera pas modifi�
     */
    void Rect::setBottomLeft(Pointf pPoint)
    {
        mX1 = pPoint.x;
        mY2 = pPoint.y;
    }


    /*! Modifie le rectangle entier.
        @param pX Nouvelle abscisse gauche
        @param pY Nouvelle ordonn�e haute
        @param pW Nouvelle largeur
        @param pH Nouvelle hauteur
     */
    void Rect::setRect(F32 pX, F32 pY, F32 pW, F32 pH)
    {
        mX1 = pX;
        mY1 = pY;
        mX2 = mX1 + pW;
        mY2 = mY1 - pH;
    }


    /*! Modifie la largeur.
        @param pW Nouvelle largeur
        @note L'abscisse gauche reste inchang�e
     */
    void Rect::setWidth(F32 pW)
    {
        mX2 = mX1 + pW;
    }


    /*! Modifie la hauteur.
        @param pH Nouvelle hauteur
        @note L'ordonn�e sup�rieure reste inchang�e
     */
    void Rect::setHeight(F32 pH)
    {
        mY2 = mY1 - pH;
    }


    /*! Modifie la taille.
        @param pSz Nouvelles dimensions
        @note Le coin sup�rieur gauche reste inchang�
     */
    void Rect::setSize(Sizef pSz)
    {
        mX2 = mX1 + pSz.width;
        mY2 = mY1 - pSz.height;
    }


//------------------------------------------------------------------------------
//                                 D�placements
//------------------------------------------------------------------------------

    /*! D�place le rectangle vers la gauche.
        @param pfOffset D�calage vers la gauche
        @note La taille n'est pas modifi�e
     */
    void Rect::moveLeft(F32 pfOffset)
    {
        mX1 -= pfOffset;
        mX2 -= pfOffset;
    }


    /*! D�place le rectangle vers le haut.
        @param pfOffset D�calage vers le haut
        @note La taille n'est pas modifi�e
     */
    void Rect::moveTop(F32 pfOffset)
    {
        mY1 += pfOffset;
        mY2 += pfOffset;
    }


    /*! D�place le rectangle vers la droite.
        @param pfOffset D�calage vers la droite
        @note La taille n'est pas modifi�e
     */
    void Rect::moveRight(F32 pfOffset)
    {
        mX1 += pfOffset;
        mX2 += pfOffset;
    }


    /*! D�place le rectangle vers le bas.
        @param pfOffset D�calage vers le bas
        @note La taille n'est pas modifi�e
     */
    void Rect::moveBottom(F32 pfOffset)
    {
        mY1 -= pfOffset;
        mY2 -= pfOffset;
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! Op�rateur d'�galit�.
        @param pL Premier rectangle � consid�rer
        @param pR Premier rectangle � consid�rer
        @return VRAI si les deux rectangles sont identiques (m�me position, m�me taille), FAUX sinon
     */
    bool operator==(const Rect& pL, const Rect& pR)
    {
        return (pL.left()   == pR.left())  &&
               (pL.top()    == pR.top())   && 
               (pL.right()  == pR.right()) && 
               (pL.bottom() == pR.bottom());
    }


    /*! Op�rateur d'in�galit�.
        @param pL Premier rectangle � consid�rer
        @param pR Premier rectangle � consid�rer
        @return VRAI si les deux rectangles diff�rent (en position ou en taille), FAUX s'ils sont
        identiques
     */
    bool operator!=(const Rect& pL, const Rect& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                               Autres Op�rations
//------------------------------------------------------------------------------

    /*! Calcule le rectangle r�sultant de l'intersection de deux rectangle.
        @param pL Premier rectangle � consid�rer
        @param pR Second rectangle � consid�rer
        @return L'intersection de <i>pL</i> et <i>pR</i> ou un rectangle vide si <i>pL</i> et <i>pR</i>
        ne se recoupent pas
     */
    Rect operator&(const Rect& pL, const Rect& pR)
    {
        // Si les rectangles sont invalides, ou s'ils ne s'intersectent pas,
        // pas la peine d'aller plus loin.
        if (!(pL.isValid() && pR.isValid()) || (intersects(pL, pR) == eOutside))
        {
            // Rectangle invalide.
            return Rect(k0F, k0F, - k1F, - k1F);
        }
        else if (intersects(pL, pR) == eInside)
        {
            // pR est contenu dans pL.
            return pR;
        }
        else if (intersects(pR, pL) == eInside)
        {
            // pL est contenu dans pR.
            return pL;
        }
        else
        {
            Rect    lIntersection(std::max(pL.left(),   pR.left()),
                                  std::min(pL.top(),    pR.top()),
                                  std::min(pL.right(),  pR.right()),
                                  std::max(pL.bottom(), pR.bottom()));

            return lIntersection;
        }
    }


    /*! Redimensionne un rectangle selon un facteur d'�chelle donn�.
        @param pRect Rectangle � redimensionner
        @param pScale  Facteur d'�chelle
        @return <i>pRect</i> �tir� (si 1.0F < <i>pScale</i>), r�tr�ci (si <i>pScale</i> < 1.0F) ou
        inchang� (si <i>pScale</i> == 1.0F)
     */
    Rect operator*(const Rect& pRect, F32 pScale)
    {
        return Rect(pRect.left()  * pScale, pRect.top()    * pScale,
                    pRect.right() * pScale, pRect.bottom() * pScale);
    }


    /*! Redimensionne un rectangle selon un facteur d'�chelle donn�.
        @param pScale Facteur d'�chelle
        @param pRect  Rectangle � redimensionner
        @return <i>pRect</i> �tir� (si 1.0F < <i>pfScale</i>), r�tr�ci (si <i>pfScale</i> < 1.0F) ou
        inchang� (si <i>pfScale</i> == 1.0F)
     */
    Rect operator*(F32 pScale, const Rect& pRect)
    {
        return pRect * pScale;
    }


//------------------------------------------------------------------------------
//                                 Intersections
//------------------------------------------------------------------------------

    /*! Teste si un point se trouve � l'int�rieur d'un rectangle.
        @param pRect  Rectangle � consid�rer
        @param pPoint Point dont on cherche � savoir s'il appartient � <i>pRect</i>
        @return VRAI si <i>pPoint</i> est � l'int�rieur de <i>pRect</i> (bords compris), FAUX sinon
        @note Un rectangle vide ne contient aucun point
     */
    bool contains(const Rect& pRect, Pointf pPoint)
    {
        return  pRect.isValid() &&
               (pRect.left()   <= pPoint.x) && (pPoint.x <= pRect.right()) &&
               (pRect.bottom() <= pPoint.y) && (pPoint.y <= pRect.top());
    }


    /*! D�termine l'intersection de deux rectangles.
        @param pL Premier rectangle � consid�rer
        @param pR Second rectangle � consid�rer
        @return E_Inside, E_Overlaps ou E_Outside selon que <i>pL</i> est, respectivement, 
        compl�tement � l'int�rieur du rectangle, en partie � l'int�rieur, ou � l'ext�rieur de
        <i>pR</i>
     */
    EIntersection intersects(const Rect& pL, const Rect& pR)
    {
        bool    lIntersects = false;
        bool    lInside     = true;

        // Si un point au moins est � l'int�rieur, il y a intersection.
        contains(pR, pL.topLeft())     ? lIntersects = true : lInside = false;
        contains(pR, pL.topRight())    ? lIntersects = true : lInside = false;
        contains(pR, pL.bottomRight()) ? lIntersects = true : lInside = false;
        contains(pR, pL.bottomLeft())  ? lIntersects = true : lInside = false;

        if      (lInside)
        {
            return eInside;
        }
        else if (lIntersects)
        {
            return eOverlaps;
        }
        else
        {
            if ((std::max(pL.left(),   pR.left())   <= std::min(pL.right(), pR.right()))
                &&
                (std::max(pL.bottom(), pR.bottom()) <= std::min(pL.top(),   pR.top())))
            {
                return eOverlaps;
            }

            return eOutside;
        }
    }


//------------------------------------------------------------------------------
//                           Affichage d'un Rectangle
//------------------------------------------------------------------------------

    /*! Affichage d'un rectangle sous la forme :<br>
        (Left, Top)-(Right, Bottom) : (Width x Height)
        @ingroup UTI_Rectangle_Interface
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Rectangle � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const Rect& pToPrint)
    {
        pLog << '(' << pToPrint.left()  << ", "  << pToPrint.top()    << ")-("
                    << pToPrint.right() << ", "  << pToPrint.bottom() << ") : ("
                    << pToPrint.width() << " x " << pToPrint.height() << ')';

        return pLog;
    }


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var PointT<TT>::x
        Abscisse du point.
     */
    /*! @var PointT<TT>::y
        Ordonn�e du point.
     */
    /*! @var SizeT<TT>::width
        Largeur de la taille (de gauche � droite).
     */
    /*! @var SizeT<TT>::height
        Hauteur de la taille (de haut en bas, ou "d'arri�re � avant").
     */
}
