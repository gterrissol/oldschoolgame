/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/UTIlity/Template.inl
    @brief Fonctions utiles (d�finitions).
    @author @ref Guillaume_Terrissol
    @date 18 Septembre 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
//------------------------------------------------------------------------------
//                              M�ta-Programmation
//------------------------------------------------------------------------------

    /*! Sp�cialisation de la fonction Init (arr�t de la r�cursivit�).
     */
    template<>
    inline void init<0>(F32[], F32) { }


    /*! Initialise r�cursivement (par m�ta-programmation) un tableau de F32, � partir du sommet.
        @param pArray Tableau d'instance de F32
        @param pValue Valeur � utiliser pour initialiser les �l�ments de <i>pArray</i>
        @warning Aucun contr�le n'est effectu� sur la taille de <i>pArray</i>
        @sa Copy
     */
    template<int TC>
    inline void init(F32 pArray[], F32 pValue)
    {
        // Initialise (en partant de la fin).
        pArray[TC - 1] = pValue;

        // Et on continue.
        init<TC - 1>(pArray, pValue);
    }


    /*! Sp�cialisation de la fonction Copy (arr�t de la r�cursivit�).
     */
    template<>
    inline void copy<0>(F32[], const F32[]) { }


    /*! Copie r�cursivement (par m�ta-programmation) un tableau de F32, � partir du sommet, dans un
        autre.
        @note Je sais, c'est bourrin, mais je fais ce que je peux
        @param pArray  Tableau d'instance de F32
        @param pValues Valeurs � utiliser pour initialiser les �l�ments de <i>pArray</i>
        @warning Aucun contr�le n'est effectu� sur les tailles des tableaux <i>pArray</i> et
        <i>pValue</i>
        @sa Init
        @note J'aurais pu �crire
        @code
        pArray[TC] = pValue[TC];
        @endcode
        et appeler Init<TC - 1>(...), mais la sp�cialisation pour l'arr�t n'aurait pas �t� une simple
        fonction vide
     */
    template<int TC>
    inline void copy(F32 pArray[], const F32 pValues[])
    {
        // Copie (en partant de la fin).
        pArray[TC - 1] = pValues[TC - 1];

        // Et on continue.
        copy<TC - 1>(pArray, pValues);
    }


    /*! Sp�cialisation de la fonction CmpN (arr�t de la r�cursivit�).
     */
    template<> inline bool cmpN<0>(const F32[], const F32[])
    {
        return true;
    }


    /*! Compare r�cursivement (par m�ta-programmation) deux tableau de F32, � partir du sommet.
        @param pRefArray Premier tableau de valeurs � comparer
        @param pArray    Second tableau de valeurs � comparer
        @note Cette fonction n'est �videmment utile que pour d'autres templates.
     */
    template<int TC>
    inline bool cmpN(const F32 pRefArray[], const F32 pArray[])
    {
        return ((pRefArray[TC - 1] == pArray[TC - 1]) && cmpN<TC - 1>(pRefArray, pArray));
    }
}
