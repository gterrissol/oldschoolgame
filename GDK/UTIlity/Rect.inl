/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/UTIlity/Rect.inl
    @brief M�thodes inline des classes UTI::PointT, UTI::SizeT & UTI::Rect.
    @author Guillaume_Terrissol
    @date 4 Avril 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
//------------------------------------------------------------------------------
//                             Point : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : cr�e un point � l'origine (0, 0).
     */
    template<typename TT>
    PointT<TT>::PointT()
        : x(0)
        , y(0)
    { }


    /*! Construit un point � une position donn�e.
        @param pX Abscisse du point
        @param pY Ordonn�e du point
     */
    template<typename TT>
    PointT<TT>::PointT(TT pX, TT pY)
        : x(pX)
        , y(pY)
    { }


//------------------------------------------------------------------------------
//                            Taille : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : cr�e une taille vide (mais valide).
     */
    template<typename TT>
    SizeT<TT>::SizeT()
        : width(0)
        , height(0)
    { }


    /*! Construit une taille de dimensions arbitraires.
        @param pW Largeur de la taille (voir SizeT<TT>::width)
        @param pH Hauteur de la taille (voir SizeT<TT>::height)
     */
    template<typename TT>
    SizeT<TT>::SizeT(TT pW, TT pH)
        : width(pW)
        , height(pH)
    { }


//------------------------------------------------------------------------------
//                             Taille : Informations
//------------------------------------------------------------------------------

    /*! Permet de savoir si la taille est nulle (i.e. width == height == 0).
        @return VRAI si la taille est nulle, FAUX sinon
     */
    template<typename TT>
    bool SizeT<TT>::isNull() const
    {
        return (width == 0) && (height == 0);
    }


    /*! Permet de savoir si la taille est valide (i.e. 0 <= width && 0 <= height).
        @return VRAI si la taille est valide, FAUX sinon
     */
    template<typename TT>
    bool SizeT<TT>::isValid() const
    {
        return (0 <= width) && (0 <= height);
    }



//------------------------------------------------------------------------------
//                         Rectangle : Acc�s aux Donn�es
//------------------------------------------------------------------------------

    /*! @return L'abscisse gauche du rectangle
     */
    inline F32 Rect::left() const
    {
        return mX1;
    }


    /*! @return L'ordonn�e sup�rieure du rectangle
     */
    inline F32 Rect::top() const
    {
        return mY1;
    }


    /*! @return L'abscisse droite du rectangle
     */
    inline F32 Rect::right() const
    {
        return mX2;
    }


    /*! @return L'ordonn�e inf�rieure du rectangle
     */
    inline F32 Rect::bottom() const
    {
        return mY2;
    }


//------------------------------------------------------------------------------
//                              Rectangle : Edition
//------------------------------------------------------------------------------

    /*! Modifie l'abscisse gauche.
        @param pfPosition Nouvelle valeur pour l'abscisse gauche
        @note La largeur peut changer mais l'abscisse droite ne sera pas modifi�e
     */
    inline void Rect::setLeft(F32 pfPosition)
    {
        mX1 = pfPosition;
    }


    /*! Modifie l'ordonn�e haute.
        @param pfPosition Nouvelle valeur pour l'ordonn�e haute
        @note La hauteur peut changer mais l'ordonn�e basse ne sera pas modifi�e
     */
    inline void Rect::setTop(F32 pfPosition)
    {
        mY1 = pfPosition;
    }


    /*! Modifie l'abscisse droite.
        @param pfPosition Nouvelle valeur pour l'abscisse droite
        @note La largeur peut changer mais l'abscisse gauche ne sera pas modifi�e
     */
    inline void Rect::setRight(F32 pfPosition)
    {
        mX2 = pfPosition;
    }


    /*! Modifie l'ordonn�e basse.
        @param pfPosition Nouvelle valeur pour l'ordonn�e basse
        @note La hauteur peut changer mais l'ordonn�e basse ne sera pas modifi�e
     */
    inline void Rect::setBottom(F32 pfPosition)
    {
        mY2 = pfPosition;
    }
}

