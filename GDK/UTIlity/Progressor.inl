/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/UTIlity/Progressor.inl
    @brief M�thodes inline de la classe UTI::Progressor.
    @author Guillaume_Terrissol
    @date 10 Mai 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
//------------------------------------------------------------------------------
//                                  Avancement
//------------------------------------------------------------------------------

    /*! @return L'avancement actuel (l'�tape actuelle)
     */
    inline U32 Progressor::currentStep() const
    {
        return mCurrent;
    }


    /*! @return L'avancement actuel (en pourcentage : [ 0.00, 1.00 ])
     */
    inline F32 Progressor::currentPercentage() const
    {
        return F32(mCurrent * mStep);
    }
}

