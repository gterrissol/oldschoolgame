/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_UTILITY_HH
#define UTI_UTILITY_HH

/*! @file UTIlity/UTIlity.hh
    @brief Pr�-d�clarations du module @ref UTIlity.
    @author @ref Guillaume_Terrissol
    @date 2007 28 D�cembre 2007 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace UTI
{
    class Progressor;
    class Rect;
    class Task;

    template<class TT>      class Allocator;
    template<typename TT>   struct PointT;
    template<typename TT>   struct SizeT;
}

#endif  // De UTI_UTILITY_HH
