/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_PROGRESSOR_HH
#define UTI_PROGRESSOR_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/Progressor.hh
    @brief En-t�te de la classe UTI::Progressor.
    @author Guillaume_Terrissol
    @date 10 Mai 2005 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace UTI
{
    /*! @brief Indicateur d'avancement.
        @version 1.0
        @ingroup UTIlity

        Cette classe permet de communiquer l'�tat d'avancement d'une t�che.
     */
    class Progressor : public MEM::OnHeap
    {
    public :
        //! @name Constructeur & destructeur
        //@{
                        Progressor();                       //!< Constructeur par d�faut.
virtual                 ~Progressor();                      //!< Destructeur.
        //@}
        //! @name Avancement
        //@{
                void    setStepCount(U32 pCount);           //!< D�finition du nombre d'�tapes.
                void    setProgress(U32 pCurrentStep);      //!< Etablissement manuel de l'avancement.
                void    next();                             //!< Vers l'�tape suivante.
        inline  U32     currentStep() const;                //!< Avancement actuel.
        inline  F32     currentPercentage() const;          //!< Avancement actuel.


    private:

virtual         void    progressChanged(F32 pNewProgress);  //!< "Signal" d'avancement.
        //@}
        //! @name Attributs
        //@{
        U32 mLastStep;                                      //!< Derni�re �tape de l'avancement.
        U32 mCurrent;                                       //!< Etape actuelle.
        F32 mStep;                                          //!< Amplitude d'une �tape.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Progressor.inl"

#endif  // De UTI_PROGRESSOR_HH


