/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_TEMPLATE_HH
#define UTI_TEMPLATE_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/Template.hh
    @brief Fonctions utiles (template).
    @author @ref Guillaume_Terrissol
    @date 18 Septembre 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                              M�ta-Programmation
//------------------------------------------------------------------------------

    /*! @defgroup UTI_Metaprogramming M�ta-programmation
        @ingroup UTIlity
     */
    //@{
    template<int TC>
    void    init(F32 pArray[], F32 pValue);                     //!< Initialisation de donn�es.
    template<int TC>
    void    copy(F32 pArray[], const F32 pValues[]);            //!< Initialisation de donn�es par copie.
    template<int TC>
    bool    cmpN(const F32 pRefArray[], const F32 pArray[]);    //!< Comparaison de tableaux.
    //@}
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Template.inl"

#endif  // De UTI_TEMPLATE_HH
