/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/UTIlity/FileMapping.inl
    @brief M�thodes inline de la classe UTI::Allocator.
    @author @ref Guillaume_Terrissol
    @date 17 Ao�t 2003 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ErrMsg.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                             Allocator : M�thodes
//------------------------------------------------------------------------------

    /*! Teste l'�galit� de 2 allocateurs.
        @return Toujours VRAI
     */
    template<class TT>
    inline bool operator==(const Allocator<TT>&, const Allocator<TT>&)
    {
        return true;
    }


    /*! Teste la non-�galit� de 2 allocateurs.
        @return Toujours FAUX
     */
    template<class TT>
    inline bool operator!=(const Allocator<TT>&, const Allocator<TT>&)
    {
        return false;
    }

        
    /*! Constructeur par d�faut, vide.
     */
    template<class TT>
    inline Allocator<TT>::Allocator() throw() { }


    /*! Constructeur par copie, vide.
     */
    template<class TT>
    inline Allocator<TT>::Allocator(const Allocator&) throw() { }
    

    /*! Constructeur par copie d'un allocateur instanci� avec un autre param�tre de mod�le, vide.
     */
    template<class TT>
    template<class TU> 
    inline Allocator<TT>::Allocator(const Allocator<TU>&) throw() { }


    /*! Destructeur, vide.
     */
    template<class TT>
    inline Allocator<TT>::~Allocator() throw() { }


    /*! @param pX Variable dont on cherche l'adresse en m�moire
        @return L'adresse en m�moire de <i>pX</i>
     */
    template<class TT>
    inline typename Allocator<TT>::pointer Allocator<TT>::address(reference pX) const
    {
        return &pX;
    }


    /*! @param pX Variable "constante" dont on cherche l'adresse en m�moire
        @return L'adresse en m�moire de <i>pX</i>
     */
    template<class TT>
    inline typename Allocator<TT>::const_pointer Allocator<TT>::address(const_reference pX) const
    {
        return &pX;
    }


    /*! Alloue de la m�moire.
        @param pN Nombre d'�l�ments � allouer
        @return Un pointeur sur bloc m�moire allou� pour <i>pN</i> <i>TT</i>
     */
    template<class TT>
    inline typename Allocator<TT>::pointer Allocator<TT>::allocate(size_type pN, const void*)
    {
        FileMappedMemoryBlock* lBlock  = new FileMappedMemoryBlock(U32(pN * sizeof(value_type)));

        return static_cast<TT*>(lBlock->data());
    }


    /*! Lib�re de la m�moire.
        @param pPointer Pointeur sur la zone m�moire � lib�rer
        @param pN       Nombre de <i>TT</i> � lib�rer
     */
    template<class TT>
    inline void Allocator<TT>::deallocate(void* pPointer, size_type pN)
    {
        FileMappedMemoryBlock* lBlock = reinterpret_cast<FileMappedMemoryBlock**>(pPointer)[-1];

        ASSERT(lBlock->size() == U32(pN * sizeof(TT)), kInvalidElementCount)

        delete lBlock;
    }


    /*! Initialise *<i>pPointer</i> par <i>pVal</i>.
        @param pPointer Pointeur sur la zone m�moire o� placer un nouvel objet
        @param pVal     Valeur � utiliser pour l'initialisation de l'objet nouvellement cr��
     */
    template<class TT>
    inline void Allocator<TT>::construct(pointer pPointer, const_reference pVal)
    {
        new(pPointer) value_type(pVal);
    }


    /*! D�truit *<i>pPointer</i>, mais ne lib�re pas la m�moire.
        @param pPointer Pointer sur l'objet � d�truire
     */
    template<class TT>
    inline void Allocator<TT>::destroy(pointer pPointer)
    {
        pPointer->~TT();
    }


    /*! @return Le nombre maximum d'objets de type <i>value_type</i> pouvant �tre allou�s par
        l'allocateur
     */
    template<class TT>
    inline typename Allocator<TT>::size_type Allocator<TT>::max_size() const throw()
    {
        return size_type(- 1) / sizeof(value_type);
    }
}
