/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UTI_BITS_HH
#define UTI_BITS_HH

#include "UTIlity.hh"

/*! @file GDK/UTIlity/Bits.hh
    @brief En-t�te de fonctions de manipulation de bits.
    @author @ref Guillaume_Terrissol
    @date 25 Mai 2007 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                                   Fonctions
//------------------------------------------------------------------------------

    /*! @defgroup UTI_Bits Manipulation de bits
        @ingroup UTIlity
     */
    //@{
    //! @name Inversion des bits d'un nombre entier
    //@{
    U8  reverseBits(U8  pBits); //!< ... 8 bits.
    U16 reverseBits(U16 pBits); //!< ... 16 bits.
    U32 reverseBits(U32 pBits); //!< ... 32 bits.
    //@}

    //! @name Arrondi � la puissance de 2 d'un nombre entier
    //@{
    U8  nextPowerOf2(U8  pN);   //!< ... 8 bits.
    U16 nextPowerOf2(U16 pN);   //!< ... 16 bits.
    U32 nextPowerOf2(U32 pN);   //!< ... 32 bits.
    //@}
    //@}
}

#endif  // De UTI_BITS_HH
