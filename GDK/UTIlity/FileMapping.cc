/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "FileMapping.hh"

/*! @file GDK/UTIlity/FileMapping.cc
    @brief M�thodes (non-inline) de la classe UTI::FileMappedMemoryBlock.
    @author @ref Guillaume_Terrissol
    @date 16 Ao�t 2003 - 30 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <string>
#include <sstream>

#if defined(LINUX)
#   include <fcntl.h>
#   include <sys/mman.h>
#   include <unistd.h>
#   define LINUX_FILEMAPPING
#   undef  WIN32_FILEMAPPING
#elif defined(MINGW)
#   include <io.h>
#   include <windows.h>
#   undef  LINUX_FILEMAPPING
#   define WIN32_FILEMAPPING
#   define MAP_FAILED 0
#else
#   error "Aucune fonction syst�me n'est disponible pour impl�menter le file-mapping."
#endif

#include "ERRor/Assert.hh"
#include "ERRor/Exception.hh"
#include "MEMory/Allocator.hh"
#include "STL/String.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                      P-Impl de File Mapped Memory Block
//------------------------------------------------------------------------------

    /*! @brief P-Impl de UTI::FileMappedMemoryBlock.
        @version 0.5
        @internal
     */
    class FileMappedMemoryBlock::Private : public OnHeap
    {
    public:
                                Private(U32 pSize);     //!< Constructeur.

                const String&   fileName() const;       //!< Nom du fichier.

        inline  U32             fullSize() const;       //!< Taille totale des donn�es.
        inline  void            setData(void* pData);   //!< D�finition des donn�es.
        inline  void*           data();                 //!< Acc�s aux donn�es.

    private:
        //! @name Donn�es
        //@{
        mutable String  mFileName;                      //!< Nom du fichier projet� pour cette instance.
        void*           mData;                          //!< Donn�es projet�es.
        U32             mFullSize;                      //!< Taille totale des donn�es projet�es.
        //@}
    };


//------------------------------------------------------------------------------
//                  File Mapped Memory Block Private : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pSize Taille du bloc � allouer
     */
    FileMappedMemoryBlock::Private::Private(U32 pSize)
         : mFileName("")
#if defined(LINUX_FILEMAPPING)
        , mData(MAP_FAILED)
#elif defined(WIN32_FILEMAPPING)
        , mData(nullptr)
#else
#   error "Aucune fonction syst�me n'est disponible pour impl�menter le file-mapping."
#endif
        , mFullSize(pSize + sizeof(FileMappedMemoryBlock*))
    { }


    /*! Tout bloc de donn�es projet� en m�moire est associ�e � un fichier.<br>
        Le nom de ce fichier est cr�� � la vol�e, lors du premier appel � cette m�thode.
        @return Le nom du fichier dont le contenu est projet� en m�moire
     */
    const String& FileMappedMemoryBlock::Private::fileName() const
    {
        // G�n�re le nom du fichier, si cela n'a pas d�j� �t� fait.
        if (mFileName == "")
        {
            // Le fichier � projeter est cr�� dans le r�pertoire courant.
            char    lWorkingDirectory[1024];
            // Win32 : _getdcwd
            if (getcwd(lWorkingDirectory, 1024) == nullptr)
            {
                LAUNCH_EXCEPTION(kPathTooLong)
            }
            mFileName  = lWorkingDirectory;
            mFileName += "/";

            // Son nom est d�fini par l'adresse de l'instance.
            const void* kThis = static_cast<const void*>(this);
            mFileName << kThis;
            mFileName += ".map";
        }

        return mFileName;
    }


    /*! @return La taille totale des donn�es projet�es
     */
    inline U32 FileMappedMemoryBlock::Private::fullSize() const
    {
        return mFullSize;
    }


    /*! @param pData Pointeur sur les donn�es projet�es
     */
    inline void FileMappedMemoryBlock::Private::setData(void* pData)
    {
#if defined(LINUX_FILEMAPPING)
        ASSERT(mData == MAP_FAILED, kAlreadyProjectedData)
#elif defined(WIN32_FILEMAPPING)
        ASSERT(mData == nullptr, kAlreadyProjectedData)
#endif
        mData = pData;
    }


    /*! @return Un pointeur sur les donn�es projet�es
     */
    inline void* FileMappedMemoryBlock::Private::data()
    {
        return mData;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pSize Taille des donn�es � allouer
        @note Quelques octets en plus sont requis
     */
    FileMappedMemoryBlock::FileMappedMemoryBlock(U32 pSize)
        : pthis(pSize)
    {
        // Nom du fichier � cr�er.
        String    lFileName   = pthis->fileName();

#if defined(LINUX_FILEMAPPING)

        // Cr�e le fichier � projeter.
        int lFileDescriptor = open(lFileName.c_str(), O_RDWR | O_CREAT, S_IRWXU);

        if (lFileDescriptor == -1)
        {
            LAUNCH_EXCEPTION(kCouldntCreateFileToProject)
        }

        static const void*  kNull = nullptr;

        // Redimensionne le fichier.
        off_t   lRealSize = lseek(lFileDescriptor, pthis->fullSize() - sizeof(void*), SEEK_SET);
        // Il faut bien �crire quelque chose, d�placer le curseur de suffit pas :
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        write(lFileDescriptor, &kNull, sizeof(void*));
#pragma GCC diagnostic pop
        // Rappel : FullSize() == n + sizeof(void*) (avec 0 < n).
        lRealSize = lseek(lFileDescriptor, 0, SEEK_CUR);

        // Projette les donn�es du fichier.
        if (pthis->fullSize() == U32(lRealSize))
        {
            pthis->setData(mmap(nullptr, lRealSize, PROT_READ | PROT_WRITE, MAP_SHARED, lFileDescriptor, 0));
        }
        close(lFileDescriptor);

        if (pthis->data() == MAP_FAILED)

#elif defined(WIN32_FILEMAPPING)

        HANDLE  lFileHdl = CreateFile(lFileName.c_str(),
                                      GENERIC_READ | GENERIC_WRITE,
                                      0,
                                      NULL,
                                      OPEN_ALWAYS,
                                      FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                                      NULL);

        if (lFileHdl != INVALID_HANDLE_VALUE)
        {
            HANDLE   lFileMapHdl = CreateFileMapping(lFileHdl,
                                                     NULL,
                                                     PAGE_READWRITE,
                                                     0,
                                                     pthis->fullSize(),
                                                     NULL);

            if  (lFileMapHdl != nullptr)
            {
                pthis->setData(MapViewOfFile(lFileMapHdl, FILE_MAP_WRITE, 0, 0, 0));
            }

            CloseHandle(lFileMapHdl); 
        }
        CloseHandle(lFileHdl);

        if (pthis->data() == nullptr)

#else

#   error "Aucune fonction syst�me n'est disponible pour impl�menter le file-mapping."

#endif

        {
            LAUNCH_EXCEPTION(kCouldntProjectTheFile)
        }
        else
        {
            // Assigne le pointeur this au d�but du bloc allou�.
            reinterpret_cast<FileMappedMemoryBlock**>(pthis->data())[0] = this;
        }
    }


    /*! Destructeur.
     */
    FileMappedMemoryBlock::~FileMappedMemoryBlock()
    {
#if defined(LINUX_FILEMAPPING)

        // D�projette les donn�es.
        if (pthis->data() != MAP_FAILED)
        {
            munmap(pthis->data(), pthis->fullSize());
        }

        // D�truit le fichier.
        unlink(pthis->fileName().c_str());

#elif defined(WIN32_FILEMAPPING)

        // D�projette les donn�es.
        if (pthis->data() != nullptr)
        {
            UnmapViewOfFile(pthis->data());
        }

        // D�truit le fichier.
        _unlink(pthis->fileName().c_str());

#else

#   error "Aucune fonction syst�me n'est disponible pour impl�menter le file-mapping."

#endif
    }


//------------------------------------------------------------------------------
//            File Mapped Memory Block : Acc�s aux donn�es projet�es
//------------------------------------------------------------------------------

    /*! Acc�s constant aux donn�es.
     */
    const void* FileMappedMemoryBlock::data() const
    {
        return static_cast<char*>(pthis->data()) + sizeof(FileMappedMemoryBlock*);
    }


    /*! Acc�s non-constant aux donn�es.
     */
    void* FileMappedMemoryBlock::data()
    {
        return static_cast<char*>(pthis->data()) + sizeof(FileMappedMemoryBlock*);
    }


    /*! Une partie des donn�es projet�es en m�moire est d�di�e � leur gestion, et n'est pas utilisable depuis l'�xt�rieur.
        @return La taille des donn�es (exploitables)
     */
    U32 FileMappedMemoryBlock::size() const
    {
        return pthis->fullSize() - U32(sizeof(FileMappedMemoryBlock*));
    }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @name Allocator<TT>::rebind<TU>::other
        Ce type est n�cessaire notamment pour les conteneurs qui ne manipulent pas directement en m�moire les types de donn�e qu'ils
        contiennent (e.g. une STL_Map(cl_Key, cl_Value) n'alloue pas directement des instances de cl_Key et de cl_Value, mais un type
        interm�diaire (un noeud d'arbre) qui pourra �tre allou� gr�ce � ce <b>typedef</b>).
     */
}
