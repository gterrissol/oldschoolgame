/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Task.hh"

/*! @file GDK/UTIlity/Task.cc
    @brief M�thodes (non-inline) de la classe UTI::Task.
    @author @ref Guillaume_Terrissol
    @date 25 Avril 2008 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de UTI::Task.
        @version 1.0
     */
    class Task::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
        Private();                                  //!< Constructeur par d�faut.
        //@}
        //! @name Objets enregsitr�s
        typedef SharedPtr<Callable> CallablePtr;    //!< Objet � sch�duler.
        typedef Vector<CallablePtr> Container;      //!< Collection d'objets � sch�duler.
        Container   mCallables;                     //!< Objets � sch�duler.
        bool        mIsEnabled;                     //!< Etat d'activation.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Task::Private::Private()
        : mCallables()
        , mIsEnabled(true)
    { }


//------------------------------------------------------------------------------
//                       Task : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Task::Task()
        : pthis()
    { }


    /*! Destructeur.
     */
    Task::~Task() { }


//------------------------------------------------------------------------------
//                                Task : Activit�
//------------------------------------------------------------------------------

    /*! Permet d'activer ou d�sactiver la t�che.
        @param pStatus VRAI pour activer la t�che, FAUX pour la d�sactiver
     */
    void Task::setEnabled(bool pStatus)
    {
        pthis->mIsEnabled = pStatus;
    }


    /*! @return VRAI si la t�che est activ�e, FAUX sinon
     */
    bool Task::isEnabled() const
    {
        return pthis->mIsEnabled;
    }


//------------------------------------------------------------------------------
//                      Task : Enregistrement & Mise � Jour
//------------------------------------------------------------------------------

    /*! Appelle, pour tous les objets enregistr�s, les m�thodes de traitement associ�es.
        @return VRAI si tous les appels effectu�s ont r�ussi, FAUX en cas d'(au moins un)�chec
        @note M�me en cas d'�chec, tous les objets enregistr�s seront trait�s
        @note Supprimer un objet devenu invalide ne force pas un retour � FAUX
     */
    bool Task::process()
    {
        bool    lSuccess = true;

        for(U32 lIndex = k0UL; lIndex < pthis->mCallables.size();)
        {
            Private::CallablePtr&   lPtr = pthis->mCallables[lIndex];

            if (lPtr->isValid())
            {
                bool    lResult = lPtr->process();
                lSuccess = lSuccess && lResult;
                ++lIndex;
            }
            else
            {
                // Retire l'objet.
                std::swap(pthis->mCallables[lIndex], pthis->mCallables.back());
                pthis->mCallables.pop_back();
                // Pas d'incr�ment : l'objet permut� doit aussi �tre trait�.
            }
        }

        return lSuccess;
    }


    /*! Enregistre un objet sch�dul� et sa m�thode.
        @param pCallable Composant encapsulant l'objet � sch�duler et sa m�thode de traitement
     */
    void Task::checkIn(SharedPtr<Callable>&& pCallable)
    {
        pthis->mCallables.push_back(pCallable);
    }


//------------------------------------------------------------------------------
//                            Callable : Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Task::Callable::~Callable() { }
}
