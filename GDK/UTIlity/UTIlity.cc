/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "UTIlity.hh"

/*! @file GDK/UTIlity/UTIlity.cc
    @brief D�finitions diverses du module @ref UTIlity.
    @author @ref Guillaume_Terrissol
    @date 24 Juin 2001 - 23 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace UTI
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlreadyProjectedData,       "Donn�es d�j� projet�es.",             "Already projected data.")
    REGISTER_ERR_MSG(kCouldntCreateFileToProject, "N'a pu cr�er de fichier � projeter.", "Couldn't create any file to project.")
    REGISTER_ERR_MSG(kCouldntProjectTheFile,      "N'a pu projeter le fichier.",         "Couldn't project the file.")
    REGISTER_ERR_MSG(kInvalidElementCount,        "Nombre d'�l�ments invalide.",         "Invalid element count.")
    REGISTER_ERR_MSG(kPathTooLong,                "Chemin trop long.",                   "Path too long.")
}
