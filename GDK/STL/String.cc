/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "String.hh"

/*! @file GDK/STL/String.cc
    @brief Fonctions (non-inline) relatives aux String.
    @author @ref Guillaume_Terrissol
    @date 8 Juillet 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <sstream>

#include "ERRor/Log.hh"

namespace std
{
//------------------------------------------------------------------------------
//                           Op�rateurs de Conversion
//------------------------------------------------------------------------------

    /*! Convertit un nombre exprim� sous-forme de cha�ne C++ en sa valeur num�rique.
        @param pText  Cha�ne d�crivant une valeur num�rique
        @param pValue R�f�rence sur l'instance � initialiser avec la valeur contenue dans <i>pText</i>
        @internal
     */
    template<class TType>
    void operator>>(const String& pText, MEM::BuiltIn<TType>& pValue)
    {
        std::basic_istringstream<char, std::char_traits<char>, MEM::Allocator<char>>    lSStream(pText);

        TType   lValue;
        lSStream >> lValue;
        pValue = MEM::BuiltIn<TType>(lValue);
    }


#ifndef NOT_FOR_DOXYGEN
    template    void operator>>(const String& pText, I8&  pValue);
    template    void operator>>(const String& pText, I16& pValue);
    template    void operator>>(const String& pText, I32& pValue);
    template    void operator>>(const String& pText, I64& pValue);
    template    void operator>>(const String& pText, U8&  pValue);
    template    void operator>>(const String& pText, U16& pValue);
    template    void operator>>(const String& pText, U32& pValue);
    template    void operator>>(const String& pText, U64& pValue);
    template    void operator>>(const String& pText, F32& pValue);
    template    void operator>>(const String& pText, F64& pValue);
#endif  // De NOT_FOR_DOXYGEN


    /*! Convertit un pointeur exprim� sous-forme de cha�ne C++ en sa valeur "num�rique".
        @param pText  Cha�ne d�crivant unpointeur
        @param pValue R�f�rence sur l'instance � initialiser avec la valeur contenue dans <i>pText</i>
        @internal
     */
    void operator>>(const String& pText, void*& pValue)
    {
        std::basic_istringstream<char, std::char_traits<char>, MEM::Allocator<char>>    lSStream(pText);

        lSStream >> pValue;
    }


    /*! Exprime sous forme de cha�ne de caract�res C++ une valeur num�rique.
        @param pText  Cha�ne dans laquelle �crire la valeur num�rique
        @param pValue R�f�rence sur la valeur dont �crire la repr�sentation dans <i>pText</i>
        @internal
     */
    template<class TType>
    void operator<<(String& pText, MEM::BuiltIn<TType> pValue)
    {
        std::basic_ostringstream<char, std::char_traits<char>, MEM::Allocator<char>>    lSStream;

        lSStream << pValue;

        pText += lSStream.str();
    }


#ifndef NOT_FOR_DOXYGEN
    template    void operator<<(String& pText, I8  pValue);
    template    void operator<<(String& pText, I16 pValue);
    template    void operator<<(String& pText, I32 pValue);
    template    void operator<<(String& pText, I64 pValue);
    template    void operator<<(String& pText, U8  pValue);
    template    void operator<<(String& pText, U16 pValue);
    template    void operator<<(String& pText, U32 pValue);
    template    void operator<<(String& pText, U64 pValue);
    template    void operator<<(String& pText, F32 pValue);
    template    void operator<<(String& pText, F64 pValue);
#endif  // De NOT_FOR_DOXYGEN


    /*! Exprime sous forme de cha�ne de caract�res C++ un pointeur.
        @param pText  Cha�ne dans laquelle �crire le pointeur
        @param pValue R�f�rence sur le pointeur dont �crire la repr�sentation dans <i>pText</i>
        @internal
     */
    void operator<<(String& pText, const void* pValue)
    {
        std::basic_ostringstream<char, std::char_traits<char>, MEM::Allocator<char>>    lSStream;

        lSStream << pValue;

        pText += lSStream.str();
    }


//------------------------------------------------------------------------------
//              Affichage d'une Cha�ne de Caract�re (String)
//------------------------------------------------------------------------------

    /*! Affichage d'une cha�ne de caract�res C++.
        @param pLog  Log sur lequel afficher <i>pText</i>
        @param pText Cha�ne de caract�res C++ � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const String& pText)
    {
        pLog << pText.c_str();

        return pLog;
    }
}
