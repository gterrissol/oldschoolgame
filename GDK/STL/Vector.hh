/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_VECTOR_HH
#define STL_VECTOR_HH

#include "STL.hh"

/*! @file GDK/STL/Vector.hh
    @brief Encapsulation de la classe std::vector.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <vector>

#include "MEMory/Allocator.hh"

    /*! @typedef Vector<TT>
        @ingroup STL
        Conteneur (d'�l�ments � acc�s al�atoire / vecteur) de la @ref STL_Page.
     */

template<class TT>
using Vector    = std::vector<TT, MEM::Allocator<TT>>;  //!< Classe std::vector.

#endif  // De STL_VECTOR_HH
