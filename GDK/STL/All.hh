/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_ALL_HH
#define STL_ALL_HH

/*! @file GDK/STL/All.hh
    @brief Interface publique du module @ref STL.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 1er Mai 2010
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.

    Ce fichier regroupe les déclarations de la plupart des containers de la STL.
 */

namespace STL   //! Encapsulation de la STL.
{
    /*! @namespace STL
        @version 0.8

        Dans la documentation, les macros d'encapsulation des conteneurs de la STL sont remplacées par
        leurs définitions, sans toutefois indiquer l'emploi de MEM::OAllocator, ceci dans un souci de
        lisibilité de la documentation (cf : @ref Map).
        @todo Quand C++0x sera là, remplacer les macros par des template typedef
     */

    /*! @defgroup STL STL : Composants de la bibliothèque standard.
        @b namespace @ref STL
     */


//------------------------------------------------------------------------------
//                         Documentation Supplémentaire
//------------------------------------------------------------------------------

    /*! @page STL_Page STL
        @version 0.5

        <b>Standard Template Library.</b><br>
        <br>
        <i>Conteneurs :</i><br>
        @ref Deque<br>
        @ref Map<br>
        @ref Vector<br>
        <i>Presque-conteneurs :</i><br>
        @ref String<br>
        @ref WString<br>
        <i>Propriétaire :</i><br>
        @ref StringList<br>
        @todo Ecrire une petite présentation de la Standard Template Library
    */
}


//------------------------------------------------------------------------------
//                    Encapsulation des Conteneurs de la STL
//------------------------------------------------------------------------------

#include "Deque.hh"
#include "Map.hh"
#include "String.hh"
#include "Vector.hh"
#include "StringList.hh"
#include "SharedPtr.hh"
#include "Shared.hh"
#include "Shared.tcc"
#include "Function.hh"
#include "Tuple.hh"
#include "Limits.hh"

#endif  // De STL_ALL_HH
