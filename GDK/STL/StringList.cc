/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "StringList.hh"

/*! @file GDK/STL/StringList.cc
    @brief Fonctions (non-inline) relatives aux String.
    @author @ref Guillaume_Terrissol
    @date 31 D�cembre 2007 - 1er F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Log.hh"
#include "MEMory/BuiltIn.hh"


    // Message d'assertion.
    REGISTER_ERR_MSG(kInvalidSeparator, "S�parateur invalide.", "Invalid separator.")


//------------------------------------------------------------------------------
//                          Op�rations sur des Cha�nes
//------------------------------------------------------------------------------

    /*! Fragmente une cha�ne selon un s�parateur arbitraire.
        @param pText Texte � fragmenter
        @param pSep  S�parateur (non vide) sur lequel baser la fragmentation de <i>pText</i>
        @return La liste des sous-cha�nes de <i>pText</i> qui �taient s�par�es par <i>pSep</i>
     */
    StringList StringList::split(const String& pText, const String& pSep)
    {
        ASSERT_EX(!pSep.empty(), kInvalidSeparator, return StringList();)

        StringList  lRes;

        for(decltype(pText.length()) lBegin{0}; lBegin < pText.length();)
        {
            // D�but d'une cha�ne recherch�e.
            lBegin  = U32(pText.find_first_not_of(pSep, lBegin));

            if (lBegin != String::npos)
            {
                // Longueur d'une cha�ne recherch�e.
                U32 lEndPos = U32(pText.find_first_of(pSep, lBegin));
                // Empilement.
                lRes.push_back(pText.substr(lBegin, lEndPos - lBegin));
                // Positionnement pour la recherche de la cha�ne suivante.
                lBegin = lEndPos;
            }
        }

        return lRes;
    }


    /*! Fusionne l'ensemble des cha�nes de l'instance.
        @param pSep S�parateur (�ventuellement vide) � ins�rer entre deux cha�ne � concat�ner
        @return L'ensemble des cha�nes de la liste, jointes par <i>pSep</i>
     */
    String StringList::merge(const String& pSep) const
    {
        // Il faut qu'il y ait des mots � fusionner...
        switch(size())
        {
            case 0:
                return "";
            case 1:
                return front();
            default:
            {
                StringList::const_iterator  lI      = begin();
                String                      lRes    = *lI;
                // La s�quence s�paratrice ne doit pas �tre introduite en fin
                // de cha�ne, mais seulement entre les mots de l'instance.
                for(++lI; lI != end(); ++lI)
                {
                    lRes += pSep + *lI;
                }

                return lRes;
            }
        }
    }


    /*! Cette m�thode permet de cr�er une liste de cha�nes en "cascade", sur une seule ligne de code.
        @param pList   Liste � laquelle ajouter une cha�ne.
        @param pString Cha�ne � ajouter � [la fin de] <i>pString</i>
        @ingroup STL
     */
    StringList& operator<<(StringList& pList, const String& pString)
    {
        pList.push_back(pString);

        return pList;
    }


//------------------------------------------------------------------------------
//                     Affichage d'une Liste de Cha�nes C++
//------------------------------------------------------------------------------

    /*! Affichage d'une liste de cha�nes de caract�res C++.
        @param pLog  Log sur lequel afficher <i>pText</i>
        @param pList Liste de cha�nes de caract�res C++ � afficher (un saut de ligne suit chaque cha�ne)
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const StringList& pList)
    {
        StringList::const_iterator  lEnd_1  = pList.end();
        StringList::const_iterator  lEnd    = lEnd_1--;
        
        for(auto lStringI = pList.begin(); lStringI != lEnd; ++lStringI)
        {
            pLog << (*lStringI);

            if (lStringI != lEnd_1)
            {
                pLog << '\n';
            }
        }

        return pLog;
    }

