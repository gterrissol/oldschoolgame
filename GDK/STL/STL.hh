/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_STL_hh
#define STL_STL_hh

/*! @file GDK/STL/STL.hh
    @brief Pr�-d�clarations du module @ref STL.
    @author @ref Guillaume_Terrissol
    @date 30 D�cembre 2007 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.

    Ce fichier regroupe les d�clarations de la plupart des containers de la STL.
 */

#if defined(__GNUC__)
#define BEFRIEND_STD_ALLOCATOR() template<class _Tp> friend class __gnu_cxx::new_allocator;
#else
#define BEFRIEND_STD_ALLOCATOR() template<class _Tp> friend class std::allocator;
#endif  // defined(__GNUC__)
#define BEFRIEND_MEM_ALLOCATOR() template<class TT> friend class MEM::Allocator;

#endif  // De STL_STL_hh
