/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_DEQUE_HH
#define STL_DEQUE_HH

#include "STL.hh"

/*! @file GDK/STL/Deque.hh
    @brief Encapsulation de la classe std::deque.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <deque>

#include "MEMory/Allocator.hh"

    /*! @typedef Deque<TT>
        @ingroup STL
        Conteneur (file � double acc�s) de la @ref STL_Page.
     */

template<class TT>
using Deque     = std::deque<TT, MEM::Allocator<TT>>;   //!< Classe std::deque.

#endif  // De STL_DEQUE_HH
