/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_SHAREDPTR_HH
#define STL_SHAREDPTR_HH

#include "STL.hh"

/*! @file GDK/STL/SharedPtr.hh
    @brief Disponibilit� des classes std::shared_ptr, std::unique_ptr & std::weak_ptr.
    @author @ref Guillaume_Terrissol
    @date 16 Mars 2008 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include "MEMory/Allocator.hh"

template<class TT>
using SharedPtr = std::shared_ptr<TT>;
template<class TT>
using UniquePtr = std::unique_ptr<TT, std::default_delete<TT>>;
template<class TT>
using WeakPtr   = std::weak_ptr<TT>;

namespace STL
{
//------------------------------------------------------------------------------
//                        Cr�ation Dynamique d'Instances
//------------------------------------------------------------------------------

    template<class TT, typename... TArgs>
    inline SharedPtr<TT> makeShared(TArgs&&... pArgs)
    {
        typedef typename std::remove_const<TT>::type    TTnc;
        return std::allocate_shared<TT>(MEM::Allocator<TTnc>(), std::forward<TArgs>(pArgs)...);
    }


//------------------------------------------------------------------------------
//                                  Null Delete
//------------------------------------------------------------------------------

    /*! @brief Deleter inactif pour shared_ptr.
        @version 1.0

        Passer une instance de cette classe lors de la construction d'un shared_ptr sur un pointeur
        quelconque permet de le manipuler comme un smart pointer avec les classes requi�rant de tels
        pointeurs.<br>
        La suppression est laiss� � la charge du programmeur.
        suppression par delete.
     */
    struct null_delete
    {
    /*! @brief Operateur fonction "vide".

        Cet op�rateur fonction ne fait rien (sinon permettre de ne pas d�truire l'instance lors de son
        "enregistrement" aupr�s d'un TR1::shared_ptr.
        @sa http://www.boost.org/libs/smart_ptr/sp_techniques.html#weak_without_shared
     */
        inline  void operator()(void const*) const { }
    };
}

#endif  // De STL_SHAREDPTR_HH
