/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_SHARED_TCC
#define STL_SHARED_TCC

#include "Shared.hh"

/*! @file GDK/STL/Shared.tcc
    @brief M�thodes inline de la classe STL::enable_shared_from_this.
    @author @ref Guillaume_Terrissol
    @date 13 Mai 2007 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "SharedPtr.hh"

namespace STL
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de STL::enable_shared_from_this.
        @version 1.0
     */
    template<class TP>
    class enable_shared_from_this<TP>::Private : public MEM::OnHeap
    {
    public:

        Private(enable_shared_from_this<TP>* pThis);    //!< Constructeur.

        SharedPtr<TP>   mThis;                          //!< Smart pointer vers instance propri�taire.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pThis Pointeur vers l'instance de enable_shared_from_this dont cette est instance est
        l'impl�mtation priv�e
     */
    template<class TP>
    enable_shared_from_this<TP>::Private::Private(enable_shared_from_this<TP>* pThis)
        : mThis(static_cast<TP*>(pThis)
        , STL::null_delete())
    { }


//------------------------------------------------------------------------------
//                                 Acc�s � this
//------------------------------------------------------------------------------

    /*! @return Un smart pointer (constant) sur l'instance
     */
    template<class TP>
    WeakPtr<TP const> enable_shared_from_this<TP>::shared_from_this() const
    {
        return pthis->mThis;
    }


    /*! @return Un smart pointer sur l'instance
     */
    template<class TP>
    WeakPtr<TP> enable_shared_from_this<TP>::shared_from_this()
    {
        return pthis->mThis;
    }


    /*! @return Un pointeur vers un smart pointer sur l'instance
     */
    template<class TP>
    SharedPtr<TP>* enable_shared_from_this<TP>::pointer_to_shared_from_this()
    {
        return &pthis->mThis;
    }


//------------------------------------------------------------------------------
//                          Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TP>
    enable_shared_from_this<TP>::enable_shared_from_this() : pthis(this) { }


    /*! Constructeur par copie.
        @note Le param�tre ne sera pas utilis�
     */
    template<class TP>
    enable_shared_from_this<TP>::enable_shared_from_this(const enable_shared_from_this<TP>&) : pthis(this) { }


    /*! Op�rateur d'affectation.
        @return L'instance
        @note Le param�tre ne sera pas utilis�
     */
    template<class TP>
    enable_shared_from_this<TP>& enable_shared_from_this<TP>::operator=(const enable_shared_from_this<TP>&)
    {
        return *this;
    }


    /*! Destructeur.
     */
    template<class TP>
    enable_shared_from_this<TP>::~enable_shared_from_this() { }
}

#endif  // De STL_SHARED_TCC
