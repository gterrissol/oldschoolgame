/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_FUNCTION_HH
#define STL_FUNCTION_HH

#include "STL.hh"

/*! @file GDK/STL/Function.hh
    @brief Disponibilité de std::function.
    @author @ref Guillaume_Terrissol
    @date 1er Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <functional>

using namespace std::placeholders;

#endif  // De STL_FUNCTION_HH
