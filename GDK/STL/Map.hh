/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_MAP_HH
#define STL_MAP_HH

#include "STL.hh"

/*! @file GDK/STL/Map.hh
    @brief Encapsulation de la classe std::map.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <map>

#include "MEMory/Allocator.hh"

    /*! @typedef Map<TKey, TValue>
        @ingroup STL
        Conteneur associatif (dictionnaire) de la @ref STL_Page.
     */

template<class TKey, class TValue>
using Map       = std::map<TKey, TValue, std::less<TKey>, MEM::Allocator<std::pair<TKey const, TValue>>>;   //!< Classe std::map.

#endif  // De STL_MAP_HH
