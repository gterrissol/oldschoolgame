/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_LIMITS_HH
#define STL_LIMITS_HH

#include "STL.hh"

/*! @file GDK/STL/Limits.hh
    @brief Spécialisation de std::numeric_limits pour les MEM::BuiltIn<>.
    @author @ref Guillaume_Terrissol
    @date 19 Mars 2009 - 27 Mars 2015
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <limits>

#include "MEMory/BuiltIn.hh"

namespace std
{
    template<class TType>
    struct numeric_limits<MEM::BuiltIn<TType>>
    {
 static const bool is_specialized = numeric_limits<TType>::is_specialized;

 static const int                   digits              = numeric_limits<TType>::digits;
 static const int                   digits10            = numeric_limits<TType>::digits10;
 static const bool                  is_signed           = numeric_limits<TType>::is_signed;
 static const bool                  is_integer          = numeric_limits<TType>::is_integer;
 static const bool                  is_exact            = numeric_limits<TType>::is_exact;
 static const int                   radix               = numeric_limits<TType>::radix;
 static const int                   min_exponent        = numeric_limits<TType>::min_exponent;
 static const int                   min_exponent10      = numeric_limits<TType>::min_exponent10;
 static const int                   max_exponent        = numeric_limits<TType>::max_exponent;
 static const int                   max_exponent10      = numeric_limits<TType>::max_exponent10;

 static const bool                  has_infinity        = numeric_limits<TType>::has_infinity;
 static const bool                  has_quiet_NaN       = numeric_limits<TType>::has_quiet_NaN;
 static const bool                  has_signaling_NaN   = numeric_limits<TType>::has_signaling_NaN;
 static const float_denorm_style    has_denorm          = numeric_limits<TType>::has_denorm;
 static const bool                  has_denorm_loss     = numeric_limits<TType>::has_denorm_loss;

 static const bool                  is_iec559           = numeric_limits<TType>::is_iec559;
 static const bool                  is_bounded          = numeric_limits<TType>::is_bounded;
 static const bool                  is_modulo           = numeric_limits<TType>::is_modulo;

 static const bool                  traps               = numeric_limits<TType>::traps;
 static const bool                  tinyness_before     = numeric_limits<TType>::tinyness_before;
 static const float_round_style     round_style         = numeric_limits<TType>::round_style;

 static MEM::BuiltIn<TType> min() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::min());
        }
 static MEM::BuiltIn<TType> max() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::max());
        }
 static MEM::BuiltIn<TType> epsilon() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::epsilon());
        }
 static MEM::BuiltIn<TType> round_error() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::round_error());
        }
 static MEM::BuiltIn<TType> infinity() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::infinity());
        }
 static MEM::BuiltIn<TType> quiet_NaN() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::quiet_NaN());
        }
 static MEM::BuiltIn<TType> signaling_NaN() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::signaling_NaN());
        }
 static MEM::BuiltIn<TType> denorm_min() throw()
        {
            return MEM::BuiltIn<TType>(numeric_limits<TType>::denorm_min());
        }
    };
}

#endif  // De STL_LIMITS_HH
