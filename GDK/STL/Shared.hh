/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_SHARED_HH
#define STL_SHARED_HH

#include "STL.hh"

/*! @file GDK/STL/Shared.hh
    @brief D�claration de la classe STL::enable_shared_from_this.
    @author @ref Guillaume_Terrissol
    @date 13 Mai 2007 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"

#include "SharedPtr.hh"

namespace STL
{
    /*! @brief Smart pointer sur this.
        @version 1.0
        @ingroup STL

        D�river de cette classe apporte deux m�thodes (shared_from_this()) permettant de r�cup�rer des
        smart pointers sur l'instance. A par la suite �t� rajout� une m�thode offrant un pointeur vers
        un smart pointer sur l'instance (un peu compliqu�, mais j'en avais besoin).
        @note Une classe de ce type est d�j� pr�sente dans std, mais il fallait que l'instance soit
        d�j� port�e par un smart pointer pour que les m�thodes shared_from_this() soient viables. Avec
        cette classe, pas de telle condition � respecter
     */
    template<class TP>
    class enable_shared_from_this
    {
    public:
        //! @name Acc�s � this
        //@{
        WeakPtr<TP const>   shared_from_this() const;                                           //!< ... sous forme de smart pointer constant.
        WeakPtr<TP>         shared_from_this();                                                 //!< ... sous forme de smart pointer.
        SharedPtr<TP>*      pointer_to_shared_from_this();                                      //!< ... via un pointeur vers un smart pointer.
        //@}

    protected:
        //! @name Constructeurs & destructeur
        //@{
                            enable_shared_from_this();                                          //!< Constructeur par d�faut.
                            enable_shared_from_this(const enable_shared_from_this&);            //!< Constructeur par copie.
                            enable_shared_from_this& operator=(const enable_shared_from_this&); //!< Op�rateur d'affectation.
                            ~enable_shared_from_this();                                         //!< Destructeur.
        //@}

    private:

        PIMPL()
    };
}

#endif  // De STL_SHARED_HH
