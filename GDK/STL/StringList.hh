/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_STRINGLIST_HH
#define STL_STRINGLIST_HH

#include "STL.hh"

/*! @file GDK/STL/StringList.hh
    @brief Encapsulation de la classe std::vector<std::string>.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 8 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Allocator.hh"

#include "String.hh"
#include "Vector.hh"

    /*! @brief Liste de cha�ne C++.
        @version 0.9
        @ingroup STL

        Liste (vector) de cha�nes de caract�res (String). Cette classe existe car ce type de conteneur
        est assez souvent employ�.<br>
        D�finir une classe m'a sembl� plus judicieux pour �tendre (mod�r�ment) l'interface de
        Vector(String).
     */
    class StringList : public Vector<String>
    {
    public:
        //! @name Constructeurs
        //@{
                    StringList() = default;                             //!< ... par d�faut.
                    StringList(const StringList&) = default;            //!< ... par copie.
        StringList& operator=(const StringList&) = default;             //!< Op�rateur d'affectation.
        //@}
        //! @name Op�rations sur des cha�nes
        //@{
 static StringList  split(const String& pText, const String& pSep);     //!< Fragmentation.
        String      merge(const String& pSep) const;                    //!< Fusion des cha�nes.
        //@}
    };


    StringList& operator<<(StringList& pList, const String& pString);   //!< Concat�nation.


//------------------------------------------------------------------------------
//                     Affichage d'une Liste de Cha�nes C++
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const StringList& pList);    //!< Ecriture d'une liste de cha�nec C++.


#endif  // De STL_STRINGLIST_HH
