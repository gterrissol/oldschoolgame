/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_TUPLE_HH
#define STL_TUPLE_HH

/*! @file GDK/STL/Tuple.hh
    @brief Disponibilit� des tuples.
    @author @ref Guillaume_Terrissol
    @date 31 D�cembre 2007 - 1er Mai 2010
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <tuple>

#endif  // De STL_TUPLE_HH
