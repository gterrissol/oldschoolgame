/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef STL_STRING_HH
#define STL_STRING_HH

#include "STL.hh"

/*! @file GDK/STL/String.hh
    @brief Encapsulation des classes std::string et std::wstring.
    @author @ref Guillaume_Terrissol
    @date 6 Octobre - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <string>

#include "MEMory/Allocator.hh"
#include "MEMory/BuiltIn.hh"

//! Classe std::string.
typedef std::basic_string<char, std::char_traits<char>, MEM::Allocator<char>>           String;

//! Classe std::wstring.
typedef std::basic_string<wchar_t, std::char_traits<wchar_t>, MEM::Allocator<wchar_t>>  WString;

    /*! @typedef String
        @ingroup STL
        Presque-conteneur (cha�ne de caract�res C++) de la @ref STL_Page.
     */
    /*! @typedef WString
        @ingroup STL
        Presque-conteneur (cha�ne de caract�res larges C++) de la @ref STL_Page.
     */

namespace std
{
//------------------------------------------------------------------------------
//                           Op�rateurs de Conversion
//------------------------------------------------------------------------------

    /*! @name Conversions
        Ces op�rateurs sont d�finis dans l'espace de noms std (je sais, c'est mal &copy;) pour permettre
        la d�finition d'autres operator>> ou operator<< dans d'autres namespace, sans se
        prendre la t�te (pour ceux qui ne voient pas ce dont je parle, cherchez des articles sur la
        "Koenig Lookup", la surcharge des op�rateurs,...).
     */
    //@{
    template<typename TType>
    void    operator>>(const String& pText, MEM::BuiltIn<TType>& pValue);   //!< 
    void    operator>>(const String& pText, void*& pValue);                 //!< 

    template<typename TType>
    void    operator<<(String& pText, MEM::BuiltIn<TType> pValue);          //!< 
    void    operator<<(String& pText, const void* pValue);                  //!< 
    //@}


//------------------------------------------------------------------------------
//                          Affichage d'une Cha�ne C++
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const String& pText);            //!< Ecriture d'une cha�ne C++.
}

#endif  // De STL_STRING_HH
