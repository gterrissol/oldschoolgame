/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseThread.hh"

/*! @file GDK/THRead/BaseThread.cc
    @brief M�thodes (non-inline) de la classe THR::Thread.
    @author @ref Guillaume_Terrissol
    @date 26 Ao�t 2002 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <chrono>
#include <thread>

#include "STL/Deque.hh"
#include "STL/Function.hh"

#include "Enum.hh"
#include "ErrMsg.hh"

namespace THR
{
    /*! Lorsq'un thread est en attente de callback ou doit �tre suspendu, une pause est marqu�e en
        attente d'une changement. Cette constante permet d'�tablir le d�lai de cette pause, en ms.
     */
    const std::chrono::milliseconds kWaitDelay(100);


//------------------------------------------------------------------------------
//                               P-Impl de Thread
//------------------------------------------------------------------------------
 
    /*! @brief P-Impl de THR::Thread.
        @version 0.8
        @internal
     */
    class Thread::Private : public OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Private();                      //!< Constructeur.
                    ~Private();                     //!< Destructeur.
        //@}
        //! @name Utilisation des callbacks
        //@{
        typedef Thread::CallBack  CallBack;         //!< Type callback.
        void        checkIn(CallBack pCallback);    //!< Enregistrement d'une callback.
        U32         callbackCount() const;          //!< Nombre de callbacks enregistr�es.
        CallBack    nextCallback();                 //!< Prochaine callback � ex�cuter.
        //@}
        //! @name Ex�cution du thread
 static void        execute(Private* pThis);        //!< Fonction thread�e.
        void        run();                          //!< Lancement du thread.
        void        suspend();                      //!< Suspension du thread.
        void        resume();                       //!< Reprise du thread.
        void        kill();                         //!< Meurtre du thread.
        void        stop();                         //!< Arr�t du thread.
        void        join();                         //!< "Jonction" avec le thread.
        bool        isSuspended() const;            //!< Thread suspendu ?
        bool        hasToProceed() const;           //!< Poursuite de l'ex�cution ?
        //@}

    private:
        //! @name Attributs
        //@{
        Deque<CallBack> mCallbacks;                 //!< Fonctions � appeler dans le thread.
        std::thread     mTheThread;                 //!< Thread.
        mutable Mutex   mTheMutex;                  //!< Mutex.
        bool            mIsSuspended;               //!< Etat suspendu.
        bool            mIsStopped;                 //!< Etat stopp�.
        //@}
    };


//------------------------------------------------------------------------------
//                 P-Impl de Thread : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Thread::Private::Private()
        : mCallbacks()
        , mTheThread()
        , mTheMutex()
        , mIsSuspended(false)
        , mIsStopped(false)
    { }


    /*! Destructeur.
     */
    Thread::Private::~Private()
    {
        join();
    }


//------------------------------------------------------------------------------
//                           Utilisation des Callbacks
//------------------------------------------------------------------------------

    /*! @copydoc THR::Thread::checkIn(CallBack pCallback)
     */
    void Thread::Private::checkIn(CallBack pCallback)
    {
        Lock    lLock(mTheMutex);

        mCallbacks.push_back(pCallback);
    }


    /*! @return Le nombre de callbacks enregistr�es qu'il reste � ex�cuter
     */
    U32 Thread::Private::callbackCount() const
    {
        Lock    lLock(mTheMutex);

        return U32(mCallbacks.size());
    }


    /*! Les callbacks sont plac�es dans une file lorsqu'elles sont enregistr�es. Cette m�thode permet de
        r�cup�rer la prochaine callback � ex�cuter (celle en d�but de file). Elle sera ensuite d�fil�e.
        @return La prochaine callback � ex�cuter (il doit y en avoir une)
     */
    Thread::Private::CallBack Thread::Private::nextCallback()
    {
        ASSERT(0UL < callbackCount(), kNoMoreCallbacks)

        CallBack    lCallBack;
        {
            Lock    lLock(mTheMutex);

            lCallBack = mCallbacks.front();
            mCallbacks.pop_front();
        }

        return lCallBack;
    }


//------------------------------------------------------------------------------
//                            Traitement des Signaux
//------------------------------------------------------------------------------

    /*! C'est cette fonction qui ex�cut�e dans un nouveau thread.<br>
        Son r�le est d'ex�cuter les callbacks dans leur ordre d'enregistrement (gestion par <b>FIFO</b>,
        jusqu'� ce que l'ordre d'arr�t (THR::eStop) soit donn�.
        @sa run, checkIn
     */
    void Thread::Private::execute(Private* pThis)
    {
        while(pThis->hasToProceed())
        {
            if (!pThis->isSuspended())
            {
                if (0UL < pThis->callbackCount())
                {
                    // R�cup�re la callback suivante et l'ex�cute.
                    pThis->nextCallback()();
                }
                else
                {
                    // S'il n a rien � faire, le thread se met en (l�ger) sommeil.
                    std::this_thread::sleep_for(kWaitDelay);
                }
            }
            else
            {
                // S'il n a rien � faire, le thread se met en (l�ger) sommeil.
                std::this_thread::sleep_for(kWaitDelay);
            }
        }

        // Au cas o� le signal eStop a �t� envoy�, il faut flusher la liste  de callback restantes.
        while(0UL < pThis->callbackCount())
        {
            pThis->nextCallback();
        }
    }


    /*! Lance le thread.
     */
    void Thread::Private::run()
    {
        join(); // Juste au cas o� le thread tournerait toujours.

        Lock    lLock(mTheMutex);
        mIsStopped = false;
        mTheThread = std::thread{std::bind(&Private::execute, this)};
    }


    /*! Suspend le lancement des callbacks.<br>
        @note Les appels ne sont pas comptabilis�s
        @note La suspension ne prendra effet qu'apr�s la fin de la callback en cours d'ex�cution
     */
    void Thread::Private::suspend()
    {
        Lock    lLock(mTheMutex);

        mIsSuspended = true;
    }


    /*! Reprend le lancement des callbacks apr�s une suspension.
        @note Reprendre alors que le thread n'est pas suspendu n'a pas d'effet
        @note La reprise n'aura lieu qu'apr�s que le timer interne sera arriv� � �ch�ance
     */
    void Thread::Private::resume()
    {
        Lock    lLock(mTheMutex);

        mIsSuspended = false;
    }



    /*! Stoppe l'ex�cution du thread : les callbacks restantes seront supprim�es
        @note La callback en cours d'ex�cution s'ach�vera toutefois
     */
    void Thread::Private::stop()
    {
        Lock    lLock(mTheMutex);

        mIsStopped = true;
    }


    /*! Stoppe l'ex�cution du thread et attend sa fin : les callbacks restantes seront supprim�es
        @note La callback en cours d'ex�cution s'ach�vera toutefois
     */
    void Thread::Private::join()
    {
        stop();                     // On arr�te.

        if (mTheThread.joinable())  // Et au besoin...
        {
            mTheThread.join();      // On attend.
        }
    }


    /*! @return VRAI si le thread est suspendu (i.e : le signal THR::eSuspend a �t� re�u, mais pas le
        THR::eResume r�ciproque), FAUX sinon
     */
    bool Thread::Private::isSuspended() const
    {
        Lock    lLock(mTheMutex);

        return mIsSuspended;
    }


    /*! @return VRAI si le thread est toujours en cours d'ex�cution, FAUX si le signal THR::eStop a �t�
        re�u
     */
    bool Thread::Private::hasToProceed() const
    {
        Lock    lLock(mTheMutex);

        return !mIsStopped;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Thread::Thread()
        : pthis()
    { }


    /*! Destructeur. Attend la fin de l'ex�cution du thread.
     */
    Thread::~Thread()
    {
        pthis->stop();
    }


//------------------------------------------------------------------------------
//                         Enregistrement des Commandes
//------------------------------------------------------------------------------

    /*! Enregistre une callback � ex�cuter dans le thread.
        @param pCallback Object fonction � ex�cuter, d�s que possible
     */
    void Thread::checkIn(CallBack pCallback)
    {
        pthis->checkIn(pCallback);
    }


//------------------------------------------------------------------------------
//                              Ex�cution du Thread
//------------------------------------------------------------------------------

    /*! Permet de communiquer avec le thread.
        @param pSignal Signal � prendre en compte par le thread
     */
    void Thread::signal(ESignals pSignal)
    {
        switch(pSignal)
        {
            case eStart:
                pthis->run();
                break;
            case ePause:
                pthis->suspend();
                break;
            case eResume:
                pthis->resume();
                break;
            case eStop:
                pthis->stop();
                break;
            case eJoin:
                pthis->join();
                break;
            default:
                WARNING(kUnknownSignal)
                break;
        }
    }
}
