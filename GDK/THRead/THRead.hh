/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_THREAD_HH
#define THR_THREAD_HH

/*! @file GDK/THRead/THRead.hh
    @brief Pr�-d�clarations du module THR.
    @author @ref Guillaume_Terrissol
    @date 1er Janvier 2008 - 13 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace THR
{
    class Thread;
}

#endif  // De THR_THREAD_HH
