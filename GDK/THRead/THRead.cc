/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "THRead.hh"

/*! @file GDK/THRead/THRead.cc
    @brief D�finitions diverses du module THR.
    @author @ref Guillaume_Terrissol
    @date 26 Ao�t 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace THR
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kUnknownSignal,   "Signal inconnu.",    "Unknown signal.")
    REGISTER_ERR_MSG(kNoMoreCallbacks, "Plus de callbacks.", "No more callbacks.")


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page THR_Threads_Page Utilisation des threads
        @todo A r�diger

        Ne pas allouer de m�moire dans les m�thodes thread�es
    */
}
