/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_ALL_HH
#define THR_ALL_HH

/*! @file GDK/THRead/All.hh
    @brief Interface publique du module THRead.
    @author @ref Guillaume_Terrissol
    @date 26 Ao�t 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"
#include "MEMory/MEMory.hh"
#include "STL/STL.hh"
#include "UTIlity/UTIlity.hh"

//------------------------------------------------------------------------------
//                                 Enum�rations
//------------------------------------------------------------------------------

#include "Enum.hh"


//------------------------------------------------------------------------------
//                              Messages d'Erreurs
//------------------------------------------------------------------------------

#include "ErrMsg.hh"


namespace THR   //! Gestion des threads.
{
    /*! @namespace THR
        @version 0.6

        Dans une application temps r�el, aucune action (m�me lourde) ne doit �tre bloquante. Il arrive
        dont qu'il faille effectuer certaines op�rations en t�che de fond. Ceci est r�alis�, au niveau
        syst�me, gr�ce aux threads.<br>
        Ce module propose une classe THR::Thread facilitant (du moins, je l'esp�re) l'emploi des threads
        pour parall�liser certaines t�ches.
        Consultez la page @ref THR_Threads_Page pour un exemple complet d'utilisation
     */

    /*! @defgroup THRead THRead : processus l�gers
        <b>namespace</b> THR.
     */
}

#include "Mutex.hh"
#include "BaseThread.hh"

#endif  // De THR_ALL_HH
