/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_BASETHREAD_HH
#define THR_BASETHREAD_HH

#include "THRead.hh"

/*! @file GDK/THRead/BaseThread.hh
    @brief En-t�te de la classe THR::Thread.
    @author @ref Guillaume_Terrissol
    @date 26 Ao�t 2002 - 7 F�vrier 2018
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/Function.hh"

#include "Enum.hh"
#include "Mutex.hh"

namespace THR
{
//------------------------------------------------------------------------------
//                                    Thread
//------------------------------------------------------------------------------

    /*! @brief Classe thread.
        @version 0.9
        @ingroup THRead

        Cette classe permet d'ex�cuter du code dans un nouveau thread, de mani�re "dynamique" (gr�ce aux
        callbacks).<br>
        Consultez la page @ref THR_Threads_Page pour un exemple complet d'utilisation.
     */
    class Thread : public MEM::OnHeap
    {
    public:
        //! @name Encapsulation des types de callbacks
        //@{
        typedef std::function<void ()>  CallBack;    //!< Type de la callback m�moris�e.
        //@}
        //! @name Contructeur & destructeur
        //@{
                Thread();                           //!< Constructeur par d�faut.
virtual         ~Thread();                          //!< Destructeur.
        //@}
        void    checkIn(CallBack pCallback);        //!< Enregistrement d'une callback.
        //! @name Ex�cution du thread
        //@{
        void    signal(ESignals pSignal);           //!< Envoie un signal au thread.
        //@}

    private:

        FORBID_COPY(Thread)
        PIMPL()
    };
}

#endif  // De THR_BASETHREAD_HH

