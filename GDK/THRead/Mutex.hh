/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_MUTEX_HH
#define THR_MUTEX_HH

#include "THRead.hh"

/*! @file GDK/THRead/Mutex.hh
    @brief En-t�te des classes THR::Mutex et THR::Synchronized & THR::Lock.
    @author @ref Guillaume_Terrissol
    @date 29 Ao�t 2002 - 6 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <mutex>


namespace THR
{
//------------------------------------------------------------------------------
//                                     Mutex
//------------------------------------------------------------------------------

    /*! @defgroup THR_Synchronize_Objects Objets de synchronsation
        @ingroup THRead
     */

    /*! @brief Mutex.
        @ingroup THR_Synchronize_Objects
        Mutex standard.
     */
    typedef std::recursive_mutex    Mutex;

    /*! @brief Verrou de mutex.
        @ingroup THR_Synchronize_Objects
        Mutex standard.
     */
    typedef std::lock_guard<Mutex>  Lock;
}

#endif  // De THR_MUTEX_HH
