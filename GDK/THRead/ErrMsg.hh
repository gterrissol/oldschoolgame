/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_ERRMSG_HH
#define THR_ERRMSG_HH

/*! @file GDK/THRead/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module THR.
    @author @ref Guillaume_Terrissol
    @date 26 Ao�t 2002 - 12 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace THR
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kUnknownSignal;
    extern  ERR::String kNoMoreCallbacks;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De THR_ERRMSG_HH
