/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef THR_ENUM_HH
#define THR_ENUM_HH

/*! @file GDK/THRead/Enum.hh
    @brief Enum�rations du module THR.
    @author @ref Guillaume_Terrissol
    @date 27 Ao�t 2002 - 11 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"

namespace THR
{
    //! Signaux : communication avec un thread lanc�.
    enum ESignals
    {
        eStart,     //!< Lance l'ex�cution du thread.
        ePause,     //!< Suspend temporairement l'ex�cution du thread.
        eResume,    //!< Reprend l'ex�cution du thread apr�s une pause.
        eStop,      //!< Stoppe l'ex�cution du thread (apr�s la callback en cours).
        eJoin,      //!< Stoppe un thread et attend la fin de son ex�cution.
    };
}

#endif // De THR_ENUM_HH
