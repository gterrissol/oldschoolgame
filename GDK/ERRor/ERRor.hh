/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_ERROR_HH
#define ERR_ERROR_HH

/*! @file ERRor/ERRor.hh
    @brief Pr�-d�clarations des modules @ref ERRor.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2007 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace ERR
{
#ifdef ASSERTIONS

    class Assertion;

#endif  // De ASSERTIONS

    class Exception;

#ifdef ASSERTIONS

    typedef const char* String;

#endif  // De ASSERTIONS
}

namespace LOG
{
    class Log;
}

#endif  // De ERR_ERROR_HH
