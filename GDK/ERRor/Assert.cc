/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Assert.hh"

/*! @file GDK/ERRor/Assert.cc
    @brief Fonctions d'assertions.
    @author @ref Guillaume_Terrissol
    @date 3 Juin 2001 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ErrMsg.hh"
#include "Log.hh"

#ifdef ASSERTIONS

namespace ERR
{
//------------------------------------------------------------------------------
//                           Assertion : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pFile     Fichier dans lequel l'assertion a �chou�
        @param pLine     Ligne du fichier <i>pFile</i> sur laquelle l'assertion a �chou�
        @param pFunction Nom de la fonction o� l'assertion a �chou�
        @param pMessage  Texte � afficher suite � l'�chec de l'assertion
        @note Les arguments de ce constructeurs seront directement assign�s aux attributs publics
     */
    Assertion::Assertion(const char* pFile, int pLine, const char* pFunction, String pMessage)
        : mFileName(pFile)
        , mLine(pLine)
        , mFunctionName(pFunction)
        , mMessage(pMessage)
    { }


//------------------------------------------------------------------------------
//                      Assertion : Gestion des Assertions
//------------------------------------------------------------------------------

    /*! D�termine la mani�re dont consid�rer une assertion.
        @param pAssertion POD contenant les informations relatives � une assertion ayant �chou�
        @return VRAI s'il y a besoin de d�boguer le code ayant fait �chouer l'assertion, FAUX si on peut
                passer outre et poursuivre l'ex�cution du programme
     */
    bool Assertion::debug(const Assertion& pAssertion)
    {
        // Si un "handler" personnalis� est d�fini, il est utilis� en priorit�.
        if (smCustomHandler != nullptr)
        {
            return smCustomHandler(pAssertion);
        }
        else
        {
            // Sinon, affiche simplement le message.
            LOG::Cerr << kInFile   << pAssertion.mFileName     << '\n'
                      << kLine     << pAssertion.mLine         << '\n'
                      << kFunction << pAssertion.mFunctionName << " : "
                                   << pAssertion.mMessage      << LOG::Endl;

            // Et demande le d�bogage du code.
            return true;
        }
    }


    /*! Activation d'un nouveau "gestionnaire" d'assertions.
     */
    void Assertion::setCustomHandler(Handler pNewHandler)
    {
        smCustomHandler = pNewHandler;
    }


    /*! R�activation du "gestionnaire" d'assertions par d�faut.
     */
    void Assertion::removeCustomHandler()
    {
        smCustomHandler = nullptr;
    }


//------------------------------------------------------------------------------
//                             Fonctions d'Affichage
//------------------------------------------------------------------------------

    /*! Affiche un message d'avertissement.
        @ingroup ERR_Assertions
        @param pMessage Texte � afficher
        @note Le texte est affich� sur le log d'erreur
     */
    void displayWarningMessage(String pMessage)
    {
        LOG::Cerr << kWarning << pMessage << LOG::Endl;
    }


//------------------------------------------------------------------------------
//                        Assertion : Attribut de Classe
//------------------------------------------------------------------------------

    /*! Pointeur d'une fonction (ou d'une m�thode de classe) destin�e � traiter l'�chec d'une assertion.
     */
    Assertion::Handler  Assertion::smCustomHandler  = nullptr;


//------------------------------------------------------------------------------
//                    Assertion : Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var Assertion::mFileName
        Nom du fichier dans lequel l'assertion a �chou�
     */
    /*! @var Assertion::mLine
        Ligne du fichier <b>mFileName</b> sur laquelle l'assertion a �chou�
     */
    /*! @var Assertion::mFunctionName
        Nom de la fonction o� l'assertion a �chou�
     */
    /*! @var Assertion::mMessage
        Texte � afficher suite � l'�chec de l'assertion
     */
}


//------------------------------------------------------------------------------
//                           Documentation des Macros
//------------------------------------------------------------------------------

    /*! @def ASSERT(pCondition, pMessage)
        @ingroup ERR_Assertions
        @param pCondition Condition � tester
        @param pMessage   Message d'erreur

        Si le test �choue (<i>pCondition</i> == FAUX), le message d'erreur <i>pMessage</i> sera affich�
        et le programme sera stopp� par un point d'arr�t.
        @sa ASSERT_EX
     */

    /*! @def ASSERT_EX(pCondition, pMessage, pInstructions)
        @ingroup ERR_Assertions
        @param pCondition    Condition � tester
        @param pMessage      Message d'erreur
        @param pInstructions Bloc d'instructions

        Si le test �choue (<i>pCondition</i> == FAUX), le message d'erreur <i>pMessage</i> sera affich�
        et le programme sera stopp� par un point d'arr�t. Si le programme poursuit son ex�cution (via un
        d�bogueur), le code <i>pInstructions</i> sera ex�cut�.
        @note <i>pInstruction</i> doit �tre termin� par ";"
        @sa ASSERT
     */

    /*! @def WARNING(pMessage)
        @ingroup ERR_Assertions
        @param pMessage Message � afficher

        Affiche un message d'avertissement sur le log d'erreur.
        @sa WARNING_IF
     */

    /*! @def WARNING_IF(pCondition, pMessage)
        @ingroup ERR_Assertions
        @param pCondition Condition � tester
        @param pMessage   Message � afficher

        Affiche un message d'avertissement sur le log d'erreur si le test r�ussit (<i>pCondition</i> ==
        VRAI).
        @note <b>Attention</b> : sur une assertion, un message sera affich� si le test �choue; pour un
        warning, le message sera affich� si le test est vrai
        @sa WARNING
     */

    /*! @def EXECUTE_ERROR_MANAGEMENT_CODE(pInstructions)
        @ingroup ERR_Assertions
        @param pInstructions Code � ex�cuter sous la condition que ASSERTIONS soit d�finie.<br>
        Cette macro permet l'ex�cution conditionnelle de code li�e � la gestion des erreurs. Elle �vite
        le recours � des blocs
        @code
    #ifdef ASSERTIONS
    ...
    #endif
        @endcode
        @note N'oubliez pas de terminer <i>pInstructions</i> par un ';' (qui ne sera pas ajout� par cette
        macro)
     */

#endif  // De ASSERTIONS
