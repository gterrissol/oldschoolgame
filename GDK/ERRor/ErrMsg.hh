/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_ERRMSG_HH
#define ERR_ERRMSG_HH

/*! @file GDK/ERRor/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs des modules ERR & LOG.
    @author @ref Guillaume_Terrissol
    @date 21 Avril 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor.hh"

namespace ERR
{
    /*! @defgroup ERR_Messages Messages d'erreur
        @ingroup ERRor
     */

#ifdef ASSERTIONS

    //! @name Type des cha�nes de caract�res d�crivant les erreurs et autres avertissements.
    //@{
#if defined(FRANCAIS) || defined(ENGLISH)
    typedef const char* String; //!< Cha�ne de caract�res C pour un message en fran�ais ou en anglais.
#else
#   error "Le type 'cha�ne de message d'erreur' n'est pas d�fini."
#endif
    //@}

    /*! Langues disponibles pour les messages d'erreur.
        @ingroup ERR_Messages
     */
    enum ELanguages
    {
        eFr,        //!< Fran�ais.
        eEn,        //!< Anglais.
        eLangCount  //!< Nombre de langues disponibles.
    };

    /*! @defgroup ERR_Message_Recording Enregistrement des messages d'erreur
        @ingroup ERR_Messages
     */
    //@{
    /*! @brief D�finition d'un message d'erreur.
        @version 1.0
        @internal

        L'enregistrement via une classe plut�t que par une fonction est possible avant l'appel � main(),
        automatiquement, gr�ce � la simple construction d'une instance (dans la m�moire statique). Il n'y
        a ainsi pas besoin d'appeler de fonction d'initialisation.
     */
    struct RegisterErrMsg
    {
        RegisterErrMsg(String& pErrMsg, String pMsgFr, String pMsgEn);  //!< Constructeur.
    };
    //! D�finition d'un message d'erreur.
#   define REGISTER_ERR_MSG(pErrMsg, pMsgFr, pMsgEn)              \
    ERR::String pErrMsg = "Untranslated error message";             \
    ERR::RegisterErrMsg gRegister##pErrMsg(pErrMsg, pMsgFr, pMsgEn);
    //@}

    /*! @defgroup ERR_Message_Language Changement de la langue des messages d'erreur
        @ingroup ERR_Messages
     */
    //@{
    /*! @brief Changement de langue.
        @version 1.0
        @internal

        La d�finition de la langue via une classe plut�t que par une fonction est possible avant l'appel
        � main(), automatiquement, gr�ce � la simple construction d'une instance (dans la m�moire
        statique). Il n'y a ainsi pas besoin d'appeler de fonction d'initialisation.
        @note N'�tait la d�finition intiale de la langue (avant l'appel � main()), une fonction aurait pu
        suffire
     */
    struct SetLanguage
    {
        SetLanguage(ELanguages pLang);  //!< Constructeur.
    };
    //! Changement de langue.
#   define SET_ERR_MSG_LANGUAGE(pLang)  \
    ERR::SetLanguage    gSet##pLang(ERR::pLang);
    //@}

    //! @name Messages d'assertion
    //@{
    extern  String  kFunction;
    extern  String  kInFile;
    extern  String  kLine;
    extern  String  kUnsafeAssert;
    extern  String  kWarning;
    //@}

#else

    /*! @defgroup ERR_Message_Recording Enregistrement des messages d'erreur
        @ingroup ERR_Messages
     */
    //@{
    //! D�finition d'un message d'erreur.
#   define REGISTER_ERR_MSG(pErrMsg, pMsgFr, pMsgEn)
    //@}
    /*! @defgroup ERR_Message_Language Changement de la langue des messages d'erreur
        @ingroup ERR_Messages
     */
    //@{
    //! Changement de langue.
#   define SET_ERR_MSG_LANGUAGE(pLang)
    //@}

#endif  // De ASSERTIONS
}


namespace LOG
{
#ifdef ASSERTIONS

    //! @name Message d'assertion
    //@{
    extern  ERR::String kInvalidStream;
    extern  ERR::String kUnknownType;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De ERR_ERRMSG_HH
