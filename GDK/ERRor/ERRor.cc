/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ERRor.hh"

/*! @file GDK/ERRor/ERRor.cc
    @brief D�finitions diverses des modules @ref ERRor.
    @author @ref Guillaume_Terrissol
    @date 4 F�vrier 2004 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <map>
#include <utility>

#include "Assert.hh"

namespace ERR
{
//------------------------------------------------------------------------------
//                D�finition des Messages d'Erreur et d'Assertion
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kFunction,     "Fonction ",                "Function ")
    REGISTER_ERR_MSG(kInFile,       "Dans le fichier ",         "In file ")
    REGISTER_ERR_MSG(kLine,         "Ligne ",                   "Line ")
    REGISTER_ERR_MSG(kUnsafeAssert, "Assertion non-s�curis�e.", "Unsafe assertion.")
    REGISTER_ERR_MSG(kWarning,      "Avertissement : ",         "Warning : ")
}


namespace LOG
{
//------------------------------------------------------------------------------
//                D�finition des Messages d'Erreur et d'Assertion
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidStream, "Flux invalide.",  "Invalid stream.")
    REGISTER_ERR_MSG(kUnknownType,   "Type inconnu : ", "Unknown type : ")
}


namespace ERR
{
//------------------------------------------------------------------------------
//                         Gestion des Messages d'Erreur
//------------------------------------------------------------------------------

#ifdef ASSERTIONS

    namespace
    {
        /*! Permet de r�cup�rer l'ensemble des messages d'erreur traduits pour une langue donn�e.
            @internal
            @param pLang Langue pour laquelle r�cup�rer les messages d'erreur
            @return Tous les messages d'erreur traduits pour la langue <i>pLang</i>
            @note Une fonction est n�cessaire pour s'assurer que les dictionnaires soient bien construits
            (dans la m�moire statique) avant leur utilisation
         */
        std::map<String*, String>& errMsgs(ELanguages pLang)
        {
            static  std::map<String*, String>   sErrMsgs[eLangCount];

            return sErrMsgs[pLang];
        }
    }


    /*! Enregistre la traduction d'un message d'erreur.
        @param pErrMsg Message d'erreur 
        @param pMsgFr  Traduction de <i>pErrMsg</i> en fran�ais
        @param pMsgEn  Traduction de <i>pErrMsg</i> en anglais
        @note Cette fonction est appel�e avant m�me l'appel � main; LOG::Cout et LOG::Cerr ne sont m�me
        pas encore construits; aucun message d'erreur ne sera �mis en cas d'�chec. Attention, donc
     */
    RegisterErrMsg::RegisterErrMsg(String& pErrMsg, String pMsgFr, String pMsgEn)
    {
        std::map<String*, String>&  lErrMsgsFr  = errMsgs(eFr);
        if (lErrMsgsFr.find(&pErrMsg) == lErrMsgsFr.end())
        {
            lErrMsgsFr[&pErrMsg] = pMsgFr;
        }

        std::map<String*, String>&  lErrMsgsEn  = errMsgs(eEn);
        if (lErrMsgsEn.find(&pErrMsg) == lErrMsgsEn.end())
        {
            lErrMsgsEn[&pErrMsg] = pMsgEn;
        }
    }


    /*! Bascule les messages d'erreur dans une autre langue.
        @param pLang Langue dans laquelle afficher les messages d'erreur
        @note L'appel � cette m�thode est d'autant plus co�teux que les messages d'erreur sont nombreux
     */
    SetLanguage::SetLanguage(ELanguages pLang)
    {
        if (pLang < eLangCount)
        {
            std::map<String*, String>&  lMsgs   = errMsgs(pLang);

            for(auto lMsgI = lMsgs.begin(); lMsgI != lMsgs.end(); ++lMsgI)
            {
                // Assigne chaque traduction enregistr�e au message d'erreur correspondant.
                *lMsgI->first = lMsgI->second;
            }
        }
    }

#endif  // De ASSERTIONS

//------------------------------------------------------------------------------
//                           Documentation des Macros
//------------------------------------------------------------------------------

    /*! @def BREAK_POINT()
        Place un point d'arr�t (disponible seulement - <i>pour l'instant</i> - sur architecture x86).
     */

    /*! @def REGISTER_ERR_MSG(pErrMsg, pMsgFr, pMsgEn)
        @param pErrMsg Cha�ne de caract�res associ�e au message d'erreur utilis�e dans le code
        @param pMsgFr  Tradution fran�aise de <i>pErrMsg</i>
        @param pMsgEn  Tradution anglaise de <i>pErrMsg</i>
        Cette macro :
        - d�finit la variable <i>pErrMsg</i> en lui assignant une cha�ne par d�faut sp�cifiant que le
        message associ� n'a pas de traduction,
        - instancie RegisterErrMsg � l'aide des 3 m�mes param�tres.

        Elle doit �tre utilis�e afin de ne laisser aucune r�f�rence aux m�canismes de gestion d'erreur
        lorsque la macro ASSERTIONS n'est pas d�finie.
     */

    /*! @def SET_ERR_MSG_LANGUAGE(pLang)
        @param pLang Langue dans laquelle afficher les messages d'erreur du moteur (ERR::eFr ou
        ERR::eEn)
        Cette macro se contente d'instancier SetLanguage. Elle doit cependant �tre utilis�e afin de ne
        laisser aucune r�f�rence aux m�canismes de gestion d'erreur lorsque la macro ASSERTIONS n'est
        pas d�finie.
     */


//------------------------------------------------------------------------------
//                         Documentation suppl�mentaire
//------------------------------------------------------------------------------

    /*! @typedef ERR::String
        La gestion multilingue des erreurs peut sembler curieuse, la documentation et les commentaires
        �tant en fran�ais, mais certains messages d'erreurs seront, par la suite, affich�s dans les
        �diteurs, qui sont, eux, r�ellement multilingues.<br>
        @par Usage typique des ERR::String
        Chaque module (qui peut g�n�rer des messages d'erreurs) poss�de un fichier ErrMsg.hh. Tous les
        messages d'erreurs y sont d�clar�s :
        @code
        extern  ERR::String kSomethingFailed;
        @endcode
        Chaque cha�ne est ensuite d�finie dans le fichier principal du module (MODule.cc) :
        @code
        REGISTER_ERR_MSG(kSomethingFailed, "Quelque chose a �chou�.", "Something failed.")
        @endcode
        Vous pouvez consulter les fichiers disponibles dans help/module pour toute pr�cision sur la mise
        en forme, la documentation,...
        @par Choix de la langue
        La langue � utiliser pour les messages peut �tre choisie ainsi :
        @code
        SET_ERR_MSG_LANGUAGE(eFr)   // Choix de langue : fran�ais.
        @endcode
        @note Par convention, les messages d'erreur commencent par une majuscule et se terminent par un
        point (comme une phrase, quoi)
     */
}
