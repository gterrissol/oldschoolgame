/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LOG_LOG_HH
#define LOG_LOG_HH

#include "ERRor.hh"

/*! @file GDK/ERRor/Log.hh
    @brief En-t�te de la classe LOG::Log.
    @author @ref Guillaume_Terrissol
    @date 21 F�vrier 2002 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace LOG
{
    /*! @defgroup LOG_Log_Interface Log
        @ingroup ERRor
     */
    /*! @brief Classe de base pour les "log"s.
        @version 2.0
        @ingroup LOG_Log_Interface

        Deux logs sont disponibles : sortie "standard" et sortie des erreurs. Ils s'utilisent comme
        std::cout et std::endl (via les op�rateurs <<).<br>
        Des "manipulateurs" sont disponibles : Endl, pour vider les tampons (comme std::endl),
        Start(const char*) et Finish() pour la gestion de groupe (voir Markup).<br>
        Pour "�tendre" le syst�me de logs (permettre l'�criture de nouveaux types), il suffit de
        d�clarer (puis d�finir) des fonctions du type :
        @code
namespace MOD
{
    LOG::Log&   operator<<(LOG::Log& pLog, const MyClass& pToPrint);  // !< Ecriture d'une MyClass.
}
@endcode
        En effet, la m�thode template operator<<(TType) est d�finie dans Log.cc et n'est instanci�e,
        explicitement, que pour les types de base. L'�criture dans un log de la valeur d'un objet
        complexe doit donc reposer sur ces instanciations.<br>
        Pour modifier la gestion de l'affichage (rediriger les sorties vers  un widget, par exemple), il
        faut appeler Plug() avec une instance d'une classe d�riv�e d'OutStream (Unplug() rebranche sur
        les sorties standard).
     */
    class Log
    {
    public:

        class OutStream;
        //! @name Constructeur & destructeur
        //@{
                    Log(OutStream* pStream);                    //!< Constructeur.
virtual             ~Log();                                     //!< Destructeur.
        //@}
        //! @name Ecriture 
        //@{
        template<typename TType>
        Log&        operator<<(TType pToPrint);                 //!< ... d'un type quelconque.
        template<typename TType>
        Log&        operator<<(TType* pToPrint);                //!< ... d'un pointeur.
        //@}
        //! @name Redirection
        //@{
        /*! @brief Classe de base pour les flux de sortie.
            @version 1.1

            "Classe d'interface" permettant, via ses d�riv�es, l'impl�mentation de l'�criture des donn�es
            des logs sur les sorties (�cran, widget,...).
         */
        class OutStream
        {
            friend class Log;                                   // Pour l'acc�s � Print, Flush et Markup.

        public:
            //! @name Constructeur & Destructeur
            //@{
                    OutStream();                                //!< Constructeur par d�faut.
    virtual         ~OutStream();                               //!< Destructeur.
        //@}

        private:
            //! @name Affichage
            //@{
    virtual void    print(bool pBool) = 0;                      //!< ... d'un bool�en.
    virtual void    print(char pChar) = 0;                      //!< ... d'un caract�re.
    virtual void    print(signed char pInt) = 0;                //!< ... d'un entier 8 bits sign�.
    virtual void    print(unsigned char pInt) = 0;              //!< ... d'un entier 8 bits non-sign�.
    virtual void    print(signed short pInt) = 0;               //!< ... d'un entier 16 bits sign�.
    virtual void    print(unsigned short pInt) = 0;             //!< ... d'un entier 16 bits non-sign�.
    virtual void    print(signed long pInt) = 0;                //!< ... d'un entier 32 bits sign�.
    virtual void    print(unsigned long pInt) = 0;              //!< ... d'un entier 32 bits non-sign�.
    virtual void    print(signed long long pInt) = 0;           //!< ... d'un entier 64 bits sign�.
    virtual void    print(unsigned long long pInt) = 0;         //!< ... d'un entier 64 bits non-sign�.
    virtual void    print(signed int pInt) = 0;                 //!< ... d'un entier sign�.
    virtual void    print(unsigned int pInt) = 0;               //!< ... d'un entier non-sign�.
    virtual void    print(float pFloat) = 0;                    //!< ... d'un r�el simple pr�cision.
    virtual void    print(double pFloat) = 0;                   //!< ... d'un r�el double pr�cision.
    virtual void    print(const char* pString) = 0;             //!< ... d'une cha�ne de caract�res C.
    virtual void    print(const void* pPointer) = 0;            //!< ... d'un pointeur.
    virtual void    flush() = 0;                                //!< Vidage du buffer.
    virtual void    markup(const char* pName) = 0;              //!< D�but ou fin d'un groupe.
            //@}
            //! @name Branchement
            //@{
            void    attach(Log* pLog);                          //!< D�rivation d'un log.
            Log*    mLog;                                       //!< Log d�riv�.
            //@}
         };

        void    plug(OutStream* pRedirect);                     //!< Utilisation d'un flux personnalis�.
        void    unplug();                                       //!< R�utilisation du flux standard.
        //@}

    private:
        // Copie d�sactiv�e.
                Log(const Log&) = delete;
        Log&   operator=(const Log&) = delete;
        //! @name Flux
        //@{
        OutStream*  mOutStream;                                 //!< ... standard.
        OutStream*  mPluggedStream;                             //!< ... personnalis�.
        //@}
    };


//------------------------------------------------------------------------------
//                                 Manipulateurs
//------------------------------------------------------------------------------

    /*! @brief Classe "fin de ligne".
        @version 1.0
        @ingroup LOG_Log_Interface

        Cette classe est utilis�e, tout comme std::endl, pour vider le tampon interne du flux (et pas
        seulement pour revenir � la ligne).
        @warning LOG::Endl ferme toutes les balises ouvertes
        @sa Endl
     */
    struct EndL { };


    /*! @brief Groupement de texte sur un log.
        @version 1.0
        @ingroup LOG_Log_Interface

        Il est possible de grouper une partie de l'affichage entre "balises" (hi�rarchiques). Les
        instances de cette classe jouent le r�le de ces balises.
        @sa Start, Finish
     */
    struct Markup
    {
        //! @name Constructeurs
        //@{
explicit            Markup();                                   //!< Fin de groupe.
explicit            Markup(const char* pName);                  //!< D�but de groupe.
        //@}
        const char* mName;                                      //!< Nom du groupe.
    };


//------------------------------------------------------------------------------
//                             Logs & Manipulateurs
//------------------------------------------------------------------------------

    /*! @defgroup LOG_global Variables globales et manipulateurs
        @ingroup LOG_Log_Interface
     */
    //@{
extern  LOG::Log    Cout;                                       //!< Log de sortie standard.
extern  LOG::Log    Cerr;                                       //!< Log de sortie des erreurs.
extern  LOG::EndL   Endl;                                       //!< Fin de ligne.
        LOG::Markup Start(const char* pName);                   //!< Balise de d�but de groupe.
extern  LOG::Markup Finish;                                     //!< Balise de fin de groupe.
    //@}
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Log.inl"

#endif  // De LOG_LOG_HH
