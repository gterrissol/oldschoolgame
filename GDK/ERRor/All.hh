/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_ALL_HH
#define ERR_ALL_HH

/*! @file GDK/ERRor/All.hh
    @brief Interface publique des modules @ref ERRor.
    @author @ref Guillaume_Terrissol
    @date 3 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace ERR   //! Gestion des erreurs.
{
    /*! @namespace ERR
        @version 2.0
        @ingroup ERRor

        Ce namespace fournit quelques outils pour la gestion des erreurs et le d�bogage.<br>
        Des macros sont disponibles : ASSERT, ASSERT_EX, WARNING, WARNING_IF.
        @note L'en-t�te ERRor.hh doit �tre inclus par les en-t�tes principaux de tous les autres modules
     */

    /*! @defgroup ERRor ERRor & LOG : Gestion des erreurs et Logs
        <b>namespace</b> ERR (et macros associ�es) et LOG.
     */
}


namespace LOG   //! Gestion des sorties standard.
{
    /*! @namespace LOG
        @version 2.0
        @ingroup ERRor

        Ce namespace permet l'affichage format� de donn�es, en proposant une syntaxe proche des flux C++.
        @sa Cout, Cerr, Endl, Start, Finish
     */
}

#include "Assert.hh"
#include "Debug.hh"
#include "Exception.hh"
#include "Log.hh"

#endif  // De ERR_ALL_HH
