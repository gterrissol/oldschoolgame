/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Exception.hh"

/*! @file GDK/ERRor/Exception.cc
    @brief M�thodes (non-inline) de la classe ERR::Exception.
    @author @ref Guillaume_Terrissol
    @date 23 Ao�t 2003 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Log.hh"

namespace ERR
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.<br>
        Dans la plupart des cas, ce constructeur ne devrait �tre appel� qu'avec une vraie cha�ne C,
        allou�e statiquement (typiquement une ERR::String, d�finie dans le fichier ErrMsg.hh du module).
        @param pMsg Message d'erreur d�crivant la cause de l'exception (� charge de l'utilisateur de
        l'afficher)
     */
    Exception::Exception(const char* pMsg) throw()
        : std::exception()
        , mMessage(pMsg)
    { }


    /*! Destructeur.
     */
    Exception::~Exception() throw() { }


//------------------------------------------------------------------------------
//                                 Autre m�thode
//------------------------------------------------------------------------------

    /*! @return Le message d'erreur
     */
    const char* Exception::what() const throw() 
    {
        return mMessage;
    }


//------------------------------------------------------------------------------
//                     Affichage du Message d'une Exception
//------------------------------------------------------------------------------

    /*! Affiche le message d'une exception sur un log.
        @ingroup ERR_Exception_Interface
        @param pLog Log sur lequel afficher le message de <i>pE</i>
        @param pE   Exception "� afficher"
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const Exception& pE)
    {
        pLog << pE.what();

        return pLog;
    }

//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var ERR::Exception::mMessage
        Ce module �tant la base, la classe ERR::Exception devait rester simple et petite, et ne pas
        utiliser d'en-t�te standard trop volumineux (i.e. pas &ltstring&gt), afin de ne pas p�naliser la
        compilation des autres modules simples (MATH, MEM, UTI,...).
     */


//------------------------------------------------------------------------------
//                           Documentation de la Macro
//------------------------------------------------------------------------------

    /*! @def LAUNCH_EXCEPTION(pMessage)
        @param pMessage Message d'information port� par l'exception � lancer
        La plupart des exception lanc�es dans le moteur sont des instances de ERR::Exception (tr�s
        rarement de ses d�riv�es). En r�gle g�n�rale, le message pass� en param�tre du constructeur sera
        une instance de ERR::String, qui n'existe donc que lorsque la macro ASSERTIONS est d�finie. La
        macro LAUNCH_EXCEPTION(pMessage) permet donc de lancer une exception dans tous les cas : si la
        cha�ne est d�finie, elle sera transmise au constructeur de l'exception; sinon, c'est une cha�ne
        vide qui sera transmise. Ceci afin de conserver les exceptions m�me lorsque les assertions (et
        les messages associ�s) sont d�sactiv�es.
     */
}
