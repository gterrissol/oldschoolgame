/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Log.hh"

/*! @file GDK/ERRor/Log.cc
    @brief M�thodes (non-inline) de la classe LOG::Log.
    @author @ref Guillaume_Terrissol
    @date 21 F�vrier 2002 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <iostream>
#include <string>
#include <vector>

#include "Assert.hh"
#include "ErrMsg.hh"
#include "Exception.hh"

namespace LOG
{
//------------------------------------------------------------------------------
//                                 Manipulateur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : balise fermante.
        @warning Utiliser LOG::Endl ferme toutes les balises ouvertes
     */
    Markup::Markup()
        : mName("")
    { }


    /*! Constructeur 
        @param pName Nom de la balise � ouvrir
     */
    Markup::Markup(const char* pName)
        : mName(pName)
    { }


//------------------------------------------------------------------------------
//                                     Flux
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Log::OutStream::OutStream()
        : mLog(nullptr)
    { }


    /*! Destructeur.
     */
    Log::OutStream::~OutStream()
    {
        if (mLog != nullptr)
        {
            mLog->unplug();
        }
    }


    /*! Lie le flux � un log (pour
     */
    void Log::OutStream::attach(Log* pLog)
    {
        mLog = pLog;
    }


    /*! @fn void Log::OutStream::print(bool)
        Affichage d'un bool�en.
        @param pBool Bool�en � afficher
     */

    /*! @fn void Log::OutStream::print(char pChar)
        Affichage d'un caract�re.
        @param pChar Caract�re � afficher
     */

    /*! @fn void Log::OutStream::print(signed char pInt)
        Affichage d'un entier 8 bits sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(unsigned char pInt)
        Affichage d'un entier 8 bits non-sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(signed short pInt)
        Affichage d'un entier 16 bits sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(unsigned short pInt)
        Affichage d'un entier 16 bits non-sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(signed long pInt)
        Affichage d'un entier 32 bits sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(unsigned long pInt)
        Affichage d'un entier 32 bits non-sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(signed long long pInt)
        Affichage d'un entier 64 bits sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(unsigned long long pInt)
        Affichage d'un entier 64 bits non-sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(signed int pInt)
        Affichage d'un entier sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(unsigned int pInt)
        Affichage d'un entier non-sign�.
        @param pInt Entier � afficher
     */

    /*! @fn void Log::OutStream::print(float pFloat)
        Affichage d'un flottant simple pr�cision.
        @param pFloat Flottant � afficher
     */

    /*! @fn void Log::OutStream::print(double pFloat)
        Affichage d'un flottant double pr�cision.
        @param pFloat Flottant � afficher
     */

    /*! @fn void Log::OutStream::print(const char* pString)
        Affichage d'une cha�ne de caract�res C.
        @param pString Cha�ne C � afficher
     */

    /*! @fn void Log::OutStream::print(const void* pPointer)
        Affichage (en h�xad�cimal) d'un pointeur.
        @param pPointer Pointeur � afficher
     */

    /*! @fn void Log::OutStream::flush()
        Vide les tampons (et revient � la ligne).
        @note Si des balises �taient ouvertes, elles vont �tre ferm�es
     */

    /*! @fn void Log::OutStream::markup(const char* pName)
        Ouvre ou ferme une balise.
        @param pName Nom de la balise � ouvrir si non-nul, ferme la derni�re balise ouverte sinon
     */


//------------------------------------------------------------------------------
//                        Log : Constructeur& Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur
        @param pStream Flux � utiliser pour l'�criture des donn�es de log
        @note <i>pStream</i> ne doit pas �tre nul
     */
    Log::Log(OutStream* pStream)
        : mOutStream(pStream)
        , mPluggedStream(nullptr)
    {
        ASSERT(pStream != nullptr, kInvalidStream)
    }


    /*! Destructeur.
     */
    Log::~Log() { }


//------------------------------------------------------------------------------
//                    Log : M�thodes El�mentaires d'Ecriture
//------------------------------------------------------------------------------

    /*! Fin de ligne.
        @par Param�tres
        N'importe quelle instance de EndL peut �tre utilis�e, m�me si LOG::Endl est fournie pour offir
        une �criture proche de celle des flux de la STL
        @return L'instance
     */
    template<>
    Log& Log::operator<<(EndL)
    {
        if (mPluggedStream == nullptr)
        {
            mOutStream->flush();
        }
        else
        {
            mPluggedStream->flush();
        }

        return *this;
    }


    /*! D�but ou fin de groupe.
       @param pMarkup Le titre du groupe.
       @return L'instance 
     */
    template<>
    Log& Log::operator<<(Markup pMarkup)
    {
        if (mPluggedStream == nullptr)
        {
            mOutStream->markup(pMarkup.mName);
        }
        else
        {
            mPluggedStream->markup(pMarkup.mName);
        }

        return *this;
    }


    /*! Affichage d'une valeur.
        @param pToPrint Variable dont afficher la valeur
        @return L'instance
     */
    template<typename TType>
    Log& Log::operator<<(TType pToPrint)
    {
        if (mPluggedStream == nullptr)
        {
            mOutStream->print(pToPrint);
        }
        else
        {
            mPluggedStream->print(pToPrint);
        }

        return *this;
    }


    // Instanciations explicites pour les types int�gr�s.
#ifndef NOT_FOR_DOXYGEN
    template    Log& Log::operator<<(bool pToPrint);
    template    Log& Log::operator<<(char pToPrint);
    template    Log& Log::operator<<(signed char pToPrint);
    template    Log& Log::operator<<(unsigned char pToPrint);
    template    Log& Log::operator<<(signed short pToPrint);
    template    Log& Log::operator<<(unsigned short pToPrint);
    template    Log& Log::operator<<(signed long pToPrint);
    template    Log& Log::operator<<(unsigned long pToPrint);
    template    Log& Log::operator<<(signed long long pToPrint);
    template    Log& Log::operator<<(unsigned long long pToPrint);
    template    Log& Log::operator<<(signed int pToPrint);
    template    Log& Log::operator<<(unsigned int pToPrint);
    template    Log& Log::operator<<(float pToPrint);
    template    Log& Log::operator<<(double pToPrint);
    template    Log& Log::operator<<(const char* pToPrint);
#endif  // De NOT_FOR_DOXYGEN


//------------------------------------------------------------------------------
//                               Log : Redirection
//------------------------------------------------------------------------------

    /*! Permet de rediriger les donn�es envoy�es sur l'instance vers un autre flux.
        @param pRedirect Nouveau flux � utiliser pour l'affichage
        @note <i>pRedirect</i> est simplement "port�" par l'instance, et ne sera pas supprim�
     */
    void Log::plug(OutStream* pRedirect)
    {
        mPluggedStream = pRedirect;
        mPluggedStream->attach(this);
    }


    /*! R�active le flux standard pour l'affichage des donn�es.
     */
    void Log::unplug()
    {
        mPluggedStream->attach(nullptr);
        mPluggedStream = nullptr;
    }


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var Log::mOutStream
        Flux standard utilis� pour l'affichage des donn�es envoy�es sur le log.
     */
    /*! @var Log::mPluggedStream
        Flux personnalis� effectuant l'affichage des donn�es envoy�es sur le log � la place de
        <i>mOutStream</i>.
     */


//------------------------------------------------------------------------------
//                                 Flux Standard
//------------------------------------------------------------------------------

    /*! @brief Impl�mentation standard des flux
        @version 1.0
        @internal

        Une instance de cette classe se charge de faire suivre � un flux standard STL des donn�es �
        afficher.
     */
    class StandardStream : public Log::OutStream
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                StandardStream(std::ostream& pStream);  //!< Constructeur.
virtual         ~StandardStream();                      //!< Destructeur.
        //@}

    private:
        //! @name Affichage
        //@{
virtual void    print(bool pBool);                      //!< ... d'un bool�en.
virtual void    print(char pChar);                      //!< ... d'un caract�re.
virtual void    print(signed char pInt);                //!< ... d'un entier 8 bits sign�.
virtual void    print(unsigned char pInt);              //!< ... d'un entier 8 bits non-sign�.
virtual void    print(signed short pInt);               //!< ... d'un entier 16 bits sign�.
virtual void    print(unsigned short pInt);             //!< ... d'un entier 16 bits non-sign�.
virtual void    print(signed long pInt);                //!< ... d'un entier 32 bits sign�.
virtual void    print(unsigned long pInt);              //!< ... d'un entier 32 bits non-sign�.
virtual void    print(signed long long pInt);           //!< ... d'un entier 64 bits sign�.
virtual void    print(unsigned long long pInt);         //!< ... d'un entier 64 bits non-sign�.
virtual void    print(signed int pInt);                 //!< ... d'un entier sign�.
virtual void    print(unsigned int pInt);               //!< ... d'un entier non-sign�.
virtual void    print(float pFloat);                    //!< ... d'un r�el simple pr�cision.
virtual void    print(double pFloat);                   //!< ... d'un r�el double pr�cision.
virtual void    print(const char* pString);             //!< ... d'une cha�ne de caract�res C.
        void    print(const std::string& pString);      //!< ... d'une cha�ne C++.
virtual void    print(const void* pPointer);            //!< ... d'un pointeur.
virtual void    flush();                                //!< Vidage du buffer.
virtual void    markup(const char* pName);              //!< D�but ou fin d'un groupe.

        std::vector<std::string>    mMarkupStack;       //!< Liste de balises ouvertes.
        std::ostream&               mStream;            //!< Flux STL.
    };


//------------------------------------------------------------------------------
//                  Flux Standard : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pStream Flux STL vers lequel envoyer les donn�es � afficher
     */
    StandardStream::StandardStream(std::ostream& pStream)
        : mMarkupStack()
        , mStream(pStream)
    { }


    /*! Destructeur.
     */
    StandardStream::~StandardStream() { }


    /*! @copydoc LOG::Log::OutStream::print(bool)
        @note C'est la valeur C++ de <i>pBool</i> qui sera affich�e (ie: true ou false)
     */
    void StandardStream::print(bool pBool)
    {
        mStream << (pBool ? "true" : "false");
    }


    /*! @copydoc Log::OutStream::print(char)
     */
    void StandardStream::print(char pChar)
    {
        mStream << pChar;
    }


    /*! @copydoc Log::OutStream::print(signed char)
     */
    void StandardStream::print(signed char pInt)
    {
        mStream << static_cast<int>(pInt);
    }


    /*! @copydoc Log::OutStream::print(unsigned char)
     */
    void StandardStream::print(unsigned char pInt)
    {
        mStream << static_cast<unsigned int>(pInt);
    }


    /*! @copydoc Log::OutStream::print(signed short)
     */
    void StandardStream::print(signed short pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(unsigned short)
     */
    void StandardStream::print(unsigned short pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(signed long)
     */
    void StandardStream::print(signed long pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(unsigned long)
     */
    void StandardStream::print(unsigned long pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(signed long long)
     */
    void StandardStream::print(signed long long pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(unsigned long long)
     */
    void StandardStream::print(unsigned long long pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(signed int)
     */
    void StandardStream::print(signed int pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(unsigned int)
     */
    void StandardStream::print(unsigned int pInt)
    {
        mStream << pInt;
    }


    /*! @copydoc Log::OutStream::print(float)
     */
    void StandardStream::print(float pFloat)
    {
        mStream << pFloat;
    }


    /*! @copydoc Log::OutStream::print(double)
     */
    void StandardStream::print(double pFloat)
    {
        mStream << pFloat;
    }


    /*! @copydoc Log::OutStream::print(const char*)
     */
    void StandardStream::print(const char* pString)
    {
        mStream << pString;
    }


    /*! Affichage d'une cha�ne de caract�res C++.
        @param pString Cha�ne C++ � afficher
     */
    void StandardStream::print(const std::string& pString)
    {
        mStream << pString;
    }


    /*! @copydoc Log::OutStream::print(const void*)
     */
    void StandardStream::print(const void* pPointer)
    {
        mStream << pPointer;
    }


    /*! @copydoc Log::OutStream::flush()
     */
    void StandardStream::flush()
    {
        for(auto lMarkupI = mMarkupStack.rbegin(); lMarkupI != mMarkupStack.rend(); ++lMarkupI)
        {
            print(*lMarkupI);
        }

        mMarkupStack.clear();
        mStream << std::endl;
    }


    /*! @copydoc Log::OutStream::markup(const char*)
     */
    void StandardStream::markup(const char* pName)
    {
        if ((pName == nullptr) || (pName[0] == '\0'))
        {
            if (!mMarkupStack.empty())
            {
                print(mMarkupStack.back());
                mMarkupStack.pop_back();
            }
            // else, pas d'affichage d'erreur (pas � ce niveau).
        }
        else
        {
            mMarkupStack.push_back(std::string("</") + pName + ">");
            print(std::string("<") + pName + ">");
        }
    }


//------------------------------------------------------------------------------
//                               Flux "par D�faut"
//------------------------------------------------------------------------------

    // Flux standard.
    StandardStream  gStdOut(std::cout); //!< @internal
    StandardStream  gStdErr(std::cerr); //!< @internal

    // Flux et manipulateurs
    //! @details Log de sortie standard (<b>std::cout</b>-like).
    Log             Cout(&gStdOut);
    //! @details Log de sortie d'erreur (<b>std::cerr</b>-like).
    Log             Cerr(&gStdErr);
    //! @details "Manipulateur" pour "flusher" les flux (<b>std::endl</b>-like).
    EndL            Endl;
    //! @details Fin de groupe.
    Markup          Finish;


    /*! Marque le d�but d'un groupe.
        @param pName Nom du groupe � ouvrir
     */
    Markup Start(const char* pName)
    {
        return Markup(pName);
    }
}
