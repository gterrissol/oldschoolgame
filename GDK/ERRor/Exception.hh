/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_EXCEPTION_HH
#define ERR_EXCEPTION_HH

#include "ERRor.hh"

/*! @file GDK/ERRor/Exception.hh
    @brief En-t�te de la classe ERR::Exception.
    @author @ref Guillaume_Terrissol
    @date 3 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <exception>

namespace ERR
{
    /*! @defgroup ERR_Exception_Interface Exception
        @ingroup ERRor
     */

    /*! @brief Classe exception de base.
        @version 2.0
        @ingroup ERR_Exception_Interface

        Les exceptions g�n�r�es par le moteur devraient d�river de cette classe.
     */
    class Exception : public std::exception
    {
    public:
        //! @name Constructeur & destructeur
        //@{
explicit            Exception(const char* pMsg) throw();    //!< Constructeur.
virtual             ~Exception() throw();                   //!< Destructeur.
        //@}
virtual const char* what() const throw();                   //!< R�cup�ration du message d'erreur.

    private:

        const char* mMessage;                               //!< Message d'erreur.
    };

#ifdef ASSERTIONS

    /*! Lancement d'une exception.
        @ingroup ERR_Exception_Interface
     */
#   define LAUNCH_EXCEPTION(pMessage) \
    throw ERR::Exception(pMessage);

#else   // ifndef ASSERTIONS

    /*! Lancement d'une exception.
        @ingroup ERR_Exception_Interface
     */
#   define LAUNCH_EXCEPTION(pMessage) \
    throw ERR::Exception("");

#endif  // De ASSERTIONS


//------------------------------------------------------------------------------
//                      Ecriture du Message d'une Exception
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const Exception& pE);    //!< Ecriture d'une exception.
}

#endif  // De ERR_EXCEPTION_HH
