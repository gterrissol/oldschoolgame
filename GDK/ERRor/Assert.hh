/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_ASSERT_HH
#define ERR_ASSERT_HH

#include "ERRor.hh"

/*! @file GDK/ERRor/Assert.hh
    @brief D�finition de macros d'assertion.
    @author @ref Guillaume_Terrissol
    @date 3 Juin 2001 - 7 F�vrier 2018
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Debug.hh"
#include "ErrMsg.hh"
#include "Exception.hh"

namespace ERR
{
    /*! @defgroup ERR_Assertions Assertions
        @ingroup ERRor
     */

#ifdef ASSERTIONS

    /*! @brief Assertion.
        @version 1.0
        @ingroup ERR_Assertions

        Cette classe peut presque �tre consid�r�e comme un POD, n'�tait la possibilit� de d�finition d'un
        gestionnaire d'assertions personnalis� (voir SetCustomHandler(Handler)).

        @note Cette classe est destin�e � un usage purement interne et ne doit pas �tre utilis�e
        explicitement
     */
    class Assertion
    {
    public:
        //! @name Interface de POD
        //@{
                Assertion(const char* pFile,
                          int         pLine,
                          const char* pFunction,
                          String      pMessage);        //!< Constructeur.

        const char* mFileName;                          //!< Nom de fichier.
        int         mLine;                              //!< Num�ro de ligne.
        const char* mFunctionName;                      //!< Nom de fonction (ou de m�thode).
        String      mMessage;                           //!< Message � caract�re informatif...
        //@}
        //! @name Gestion des assertions
        //@{
        typedef bool (*Handler)(const Assertion&);      //!< Type d'un "gestionnaire" d'assertions.

 static bool    debug(const Assertion& pAssertion);     //!< Prise en compte d'une assertion.
 static void    setCustomHandler(Handler pNewHandler);  //!< Activation d'un nouveau "gestionnaire".
 static void    removeCustomHandler();                  //!< R�activation du "gestionnaire" par d�faut.
        //@}

    private:

 static Handler smCustomHandler;                        //!< Gestionnaire d'assertions "personnalis�".
    };


    void    displayWarningMessage(String pMessage);     //!< Avertissement.


    //! Assertion simple.
#   define ASSERT(pCondition, pMessage)                                     \
    {                                                                       \
        if (!(pCondition))                                                  \
        {                                                                   \
            if (ERR::Assertion::debug(ERR::Assertion(__FILE__,              \
                                                     __LINE__,              \
                                                     __PRETTY_FUNCTION__,   \
                                                     pMessage)))            \
            {                                                               \
                BREAK_POINT()                                               \
            }                                                               \
            throw ERR::Exception(ERR::kUnsafeAssert);                       \
        }                                                                   \
    }


    //! Assertion simple (sans exception).
#   define ASSERT_NOTHROW(pCondition, pMessage)                             \
    {                                                                       \
        if (!(pCondition))                                                  \
        {                                                                   \
            if (ERR::Assertion::debug(ERR::Assertion(__FILE__,              \
                                                     __LINE__,              \
                                                     __PRETTY_FUNCTION__,   \
                                                     pMessage)))            \
            {                                                               \
                BREAK_POINT()                                               \
            }                                                               \
        }                                                                   \
    }


    //! Assertion et ex�cution de code (pour blinder).
#   define ASSERT_EX(pCondition, pMessage, pInstructions)                   \
    {                                                                       \
        if (!(pCondition))                                                  \
        {                                                                   \
            if (ERR::Assertion::debug(ERR::Assertion(__FILE__,              \
                                                     __LINE__,              \
                                                     __PRETTY_FUNCTION__,   \
                                                     pMessage)))            \
            {                                                               \
                BREAK_POINT()                                               \
            }                                                               \
            pInstructions                                                   \
        }                                                                   \
    }


    //! Avertissement (simple affichage d'un message).
#   define WARNING(pMessage)                    \
    {                                           \
        ERR::displayWarningMessage(pMessage);   \
    }


    //! Avertissement (affichage d'un message) sous condition.
#   define WARNING_IF(pCondition, pMessage)         \
    {                                               \
        if (pCondition)                             \
        {                                           \
            ERR::displayWarningMessage(pMessage);   \
        }                                           \
    }

    //! Ex�cution de code de gestion d'erreur.
#   define EXECUTE_ERROR_MANAGEMENT_CODE(pInstructions) \
    {                                                   \
        pInstructions                                   \
    }

#else   // ifndef ASSERTIONS

    //! Assertion simple.
#   define ASSERT(pCondition, pMessage)
    //! Assertion simple (sans exception).
#   define ASSERT_NOTHROW(pCondition, pMessage)
    //! Assertion et ex�cution de code (pour blinder).
#   define ASSERT_EX(pCondition, pMessage, pInstructions)
    //! Avertissement (simple affichage d'un message).
#   define WARNING(pMessage)
    //! Avertissemnt (affichage d'un message) sous condition.
#   define WARNING_IF(pCondition, pMessage)
    //! Ex�cution de code de gestion d'erreur.
#   define EXECUTE_ERROR_MANAGEMENT_CODE(pInstructions)

#endif  // De ASSERTIONS
}

#endif  // De ERR_ASSERT_HH
