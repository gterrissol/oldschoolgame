/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef ERR_DEBUG_HH
#define ERR_DEBUG_HH

/*! @file GDK/ERRor/Debug.hh
    @brief D�finition d'un point d'arr�t.
    @author @ref Guillaume_Terrissol
    @date 3 Juin 2001 - 11 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace ERR
{
    /*! @defgroup ERR_Breakpoint Point d'arr�t
        @ingroup ERRor
     */
    //@{
#ifdef X86
#   ifndef BREAK_POINT
#       define BREAK_POINT() __asm__ __volatile__("INT3");  //!< Point d'arr�t exploitable par un d�bogueur.
#   endif
#else   // ifndef X86
#   error "Architecture non-prise en compte."
#endif  // De X86
    //@}
}

#endif  // De ERR_DEBUG_HH
