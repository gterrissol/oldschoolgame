/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GDK/ERRor/Log.inl
    @brief M�thodes inline de la classe LOG::Log.
    @author @ref Guillaume_Terrissol
    @date 21 F�vrier 2002 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace LOG
{
//------------------------------------------------------------------------------
//                           
//------------------------------------------------------------------------------

    /*! Cette sp�cialisation partielle permet d'�crire la valeur de n'importe quel pointeur.
        @param pToPrint Pointeur dont afficher la valeur dans un log
     */
    template<typename TType>
    Log& Log::operator<<(TType* pToPrint)
    {
        if (mPluggedStream == nullptr)
        {
            mOutStream->print(pToPrint);
        }
        else
        {
            mPluggedStream->print(pToPrint);
        }

        return *this;
    }
}
