/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file EXTernals/org.libsdl.cc
    @brief D�finition de la classe SDL::Activator.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2011 - 1er F�vrier 2015
    @note This file is distributed under the GPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Define.hh"

MINIMAL_ACTIVATOR(SDL)
