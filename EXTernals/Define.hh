/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EXT_DEFINE_HH
#define EXT_DEFINE_HH

#include <iostream>

#include <OSGi/Activator.hh>
#include <OSGi/Context.hh>

/*! @file External/Define.hh
    @brief Macro g�n�rique pour la d�finition d'un activateur.
    @author @ref Guillaume_Terrissol
    @date 22 Ao�t 2011 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#define MINIMAL_ACTIVATOR(Component) \
namespace Component \
{ \
    class Activator : public OSGi::Activator \
    { \
    public: \
                Activator() : OSGi::Activator() { } \
virtual         ~Activator() { } \
    private: \
virtual void    doStart(OSGi::Context* pContext) override \
                { \
                    pContext->out() << "Starting "#Component << std::endl; \
                } \
virtual void    doStop(OSGi::Context* pContext) override \
                { \
                    pContext->out() << "Stopping "#Component << std::endl; \
                } \
    }; \
} \
 \
OSGI_REGISTER_ACTIVATORS() \
    OSGI_DECLARE_ACTIVATOR(Component::Activator) \
OSGI_END_REGISTER_ACTIVATORS()

#endif  // De EXT_DEFINE_HH
