/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QMAT_CONTROLLER_HH
#define QMAT_CONTROLLER_HH

#include "QMATerial.hh"

/*! @file IDE/QMATerial/Controller.hh
    @brief En-t�te des classes MAT::ImageController, MAT::TextureController & MAT::MaterialController.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATum/Controller.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATerial/Image.hh"
#include "MATerial/Texture.hh"

namespace MAT
{
    /*! @brief Contr�leur d'mage.
        @version 0.2
     */
    class ImageController : public DAT::GenericController<MAT::Image>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        ImageController(TEditedPtr pImage);                //!< Constructeur.
virtual                 ~ImageController();                                //!< Destructeur.
        //@}
        unsigned short  width() const;                                      //!< Largeur.
        unsigned short  height() const;                                     //!< Hauteur.
        void            format(EFormat pPixelFormat);                       //!< D�finition du format.
        void            setWidth(unsigned short pWidth);                    //!< Redimensionnement (largeur).
        void            setHeight(unsigned short pHeight);                  //!< Redimensionnement (hauteur).
        void            setLine(unsigned short pLine, const void* pData);   //!< D�finition des donn�es d'une ligne de l'image.
        void            bind();                                             //!< Cr�ation de la texture OpenGL.

    private:
virtual QT::QSubject*   asSubject();                                        //!< R�cup�ration du sujet d'�dition.
virtual RendererPtr    getRenderer();                                      //!< R�cup�ration du rendrerer du sujet �dit�.
    };


    /*! @brief Contr�leur de texture.
        @version 0.2
     */
    class TextureController : public DAT::GenericController<MAT::Texture>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                TextureController(TEditedPtr pTexture);    //!< Constructeur.
virtual         ~TextureController();                      //!< Destructeur.
        //@}
    };


    /*! @brief Contr�leur de mat�riau.
        @version 0.2
     */
    class MaterialController : public DAT::GenericController<MAT::Material>
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                MaterialController(TEditedPtr pMaterial);  //!< Constructeur par d�faut.
virtual         ~MaterialController();                     //!< Destructeur.
        //@}
    };
}

#endif  // De QMAT_CONTROLLER_HH
