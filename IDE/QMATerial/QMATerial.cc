/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QMATerial.hh"

/*! @file IDE/QMATerial/QMATerial.cc
    @brief D�finitions diverses du module QMAT.
    @author @ref Guillaume_Terrissol
    @date 28 Ao�t 2006 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QMAT
{
}
