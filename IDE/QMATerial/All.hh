/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QMAT_QMATERIAL_HH
#define QMAT_QMATERIAL_HH

/*! @file IDE/QMATerial/QMATerial.hh
    @brief Interface publique du module @ref QMAT.
    @author @ref Guillaume_Terrissol
    @date 28 Ao�t 2006 - 7 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QMAT  //! Editeurs de mat�riaux.
{
    /*! @namespace QMAT
        @version 0.3

     */
}

#include "Controller.hh"

#endif  // De QMAT_QMATERIAL_HH
