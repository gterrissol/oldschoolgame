/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/QMATerial/Controller.cc
    @brief M�thodes (non-inline) des classes QMAT::ImageController, QMAT::TextureController &
    QMAT::MaterialController.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 22 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/RGBA.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATerial/Image.hh"
#include "MATerial/Texture.hh"
#include "MATH/Const.hh"
#include "QT/QT.hh"

namespace MAT
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    ImageController::ImageController(TEditedPtr pImage)
        : DAT::GenericController<MAT::Image>(pImage)
    {
        // Mode �diteur : conservation des donn�es apr�s le bind OpenGL.
        pImage.lock()->keepDataAfterBind(true);
    }


    /*! Destructeur.
     */
    ImageController::~ImageController() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    unsigned short ImageController::width() const
    {
        return edited().lock()->width();
    }


    /*!
     */
    unsigned short ImageController::height() const
    {
        return edited().lock()->height();
    }


    /*!
     */
    void ImageController::format(EFormat pPixelFormat)
    {
        edited().lock()->format(pPixelFormat);
    }


    /*!
     */
    void ImageController::setWidth(unsigned short pWidth)
    {
        edited().lock()->setWidth(U16(pWidth));
    }


    /*!
     */
    void ImageController::setHeight(unsigned short pHeight)
    {
        edited().lock()->setHeight(U16(pHeight));
    }


    /*!
     */
    void ImageController::setLine(unsigned short pLine, const void* pData)
    {
        edited().lock()->setLine(U16(pLine), pData);
    }


    /*!
     */
    void ImageController::bind()
    {
        edited().lock()->bind();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QT::QSubject* ImageController::asSubject()
    {
        return nullptr;
    }


    /*!
     */
    ImageController::RendererPtr ImageController::getRenderer()
    {
        return RendererPtr();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    TextureController::TextureController(TEditedPtr pTexture)
        : DAT::GenericController<MAT::Texture>(pTexture)
    { }


    /*! Destructeur.
     */
    TextureController::~TextureController() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    MaterialController::MaterialController(TEditedPtr pMaterial)
        : DAT::GenericController<MAT::Material>(pMaterial)
    {
        pMaterial.lock()->setAmbient( CLR::RGBA(k0F, k0F, k0F, k0F));
        pMaterial.lock()->setDiffuse( CLR::RGBA(k1F, k1F, k1F, k1F));
        pMaterial.lock()->setSpecular(CLR::RGBA(k0F, k0F, k0F, k1F));
        pMaterial.lock()->setEmissive(CLR::RGBA(k0F, k0F, k0F, k1F));
    }


    /*! Destructeur.
     */
    MaterialController::~MaterialController() { }
}
