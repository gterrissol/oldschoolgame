/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/Memento/Container.cc
    @brief M�thodes (non-inline) de la classe Memento::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Assert.hh"

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"

namespace Memento
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @page category_memento Permanence des donn�es de jeu.
        @todo A r�diger
     */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Container::Container(OSGi::Context* pContext)
        : CORE::Container("memento", tr("Save"), QStringLiteral(":/ide/memento/icons/Save"), pContext)
    { }


    /*!
     */
    Container::~Container() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("memento.container", [=]() { return CORE::UIService::Widget{new Container(pContext)}; });

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->declare(CORE::Synchronizer("memento.undo").enter("positive memento.undo").leave("null memento.undo"));
                    lSyncService->declare(CORE::Synchronizer("memento.redo").enter("positive memento.redo").leave("null memento.redo"));
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("memento.container");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(Memento::Activator)
OSGI_END_REGISTER_ACTIVATORS()
