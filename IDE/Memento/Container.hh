/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MEMENTO_CONTAINER_HH
#define MEMENTO_CONTAINER_HH

/*! @file IDE/Memento/Container.hh
    @brief En-t�te de la classe Memento::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Container.hh"

namespace Memento
{
    /*! @brief Cat�gorie @ref category_memento "Sauvegarde".
        @version 0.2
     */
    class Container : public CORE::Container
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
        Container(OSGi::Context* pContext); //!< Constructeur.
virtual ~Container();                       //!< Destructeur.
        //@}
    };
}

#endif  // MEMENTO_CONTAINER_HH
