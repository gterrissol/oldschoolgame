/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/Program/Container.cc
    @brief M�thodes (non-inline) de la classe Program::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QTabWidget>

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/MenuService.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/String.hh"

#include "Editor.hh"

namespace Program
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @page category_program Edition des scripts.
        @todo A r�diger
     */

//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export d'�tat de jeu.
        @version 0.25
     */
    class ImportExportGame : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportGame();                                                //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!< Export.
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< Enregistrement de donn�es minimales.
    };


//------------------------------------------------------------------------------
//                                  Etat de jeu
//------------------------------------------------------------------------------

    //!
    ImportExportGame::~ImportExportGame() = default;


    /*!
     */
    bool ImportExportGame::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        data()->write<eString>(pWriter, pLoader, "Name");

        return true;
    }


    /*!
     */
    void ImportExportGame::doMake(PAK::Saver& pSaver)
    {
        pSaver << String("Undefined");
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Container::Container(OSGi::Context* pContext)
        : CORE::Container("program", tr("Scripts"), QStringLiteral(":/ide/program/icons/Scripts"), pContext)
        , mDataTabs{}
    {
        mDataTabs = new QTabWidget(this);
        setCentralWidget(mDataTabs);

        auto    lMenuService = pContext->services()->findByTypeAndName<CORE::MenuService>("fr.osg.ide.menu");

        add(lMenuService->menu("scripts").from(this).
            text(tr("&Scripts")).
            sync(   "file + enabled + edition"));
        add(lMenuService->item("scripts", "build").from(this).
            text(tr("&Build")).
            key (tr("F7")).
            sync(   "file + enabled + edition").// + shd;").
            tip(tr("Builds the selected script, or program.")));
        add(lMenuService->item("scripts", "buildAll").from(this).
            text(tr("Build &all")).
            key (tr("Shift+F7")).
            sync(   "file + enabled + edition").// + shd;").
            tip(tr("Builds all the scripts and programs.")));

        lMenuService->connect(QStringLiteral("program.file.new"),   QStringLiteral("data.file.new"));
        lMenuService->connect(QStringLiteral("program.file.open"),  QStringLiteral("data.file.open"));
        lMenuService->connect(QStringLiteral("program.file.save"),  QStringLiteral("data.file.save"));
        lMenuService->connect(QStringLiteral("program.file.close"), QStringLiteral("data.file.close"));

        lMenuService->connect("program.scripts.build", CORE::MenuService::Slot([=]()
        {
            if (auto lTab = dynamic_cast<Editor*>(mDataTabs->currentWidget()))
            {
                lTab->build();
            }
        }));
        lMenuService->connect(QStringLiteral("program.scripts.buildAll"), CORE::MenuService::Slot([=]()
        {
            if (auto lTab = dynamic_cast<Editor*>(mDataTabs->currentWidget()))
            {
                lTab->buildAll();
            }
        }));
    }


    /*!
     */
    Container::~Container() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::addWidget(Widget pWidget)
    {
        int lIndex = mDataTabs->addTab(pWidget.get(), pWidget->windowTitle());
        if (dynamic_cast<CORE::Translated*>(pWidget.get()) != nullptr)
        {
            QWidget*    lW = pWidget.get();
            add([=]() { mDataTabs->setTabText(lIndex, lW->windowTitle()); });
        }
        pWidget.release();
    }


    /*!
     */
    QStringList Container::sortWidgets(const QStringList& pNames) const
    {
        auto lOrderedTabs = QString::fromStdString(context()->properties(BUNDLE_NAME)->get("tabs", "")).split(",") + pNames;
        lOrderedTabs.removeDuplicates();

        return lOrderedTabs;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("program.container", [=]() { return CORE::UIService::Widget{new Container(pContext)}; });

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->declare(CORE::Synchronizer("program.undo").enter("positive program.undo").leave("null program.undo"));
                    lSyncService->declare(CORE::Synchronizer("program.redo").enter("positive program.redo").leave("null program.redo"));

                    auto    lIEService      = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir   = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportGame>("game", lExtensionDir);
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("program.container");

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("game");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(Program::Activator)
OSGI_END_REGISTER_ACTIVATORS()
