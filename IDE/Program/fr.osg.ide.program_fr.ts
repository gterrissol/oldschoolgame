<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Program::Container</name>
    <message>
        <source>Scripts</source>
        <translation>Scripts</translation>
    </message>
    <message>
        <source>&amp;Scripts</source>
        <translation>&amp;Scripts</translation>
    </message>
    <message>
        <source>&amp;Build</source>
        <translation>&amp;Compiler</translation>
    </message>
    <message>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <source>Builds the selected script, or program.</source>
        <translation>Compile le script, ou le programme sélectionné.</translation>
    </message>
    <message>
        <source>Build &amp;all</source>
        <translation>&amp;Tout compiler</translation>
    </message>
    <message>
        <source>Shift+F7</source>
        <translation>Shift+F7</translation>
    </message>
    <message>
        <source>Builds all the scripts and programs.</source>
        <translation>Compile tous les scripts et programmes.</translation>
    </message>
</context>
</TS>
