/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QSHD_QSHADERS_HH
#define QSHD_QSHADERS_HH

/*! @file IDE/Program/QSHaDers/QSHaDers.hh
    @brief Pr�-d�clarations du module @ref QSHaDers.
    @author @ref Guillaume_Terrissol
    @date 18 Mai 2015 - 22 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QSHD
{
    /*! @namespace QSHD
        @version 0.5

     */

    /*! @defgroup QSHaDers QSHaDers : Edition des shaders.
        <b>namespace</b> QSHD.
     */

    class Editor;
}

#endif  // De QSHD_QSHADERS_HH
