/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controllers.hh"

/*! @file IDE/Program/QSHaDers/Controllers.cc
    @brief M�thodes (non-inline) des classes QSHD::Controller & QSHD::Controller.
    @author @ref Guillaume_Terrissol
    @date 18 Mai 2015 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>

#include "CORE/GameService.hh"
#include "EDITion/Attributes.hh"
#include "EDITion/Observer.hh"
#include "EDITion/Subject.hh"
#include "EDITion/Requests.hh"
#include "SHaDers/Library.hh"

#include "Editor.hh"

namespace SHD
{
//------------------------------------------------------------------------------
//                                     Sujet
//------------------------------------------------------------------------------

    /*! @brief Sujet du contr�leur de la biblioth�que de shaders.
        @version 0.5
     */
    class Controller::Subject
        : public EDIT::Subject
        , public EDIT::Container<ShaderDef&, 0>
        , public EDIT::Container<ShaderDef&, 1>
        , public EDIT::Container<ProgramDef&>
        , public EDIT::Value<const ProgramDef&>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Subject(Controller* pParent);                                       //!< Constructeur.
virtual         ~Subject();                                                         //!< Destructeur.
        //@}
        void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo) override;   //! Envoi de r�ponse.
    };


//------------------------------------------------------------------------------
//                   Controller : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @param pContext Contexte d'ex�cution.
     */
    Controller::Controller(OSGi::Context* pContext)
        : EDIT::GenericController<Library>{pContext->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->shaders(), pContext}
        , mSubject{}
    {
        mSubject = std::make_unique<Subject>(this);
    }

    /*! Sauvegarde les donn�es de la biblioth�que.
     */
    Controller::~Controller()
    {
        edited().lock()->save();
    }


//------------------------------------------------------------------------------
//                        Controller : M�thodes d'�dition
//------------------------------------------------------------------------------

    /*! @param pProgram D�finition du programme � ajouter.
        @retval true  Si l'ajout a r�ussi (l'id du programme est d�fini � ce moment),
        @retval false Sinon.
     */
    bool Controller::addProgram(ProgramDef& pProgram)
    {
        auto    lModules = Program::Modules{pProgram.mModules.begin(), pProgram.mModules.end()};
        auto    lId = edited().lock()->link(lModules);
        if (lId != Program::kNullId)
        {
            pProgram.mId = lId;
            return true;
        }
        else
        {
            return false;
        }

    }

    /*! @param pProgram D�finition du programme � supprimer.
        @retval true  Si la suppression a r�ussi,
        @retval false Sinon.
     */
    bool Controller::delProgram(ProgramDef& pProgram)
    {
        try
        {
            edited().lock()->unlink(pProgram.mId);
            return true;
        }
        catch(std::exception&)
        {
            return false;
        }
    }

    /*! @param pProgram D�finition du programme dont �tablir les modules.
     */
    void Controller::setProgramModules(const ProgramDef& pProgram)
    {
        try
        {
            auto    lModules = Program::Modules{pProgram.mModules.begin(), pProgram.mModules.end()};

            edited().lock()->relink(pProgram.mId, lModules);
        }
        catch(std::exception& e)
        {
            qDebug() << e.what();
        }
    }

    /*! @param pProgram D�finition du programme dont r�cup�rer les modules.
     */
    void Controller::getProgramModules(ProgramDef& pProgram) const
    {
        auto    lModules = edited().lock()->modules(pProgram.mId);
        pProgram.mModules = QVector<Shader::Id>(lModules.size());
        std::copy(lModules.begin(), lModules.end(), pProgram.mModules.begin());
    }

    /*! @param pProgram D�finition du shader � ajouter.
        @retval true  Si l'ajout a r�ussi (l'id du shader est d�fini � ce moment),
        @retval false Sinon.
     */
    bool Controller::addShader(ShaderDef& pShader)
    {
        auto    lId = edited().lock()->addShader(pShader.mType, qPrintable(pShader.mCode));

        if (lId != Shader::kNullId)
        {
            pShader.mId = lId;
            return true;
        }
        else
        {
            return false;
        }
    }

    /*! @param pProgram D�finition du shader � supprimer.
        @retval true  Si la suppression a r�ussi,
        @retval false Sinon.
     */
    bool Controller::delShader(ShaderDef& pShader)
    {
        try
        {
            return edited().lock()->delShader(pShader.mId);
        }
        catch(std::exception& e)
        {
            return false;
        }
    }

    /*! @param pProgram D�finition du shader dont �diter le code.
     */
    void Controller::editShader(const ShaderDef& pShader)
    {
        try
        {
            edited().lock()->editShader(pShader.mId, qPrintable(pShader.mCode));
        }
        catch(std::exception& e)
        {
            qDebug() << e.what();
        }
    }

    /*! @param pProgram D�finition du shader dont r�cup�rer le code et le type.
     */
    void Controller::getShader(ShaderDef& pShader) const
    {
        pShader.mCode = edited().lock()->code(pShader.mId).c_str();
        pShader.mType = edited().lock()->type(pShader.mId);
    }

    /*! @param pProgram D�finition du programme � compiler et lier.
     */
    QString Controller::build(ProgramDef& pProgram)
    {
        return edited().lock()->link(pProgram.mId, &pProgram.mIsValid).c_str();
    }

//------------------------------------------------------------------------------
//                        Controller : Gestion du Sujet
//------------------------------------------------------------------------------

    /*! @return Le sujet du contr�leur.
     */
    EDIT::Subject* Controller::asSubject()
    {
        return mSubject.get();
    }

//------------------------------------------------------------------------------
//                               Sujet : M�thodes
//------------------------------------------------------------------------------

    /*! @param pParent Contr�leur du sujet.
     */
    Controller::Subject::Subject(Controller* pParent)
        : EDIT::Subject{"program"}
        , INITIALIZE_CONTAINER(addShader,         delShader, 0)
        , INITIALIZE_CONTAINER(addShader,         delShader, 1)
        , INITIALIZE_CONTAINER(addProgram,        delProgram)
        , INITIALIZE_VALUE(    setProgramModules, getProgramModules)
    { }

    //!
    Controller::Subject::~Subject() = default;

    //!
    void Controller::Subject::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }
}

namespace QSHD
{
//------------------------------------------------------------------------------
//                                     Sujet
//------------------------------------------------------------------------------

    /*! @brief Sujet du contr�leur de l'�diteur de shaders.
        @version 0.5
     */
    class Controller::Subject
        : public EDIT::Subject
        , public EDIT::Value<const Renamer&, 0>
        , public EDIT::Value<const Renamer&, 1>
        , public EDIT::Value<const Renamer&, 2>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Subject(Controller* pParent);                                       //!< Constructeur.
virtual         ~Subject();                                                         //!< Destructeur.
        //@}
        void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo) override;   //! Envoi de r�ponse.
    };


//------------------------------------------------------------------------------
//                   Controller : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @param pEditor  Editeur � contr�ler.
        @param pContext Contexte d'ex�cution.
     */
    Controller::Controller(Editor* pEditor, OSGi::Context* pContext)
        : EDIT::GenericController<Editor>{pEditor, pContext}
        , mSubject{}
    {
        mSubject = std::make_unique<Subject>(this);
    }

    /*! Sauvegarde des donn�es de l'�diteur.
     */
    Controller::~Controller()
    {
        edited().lock()->save();
    }

//------------------------------------------------------------------------------
//                        Controller : M�thodes d'�dition
//------------------------------------------------------------------------------

    /*! @param pProgram D�finitions pour le nouveau nom.
     */
    void Controller::renameProgram(const Renamer& pProgram)
    {
        edited().lock()->setElementName(0, pProgram.mIndex, pProgram.mName);
    }

    /*! @param pProgram Sera initialis� avec le nom du programme.
     */
    void Controller::programName(Renamer& pProgram) const
    {
        pProgram.mName = edited().lock()->elementName(0, pProgram.mIndex);
    }

    /*! @param pVertex D�finitions pour le nouveau nom.
     */
    void Controller::renameVertexShader(const Renamer& pVertex)
    {
        edited().lock()->setElementName(1, pVertex.mIndex, pVertex.mName);
    }

    /*! @param pProgram Sera initialis� avec le nom du vertex shader.
     */
    void Controller::vertexShaderName(Renamer& pVertex) const
    {
        pVertex.mName = edited().lock()->elementName(1, pVertex.mIndex);
    }

    /*! @param pFragment D�finitions pour le nouveau nom.
     */
    void Controller::renameFragmentShader(const Renamer& pFragment)
    {
        edited().lock()->setElementName(2, pFragment.mIndex, pFragment.mName);
    }

    /*! @param pProgram Sera initialis� avec le nom du fragment shader.
     */
    void Controller::fragmentShaderName(Renamer& pFragment) const
    {
        pFragment.mName = edited().lock()->elementName(2, pFragment.mIndex);
    }

//------------------------------------------------------------------------------
//                        Controller : Gestion du Sujet
//------------------------------------------------------------------------------

    /*! @return Le sujet du contr�leur.
     */
    EDIT::Subject* Controller::asSubject()
    {
        return mSubject.get();
    }

//------------------------------------------------------------------------------
//                               Sujet : M�thodes
//------------------------------------------------------------------------------

    /*! @param pParent Contr�leur du sujet.
     */
    Controller::Subject::Subject(Controller* pParent)
        : EDIT::Subject{"program"}
        , INITIALIZE_VALUE(renameProgram,        programName,        0)
        , INITIALIZE_VALUE(renameVertexShader,   vertexShaderName,   1)
        , INITIALIZE_VALUE(renameFragmentShader, fragmentShaderName, 2)
    { }

    //!
    Controller::Subject::~Subject() = default;

    //!
    void Controller::Subject::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }
}
