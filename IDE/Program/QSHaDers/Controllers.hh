/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QSHD_CONTROLLERS_HH
#define QSHD_CONTROLLERS_HH

#include "QSHaDers.hh"

/*! @file IDE/Program/QSHaDers/Controllers.hh
    @brief En-t�te des classes QSHD::Controller & QSHD::Controller.
    @author @ref Guillaume_Terrissol
    @date 18 Mai 2015 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QString>
#include <QVector>

#include <OSGi/Context.hh>

#include "EDITion/Controller.hh"
#include "SHaDers/Library.hh"


namespace SHD
{
//------------------------------------------------------------------------------
//                     Controller : Biblioth�que de Shaders
//------------------------------------------------------------------------------

    /*! Donn�es IDE d'un shader (stock�es dans un conteneur par type de shader).
     */
    struct ShaderDef
    {
        QString         mName;  // Nom affich� dans l'interface.
        QString         mCode;  // Code source.
        Shader::EType   mType;  // Type du shader.
        Shader::Id      mId;    // Id du shader.
        int             mIndex; // Index dans le conteneur.
    };

    /*! Donn�es IDE d'un programme (stock�es dans un conteneur).
     */
    struct ProgramDef
    {
        QString                 mName;      //!< Nom affich� dans l'interface.
        QVector<Shader::Id>     mModules;   //!< Liste de shaders du programme.
        Program::Id             mId;        //!< Id du shader.
        int                     mIndex;     //!< Index dans le conteneur.
        bool                    mIsValid;   //!< Validit�.
    };


    /*! Contr�leur pour �dition des shaders.
        @version 0.6

        Contr�leur de la biblioth�que de shaders.
     */
    class Controller : public EDIT::GenericController<Library>
    {
    public:

        using Ptr = std::shared_ptr<Controller>;                //!< Pointeur sur contr�leur.
        //! @name Constructeurs & destructeur
        //@{
                        Controller(OSGi::Context* pContext);    //!< Constructeur.
virtual                 ~Controller();                          //!< Destructeur.
        //@}
        //! @name M�thodes d'�dition
        //@{
        bool    addProgram(ProgramDef& pProgram);               //!< Ajout d'un programme.
        bool    delProgram(ProgramDef& pProgram);               //!< Suppression d'un programme.
        void    setProgramModules(const ProgramDef& pProgram);  //!< D�finition des modules d'un programme.
        void    getProgramModules(ProgramDef& pProgram) const;  //!< R�cup�ration des modules d'un programme.

        bool    addShader(ShaderDef& pShader);                  //!< Ajout d'un shader.
        bool    delShader(ShaderDef& pShader);                  //!< Suppression d'un shader.
        void    editShader(const ShaderDef& pShader);           //!< Edition d'un shader.
        void    getShader(ShaderDef& pShader) const;            //!< R�cup�ration des informations d'un shader.
        //@}
        //! @name Compilation
        //@{
        QString build(ProgramDef& pProgram);                    //!< Compilation d'un programme.
        //@}
    protected:
        //! @name Gestion du sujet
        //@{
        EDIT::Subject*  asSubject() override;                   //!< R�cup�ration du sujet du contr�leur.

    private:

        class Subject;
        std::unique_ptr<Subject>    mSubject;                   //!< Sujet du contr�leur.
        //@}
    };
}

namespace QSHD
{
//------------------------------------------------------------------------------
//                        Controller : Editeur de Shaders
//------------------------------------------------------------------------------

    struct Renamer
    {
        int     mIndex;
        QString mName;
    };

    /*! Contr�leur pour l'�dition de l'interface de la bilbioth�que de shaders.
        @version 0.4
     */
    class Controller : public EDIT::GenericController<Editor>
    {
    public:

        using Ptr = std::shared_ptr<Controller>;
        //! @name Constructeurs & destructeur
        //@{
                        Controller(Editor*        pEditor,
                                   OSGi::Context* pContext);    //!< Constructeur.
virtual                 ~Controller();                          //!< Destructeur.
        //@}
        //! @name M�thodes d'�dition
        //@{
        void    renameProgram(const Renamer& pProgram);         //!< Renommage d'un programme.
        void    programName(Renamer& pProgram) const;           //!< Nom d'un programme.
        void    renameVertexShader(const Renamer& pVertex);     //!< Renommage d'un vertex shader.
        void    vertexShaderName(Renamer& pVertex) const;       //!< Nom d'un vertex shader.
        void    renameFragmentShader(const Renamer& pFragment); //!< Renommage d'un fragment shader.
        void    fragmentShaderName(Renamer& pFragment) const;   //!< Nom d'un fragment shader.
        //@}
    protected:
        //! @name Gestion du sujet
        //@{
        EDIT::Subject*  asSubject() override;                   //!< R�cup�ration du sujet du contr�leur.

    private:

        class Subject;
        std::unique_ptr<Subject>    mSubject;                   //!< Sujet du contr�leur.
        //@}
    };
}

#endif  // De QSHQ_CONTROLLERS_HH
