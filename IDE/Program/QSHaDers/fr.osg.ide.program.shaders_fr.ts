<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>GLSLLibrary</name>
    <message>
        <source>GLSL Programs</source>
        <translation>Programmes GLSL</translation>
    </message>
    <message>
        <source>Programs</source>
        <translation>Programmes</translation>
    </message>
    <message>
        <source>Vertex Shaders</source>
        <translation>Vertex Shaders</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <source>Fragment Shaders</source>
        <translation>Fragment Shaders</translation>
    </message>
</context>
<context>
    <name>QSHD::Editor</name>
    <message>
        <source>Modules of %1</source>
        <translation>Modules de %1</translation>
    </message>
    <message>
        <source>Building %1
%2
Shaders saved.

</source>
        <translation>Compile %1
%2
Shaders sauvegardés.

</translation>
    </message>
    <message>
        <source>Building %1 failed :
%2
Save cancelled.

</source>
        <translation>La compilation de %1 a échoué :
%2
Sauvegarde annulée.

</translation>
    </message>
</context>
</TS>
