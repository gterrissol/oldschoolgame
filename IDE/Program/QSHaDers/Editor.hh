/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QSHD_EDITOR_HH
#define QSHD_EDITOR_HH

#include "QSHaDers.hh"

/*! @file IDE/Program/QSHaDers/Editor.hh
    @brief En-t�te de la classe QSHD::Editor.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2015 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QAbstractItemDelegate>

#include <OSGi/Context.hh>

#include "CORE/Private.hh"
#include "CORE/Synchronize.hh"
#include "CORE/Translate.hh"
#include "DATA/Store.hh"
#include "Program/Editor.hh"

class QListWidgetItem;

namespace QSHD
{
//------------------------------------------------------------------------------
//                              Editeur de Shaders
//------------------------------------------------------------------------------

    /*! @brief Editeur de shaders.
        @version 0.6

        Interface de cr�ation/�dition de programmes GLSL.
     */
    class Editor : public Program::Editor, public CORE::Translate<Editor>, public CORE::Synchronized, DATA::Store
    {
        Q_OBJECT
        friend class Controller;                                            //!< Pour les m�thodes d'�dition.
    public:
        //! @name Constructeur & destructeur
        //@{
                Editor(OSGi::Context* pContext);                            //!< Constructeur.
virtual         ~Editor();                                                  //!< Destructeur.
        //@}
        bool    eventFilter(QObject* pWatched, QEvent* pEvent) override;    //!< Filtrage d'�v�nements.

    protected:
        //! @name Store
        //@{
        QString saveFolder() const override;                                //!< R�pertoire de sauvegarde.
        void    fillAtCreation() override;                                  //!< Donn�es IDE initiale.
        //@}
        //! @name
        //@{
        void    editNow() override;                                         //!< Activation de l'�diteur.
        void    endEdit() override;                                         //!< D�sactivation de l'�diteur.
        //@}
    private slots:
        //! @name S�lection d'un �l�ment
        //@{
        void    programSelected();                                          //!< [D�s]S�lection d'un programme.
        void    programSelected(QListWidgetItem* pItem);                    //!< S�lection d'un programme.
        void    moduleSelected();                                           //!< [D�s]S�lection d'un module de programme
        void    moduleSelected(QListWidgetItem* pItem);                     //!< S�lection d'un module de programme
        void    vertexShaderSelected();                                     //!< [D�s]S�lection d'un vertex shader.
        void    vertexShaderSelected(QListWidgetItem* pItem);               //!< S�lection d'un vertex shader.
        void    fragmentShaderSelected();                                   //!< [D�s]S�lection d'un fragment shader.
        void    fragmentShaderSelected(QListWidgetItem* pItem);             //!< S�lection d'un fragment shader.
        //@}
        //! @name Ajout/suppression d'�l�ments
        //@{
        void    addProgram();                                               //!< Ajout d'un programme.
        void    delProgram();                                               //!< Suppression d'un programme.
        void    delModule();                                                //!< Suppression d'un module de programme.
        void    useVertexShader();                                          //!< Utilisation d'un vertex shader dans un programme.
        void    useFragmentShader();                                        //!< Utilisation d'un fragment shader dans un programme.
        void    addVertexShader();                                          //!< Ajout d'un vertex shadeR.
        void    delVertexShader();                                          //!< Suppression d'un fragment shader.
        void    addFragmentShader();                                        //!< Ajout d'un vertex shadeR.
        void    delFragmentShader();                                        //!< Suppression d'un fragment shader.
        //@}
        //! @name Renommage des �l�m�nts (programme, shaders).
        //{
        void    close(QWidget* pE, QAbstractItemDelegate::EndEditHint pH);  //!< Fermeture de l'�diteur de nom.

    private:

        void    requestRenameElement(int pType, int pIndex, QString pName); //!< Requ�te de renommage d'un �l�ment.
        void    setElementName(int pType, int pIndex, QString pName);       //!< Nommage d'un �l�ment.
        QString elementName(int pType, int pIndex) const;                   //!< Nom d'un �l�ment.
        //@}
        //! @name Compilation
        //@{
        bool    canBuild() const override;                                  //!< Possibilit� de compilation ?
        void    doBuild() override;                                         //!< Compilation.
        bool    canBuildAll() const override;                               //!< Possibilit� de compilation ?
        void    doBuildAll() override;                                      //!< Compilation de tous les programmes.
        bool    build(int pProgram);                                        //!< Compilation d'un programme.
        //@}
        auto    buildTranslater() -> CORE::Translated::Updater override;    //!< Traduction de l'interface.

        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}

#endif  // De QSHD_EDITOR_HH
