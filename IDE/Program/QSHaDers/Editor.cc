/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Program/QSHaDers/Editor.cc
    @brief M�thodes (non-inline) des classes QSHD::Editor::EditorPrivate & QSHD::Editor.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2015 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDateTime>
#include <QKeyEvent>
#include <QLineEdit>
#include <QVector>

#include "CORE/MenuService.hh"
#include "DATA/QPAcKage/FileService.hh"
#include "EDITion/Observer.hh"
#include "EDITion/Requests.hh"
#include "SHaDers/Const.hh"

#include "Controllers.hh"
#include "GLSLLibrary.ui.hh"

namespace
{
//------------------------------------------------------------------------------
//                              Fonction Assistante
//------------------------------------------------------------------------------

    /*! Appeler cette fonction permet d'activer l'�dition des items de listes d�s leur cr�ation.
        @param pList  List dans laquelle ins�rer un item.
        @param pIndex Position du nouvel item dans la liste.
        @param pText  Texte initial du nouvel item.
     */
    void insertItem(QListWidget* pList, int pIndex, const QString& pText)
    {
        pList->insertItem(pIndex, pText);
        auto    lItem = pList->item(pIndex);
        lItem->setFlags(lItem->flags() | Qt::ItemIsEditable);
    }


//------------------------------------------------------------------------------
//                              Shaders par D�faut
//------------------------------------------------------------------------------

    //! Code par d�faut pour un vertex shader.
    const QString kDefaultVertexShader = QStringLiteral(
        "#version 130\n"
        "\n"
        "uniform mat4 uPVM;\n"
        "\n"
        "smooth out vec4 ioColor;\n"
        "\n"
        "void main()\n"
        "{\n"
        "    gl_Position = uPVM * gl_Vertex;\n"
        "    ioColor = gl_Color;\n"
        "}\n"
    );

    //! Code par d�faut pour un fragment shader.
    const QString kDefaultFragmentShader = QStringLiteral(
        "#version 130\n"
        "\n"
        "smooth in vec4 ioColor;\n"
        "\n"
        "void main()\n"
        "{\n"
        "    gl_FragColor = ioColor;\n"
        "}\n"
    );
}

namespace QSHD
{
//------------------------------------------------------------------------------
//                                 EditorPrivate
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Editor.
        @version 0.5
     */
    class Editor::EditorPrivate : public EDIT::Observer
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
        //! @name Constructeurs
        //@{
                EditorPrivate(Editor* parent, OSGi::Context* pContext);         //!< Constructeur.
        void    init();                                                         //!< Initialisation.
        //@}
        void    editShader(QListWidgetItem* pItem);                             //!< Edition du code d'un shader.
        void    cacheShader();                                                  //!< Mise en cache du code d'un shader
        void    appendLog(QString pMessage);                                    //!< Ajout d'informations de log.
        void    appendLog(QString pMessage, const SHD::ProgramDef* pProgram);   //!< Ajout d'informations de log.

        OSGi::Context*              mContext;                                   //!< Contexte d'ex�cution.
        Ui::GLSLLibrary             mUI;                                        //!< Interface Qt.
        EDIT::Controller::Ptr       mController;                                //!< Contr�leur de la bibiloth�que de shaders.
        EDIT::Controller::Ptr       mUIController;                              //!< Contr�leur du l'interface.
        QVector<SHD::ProgramDef>    mPrograms;                                  //!< D�finition des programmes GLSL.
        QVector<SHD::ShaderDef>     mVertices;                                  //!< D�finition des vertex shaders.
        QVector<SHD::ShaderDef>     mFragments;                                 //!< D�finition des fragments shaders.
        SHD::ShaderDef*             mEditedShader;                              //!< Shader �dit�.

    private:

        QString makeLog(QString pMessage, QString pProgramName, bool pSuccess); //!< Affichage d'informations de log.
        void    listen(const EDIT::Answer* pAnswer) override;                   //!< Ecoute.
    };


//------------------------------------------------------------------------------
//                                Editor : P-Impl
//------------------------------------------------------------------------------

    /*! @param pParent  Editeur dont l'instance est le p-impl.
        @param pContext Contexte d'ex�cution.
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent, OSGi::Context* pContext)
        : q{pParent}
        , mContext{pContext}
        , mUI{}
        , mController{}
        , mUIController{}
        , mPrograms{}
        , mVertices{}
        , mFragments{}
        , mEditedShader{}
    { }


    //!
    void Editor::EditorPrivate::init()
    {
        // Construit l'interface.
        q->from(q);
        mUI.setupUi(q);
        q->setObjectName(QString::null);    // Le widget pourra �tre correctement renomm� par UIService.

        q->connect(mUI.programList,         SIGNAL(itemSelectionChanged()),        q, SLOT(programSelected()));
        q->connect(mUI.programList,         SIGNAL(itemClicked(QListWidgetItem*)), q, SLOT(programSelected(QListWidgetItem*)));
        q->connect(mUI.moduleList,          SIGNAL(itemSelectionChanged()),        q, SLOT(moduleSelected()));
        q->connect(mUI.moduleList,          SIGNAL(itemClicked(QListWidgetItem*)), q, SLOT(moduleSelected(QListWidgetItem*)));
        q->connect(mUI.vertexShaderList,    SIGNAL(itemSelectionChanged()),        q, SLOT(vertexShaderSelected()));
        q->connect(mUI.vertexShaderList,    SIGNAL(itemClicked(QListWidgetItem*)), q, SLOT(vertexShaderSelected(QListWidgetItem*)));
        q->connect(mUI.fragmentShaderList,  SIGNAL(itemSelectionChanged()),        q, SLOT(fragmentShaderSelected()));
        q->connect(mUI.fragmentShaderList,  SIGNAL(itemClicked(QListWidgetItem*)), q, SLOT(fragmentShaderSelected(QListWidgetItem*)));

        q->connect(mUI.addProgramButton,        SIGNAL(clicked()),  q, SLOT(addProgram()));
        q->connect(mUI.delProgramButton,        SIGNAL(clicked()),  q, SLOT(delProgram()));
        q->connect(mUI.delModuleButton,         SIGNAL(clicked()),  q, SLOT(delModule()));
        q->connect(mUI.useVertexShaderButton,   SIGNAL(clicked()),  q, SLOT(useVertexShader()));
        q->connect(mUI.useFragmentShaderButton, SIGNAL(clicked()),  q, SLOT(useFragmentShader()));
        q->connect(mUI.addVertexShaderButton,   SIGNAL(clicked()),  q, SLOT(addVertexShader()));
        q->connect(mUI.delVertexShaderButton,   SIGNAL(clicked()),  q, SLOT(delVertexShader()));
        q->connect(mUI.addFragmentShaderButton, SIGNAL(clicked()),  q, SLOT(addFragmentShader()));
        q->connect(mUI.delFragmentShaderButton, SIGNAL(clicked()),  q, SLOT(delFragmentShader()));

        q->connect(mUI.vertexShaderList->itemDelegate(),    SIGNAL(closeEditor(QWidget*,QAbstractItemDelegate::EndEditHint)),
                   q,                                       SLOT(close(QWidget*,QAbstractItemDelegate::EndEditHint)));
        q->connect(mUI.fragmentShaderList->itemDelegate(),  SIGNAL(closeEditor(QWidget*,QAbstractItemDelegate::EndEditHint)),
                   q,                                       SLOT(close(QWidget*,QAbstractItemDelegate::EndEditHint)));
        q->connect(mUI.programList->itemDelegate(),         SIGNAL(closeEditor(QWidget*,QAbstractItemDelegate::EndEditHint)),
                   q,                                       SLOT(close(QWidget*,QAbstractItemDelegate::EndEditHint)));

        mUI.programList->installEventFilter(q);
        mUI.moduleList->installEventFilter(q);
        mUI.vertexShaderList->installEventFilter(q);
        mUI.fragmentShaderList->installEventFilter(q);
    }


    /*! @param pItem Item de liste s�lectionn�.
     */
    void Editor::EditorPrivate::editShader(QListWidgetItem* pItem)
    {
        cacheShader();

        if (pItem != nullptr)
        {
            if      (pItem->listWidget() == mUI.vertexShaderList)
            {
                mEditedShader = &mVertices[mUI.vertexShaderList->row(pItem)];
            }
            else if (pItem->listWidget() == mUI.fragmentShaderList)
            {
                mEditedShader = &mFragments[mUI.fragmentShaderList->row(pItem)];
            }
            else    // Est-ce possible ?
            {
                mEditedShader = nullptr;
            }
        }
        else
        {
            mEditedShader = nullptr;
        }
    }

    /*! Le code actuellement �dit� est mis de c�t�, en attendant une nouvelle �dition, ou une compilation.
     */
    void Editor::EditorPrivate::cacheShader()
    {
        if (mEditedShader != nullptr)
        {
            mEditedShader->mCode = mUI.shaderEditor->toPlainText();
        }
    }


    /*! @param pMessage Informations � ajouter � la fen�tre de log.
     */
    void Editor::EditorPrivate::appendLog(QString pMessage)
    {
        mUI.infoLog->setPlainText(mUI.infoLog->toPlainText() + makeLog(pMessage, QString::null, false));
    }

    /*! @param pMessage Informations � ajouter � la fen�tre de log.
        @param pProgram Programme dont la compilation est � l'origine de @p pMessage.
     */
    void Editor::EditorPrivate::appendLog(QString pMessage, const SHD::ProgramDef* pProgram)
    {
        auto    lSucess = (pProgram != nullptr);
        auto    lName   = lSucess ? pProgram->mName : QString::null;
        mUI.infoLog->setPlainText(mUI.infoLog->toPlainText() + makeLog(pMessage, lName, lSucess));
    }

    /*! @param pMessage     Informations � ajouter � la fen�tre de log.
        @param pProgramName Programme dont la compilation est � l'origine de @p pMessage.
        @param pSuccess     Status de la compilation de @p pProgramName.
     */
    QString Editor::EditorPrivate::makeLog(QString pMessage, QString pProgramName, bool pSuccess)
    {
        auto    lStatus = pSuccess ?
                            tr("Building %1\n%2\nShaders saved.\n\n") :
                            tr("Building %1 failed :\n%2\nSave cancelled.\n\n");

        return QString{"%1\n"}.arg(QDateTime::currentDateTime().toString())
             + lStatus.arg(pProgramName).arg(pMessage)
             + QString{80, QChar{'-'}}
             + QStringLiteral("\n");
    }

    /*! @param pAnswer R�ponse � l'�dition avec laquelle mettre � jour l'�diteur.
     */
    void Editor::EditorPrivate::listen(const EDIT::Answer* pAnswer)
    {
        if      (auto lAnswer = dynamic_cast<const EDIT::GenericAnswer<SHD::ShaderDef&, 0>*>(pAnswer))
        {
            auto    lDef = lAnswer->data();
            if (lAnswer->from() == EDIT::InsertRequest<SHD::ShaderDef&, 0>::type())
            {
                mVertices.insert(lDef.mIndex, lDef);
                insertItem(mUI.vertexShaderList, lDef.mIndex, lDef.mName);
            }
            else if(lAnswer->from() == EDIT::RemoveRequest<SHD::ShaderDef&, 0>::type())
            {
                mVertices.remove(lDef.mIndex);
                delete mUI.vertexShaderList->takeItem(lDef.mIndex);
                mUI.vertexShaderList->clearSelection();
            }
        }
        if      (auto lAnswer = dynamic_cast<const EDIT::GenericAnswer<SHD::ShaderDef&, 1>*>(pAnswer))
        {
            auto    lDef = lAnswer->data();
            if (lAnswer->from() == EDIT::InsertRequest<SHD::ShaderDef&, 1>::type())
            {
                mFragments.insert(lDef.mIndex, lDef);
                insertItem(mUI.fragmentShaderList, lDef.mIndex, lDef.mName);
            }
            else if(lAnswer->from() == EDIT::RemoveRequest<SHD::ShaderDef&, 1>::type())
            {
                mFragments.remove(lDef.mIndex);
                delete mUI.fragmentShaderList->takeItem(lDef.mIndex);
                mUI.fragmentShaderList->clearSelection();
            }
        }
        else if (auto lAnswer = dynamic_cast<const EDIT::GenericAnswer<SHD::ProgramDef&>*>(pAnswer))
        {
            auto    lDef = lAnswer->data();
            if (lAnswer->from() == EDIT::InsertRequest<SHD::ProgramDef&>::type())
            {
                mPrograms.insert(lDef.mIndex, lDef);
                insertItem(mUI.programList, lDef.mIndex, lDef.mName);
            }
            else if(lAnswer->from() == EDIT::RemoveRequest<SHD::ProgramDef&>::type())
            {
                mPrograms.remove(lDef.mIndex);
                delete mUI.programList->takeItem(lDef.mIndex);
                mUI.programList->clearSelection();
            }
        }
        else if (auto lAnswer = dynamic_cast<const EDIT::GenericAnswer<const SHD::ProgramDef&>*>(pAnswer))
        {
            auto    lDef = lAnswer->data();
            mPrograms[lDef.mIndex].mModules = lDef.mModules;
            q->programSelected();
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param pContext Contexte d'ex�cution.
     */
    Editor::Editor(OSGi::Context* pContext)
        : Program::Editor{}
        , CORE::Translate<Editor>{}
        , CORE::Synchronized{"file + hardware + enabled + edition"}
        , DATA::Store{"GLSL"}
        , pthis{this, pContext}
    {
        pthis->init();
    }


    //!
    Editor::~Editor() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Pour l'instant, ne g_re que les demandes de suppression d'�l�ments de listes.
        @param pWatched Objet surveill�
        @param pEvent   Ev�nement � traiter
        @retval true Si @p pEvent> �tait une demande de suppression (Suppr.); la valeur de
        retour de QWidget::eventFilter() sinon
     */
    bool Editor::eventFilter(QObject* pWatched, QEvent* pEvent)
    {
        if (auto lEvent = dynamic_cast<QKeyEvent*>(pEvent))
        {
            if      (lEvent->key() == Qt::Key_Delete)
            {
                if      (pWatched == pthis->mUI.programList)
                {
                    delProgram();
                    return true;
                }
                else if (pWatched == pthis->mUI.moduleList)
                {
                    delModule();
                    return true;
                }
                else if (pWatched == pthis->mUI.vertexShaderList)
                {
                    delVertexShader();
                    return true;
                }
                else if (pWatched == pthis->mUI.fragmentShaderList)
                {
                    delFragmentShader();
                    return true;
                }
            }
        }

        return Program::Editor::eventFilter(pWatched, pEvent);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    QString Editor::saveFolder() const
    {
        return pthis->mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }

    //!
    void Editor::fillAtCreation()
    {
        set("vertexShaders",   QStringList{ QString{"Default vertex shader:0"}, QString{"Picking vertex shader:2"}, QString{"MegaTexture vertex shader:4"} });
        set("fragmentShaders", QStringList{ QString{"Default fragment shader:1"}, QString{"Picking fragment shader:3"}, QString{"MegaTexture fragment shader:5"} });
        set("programs",        QStringList{ QString{"Default:%1"}.arg(SHD::kDefaultProgram), QString{"Picking:%1"}.arg(SHD::kPickingProgram), QString{"MegaTexture:%1"}.arg(SHD::kMegaTexProgram) });
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Une fois le @b pak file ouvert, certains composants d'�dition, bas�s sur des objets moteur, peuvent enfin �tre cr��s.
     */
    void Editor::editNow()
    {
        load();
        pthis->mController   = std::make_shared<SHD::Controller>(pthis->mContext);
        pthis->mUIController = std::make_shared<Controller>(this, pthis->mContext);
        pthis->watch(EDIT::Controller::subject(pthis->mController));

        // Donn�es initiales.

        int i = 0;
        for(auto d : list("vertexShaders"))
        {
            auto    lFields = d.split(":");
            if (lFields.count() == 2)
            {
                auto    lDef = SHD::ShaderDef{lFields[0], kDefaultVertexShader, SHD::Shader::eNone, SHD::Shader::Id(lFields[1].toUInt()), i++};
                std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->getShader(lDef);
                pthis->mVertices.push_back(lDef);
                insertItem(pthis->mUI.vertexShaderList, i - 1, lDef.mName);
            }
        }
        i = 0;
        for(auto d : list("fragmentShaders"))
        {
            auto    lFields = d.split(":");
            if (lFields.count() == 2)
            {
                auto    lDef = SHD::ShaderDef{lFields[0], kDefaultFragmentShader, SHD::Shader::eNone, SHD::Shader::Id(lFields[1].toUInt()), i++};
                std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->getShader(lDef);
                pthis->mFragments.push_back(lDef);
                insertItem(pthis->mUI.fragmentShaderList, i - 1, lDef.mName);
            }
        }
        i = 0;
        for(auto d : list("programs"))
        {
            auto    lFields = d.split(":");
            if (lFields.count() == 2)
            {
                auto    lDef = SHD::ProgramDef{lFields[0], QVector<SHD::Shader::Id>{}, SHD::Program::Id(lFields[1].toUInt()), i++, false};
                std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->getProgramModules(lDef);
                pthis->mPrograms.push_back(lDef);
                insertItem(pthis->mUI.programList, i - 1, lDef.mName);
            }
        }
    }

    /*! M�nage � effectuer lors de la fermeture du @b pak file.
     */
    void Editor::endEdit()
    {
        QStringList lVertices{};
        for(auto d : pthis->mVertices)
        {
            lVertices << QString{"%1:%2"}.arg(d.mName).arg(d.mId);
        }
        set("vertexShaders", lVertices);
        pthis->mVertices.clear();
        pthis->mUI.vertexShaderList->clear();

        QStringList lFragments{};
        for(auto d : pthis->mFragments)
        {
            lFragments << QString{"%1:%2"}.arg(d.mName).arg(d.mId);
        }
        set("fragmentShaders", lFragments);
        pthis->mFragments.clear();
        pthis->mUI.fragmentShaderList->clear();

        pthis->mUI.shaderEditor->setPlainText("");

        QStringList lPrograms{};
        for(auto d : pthis->mPrograms)
        {
            lPrograms << QString{"%1:%2"}.arg(d.mName).arg(d.mId);
        }
        set("programs", lPrograms);
        pthis->mPrograms.clear();
        pthis->mUI.programList->clear();
        pthis->mUI.moduleList->clear();

        pthis->watch(nullptr);
        pthis->mUIController.reset();
        pthis->mController.reset();
        save();
    }


//------------------------------------------------------------------------------
//                        Slots : S�lection d'un El�ment
//------------------------------------------------------------------------------

    /*! Traite le nouveau programme s�lectionn�, ou la d�s�lection de tout programme.
     */
    void Editor::programSelected()
    {
        auto    lItems = pthis->mUI.programList->selectedItems();
        programSelected(!lItems.isEmpty() ? lItems.front() : nullptr);
    }

    /*! Met � jour l'�diteur suite � une [d�]s�lection de programme.
        @param pItem item du nouveau programme s�lectionn�, ou @b nullptr en cas de d�selection.
     */
    void Editor::programSelected(QListWidgetItem* pItem)
    {
        pthis->mUI.moduleList->clear();

        if (pItem != nullptr)
        {
            int lRow = pthis->mUI.programList->row(pItem);
            pthis->mUI.modulesLabel->setText(tr("Modules of %1").arg(pthis->mPrograms[lRow].mName));
            

            for(auto m : pthis->mPrograms[lRow].mModules)
            {
                for(auto v : pthis->mVertices)
                {
                    if (v.mId == m)
                    {
                        pthis->mUI.moduleList->addItem(QString("%1").arg(v.mName));
                        break;
                    }
                }
                for(auto f : pthis->mFragments)
                {
                    if (f.mId == m)
                    {
                        pthis->mUI.moduleList->addItem(QString("%1").arg(f.mName));
                        break;
                    }
                }
            }
        }
        else
        {
            pthis->mUI.modulesLabel->setText("");
        }
        pthis->mUI.delProgramButton->setEnabled(pItem != nullptr);
    }

    /*! Traite le nouveau vertex shader s�lectionn�, ou la d�s�lection de tout vertex shader.
     */
    void Editor::moduleSelected()
    {
        auto    lItems = pthis->mUI.moduleList->selectedItems();
        moduleSelected(!lItems.isEmpty() ? lItems.front() : nullptr);
    }

    /*! Met � jour l'�diteur suite � une [d�]s�lection de module.
        @param pItem item du nouveau module s�lectionn�, ou @b nullptr en cas de d�selection.
     */
    void Editor::moduleSelected(QListWidgetItem* pItem)
    {
        pthis->mUI.delModuleButton->setEnabled((pItem != nullptr) && !pthis->mUI.moduleList->selectedItems().isEmpty());
    }

    /*! Traite le nouveau module s�lectionn�, ou la d�s�lection de tout module.
     */
    void Editor::vertexShaderSelected()
    {
        auto    lItems = pthis->mUI.vertexShaderList->selectedItems();
        vertexShaderSelected(!lItems.isEmpty() ? lItems.front() : nullptr);
    }

    /*! Met � jour l'�diteur suite � une [d�]s�lection de vertex shader.
        @param pItem item du nouveau vertex shader s�lectionn�, ou @b nullptr en cas de d�selection.
     */
    void Editor::vertexShaderSelected(QListWidgetItem* pItem)
    {
        pthis->editShader(pItem);
        if (pItem != nullptr)
        {
            auto&   lDef = pthis->mVertices[pthis->mUI.vertexShaderList->row(pItem)];
            pthis->mUI.shaderEditor->setPlainText(lDef.mCode);
            pthis->mUI.shaderNameLabel->setText(QString{"Vertex shader : %1"}.arg(lDef.mName));
        }

        pthis->mUI.delVertexShaderButton->setEnabled((pItem != nullptr) && !pthis->mUI.vertexShaderList->selectedItems().isEmpty());
        pthis->mUI.useVertexShaderButton->setEnabled((pItem != nullptr) && !pthis->mUI.programList->selectedItems().isEmpty());

    }

    /*! Traite le nouveau fragmentshader s�lectionn�, ou la d�s�lection de tout fragment shader.
     */
    void Editor::fragmentShaderSelected()
    {
        auto    lItems = pthis->mUI.fragmentShaderList->selectedItems();
        fragmentShaderSelected(!lItems.isEmpty() ? lItems.front() : nullptr);
    }

    /*! Met � jour l'�diteur suite � une [d�]s�lection de fragment shader.
        @param pItem item du nouveau fragment shader s�lectionn�, ou @b nullptr en cas de d�selection.
     */
    void Editor::fragmentShaderSelected(QListWidgetItem* pItem)
    {
        pthis->editShader(pItem);
        if (pItem != nullptr)
        {
            auto&   lDef = pthis->mFragments[pthis->mUI.fragmentShaderList->row(pItem)];
            pthis->mUI.shaderEditor->setPlainText(lDef.mCode);
            pthis->mUI.shaderNameLabel->setText(QString{"Fragment shader : %1"}.arg(lDef.mName));
        }
        pthis->mUI.delFragmentShaderButton->setEnabled((pItem != nullptr) && !pthis->mUI.vertexShaderList->selectedItems().isEmpty());
        pthis->mUI.useFragmentShaderButton->setEnabled((pItem != nullptr) && !pthis->mUI.programList->selectedItems().isEmpty());
    }


//------------------------------------------------------------------------------
//                     Slots : Ajout/Suppression d'El�ments
//------------------------------------------------------------------------------

    //!
    void Editor::addProgram()
    {
        auto            lNum = pthis->mPrograms.count();
        SHD::ProgramDef lDef{QString{"New program %1"}.arg(lNum + 1), QVector<SHD::Shader::Id>{}, SHD::Shader::kNullId, lNum, false};
        auto            lRequest = EDIT::InsertRequest<SHD::ProgramDef&>{lDef};

        EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
    }

    //!
    void Editor::delProgram()
    {
        auto    lSelectionModel = pthis->mUI.programList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lNum = lSelectionModel->selectedIndexes().front().row();
            SHD::ProgramDef lDef = pthis->mPrograms[lNum];
            lDef.mIndex = lNum;
            auto            lRequest = EDIT::RemoveRequest<SHD::ProgramDef&>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }

    //!
    void Editor::delModule()
    {
        auto    lSelectionModel = pthis->mUI.moduleList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lProg       = lSelectionModel->selectedIndexes().front().row();
            SHD::ProgramDef lDef        = pthis->mPrograms[lProg];
            auto            lModule     = lDef.mModules[pthis->mUI.moduleList->selectionModel()->selectedIndexes().front().row()];
            lDef.mModules.removeOne(lModule);
            auto            lRequest    = EDIT::EditRequest<const SHD::ProgramDef&>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }

    //!
    void Editor::useVertexShader()
    {
        auto    lSelectionModel = pthis->mUI.programList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lProg       = lSelectionModel->selectedIndexes().front().row();
            SHD::ProgramDef lDef        = pthis->mPrograms[lProg];
            auto            lModule     = pthis->mVertices[pthis->mUI.vertexShaderList->selectionModel()->selectedIndexes().front().row()].mId;
            lDef.mModules.push_back(lModule);
            auto            lRequest    = EDIT::EditRequest<const SHD::ProgramDef&>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }

    //!
    void Editor::useFragmentShader()
    {
        auto    lSelectionModel = pthis->mUI.programList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lProg       = lSelectionModel->selectedIndexes().front().row();
            SHD::ProgramDef lDef        = pthis->mPrograms[lProg];
            auto            lModule     = pthis->mFragments[pthis->mUI.fragmentShaderList->selectionModel()->selectedIndexes().front().row()].mId;
            lDef.mModules.push_back(lModule);
            auto            lRequest    = EDIT::EditRequest<const SHD::ProgramDef&>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }

    //!
    void Editor::addVertexShader()
    {
        auto            lNum = pthis->mVertices.count();
        SHD::ShaderDef  lDef{QString{"Vertex shader"},
                             kDefaultVertexShader,
                             SHD::Shader::eVertex,
                             SHD::Shader::kNullId,
                             lNum
                        };
        auto            lRequest = EDIT::InsertRequest<SHD::ShaderDef&, 0>{lDef};

        EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
    }

    //!
    void Editor::delVertexShader()
    {
        auto    lSelectionModel = pthis->mUI.vertexShaderList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lNum = lSelectionModel->selectedIndexes().front().row();
            SHD::ShaderDef  lDef = pthis->mVertices[lNum];
            lDef.mIndex = lNum;
            auto            lRequest = EDIT::RemoveRequest<SHD::ShaderDef&, 0>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }

    //!
    void Editor::addFragmentShader()
    {
        auto            lNum = pthis->mFragments.count();
        SHD::ShaderDef  lDef{QString{"Fragment shader"},
                             kDefaultFragmentShader,
                             SHD::Shader::eFragment,
                             SHD::Shader::kNullId,
                             lNum
                        };
        auto            lRequest = EDIT::InsertRequest<SHD::ShaderDef&, 1>{lDef};

        EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
    }

    //!
    void Editor::delFragmentShader()
    {
        auto    lSelectionModel = pthis->mUI.fragmentShaderList->selectionModel();
        if (lSelectionModel->hasSelection())
        {
            auto            lNum = lSelectionModel->selectedIndexes().front().row();
            SHD::ShaderDef  lDef = pthis->mFragments[lNum];
            lDef.mIndex = lNum;
            auto            lRequest = EDIT::RemoveRequest<SHD::ShaderDef&, 1>{lDef};

            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);
        }
    }


//------------------------------------------------------------------------------
//                                   Renommage
//------------------------------------------------------------------------------

    /*! Slot appel� sur la fermeture de l'�diteur d'items des listes d'�l�ments.
     */
    void Editor::close(QWidget* pE, QAbstractItemDelegate::EndEditHint pH)
    {
        if (pH == QAbstractItemDelegate::SubmitModelCache)
        {
            for(auto lObject = pE->parent(); lObject != nullptr; lObject = lObject->parent())
            {
                if (auto lList = dynamic_cast<QListWidget*>(lObject))
                {
                    if      (lList == pthis->mUI.programList)
                    {
                        requestRenameElement(0, lList->currentRow(), lList->currentItem()->text());
                    }
                    else if (lList == pthis->mUI.vertexShaderList)
                    {
                        requestRenameElement(1, lList->currentRow(), lList->currentItem()->text());
                    }
                    else if (lList == pthis->mUI.fragmentShaderList)
                    {
                        requestRenameElement(2, lList->currentRow(), lList->currentItem()->text());
                    }
                    break;
                }
            }
        }
    }

    /*!
     */
    void Editor::requestRenameElement(int pType, int pIndex, QString pName)
    {
        switch(pType)
        {
            case 0:
            {
                Renamer lRename{pIndex, pName};
                auto    lRequest = EDIT::EditRequest<const Renamer&, 0>{lRename};

                EDIT::SubjectList{EDIT::Controller::subject(pthis->mUIController)}.perform(&lRequest);
            } break;
            case 1:
            {
                Renamer lRename{pIndex, pName};
                auto    lRequest = EDIT::EditRequest<const Renamer&, 1>{lRename};

                EDIT::SubjectList{EDIT::Controller::subject(pthis->mUIController)}.perform(&lRequest);
            } break;
            case 2:
            {
                Renamer lRename{pIndex, pName};
                auto    lRequest = EDIT::EditRequest<const Renamer&, 2>{lRename};

                EDIT::SubjectList{EDIT::Controller::subject(pthis->mUIController)}.perform(&lRequest);
            } break;
            default:
            break;
        }
    }

    /*!
     */
    void Editor::setElementName(int pType, int pIndex, QString pName)
    {
        switch(pType)
        {
            case 0:
            {
                pthis->mPrograms[pIndex].mName = pName;
                pthis->mUI.programList->item(pIndex)->setText(pName);
            } break;
            case 1:
            {
                pthis->mVertices[pIndex].mName = pName;
                pthis->mUI.vertexShaderList->item(pIndex)->setText(pName);
            } break;
            case 2:
            {
                pthis->mFragments[pIndex].mName = pName;
                pthis->mUI.fragmentShaderList->item(pIndex)->setText(pName);
            } break;
            default:
            break;
        }
    }


    /*!
     */
    QString Editor::elementName(int pType, int pIndex) const
    {
        switch(pType)
        {
            case 0: return pthis->mPrograms[pIndex].mName;
            case 1: return pthis->mVertices[pIndex].mName;
            case 2: return pthis->mFragments[pIndex].mName;
            default: return QString::null;
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canBuild() const
    {
        return pthis->mUI.programList->selectionModel()->hasSelection();
    }

    /*!
     */
    void Editor::doBuild()
    {
        pthis->mUI.infoLog->setPlainText("");
        auto    lItems = pthis->mUI.programList->selectedItems();

        if (!lItems.isEmpty())
        {
            int     lRow = pthis->mUI.programList->row(lItems.front());
            build(lRow);
        }
    }

    /*!
     */
    bool Editor::canBuildAll() const
    {
        return !pthis->mPrograms.isEmpty() ||
               !pthis->mVertices.isEmpty() ||
               !pthis->mFragments.isEmpty();
    }

    /*!
     */
    void Editor::doBuildAll()
    {
        pthis->mUI.infoLog->setPlainText("");
        for(int p = 0; p < pthis->mPrograms.count(); ++p)
        {
            if (!build(p))
            {
                return;
            }
        }
    }


    /*!
     */
    bool Editor::build(int pProgram)
    {
        if ((0 <= pProgram) && (pProgram < pthis->mPrograms.count()))
        {
            auto&   lProgramDef = pthis->mPrograms[pProgram];

            QMap<SHD::Shader::Id, QString>  lCodes{};

            try
            {
                // Tiens compte de l'�dition en cours.
                pthis->cacheShader();

                // 1 - M�morise le code des shaders dans la bilbioth�que (qui est valide).
                SHD::ShaderDef  lShaderDef{ QString::null, QString::null, SHD::Shader::eNone, SHD::Shader::kNullId, -1};
                for(auto m : lProgramDef.mModules)
                {
                    lShaderDef.mId = m;
                    std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->getShader(lShaderDef);
                    lCodes[lShaderDef.mId] = lShaderDef.mCode;
                }

                // 2 - R�cup�re le [nouveau] code des shaders et l'envoie dans la bilbioth�que.
                for(auto m : lProgramDef.mModules)
                {
                    for(const auto& v : pthis->mVertices)
                    {
                        if (v.mId == m)
                        {
                            std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->editShader(v);
                            break;
                        }
                    }
                    for(const auto& f : pthis->mFragments)
                    {
                        if (f.mId == m)
                        {
                            std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->editShader(f);
                            break;
                        }
                    }
                }

                // 3 - Tente la compilation des programmes.
                auto    lLog    = std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->build(lProgramDef);

                if (!lProgramDef.mIsValid)
                {
                    throw std::invalid_argument(qPrintable(lLog));
                }
                else
                {
                    pthis->appendLog(lLog, &lProgramDef);
                }
            }
            catch(std::exception& e)
            {
                pthis->appendLog(e.what());

                // 4 - Restaure le code des shaders en cas d'erreur.
                SHD::ShaderDef  lShaderDef{ QString::null, QString::null, SHD::Shader::eNone, SHD::Shader::kNullId, -1};
                for(const auto& ic : lCodes.toStdMap())
                {
                    lShaderDef.mId   = ic.first;
                    lShaderDef.mCode = ic.second;
                    std::dynamic_pointer_cast<SHD::Controller>(pthis->mController)->editShader(lShaderDef);
                }
            }

            return lProgramDef.mIsValid;
        }

        return false;
    }

//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    CORE::Translated::Updater Editor::buildTranslater()
    {
        return [=]()
        {
            pthis->mUI.retranslateUi(this);
        };
    }
}
