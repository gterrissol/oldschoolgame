/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QSHaDers.hh"

/*! @file IDE/Program/QSHaDers/QSHaDers.cc
    @brief M�thodes (non-inline) des classes SHD::ImportExportShaders::Shaders, SHD::ShadersActivator.
    @author @ref Guillaume_Terrissol
    @date 16 Avril 2015 - 26 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>
#include <QXmlStreamWriter>

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "PAcKage/LoaderSaver.hh"
#include "SHaDers/Const.hh"
#include "SHaDers/Shader.hh"
#include "TILE/Enum.hh"

#include "Editor.hh"


#define SHD_DECLARE_ATTRIBUTE_VERTEX3   "in vec3  " SHD_ATTRIBUTE_VERTEX  ";\n"
#define SHD_DECLARE_ATTRIBUTE_VERTEX4   "in vec4  " SHD_ATTRIBUTE_VERTEX  ";\n"
#define SHD_DECLARE_ATTRIBUTE_NORMAL    "in vec3  " SHD_ATTRIBUTE_NORMAL  ";\n"
#define SHD_DECLARE_ATTRIBUTE_COLOR4    "in vec4  " SHD_ATTRIBUTE_COLOR   ";\n"
#define SHD_DECLARE_ATTRIBUTE_COLOR3    "in vec3  " SHD_ATTRIBUTE_COLOR   ";\n"
#define SHD_DECLARE_ATTRIBUTE_TEX       "in vec2  " SHD_ATTRIBUTE_TEX    "s;\n"
#define SHD_DECLARE_ATTRIBUTE_TEX0      "in vec2  " SHD_ATTRIBUTE_TEX    "0;\n"
#define SHD_DECLARE_ATTRIBUTE_TEX1      "in vec2  " SHD_ATTRIBUTE_TEX    "1;\n"

namespace QSHD
{
//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de la biblioth�que de shaders.
        @version 0.3
     */
    class ImportExportShaders : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportShaders() = default;                                    //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!<
    };


    /*!
     */
    bool ImportExportShaders::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        pWriter.setAutoFormattingIndent(1);

        U32 lCount{};
        pLoader >> lCount;
        pLoader.rollback(U32{sizeof(lCount)});
        data()->write<eU32>(pWriter, pLoader, QStringLiteral("Shader count"));

        for(U32 lS = k0UL; lS < lCount; ++lS)
        {
            data()->begin(pWriter, pLoader, QStringLiteral("Shader #%1").arg(1 + lS));
            data()->write<eU8>(pWriter, pLoader, QStringLiteral("Type"));
            data()->write<eString>(pWriter, pLoader, QStringLiteral("Code"));
            data()->end(pWriter, pLoader, QStringLiteral("Shader #%1").arg(1 + lS));
        }

        pLoader >> lCount;
        pLoader.rollback(U32{sizeof(lCount)});
        data()->write<eU32>(pWriter, pLoader, QStringLiteral("Program count"));
        for(U32 lP = k0UL; lP < lCount; ++lP)
        {
            data()->begin(pWriter, pLoader, QStringLiteral("Program #%1").arg(1 + lP));
            U16 lModuleCount{};
            pLoader >> lModuleCount;
            pLoader.rollback(U32{sizeof(lModuleCount)});
            data()->write<eU16>(pWriter, pLoader, QStringLiteral("Module count"));
            data()->begin(pWriter, pLoader, QStringLiteral("Module list"));
            for(U16 lM = k0UW; lM < lModuleCount; ++lM)
            {
                data()->write<eU16>(pWriter, pLoader);
            }
            data()->end(pWriter, pLoader, QStringLiteral("Module list"));
            data()->end(pWriter, pLoader, QStringLiteral("Program #%1").arg(1 + lP));
        }

        return true;
    }


    /*! Cr�e les donn�es d'un gestionnaire vide (pour la cr�ation d'un <b>pak</b> file).
     */
    void ImportExportShaders::doMake(PAK::Saver& pSaver)
    {
        // 6 shaders.
        pSaver << U32{6};
        // Rendu : 1 vertex shader.
        pSaver << SHD::Shader::eVertex;
        pSaver << String{
                    "#version 130\n"
                    "\n"
                    "uniform mat4 uPVM;\n"
                    "uniform mat4 uModel;\n"
                    "uniform mat4 uInvModel;\n"
                    "uniform bool uIsLit;\n"
                    "\n"
                    SHD_DECLARE_ATTRIBUTE_VERTEX3
                    SHD_DECLARE_ATTRIBUTE_NORMAL
                    SHD_DECLARE_ATTRIBUTE_COLOR4
                    SHD_DECLARE_ATTRIBUTE_TEX0
                    "\n"
                    "smooth out vec4 ioNormal;\n"
                    "smooth out vec4 ioColor;\n"
                    "smooth out vec2 ioTexCoord0;\n"
                    "\n"
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    gl_Position = uPVM * vec4(" SHD_ATTRIBUTE_VERTEX ", 1.0);\n"
                    "\n"
                    "    if (uIsLit)\n"
                    "    {\n"
                    "        ioNormal = normalize(uModel * vec4(" SHD_ATTRIBUTE_NORMAL ", 0.0));\n"
                    "        ioColor = vec4(1.0, 1.0, 1.0, 1.0);\n"     // Pour l'instant (tout �a n'est pas encore parfaitement g�r�...)
                    "        ioTexCoord0 = " SHD_ATTRIBUTE_TEX "0;\n"
                    "    }\n"
                    "    else\n"
                    "    {\n"
                    "        ioColor = " SHD_ATTRIBUTE_COLOR ";\n"
                    "        ioTexCoord0 = vec2(0.0, 0.0);\n"
                    "    }\n"
                    "}\n"
                  };
        // Rendu : 1 fragment shader.
        pSaver << SHD::Shader::eFragment;
        pSaver << String{
                    "#version 130\n"
                    "\n"
                    "uniform sampler2D uTexture0;\n"
                    "uniform vec4      uLightDir;\n"
                    "uniform vec4      uAmbient;\n"
                    "uniform vec4      uDiffuse;\n"
                    "uniform bool      uIsLit;\n"
                    "\n"
                    "smooth in vec4 ioNormal;\n"
                    "smooth in vec4 ioColor;\n"
                    "smooth in vec2 ioTexCoord0;\n"
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    vec4 color;\n"
                    "    if (uIsLit)\n"
                    "    {\n"
                    "        color = uAmbient;\n"
                    "        vec4 n = normalize(ioNormal);\n"
                    "        float NdotL = max(dot(uLightDir, n), 0.0);\n"
                    "        if (0.0 <= NdotL)\n"
                    "        {\n"
                    "            color += uDiffuse * NdotL;\n"
                    "        }\n"
                    "        color *= texture2D(uTexture0, ioTexCoord0);\n"
                    "    }\n"
                    "    else\n"
                    "    {\n"
                    "        color = ioColor;\n"
                    "    }\n"
                    "    gl_FragColor = color;\n"
                    "}\n"
                  };
        // Picking : 1 vertx shader.
        pSaver << SHD::Shader::eVertex;
        pSaver << String{
                    "#version 130\n"
                    "\n"
                    "uniform mat4 uPV;\n"
                    "uniform mat4 uModel;\n"
                    "\n"
                    SHD_DECLARE_ATTRIBUTE_VERTEX3
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    gl_Position = uPV * uModel * vec4(" SHD_ATTRIBUTE_VERTEX ", 1.0);\n"
                    "}\n"
                  };
        // Picking : 1 fragment shader.
        pSaver << SHD::Shader::eFragment;
        pSaver << String{
                    "#version 130\n"
                    "\n"
                    "uniform vec4 uPickingColor;\n"
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    gl_FragColor = uPickingColor;\n"
                    "}"
                  };
        // MegaTexture : 1 vertex shader.
        pSaver << SHD::Shader::eVertex;
        pSaver << String{
                    "#version 330\n"
                    "\n"
                    "uniform mat4 uPVM;\n"
                    "uniform mat4 uModel;\n"
                    "uniform mat4 uInvModel;\n"
                    "uniform bool uIsLit;\n"
                    "\n"
                    SHD_DECLARE_ATTRIBUTE_VERTEX3
                    SHD_DECLARE_ATTRIBUTE_NORMAL
                    SHD_DECLARE_ATTRIBUTE_TEX
                    "in vec2  iFragsTxy[4];\n"
                    "in ivec4 iTTs;\n"
                    "in int   iEnabled;\n"
                    "in int   iBlending;\n"
                    "\n"
                    "out VS_OUT\n"
                    "{\n"
                    "  smooth  vec4    normal;\n"
                    "  smooth  vec2    uv;\n"   // Tex coord [1, 32] x [1, 32]
                    "\n"
                    "  flat    vec2    fragsTxy[4];\n"   //   identifie les fragments n�co, n�li
                    "  flat    ivec4   tts;\n"
                    "  flat    int     enabled;\n"
                    "  flat    int     blending;\n"
                    "} vs_out;\n"
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    gl_Position = uPVM * vec4(" SHD_ATTRIBUTE_VERTEX ", 1.0);\n"
                    "    vs_out.normal = normalize(uModel * vec4(" SHD_ATTRIBUTE_NORMAL ", 0.0));\n"
                    "\n"
                    "    vs_out.fragsTxy[0]  = iFragsTxy[0];\n"
                    "    vs_out.fragsTxy[1]  = iFragsTxy[1];\n"
                    "    vs_out.fragsTxy[2]  = iFragsTxy[2];\n"
                    "    vs_out.fragsTxy[3]  = iFragsTxy[3];\n"
                    "    vs_out.tts          = iTTs;\n"
                    "    vs_out.enabled      = iEnabled;\n"
                    "    vs_out.blending     = iBlending;\n"
                    "    vs_out.uv           = iTexCoords;\n"
                    "}\n"
                  };
        // MegaTexture : 1 fragment shader.
        pSaver << SHD::Shader::eFragment;
        pSaver << String{
                    "#version 330\n"
                    "\n"
                    "uniform sampler2D uTilings[4];\n"
                    "uniform vec4      uLightDir;\n"
                    "uniform vec4      uAmbient;\n"
                    "uniform vec4      uDiffuse;\n"
                    "uniform bool      uIsLit;\n"
                    "uniform sampler2D uMasks;\n"
                    "\n"
                    "in VS_OUT\n"
                    "{\n"
                    "  smooth  vec4    normal;\n"
                    "  smooth  vec2    uv;\n"
                    "\n"
                    "  flat    vec2    fragsTxy[4];\n"
                    "  flat    ivec4   tts;\n"
                    "  flat    int     enabled;\n"
                    "  flat    int     blending;\n"
                    "} fs_in;\n"
                    "\n"
                    "vec4 mtBlending(int blend, vec2 maskCoords)\n"
                    "{\n"
                    "  vec2  coord = vec2(maskCoords.x * 2.0, 1.0 + blend * 4.0 + maskCoords.y * 2.0) / 16.0;\n"
                    "  return vec4(\n"
                    "             texture2D(uMasks, coord + vec2(0.0625, 0.0)).r,\n"
                    "             texture2D(uMasks, coord + vec2(0.3125, 0.0)).r,\n"
                    "             texture2D(uMasks, coord + vec2(0.5625, 0.0)).r,\n"
                    "             texture2D(uMasks, coord + vec2(0.8125, 0.0)).r\n"
                    "         );\n"//texture2D(uMasks, (vec2({1.0|5.0|9.0|13.0}, 1.0 + fs_in.blending * 4.0) + uv * 2.0) / 16.0).r
                    "}\n"
                    "\n"
                    "vec4 mtTexColor(int t, vec2 texCoords)\n"
                    "{\n"
                    "    if (t < 2)\n"
                    "        if (t == 0)\n"
                    "            return texture2D(uTilings[0], texCoords);\n"
                    "        else\n"
                    "            return texture2D(uTilings[1], texCoords);\n"
                    "    else\n"
                    "        if (t == 2)\n"
                    "            return texture2D(uTilings[2], texCoords);\n"
                    "        else\n"
                    "            return texture2D(uTilings[3], texCoords);\n"
                    "}\n"
                    "\n"
                    "out vec4 fragColor;\n"
                    "\n"
                    "void main()\n"
                    "{\n"
                    "    vec4    color = uAmbient;\n"
                    "    vec4    n = normalize(fs_in.normal);\n"
                    "    float   NdotL = max(dot(uLightDir, n), 0.0);\n"
                    "    if (0.0 <= NdotL)\n"
                    "    {\n"
                    "        color += uDiffuse * NdotL;\n"
                    "    }\n"
                    "\n"
                    "    if (fs_in.enabled != 0)\n"
                    "    {\n"
                    "        vec4    texColors[4];\n"   // = vec4(0.0, 0.0, 0.0, 0.0);
                    "        vec2    uv = fract(fs_in.uv);\n"
                    "        vec4    blends = mtBlending(fs_in.blending, uv);\n"
                    "\n"
                    "        uv /= " +
                  String{std::to_string(TILE::eTilingsInTextureSide * TILE::eTilingSize / 100.0F).c_str()} + ";\n\n"
                    "        texColors[0] = mtTexColor(fs_in.tts.x, vec2(fs_in.fragsTxy[0].x + uv.x, fs_in.fragsTxy[0].y + uv.y));\n"
                    "        texColors[1] = mtTexColor(fs_in.tts.y, vec2(fs_in.fragsTxy[1].x + uv.x, fs_in.fragsTxy[1].y + uv.y));\n"
                    "        texColors[2] = mtTexColor(fs_in.tts.z, vec2(fs_in.fragsTxy[2].x + uv.x, fs_in.fragsTxy[2].y + uv.y));\n"
                    "        texColors[3] = mtTexColor(fs_in.tts.w, vec2(fs_in.fragsTxy[3].x + uv.x, fs_in.fragsTxy[3].y + uv.y));\n"
                    "        //A0 B1\n"
                    "        //C2 D3\n"
                    "        vec4    texColor = blends[0] * texColors[0] + blends[1] * texColors[1] + blends[2] * texColors[2] + blends[3] * texColors[3];\n"
                    "        color *= texColor;\n"
                    "    }\n"
                    "    else\n"
                    "    {\n"
                    "        discard;\n"
                    "    }\n"
                    "\n"
                    "    fragColor = color;\n"
                    "}\n"
                   };
        // 3 programmes.
        pSaver << U32{3};
        // 1 pour le rendu (2 shaders).
        pSaver << U16{2};
        pSaver << SHD::Shader::Id{0};
        pSaver << SHD::Shader::Id{1};
        // 1 pour le picking (2 shaders).
        pSaver << U16{2};
        pSaver << SHD::Shader::Id{2};
        pSaver << SHD::Shader::Id{3};
        // 1 pour la MegaTexture (2 shaders).
        pSaver << U16{2};
        pSaver << SHD::Shader::Id{4};
        pSaver << SHD::Shader::Id{5};
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class ShadersActivator : public CORE::IDEActivator
    {
    public:
                ShadersActivator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~ShadersActivator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("program.glsl",    [=]() { return CORE::UIService::Widget{new Editor{pContext}}; });

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportShaders>("shd", lExtensionDir);

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->registerType(QStringLiteral("shd"));
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("program.glsl");

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("shd");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QSHD::ShadersActivator)
OSGI_END_REGISTER_ACTIVATORS()
