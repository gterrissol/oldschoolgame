/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROGRAM_EDITOR_HH
#define PROGRAM_EDITOR_HH

/*! @file IDE/Program/Editor.hh
    @brief En-t�te de la classe PROGRAM::Editor.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2015 - 19 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

namespace Program
{
//------------------------------------------------------------------------------
//                              Editeur de Scripts
//------------------------------------------------------------------------------

    /*! @brief Editeur de script.
        @version 0.5

        Classe de base pour un �diteur de l'onglet des Scripts.
     */
    class Editor : public QWidget
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Editor();                               //!< Constructeur.
virtual         ~Editor();                              //!< Destructeur.
        //@]
        //! @name Compilation
        //@{
        void    build();                                //!< Compilation du programme s�lectionn�.
        void    buildAll();                             //!< Compilation de tous les programmes de l'onglet actuel.
        //@}
    protected:
        //! @name Gestion d'�v�nements
        //@{
        void    changeEvent(QEvent* pEvent) override;   //!< Ev�nement de changement.
virtual void    editNow() = 0;                          //!< Activation de l'�diteur.
virtual void    endEdit() = 0;                          //!< D�sactivation de l'�diteur.
        //@}
    private:

virtual bool    canBuild() const = 0;                   //!< Possibilit� de compilation ?
virtual void    doBuild() = 0;                          //!< Compilation.
virtual bool    canBuildAll() const = 0;                //!< Possibilit� de compilation ?
virtual void    doBuildAll() = 0;                       //!< Compilation de tous les programmes.
    };
}

#endif  // De QOUT_EDITOR_HH
