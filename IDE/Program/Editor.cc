/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Program/Editor.cc
    @brief M�thodes (non-inline) de la classe Program::Editor.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2015 - 19 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QEvent>

namespace Program
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    //! Par d�faut.
    Editor::Editor() = default;


    //! Par d�faut.
    Editor::~Editor() = default;


//------------------------------------------------------------------------------
//                                  Compilation
//------------------------------------------------------------------------------

    //!
    void Editor::build()
    {
        if (canBuild())
        {
            doBuild();
        }
    }

    //!
    void Editor::buildAll()
    {
        if (canBuildAll())
        {
            doBuildAll();
        }
    }


//------------------------------------------------------------------------------
//                             Gestion d'Ev�nements
//------------------------------------------------------------------------------

    /*! @param pEvent Si �v�nement de changement d'�tat d'activation, appelle, selon le nouvel �tat, editNow() ou endEdit().
     */
    void Editor::changeEvent(QEvent* pEvent)
    {
        if (pEvent->type() == QEvent::EnabledChange)
        {
            if (isEnabled())
            {
                editNow();
            }
            else
            {
                endEdit();
            }
        }
    }
}
