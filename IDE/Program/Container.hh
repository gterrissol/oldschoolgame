/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef PROGRAM_CONTAINER_HH
#define PROGRAM_CONTAINER_HH

/*! @file IDE/Program/Container.hh
    @brief En-t�te de la classe Program::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Container.hh"

class QTabWidget;

namespace Program
{
    /*! @brief Cat�gorie @ref category_program "Scripts".
        @version 0.2
     */
    class Container : public CORE::Container
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
        Container(OSGi::Context* pContext);                                 //!< Constructeur.
virtual ~Container();                                                       //!< Destructeur.
        //@}
    protected:

        void        addWidget(Widget pWidget) override;                     //!< Ajout d'un onglet.
        QStringList sortWidgets(const QStringList& pNames) const override;  //!< Tri des widgets.

    private:

        QTabWidget* mDataTabs;                                              //!< Conteneur d'onglets.
    };
}

#endif  // PROGRAM_CONTAINER_HH
