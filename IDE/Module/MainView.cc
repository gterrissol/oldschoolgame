/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MainView.hh"

/*! @file IDE/Module/MainView.cc
    @brief M�thodes (non-inline) de la classe Module::MainView.
    @author @ref Guillaume_Terrissol
    @date 25 Novembre 2014 - 30 Novembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace Module
{
    //!
    MainView::~MainView() = default;

    //! @return Le widget "vue sur le monde".
    QWidget* MainView::worldView() const
    {
        return getWorldView();
    }
}
