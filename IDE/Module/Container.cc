/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/Module/Container.cc
    @brief M�thodes (non-inline) de la classe Module::Container.
    @author @ref Guillaume_Terrissol
    @date 16 Janvier 2013 - 22D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>
#include <QFrame>
#include <QLayout>
#include <QTabWidget>

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/MenuService.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"

#include "Editor.hh"
#include "MainView.hh"

namespace Module
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @page category_module Edition du monde 3D.
        @todo A r�diger
     */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de CORE::Container.
        @version 0.2
     */
    class Container::ContainerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Container)
    public:
        ContainerPrivate(Container* pThat);

        QFrame*     mMainFrame;
        MainView*   mMainView;
        QTabWidget* mTools;
        QTabWidget* mEditors;
    };


    /*!
     */
    Container::ContainerPrivate::ContainerPrivate(Container* pThat)
        : q{pThat}
        , mMainFrame{}
        , mMainView{}
        , mTools{}
        , mEditors{}
    {
        auto    mMain   = new QWidget{pThat};
        q->setCentralWidget(mMain);
        auto    lLayout = new QHBoxLayout{mMain};

        mTools   = new QTabWidget{mMain};
        mTools->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        lLayout->addWidget(mTools);

        mMainFrame = new QFrame{mMain};
        mMainFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        new QGridLayout{mMainFrame};
        lLayout->addWidget(mMainFrame);

        mEditors = new QTabWidget{mMain};
        mEditors->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        lLayout->addWidget(mEditors);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Container::Container(OSGi::Context* pContext)
        : CORE::Container("module", tr("3D"), QStringLiteral(":/ide/module/icons/3D"), pContext)
        , pthis(this)
    {
        auto lService = pContext->services()->findByTypeAndName<CORE::MenuService>("fr.osg.ide.menu");

        // => passer les param�tres en constructeurs param�tr�s.
        add(lService->menu("engine").from(this).
            text(tr("En&gine")).
            sync(   "file"));
        add(lService->item("engine", "start").from(this).
            text(tr("&Start")).
            icon(   ":/ide/core/icons/play").
            key (tr("F5", "Engine | Start")).
            sync(   "file + !engine").
            tip(tr("Launches the engine.")));
        add(lService->item("engine", "restart").from(this).
            text(tr("&Restart")).
            key (tr("F7", "Engine | Restart")).
            sync(   "file + engine").
            tip (tr("Stops, then relaunches the engine.")));
        add(lService->item("engine", "stop").from(this).
            text(tr("&Stop")).
            icon(   ":/ide/core/icons/stop").
            key (tr("F5", "Engine | Stop")).
            sync(   "file + engine").
            tip (tr("Stops the engine.")));

        auto    lMenuService = pContext->services()->findByTypeAndName<CORE::MenuService>("fr.osg.ide.menu");
        lMenuService->connect(QStringLiteral("module.file.new"),   QStringLiteral("data.file.new"));
        lMenuService->connect(QStringLiteral("module.file.open"),  QStringLiteral("data.file.open"));
        lMenuService->connect(QStringLiteral("module.file.save"),  QStringLiteral("data.file.save"));
        lMenuService->connect(QStringLiteral("module.file.close"), QStringLiteral("data.file.close"));
    }


    /*!
     */
    Container::~Container() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::engine(QString pItem)
    {
        qDebug() << QString("Doing %1 on engine").arg(pItem);
    }


    /*! @return Le widget "vue sur le monde".
     */
    QWidget* Container::worldView() const
    {
        return pthis->mMainView->worldView();
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::addWidget(Widget pWidget)
    {
        Q_D(Container);

        QTabWidget* lTabs = nullptr;

        if      (pWidget->objectName().startsWith(QStringLiteral("module.tool.")))
        {
            lTabs = d->mTools;
        }
        else if (pWidget->objectName().startsWith(QStringLiteral("module.editor.")))
        {
            lTabs = d->mEditors;
        }
        else if (pWidget->objectName() == QStringLiteral("module.3D"))
        {
            if ((pthis->mMainView = dynamic_cast<MainView*>(pWidget.get())))
            {
                pWidget->setParent(d->mMainFrame);
                d->mMainFrame->layout()->addWidget(pWidget.get());
                emit worldViewChanged(pthis->mMainView->worldView());
                connect(pthis->mMainView->worldView(), SIGNAL(edit(EDIT::Controller::Ptr)),  SLOT(onEdit(EDIT::Controller::Ptr)));
                connect(pthis->mMainView->worldView(), SIGNAL(leave(EDIT::Controller::Ptr)), SLOT(onLeave(EDIT::Controller::Ptr)));
            }
        }

        if (lTabs != nullptr)
        {
            int lIndex = lTabs->addTab(pWidget.get(), pWidget->windowIcon(), pWidget->windowTitle());

            if  (auto lEditor = dynamic_cast<Editor*>(pWidget.get()))
            {
                connect(this, SIGNAL(worldViewChanged(QWidget*)), lEditor, SLOT(setWorldView(QWidget*)));
                if (pthis->mMainView != nullptr)
                {
                    lEditor->setWorldView(pthis->mMainView->worldView());
                }
            }

            if (dynamic_cast<CORE::Translated*>(pWidget.get()) != nullptr)
            {
                QWidget*    lW = pWidget.get();
                add([=]() { lTabs->setTabText(lIndex, lW->windowTitle()); });
            }
        }

        pWidget.release();
    }

    /*!
     */
    void Container::onEdit(EDIT::Controller::Ptr pController)
    {
        if (pController)
        {
            QString lExtension = pController->extension();

            context()->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->open(lExtension);
            qDebug() << "openDocument(" << lExtension << ");";
        }
        else
        {
            context()->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->closeAll();
            qDebug() << "close all documents";
        }
    }

    /*!
     */
    void Container::onLeave(EDIT::Controller::Ptr pController)
    {
        if (pController)
        {
            QString lExtension = pController->extension();

            context()->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->close(lExtension);
            qDebug() << "closeDocument(" << lExtension << ");";
        }
    }

    /*!
     */
    QStringList Container::sortWidgets(const QStringList& pNames) const
    {
        auto lOrderedTabs = QString::fromStdString(context()->properties(BUNDLE_NAME)->get("tabs", "")).split(",") + pNames;
        lOrderedTabs.removeDuplicates();

        return lOrderedTabs;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("module.container", [=]() { return CORE::UIService::Widget{new Container(pContext)}; });

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->declare(CORE::Synchronizer{"module.undo"}.enter("positive module.undo").leave("null module.undo"));
                    lSyncService->declare(CORE::Synchronizer{"module.redo"}.enter("positive module.redo").leave("null module.redo"));
                    lSyncService->declare(CORE::Synchronizer{"engine"}.     enter("start engine").        leave("stop engine"));
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("module.container");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(Module::Activator)
OSGI_END_REGISTER_ACTIVATORS()
