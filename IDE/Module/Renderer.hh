/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_RENDERER_HH
#define EDIT_RENDERER_HH

/*! @file IDE/Module/Renderer.hh
    @brief En-t�te de la classe EDIT::Renderer.
    @author @ref Guillaume_Terrissol
    @date 16 Novembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "OBJect/GameObject.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Shared.hh"

class QString;

namespace EDIT
{
//------------------------------------------------------------------------------
//                               Renderer Editeur
//------------------------------------------------------------------------------

    /*! @brief Renderer (mode �diteur)
        @version 0.2
     */
    class Renderer : public RDR::Renderer, public STL::enable_shared_from_this<Renderer>
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef std::weak_ptr<OBJ::GameEntity> EntityPtr;     //!< Pointeur sur entit� de jeu.
        typedef std::weak_ptr<Renderer>        RendererPtr;   //!< Pointeur sur renderer.
        //@}
        //! @name Interface
        //@{
        Renderer(EntityPtr pEdited);
virtual ~Renderer();                      //!< Destructeur.
        //@}

    protected:

        void    watchRendererOf(EntityPtr pChild);                 //!< 
//        void    destroyed(EntityPtr pChild, RendererPtr pRenderer);   //!< Slot sur destruction du renderer d'une sous-entit�.

    private:

virtual void    created(EntityPtr pChild, RendererPtr pRenderer);     //!< Slot sur cr�ation du renderer d'une sous-entit�.

        FREE_PIMPL()
    };
}

#endif  // De EDIT_RENDERER_HH
