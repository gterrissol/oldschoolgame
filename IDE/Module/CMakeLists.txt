add_ide_bundle(fr.osg.ide.module VERSION 1.1.0 SOURCES
    Renderer.cc MainView.cc Editor.cc Container.cc
    Renderer.hh MainView.hh Editor.hh Container.hh
)

foreach(module QT3D QWORld QTILe QOUTside QUNDerground QUNIverse)
    add_subdirectory(${module})
endforeach()
