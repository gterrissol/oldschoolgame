/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QUNI_QUNIVERSE_HH
#define QUNI_QUNIVERSE_HH

/*! @file IDE/Module/QUNIverse/All.hh
    @brief Interface publique du module @ref QUNIverse.
    @author @ref Guillaume_Terrissol
    @date 1er Septembre 2003 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QUNI  //! Editeur d'univers.
{
    /*! @namespace QUNI
        @version 0.25

     */

    /*! @defgroup QUNIverse QUNIverse : Editeur d'univers
        <b>namespace</b> QUNI.
     */
}

#include "Editor.hh"

#endif  // De QUNI_QUNIVERSE_HH
