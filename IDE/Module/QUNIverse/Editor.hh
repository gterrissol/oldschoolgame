/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QUNI_EDITOR_HH
#define QUNI_EDITOR_HH

#include "QUNIverse.hh"

/*! @file IDE/Module/QUNIverse/Editor.hh
    @brief En-t�te de la classe UNI::Controller.
    UNI::UniverseDatum & QUNI::Editor.
    @author @ref Guillaume_Terrissol
    @date 22 Juin 2003 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Module/QT3D/Controller.hh"
#include "Module/QT3D/GameEntityService.hh"
#include "PAcKage/Handle.hh"
#include "UNIverse/UNIverse.hh"

namespace UNI
{
//------------------------------------------------------------------------------
//                                  Controller
//------------------------------------------------------------------------------

    /*! @brief Contr�leur d'Univers.
        @version 0.4
     */
    class Controller : public QT3D::GenericController<Universe>
    {
        BEFRIEND_STD_ALLOCATOR()
    public:
        //! @name Cr�ation
        //@{
        using EntityPtr = QT3D::GameEntityService::EntityPtr;               //!< Pointeur sur entit�.
 static Ptr             make(EntityPtr pEntity, OSGi::Context* pContext);   //!< Cr�ation.
        //@}
        bool            insertWorld(PAK::FileHdl pWorldHdl);                //!< Insertion d'un monde.
        bool            removeWorld(PAK::FileHdl pWorldHdl);                //!< Retrait d'un monde.

    protected:
        //! @name Constructeur & destructeur
        //@{
                        Controller(EditedPtr      pUniverse,
                                   OSGi::Context* pContext);                //!< Constructeur.
virtual                 ~Controller();                                      //!< Destructeur.
        //@}

    private:

virtual EDIT::Subject*  asSubject();                                        //!< R�cup�ration du sujet d'�dition.
virtual QString         getExtension() const;                               //!< Extension associ�e � l'objet contr�l�
virtual void            doSave() const;                                     //!< Sauvegarde de l'�tat de l'objet contr�l�.
virtual RendererPtr     getRenderer();                                      //!< R�cup�ration du rendrerer du sujet �dit�.

        Q_CUSTOM_DECLARE_PRIVATE(Controller)
    };

#if 0
    /*! @brief Mode d'�dition des univers.
        @version 0.2
     */
    class Mode : public QT3D::StandardMode
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Mode(WidgetPtr pWindow);  //!< Constructeur.
virtual         ~Mode();                   //!< Destructeur.
        //@}
    };
#endif  // 0
}

#if 0
namespace QUNI
{
//------------------------------------------------------------------------------
//                              Editeur d'Univers
//------------------------------------------------------------------------------

    /*! @brief Editeur d'univers.
        @version 0.25

        Interface QT d'�dition d'univers.
     */
    class Editor : public QPNL::QGenericEditor<UNI::Universe>, public QT::QObserver
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Editor();                                //!< Constructeur par d�faut.
static  QTAB::QTab* builder();                                  //!< 
virtual             ~Editor();                               //!< Destructeur.
        //@}

    protected:
        //! @name Changement de langue
        //@{
virtual void        onLanguageChange();                         //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private:
        //! @name Comportement de page
        //@{
virtual void        manageMenuEntry(QAction* entry);            //!< Gestion d'une entr�e du menu du widget.
virtual void        fillPageMenu(QMenu*& pageMenu) const;       //!< Cr�ation du menu du widget.
virtual QIcon       statusIcon() const;                         //!< Ic�ne repr�sentant l'�tat actuel du widget.
        //@}
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(ControllerPtr controller) const; //!< Possibilit� de gestion d'un sujet.
virtual void        manage(ControllerPtr controller);          //!< Gestion effective d'un sujet.
        //@}
        //! @name Ecoute du sujet
        //@{
virtual void        listen(const QT::QAnswer* answer = 0);      //!< Ecoute effective de la cible.
        //@}
        Q_DISABLE_COPY(Editor)
        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}
#endif  // 0

#endif  // De QUNI_EDITOR_HH
