/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QUNIverse.hh"

/*! @file IDE/Module/QUNIverse/QUNIverse.cc
    @brief D�finitions diverses du module QUNI.
    @author @ref Guillaume_Terrissol
    @date 1er Septembre 2003 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Activator.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "Module/QT3D/GameEntityService.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/String.hh"
#include "UNIverse/Const.hh"
#include "UNIverse/GameUniverse.hh"

#include "Editor.hh"

namespace QUNI
{
//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export d'univers.
        @version 0.25
     */
    class ImportExportUniverse : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportUniverse();                                            //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


//------------------------------------------------------------------------------
//                                    Univers
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportUniverse::~ImportExportUniverse() { }


    /*!
     */
    bool ImportExportUniverse::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        U32 lGroupCount = k0UL;
        pLoader >> lGroupCount;
        pLoader.rollback(U32(sizeof(lGroupCount)));

        data()->write<eU32>(pWriter, pLoader, "World group count");

        data()->begin(pWriter, pLoader, "Groups");

        for(U32 lG = k0UL; lG < lGroupCount; ++lG)
        {
            data()->write<eFileHdl>(pWriter, pLoader, QStringLiteral("Group %1").arg(1 + lG));
        }

        data()->end(pWriter, pLoader, "Groups");

        data()->write<eFileHdl>(pWriter, pLoader, "Fragment Manager");

        return true;
    }


    /*! Sauvegarde dans un fichier les donn�es d'un univers minimal (par d�faut).<br>
        Cette m�thode est n�cessaire pour cr�er un nouveau pak file. En effet, tout pak file requiert un
        univers, unique. Or, lors de la cr�ation d'un pak file, il n'est pas s�r que le gestionnaire
        d'object (RSC::ObjectMgr::get()) soit cr�� (il peut d�j� l'�tre, si un pak file est d�j� ouvert;
        mais dans ce cas, la fermeture automatique du pak file ne d�truirait pas le gestionnaire
        imm�diatement, emp�chant de le recr�er).
        @param pSaver  "Fichier" dans lequel �crire les donn�es d'un univers par d�faut
     */
    void ImportExportUniverse::doMake(PAK::Saver& pSaver)
    {
        // Par d�faut, un groupe de mondes.
        pSaver << U32{1};
        pSaver << data()->fileDB()->create("root.grp");
        pSaver << data()->fileDB()->create("game.ttm"); // Gestionnaire de fragments de textures de tuile.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportUniverse>("uni",  lExtensionDir);

                    auto    lGEService = pContext->services()->findByTypeAndName<QT3D::GameEntityService>("fr.osg.ide.module.3d.game_entity");
                    lGEService->checkIn<UNI::Universe>(UNI::Controller::make);
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("uni");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QUNI::Activator)
OSGI_END_REGISTER_ACTIVATORS()
