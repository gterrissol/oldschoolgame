/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Module/QUNIverse/Editor.cc
    @brief M�thodes (non-inline) de la classes UNI::Controller.
    @author @ref Guillaume_Terrissol
    @date 22 Juin 2003 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <map>

#include "APPearance/Primitive.hh"
#include "CoLoR/Colors.hh"
#include "EDITion/Attributes.hh"
#include "Module/Renderer.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "Module/QT3D/Entity.hh"
#include "STL/String.hh"
#include "STL/Vector.hh"
#include "UNIverse/GameUniverse.hh"
#include "UTIlity/Rect.hh"
#include "WORld/BaseWorld.hh"



namespace UNI
{
//------------------------------------------------------------------------------
//                      Renderer d'Univers en Mode Editeur
//------------------------------------------------------------------------------

    /*! @brief Renderer d'univers (IDE)
        @version 0.3
     */
    class RendererIDE : public EDIT::Renderer
    {
    public:
        //! @name Type de pointeur
        //@{
        using UniversePtr   = std::weak_ptr<Universe>;                                  //!< Pointeur sur univers.
        using EntityIDE     = std::shared_ptr<QT3D::Entity>;                            //!< Pointeur sur entit� IDE.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit    RendererIDE(UniversePtr pUniverse);                                 //!< Constructeur.
virtual             ~RendererIDE();                                                     //!< Destructeur.
        //@}

        private:

virtual void        created(EntityPtr pChild, RendererPtr pRenderer);                   //!< Slot sur cr�ation du renderer d'une sous-entit�.

        //! @name Rendu
        //@{
virtual void        dumpAppearances();                                                  //!< Pr�paration du rendu.
        //@}
        std::map<EntityPtr, RendererPtr, std::owner_less<EntityPtr>>    mRenderers;     //!< 
        std::map<EntityPtr, EntityIDE, std::owner_less<EntityPtr>>      mSubstitutes;   //!< 
        UniversePtr                                                     mUniverse;      //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    RendererIDE::RendererIDE(UniversePtr pUniverse)
        : EDIT::Renderer(pUniverse)
        , mRenderers()
        , mSubstitutes()
        , mUniverse(pUniverse)
    {
        Universe::WorldContainer  lWorlds = mUniverse.lock()->activeWorlds();

        for(size_t lW = 0; lW < lWorlds.size(); ++lW)
        {
            EntityPtr  lWorld  = lWorlds[lW];

            watchRendererOf(lWorld);
            mRenderers[lWorld]; // Cr�e un pointeur de renderer nul.
            F32 lWidth = F32(0.5F * lWorlds[lW]->area().width());

            if (lWidth <= 1.0F)
            {
                lWidth = F32(128.0F);
            }
            EntityIDE  lSubs = std::make_shared<QT3D::Entity>(
                                "",
                                APP::Primitive::create(APP::eCube, lWidth, (lW == 0) ? CLR::kGreen : CLR::kBlack),
                                APP::Primitive::create(APP::eCube, lWidth, (lW == 0) ? CLR::kGreen : CLR::kBlack));
            lSubs->bindTo(lWorld.lock());
            MATH::M3   lScale;
            lScale.loadIdentity();
            lScale.K.z = F32(10.0F) / lWidth;
            MATH::M4   lTransform(lScale);
            if (0 < lW)
            {
                lTransform.L.z = F32(- 20.0F);
            }
            if (0 == lW)
            {
                lTransform.L.z = F32(+  0.0F);
            }
            lSubs->setTransform(lTransform);
            mSubstitutes[lWorld] =  lSubs;
        }
    }


    /*!
     */
    RendererIDE::~RendererIDE() { }


    /*!
     */
    void RendererIDE::dumpAppearances()
    {
        // A d�placer plus tard...
//        GeometryMgr->deliverResources();
//        ImageMgr->deliverResources();

        for(auto lRendererI = mRenderers.begin(); lRendererI != mRenderers.end(); ++lRendererI)
        {
            if (lRendererI->second.expired())
            {
                EntityIDE  lSubs = mSubstitutes[lRendererI->first];
                pushAppearance(lSubs->appearance(), lSubs->transform());
            }
        }
    }


    /*!
     */
    void RendererIDE::created(EntityPtr pChild, RendererPtr pRenderer)
    {
        for(auto lRendererI = mRenderers.begin(); lRendererI != mRenderers.end(); ++lRendererI)
        {
            if (lRendererI->first.lock().get() == pChild.lock().get())
            {
                lRendererI->second = pRenderer;
                break;
            }
        }
        //mRenderers[pChild] = pRenderer; => il semble y avoir un comportement erratique : � v�rifier plus tard.��
        /* Garder une r�f�rence sur les renderers des mondes : tant qu'ils ne sont pas "suffisamment"
           d�finis, afficher une primitive � la place (plan, cube, peu importe); il faut juste s'assurer
           que les primitives en question sont bien associ�es aux bonnes entit�s (afin de permettre leur
           �dition).
           Une fois que les entit�s sont "suffisamment" d�finies, leurs renderers peuvent prendre le relai
           (tant qu'ils sont d�finis; en cas de d�charge, les "anciennes" apparences (primitives) reviennent
           dans la danse).
         */
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de UNI::Controller.
        @version 0.2
     */
    class Controller::ControllerPrivate :
        public EDIT::Subject,
        public EDIT::Container<PAK::FileHdl>
    {
        Q_CUSTOM_DECLARE_PUBLIC(Controller)
    public:
        //! @name Constructeur & destructeur
        //@{
                ControllerPrivate(Controller* pParent);                     //!< Constructeur par d�faut.
virtual         ~ControllerPrivate();                                       //!< Destructeur.
        //@}
        std::shared_ptr<RendererIDE>   mRenderer;                           //!< Renderer.

    private:
    
virtual void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo);    //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::ControllerPrivate::ControllerPrivate(Controller* pParent)
        : EDIT::Subject{"module"}
        , INITIALIZE_CONTAINER(insertWorld, removeWorld)
        , q{pParent}
        , mRenderer{}
    { }


    /*! Destructeur.
     */
    Controller::ControllerPrivate::~ControllerPrivate() { }


    /*!
     */
    void Controller::ControllerPrivate::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Controller::Ptr Controller::make(EntityPtr pEntity, OSGi::Context* pContext)
    {
        return std::make_shared<Controller>(std::dynamic_pointer_cast<Universe>(pEntity.lock()), pContext);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    bool Controller::insertWorld(PAK::FileHdl pWorldHdl)
    {
        return edited().lock()->insertWorld(pWorldHdl);
    }


    bool Controller::removeWorld(PAK::FileHdl pWorldHdl)
    {
        return edited().lock()->removeWorld(pWorldHdl);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::Controller(EditedPtr pUniverse, OSGi::Context* pContext)
        : QT3D::GenericController<Universe>(pUniverse, pContext)
        , pthis{this}
    {
        // L'univers entre en �dition : v�rifications initiales.
        // Mode ext�rieur.
        Q_D(Controller);

        d->mRenderer = std::make_shared<RendererIDE>(pUniverse);

        pUniverse.lock()->activateSaveSelf(true);
    }


    /*! Destructeur
     */
    Controller::~Controller() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    EDIT::Subject* Controller::asSubject()
    {
        Q_D(Controller);

        return d;
    }


    /*!
     */
    QString Controller::getExtension() const
    {
        return "uni";
    }


    /*!
     */
    void Controller::doSave() const
    {
        edited().lock()->saveSelf();
    }


    /*!
     */
    Controller::RendererPtr Controller::getRenderer()
    {
        Q_D(Controller);

        return d->mRenderer;
    }

#if 0
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Mode::Mode(WidgetPtr pWindow)
        : QT3D::StandardMode(pWindow, "UniverseMode")
    { }


    /*!
     */
    Mode::~Mode() { }
}

namespace QUNI
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Editor::EditorPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
                        EditorPrivate(Editor* pParent);   //!< Constructeur.
 static QTAB::QPage*    create();                           //!< Cr�ation d'un onglet.
        void            init();                             //!< Initialisation.

        void            updateUI();                         //!< Mise � jour de l'interface.

        typedef std::weak_ptr<UNI::Controller> UniverseControllerPtr;
        //! @name Composants de l'interface
        //@{
        Ui::UniverseEditor      mUI;                        //!< Interface cr��e par Designer.
        //@}
        ControllerPtr          mController;                //!< 
        UniverseControllerPtr  mUniverse;                  //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent)
        : q_custom_ptr(pParent)
        , mUI()
        , mController()
        , mUniverse()
    { }


    /*!
     */
    QTAB::QPage* Editor::EditorPrivate::create()
    {
        return new Editor();
    }


    /*!
     */
    void Editor::EditorPrivate::init()
    {
        Q_Q(Editor);

        // Construit l'interface.
        mUI.setupUi(q);

        QT::QMainWindow::synchronize(q, "file + enabled + edition + uni;");
    }


    /*!
     */
    void Editor::EditorPrivate::updateUI()
    {
        mUI.universeName->setText(mUniverse.lock()->name().c_str());
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor()
        : QPNL::QGenericEditor<UNI::Universe>()
        , QT::QObserver(nullptr)
        , d_custom_ptr(new EditorPrivate(this))
    {
        Q_D(Editor);

        d->init();
    }


    /*!
     */
    QTAB::QTab* Editor::builder()
    {
        return new QTAB::QTab(&Editor::name, &Editor::title, QIcon(":/types/universe"), &EditorPrivate::create);
    }


    /*! Destructeur
     */
    Editor::~Editor() { }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    void Editor::onLanguageChange()
    {
        Q_D(Editor);

        d->mUI.retranslateUi(this);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Nettoyage entre deux chargements.
     * /
    void Editor::cleanUp()
    {
        delete mpo_Universe;
        mpo_Universe = 0;
    }*/

//------------------------------------------------------------------------------
//Comportement de page
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::manageMenuEntry(QAction* /*entry*/)
    {
        // Rien pour l'instant.
    }


    /*!
     */
    void Editor::fillPageMenu(QMenu*& /*pageMenu*/) const
    {
        // Rien pour l'instant.
    }


    /*!
     */
    QIcon Editor::statusIcon() const
    {
        return QIcon(":/types/universe");
    }


//------------------------------------------------------------------------------
//Pertinence du sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canManage(ControllerPtr controller) const
    {
        return (std::dynamic_pointer_cast<UNI::Controller>(controller) != nullptr);
    }


    /*!
     */
    void Editor::manage(ControllerPtr controller)
    {
        Q_D(Editor);

        // controller a �t� contr�l� en amont.
        d->mController  = controller;
        d->mUniverse    = std::dynamic_pointer_cast<EditorPrivate::UniverseControllerPtr::element_type>(controller);

        QT::QSubject*   lUniverse = DAT::Controller::subject(d->mController);

        watch(lUniverse);

        d->updateUI();
    }


//------------------------------------------------------------------------------
//  Ecoute du sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::listen(const QT::QAnswer* /*answer*/)
    {
        Q_D(Editor);

        // Met simplement � jour l'interface.
        d->updateUI();

        // Et la vue 3D.
        workingWidget()->update();
    }
#endif  // 0
}
