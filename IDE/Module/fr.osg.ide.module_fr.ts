<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Module::Container</name>
    <message>
        <source>3D</source>
        <translation>3D</translation>
    </message>
    <message>
        <source>En&amp;gine</source>
        <translation>&amp;Moteur</translation>
    </message>
    <message>
        <source>&amp;Start</source>
        <translation>&amp;Démarrer</translation>
    </message>
    <message>
        <source>F5</source>
        <comment>Engine | Start</comment>
        <translation>F5</translation>
    </message>
    <message>
        <source>Launches the engine.</source>
        <translation>Lance le moteur.</translation>
    </message>
    <message>
        <source>&amp;Restart</source>
        <translation>&amp;Redémarrer</translation>
    </message>
    <message>
        <source>F7</source>
        <comment>Engine | Restart</comment>
        <translation>F7</translation>
    </message>
    <message>
        <source>Stops, then relaunches the engine.</source>
        <translation>Arrête, puis relance le moteur.</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <source>F5</source>
        <comment>Engine | Stop</comment>
        <translation>F5</translation>
    </message>
    <message>
        <source>Stops the engine.</source>
        <translation>Stoppe le moteur.</translation>
    </message>
</context>
</TS>
