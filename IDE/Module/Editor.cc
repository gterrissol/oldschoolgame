/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Module/Editor.cc
    @brief M�thodes (non-inline) des classes EDIT::Editor::EditorPrivate, EDIT::Editor.
    @author @ref Guillaume_Terrissol
    @date 20 Novembre 2005 - 30 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QChildEvent>
#include <QEvent>
#include <QSet>

#include "Container.hh"

namespace Module
{
//------------------------------------------------------------------------------
//                     Editor : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor()
        : QWidget{}
        , mWorldView{}
    {}


    /*! Destructeur
     */
    Editor::~Editor() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::applyOn(EDIT::Controller::Ptr controller)
    {
        if (controller)
        {
            if (canManage(controller))
            {
                manage(controller);
            }
            else
            {
                unmanage();
            }
        }
        else
        {
            manageNone();
        }
    }

    void Editor::setWorldView(QWidget* pView)
    {
        if ((mWorldView = pView))
        {
            connect(mWorldView, SIGNAL(edit(EDIT::Controller::Ptr)), SLOT(applyOn(EDIT::Controller::Ptr)));
        }
    }


    /*!
     */
    QWidget* Editor::worldView() const
    {
        return mWorldView;
    }


    /*!
     */
    void Editor::unmanage() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @fn bool Editor::canManage(EDIT::Controller::Ptr controller) const
        Permet de savoir si l'outil est adapt� � "travailler" sur le contr�leur <i>controller</i>.
        @param controller Nouvelle donn�e � �diter
        @return Vrai si l'outil peut travailler sur cette donn�e, Faux sinon
        @note Un des moyens de savoir si le type est acceptable est d'utiliser <b>dynamic_cast</b>
     */

    /*! @fn void Editor::manage(EDIT::Controller::Ptr controller)
        Effectue toutes les initialisations requises pour commencer � travailler sur le sujet
        <i>datum</i>.
        @param controller Nouvelle donn�e � �diter
        @note Il n'y a pas de m�thode pour r�cup�rer le sujet par la suite pour une raison simple : le
        type exact (ou, du moins, plus pr�cis) du sujet �tant requis pour travailler, le stocker via ce
        type exact est laiss� � la charge des classes d�riv�es
     */

    /*! @fn void Editor::manageNone()
        Arr�te de g�rer le sujet actuellement trait�.
     */
}
