/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_MOBILECONTROLLER_HH
#define QT3D_MOBILECONTROLLER_HH

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/MobileController.hh
    @brief En-t�te de la classe QT3D::MobileController.
    @author @ref Guillaume_Terrissol
    @date 11 Juin 2009 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "MATH/MATH.hh"

#include "Controller.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                                  Contr�leurs
//------------------------------------------------------------------------------

    /*! @brief Contr�leur d'objet du moteur.
        @version 0.6
     */
    class MobileController : public Controller
    {
    public:
        //! @name Types de pointeur
        //@{
        using Ptr           = std::shared_ptr<MobileController>;    //!< Pointeur sur contr�leur.
        using AppearancePtr = std::shared_ptr<APP::Appearance>;     //!< Pointeur sur apparence.
        //! @name Interface
        //@{
virtual                 ~MobileController();                        //!< Destructeur.
        AppearancePtr   appearance() const;                         //!< 
        MATH::V3        position() const;                           //!< 
        MATH::Q         orientation() const;                        //!< 
        MATH::V3        scale() const;                              //!< 
        MATH::M4        frame() const;                              //!< 
        //@}
        //! @name M�thodes d'�dition
        //@{
        void            setPosition(MATH::V3 pNew);                 //!< 
        void            setOrientation(MATH::Q pNew);               //!< 
        void            setScale(MATH::V3 pNew);                    //!< 
        void            translate(MATH::V3 pOffset);                //!< 
        void            rotate(MATH::Q pRotation);                  //!< 
        void            stretch(MATH::V3 pScale);                   //!< 
        //@}
    private:
        //! @name Comportement � d�finir
        //@{
virtual AppearancePtr   getAppearance() const = 0;                  //!<
virtual MATH::V3        getPosition() const = 0;                    //!< 
virtual MATH::Q         getOrientation() const = 0;                 //!< 
virtual MATH::V3        getScale() const = 0;                       //!< 
virtual void            doSetPosition(MATH::V3 pNew) = 0;           //!< 
virtual void            doSetOrientation(MATH::Q pNew) = 0;         //!< 
virtual void            doSetScale(MATH::V3 pNew) = 0;              //!< 
        //@}
    };
}

#endif  // De QT3D_MOBILECONTROLLER_HH
