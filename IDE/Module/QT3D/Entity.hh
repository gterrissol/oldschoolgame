/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_ENTITY_HH
#define QT3D_ENTITY_HH

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/Entity.hh
    @brief En-t�te de la classe QT3D::Entity.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2008 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QString>

#include "APPearance/BaseAppearance.hh"
#include "MATH/MATH.hh"
#include "STL/STL.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                              
//------------------------------------------------------------------------------

    /*! @brief Entit� �diteur.
        @version 0.2
     */
    class Entity : public APP::Pickable
    {
    public:
        //! @name Types de pointeur
        //@{
        using ConstAppearancePtr    = std::shared_ptr<APP::Appearance const>;   //!< Pointeur sur apparence constante.
        using AppearancePtr         = std::shared_ptr<APP::Appearance>;         //!< Pointeur sur apparence.
        using PickablePtr           = std::shared_ptr<APP::Pickable>;           //!< Pointeur sur objet pickable.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            Entity(QString pName, AppearancePtr pNormal, AppearancePtr pHighlighted);
virtual                     ~Entity();
        //@}
        //! @name Interface
        //@{
        void                bindTo(PickablePtr pObject);
        void                getLit(APP::Appearance::LightPtr pLight);
        QString             name() const;
        void                highlight(bool pOn);
        bool                isHighlighted() const;
        void                setVisible(bool pOn);
        bool                isVisible() const;
        ConstAppearancePtr  appearance() const;
        void                setTransform(MATH::M4 pTransform, bool pVisible = true);
        MATH::M4            transform() const;
        //@}

    private:

virtual U32                 getType() const;

        FREE_PIMPL()
    };
}

#endif  // De QT3D_ENTITY_HH
