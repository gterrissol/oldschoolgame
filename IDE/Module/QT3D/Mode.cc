/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include <Qt>
#include <memory>

//---------------------------------------------------------------------------
//                           Fonctions Assistantes
//---------------------------------------------------------------------------

    template<class T>
    inline uint qHash(const std::shared_ptr<T>& pPtr);                              //!< 
    template<class T>
    inline bool operator==(const std::weak_ptr<T>& pL, const std::weak_ptr<T>& pR); //!< Egalit�


#include "Mode.hh"

/*! @file IDE/Module/QT3D/Mode.cc
    @brief M�thodes (non-inline) de la classe QT3D::Mode.
    @author @ref Guillaume_Terrissol
    @date 9 Novembre 2003 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

//---------------------------------------------------------------------------
//                                  En-t�tes
//---------------------------------------------------------------------------

#include <cmath>

#include <QCursor>
#include <QMap>
#include <QMenu>
#include <QMouseEvent>
#include <QPoint>
#include <QSet>
#include <QVector>

#include "APPearance/Primitive.hh"
#include "CAMera/BaseCamera.hh"
#include "EDITion/Requests.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "OBJect/GameObject.hh"
#include "RenDeR/Renderer.hh"
#include "UTIlity/Rect.hh"

#include "Const.hh"
#include "Controller.hh"
#include "Entity.hh"
#include "ErrMsg.hh"
#include "MobileController.hh"
#include "Widget.hh"

namespace
{
    const int   kWheelDelta     = 120;  //!< param�trage de la molette.
    const F32   kCubeDelta      = F32(3.0F);
    const F32   kEditToolsScale = F32(0.1F);
}

    template<class T>
    inline uint qHash(const std::shared_ptr<T>& pPtr)
    {
        return qHash(reinterpret_cast<long>(pPtr.get()));
    }
    template<class T>
    inline bool operator==(const std::weak_ptr<T>& pL, const std::weak_ptr<T>& pR)
    {
        return (pL.lock().get() == pR.lock().get());
    }

namespace QT3D
{
//------------------------------------------------------------------------------
//                                Noms des Modes
//------------------------------------------------------------------------------

    // Noms des modes de base.
#if     defined(FRANCAIS)
    const char* kEditToolsMode = "Mode Outils d'Edition.";
    const char* kStandardMode  = "Mode Standard.";
#elif   defined(ENGLISH)
    const char* kEditToolsMode = "Edit Tools mode.";
    const char* kStandardMode  = "Standard mode.";
#endif  // Du langage.


//------------------------------------------------------------------------------
//                              Op�rateur d'Egalit�
//------------------------------------------------------------------------------

    /*! Compare deux weak pointers.
        @param pL Premier pointeur � comparer
        @param pR Second pointeur � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont �quivalents (i.e. partagent un m�me pointeur
        r�f�rence ou sont tous deux vides)
     */
    bool operator==(const ModeMgr::ModePtr& pL, const ModeMgr::ModePtr& pR)
    {
        return !std::owner_less<ModeMgr::ModePtr>()(pL, pR) && !std::owner_less<ModeMgr::ModePtr>()(pR, pL);
    }


//------------------------------------------------------------------------------
//                                   Renderers
//------------------------------------------------------------------------------

    /*! @brief Renderer pour les Edit Tools.
        @version 0.4
     */
    class EditToolsRenderer : public RDR::Renderer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        EditToolsRenderer();        //!< Constructeur.
virtual                 ~EditToolsRenderer();       //!< Destructeur.
        //@}
        //! @name G�om�tries des Outils
        //@{
        void            insert(Entity* pEntity);    //!< 
        void            remove(Entity* pEntity);    //!< 
        //@}

    private:
        //! @name Gestion du rendu
        //@{
virtual void            dumpAppearances() override; //!< 
virtual RDR::ELayers    getLayer() const override;  //!< 
        //@}
        QSet<Entity*>   mEntities;                  //!< 
    };


    /*! @brief Renderer pour la s�lection.
        @version 0.
     */
    class SelectionRenderer : public RDR::Renderer
    {
    public:
        //! @name Type de pointeur
        //@{    
        using ConstSelectionPtr = std::weak_ptr<Selection const>;           //!< Pointeur sur s�lection (constante).
        //@}
        //! @name Constructeur & destructeur
        //@{
                        SelectionRenderer(ConstSelectionPtr pSelection);    //!< 
virtual                 ~SelectionRenderer();                               //!< 
        //@}

    private:
        //! @name Gestion du rendu
        //@{
virtual void            dumpAppearances() override;                         //!< 
virtual RDR::ELayers    getLayer() const override;                          //!< 
        //@}
        ConstSelectionPtr   mSelection;                                     //!< 
    };


//------------------------------------------------------------------------------
//               Edit Tools Renderer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    EditToolsRenderer::EditToolsRenderer()
        : mEntities()
    { }


    /*! Destructeur.
     */
    EditToolsRenderer::~EditToolsRenderer() { }


//------------------------------------------------------------------------------
//                    Edit Tools Renderer : G�om�tries des Outils
//------------------------------------------------------------------------------

    /*!
     */
    void EditToolsRenderer::insert(Entity* pEntity)
    {
        mEntities.insert(pEntity);
    }


    /*!
     */
    void EditToolsRenderer::remove(Entity* pEntity)
    {
        mEntities.remove(pEntity);
    }


//------------------------------------------------------------------------------
//                    Edit Tools Renderer : Gestion du Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void EditToolsRenderer::dumpAppearances()
    {
        F32         lZoom   = kEditToolsScale * camera().lock()->zoom();
        MATH::M4    lScale  = MATH::M4(MATH::V4(lZoom, k0F,   k0F,   k0F),
                                       MATH::V4(k0F,   lZoom, k0F,   k0F),
                                       MATH::V4(k0F,   k0F,   lZoom, k0F),
                                       MATH::V4(k0F,   k0F,   k0F,   k1F)); 
        foreach(Entity* lEntity, mEntities)
        {
            pushAppearance(lEntity->appearance(), lEntity->transform() * lScale);
        }
    }


    /*!
     */
    RDR::ELayers EditToolsRenderer::getLayer() const
    {
        return RDR::eMainOverlay;
    }


//------------------------------------------------------------------------------
//                Selection Renderer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pSelection
     */
    SelectionRenderer::SelectionRenderer(ConstSelectionPtr pSelection)
        : mSelection(pSelection)
    { }


    /*! Destructeur.
     */
    SelectionRenderer::~SelectionRenderer() { }


//------------------------------------------------------------------------------
//                     Selection Renderer : Gestion du Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void SelectionRenderer::dumpAppearances()
    {
        foreach(Selection::SelectablePtr lSelected, mSelection.lock()->asList())
        {
            Selection::SelectablePtr::element_type* lObject = lSelected.get();
            if (lObject != nullptr)
            {
                pushAppearance(lObject->appearance(), lObject->frame());
            }
        }
    }


    /*!
     */
    RDR::ELayers SelectionRenderer::getLayer() const
    {
        return RDR::eHighlightLayer;
    }


//------------------------------------------------------------------------------
//                              Selection : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::Selection.
        @version 0.5
     */
    class Selection::Private
    {
    public:

        Private()
            : mSelectedObjects()
        { }
        QSet<SelectablePtr> mSelectedObjects;   //!< Ensemble des objets s�lectionn�s.
    };


//---------------------------------------------------------------------------
//                         Constructeur & Destructeur
//---------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Selection::Selection()
        : pthis{}
    { }


    /*! Destructeur.
     */
    Selection::~Selection() { }


//---------------------------------------------------------------------------
//                      Selection : Op�rations de Liste
//---------------------------------------------------------------------------

    /*! @return VRAI si la s�lection est vide, FAUX sinon
     */
    bool Selection::empty() const
    {
        return pthis->mSelectedObjects.empty();
    }


    /*! Ajoute un �l�ment dans la s�lection.
        @param pObject Nouvel objet s�lectionn�
     */
    void Selection::insert(SelectablePtr pObject)
    {
        pthis->mSelectedObjects.insert(pObject);
    }


    /*! Retire un �lement de la s�lection.
        @param pObject Objet d�s�lectionn�
     */
    void Selection::remove(SelectablePtr pObject)
    {
        foreach(SelectablePtr lSelected, asList())
        {
            if (lSelected->equals(pObject))
            {
                pthis->mSelectedObjects.remove(lSelected);
                return;
            }
        }
    }


    /*! Permet de savoir si un �l�ment est s�lectionn�.
        @param pObject Cet �l�ment fait-il partie de la s�lection actuelle ?
        @return VRAI si <i>pObject</i> fait aprtie de cette s�lection, FAUX sinon
     */
    bool Selection::isSelected(SelectablePtr pObject) const
    {
        foreach(SelectablePtr lSelected, asList())
        {
            if (lSelected->equals(pObject))
            {
                return true;
            }
        }

        return false;
    }


    /*! Vide la s�lection.
     */
    void Selection::clear()
    {
        pthis->mSelectedObjects.clear();
    }


    /*! @return L'ensemble des �l�ments sous forme d'une liste
     */
    auto Selection::asList() const -> List
    {
        return pthis->mSelectedObjects.toList();
    }


//------------------------------------------------------------------------------
//                                 Mode : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::Mode.
        @version 0.4
     */
    class Mode::Private
    {
        Q_CUSTOM_DECLARE_PUBLIC(Mode)
    public:
        //! @name Constructeur
        //@{
        Private(WidgetPtr pWindow, QString pName);  //!< Constructeur.
        //@}
        //! @name Informations sur la souris
        //@{
        QPoint      mClickPosition;                 //!< Position du dernier clic souris.
        QPoint      mCurrentPosition;               //!< Position actuelle de la souris.
        QPoint      mCurrentDelta;                  //!< Variation de la position de la souris.
        int         mCurrentButton;                 //!< Bouton actif.
        int         mCurrentWheelStep;              //!< Incr�ment de la molette.
        //@}
        WidgetPtr   mWindow;                        //!< Fen�tre acc�d�e par le mode.
        QString     mName;                          //!< Nom du mode.
        PickablePtr mHighlighted;                   //!< Entit� sous la souris.
        bool        mIsActive;                      //!< Mode activ� ?
        bool        mCursorChanged;                 //!< Curseur modifi�.
    };


//---------------------------------------------------------------------------
//                          P-Impl : "Constructeurs"
//---------------------------------------------------------------------------

    /*! Constructeur par d�faut.
        @param pWindow Fen�tre � laquelle lier le mode
        @param pName Nom du mode
     */
    Mode::Private::Private(WidgetPtr pWindow, QString pName)
        : mClickPosition{}
        , mCurrentPosition{}
        , mCurrentDelta{}
        , mCurrentButton{eNone}
        , mCurrentWheelStep{0}
        , mWindow{pWindow}
        , mName{pName}
        , mHighlighted{}
        , mIsActive{false}
        , mCursorChanged{false}
    {
        ASSERT(mWindow != nullptr, kModeMustBeBoundToValidWindow)
    }


//---------------------------------------------------------------------------
//                     Mode : Constructeur & Destructeur
//---------------------------------------------------------------------------

    /*! Constructeur.
        @param pWindow Fen�tre � laquelle cette instance est associ�e
        @param pName   Nom du mode
        @note <i>pWindow</i> ne doit pas �tre nul
        @note Le mode s'enregistre lui-m�me aupr�s de <i>pWindow</i>
     */
    Mode::Mode(WidgetPtr pWindow, QString pName)
        : pthis{pWindow, pName}
    { }


    /*! Destructeur.
     */
    Mode::~Mode() { }


//---------------------------------------------------------------------------
//                 Mode : Interface Commune � tous les Modes
//---------------------------------------------------------------------------

    /*! Pr�pare l'entr�e dans le mode.
        @sa OnEnter
     */
    void Mode::enter()
    {
//qDebug(QString("Entering mode %1").arg(name()).toAscii().constData());
        // Amis du jour, bonjour.
        onEnter();

        pthis->mIsActive = true;
    }


    /*! @return VRAI si le mode est actif (i.e. enter() a �t� appel�, mais pas exit())
        @note Ne pas se reposer sur la valeur de retour lors de r�impl�mentation de onEnter() ou onExit()
     */
    bool Mode::isActive() const
    {
        return pthis->mIsActive;
    }


    /*! Pr�pare la sortie du mode.
        @sa OnExit
     */
    void Mode::exit()
    {
//qDebug(QString("Exiting mode %1").arg(name()).toAscii().constData());
        pthis->mIsActive = false;

        // Amis du soir, bonsoir.
        onExit();

        // Restaure le curseur de la souris (s'il avait �t� chang�).
        unsetWindowCursor();
    }


    /*! Comportement associ� � un clic souris (bouton utile).<br>
        <i>Rappel :</i> Le comportement d'un seul bouton peut-�tre personnalis� pour un mode (le gauche).
        @param pPos     Position du clic
        @param pButtons Combinaison d'EState repr�sentant l'action ayant d�clench� le "click"
        @note Comme enter() et exit(), ceci est un patron de m�thode. Selon la valeur de
        <i>pButtonState</i>, press() ou release() pourront �tre appel�es
     */
    void Mode::click(const QPoint& pPos, int pButtons)
    {
        pthis->mClickPosition   = pthis->mCurrentPosition   = pPos;
        pthis->mCurrentDelta    = QPoint(0, 0);
        pthis->mCurrentButton   = pButtons & eButton;

        switch(pButtons & eState)
        {
            case ePress:
                press();
                break;
            case eRelease:
                release();
                pthis->mCurrentButton = eNone;
                break;
            default:
                ASSERT(false, kInvalidState)
                break;
        }
    }


    /*! 
     */
    void Mode::wheel(int pStep)
    {
        pthis->mCurrentWheelStep = pStep;

        doWheel();
    }


    /*! Comportement associ� au glissement de la souris (drag).
        @param pPos Position de la souris
        @sa DoDrag, Click
     */
    void Mode::drag(const QPoint& pPos)
    {
        pthis->mCurrentDelta    = pPos - pthis->mCurrentPosition;
        pthis->mCurrentPosition = pPos;
        pthis->mWindow->makeCurrent();

        doDrag();
    }


    /*! Cr�e et affiche un menu li� au mode et � son contexte (objets actuellement s�lectionn�s,
        sous-mode activ�, derni�re action r�alis�e,...)
        @sa BuildContextMenu
     */
    void Mode::displayContextMenu()
    {
        if (QMenu* lMenu = buildContextMenu())
        {
            // Note : le menu doit avoir effectu� ses connections (signal/slot).
            lMenu->exec(QCursor::pos());
        }
    }


    /*! Effectue une mise � jour du mode.
     */
    void Mode::update()
    {
        doUpdate();
    }


    /*! @return Le nom du mode
     */
    QString Mode::name() const
    {
        return pthis->mName;
    }


    /*!
     */
    void Mode::picked(PickablePtr pEntity)
    {
        onPickedChange(pEntity);

        pthis->mHighlighted = pEntity;
    }


    /*! @return VRAI si le mode g�re directement <i>pEntity</i>, FAUX sinon
     */
    bool Mode::manages(PickablePtr pEntity)
    {
        return doesManage(pEntity);
    }


//---------------------------------------------------------------------------
//                Mode : Informations sur l'Etat de la Souris
//---------------------------------------------------------------------------

    /*! @return La position de la souris lors du dernier clic
     */
    QPoint Mode::clickMousePosition() const
    {
        return pthis->mClickPosition;
    }


    /*! @return La position actuelle du pointeur de la souris
     */
    QPoint Mode::currentMousePosition() const
    {
        return pthis->mCurrentPosition;
    }


    /*! @return La variation de la position de la souris entre deux appels � drag()
     */
    QPoint Mode::currentDeltaPosition() const
    {
        return pthis->mCurrentDelta;
    }


    /*! @return Le code du boutton ayant provoqu� le dernier traitement par le mode
     */
    int Mode::button() const
    {
        return pthis->mCurrentButton;
    }


    /*!
     */
    int Mode::wheelDelta() const
    {
        return pthis->mCurrentWheelStep;
    }


//---------------------------------------------------------------------------
//                          Entr�e & Sortie de Mode
//---------------------------------------------------------------------------

    /*! Cette m�thode doit permettre d'effectuer toute t�che jug�e utile avant que le mode ne soit activ�.
        @note Une impl�mentation par d�faut (vide) est fournie car rien n'oblige un mode � fournir un
        quelconque travail pour son activation
     */
    void Mode::onEnter() { }


    /*! Cette m�thode doit permettre d'effectuer toute t�che jug�e utile avant de sortir du mode (et d'en
        activer un autre).
        @note Une impl�mentation par d�faut (vide) est fournie car rien n'oblige un mode � fournir un
        quelconque travail 
     */
    void Mode::onExit() { }


    /*  Il peut arriver qu'un undo ou un redo vienne perturber le fonctionnement d'un mode (e.g. pour
        l'�dition de tuiles, le "masque" du pinceau �pouse la forme du terrain "avant" l'undo ou le redo,
        et n'est donc plus correct). Cette m�thode peut donc �tre r�impl�ment�e pour effectuer toute mise
        � jour permettant de rester dans un �t�t coh�rent.
        @note L'impl�menation par d�faut ne fait rien
        @note En cas de d�rivation successive, n'oubliez pas d'appeler la version de la classe parente
     */
    void Mode::doUpdate() { }


//---------------------------------------------------------------------------
//                       Mode : Actions sur la Fen�tre
//---------------------------------------------------------------------------

    /*! @return La cam�ra associ�e � la fen�tre li�e
     */
    Mode::CameraPtr Mode::windowCamera()
    {
        return pthis->mWindow->camera();
    }


    /*!
     */
    void Mode::setWindowCursor(const QCursor& pCursor)
    {
        pthis->mWindow->setCursor(pCursor);
        pthis->mCursorChanged = true;
    }


    /*!
     */
    void Mode::unsetWindowCursor()
    {
        if (pthis->mCursorChanged)
        {
            pthis->mWindow->unsetCursor();
            pthis->mCursorChanged = false;
        }
    }


    /*! @param pPoint Point sur l'�cran (coordonn�es Qt)
     */
    MATH::V3 Mode::pickPoint(QPoint pPoint)
    {
        return pthis->mWindow->pickPoint(pPoint);
    }


    /*!
     */
    void Mode::updateWindow()
    {
        pthis->mWindow->update();
    }


    /*!
     */
    void Mode::attach(RendererPtr pRenderer)
    {
        pthis->mWindow->attach(pRenderer);
    }


    /*!
     */
    void Mode::detach(RendererPtr pRenderer)
    {
        pthis->mWindow->detach(pRenderer);
    }


    /*!
     */
    QSize Mode::windowSize() const
    {
        return pthis->mWindow->size();
    }


//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

    /*!
     */
    Mode::PickablePtr Mode::picked()
    {
        return pthis->mHighlighted;
    }


    /*!
     */
    void Mode::onPickedChange(PickablePtr pEntity)
    {
        if (!picked().expired())
        {
            if (auto lPtr = std::dynamic_pointer_cast<Entity>(picked().lock()))
            {
                lPtr->highlight(false);
            }
        }

        if (!pEntity.expired())
        {
            if (auto lPtr = std::dynamic_pointer_cast<Entity>(pEntity.lock()))
            {
                lPtr->highlight(true);
            }
        }

        updateWindow();
    }


    /*! @return FAUX par d�faut
     */
    bool Mode::doesManage(PickablePtr)
    {
        return false;
    }


//---------------------------------------------------------------------------
//                           Mode : Autres M�thodes
//---------------------------------------------------------------------------

    /*! Impl�mente le changement d'�tat du mode afin de prendre en compte de nouveaux modificateurs.
        @param pNewModifiers Nouveaux modificateurs
        @note L'impl�mentation par d�faut ne fait rien.
     */
    void Mode::modifiersChanged(int)
    {
        // Par d�faut, ne fait rien.
    }


    /*! Un mode peut proposer plusieurs menus diff�rents; cette m�thode ne sert donc pas qu'� cr�er un
        menu : elle doit se charger de m�moriser les menus au fur et � mesure de leur cr�ation (cel�
        n'est pas fait au niveau de cette classe de base).
        @note Qt se chargera de la destruction de ces menus (s'ils ont bien un parent)
        @return Un menu associ� au contexte du mode lors de l'appel, si cela a du sens; nullptr sinon
     */
    QMenu* Mode::buildContextMenu() const
    {
        // Par d�faut, aucun menu contextuel n'est disponible.
        return nullptr;
    }


//---------------------------------------------------------------------------
//                    Mode : Documentation Suppl�mentaire
//---------------------------------------------------------------------------

    /*! @fn Mode::press()
        L'impl�mentation de cette m�thode doit traiter l'appui sur le bouton utile (gauche) de la souris.
     */


    /*! @fn Mode::release()
        L'impl�mentation de cette m�thode doit traiter le rel�chement du bouton utile (gauche) de la souris.
     */


    /*! @fn Mode::doDrag()
        L'impl�mentation de cette m�thode doit permettre de g�rer le glissement de la souris (drag).
     */


//---------------------------------------------------------------------------
//                Selection Mode : Constructeurs & Destructeur
//---------------------------------------------------------------------------


    /*! Constructeur.
        @param pWindow Fen�tre � laquelle cette instance est associ�e
     */
    StandardMode::StandardMode(WidgetPtr pWindow)
        : Mode{pWindow, kStandardMode}
        , mIsCameraActive{true} // Cam�ra par d�faut.
        , mIsSelectionActive{false}
    { }


    /*! Destructeur.
     */
    StandardMode::~StandardMode() { }


    /*!
     */
    StandardMode::StandardMode(WidgetPtr pWindow, QString pName)
        : Mode{pWindow, pName}
        , mIsCameraActive{true} // Cam�ra par d�faut.
        , mIsSelectionActive{false}
    { }


//---------------------------------------------------------------------------
//                Selection Mode : R�actions aux Ev�nement Souris
//---------------------------------------------------------------------------

    /*! Permet de s�lectioner un objet, ou de commencer une s�lection au lasso.
     */
    void StandardMode::press()
    {
        switch(button())
        {
            case eLeft:
                // R�active la s�lection.
                mIsSelectionActive = true;
                break;
            default:
                // Rien pour l'instant.
                break;
        }
    }


    /*! Dans le cas d'une s�lection au lasso, permet de finaliser celle-ci (les objets dans le lasso deviennent s�lectionn�s).
     */
    void StandardMode::release()
    {
        switch(button())
        {
            case eLeft:
                if (mIsSelectionActive)
                {
                    // Tentative de picking
                }
                break;
            default:
                break;
        }
    }


    /*! Permet de manipuler un lasso de s�lection cr�� au pr�alable.
     */
    void StandardMode::doDrag()
    {
        if (mIsCameraActive)
        {
            // En cas de drag et cam�ra active (i.e. ni Control,
            // ni Shift ne sont enfonc�s), on g�re la cam�ra.
            mIsSelectionActive = false;

            std::shared_ptr<CameraPtr::element_type>   lCamera = windowCamera().lock();

            switch(button())
            {
                case eMiddle:
                {
                    QPoint  lDelta  = currentDeltaPosition();
                    float   lCoeff  = lCamera->zoom() / 256.0F;

                    lCamera->move(MATH::V3(F32(- lDelta.x() * lCoeff), F32(+ lDelta.y() * lCoeff), k0F));
                    updateWindow();

                    break;
                }
                case eRight:
                    lCamera->rotate(F32(currentDeltaPosition().x() * 0.40F) * kDegToRad);
                    updateWindow();
                    break;
                default:
                    break;
            }
        }
    }


    /*!
     */
    void StandardMode::doWheel()
    {
        if (mIsCameraActive)
        {
            std::shared_ptr<CameraPtr::element_type>   lCamera = windowCamera().lock();

            lCamera->zoom(F32(- wheelDelta() * 5.0F));
            //lCamera->move(MATH::V3(k0F, k0F, F32(currentDeltaPosition().x() * 1.5F)));
            updateWindow();
        }
    }


//---------------------------------------------------------------------------
//                 Selection Mode : Gestion des Modificateurs
//---------------------------------------------------------------------------

    /*! En fonction des modificateurs, permet de passer sur un sous-mode.
        @param pNewModifiers Nouveaux modificateurs (Meta, Alt, Ctrl, Shift)
     */
    void StandardMode::modifiersChanged(int pNewModifiers)
    {
        switch(pNewModifiers)
        {
            case Qt::Key_Control:
                mIsCameraActive = false;
                break;
            case Qt::Key_Shift:
                mIsCameraActive = false;
                break;
            default:
                mIsCameraActive = true;
                break;
        }
    }


//---------------------------------------------------------------------------
//                      Selection Mode : Autres M�thodes
//---------------------------------------------------------------------------

    /*! Cr�ation d'un menu contextuel. Selon les cas, les entr�es suivantes pourront �tre pr�sent�es (activ�es ou non) :
        - Copier
        - Couper
        - Coller
        - ...?
        @return Un menu associ� aux s�lections
     */
    QMenu* StandardMode::buildContextMenu() const
    {
        return nullptr;
    }


//---------------------------------------------------------------------------
//                           EditToolsMode : P-Impl
//---------------------------------------------------------------------------

    /*! @brief P-impl de QT3D::EditToolsMode
        @version 0.5
     */
    class EditToolsMode::Private
    {
    public:
        //! @name Identification des outils
        //@{
        //! Outils d'�dition.
        enum ETools
        {
            eDefault,   //!< Aucun.
            eTranslate, //!< Translation.
            eRotate,    //!< Rotation.
            eScale,     //!< Echelle.
        };

        //! Actions d'�dition.
        enum EActions
        {
            eNoop,      //!< Aucune.
            eMoveX,     //!< D�placement sur [0,x) local.
            eMoveY,     //!< D�placement sur [0,y) local.
            eMoveZ,     //!< D�placement sur [0,z) local.
            eRotateX,   //!< Rotation selon [0,x) local.
            eRotateY,   //!< Rotation selon [0,y) local.
            eRotateZ,   //!< Rotation selon [0,z) local.
            eScaleX,    //!< Changement d'�chelle sur [0,x) local.
            eScaleY,    //!< Changement d'�chelle sur [0,y) local.
            eScaleZ,    //!< Changement d'�chelle sur [0,z) local.
            eResetT,    //!< R�initialisation de la position.
            eResetR,    //!< R�initialisation d'orientation.
            eResetS,    //!< R�initialisation de l'�chelle.
        };

        //! G�om�tries des outils.
        enum EGeometries
        {
            eArrowX,        //!< Fl�che sur [0,x).
            eArrowY,        //!< Fl�che sur [0,y).
            eArrowZ,        //!< Fl�che sur [0,z).
            eCircleYZ,      //!< Cercle sur (y0z).
            eCircleZX,      //!< Cercle sur (z0x).
            eCircleXY,      //!< Cercle sur (x0y).
            eCubeX,         //!< Cube sur (x = 1).
            eCubeY,         //!< Cube sur (y = 1).
            eCubeZ,         //!< Cube sur (z = 1).
            eLineToCubeX,   //!< Ligne 0 - Cube X.
            eLineToCubeY,   //!< Ligne 0 - Cube Y.
            eLineToCubeZ,   //!< Ligne 0 - Cube Z.
            eResetTCube,    //!< Cube � l'origine.
            eResetRCube,    //!< Cube � l'origine.
            eResetSCube,    //!< Cube � l'origine.
            eGeometryCount  //!< Nombre de g�om�tries.
        };
        //@}
        //! @name Types de pointeur
        //@{
        using LightPtr      = std::shared_ptr<LIT::DirectionalLight>;           //!< Pointeur sur lumi�re.
        using EntityPtr     = std::shared_ptr<Entity>;                          //!< Pointeur sur entit�.
        using RendererPtr   = std::shared_ptr<EditToolsRenderer>;               //!< Pointeur sur renderer.
        using SelectablePtr = Selection::SelectablePtr;                         //!< Pointeur sur object s�lectionnable.
        //@}

                    Private(EditToolsMode* pParent, SelectionPtr pSelection);   //!< Constructeur
        void        setCurrentAction();
        //! @name Gestion des g�om�tries des outils
        //@{
 static EntityPtr   buildModel(const QString&   pName,
                               APP::EPrimitive  pType,
                               F32              pSize,
                               const CLR::RGB& pColor,
                               const CLR::RGB& pHighlightColor = CLR::kYellow); //!< Construction d'une g�om�trie.
        void        buildModels();                                              //!< Construction des g�om�tries.
        void        cleanModels();                                              //!< Destruction des g�om�tries.
        void        updateFrame();                                              //!< 
        //@}
        //! @name Attributs
        //@{
        QVector<EntityPtr>      mGeometries;                                    //!< 
        QMap<QString, EActions> mActions;                                       //!< 
        RendererPtr             mEditToolsRenderer;                             //!< 
        LightPtr                mSun;                                           //!< 
        EntityPtr               mSelected;                                      //!< 
        MATH::V3                mRefPosition;                                   //!< 
        MATH::Q                 mRefOrientation;                                //!< 
        MATH::V3                mRefScale;                                      //!< 
        SelectionPtr            mSelection;                                     //!< 
        EditToolsMode*          mParent;                                        //!< 
        ETools                  mCurrentTool;                                   //!< 
        EActions                mCurrentAction;                                 //!< 
        //@}
    };


//---------------------------------------------------------------------------
//                      EditToolsMode P-Impl : M�thodes
//---------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent    Mode propri�taire
        @param pSelection S�lection �dit�e
     */     
    EditToolsMode::Private::Private(EditToolsMode* pParent, SelectionPtr pSelection)
        : mGeometries{}
        , mActions{}
        , mEditToolsRenderer{}
        , mSun{}
        , mSelected{}
        , mRefPosition{MATH::kNullV3}
        , mRefOrientation{MATH::kNullQ}
        , mRefScale{MATH::V3{k1F, k1F, k1F}}
        , mSelection{pSelection}
        , mParent{pParent}
        , mCurrentTool{eDefault}
        , mCurrentAction{eNoop}
    {
        ASSERT(!mSelection.expired(), kInvalidSelection)

        mActions["ArrowX"]      = eMoveX;
        mActions["ArrowY"]      = eMoveY;
        mActions["ArrowZ"]      = eMoveZ;
        mActions["CircleYZ"]    = eRotateX;
        mActions["CircleZX"]    = eRotateY;
        mActions["CircleXY"]    = eRotateZ;
        mActions["CubeX"]       = eScaleX;
        mActions["CubeY"]       = eScaleY;
        mActions["CubeZ"]       = eScaleZ;
        mActions["ResetTCube"]  = eResetT;
        mActions["ResetRCube"]  = eResetR;
        mActions["ResetSCube"]  = eResetS;
    }


    /*!
     */
    void EditToolsMode::Private::setCurrentAction()
    {
        EActions    lNewAction = eNoop;

        if (mSelected != nullptr)
        {
            auto    lAction = mActions.find(mSelected->name());
            if (lAction != mActions.end())
            {
                lNewAction = lAction.value();
            }
        }

        if (lNewAction != mCurrentAction)
        {
            SelectablePtr   lObject = mSelection.lock()->asList().first();
            mRefPosition    = lObject->position();
            mRefOrientation = lObject->orientation();
            mRefScale       = lObject->scale();
        }

        mCurrentAction = lNewAction;
    }


//---------------------------------------------------------------------------
//          EditToolsMode P-Impl : Gestion des G�om�tries des Outils
//---------------------------------------------------------------------------

    /*!
     */
    auto EditToolsMode::Private::buildModel(const QString& pName, APP::EPrimitive pType, F32 pSize, const CLR::RGB& pColor, const CLR::RGB& pHighlightColor) -> EntityPtr
    {
        return std::make_shared<QT3D::Entity>(pName, APP::Primitive::create(pType, pSize, pColor), APP::Primitive::create(pType, pSize, pHighlightColor));
    }


    /*!
     */
    void EditToolsMode::Private::buildModels()
    {
        const F32   k01F    = F32(0.1F);
        mSun = STL::makeShared<LIT::DirectionalLight>();
        mSun->setDirection(MATH::V4(k1F, k1F, k1F, k0F).normalized());
        mSun->setAmbient(CLR::RGBA( F32(0.2F), F32(0.2F), F32(0.2F), F32(1.0F)));
        mSun->setDiffuse(CLR::RGBA( F32(0.8F), F32(0.8F), F32(0.8F), F32(1.0F)));
        mSun->setSpecular(CLR::RGBA(F32(0.0F), F32(0.0F), F32(0.0F), F32(1.0F)));

        mGeometries.resize(eGeometryCount);

        mGeometries[eArrowX]       = buildModel("ArrowX",     APP::eArrow, k1F,  CLR::kRed);
        mGeometries[eArrowY]       = buildModel("ArrowY",     APP::eArrow, k1F,  CLR::kGreen);
        mGeometries[eArrowZ]       = buildModel("ArrowZ",     APP::eArrow, k1F,  CLR::kBlue);
        mGeometries[eCircleYZ]     = buildModel("CircleYZ",   APP::eTorus, k1F,  CLR::kRed);
        mGeometries[eCircleZX]     = buildModel("CircleZX",   APP::eTorus, k1F,  CLR::kGreen);
        mGeometries[eCircleXY]     = buildModel("CircleXY",   APP::eTorus, k1F,  CLR::kBlue);
        mGeometries[eCubeX]        = buildModel("CubeX",      APP::eCube,  k01F, CLR::kRed);
        mGeometries[eCubeY]        = buildModel("CubeY",      APP::eCube,  k01F, CLR::kGreen);
        mGeometries[eCubeZ]        = buildModel("CubeZ",      APP::eCube,  k01F, CLR::kBlue);
        mGeometries[eLineToCubeX]  = buildModel("",           APP::eLine,  k1F,  CLR::kLightGrey, CLR::kLightGrey);
        mGeometries[eLineToCubeY]  = buildModel("",           APP::eLine,  k1F,  CLR::kLightGrey, CLR::kLightGrey);
        mGeometries[eLineToCubeZ]  = buildModel("",           APP::eLine,  k1F,  CLR::kLightGrey, CLR::kLightGrey);
        mGeometries[eResetTCube]   = buildModel("ResetTCube", APP::eCube,  k01F, CLR::kDarkGrey);
        mGeometries[eResetRCube]   = buildModel("ResetRCube", APP::eCube,  k01F, CLR::kDarkGrey);
        mGeometries[eResetSCube]   = buildModel("ResetSCube", APP::eCube,  k01F, CLR::kDarkGrey);

        updateFrame();

        for(auto lGeo : mGeometries)
        {
            mEditToolsRenderer->insert(lGeo.get());
            lGeo->getLit(mSun);
            ASSERT(lGeo->name().isEmpty() || mActions.contains(lGeo->name()), kInvalidEntityName)
        }
    }


    /*!
     */
    void EditToolsMode::Private::cleanModels()
    {
        for(auto lGeo : mGeometries)
        {
            mEditToolsRenderer->remove(lGeo.get());
        }
        mGeometries.clear();
        mSun = nullptr;
    }


    /*!
     */
    void EditToolsMode::Private::updateFrame()
    {
        SelectablePtr   lObject         = mSelection.lock()->asList().first();
        MATH::V3        lRefPosition    = lObject->position();
        MATH::M3        lRefRotation    = MATH::M3(lObject->orientation());
        MATH::V3        lRefScale       = lObject->scale();
        MATH::M3        lRotation;
        lRotation.loadIdentity();

        for(auto lGeo : mGeometries)
        {
            lGeo->setVisible(false);
        }

        switch(mCurrentTool)
        {
            case eTranslate:
            {
                //mGeometries[eResetTCube]->setTransform(MATH::M4(lRefRotation, lRefPosition)); : pas de reset pour l'instant.

                mGeometries[eArrowX]->setTransform(MATH::M4(lRefRotation, lRefPosition));

                lRotation.createRotationOnZ(kPiBy2);
                mGeometries[eArrowY]->setTransform(MATH::M4(lRefRotation * lRotation, lRefPosition));

                lRotation.createRotationOnY(kPiBy2);
                mGeometries[eArrowZ]->setTransform(MATH::M4(lRefRotation * lRotation, lRefPosition));

                break;
            }
            case eRotate:
            {
                mGeometries[eResetRCube]->setTransform(MATH::M4(lRefRotation, lRefPosition));

                mGeometries[eCircleXY]->setTransform(MATH::M4(lRefRotation, lRefPosition));

                lRotation.createRotationOnX(kPiBy2);
                mGeometries[eCircleZX]->setTransform(MATH::M4(lRefRotation * lRotation, lRefPosition));

                lRotation.createRotationOnY(kPiBy2);
                mGeometries[eCircleYZ]->setTransform(MATH::M4(lRefRotation * lRotation, lRefPosition));

                break;
            }
            case eScale:
            {
                F32 lRScale = kEditToolsScale * mParent->windowCamera().lock()->zoom();

                mGeometries[eResetSCube]->setTransform(MATH::M4(lRefRotation, lRefPosition));

                mGeometries[eCubeX]->setTransform(MATH::M4(lRefRotation, lRefPosition + lRefRotation * MATH::V3(lRScale * lRefScale.x, k0F, k0F)));
                mGeometries[eLineToCubeX]->setTransform(MATH::M4(lRefRotation, lRefPosition, MATH::V3(lRefScale.x, k1F, k1F)));

                lRotation.createRotationOnY(kPiBy2);
                lRotation = lRefRotation * lRotation;
                mGeometries[eCubeY]->setTransform(MATH::M4(lRotation, lRefPosition + lRotation * MATH::V3(k0F, lRScale * lRefScale.y, k0F)));
                mGeometries[eLineToCubeZ]->setTransform(MATH::M4(lRotation, lRefPosition, MATH::V3(lRefScale.z, k1F, k1F)));

                lRotation.createRotationOnZ(kPiBy2);
                lRotation = lRefRotation * lRotation;
                mGeometries[eCubeZ]->setTransform(MATH::M4(lRotation, lRefPosition + lRotation * MATH::V3(k0F, k0F, lRScale * lRefScale.z)));
                mGeometries[eLineToCubeY]->setTransform(MATH::M4(lRotation, lRefPosition, MATH::V3(lRefScale.y, k1F, k1F)));

                break;
            }
            default:
                // Toutes les g�om�tries sont d�j� cach�es.
                break;
        }
    }


//---------------------------------------------------------------------------
//              Edit Tools Mode : Constructeur & Destructeur
//---------------------------------------------------------------------------

    /*! Constructeur.
        @param pWindow    Fen�tre � laquelle cette instance est associ�e
        @param pSelection S�lection active sur laquelle les outils vont travailler
     */
    EditToolsMode::EditToolsMode(WidgetPtr pWindow, SelectionPtr pSelection)
        : StandardMode{pWindow, kEditToolsMode}
        , pthis{this, pSelection}
    { }


    /*! Destructeur.
     */
    EditToolsMode::~EditToolsMode() { }


//---------------------------------------------------------------------------
//              Edit Tools Mode : Gestion des Outils
//---------------------------------------------------------------------------

    /*! Permet de savoir si le sous-mode par d�faut (pas d'outil) est activ�.
        @return VRAI si aucun outil d'�dition n'est activ�, FAUX sinon
     */
    bool EditToolsMode::noToolActivated() const
    {
        return (pthis->mCurrentTool == Private::eDefault);
    }


//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

    /*!
     */
    void EditToolsMode::onEnter()
    {
        ASSERT_EX(!pthis->mSelection.lock()->empty(), kEmptySelection, return;)

        StandardMode::onEnter();

        if (pthis->mEditToolsRenderer == nullptr)
        {
            pthis->mEditToolsRenderer = std::make_shared<EditToolsRenderer>();
            attach(pthis->mEditToolsRenderer);
        }

        pthis->buildModels();
        pthis->setCurrentAction();

        updateWindow();
    }


    /*!
     */
    void EditToolsMode::onExit()
    {
        pthis->mRefPosition     = MATH::kNullV3;
        pthis->mRefOrientation  = MATH::kNullQ;
        pthis->mRefScale        = MATH::V3(k1F, k1F, k1F);
        pthis->mCurrentTool     = Private::eDefault;

        StandardMode::onExit();

        pthis->cleanModels();

        if (pthis->mEditToolsRenderer != nullptr)
        {
            detach(pthis->mEditToolsRenderer);
            pthis->mEditToolsRenderer = nullptr;
        }

        updateWindow();
    }


    /*!
     */
    void EditToolsMode::doUpdate()
    {
        pthis->updateFrame();

        updateWindow();
    }


//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

    /*!
     */
    void EditToolsMode::onPickedChange(PickablePtr pEntity)
    {
        StandardMode::onPickedChange(pEntity);
    }


    /*! @return VRAI si <i>pEntity</i> est la g�om�trie d'un des Edit Tools, FAUX sinon
     */
    bool EditToolsMode::doesManage(PickablePtr pEntity)
    {
        if (auto lEntity = std::dynamic_pointer_cast<Entity>(pEntity.lock()))
        {
            return pthis->mActions.contains(lEntity->name());
        }
        else
        {
            return false;
        }
    }


//---------------------------------------------------------------------------
//              Edit Tools Mode : R�actions aux Ev�nement Souris
//---------------------------------------------------------------------------

    /*! Si un outil �tait activ�, permet de commencer d'�dition du contenu de la s�lection (si l'outil a �t� "pick�").
     */
    void EditToolsMode::press()
    {
        if ((button() == eLeft) && !picked().expired())
        {
            if (auto lPtr = std::dynamic_pointer_cast<Entity>(picked().lock()))
            {
                pthis->mSelected = lPtr;
                pthis->setCurrentAction();

                return;
            }
        }

        StandardMode::press();
    }


    /*! Termine l'�dition en cours (s'il y en avait une).
     */
    void EditToolsMode::release()
    {
        switch(pthis->mCurrentAction)
        {
            case Private::eResetT:
            case Private::eResetR:
            case Private::eResetS:
            {
                Selection::List     lList   = pthis->mSelection.lock()->asList();
                EDIT::SubjectList   lSubjectList{};
                foreach(Selection::SelectablePtr lElement, lList)
                {
                    std::shared_ptr<EDIT::Controller> lCtrler = lElement;
                    lSubjectList << EDIT::Controller::subject(lCtrler);
                }

                switch(pthis->mCurrentAction)
                {
                    case Private::eResetT:
                        // Rien pour l'instant.
                        break;
                    case Private::eResetR:
                    {
                        EDIT::EditRequest<MATH::Q>      lReset(MATH::kNullQ);
                        lSubjectList.perform(&lReset);
                        break;
                    }
                    case Private::eResetS:
                    {
                        EDIT::EditRequest<MATH::V3, 1>  lReset(MATH::V3(k1F, k1F, k1F));
                        lSubjectList.perform(&lReset);
                        break;
                    }
                    default:
                        break;
                }

                doUpdate();
                break;
            }
            default:
                break;
        }

        if (pthis->mSelected != nullptr)
        {
            pthis->mSelected = nullptr;
        }
        else
        {
            StandardMode::release();
        }

        pthis->setCurrentAction();
    }


    /*! Si une �dition a d�but� (outil activ� et cliqu�), d�placer la souris permet d'utiliser l'outil en question.
     */
    void EditToolsMode::doDrag()
    {
        if (pthis->mSelected != nullptr)
        {
            if (button() == eLeft)
            {
                auto                    lCamera = windowCamera().lock();
                Selection::List         lList   = pthis->mSelection.lock()->asList();
                Private::SelectablePtr  lObject = lList.first();
                EDIT::SubjectList       lSubjectList;
                foreach(Selection::SelectablePtr lElement, lList)
                {
                    std::shared_ptr<EDIT::Controller> lCtrler = lElement;
                    lSubjectList << EDIT::Controller::subject(lCtrler);
                    if (lCtrler == nullptr)
                    {
                        lElement = nullptr;
                        // Et ...?
                    }
                }

                QPoint      lMousePos   = currentMousePosition();
                QPoint      lClickPos   = clickMousePosition();
                QSize       lSize       = windowSize();
                F32         lCoeffX     = F32(2.0F / lSize.width());
                F32         lCoeffY     = F32(8.0F / lSize.height());   // Pourquoi 8 ? Je ne sais pas. �a marche, c'est tout.
                F32         lYaw        = lCamera->viewAngle();
                F32         lZoom       = F32(0.5F) * lCamera->zoom();

                MATH::M4    lMV         = lCamera->modelView();
                MATH::M3    lRot        = lMV.rotation();
                lRot.invert();
                F32         lCotanPitch = k1F / F32(tanf(lCamera->pitchAngle()));

                switch(pthis->mCurrentAction)
                {
                    case Private::eMoveX:
                    case Private::eMoveY:
                    case Private::eMoveZ:
                    {
                        MATH::Q     lOrientation    = lObject->orientation();
                        QPoint      lDelta          = lMousePos - lClickPos;
                        MATH::V3    lMove           = lRot * MATH::V3(F32(lDelta.x() * lCoeffX),  - F32(lDelta.y() * lCoeffY), k0F) * lZoom;
                        lMove.z = F32((sin(lYaw) * lMove.x + cos(lYaw) * lMove.y) * lCotanPitch);

                        switch(pthis->mCurrentAction)
                        {
                            case Private::eMoveX:
                            {
                                MATH::V3    lX(k1F, k0F, k0F);
                                lX.rotate(lOrientation);
                                lMove   = MATH::V3(lMove * lX, k0F, k0F);
                                lMove.rotate(lOrientation);
                                break;
                            }
                            case Private::eMoveY:
                            {
                                MATH::V3    lY(k0F, k1F, k0F);
                                lY.rotate(lOrientation);
                                lMove   = MATH::V3(k0F, lMove * lY, k0F);
                                lMove.rotate(lOrientation);
                                break;
                            }
                            case Private::eMoveZ:
                            {
                                MATH::V3    lZ(k0F, k0F, k1F);
                                lZ.rotate(lOrientation);
                                lMove   = MATH::V3(k0F, k0F, lMove * lZ);
                                lMove.rotate(lOrientation);
                                break;
                            }
                            default:    // Impossible n'est pas fran�ais.
                                lMove = MATH::kNullV3;
                                break;
                        }
                        MATH::V3    lPrev = lObject->position();
                        EDIT::ModulateRequest<MATH::V3, 0>  lTranslate( - lPrev + pthis->mRefPosition + lMove);
                        lSubjectList.perform(&lTranslate);
                        break;
                    }
                    case Private::eRotateX:
                    case Private::eRotateY:
                    case Private::eRotateZ:
                    {
                        MATH::V4    lProjectedCenter    = lCamera->projection() * lMV * MATH::V4(lObject->position());

                        QPoint      lCenter(                 ((k1F + lProjectedCenter.x) / 2.0F ) * lSize.width(),
                                            lSize.height() - ((k1F + lProjectedCenter.y) / 2.0F ) * lSize.height());

                        MATH::V3    lCompass    = lRot * MATH::V3{F32(lClickPos.x() - lCenter.x()) * lCoeffX,
                                                                  F32(lClickPos.y() - lCenter.y()) * lCoeffY,
                                                                  k0F} * lZoom;
                        MATH::V3    lDirection  = lRot * MATH::V3{F32(lMousePos.x() - lCenter.x()) * lCoeffX,
                                                                  F32(lMousePos.y() - lCenter.y()) * lCoeffY,
                                                                  k0F} * lZoom;
                        lCompass.z   = F32((sin(lYaw) * lCompass.x   + cos(lYaw) * lCompass.y)   * lCotanPitch);
                        lDirection.z = F32((sin(lYaw) * lDirection.x + cos(lYaw) * lDirection.y) * lCotanPitch);

                        MATH::V3    lX(k1F, k0F, k0F);
                        lX.rotate(pthis->mRefOrientation);
                        MATH::V3    lY(k0F, k1F, k0F);
                        lY.rotate(pthis->mRefOrientation);
                        MATH::V3    lZ(k0F, k0F, k1F);
                        lZ.rotate(pthis->mRefOrientation);

                        switch(pthis->mCurrentAction)
                        {
                            case Private::eRotateX:
                                lCompass    = MATH::V3(k0F, lCompass   * lY, lCompass   * lZ);
                                lDirection  = MATH::V3(k0F, lDirection * lY, lDirection * lZ);
                                break;
                            case Private::eRotateY:
                                lCompass    = MATH::V3(lCompass   * lX, k0F, lCompass   * lZ);
                                lDirection  = MATH::V3(lDirection * lX, k0F, lDirection * lZ);
                                break;
                            case Private::eRotateZ:
                                lCompass    = MATH::V3(lCompass   * lX, lCompass   * lY, k0F);
                                lDirection  = MATH::V3(lDirection * lX, lDirection * lY, k0F);
                                break;
                            default:    // Impossible n'est pas fran�ais.
                                lDirection = lCompass = lX;
                                break;
                        }
                        MATH::Q lRotation   = MATH::Q(lDirection, lCompass);
                        MATH::Q lPrev       = lObject->orientation();
                        MATH::Q lFinal      = (lPrev.conjugated() * pthis->mRefOrientation * lRotation).normalized();
                        EDIT::ModulateRequest<MATH::Q>  lRotate(lFinal);
                        lSubjectList.perform(&lRotate);
                        break;
                    }
                    case Private::eScaleX:
                    case Private::eScaleY:
                    case Private::eScaleZ:
                    {
                        MATH::Q     lOrientation    = lObject->orientation();
                        QPoint      lDelta          = lMousePos - lClickPos;
                        MATH::V3    lMove           = lRot * MATH::V3(F32(lDelta.x() * lCoeffX), - F32(lDelta.y() * lCoeffY), k0F) * lZoom;
                        lMove.z = F32((sin(lYaw) * lMove.x + cos(lYaw) * lMove.y) * lCotanPitch);

                        switch(pthis->mCurrentAction)
                        {
                            case Private::eScaleX:
                            {
                                MATH::V3    lX(k1F, k0F, k0F);
                                lX.rotate(lOrientation);
                                lMove   = MATH::V3(lMove * lX, k0F, k0F);
                                break;
                            }
                            case Private::eScaleY:
                            {
                                MATH::V3    lY(k0F, k1F, k0F);
                                lY.rotate(lOrientation);
                                lMove   = MATH::V3(k0F, lMove * lY, k0F);
                                break;
                            }
                            case Private::eScaleZ:
                            {
                                MATH::V3    lZ(k0F, k0F, k1F);
                                lZ.rotate(lOrientation);
                                lMove   = MATH::V3(k0F, k0F, lMove * lZ);
                                break;
                            }
                            default:    // Impossible n'est pas fran�ais.
                                lMove = MATH::kNullV3;
                                break;
                        }
                        MATH::V3    lPrev   = lObject->scale();
                        EDIT::ModulateRequest<MATH::V3, 1>  lStretch( - lPrev + pthis->mRefScale + lMove / kCubeDelta);
                        lSubjectList.perform(&lStretch);
                        break;
                    }
                    default:
                        break;
                }

                pthis->updateFrame();
            }
        }
        else
        {
            StandardMode::doDrag();
        }

    }


    /*! En fonction des roulements de la molette, passe d'un outil � un autre autre (translation, rotation, changement d'�chelle).
     */
    void EditToolsMode::doWheel()
    {
        switch(pthis->mCurrentTool)
        {
            case Private::eTranslate:
                pthis->mCurrentTool = (wheelDelta() < 0) ? Private::eRotate    : Private::eDefault;
                break;
            case Private::eRotate:
                pthis->mCurrentTool = (wheelDelta() < 0) ? Private::eScale     : Private::eTranslate;
                break;
            case Private::eScale:
                pthis->mCurrentTool = (wheelDelta() < 0) ? Private::eDefault   : Private::eRotate;
                break;
            default:
                pthis->mCurrentTool = (wheelDelta() < 0) ? Private::eTranslate : Private::eScale;
                break;
        }

        pthis->updateFrame();
    }


//------------------------------------------------------------------------------
//                               ModeMgr : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::ModeMgr.
        @version 0.4
     */
    class ModeMgr::Private
    {
    public:
        //! @name Constructeur
        //@{
                Private(WidgetPtr pWindow);             //!< Initialisation.
        //@}
        //! @name Gestion interne des modes
        //@{
        void    setCurrentMode(ModePtr pMode);          //!< Activation d'un mode personnalis�.
        bool    isStandardCurrentMode() const;          //!< Mode standard activ� ?
        bool    areEditToolsCurrentMode() const;        //!< Edit Tools activ�s ?
        //@}
 static int     modifiers(const QKeyEvent* pKeyEvent);  //!< Interpr�tation d'un �v�nement clavier.
        //! @name Attributs li�s aux modes
        //@{
        WidgetPtr           mWindow;                    //!< Fen�tre � laquelle le gestionnaire est associ�.
        QList<ModePtr>      mModes;                     //!< Liste des modes personnalis�s disponibles.
        QMap<QString, long> mModesByName;               //!< Indexation des modes par leur nom.
        SelectionPtr        mSelection;                 //!< Liste d'objets actuellement s�lectionn�s.
        StandardModePtr     mStandardMode;              //!< Mode standard.
        EditToolsModePtr    mEditToolsMode;             //!< Outils d'�dtion.
        ModePtr             mActivatedMode;             //!< Mode personnalis� activ�.
        ModePtr             mCurrentMode;               //!< Mode actuel.
        //@}
        //! @name Autres attributs
        //@{
        using RendererPtr = std::shared_ptr<RDR::Renderer>;
        RendererPtr         mSelectionRenderer;         //!< Renderer pour la s�lection.
        Qt::MouseButton     mCurrentPressedButton;      //!< Bouton enfonc� actuellement.
        int                 mLastPressedKey;            //!< Derni�re touche frapp�e.
        bool                mEnabled;                   //!< Activation de la gestion des �v�nements.
        //@}
    };


    /*! Constructeur.
        @param pWindow Fen�tre � laquelle appartient le gestionnaire de modes dont cette instance est le
        p-impl
     */
    ModeMgr::Private::Private(WidgetPtr pWindow)
        : mWindow{pWindow}
        , mModes{}
        , mModesByName{}
        , mSelection{std::make_shared<Selection>()}
        , mStandardMode{std::make_shared<StandardMode>(mWindow)}
        , mEditToolsMode{std::make_shared<EditToolsMode>(mWindow, mSelection)}
        , mActivatedMode{}
        , mCurrentMode{mStandardMode}
        , mSelectionRenderer{}
        , mCurrentPressedButton{Qt::NoButton}
        , mLastPressedKey{0}
        , mEnabled{true}
    {
        // Mode par d�faut �tabli � la main.
        mCurrentMode.lock()->enter();
    }


//---------------------------------------------------------------------------
//                    Mode Mgr : Gestion Interne des Modes
//---------------------------------------------------------------------------

    /*! Vide la pile de modes et �tablit un mode personnalis� arbitraire en tant que mode actuel.
        @param pMode Mode � �tablir comme l'actuel
        @note Comme pour pushMode() et popMode(), tout mode d�pil� appelle Mode::exit()
     */
    void ModeMgr::Private::setCurrentMode(ModePtr pMode)
    {
        PickablePtr lPicked;

        if (!mCurrentMode.expired())
        {
            lPicked = mCurrentMode.lock()->picked();
            mCurrentMode.lock()->exit();
        }

        mCurrentMode = pMode;

        if (!mCurrentMode.expired())
        {
            mCurrentMode.lock()->picked(lPicked);
            mCurrentMode.lock()->enter();
        }
    }


    /*! @return VRAI si le mode actuel est celui par d�faut (s�lection), FAUX sinon
     */
    bool ModeMgr::Private::isStandardCurrentMode() const
    {
        // Cette version n'est clairement pas optimale, mais elle garantit une bonne encapsulation.
        return (mCurrentMode == mStandardMode);
    }


    /*! @return VRAI si les Edit Tools sont le mode actuel, FAUX sinon
     */
    bool ModeMgr::Private::areEditToolsCurrentMode() const
    {
        return (mCurrentMode == mEditToolsMode);
    }


//---------------------------------------------------------------------------
//                      Mode Mgr : M�thodes Assistantes
//---------------------------------------------------------------------------

    /*! A partir d'une instance de QKeyEvent, retourne la combinaison de Qt::MouseButton correspondant
        aux touches de modification (Control, Shift, Alt, Meta) appuy�es. Ex:
        - pKeyEvent->key()   == Qt::Key_Control (appuy�e)
        - pKeyEvent->modifiers() == QT::SHIFT
        Retoune : Qt::Key_Control | Qt::Key_Shift
        @param pKeyEvent Ev�nement duquel extraire des informations sur les modificateurs
        @return Une combinaison (ou logique) de code de modificateurs
        @note Cette m�thode tient compte de la nature <i>pKeyEvent</i> (touche frapp�e ou rel�ch�e)
     */
    int ModeMgr::Private::modifiers(const QKeyEvent* pKeyEvent)
    {
        int lState = pKeyEvent->modifiers();

        if (pKeyEvent->type() == QEvent::KeyPress)
        {
            switch(pKeyEvent->key())
            {
                case Qt::Key_Shift:
                    lState |= Qt::SHIFT;
                    break;
                case Qt::Key_Control:
                    lState |= Qt::CTRL;
                    break;
                case Qt::Key_Meta:
                    lState |= Qt::META;
                    break;
                case Qt::Key_Alt :
                    lState |= Qt::ALT;
                    break;
            }
        }
        else    // (pKeyEvent->type() == Qt::KeyRelease)
        {
            switch(pKeyEvent->key())
            {
                case Qt::Key_Shift:
                    lState &= ~Qt::SHIFT;
                    break;
                case Qt::Key_Control:
                    lState &= ~Qt::CTRL;
                    break;
                case Qt::Key_Meta:
                    lState &= ~Qt::META;
                    break;
                case Qt::Key_Alt :
                    lState &= ~Qt::ALT;
                    break;
            }
        }

        return lState;
    }


//---------------------------------------------------------------------------
//                   Mode Mgr : Constructeur & destructeur
//---------------------------------------------------------------------------

    /*! Constructeur par d�faut.
        @param pWindow Fen�tre � laquelle cette instance est associ�e
        @note Les modes standard (s�lection/cam�ra, outils d'�dition) sont syst�matiquement cr��s
     */
    ModeMgr::ModeMgr(WidgetPtr pWindow)
        : pthis{pWindow}
    { }


    /*! Destructeur.
     */
    ModeMgr::~ModeMgr()
    {
        activateMode("");
    }


//---------------------------------------------------------------------------
//                     Mode Mgr : Gestion des �v�nements
//---------------------------------------------------------------------------

    /*! Active ou d�sactive la gestion des �v�nements pas le mode.
        @param pEnabled VRAI pour activer la gestion des �v�nements, FAUX pour l'inhiber
     */
    void ModeMgr::setEnabled(bool pEnabled)
    {
        pthis->mEnabled = pEnabled;
    }


    /*! R�ponse � un clic souris.
        @param pEvent Ev�nement souris d�clench� par l'enfoncement d'un bouton de la souris
     */
    void ModeMgr::mousePressEvent(QMouseEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        pthis->mCurrentPressedButton = pEvent->button();
        ModePtr lMode   = pthis->mStandardMode;
        int     lEvent  = ePress;

        switch(pthis->mCurrentPressedButton)
        {
            case Qt::LeftButton:
                lEvent |= eLeft;
                lMode = pthis->mCurrentMode;
                break;
            case Qt::RightButton:
                lEvent |= eRight;
                break;
            case Qt::MidButton:
                lEvent |= eMiddle;
                break;
            default:
                return; // On ne devrait pas passer par l�...
        }

        if (!lMode.expired())
        {
            lMode.lock()->click(pEvent->pos(), lEvent);
        }
    }


    /*! R�ponse � la fin d'un clic souris.
        @param pEvent Ev�nement souris d�clench� par le rel�chement d'un bouton de la souris
     */
    void ModeMgr::mouseReleaseEvent(QMouseEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        ModePtr lMode   = pthis->mStandardMode;
        int     lEvent  = eRelease;

        switch(pthis->mCurrentPressedButton)
        {
            case Qt::LeftButton:
                lEvent |= eLeft;
                lMode = pthis->mCurrentMode;
                break;
            case Qt::RightButton:
                lEvent |= eRight;
                break;
            case Qt::MidButton:
                lEvent |= eMiddle;
                break;
            default:
                return; // On ne devrait pas passer par l�...
        }

        if (!lMode.expired())
        {
            lMode.lock()->click(pEvent->pos(), lEvent);
        }

        pthis->mCurrentPressedButton = Qt::NoButton;
    }


    /*! Ne fait rien (pour l'instant).
     */
    void ModeMgr::mouseDoubleClickEvent(QMouseEvent* /*pEvent*/) { }


    /*! R�ponse � un d�placement de la souris.
        @param pEvent Ev�nement souris d�clench� par un d�placement de la souris
     */
    void ModeMgr::mouseMoveEvent(QMouseEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        switch(pthis->mCurrentPressedButton)
        {
            case Qt::NoButton:
            case Qt::LeftButton:
                if (!pthis->mCurrentMode.expired())
                {
                    pthis->mCurrentMode.lock()->drag(pEvent->pos());
                }
                break;
            case Qt::RightButton:
            case Qt::MidButton:
                pthis->mStandardMode->drag(pEvent->pos());
                break;
            default:
                return; // On ne devrait pas passer par l�...
        }
    }


    /*! R�ponse � un roulement de la molette.
        @param pEvent Ev�nement souris d�clench� par un roulement de la molette
     */
    void ModeMgr::wheelEvent(QWheelEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        int         lStep       = pEvent->delta() / kWheelDelta;
        PickablePtr lPicked     = pthis->mCurrentMode.lock()->picked();
        bool        lOnSelected = pthis->mEditToolsMode->manages(lPicked);

        if (!lOnSelected)
        {
            const OBJ::GameObject*  lPickedGO   = dynamic_cast<const OBJ::GameObject*>(lPicked.lock().get());
            Selection::List         lList       = pthis->mSelection->asList();
            foreach(SelectablePtr lElement, lList)
            {
                if (lElement->manages(lPickedGO))
                {
                    lOnSelected = true;
                    break;
                }
            }
        }

        if (pthis->areEditToolsCurrentMode())
        {
            if (lOnSelected)
            {
                pthis->mEditToolsMode->wheel(lStep);

                if (pthis->mEditToolsMode->noToolActivated())
                {
                    pthis->setCurrentMode(pthis->mStandardMode);
                }
            }
            else
            {
                pthis->mStandardMode->wheel(lStep);
                pthis->mEditToolsMode->update();
            }
        }
        else
        {
            if (lOnSelected)
            {
                pthis->setCurrentMode(pthis->mEditToolsMode);
                pthis->mEditToolsMode->wheel(lStep);
            }
            else
            {
                pthis->mStandardMode->wheel(lStep);
            }
        }
    }


    /*! R�ponse � l'enfoncement d'une touche du clavier.
        @param pEvent Ev�nement d�clench� par la frappe d'une touche du clavier
     */
    void ModeMgr::keyPressEvent(QKeyEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        switch(pEvent->key())
        {
            case Qt::Key_Control:
            case Qt::Key_Shift:
                if (!pthis->mCurrentMode.expired())
                {
                    pthis->mCurrentMode.lock()->modifiersChanged(Private::modifiers(pEvent));
                }
                break;
            default:
                break;
        }
    }


    /*! R�ponse au rel�chement d'une touche du clavier.
        @param pEvent Ev�nement d�clench� par le rel�vement d'une touche du clavier
     */
    void ModeMgr::keyReleaseEvent(QKeyEvent* pEvent)
    {
        if (!pthis->mEnabled)
        {
            return;
        }

        switch(pEvent->key())
        {
            /*case Qt::Key_Escape:
                if (!pthis->mCurrentMode.expired())
                {
                    pthis->setCurrentMode(pthis->mStandardMode);
                }
                break;*/
            case Qt::Key_Control:
            case Qt::Key_Shift:
                if (!pthis->mCurrentMode.expired())
                {
                    pthis->mCurrentMode.lock()->modifiersChanged(0);
                }
                break;
            default:
                break;
        }
    }


    /*! Force une mise � jour du mode actuel.
     */
    void ModeMgr::forceUpdate()
    {
        if (!pthis->mCurrentMode.expired())
        {
            pthis->mCurrentMode.lock()->update();
        }
    }


//---------------------------------------------------------------------------
//                Mode Mgr : Gestion des modes suppl�mentaires
//---------------------------------------------------------------------------

    /*! Enregistre d'un mode personnalis�. Celui-ci pourra �tre activ� � l'aide de activateMode().
        @param pMode Nouveau mode pour la fen�tre 
     */
    void ModeMgr::registerMode(ModePtr pMode)
    {
        ASSERT_EX(!pMode.expired(), kUnknownMode, return;)
        ASSERT_EX(!pthis->mModesByName.contains(pMode.lock()->name()), kAlreadyRegisteredMode, return;)

        long    lIndex = pthis->mModes.size();

        pthis->mModesByName[pMode.lock()->name()] = lIndex;
        pthis->mModes.push_back(pMode);
    }


    /*! 
     */
    void ModeMgr::flushModes()
    {
        activateMode("");

        pthis->mModes.clear();
        pthis->mModesByName.clear();
    }


    /*! Active un mode personnalis� (les modes cam�ra et Edit Tools pourront �tre temporairement
        "empil�s" au-dessus de ce mode, pour des manipulations ponctuelles).
        @param pModeName Nom du mode � activer
        @note La pile de modes est vid�e (le mode s�lection redevient actif) si aucun mode disponible
        ne s'appelle <i>pModeName</i>
     */
    void ModeMgr::activateMode(QString pModeName)
    {
        if (pthis->mModesByName.contains(pModeName))
        {
            if (pthis->mActivatedMode.expired() || (pModeName != pthis->mActivatedMode.lock()->name()))
            {
                pthis->mActivatedMode = pthis->mModes[pthis->mModesByName[pModeName]];
                pthis->setCurrentMode(pthis->mActivatedMode);
            }
        }
        else
        {
            pthis->mActivatedMode.reset();
            pthis->setCurrentMode(pthis->mStandardMode);
        }

        pthis->mWindow->setFocus();
    }


//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

    /*!
     */
    void ModeMgr::picked(PickablePtr pEntity)
    {
        StandardModePtr    lCurrentMode = pthis->mCurrentMode.lock();

        if (lCurrentMode != nullptr)
        {
            lCurrentMode->picked(pEntity);
        }
    }


    /*!
     */
    void ModeMgr::select(SelectablePtr pObject)
    {
        if (pObject != nullptr)
        {
            SelectionPtr   lSelection  = pthis->mSelection;
            if (!lSelection->isSelected(pObject))
            {
                lSelection->insert(pObject);
                if (pthis->mSelectionRenderer == nullptr)
                {
                    pthis->mSelectionRenderer = std::make_shared<SelectionRenderer>(lSelection);
                    pthis->mWindow->attach(pthis->mSelectionRenderer);
                }
            }
            else
            {
                lSelection->remove(pObject);
                if (lSelection->empty())
                {
                    if (pthis->areEditToolsCurrentMode())
                    {
                        if (!pthis->mActivatedMode.expired())
                        {
                            pthis->setCurrentMode(pthis->mActivatedMode);
                        }
                        else
                        {
                            pthis->setCurrentMode(pthis->mStandardMode);
                        }
                    }

                    if (pthis->mSelectionRenderer != nullptr)
                    {
                        pthis->mWindow->detach(pthis->mSelectionRenderer);
                        pthis->mSelectionRenderer = nullptr;
                    }
                }
                else
                {
                    pthis->mEditToolsMode->update();
                }
            }

            pthis->mWindow->update();
        }
    }


    /*!
     */
    void ModeMgr::clearSelection()
    {
        pthis->mSelection->clear();
    }


    /*! Ajout/retrait d'un objet � la s�lection.
     */
    ModeMgr::ConstSelectionPtr ModeMgr::selection() const
    {
        return pthis->mSelection;
    }
}
