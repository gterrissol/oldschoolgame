/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_GAMEVIEW_HH
#define QT3D_GAMEVIEW_HH

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/GameView.hh
    @brief En-t�te de la classe QT3D::GameView.
    @author @ref Guillaume_Terrissol
    @date 3 Juillet 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QStackedWidget>

#include "CORE/Synchronize.hh"
#include "Module/MainView.hh"

#include "Widget.hh"

namespace QT3D
{
    /*! @brief Vue principale sur le jeu.
        @version 0.6

     */
    class GameView : public QT3D::Widget
    {
        Q_OBJECT
    public:
        //! @name Type de pointeur
        //@{
        using GameEntityPtr = std::shared_ptr<OBJ::GameEntity>;     //!< Pointeur sur entit� de jeu.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    GameView(OSGi::Context* pContext,
                             QWidget*       pParent = nullptr);     //!< Constructeur.
virtual             ~GameView();                                    //!< Destructeur.
        //@}
        //! @name [D�s]Initialisation
        //@{
        void        boot();                                         //!< 
        void        halt();                                         //!< 
        //@}
        //! @name Lancement du moteur
        //@{
        void        start();                                        //!< 
        bool        step();                                         //!< 
        void        pause();                                        //!< 
        void        resume();                                       //!< 
        void        stop();                                         //!< 
        //@}
        void        save();                                         //!< 

    signals:

        void        edit(EDIT::Controller::Ptr pController);        //!< Edition d'une donn�e.
        void        leave(EDIT::Controller::Ptr pController);       //!< Arr�t de l'�dition d'une donn�e.

    protected:
        //! @name R�impl�mentation
        //@{
        void        updateOnUndoRedo() override;                    //!< Mise � jour suite � un undo ou un redo.
        void        manage(GameEntityPtr pEntity);                  //!< 
        void        manageDefault();                                //!< 

        void        mouseMoveEvent(QMouseEvent* pEvent) override;   //!< Gestion des d�placements du curseur de la souris.
        void        wheelEvent(QWheelEvent* pEvent) override;       //!< Gestion des roulements ou clics de la molette de la souris.

        void        glInit() override;                              //!< Initialisation Qt.
        void        paintGL() override;                             //!< Rafra�chissement Qt.
        void        resizeGL(int pWidth, int pHeight) override;     //!< Redimensionnement Qt.

        void        onCleanUp() override;                           //!< Nettoyage.
        //@}
    private:

        void        tryToManageAt(QPoint pPosition);                //!< Tentative de gestion d'une entit�.
        void        createViewport() override;                      //!< Cr�ation du viewport.
        ViewportPtr viewport() const override;                      //!< Viewport OpenGL.

        Q_CUSTOM_DECLARE_PRIVATE(GameView)
    };


    /*! @brief Vue 3D sur l'univers.
        @version 0.3

        Afin de ne pr�senter une vue sur le monde que lorsqu'un fichier est ouvert, ce widget empile deux vues :
        - une vue 3D, classe d�riv�e de QT3D::Widget (accessible via worldView()),
        - une vue vide, par d�faut (pas de fichier ouvert).
     */
    class View : public QStackedWidget, public CORE::Synchronized, public Module::MainView
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    View(OSGi::Context* pContext, QWidget* pParent = nullptr);  //!< Constructeur.
virtual             ~View();                                                    //!< Destructeur.
        //@}
    protected:

        void        changeEvent(QEvent* pEvent) override;                       //!< Traitement d'un �v�nement Qt.

    private:

        QWidget*    getWorldView() const override;                              //!< Acc�s � la vue du monde

        GameView*   mGLPage;                                                    //!< Vue du monde.
        QWidget*    mBlankPage;                                                 //!< Vue vide.
    };
}

#endif  // De QT3D_GAMEVIEW_HH
