/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Widget.hh"

/*! @file IDE/Module/QT3D/Widget.cc
    @brief M�thodes (non-inline) de la classe QT3D::Widget.
    @author @ref Guillaume_Terrissol & @ref Vincent_David
    @date 19 Septembre 2005 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>
#include <limits>
#include <typeinfo>
#include <QHash>
#include <QMap>
#include <QMouseEvent>
#include <QSet>
#include <QTimer>
#include <OSGi/ServiceRegistry.hh>

#ifdef RGB
#undef RGB
#endif

#include "CAMera/BaseCamera.hh"
#include "CoLoR/Colors.hh"
#include "CORE/GameService.hh"
#include "EDITion/Subject.hh"
#include "GEOmetry/AABBox.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "GraphXView/EngineViewport.hh"
#include "GraphXView/Viewport.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "OBJect/GameObject.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Shared.tcc"
#include "UTIlity/Rect.hh"

#include "Controller.hh"
#include "ErrMsg.hh"
#include "GameEntityService.hh"
#include "MobileController.hh"
#include "Mode.hh"

class QMenu;

namespace
{
    const F32   kGLScale    = F32(k1F / 4294967296.0F); //!< Echelle pour le picking OpenGL (g�n�rique).
    const F32   kATIScale   = F32(k1F /   16777216.0F); //!< Echelle pour le picking OpenGL (sp�cifique aux cartes ATI, pour cause de bug).
}


namespace QT3D
{
//------------------------------------------------------------------------------
//                                Widget : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::Widget.
        @version 0.4
     */
    class Widget::WidgetPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Widget)
        std::shared_ptr<Widget> mQ;
    public:
        //! @name "Constructeurs"
        //@{
                WidgetPrivate(Widget*        pParent,
                              OSGi::Context* pContext); //!< Constructeur.
        void    init();                                 //!< Initialisation.
        //@}
        //! @name Attributs
        //@{
        OSGi::Context*              mContext;           //!< Contexte d'ex�cution.
        QMap<int, QString>          mModes;             //!< Modes class�s par types de sujet.
        QMenu*                      mContextMenu;       //!< Menu contextuel.
        QVector<RendererPtr>        mIDERenderers;      //!< Renderers purement IDE.
        std::shared_ptr<ModeMgr>    mModeMgr;           //!< Gestionnaire de modes de travail.
        ERender                     mCurrentRender;     //!< Type de renderers activ�s.
        bool                        mATIHack;           //!< Activation d'une correction de bug sur les cartes ATI.
        bool                        mDragged;           //!< Un drag a-t-il eu lieu ?
        //@}
    };


//---------------------------------------------------------------------------
//                          P-Impl : "Constructeurs"
//---------------------------------------------------------------------------

    /*! Constructeur par d�faut.
        @param pParent  Instance propri�taire.
        @param pContext Contexte d'ex�cution.
     */
    Widget::WidgetPrivate::WidgetPrivate(Widget* pParent, OSGi::Context* pContext)
        : q{pParent}
        , mQ(pParent, STL::null_delete())
        , mContext{pContext}
        , mModes()
        , mContextMenu(nullptr)
        , mIDERenderers()
        , mModeMgr()
        , mCurrentRender(eEditor)
        , mATIHack(false)
        , mDragged(false)
    { }


    /*!
     */
    void Widget::WidgetPrivate::init()
    {
        Q_Q(Widget);

        q->setWindowTitle(tr("3D view"));
        q->setFocusPolicy(Qt::StrongFocus);

        q->connect(EDIT::SubjectManager::instance("module"), SIGNAL(undoDone()), SLOT(updateOnUndoRedo()));
        q->connect(EDIT::SubjectManager::instance("module"), SIGNAL(redoDone()), SLOT(updateOnUndoRedo()));

        mModeMgr = std::make_shared<ModeMgr>(mQ);

#if   (GXD_IMPL == 0)
        QString lVendor = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL

        // V�rifie si "ATI" est tout au d�but de la cha�ne de caract�res du vendeur.
        // Attention il ne faut pas chercher au milieu, sinon la recherche dans "NVIDIA Corporation" est fructueuse !
        mATIHack = (lVendor.indexOf(QString("ATI"), 0, Qt::CaseInsensitive) == 0);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*  Format � utiliser pour les instances de QGLWidget.
     */
    const QGLFormat kGLFormat(QGL::DoubleBuffer | QGL::DepthBuffer | QGL::Rgba | QGL::AlphaChannel | QGL::StencilBuffer | QGL::DirectRendering);


    /*! Constructeur.
        @param pContext Contexte d'ex�cution.
        @param pParent  Widget parent.
     */
    Widget::Widget(OSGi::Context* pContext, QWidget* pParent)
        : QGLWidget{kGLFormat, pParent, nullptr}
        , pthis{this, pContext}
    {
        Q_D(Widget);

        d->init();
    }


    /*! Destructeur.
     */
    Widget::~Widget() { }


//------------------------------------------------------------------------------
//                              Syst�me de Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void Widget::attach(RendererPtr pRenderer)
    {
        Q_D(Widget);

        if (!pRenderer.expired())
        {
            viewport()->attach(pRenderer);

            pRenderer.lock()->enable(d->mCurrentRender == eEditor);
            if (!d->mIDERenderers.contains(pRenderer))
            {
                d->mIDERenderers.push_back(pRenderer);
            }
        }
    }


    /*!
     */
    void Widget::detach(RendererPtr pRenderer)
    {
        Q_D(Widget);

        if (!pRenderer.expired())
        {
            viewport()->detach(pRenderer);
            int lIndex = d->mIDERenderers.indexOf(pRenderer);
            Q_ASSERT_X(lIndex != -1, "void Widget::detach(RendererPtr pRenderer)", "Renderer not found");
            d->mIDERenderers[lIndex] = d->mIDERenderers.back();
            d->mIDERenderers.pop_back();
        }
    }


    /*!
     */
    void Widget::enableRenderers(ERender pWhich)
    {
        Q_D(Widget);

        if (d->mCurrentRender != pWhich)
        {
            bool    lEnableAll  = true;
            bool    lEnableIDE  = true;

            switch(pWhich)
            {
                case eEngine:
                    lEnableIDE  = false;
                    break;
                case eEditor:
                    lEnableAll  = false;
                    break;
                default:
                    return;
            }

            viewport()->enableRenderers(lEnableAll);
            for(auto lIter : d->mIDERenderers)
            {
                lIter.lock()->enable(lEnableIDE);
            }

            d->mCurrentRender = pWhich;
            d->mModeMgr->setEnabled(lEnableIDE);
        }
    }


    /*!
     */
    Widget::CameraPtr Widget::camera() const
    {
        return viewport()->camera();
    }


//------------------------------------------------------------------------------
//                               Gestion des Modes
//------------------------------------------------------------------------------

    /*! Enregistrement d'un mode.
     */
    void Widget::registerMode(ModePtr pCustomMode, int pType)
    {
        Q_D(Widget);

        ASSERT_EX(!d->mModes.contains(pType), kAlreadyRegisteredMode, return;)

        d->mModeMgr->registerMode(pCustomMode);
        d->mModes[pType] = pCustomMode->name();
    }


    /*! Active manuellement le mode identifi� par <i>pType</i>
        @param pType Type du mode � activer
     */
    void Widget::activateMode(int pType)
    {
        Q_D(Widget);

        if (pType != 0)
        {
            if (d->mModes.contains(pType))
            {
                ASSERT_EX(d->mModes.contains(pType), kUnknownMode, return;)

                d->mModeMgr->activateMode(d->mModes[pType]);
            }
        }
        else
        {
            d->mModeMgr->activateMode("");
        }
    }


//------------------------------------------------------------------------------
//                                   S�lection
//------------------------------------------------------------------------------

    /*! @sa unProject()
     */
    MATH::V3 Widget::pickPoint(QPoint pOnScreen) const
    {
        const_cast<Widget*>(this)->makeCurrent();
        // Y est invers� car les ordonn�es sont invers�es par rapport � OpenGL.
        return camera().lock()->pickPoint(U16(pOnScreen.x()), U16(height() - pOnScreen.y()));
    }


    /*!
     */
    void Widget::picked(PickablePtr pEntity)
    {
        Q_D(Widget);

        d->mModeMgr->picked(pEntity);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Widget::cleanUp()
    {
        Q_D(Widget);

        onCleanUp();

        activateMode(0);
        d->mIDERenderers.clear();
        d->mModeMgr->flushModes();
        d->mModeMgr->clearSelection();
        d->mModes.clear();
    }


//------------------------------------------------------------------------------
//                                 Widget : Slot
//------------------------------------------------------------------------------

    /*!
     */
    void Widget::updateOnUndoRedo()
    {
        Q_D(Widget);

        // Mise � jour du mode actuel.
        d->mModeMgr->forceUpdate();
    }


//------------------------------------------------------------------------------
//                             Gestion OpenGL par Qt
//------------------------------------------------------------------------------

    /*! Cette m�thode est appel�e avant le premier appel � paintGL() ou resizeGL() et, par la suite,
        chaque fois qu'un nouveau contexte est assign� au widget.
     */
    void Widget::initializeGL()
    {
        Q_D(Widget);

        createViewport();

        viewport()->initialize();
        camera().lock()->setDepthScale(d->mATIHack ? kATIScale : kGLScale);
    }


    /*! Cette m�thode est appel�e chaque fois que le widget doit �tre repeint.
     */
    void Widget::paintGL()
    {
        if (isEnabled())
        {
            viewport()->paint();
            camera().lock()->postRender();
        }
        else
        {
            viewport()->clear();
        }
    }


    /*! Cette m�thode est appel�e chaque fois que le widget a �t� redimensionn�.
        @param pWidth  Nouvelle largeur de la fen�tre
        @param pHeight Nouvelle hauteur de la fen�tre
     */
    void Widget::resizeGL(int pWidth, int pHeight)
    {
        QGLWidget::resizeGL(pWidth, pHeight);

        viewport()->resize(I32(pWidth), I32(pHeight));
    }


//------------------------------------------------------------------------------
//               Gestion de la Souris & du Clavier (pour Edition)
//------------------------------------------------------------------------------

    /*!
     */
    void Widget::mousePressEvent(QMouseEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->mousePressEvent(pEvent);
        d->mDragged = false;
    }


    /*!
     */
    void Widget::mouseReleaseEvent(QMouseEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->mouseReleaseEvent(pEvent);

        if (!d->mDragged && (pEvent->button() == Qt::LeftButton))
        {
            PickablePtr lPicked = pick(pEvent->pos());

            std::shared_ptr<OBJ::GameObject>    lGO         = std::dynamic_pointer_cast<OBJ::GameObject>(lPicked.lock());
            if (auto lGameEntityService = context()->services()->findByTypeAndName<GameEntityService>("fr.osg.ide.module.3d.game_entity"))
            {
                Selection::SelectablePtr        lSelected   = std::dynamic_pointer_cast<MobileController>(lGameEntityService->createController(lGO.get()));

                d->mModeMgr->select(lSelected);
            }
        }
    }


    /*!
     */
    void Widget::mouseDoubleClickEvent(QMouseEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->mouseDoubleClickEvent(pEvent);
    }


    /*!
     */
    void Widget::mouseMoveEvent(QMouseEvent* pEvent)
    {
        Q_D(Widget);

        d->mDragged = true;
        d->mModeMgr->mouseMoveEvent(pEvent);
    }


    /*!
     */
    void Widget::paintEvent(QPaintEvent* pEvent)
    {
        QGLWidget::paintEvent(pEvent);
    }

    /*!
     */
    void Widget::wheelEvent(QWheelEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->wheelEvent(pEvent);
    }


    /*!
     */
    void Widget::keyPressEvent(QKeyEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->keyPressEvent(pEvent);
    }


    /*!
     */
    void Widget::keyReleaseEvent(QKeyEvent* pEvent)
    {
        Q_D(Widget);

        d->mModeMgr->keyReleaseEvent(pEvent);
    }


//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    OSGi::Context* Widget::context() const
    {
        return pthis->mContext;
    }


//------------------------------------------------------------------------------
//                                    Picking
//------------------------------------------------------------------------------

    /*! Surbrillance,
        S�lection (multiple, ajout, retrait),
        Copie, collage, coupage (+ supression simple),
        D�placement du point de vue,
        Edit Tools sur un objet pour �dition,
        Affichage des objets dans un autre style (fil de fer, points, normales, courbes de niveaux,...
        => multipasse pour tout afficher)
     */
    Widget::PickablePtr Widget::pick(const QPoint& pPoint)
    {
        return viewport()->pick(UTI::Point{I32(pPoint.x()), I32(pPoint.y())});
    }


//------------------------------------------------------------------------------
//                                Widget : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::Widget.
        @version 0.4
     */
    class Viewer3D::Viewer3DPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Viewer3D)
    public:

                Viewer3DPrivate(Viewer3D* pParent);     //!< Constructeur.

        void    animate(bool pEnabled);                 //!< [D�s]Activation de l'animation du mod�le affich�.

        ViewportPtr                 mViewport;          //!< Viewport OpenGL.
        class Renderer;
        std::shared_ptr<Renderer>   mRenderer;          //!< Renderer.
        QTimer                      mTimer;             //!< Timer pour le rafra�chissement cadenc�.
        bool                        mAnimationEnabled;  //!< 
    };


    /*!
     */
    Viewer3D::Viewer3DPrivate::Viewer3DPrivate(Viewer3D* pParent)
        : q{pParent}
        , mViewport{}
        , mRenderer{}
        , mTimer{}
        , mAnimationEnabled{true}
    { }


    /*!
     */
    void Viewer3D::Viewer3DPrivate::animate(bool pEnabled)
    {
        if (pEnabled)
        {
            mTimer.start();
        }
        else
        {
            mTimer.stop();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Renderer pour une apparence unique
        @version 0.4
     */
    class Viewer3D::Viewer3DPrivate::Renderer : public RDR::Renderer
    {
    public:
                Renderer(OSGi::Context* pContext)
                    : mAppearance()
                    , mContext{pContext}
                    , mOffset(MATH::kNullV3)
                    , mYaw(k0F)
                    , mIsLightOn(true)
                {
                    mSun = std::make_shared<LIT::DirectionalLight>();
                    mSun->setDirection(MATH::V4(+k1F, +k1F, +k1F, k0F).normalized());
                    mSun->setAmbient(CLR::RGBA(F32(0.2F), F32(0.2F), F32(0.2F), k1F));
                    mSun->setDiffuse(CLR::RGBA(F32(0.8F), F32(0.8F), F32(0.8F), k1F));
                    mSun->setSpecular(CLR::RGBA(k0F, k0F, k0F, k1F));
                }
virtual         ~Renderer() { }

        void    display(AppearancePtr pAppearance)
        {
            if (mAppearance != nullptr)
            {
                // Un peu lourd, mais bon...
                std::shared_ptr<GEO::Geometry const>       lConstGeo       = std::const_pointer_cast<AppearancePtr::element_type const>(mAppearance)->geometry();
                std::shared_ptr<GEO::GeometryRsc const>    lConstGeoRsc    = std::dynamic_pointer_cast<GEO::GeometryRsc const>(lConstGeo);

                if (lConstGeoRsc != nullptr)
                {
                    std::shared_ptr<GEO::GeometryRsc>      lGeoRsc         = std::const_pointer_cast<GEO::GeometryRsc>(lConstGeoRsc);
                    if (auto lGameService = mContext->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game"))
                    {
                        lGameService->geometries()->release(lGeoRsc);
                    }
                }
            }

            mAppearance = pAppearance;
        }

        void switchLight(bool pOn)
        {
            mIsLightOn = pOn;
        }

        bool isDisplaying() const
        {
            return (mAppearance != nullptr);
        }

        void    setOffset(MATH::V3 pOffset)
        {
            mOffset = pOffset;
        }

        void    rotate()
        {
            mYaw += F32(3.6F) * kDegToRad;
        }


    private:

virtual void    dumpAppearances()
        {
            if (mAppearance != nullptr)
            {
                if (mIsLightOn)
                {
                    if (mAppearance->litingLights()->count() == 0)
                    {
                        mAppearance->getLit(mSun);
                    }
                }
                else
                {
                    if (mAppearance->litingLights()->count() != 0)
                    {
                        mAppearance->ungetLit(mSun);
                    }
                }

                MATH::M3   lRotation;
                lRotation.createRotationOnZ(mYaw);
                MATH::M4   lTransform(lRotation, mOffset);

                pushAppearance(mAppearance, lTransform);
            }
        }

        AppearancePtr                           mAppearance;    //!< 
        OSGi::Context*                          mContext;       //!< 
        std::shared_ptr<LIT::DirectionalLight>  mSun;           //!< Lux fiat.
        MATH::V3                                mOffset;        //!< 
        F32                                     mYaw;           //!< 
        bool                                    mIsLightOn;     //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Viewer3D::Viewer3D(OSGi::Context* pContext, QWidget* pParent)
        : Widget{pContext, pParent}
        , pthis{this}
    {
        Q_D(Viewer3D);

        d->mRenderer = std::make_shared<Viewer3DPrivate::Renderer>(pContext);
        d->mTimer.setInterval(50);
        connect(&d->mTimer, SIGNAL(timeout()), SLOT(refreshView()));
    }


    /*! Destructeur.
     */
    Viewer3D::~Viewer3D() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Nettoyage.
     */
    void Viewer3D::onCleanUp()
    {
        // Rien pour l'instant.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Viewer3D::createViewport()
    {
        Q_D(Viewer3D);

        if (d->mViewport == nullptr)
        {
            d->mViewport = STL::makeShared<GXV::Viewport>();
            attach(d->mRenderer);
        }
    }


    /*!
     */
    Viewer3D::ViewportPtr Viewer3D::viewport() const
    {
        Q_D(const Viewer3D);

        return d->mViewport;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    void Viewer3D::refreshView()
    {
        Q_D(Viewer3D);

        d->mRenderer->rotate();
        update();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Viewer3D::animate(bool pEnabled)
    {
        Q_D(Viewer3D);

        d->mAnimationEnabled = pEnabled;
        d->animate(pEnabled && (d->mRenderer->isDisplaying() != 0));
        update();
    }


    /*!
     */
    void Viewer3D::light(bool pEnabled)
    {
        Q_D(Viewer3D);

        d->mRenderer->switchLight(pEnabled);
        update();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Viewer3D::display(AppearancePtr pAppearance)
    {
        Q_D(Viewer3D);

        if (pAppearance != nullptr)
        {
            std::shared_ptr<GEO::Geometry const>    lGeo = std::dynamic_pointer_cast<AppearancePtr::element_type const>(pAppearance)->geometry();
            GEO::AABBox                             lBox{lGeo};

            d->mViewport->camera()->setZoom(lBox.dimensions().max() * F32(2.0F));
            d->mRenderer->setOffset(- lBox.center());
        }

        d->mRenderer->display(pAppearance);
        animate(d->mAnimationEnabled);
    }

//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var Widget::WidgetPrivate::mContextMenu
        Menu contextuel : � remplir (puis afficher) en fonction de l'objet cliqu�.
    */
}
