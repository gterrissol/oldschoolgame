/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_QT3D_HH
#define QT3D_QT3D_HH

/*! @file IDE/Module/QT3D/QT3D.hh
    @brief Pr�-d�clarations du module @ref QT3D.
    @author @ref Guillaume_Terrissol
    @date 6 Mai 2008 - 14 Ao�t 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT3D
{
    class Controller;
    class Entity;
    class GameEntityService;
    class GameView;
    class MobileController;
    class Mode;
    class Selection;
    class Viewer3D;
    class Widget;
    class Window;
}

#endif  // De QT3D_QT3D_HH
