/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/Module/QT3D/GameEntityService.inl
    @brief M�thodes inline de la classe QT3D::GameEntityService.
    @author @ref Guillaume_Terrissol
    @date 22 Juillet 2014 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <typeinfo>
#include "STL/Function.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                              Game Entity Service
//------------------------------------------------------------------------------

    /*!
     */
    template<class TType>
    void GameEntityService::checkIn(ControllerMaker pMaker)
    {
        checkIn(QString(typeid(TType).name()), pMaker);
    }
}
