/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MobileController.hh"

/*! @file IDE/Module/QT3D/MobileController.cc
    @brief M�thodes (non-inline) de la classe QT3D::MobileController.
    @author @ref Guillaume_Terrissol
    @date 14 Juin 2009 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/M4.hh"
#include "MATH/M3.hh"
#include "MATH/Q.hh"
#include "MATH/V3.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                         Mobile Controller : Interface
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    MobileController::~MobileController() { }


    /*!
     */
    auto MobileController::appearance() const -> AppearancePtr
    {
        return getAppearance();
    }


    /*!
     */
    MATH::V3 MobileController::position() const
    {
        return getPosition();
    }


    /*!
     */
    MATH::Q MobileController::orientation() const
    {
        return getOrientation();
    }


    /*!
     */
    MATH::V3 MobileController::scale() const
    {
        return getScale();
    }


    /*!
     */
    MATH::M4 MobileController::frame() const
    {
        return MATH::M4{MATH::M3{orientation()}, position(), scale()};
    }


//------------------------------------------------------------------------------
//                    Mobile Controller : M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    void MobileController::setPosition(MATH::V3 pNew)
    {
        doSetPosition(pNew);
    }


    /*!
     */
    void MobileController::setOrientation(MATH::Q pNew)
    {
        doSetOrientation(pNew.normalized());
    }


    /*!
     */
    void MobileController::setScale(MATH::V3 pNew)
    {
        doSetScale(pNew);
    }


    /*!
     */
    void MobileController::translate(MATH::V3 pOffset)
    {
        doSetPosition(position() + pOffset);
    }


    /*!
     */
    void MobileController::rotate(MATH::Q pRotation)
    {
        doSetOrientation((orientation() * pRotation.normalized()).normalized());
    }


    /*!
     */
    void MobileController::stretch(MATH::V3 pScale)
    {
        doSetScale(scale() + pScale);
    }
}
