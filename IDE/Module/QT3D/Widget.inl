/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/Module/QT3D/Widget.inl
    @brief M�thodes inline de la classe QT3D::Widget.
    @author @ref Guillaume_Terrissol
    @date 21 F�vrier 2009 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/SharedPtr.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//               
//------------------------------------------------------------------------------

    /*! Cr�e un mode et le lie � l'instance Widget.
        @param  pType Type associ� au mode � cr�er
        @tparam TMode Type (classe) du mode � cr�er
        @return Le mode cr��
     */
    template<class TMode>
    std::shared_ptr<TMode> Widget::buildMode(int pType)
    {
        std::shared_ptr<Widget> lThis{this, STL::null_delete()};
        auto                    lMode = std::make_shared<TMode>(lThis);

        registerMode(lMode, pType);

        return lMode;
    }



    /*! Cr�e un mode et le lie � l'instance Widget.
        @param  pType Type associ� au mode � cr�er
        @param  pName Nom du mode
        @tparam TMode Type (classe) du mode � cr�er
        @return Le mode cr��
     */
    template<class TMode>
    std::shared_ptr<TMode> Widget::buildMode(int pType, QString pName)
    {
        std::shared_ptr<Widget> lThis{this, STL::null_delete()};
        auto                    lMode = std::make_shared<TMode>(lThis, pName);

        registerMode(lMode, pType);

        return lMode;
    }
}
