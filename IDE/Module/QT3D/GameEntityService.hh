/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_GAMEENTITYSERVICE_HH
#define QT3D_GAMEENTITYSERVICE_HH

/*! @file IDE/Module/QT3D/GameEntityService.hh
    @brief En-t�te de la classe QT3D::GameEntityService.
    @author @ref Guillaume_Terrissol
    @date 22 Juillet 2014 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>
#include <OSGi/Service.hh>

#include "CORE/Private.hh"
#include "Controller.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                              Game Entity Service
//------------------------------------------------------------------------------

    /*! @brief Gestion des entit�s par l'�diteur
        @version 0.3
     */
    class GameEntityService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr             = std::shared_ptr<GameEntityService>;                         //!< Pointeur sur service.
        using ControllerPtr   = std::shared_ptr<Controller>;                                //!< Pointeur sur contr�leur.
        using EntityPtr       = std::weak_ptr<OBJ::GameEntity>;                             //!< Pointeur sur entit� de jeu.
        using ControllerMaker = std::function<ControllerPtr (EntityPtr, OSGi::Context*)>;   //!< Cr�ateur de contr�leur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        GameEntityService(OSGi::Context* pContext);                         //!< Constructeur.
virtual                 ~GameEntityService();                                               //!< Destructeur.
        //@}
        //! @name Contr�leur pour �dition
        //@{
        ControllerPtr   createController(OBJ::GameEntity* pEntity);                         //!< Cr�ation d'un contr�leur.
        template<class TType>
        void            checkIn(ControllerMaker pMaker);                                    //!< Enregistrement d'un cr�ateur de contr�leur.
        void            checkOutAll();                                                      //!< Supprime les r�f�rences vers les cr�ateurs.
        //@}
        //! @name Impl�mentation (service)
        //@{
    protected:

        bool            isType(const std::string& pTypeName) const override;                //!< Est de type ... ?

    private:

        std::string     typeName() const override;                                          //!< Nom du type (r�el).
        //@}
        void            checkIn(QString pTypeName, ControllerMaker pMaker);                     //!< Enregistrement d'un cr�ateur de contr�leur.

        Q_CUSTOM_DECLARE_PRIVATE(GameEntityService)
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "GameEntityService.inl"

#endif  // De QT3D_GAMEENTITYSERVICE_HH
