/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameView.hh"

/*! @file IDE/Module/QT3D/GameView.cc
    @brief M�thodes (non-inline) des classes QT3D::GameView::GameViewPrivate & QT3D::GameView.
    @author @ref Guillaume_Terrissol
    @date 3 Juillet 2006 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QEvent>
#include <QMouseEvent>

#include "CORE/GameService.hh"
#include "PROGram/GameSteps.hh"
#include "UNIverse/GameUniverse.hh"
#include "VIEWport/Window.hh"

#include "ErrMsg.hh"
#include "GameEntityService.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                              P-Impl de GameView
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GameView.
     */
    class GameView::GameViewPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(GameView)
    public:
        //! @name "Constructeurs"
        //@{
                GameViewPrivate(GameView* pParent);                     //!< Constructeur.
        void    init();                                                 //!< Initialisation.
        //@}

        using WeakControllerPtr = std::weak_ptr<Controller>;            //!< Pointeur [faible] sur contr�leur.

        QHash<OBJ::GameEntity*, WeakControllerPtr>  mEntities;          //!< Entit�s.
        OBJ::GameEntity*                            mCurrentEntity;     //!< Entit� actulle.
        std::weak_ptr<APP::Pickable>                mHighlighted;       //!< 

        std::shared_ptr<RDR::Renderer>              mOutisdeRenderer;   //!< Rendu du monde ext�rieur.
        std::shared_ptr<RDR::Renderer>              mTileRenderer;      //!< Rendu des tuiles.
        PROG::Game::Ptr                             mGameEngine;        //!< Moteur de jeu.
        EDIT::Controller::Ptr                       mCurrentController; //!< Contr�leur d'objet actuel.
    };


//------------------------------------------------------------------------------
//                     Impl�mentation du P-Impl de GameView
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Instance de GameView parente
     */
    GameView::GameViewPrivate::GameViewPrivate(GameView* pParent)
        : q{pParent}
        , mEntities{}
        , mCurrentEntity{nullptr}
        , mHighlighted{}
        , mOutisdeRenderer{}
        , mTileRenderer{}
        , mGameEngine{}
    { }


    /*! Initialisation (vide pour l'instant).
     */
    void GameView::GameViewPrivate::init()
    {
        // Pour �viter un rendu pr�matur� (avant la cr�ation du viewport principal).
        q->setEnabled(false);

        q->setMouseTracking(true);

        q->setEnabled(true);//?
    }


//------------------------------------------------------------------------------
//                     GameView : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pContext Contexte d'ex�cution.
        @param pParent  Widget parent.
     */
    GameView::GameView(OSGi::Context* pContext, QWidget* pParent)
        : Widget{pContext, pParent}
        , pthis{this}
    {
        pthis->init();
    }


    /*! Destructeur.
     */
    GameView::~GameView()
    {
        GameView::onCleanUp();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::boot()
    {
        pthis->mGameEngine = context()->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->boot(reinterpret_cast<void*>(winId()));
        manageDefault();
    }


    /*!
     */
    void GameView::halt()
    {
        // Force la sauvegarde.
        save();

        manage(nullptr);
        if (!pthis->mGameEngine.expired())
        {
            context()->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->halt();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::start()
    {
        enableRenderers(eEngine);
    }


    /*! @todo Appeler le tick hardware.
     */
    bool GameView::step()
    {
        return pthis->mGameEngine.lock()->step();
    }


    /*!
     */
    void GameView::pause() { }


    /*!
     */
    void GameView::resume() { }


    /*!
     */
    void GameView::stop()
    {
        enableRenderers(eEditor);

        update();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::save()
    {
        foreach(GameViewPrivate::WeakControllerPtr  lCtrler, pthis->mEntities.values())
        {
            if (!lCtrler.expired())
            {
                lCtrler.lock()->save();
            }
        }
    }


//------------------------------------------------------------------------------
//                                GameView : Slot
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::updateOnUndoRedo()
    {
        Widget::updateOnUndoRedo();

        // Mise � jour du contr�leur actuel.
        if ((pthis->mCurrentEntity != nullptr) && pthis->mEntities.contains(pthis->mCurrentEntity))
        {
            pthis->mEntities[pthis->mCurrentEntity].lock()->takeOver();
        }
    }


//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------

    /*!
     */
    void GameView::manage(GameEntityPtr pEntity)
    {
        if (OBJ::GameEntity* lEntity = pEntity.get())
        {
            Controller::Ptr lController{};
            if (lEntity != pthis->mCurrentEntity)
            {
                // Pr�c�dent contr�leur.
                lController = pthis->mEntities[pthis->mCurrentEntity].lock();
                if (lController != nullptr)
                {
                    emit leave(lController);
                }
                if (!pthis->mEntities.contains(lEntity) || pthis->mEntities[lEntity].expired())
                {
                    if (auto lGameEntityService = context()->services()->findByTypeAndName<GameEntityService>("fr.osg.ide.module.3d.game_entity"))
                    {
                        if ((lController = lGameEntityService->createController(lEntity)))
                        {
                            pthis->mEntities[lEntity] = lController;
                            attach(lController->renderer());

                            pthis->mCurrentController = lController;
                            emit edit(lController);
                        }
                    }
                }
                else
                {
                    lController = pthis->mEntities[lEntity].lock();
                    ASSERT_EX(lController != nullptr, kUndefinedController, return;)
                    lController->takeOver();

                    emit edit(lController);
                }

                // Objet consid�r� pour l'�dition.
                pthis->mCurrentEntity = lEntity;
            }
            else
            {
                lController = pthis->mEntities[pthis->mCurrentEntity].lock();
            }

            // Active le mode associ� au type du contr�leur.
            if (lController)
            {
                activateMode(lController->rTTI());
            }
        }
        else
        {
            if (pthis->mCurrentEntity != nullptr)
            {
                // Plus de sujet � �diter.
                pthis->mCurrentController.reset();
                emit edit(nullptr);
                pthis->mCurrentEntity = nullptr;
                activateMode(0);
            }
            // Sinon, tout est d�j� d�sactiv�.
        }
    }


    /*!
     */
    void GameView::manageDefault()
    {
        PickablePtr    lUniverse = context()->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->universe();
        manage(std::dynamic_pointer_cast<OBJ::GameEntity>(lUniverse.lock()));
    }


//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------

    /*! Permet de d�terminer le sujet � "�diter" [implicitement].
     */
    void GameView::mouseMoveEvent(QMouseEvent* pEvent)
    {
        setMouseTracking(false);

        Widget::mouseMoveEvent(pEvent);

        if (pEvent->button() == Qt::NoButton)
        {
            tryToManageAt(pEvent->pos());
        }

        setMouseTracking(true);
    }


    /*!
     */
    void GameView::wheelEvent(QWheelEvent* pEvent)
    {
        Widget::wheelEvent(pEvent);

        if (pEvent->buttons() == Qt::NoButton)
        {
            tryToManageAt(pEvent->pos());
        }
    }


    /*!
     */
    void GameView::glInit()
    {
        if (isEnabled())
        {
            Widget::glInit();
        }
    }


    /*!
     */
    void GameView::paintGL()
    {
        /*if (auto lGameEngine = pthis->mGameEngine.lock())
        {
            lGameEngine->step();
        }*/
        Widget::paintGL();
    }


    /*! Cette m�thode est appel�e chaque fois que le widget a �t� redimensionn�.
        @param pWidth  Nouvelle largeur de la fen�tre
        @param pHeight Nouvelle hauteur de la fen�tre
     */
    void GameView::resizeGL(int pWidth, int pHeight)
    {
        if (isEnabled())
        {
            Widget::resizeGL(pWidth, pHeight);
            context()->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->resizeViewport(QSize{pWidth, pHeight});
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::onCleanUp()
    {
        pthis->mEntities.clear();

        // R�active le mode par d�faut.
        activateMode(0);

        halt();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void GameView::tryToManageAt(QPoint pPosition)
    {
        // Si s�lection, rien ?
        // Sinon :
        PickablePtr lEntity = pick(pPosition);

        if (!lEntity.expired())
        {
            switch(lEntity.lock()->type())
            {
            case 1:
                manage(std::dynamic_pointer_cast<OBJ::GameEntity>(lEntity.lock()));
                break;
            case 2:
                /*{
                    std::shared_ptr<Entity>    lNewHighlighted = std::dynamic_pointer_cast<Entity>(lEntity.lock());
                    lNewHighlighted->highlight(true);
                }*/
                break;
            default:
                break;
            }
        }
        else
        {
            if (pthis->mCurrentEntity == nullptr)
            {
                manageDefault();
            }
        }

        picked(lEntity);
    }


    /*! Pas de viewport � cr�er : c'est celui du l'univers qui va �tre utilis�.
     */
    void GameView::createViewport() { }


    /*!
     */
    GameView::ViewportPtr GameView::viewport() const
    {
        if (auto lUniverse = context()->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->universe())
        {
            return lUniverse->viewport();
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    View::View(OSGi::Context* pContext, QWidget* pParent)
        : QStackedWidget{pParent}
        , CORE::Synchronized{"file"}
        , Module::MainView{}
        , mGLPage{}
        , mBlankPage{}
    {
        addWidget(mBlankPage = new QWidget{this});
        addWidget(mGLPage = new GameView{pContext, this});
        setCurrentWidget(mBlankPage);
    }

    //!
    View::~View() { }


    /*! @param pEvent Ev�nement Qt � traiter.
        Si @p pEvent est un �v�nement de changement d'activation, affiche la page requise selon le nouvel �tat.
     */
    void View::changeEvent(QEvent* pEvent)
    {
        QStackedWidget::changeEvent(pEvent);
        if (pEvent->type() == QEvent::EnabledChange)
        {
            if (isEnabled())
            {
                mGLPage->boot();
                setCurrentWidget(mGLPage);
            }
            else
            {
                setCurrentWidget(mBlankPage);
                mGLPage->halt();
            }
        }
    }


    /*! @return Le widget de vue sur le monde, m�me s'il n'est pas actuellement affich�.
     */
    QWidget* View::getWorldView() const
    {
        return mGLPage;
    }
}
