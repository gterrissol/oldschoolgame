/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_CONTROLLER_HH
#define QT3D_CONTROLLER_HH

/*! @file IDE/Module/QT3D/Controller.hh
    @brief En-t�te de la classe EDIT::Generic3DController.
    @author @ref Guillaume_Terrissol
    @date 21 Juillet 2014 - 1er D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "EDITion/Controller.hh"
#include "RenDeR/Renderer.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                                  Contr�leurs
//------------------------------------------------------------------------------

    /*! @brief Contr�leur d'objet du moteur.
        @version 0.6
     */
    class Controller : public EDIT::Controller
    {
    public:
        //! @name Types de pointeur
        //@{
        using   Ptr         = std::shared_ptr<Controller>;  //!< Pointeur sur contr�leur 3D.
        using   RendererPtr = std::weak_ptr<RDR::Renderer>; //!< Pointeur sur renderer.
        //@}
        //! @name Interface
        //@{
virtual             ~Controller();                          //!< Destructeur.
        RendererPtr renderer();                             //!< R�cup�ration du rendrerer du sujet �dit�.
        //@}
    protected:

        using EDIT::Controller::Controller;

    private:
        //! @name Comportement � d�finir
        //@{
virtual RendererPtr getRenderer() = 0;                      //!< R�cup�ration du rendrerer du sujet �dit�.
        //@}
    };


    /*! @brief Contr�leur g�n�rique.
        @version 0.9
     */
    template<class TEdited>
    using GenericController = EDIT::GenericController<TEdited, Controller>;
}

#endif  // De QT3D_CONTROLLER_HH
