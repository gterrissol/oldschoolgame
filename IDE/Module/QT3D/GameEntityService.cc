/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameEntityService.hh"

/*! @file IDE/Module/QT3D/GameEntityService.cc
    @brief M�thodes (non-inline) de la classe QT3D::GameEntityService.
    @author @ref Guillaume_Terrissol
    @date 21 Juillet 2014 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>

#include "OBJect/GameObject.hh"
#include "STL/Shared.tcc"

namespace QT3D
{
//------------------------------------------------------------------------------
//                          GameEntityService : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QT3D::GameEntityService.
        @version 0.5
     */
    class GameEntityService::GameEntityServicePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(GameEntityService)
    public:

        GameEntityServicePrivate(GameEntityService* pThat, OSGi::Context* pContext)
            : q{pThat}
            , mMakers{}
            , mContext{pContext}
        { }

        std::map<QString, GameEntityService::ControllerMaker>   mMakers;
        OSGi::Context*                                          mContext;
    };


//------------------------------------------------------------------------------
//               Game Entity Service : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    GameEntityService::GameEntityService(OSGi::Context* pContext)
        : OSGi::Service{}
        , pthis{this, pContext}
    { }


    //! Par d�faut.
    GameEntityService::~GameEntityService() = default;


//------------------------------------------------------------------------------
//                 Game Entity Service : Contr�leur pour �dition
//------------------------------------------------------------------------------

    /*!
     */
    GameEntityService::ControllerPtr GameEntityService::createController(OBJ::GameEntity* pEntity)
    {
        if (pEntity != nullptr)
        {
            QString lName{typeid(*pEntity).name()};

            auto    lMaker = pthis->mMakers.find(lName);
            if (lMaker != pthis->mMakers.end())
            {
                return lMaker->second(std::dynamic_pointer_cast<OBJ::GameEntity>(pEntity->shared_from_this().lock()), pthis->mContext);
            }
            else
            {
                qDebug() << "No controller registered for " << lName;
            }
        }

        return {};
    }


    /*!
     */
    void GameEntityService::checkOutAll()
    {
        pthis->mMakers.clear();
    }


//------------------------------------------------------------------------------
//                Game Entity Service : Impl�mentation (Service)
//------------------------------------------------------------------------------

    /*!
     */
    bool GameEntityService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string GameEntityService::typeName() const
    {
        return typeid(*this).name();
    }


//------------------------------------------------------------------------------
//                 Game Entity Service : Contr�leur pour �dition
//------------------------------------------------------------------------------

    /*! 
     */
    void GameEntityService::checkIn(QString pTypeName, ControllerMaker pMaker)
    {
        pthis->mMakers[pTypeName] = pMaker;
    }
}
