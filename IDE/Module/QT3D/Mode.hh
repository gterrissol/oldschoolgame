/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_MODE_HH
#define QT3D_MODE_HH

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/Mode.hh
    @brief En-t�te de la classe QT3D::Mode.
    @author @ref Guillaume_Terrissol
    @date 9 Novembre 2003 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <Qt>
#include <QList>

#include "APPearance/BaseAppearance.hh"
#include "CAMera/CAMera.hh"
#include "EDITion/EDITion.hh"
#include "MEMory/PImpl.hh"
#include "RenDeR/RenDeR.hh"
#include "STL/STL.hh"

#include "Enum.hh"

class QCursor;
class QKeyEvent;
class QMenu;
class QMouseEvent;
class QPoint;
class QSize;
class QWheelEvent;

namespace QT3D
{
//---------------------------------------------------------------------------
//                                 S�lection
//---------------------------------------------------------------------------

    /*! @brief S�lection d'�l�ments
        @version 0.4

        Cette classe permet de regrouper, de mani�re virtuelle, un certain nombre d'objets affich�s par
        une instance donn�e de QT3D::Widget. Ainsi, certaines commandes d'�dition pourront �tre
        appliqu�es sur plusieurs objets simultan�ment.
        @note Chaque GLWidget poss�de - au moins - une instance de cette classe
     */
    class Selection
    {
    public:
        //! @name Types
        //@{
        using Selectable    = MobileController;             //!< El�ment s�lectionnable.
        using SelectablePtr = std::shared_ptr<Selectable>;  //!< Pointeur sur �l�ment s�lectionnable.
        using List          = QList<SelectablePtr>;         //!< Liste d'�l�ments s�lectionnables.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Selection();                                //!< Constructeur.
                ~Selection();                               //!< Destructeur.
        //@}
        //! @name Op�rations de liste
        //@{
        bool    empty() const;                              //!< S�lection vide ?
        void    insert(SelectablePtr pObject);              //!< Ajout d'un �l�ment.
        void    remove(SelectablePtr pObject);              //!< Retrait d'un �lement.
        bool    isSelected(SelectablePtr pObject) const;    //!< Element s�lectionn� ?
        void    clear();                                    //!< Vide la s�lection.
        List    asList() const;                             //!< Liste des �l�ments port�s.
        //@}

    private:

        FORBID_COPY(Selection)
        FREE_PIMPL()
    };


//---------------------------------------------------------------------------
//                          Modes : Classes de Base
//---------------------------------------------------------------------------

    /*! @brief Mode d'�dition.
        @version 0.4

        Les sous-�diteurs peuvent proposer plusieurs modes de travail (i.e. les m�mes commandes
        provoqueront des actions d'�dition diff�rentes). L'activation de certains d'entre eux est d�finie
        une fois pour toutes (voir QT3D::Mode). Un nouveau mode peut �tre rendu disponible pour un
        �diteur en l'enregistrant simplement aupr�s du gestionnaire de modes d'un viewport (voir
        QT3D::Mode & QT3D::ModeMgr).
     */
    class Mode
    {
    public:
        //! @name Classe amie
        //@{
 friend class ModeMgr;                                          //!< Le gestionnaire conna�t ses modes.
        //@}
        //! @name Types de pointeur
        //@{
        using WidgetPtr   = std::shared_ptr<Widget>;            //!< Pointeur sur Widget 3D.
        using CameraPtr   = std::weak_ptr<CAM::Camera>;         //!< Pointeur sur cam�ra.
        using RendererPtr = std::weak_ptr<RDR::Renderer>;       //!< Pointeur sur Renderer.
        using PickablePtr = APP::Pickable::Ptr;                 //!< Pointeur sur objet pickable.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Mode(WidgetPtr pWindow, QString pName);     //!< Constructeur.
virtual             ~Mode();                                    //!< Destructeur.
        //@}
        //! @name Interface commune � tous les modes
        //@{
        void        enter();                                    //!< Activation du mode.
        bool        isActive() const;                           //!< Mode activ� ?
        void        exit();                                     //!< Sortie du mode.

        void        click(const QPoint& pPos, int pButtons);    //!< Clic souris (bouton utile).
        void        wheel(int pStep);                           //!< Action � la molette.
        void        drag(const QPoint& pPos);                   //!< Glissement de la souris.
        void        displayContextMenu();                       //!< Affichage d'un menu contextuel.
        void        update();                                   //!< Mise � jour (forc�e).
        QString     name() const;                               //!< Nom du mode.
        void        picked(PickablePtr pEntity);                //!< 
        bool        manages(PickablePtr pEntity);               //!< 
        //@}

    protected:
        //! @name Informations sur l'�tat de la souris
        //@{
        QPoint      clickMousePosition() const;                 //!< Position du dernier clic souris.
        QPoint      currentMousePosition() const;               //!< Position actuelle de la souris.
        QPoint      currentDeltaPosition() const;               //!< Variation de la position de la souris.
        int         button() const;                             //!< Dernier �tat des boutons.
        int         wheelDelta() const;                         //!< Rotation de la molette.
        //@}
        //! @name Entr�e & sortie de mode
        //@{
virtual void        onEnter();                                  //!< Pr�paration de l'activation du mode.
virtual void        onExit();                                   //!< Pr�paration de la sortie du mode.
virtual void        doUpdate();                                 //!< Effectue une mise � jour (forc�e).
        //@}
        //! @name Actions sur la fen�tre
        //@{
        CameraPtr   windowCamera();                             //!< Acc�s � la cam�ra de la fen�tre.
        void        setWindowCursor(const QCursor& pCursor);    //!< Modification du curseur de la souris.
        void        unsetWindowCursor();                        //!< Restauration du curseur de la souris.
        MATH::V3    pickPoint(QPoint pPoint);                   //!< S�lection d'un point.
        void        updateWindow();                             //!< Rafra�chissement explicite de la fen�tre.
        void        attach(RendererPtr pRenderer);              //!< Attachement d'un objet de rendu.
        void        detach(RendererPtr pRenderer);              //!< D�tachement d'un objet de rendu.
        QSize       windowSize() const;                         //!< Dimensions de la fen�tre.
        //@}
        //! @name Entit� sous la souris
        //@{
        PickablePtr picked();                                   //!< 
virtual void        onPickedChange(PickablePtr pEntity);        //!< 
virtual bool        doesManage(PickablePtr pEntity);            //!< 
        //@}
        //! @name R�actions aux �v�nement souris
        //@{
virtual void        press() = 0;                                //!< Pression bouton (utile) souris.
virtual void        release() = 0;                              //!< Rel�chement bouton (utile) souris.
virtual void        doWheel() = 0;                              //!< Utilisation de la molette.
virtual void        doDrag() = 0;                               //!< Glissement de la souris.
        //@}

    private:
        //! @name Autres m�thodes
        //@{
virtual void        modifiersChanged(int pNewModifiers);        //!< Prise en compte de nouveaux modificateurs.
virtual QMenu*      buildContextMenu() const;                   //!< Cr�ation d'un menu contextuel.
        //@}
        FORBID_COPY(Mode)
        FREE_PIMPL()
    };


//---------------------------------------------------------------------------
//                              Modes Classiques
//---------------------------------------------------------------------------

    /*! @brief Mode standard (mode par d�faut usuel).
        @version 0.35

        Ce mode g�re la s�lection et la cam�ra.
        Comportement usuel :
        - Drag gauche : d�placement de la cam�ra,
        - Drag droit : rotation de la cam�ra (selon l'axe vertical),
        - Molette : zoom,
        - clic gauche s�lection,
        - Shift + drag gauche : s�lection au lasso,
        - Ctrl + clic gauche : ajout � la s�lection,
        - Clic droit : menu contextuel.
     */
    class StandardMode : public Mode
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                 StandardMode(WidgetPtr pWindow);               //!< Constructeur.
virtual         ~StandardMode();                                //!< Destructeur.
        //@}

    protected:
        //! @name Autre constructeur
        //@{
                StandardMode(WidgetPtr pWindow, QString pName); //!< Constructeur.
        //@}
        //! @name R�actions aux �v�nement souris
        //@{
virtual void    press() override;                               //!< Pression bouton (utile) souris.
virtual void    release() override;                             //!< Rel�chement bouton (utile) souris.
virtual void    doDrag() override;                              //!< Glissement de la souris.
virtual void    doWheel() override;                             //!< Utilisation de la molette.
        //@}

    private:
        //! @name Gestion des modificateurs
        //@{
virtual void    modifiersChanged(int pNewModifiers) override;   //!< Prise en compte de nouveaux modificateurs.
        //@}
        //! @name Autres m�thodes
        //@{
virtual QMenu*  buildContextMenu() const override;              //!< Cr�ation d'un menu contextuel.
        //@}
        //! @name Mode interne
        //@{
        bool    mIsCameraActive;                                //!< 
        bool    mIsSelectionActive;                             //!< 
        //@}
    };


    /*! @brief Edit Tools
        @version 0.25
     */
    class EditToolsMode : public StandardMode
    {
    public:
        //! @name Types de pointeur
        //@{
        using SelectionPtr = std::weak_ptr<Selection>;          //!< Pointeur sur s�lection.
        //@}
        //! @name Constructeur & destructeur
        //@{
                EditToolsMode(WidgetPtr    pWindow,
                              SelectionPtr pSelection);         //!< Constructeur.
virtual         ~EditToolsMode();                               //!< Destructeur.
        //@}
        //! @name Gestion des outils
        //@{
        bool    noToolActivated() const;                        //!< 
        //@}

    protected:
        //! @name Entr�e & sortie de mode
        //@{
virtual void    onEnter() override;                             //!< Pr�paration de l'activation du mode.
virtual void    onExit() override;                              //!< Pr�paration de la sortie du mode.
virtual void    doUpdate() override;                            //!< Effectue une mise � jour (forc�e).
        //@}
        //! @name Entit� sous la souris
        //@{
virtual void    onPickedChange(PickablePtr pEntity) override;   //!< 
virtual bool    doesManage(PickablePtr pEntity) override;       //!< 
        //@}
        //! @name R�actions aux �v�nement souris
        //@{
virtual void    press() override;                               //!< Pression sur un bouton.
virtual void    release() override;                             //!< Rel�chement d'un bouton.
virtual void    doDrag() override;                              //!< Glissement de la souris.
virtual void    doWheel() override;                             //!< Utilisation de la molette.
        //@}
    private:

        FORBID_COPY(EditToolsMode)
        FREE_PIMPL()
    };


//---------------------------------------------------------------------------
//                           Gestionnaire de Modes
//---------------------------------------------------------------------------

    /*! @brief Gestionnaire de modes.
        @version 0.41

        @par M�thode d'�dition standard
        
        @note Cette classe n'est pas un singleton car chacune de ses instances est associ�e � un QT3D::Widget.
     */
    class ModeMgr
    {
    public:
        //! @name Types de pointeur
        //@{
        using StandardModePtr   = std::shared_ptr<Mode>;                //!< Pointeur sur mode de base.
        using EditToolsModePtr  = std::shared_ptr<EditToolsMode>;       //!< Pointeur sur mode de base.
        using ConstModePtr      = std::weak_ptr<Mode const>;            //!< Pointeur sur mode (constant).
        using ModePtr           = std::weak_ptr<Mode>;                  //!< Pointeur sur mode.
        using WidgetPtr         = Mode::WidgetPtr;                      //!< Pointeur sur Widget.
        using PickablePtr       = Mode::PickablePtr;                    //!< Pointeur sur objet pickable.
        using SelectionPtr      = std::shared_ptr<Selection>;           //!< Pointeur sur s�lection (constante).
        using ConstSelectionPtr =  std::shared_ptr<Selection const>;    //!< Pointeur sur s�lection (constante).
        using SelectablePtr     = Selection::SelectablePtr;             //!< Pointeur sur game object.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            ModeMgr(WidgetPtr pWindow);                 //!< Constructeur.
                            ~ModeMgr();                                 //!< Destructeur.
        //@}
        //! @name Gestion des �v�nements
        //@{
        void                setEnabled(bool pEnabled);                  //!< [D�s]Activation.
        void                mousePressEvent(QMouseEvent* pEvent);       //!< Clic souris.
        void                mouseReleaseEvent(QMouseEvent* pEvent);     //!< Fin clic souris.
        void                mouseDoubleClickEvent(QMouseEvent* pEvent); //!< Double clic.
        void                mouseMoveEvent(QMouseEvent* pEvent);        //!< D�placement de la souris.
        void                wheelEvent(QWheelEvent* pEvent);            //!< Roulement molette.
        void                keyPressEvent(QKeyEvent* pEvent);           //!< Pression touche clavier.
        void                keyReleaseEvent(QKeyEvent* pEvent);         //!< Rel�chement touche clavier.
        void                forceUpdate();                              //!< Mise � jour suite � un undo, un redo, ou ...?
        //@}
        //! @name Gestion des modes suppl�mentaires
        //@{
        void                registerMode(ModePtr pMode);                //!< Enregistrement d'un mode personnalis�.
        void                flushModes();                               //!< Suppression des modes personnalis�s.
        void                activateMode(QString pModeName);            //!< Activation d'un mode personnalis�.
        //@}
        void                picked(PickablePtr pEntity);                //!< 
        void                select(SelectablePtr pObject);              //!< 
        void                clearSelection();                           //!< 
        ConstSelectionPtr   selection() const;                          //!< 

    private:

        FORBID_COPY(ModeMgr)
        FREE_PIMPL()
    };
}

#endif  // De QT3D_MODE_HH
