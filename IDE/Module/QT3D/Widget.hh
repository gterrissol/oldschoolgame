/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_WIDGET_HH
#define QT3D_WIDGET_HH

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/Widget.hh
    @brief En-t�te des classes QT3D::Widget, QT3D::Window & QT3D::Viewer3D.
    @author @ref Guillaume_Terrissol
    @date 19 Septembre 2005 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QGLWidget>

#include <OSGi/Context.hh>

#include "APPearance/BaseAppearance.hh"
#include "CAMera/CAMera.hh"
#include "EDITion/EDITion.hh"
#include "GraphXView/GraphXView.hh"
#include "MATH/MATH.hh"
#include "OBJect/OBJect.hh"
#include "CORE/Private.hh"
#include "RenDeR/RenDeR.hh"
#include "UTIlity/UTIlity.hh"

#include "Controller.hh"
#include "Enum.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                              Viewport OpenGL Qt
//------------------------------------------------------------------------------

    /*! @brief Fen�tre 3D (Qt).
        @version 0.71

        Viewport 3D Qt.
        @todo Dans les classes d�riv�es, ajouter, comme attribut, une instance d'une classe d�riv�e de
        RDR::Renderer (� initialiser avec une instance d'un objet drag'n'drop�e (ou le monde))
        (ah bon ?)
     */
    class Widget : public QGLWidget
    {
        Q_OBJECT
    public:
        //! @name Types de pointeur
        //@{
        using CameraPtr     = std::weak_ptr<CAM::Camera>;                               //!< Pointeur sur cam�ra.
        using ModePtr       = std::shared_ptr<Mode>;                                    //!< Pointeur sur mode.
        using PickablePtr   = APP::Pickable::Ptr;                                       //!< Pointeur sur objet pickable.
        using RendererPtr   = std::weak_ptr<RDR::Renderer>;                             //!< Pointeur sur Renderer.
        using RenderTreePtr = std::shared_ptr<RDR::Tree>;                               //!< Pointeur sur arbre de rendu.
        using ViewportPtr   = std::shared_ptr<GXV::Viewport>;                           //!< Pointeur sur viewport.
        //@}
        //! @name Constructeur & destructeur
        //@{
                                Widget(OSGi::Context* pContext,
                                       QWidget*       pParent = nullptr);               //!< Constructeur (par d�faut).
virtual                         ~Widget();                                              //!< Destructeur.
        //@}
        //! @name Syst�me de rendu
        //@{
        void                    attach(RendererPtr pRenderer);                          //!< Attachement d'un objet de rendu. 
        void                    detach(RendererPtr pRenderer);                          //!< D�tachement d'un objet de rendu. 
        void                    enableRenderers(ERender pWhich);                        //!< Activation des renderers.
        CameraPtr               camera() const;                                         //!< Acc�s � la cam�ra.
        //@}
        //! @name Gestion des modes
        //@{
        template<class TMode>
        std::shared_ptr<TMode>  buildMode(int pType);                                   //!< Construction d'un mode.
        template<class TMode>
        std::shared_ptr<TMode>  buildMode(int pType, QString pName);                    //!< Construction d'un mode.
        void                    activateMode(int pType);                                //!< Activation manuelle d'un mode.
        //@}
        //! @name S�lection
        //@{
        MATH::V3                pickPoint(QPoint pOnScreen) const;                      //!< ... d'un point d'un objet du monde affich�.
        void                    picked(PickablePtr pEntity);                            //!< ... d'une entit�e (sous le curseur de la souris).
        //@}
        void                    cleanUp();                                              //!< Nettoyage.

    protected slots:

virtual void                    updateOnUndoRedo();                                     //!< Mise � jour suite � un undo ou un redo.


    protected:
        //! @name Gestion OpenGL par Qt
        //@{
virtual void                    initializeGL() override;                                //!< Initialisation Qt.
virtual void                    paintGL() override;                                     //!< Rafra�chissement Qt.
virtual void                    resizeGL(int pWidth, int pHeight) override;             //!< Redimensionnement Qt.
        //@}
        //! @name Gestion de la souris & du clavier (pour �dition)
        //@{
virtual void                    mousePressEvent(QMouseEvent* pEvent) override;          //!< Gestion des pressions sur les boutons de la souris.
virtual void                    mouseReleaseEvent(QMouseEvent* pEvent) override;        //!< Gestion des rel�chement des bouton de la souris.
virtual void                    mouseDoubleClickEvent(QMouseEvent* pEvent) override;    //!< Gestion des double-clics.
virtual void                    mouseMoveEvent(QMouseEvent* pEvent) override;           //!< Gestion des d�placements du curseur de la souris.
virtual void                    paintEvent(QPaintEvent* pEvent) override;               //!< Gestion du rafra�chissement du widget.
virtual void                    wheelEvent(QWheelEvent* pEvent) override;               //!< Gestion des roulements ou clics de la molette de la souris.
virtual void                    keyPressEvent(QKeyEvent* pEvent) override;              //!< Gestion des pressions sur les touches du clavier.
virtual void                    keyReleaseEvent(QKeyEvent* pEvent) override;            //!< Gestion des rel�chement des touches du clavier.
        //@}
        OSGi::Context*          context() const;                                        //!< Contexte d'ex�cution.
        PickablePtr             pick(const QPoint& pPoint);                             //!< Picking d'un objet.
virtual void                    onCleanUp() = 0;                                        //!< Nettoyage.


    private:

virtual void                    createViewport() = 0;                                   //!< Cr�ation d'un viewport.
virtual ViewportPtr             viewport() const = 0;                                   //!< Viewport utilis�.
        void                    registerMode(ModePtr pCustomMode, int pType);           //!< Enregistrement d'un mode.

        Q_CUSTOM_DECLARE_PRIVATE(Widget)
    };


    /*! @brief Fen�tre 3D pour la visualisation de donn�es (Qt).
        @version 0.3
     */
    class Viewer3D : public Widget
    {
        Q_OBJECT
    public:
        //! @name Type de pointeur
        //@{
        using AppearancePtr = std::shared_ptr<APP::Appearance>; //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Viewer3D(OSGi::Context* pContext,
                             QWidget*       pParent = nullptr); //!< Constructeur.
virtual             ~Viewer3D();                                //!< Destructeur.
        //@}
        void        display(AppearancePtr pAppearance);         //!< 

    public slots:
        //! @name Configuration
        //@{
        void        animate(bool pEnabled);                     //!< 
        void        light(bool pEnabled);                       //!< 
        //@}

    protected:

virtual void        onCleanUp() override;                       //!< Nettoyage.

    private slots:

        void        refreshView();                              //!< Rafra�chissement cadenc�.


    private:

virtual void        createViewport() override;                  //!< Cr�ation du viewport.
virtual ViewportPtr viewport() const override;                  //!< Viewport OpenGL.

        Q_CUSTOM_DECLARE_PRIVATE(Viewer3D)
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Widget.inl"

#endif  // De QT3D_WIDGET_HH
