/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_ERRMSG_HH
#define QT3D_ERRMSG_HH

/*! @file IDE/Module/QT3D/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module QT3D.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace QT3D
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kAlreadyRegisteredMode;
    extern  ERR::String kCameraModeShouldntBeActiveAnyLonger;
    extern  ERR::String kEmptySelection;
    extern  ERR::String kInvalidEntityName;
    extern  ERR::String kInvalidMode;
    extern  ERR::String kInvalidModeName;
    extern  ERR::String kInvalidSelection;
    extern  ERR::String kInvalidState;
    extern  ERR::String kModeMustBeBoundToValidWindow;
    extern  ERR::String kUndefinedController;
    extern  ERR::String kUnknownMode;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De QT3D_ERRMSG_HH
