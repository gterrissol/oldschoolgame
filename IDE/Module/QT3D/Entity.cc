/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Entity.hh"

/*! @file IDE/Module/QT3D/Entity.cc
    @brief M�thodes (non-inline) de la classe QT3D::Entity.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2008 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

//---------------------------------------------------------------------------
//                                  En-t�tes
//---------------------------------------------------------------------------

#include "APPearance/Primitive.hh"
#include "CAMera/BaseCamera.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/V3.hh"
#include "OBJect/GameObject.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Shared.tcc"

#include "Const.hh"
#include "ErrMsg.hh"
#include "Widget.hh"

namespace QT3D
{
    /*!
     */
    class Entity::Private
    {
    public:

        Private(QString pName, AppearancePtr pNormal, AppearancePtr pHighlighted)
            : mShape(pNormal)
            , mHighlight(pHighlighted)
            , mTransform()
            , mName(pName)
            , mIsHighlighted(false)
            , mIsVisible(true)
        { }

        AppearancePtr   mShape;
        AppearancePtr   mHighlight;
        MATH::M4        mTransform;
        QString         mName;
        bool            mIsHighlighted;
        bool            mIsVisible;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Entity::Entity(QString pName, AppearancePtr pNormal, AppearancePtr pHighlighted)
        : pthis(pName, pNormal, pHighlighted)
    {
        pthis->mShape->bind(shared_from_this());
        pthis->mHighlight->bind(shared_from_this());
        pthis->mTransform.loadIdentity();
    }


    /*!
     */
    Entity::~Entity() { }


    /*!
     */
    U32 Entity::getType() const
    {
        return U32(2);
    }


    /*!
     */
    void Entity::bindTo(PickablePtr pObject)
    {
        pthis->mShape->bind(pObject->shared_from_this());
        pthis->mHighlight->bind(pObject->shared_from_this());
    }


    /*!
     */
    void Entity::getLit(APP::Appearance::LightPtr pLight)
    {
        pthis->mShape->getLit(pLight);
        pthis->mHighlight->getLit(pLight);
    }

    /*!
     */
    QString Entity::name() const
    {
        return pthis->mName;
    }

    /*!
     */
    void Entity::highlight(bool pOn)
    {
        pthis->mIsHighlighted = pOn;
    }


    /*!
     */
    bool Entity::isHighlighted() const
    {
        return pthis->mIsHighlighted;
    }


    /*!
     */
    void Entity::setVisible(bool pOn)
    {
        pthis->mIsVisible = pOn;
    }


    /*!
     */
    bool Entity::isVisible() const
    {
        return pthis->mIsVisible;
    }

    /*!
     */
    Entity::ConstAppearancePtr Entity::appearance() const
    {
        if (pthis->mIsVisible)
        {
            return ConstAppearancePtr{isHighlighted() ? pthis->mHighlight : pthis->mShape};
        }
        else
        {
            return {};
        }
    }


    /*!
     */
    void Entity::setTransform(MATH::M4 pTransform, bool pVisible)
    {
        pthis->mTransform  = pTransform;
        pthis->mIsVisible  = pVisible;
    }


    /*!
     */
    MATH::M4 Entity::transform() const
    {
        return pthis->mTransform;
    }
}
