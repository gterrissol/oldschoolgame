/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_ALL_HH
#define QT3D_ALL_HH

/*! @file IDE/Module/QT3D/All.hh
    @brief Interface publique du module @ref QT3D.
    @author @ref Guillaume_Terrissol
    @date 3 Mars 2003 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT3D  //! Editeurs 3D.
{
    /*! @namespace QT3D
        @version 0.5

     */

    /*! @defgroup QT3D QT3D : Fen�tre de visualisation 3D et m�taphores de visualisation et d'�dition
        <b>namespace</b> QT3D.
     */
}

#include "Mode.hh"
#include "GLWidget.hh"

#endif  // De QT3D_QT3D_HH
