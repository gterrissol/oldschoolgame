/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/Module/QT3D/Controller.cc
    @brief M�thodes (non-inline) de la classe QT3D::Controller.
    @author @ref Guillaume_Terrissol
    @date 21 Juillet 2014 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT3D
{
//------------------------------------------------------------------------------
//                            Controller : Interface
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Controller::~Controller() { }


    /*!
     */
    auto Controller::renderer() -> RendererPtr
    {
        return getRenderer();
    }
}
