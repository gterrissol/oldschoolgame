/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QT3D.hh"

/*! @file IDE/Module/QT3D/QT3D.cc
    @brief D�finitions diverses du module QT3D.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Activator.hh"
#include "CORE/UIService.hh"
#include "ERRor/ErrMsg.hh"

#include "GameEntityService.hh"
#include "GameView.hh"

namespace QT3D
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlreadyRegisteredMode,                 "Mode d�j� enregistr�.",                            "Already registered mode.")
    REGISTER_ERR_MSG(kCameraModeShouldntBeActiveAnyLonger,   "Le mode cam�ra ne devrait plus �tre actif.",       "Camera mode shouldn't be active any longer.")
    REGISTER_ERR_MSG(kEmptySelection,                        "S�lection vide.",                                  "Empty selection.")
    REGISTER_ERR_MSG(kInvalidEntityName,                     "Nom d'entit� invalide.",                           "Invalid entity name.")
    REGISTER_ERR_MSG(kInvalidMode,                           "Mode invalide.",                                   "Invalid mode.")
    REGISTER_ERR_MSG(kInvalidModeName,                       "Nom de mode invalide.",                            "Invalid mode name.")
    REGISTER_ERR_MSG(kInvalidSelection,                      "S�lection invalide.",                              "Invalid selection.")
    REGISTER_ERR_MSG(kInvalidState,                          "Etat invalide.",                                   "Invalid state.")
    REGISTER_ERR_MSG(kModeMustBeBoundToValidWindow,          "Un mode doit �tre associ� � une fen�tre valide.",  "A mode must be bound to a valid window.")
    REGISTER_ERR_MSG(kUndefinedController,                   "Contr�leur non-d�fini.",                           "Undefined controller.")
    REGISTER_ERR_MSG(kUnknownMode,                           "Mode inexistant.",                                 "Unknown mode.")


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }


        void    checkInServices(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkIn("fr.osg.ide.module.3d.game_entity", std::make_shared<GameEntityService>(pContext));
                }
        void    checkOutServices(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkOut("fr.osg.ide.module.3d.game_entity");
                }

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("module.3D", [=]() { return std::make_unique<View>(pContext); });
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("module.3D");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QT3D::Activator)
OSGI_END_REGISTER_ACTIVATORS()
