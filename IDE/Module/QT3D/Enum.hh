/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT3D_ENUM_HH
#define QT3D_ENUM_HH

/*! @file IDE/Module/QT3D/Enum.hh
    @brief Enum�rations du module QT3D.
    @author @ref Guillaume_Terrissol
    @date 27 Avril 2003 - 22 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT3D
{
    //! Etat des boutons de la souris.
    enum EState
    {
        eNone       = 0x00000000,

        ePress      = 0x00000001,
        eRelease    = 0x00000010,
        eState      = 0x00000011,

        eLeft       = 0x00001000,
        eMiddle     = 0x00010000,
        eRight      = 0x00100000,
        eExtra1     = 0x01000000,
        eExtra2     = 0x10000000,
        eButton     = 0x11111000
    };

    enum ERender
    {
        eEngine,
        eEditor
    };
}

#endif // De QT3D_ENUM_HH
