/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/Module/QUNDerground/Controller.cc
    @brief M�thodes (non-inline) de la classe QUND::Controller.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "QWORld/GenericBrush.tcc"
#include "STL/Shared.tcc"
#include "TILE/HeightField.tcc"

namespace UND
{
//------------------------------------------------------------------------------
//                             P-Impl de Controller
//------------------------------------------------------------------------------

    /*! @brief P-Impl de OUT::Controller.
        @version 0.5
     */
    class Controller::Private :
        public QT::QSubject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Private(Controller* pParent);                     //!< Constructeur.
                ~Private();                                        //!< Destructeur.
        //@}
        std::shared_ptr<Controller>    mParent;                    //!< Contr�leur propri�taire.
        QWOR::Data::Ptr               mWorldData;                 //!< Donn�es de monde.

    private:

virtual void sendAnswer(QT::QAnswer* pAnswer, QT::QRequest* pUndo); //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                Controller P-Impl : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Monde ext�rieur �ditable dont cette instance est l'"impl�mentation priv�e"
     */
    Controller::Private::Private(Controller* pParent)
        : QT::QSubject()
        , mParent(pParent, STL::null_delete())
        , mWorldData()
    { }


    /*! Destructeur.
     */
    Controller::Private::~Private()
    {
    }


//------------------------------------------------------------------------------
//                         Controller P-Impl : 
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::Private::sendAnswer(QT::QAnswer* pAnswer, QT::QRequest* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                   Controller : Constructeurs & destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::Controller(TEditedPtr pWorld)
        : DAT::GenericController<World>(pWorld)
        , pthis(this)
    {
        pWorld.lock()->activateSaveSelf(true);

        pthis->mWorldData = QWOR::Data::get(pWorld.lock()->commonData());
    }


    /*! Destructeur.
     */
    Controller::~Controller() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Controller::ControllerPtr Controller::make(EntityPtr pEntity)
    {
        return std::make_shared<Controller>(std::dynamic_pointer_cast<World>(pEntity.lock()));
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QWOR::Data::Ptr Controller::data()
    {
        return pthis->mWorldData;
    }


//------------------------------------------------------------------------------
//                   Controller : Acc�s aux donn�es (lecture)
//------------------------------------------------------------------------------

    /*! Permet de r�cup�rer des �l�vations. 
        @param pHeights Les �l�ments r�f�rencent les �l�vations � r�cup�rer : ils seront remplis avec les �l�vations correspondantes
     */
    void Controller::get(QWOR::HeightGroup& /*pHeights*/) const
    {
    }


    /*! Permet de r�cup�rer des texels.
        @param pTexels Les �l�ments r�f�rencent les texels � r�cup�rer : ils seront remplis avec les texels correspondants
     */
    void Controller::get(QWOR::TexelGroup& /*pTexels*/) const
    {
    }


//------------------------------------------------------------------------------
//                              Controller : Rendu
//------------------------------------------------------------------------------

    /*! Recense les apparences � envoyer au moteur de rendu.
        @param pVisibles  Ce vecteur sera rempli avec les apparences � afficher
        @param pVisibleArea Zone de visibilit� actuelle (seules les apparences � l'int�rieur seront ins�r�es dans <i>pVisibles</i>)
     */
    void Controller::getAppearances(std::vector<AppearancePtr>& /*pVisibles*/, const UTI::Rect& /*pVisibleArea*/) const
    {
    }


//------------------------------------------------------------------------------
//                   Controller : Acc�s aux donn�es (�criture)
//------------------------------------------------------------------------------

    /*! Permet de modifier les �l�vations.
        @param pHeights Groupe d'�l�vations (et leur position) � mettre � jour dans le terrain
     */
    void Controller::set(const QWOR::HeightGroup& /*pHeights*/)
    {
    }



    /*! Permet de modifier les texels.
        @param pTexels Groupe de texels (et leur position) � mettre � jour dans la texture (environnement) du terrain
     */
    void Controller::set(const QWOR::TexelGroup& /*pTexels*/)
    {
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    UTI::Rect Controller::getWorldExtent() const
    {
        return UTI::Rect();
    }


    /*!
     */
    UTI::Rect Controller::getTextureExtent() const
    {
        return UTI::Rect();
    }


    /*!
     */
    int Controller::getType() const
    {
        return rTTI();
    }


    /*!
     */
    Controller::CliffBrushPtr Controller::getCliffBrush()
    {
        CliffBrushPtr  lBrush  = std::make_shared<QWOR::GenericCliffBrush<QWOR::HeightGroup, Controller>>(pthis->mParent);

        return lBrush;
    }


    /*!
     */
    Controller::SlideBrushPtr Controller::getSlideBrush()
    {
        SlideBrushPtr  lBrush  = std::make_shared<QWOR::GenericSlideBrush<QWOR::HeightGroup, Controller>>(pthis->mParent);

        return lBrush;
    }


    /*!
     */
    Controller::EnvironmentBrushPtr Controller::getEnvironmentBrush()
    {
        EnvironmentBrushPtr    lBrush  = std::make_shared<QWOR::GenericEnvironmentBrush<QWOR::TexelGroup, Controller>>(pthis->mParent);

        return lBrush;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QT::QSubject* Controller::asSubject()
    {
        return nullptr;//&(*pthis);
    }


    /*!
     */
    QString Controller::getExtension() const
    {
        return "und";
    }


    /*!
     */
    void Controller::doTakeOver()
    {
    }


    /*!
     */
    void Controller::doSave() const
    {
    }


    /*!
     */
    Controller::RendererPtr Controller::getRenderer()
    {
        return Controller::RendererPtr();
    }
}
