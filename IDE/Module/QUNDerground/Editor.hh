/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QUND_EDITOR_HH
#define QUND_EDITOR_HH

#include "QUNDerground.hh"

/*! @file IDE/Module/QUNDerground/Editor.hh
    @brief En-t�te de la classe QUND::Editor.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "UNDerground/UNDerground.hh"
#include "QPaNeL/Editor.hh"
#include "QT/Observer.hh"

namespace QUND
{
    /*! @brief Editeur de monde sous-terrain.
        @version 0.3

        Interface Qt d'�dition de monde sous-terrain.
     */
    class Editor : public QPNL::QGenericEditor<UND::World>, public QT::QObserver
    {
        Q_OBJECT
    public :
        //! @name Constructeur & destructeur
        //@{
                    Editor();                                  //!< Constructeur par d�faut.
static  QTAB::QTab* builder();                                  //!< Cr�ation.
virtual             ~Editor();                                 //!< Destructeur.
        //@}

    protected:
        //! @name Changement de langue
        //@{
virtual void        onLanguageChange();                         //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private:
        //! @name Comportement de page
        //@{
virtual void        manageMenuEntry(QAction* entry);            //!< Gestion d'une entr�e du menu du widget.
virtual void        fillPageMenu(QMenu*& pageMenu) const;       //!< Cr�ation du menu du widget.
virtual QIcon       statusIcon() const;                         //!< Ic�ne repr�sentant l'�tat actuel du widget.
        //@}
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(ControllerPtr controller) const; //!< Possibilit� de gestion d'un sujet.
virtual void        manage(ControllerPtr controller);          //!< Gestion effective d'un sujet.
        //@}
        //! @name Ecoute du sujet
        //@{
virtual void        listen(const QT::QAnswer* answer = 0);      //!< Ecoute effective de la cible.
        //@}
        Q_DISABLE_COPY(Editor)
        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}

#endif  // De QUND_EDITOR_HH
