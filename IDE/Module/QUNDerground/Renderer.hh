/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QUND_RENDERER_HH
#define QUND_RENDERER_HH

#include "QUNDerground.hh"

/*! @file IDE/Module/QUNDerground/Renderer.hh
    @brief En-t�te de la classe QUND::Renderer.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATum/Renderer.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "UNDerground/UNDerground.hh"
#include "UTIlity/Rect.hh"

namespace QUND
{
    /*! @brief Fragment d'ext�rieur.
        @version 0.6

        L'apparence d'un monde sous-terrain se compose d'une seule g�om�trie, existant pour diff�rents
        niveaux de d�tail, qui sont des instances de cette classe.
        @note Pas de normales, pas de texture
     */
    class Geometry : public GEO::Geometry
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                                    Geometry(const UTI::Rect& pArea,
                                              unsigned long pLevel);            //!< Constructeur.
virtual                             ~Geometry();                               //!< Destructeur
        //@}
        //! @name Informations
        //@{
        inline  const UTI::Rect&   area() const;                               //!< Zone couverte par la g�om�trie.
        inline  unsigned long       detailLevel() const;                        //!< Niveau de d�tail de la g�om�trie.
        //@}
        //! @name D�finition des donn�es
        //@{
                void                setElevation(unsigned short pLi,
                                                 unsigned short pCo,
                                                 float          pElevation);    //!< Edition d'une �l�vation.


    private:

        inline  void                pushVertex3(GEO::Vertex3 pVertex);         //!< Empilement d'un vertex.
        //@}
        //! @name Informations
        //@{
        UTI::Rect      mArea;                                                  //!< Zone couverte par la g�om�trie.
        unsigned long   mLevel;                                                 //!< Niveau de d�tail associ� � la g�om�trie.
        //@}
    };


    /*! @brief Affichage de monde sous-terrain (�ditable).
        @version 0.25

        Pour le rendu de monde sous-terrain (dans l'�diteur).
     */
    class Renderer : public DAT::Renderer
    {
    public :
        //! @name Type de pointeur
        //@{
        typedef std::weak_ptr<UND::World>      WorldPtr;          //!< Pointeur sur monde ext�rieur.
        typedef std::weak_ptr<UND::Controller> ControllerPtr;     //!< Pointeur sur contr�leur de monde ext�rieur.
        typedef std::weak_ptr<APP::Appearance> AppearancePtr;     //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Renderer(WorldPtr      pUnderworld,
                          ControllerPtr pController);              //!< Constructeur.
virtual         ~Renderer();                                       //!< Destructeur.
        //@}

    private:

virtual void    created(EntityPtr pChild, RendererPtr pRenderer); //!< Slot sur cr�ation du renderer d'une sous-entit�.
        //! @name Rendu
        //@{
virtual void    dumpAppearances();                                  //!< Pr�paration du rendu.
        //@}
    };
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Renderer.inl"

#endif  // De QUND_RENDERER_HH
