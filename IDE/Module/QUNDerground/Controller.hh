/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QUND_CONTROLLER_HH
#define QUND_CONTROLLER_HH

#include "QUNDerground.hh"

/*! @file IDE/Module/QUNDerground/Controller.hh
    @brief En-t�te de la classe QUND::Controller.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATum/Controller.hh"
#include "QWORld/Brush.hh"
#include "QWORld/Data.hh"
#include "STL/Shared.hh"
#include "UNDerground/World.hh"

namespace UND
{
    /*! @brief Contr�leur de monde sous-terrain
        @version 0.2

        
     */
    class Controller :
        public STL::enable_shared_from_this<Controller>,
        public DAT::GenericController<World>,
        public QWOR::Brushable
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef std::weak_ptr<APP::Appearance const>   ConstAppearancePtr;            //!< Pointeur (constant) sur apparence.
        typedef std::weak_ptr<APP::Appearance>         AppearancePtr;                 //!< Pointeur sur apparence.
        typedef DAT::GameEntityMgr::ControllerPtr     ControllerPtr;                 //!< Pointeur sur contr�leur.
        typedef DAT::GameEntityMgr::EntityPtr         EntityPtr;                     //!< Pointeur sur entit�.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                                Controller(TEditedPtr pWorld);                         //!< Constructeur.
virtual                         ~Controller();                                         //!< Destructeur.
 static ControllerPtr          make(EntityPtr pEntity);                               //!< Cr�ation.
        //@}
        QWOR::Data::Ptr   data();                                                 //!< Donn�es communes.
        //! @name Acc�s aux donn�es (lecture)
        //@{
        void                    get(QWOR::HeightGroup& pHeights) const;                //!< Acc�s aux �l�vations.
        void                    get(QWOR::TexelGroup&  pTexels) const;                 //!< Acc�s aux texels.
        //@}
        //! @name Rendu
        //@{
        void                    getAppearances(std::vector<AppearancePtr>& pVisibles,
                                               const UTI::Rect& pVisibleArea) const;   //!< R�cup�ration des apparences.
        //@}
        //! @name Acc�s aux donn�es (�criture)
        //@{
        void                    set(const QWOR::HeightGroup& pHeights);                //!< Edition des �l�vations
        void                    set(const QWOR::TexelGroup&  pTexels);                 //!< Edition des texels.
        //@}
        //! @name Impl�mentation de Brushable
        //@{
virtual UTI::Rect              getWorldExtent() const;                                 //!< 
virtual UTI::Rect              getTextureExtent() const;                               //!< 
virtual int                     getType() const;                                        //!< 
virtual CliffBrushPtr          getCliffBrush();                                        //!< 
virtual SlideBrushPtr          getSlideBrush();                                        //!< 
virtual EnvironmentBrushPtr    getEnvironmentBrush();                                  //!< 
        //@}
virtual QT::QSubject*           asSubject();                                            //!< R�cup�ration du sujet d'�dition.
virtual QString                 getExtension() const;                                   //!< Extension associ�e � l'objet contr�l�
virtual void                    doTakeOver();                                           //!< Reprise en main du contr�leur
virtual void                    doSave() const;                                         //!< Sauvegarde de l'�tat de l'objet contr�l�.
virtual RendererPtr            getRenderer();                                          //!< R�cup�ration du rendrerer du sujet �dit�.
        FORBID_COPY(Controller)
        FREE_PIMPL()
    };
}

#endif  // De QUND_CONTROLLER_HH
