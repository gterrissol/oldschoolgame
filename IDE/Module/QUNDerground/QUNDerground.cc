    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QUNDerground.hh"

/*! @file IDE/Module/QUNDerground/QUNDerground.cc
    @brief D�finitions diverses du module @ref QUNDerground.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "Module/QWORld/ImportExport.hh"
#include "Module/QWORld/WorldDataService.hh"
#include "PAcKage/LoaderSaver.hh"
#include "UTIlity/Rect.hh"
#include "WORld/Enum.hh"

namespace QUND
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kIncorrectDimensions,   "Dimensions incorrectes.",  "Incorrect dimensions.")


//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de monde sous-terrain.
        @version 0.25
     */
    class ImportExportUnderworld : public QWOR::ImportExport
    {
    public:

virtual         ~ImportExportUnderworld();                                          //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


//------------------------------------------------------------------------------
//                               Monde Souterrain
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportUnderworld::~ImportExportUnderworld() { }


    /*!
     */
    bool ImportExportUnderworld::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        exportHeader(pWriter, pLoader);

        data()->write<eRect>(pWriter, pLoader, "Zone");

        return true;
    }


    /*!
     */
    void ImportExportUnderworld::doMake(PAK::Saver& pSaver)
    {
        auto    lService    = data()->fileDB()->context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data");
        auto    lGroup      = lService->groupFilenameFromWorld(data()->fileDB()->packFileName(pSaver.fileHdl()));

        pSaver << data()->fileDB()->packFileHdl(lGroup);    // Handle des donn�es g�n�rales.
        pSaver << I32{WOR::eUnderground};   // Type de monde.

        pSaver << UTI::Rect{};              // Zone couverte.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportUnderworld>("und",  lExtensionDir);
                    pContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->checkIn("und", WOR::eUnderground);

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->registerType(QStringLiteral("und"));
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie")->checkOut("und");
                    pContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->checkOut("und");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QUND::Activator)
OSGI_END_REGISTER_ACTIVATORS()
