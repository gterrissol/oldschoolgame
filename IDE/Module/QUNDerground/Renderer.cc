/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Renderer.hh"

/*! @file IDE/Module/QUNDerground/Renderer.cc
    @brief M�thodes (non-inline) de la classe QUND::Renderer.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>

#include "GEOmetry/List.tcc"
#include "QOUTside/Enum.hh"

#include "Controller.hh"
#include "ErrMsg.hh"

namespace QUND
{
//------------------------------------------------------------------------------
//                 Outside Geometry : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pArea  Zone couverte par la g�om�trie
        @param pLevel Niveau de d�tail; correspond � l'it�ration � laquelle cette instance a �t�
        construite
     */
    Geometry::Geometry(const UTI::Rect& pArea, unsigned long pLevel)
        : mArea(pArea)
        , mLevel(pLevel)
    {
        ASSERT((area().roundedWidth()  == static_cast<unsigned long>((QOUT::eGeometryDefaultLength << mLevel) + 1)) &&
                 (area().roundedHeight() == static_cast<unsigned long>((QOUT::eGeometryDefaultLength << mLevel) + 1)),
                 kIncorrectDimensions)
        setVertexCount(U16((QOUT::eGeometryDefaultLength + 1) * (QOUT::eGeometryDefaultLength + 1)));

        setFlags(I32(GEO::eVertex3 | GEO::eLines));
        setAutoFlags(I32(GEO::eVertex3 | GEO::eLines));

        float           lTop  = area().top()  * QOUT::kOutsideScale;
        float           lLeft = area().left() * QOUT::kOutsideScale;

        /*float           lInvWidth  = k1F / QOUT::eGeometryDefaultLength;
        float           lInvHeight = k1F / QOUT::eGeometryDefaultLength;*/

        unsigned long   lStep = static_cast<unsigned long>(QOUT::kOutsideScale * (0x00000001 << detailLevel()));

        // Cr�e les vertex.
        //F32 lNormal[3] = { k0F, k0F, - k1F };
        for(unsigned short lLi = 0; lLi <= QOUT::eGeometryDefaultLength; ++lLi)
        {
            for(unsigned short lCo = 0; lCo <= QOUT::eGeometryDefaultLength; ++lCo)
            {
                F32 lVertex[3] = { F32(lLeft + lCo * lStep), F32(lTop  - lLi * lStep), F32((rand() % 1024) / 64.0F) };
                //k0F };
                pushVertex3(GEO::Vertex3(lVertex));

                /*pushNormal(GEO::Normal(lNormal));

                F32 lTexCoords[2] = { F32(lCo * lInvWidth), F32(lLi * lInvHeight) };
                pushTexCoord(GEO::TexCoord1(lTexCoords));*/
            }
        }

        // Cr�� la liste de triangles, puis la lie � la base.
        TriangleListPtr    lCustomTriangleList = STL::makeShared<TriangleListPtr::element_type>(U16(2 * QOUT::eGeometryDefaultLength * QOUT::eGeometryDefaultLength));
        // D�finition simple : liste de "bandes" de triangles.
        for(unsigned long lLi = 0; lLi < QOUT::eGeometryDefaultLength; ++lLi)
        {
            unsigned long   lUp     = lLi * (QOUT::eGeometryDefaultLength + 1);
            unsigned long   lDown   = lUp + (QOUT::eGeometryDefaultLength + 1);

            for(unsigned long lCo = 0; lCo < QOUT::eGeometryDefaultLength; ++lCo)
            {
                lCustomTriangleList->push(GEO::Triangle(U16(lUp + lCo),     U16(lDown + lCo), U16(lUp   + lCo + 1)));
                lCustomTriangleList->push(GEO::Triangle(U16(lUp + lCo + 1), U16(lDown + lCo), U16(lDown + lCo + 1)));
            }
        }

        cloneTriangleList(std::const_pointer_cast<TriangleListPtr::element_type const>(lCustomTriangleList));
    }


    /*! Destructeur
     */
    Geometry::~Geometry() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pUnderworld Monde � rendre.
        @param pController Contr�leur du monde.
     */
    Renderer::Renderer(WorldPtr pUnderworld, ControllerPtr /*pController*/)
        : DAT::Renderer(pUnderworld)
    { }
/*        , mRenderers()
        , mGrounds()
        , mOutside(pOutside)
        , mController(pController)
        , mSun(new LIT::DirectionalLight())
    {
        mSun->setDirection(MATH::V4(+k1F, +k1F, +k1F, k0F).normalized());
        mSun->setAmbient(CLR::RGBA(F32(0.2F), F32(0.2F), F32(0.2F), k1F));
        mSun->setDiffuse(CLR::RGBA(F32(0.8F), F32(0.8F), F32(0.8F), k1F));
        mSun->setSpecular(CLR::RGBA(k0F, k0F, k0F, k1F));
    }*/


    /*! Destructeur.
     */
    Renderer::~Renderer() { }


    /*!
     */
    void Renderer::created(EntityPtr /*pChild*/, RendererPtr /*pRenderer*/)
    {
        // Gestion des tuiles...
    }


    /*!
     */
    void Renderer::dumpAppearances()
    {
    }
}
