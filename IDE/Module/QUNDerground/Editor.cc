/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Module/QUNDerground/Editor.cc
    @brief M�thodes (non-inline) de la classe QUND::Editor.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "QT/Application.hh"
#include "QT/MainWindow.hh"
//#include "UI/UnderworldEditor.hh"

#include "Controller.hh"

SPECIALIZE_EDITOR(UND::World, QT_TR_NOOP("Monde sous-terrain"), QT_TR_NOOP("Editeur de monde sous-terrain"), ":/types/cave")

namespace QUND
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Editor.
        @version 0.3
     */
    class Editor::EditorPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
        //! @name Constructeurs
        //@{
                        EditorPrivate(Editor* pParent);                   //!< Constructeur.
 static QTAB::QPage*    create();                                           //!< Cr�ation d'un onglet.
        void            init();                                             //!< Initialisation.
        //@}
        void            updateUI();                                         //!< Mise � jour de l'interface.
        //! @name Composants de l'interface
        //@{
//        Ui::UnderworldEditor    mUI;                                        //!< Interface cr��e par Designer.
        //@}
        typedef std::shared_ptr<DAT::Controller>   ControllerPtr;         //!< 
        typedef std::weak_ptr<UND::Controller>     WorldControllerPtr;    //!< 
        ControllerPtr          mController;                                //!< 
        WorldControllerPtr     mWorld;                                     //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent)
        : q_custom_ptr(pParent)
//        , mUI()
        , mController()
        , mWorld()
    { }


    /*!
     */
    QTAB::QPage* Editor::EditorPrivate::create()
    {
        return new Editor();
    }


    /*!
     */
    void Editor::EditorPrivate::init()
    {
        Q_Q(Editor);

        // Construit l'interface.
//        mUI.setupUi(q);

        // ...

        QT::QMainWindow::synchronize(q, "file + enabled + edition + und;");
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::EditorPrivate::updateUI()
    {
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor()
        : QPNL::QGenericEditor<UND::World>()
        , QT::QObserver(nullptr)
        , d_custom_ptr(new EditorPrivate(this))
    {
        Q_D(Editor);

        d->init();
    }


    /*!
     */
    QTAB::QTab* Editor::builder()
    {
        return new QTAB::QTab(&Editor::name, &Editor::title, QIcon(":/types/cave"), &EditorPrivate::create);
    }


    /*! Destructeur
     */
    Editor::~Editor() { }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    void Editor::onLanguageChange()
    {
//        Q_D(Editor);

//        d->mUI.retranslateUi(this);
    }


//------------------------------------------------------------------------------
//                             Comportement de page
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::manageMenuEntry(QAction* /*entry*/)
    {
        // Rien pour l'instant.
    }


    /*!
     */
    void Editor::fillPageMenu(QMenu*& /*pageMenu*/) const
    {
        // Rien pour l'instant.
    }


    /*!
     */
    QIcon Editor::statusIcon() const
    {
        return QIcon(":/types/cave");
    }


//------------------------------------------------------------------------------
//                              Pertinence du sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canManage(ControllerPtr controller) const
    {
        return (std::dynamic_pointer_cast<EditorPrivate::WorldControllerPtr::element_type>(controller) != nullptr);
    }


    /*!
     */
    void Editor::manage(ControllerPtr controller)
    {
        Q_D(Editor);

        // controller a �t� contr�l� en amont.
        // NB : il ne faut guarder qu'une seul r�f�rence forte (et g�n�rique)
        // sur le contr�leur (pour DAT::Contorler::subject(...)).
        d->mController  = controller;
        d->mWorld       = std::dynamic_pointer_cast<EditorPrivate::WorldControllerPtr::element_type>(controller);

        QT::QSubject*   lWorld  = DAT::Controller::subject(d->mController);

        watch(lWorld);

        // Mise � jour de l'interface en fonction du nouveau datum.
        d->updateUI();
    }


//------------------------------------------------------------------------------
//                                Ecoute du sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::listen(const QT::QAnswer* /*answer*/)
    {
        Q_D(Editor);

        // Met simplement � jour l'interface.
        d->updateUI();

        // Et la vue 3D.
        workingWidget()->update();
    }
}
