/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/Module/QUNDerground/Renderer.inl
    @brief M�thodes inline de la classe QUND::cl_Renderer.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GEOmetry/Fields.hh"
#include "STL/Vector.hh"

namespace QUND
{
//------------------------------------------------------------------------------
//                        OutsideGeometry : Informations
//------------------------------------------------------------------------------

    /*! Permet de conna�tre la zone couverte par cette g�om�trie, par rapport au monde dont cette instance repr�sente une partie.
        @return La zone repr�sent�e par cette g�om�trie
     */
    inline const UTI::Rect& Geometry::area() const
    {
        return mArea;
    }


    /*! @return Le niveau de d�tail de cette g�om�trie
     */
    inline unsigned long Geometry::detailLevel() const
    {
        return mLevel;
    }


//------------------------------------------------------------------------------
//                   OutsideGeometry : D�finition des Donn�es
//------------------------------------------------------------------------------

    /*! Ajoute un vertex � la g�om�trie. Cette m�thode est juste l� pour encapsuler l'acc�s aux conteneurs
        de GEO::Geometry.
        @param pVertex Nouveau vertex
     */
    inline void Geometry::pushVertex3(GEO::Vertex3 pVertex)
    {
        vertices3().push_back(pVertex);
    }
}
