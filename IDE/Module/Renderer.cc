/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Renderer.hh"

/*! @file IDE/Module/Renderer.cc
    @brief M�thodes (non-inline) de la classe EDIT::Renderer.
    @author @ref Guillaume_Terrissol
    @date 16 Novembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <map>
#include <vector>

#include "OBJect/GameObject.hh"
#include "STL/Shared.tcc"

namespace EDIT
{
//------------------------------------------------------------------------------
//                             
//------------------------------------------------------------------------------

    /*!
     */
    class Renderer::Private
    {
    public:

        Private(EntityPtr pEdited, Renderer* pOwner)
            : mEdited(pEdited)
        {
            if (smMaster == nullptr)
            {
                smMaster = this;
            }
            EntityPtr::element_type*   lEdited = pEdited.lock().get();

            if (lEdited != nullptr)
            {
                std::map<EntityPtr::element_type*, std::vector<std::weak_ptr<Renderer>>>::iterator    lIter = smMaster->mConnections.find(lEdited);

                if (lIter != smMaster->mConnections.end())
                {
                    std::vector<std::weak_ptr<Renderer>>&  lTargets = lIter->second;

                    lTargets.erase(std::remove_if(lTargets.begin(),
                                                  lTargets.end(),
                                                  std::mem_fun_ref(&RendererPtr::expired)),
                                   lTargets.end());

                    for(size_t lT = 0; lT < lTargets.size(); ++lT)
                    {
                        lTargets[lT].lock()->created(mEdited, pOwner->shared_from_this());
                    }
                }
            }
        }

        ~Private()
        {
            if (smMaster == this)
            {
                smMaster = nullptr;
            }
        }

        EntityPtr              mEdited;
 static Renderer::Private*    smMaster;
        std::map<EntityPtr::element_type*, std::vector<std::weak_ptr<Renderer>>>  mConnections;
    };


    Renderer::Private*    Renderer::Private::smMaster = nullptr;


    /*!
     */
    
//------------------------------------------------------------------------------
//                             
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Renderer::Renderer(EntityPtr pEdited)
        : RDR::Renderer()
        , STL::enable_shared_from_this<Renderer>()
        , pthis(pEdited, this)
    {
        enable(true);
    }


    /*! Destructeur.
     */
    Renderer::~Renderer() { }


    /*! 
     */
    void Renderer::watchRendererOf(EntityPtr pChild)
    {
        EntityPtr::element_type*   lChild = pChild.lock().get();

        if (lChild != nullptr)
        {
            std::vector<std::weak_ptr<Renderer>>&  lTargets    = Private::smMaster->mConnections[lChild];
            std::weak_ptr<Renderer>                lThis       = shared_from_this();

            if (std::find(lTargets.begin(), lTargets.end(), lThis) == lTargets.end())
            {
                lTargets.push_back(lThis);
            }
        }
    }


    /*!
     */
    void Renderer::created(EntityPtr /*pChild*/, RendererPtr /*pRenderer*/)
    {
    }
}
