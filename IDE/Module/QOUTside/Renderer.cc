/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Renderer.hh"

/*! @file IDE/Module/QOUTside/Renderer.cc
    @brief M�thodes (non-inline) des classes 
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 16 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>
#include <cstdlib>

#include "APPearance/Primitive.hh"
#include "CAMera/BaseCamera.hh"
#include "GEOmetry/List.tcc"
#include "LIghT/BaseLight.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "Module/QT3D/Entity.hh"

#include "Controller.hh"
#include "ErrMsg.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//                 Outside Geometry : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pArea  Zone couverte par la g�om�trie
        @param pLevel Niveau de d�tail; correspond � l'it�ration � laquelle cette instance a �t�
        construite
     */
    Geometry::Geometry(const UTI::Rect& pArea, unsigned long pLevel)
        : mArea(pArea)
        , mCurrentVertex(MATH::kNullV3)
        , mLevel(pLevel)
    {
        ASSERT((area().roundedWidth()  == static_cast<unsigned long>((eGeometryDefaultLength << mLevel) + 1)) &&
                 (area().roundedHeight() == static_cast<unsigned long>((eGeometryDefaultLength << mLevel) + 1)),
                 kIncorrectDimensions)
        setVertexCount(U16((eGeometryDefaultLength + 1) * (eGeometryDefaultLength + 1)));

        setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eTexCoord0 | GEO::eTriangles));
        setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eTexCoord0 | GEO::eTriangles));

        float           lTop  = area().top()  * kOutsideScale;
        float           lLeft = area().left() * kOutsideScale;

        float           lInvWidth  = k1F / eGeometryDefaultLength;
        float           lInvHeight = k1F / eGeometryDefaultLength;

        unsigned long   lStep = static_cast<unsigned long>(kOutsideScale * (0x00000001 << detailLevel()));

        // Cr�e les vertex.
        F32 lNormal[3] = { k0F, k0F, - k1F };
        for(unsigned short lLi = 0; lLi <= eGeometryDefaultLength; ++lLi)
        {
            for(unsigned short lCo = 0; lCo <= eGeometryDefaultLength; ++lCo)
            {
                F32 lVertex[3] = { F32(lLeft + lCo * lStep), F32(lTop  - lLi * lStep), F32((rand() % 1024) / 64.0F) };
                //k0F };
                pushVertex3(GEO::Vertex3(lVertex));

                pushNormal(GEO::Normal(lNormal));

                F32 lTexCoords[2] = { F32(lCo * lInvWidth), F32(lLi * lInvHeight) };
                pushTexCoord(GEO::TexCoord0(lTexCoords));
            }
        }

        // Cr�� la liste de triangles, puis la lie � la base.
        TriangleListPtr    lCustomTriangleList = STL::makeShared<TriangleListPtr::element_type>(U16(2 * eGeometryDefaultLength * eGeometryDefaultLength));
        // D�finition simple : liste de "bandes" de triangles.
        for(unsigned long lLi = 0; lLi < eGeometryDefaultLength; ++lLi)
        {
            unsigned long   lUp     = lLi * (eGeometryDefaultLength + 1);
            unsigned long   lDown   = lUp + (eGeometryDefaultLength + 1);

            for(unsigned long lCo = 0; lCo < eGeometryDefaultLength; ++lCo)
            {
                lCustomTriangleList->push(GEO::Triangle(U16(lUp   + lCo),     U16(lDown + lCo),     U16(lDown + lCo + 1)));
                lCustomTriangleList->push(GEO::Triangle(U16(lDown + lCo + 1), U16(lUp   + lCo + 1), U16(lUp   + lCo)));
            }
        }

        cloneTriangleList(std::const_pointer_cast<TriangleListPtr::element_type const>(lCustomTriangleList));
    }


    /*! Destructeur
     */
    Geometry::~Geometry() { }


//------------------------------------------------------------------------------
//                        Outside Geometry : Mise � Jour
//------------------------------------------------------------------------------

    /*!
     */
    void Geometry::updateNormal(unsigned short pLi, unsigned short pCo)
    {
        unsigned long   lOffset = pLi * (eGeometryDefaultLength + 1) + pCo;

        GEO::Vertex3&  lVertex = vertices3()[lOffset];
        mCurrentVertex = MATH::V3(lVertex[0], lVertex[1], lVertex[2]);

        // Mise � jour de la normale.
        MATH::V3       lNormal(k0F, k0F, k1F);

        if ((0 < pLi) && (pLi < eGeometryDefaultLength) && (0 < pCo) && (pCo < eGeometryDefaultLength))
        {
            MATH::V3   lN  = computeN (pLi, pCo);
            MATH::V3   lW  = computeW (pLi, pCo);
            MATH::V3   lSW = computeSW(pLi, pCo);
            MATH::V3   lS  = computeS (pLi, pCo);
            MATH::V3   lE  = computeE (pLi, pCo);
            MATH::V3   lNE = computeNE(pLi, pCo);

            lNormal = ((lN ^ lW).normalized() + (lW ^ lSW).normalized() + (lSW ^ lS).normalized() +
                       (lS ^ lE).normalized() + (lE ^ lNE).normalized() + (lNE ^ lN).normalized());
        }
        // D'abord, les 4 coins.
        else if ((pLi == 0) && (pCo == 0))
        {
            MATH::V3   lS  = computeS(pLi, pCo);
            MATH::V3   lE  = computeE(pLi, pCo);

            // NW.
            lNormal = (lS  ^ lE);
        }
        else if ((pLi == 0) && (pCo == eGeometryDefaultLength))
        {
            MATH::V3   lW  = computeW (pLi, pCo);
            MATH::V3   lSW = computeSW(pLi, pCo);
            MATH::V3   lS  = computeS (pLi, pCo);

            // NE.
            lNormal = (lW ^ lSW).normalized() + (lSW ^ lS).normalized();
        }
        else if ((pLi == eGeometryDefaultLength) && (pCo == 0))
        {
            MATH::V3   lE  = computeE (pLi, pCo);
            MATH::V3   lNE = computeNE(pLi, pCo);
            MATH::V3   lN  = computeN (pLi, pCo);

            // SW.
            lNormal = (lE ^ lNE).normalized() + (lNE ^ lN).normalized();
        }
        else if ((pLi == eGeometryDefaultLength) && (pCo == eGeometryDefaultLength))
        {
            MATH::V3   lW  = computeW(pLi, pCo);
            MATH::V3   lN  = computeN(pLi, pCo);

            // SE.
            lNormal = (lN ^ lW).normalized();
        }
        // Puis les lignes.
        else if (pLi == 0)
        {
            MATH::V3   lW  = computeW (pLi, pCo);
            MATH::V3   lSW = computeSW(pLi, pCo);
            MATH::V3   lS  = computeS (pLi, pCo);
            MATH::V3   lE  = computeE (pLi, pCo);

            // Premi�re ligne.
            lNormal = (lW  ^ lSW).normalized() + (lSW  ^ lS).normalized() + (lS ^ lE).normalized();
        }
        else if (pLi == eGeometryDefaultLength)
        {
            MATH::V3   lE  = computeE (pLi, pCo);
            MATH::V3   lNE = computeNE(pLi, pCo);
            MATH::V3   lN  = computeN (pLi, pCo);
            MATH::V3   lW  = computeW (pLi, pCo);

            // Derni�re ligne.
            lNormal = (lE ^ lNE).normalized() + (lNE  ^ lN).normalized() + (lN  ^ lW).normalized();
        }
        // Et enfin, les colonnes.
        else if (pCo == 0)
        {
            MATH::V3   lS  = computeS (pLi, pCo);
            MATH::V3   lE  = computeE (pLi, pCo);
            MATH::V3   lNE = computeNE(pLi, pCo);
            MATH::V3   lN  = computeN (pLi, pCo);

            // Premi�re colonne.
            lNormal = (lS  ^ lE).normalized() + (lE ^ lNE).normalized() + (lNE  ^ lN).normalized();
        }
        else if (pCo == eGeometryDefaultLength)
        {
            MATH::V3   lN  = computeN (pLi, pCo);
            MATH::V3   lW  = computeW (pLi, pCo);
            MATH::V3   lSW = computeSW(pLi, pCo);
            MATH::V3   lS  = computeS (pLi, pCo);

            // Derni�re colonne.
            lNormal = (lN  ^ lW).normalized() + (lW ^ lSW).normalized() + (lSW  ^ lS).normalized();
        }

        if (lNormal.sqrNorm() != k0F)
        {
            lNormal.normalize();
        }
        else
        {
            lNormal = MATH::V3(k0F, k0F, k1F);
        }
        normals()[lOffset][0] = lNormal[0];
        normals()[lOffset][1] = lNormal[1];
        normals()[lOffset][2] = lNormal[2];
    }


//------------------------------------------------------------------------------
//                   Outside Geometry : D�finition des donn�es
//------------------------------------------------------------------------------

    /*! Modifie une �l�vation est met � jour les normales en cons�quence.
        @param pLi        Ligne (sur la g�om�trie) de l'�l�vation � modifier
        @param pCo        Colonne (sur la g�om�trie) de l'�l�vation � modifier
        @param pElevation Nouvelle �l�vation
     */
    void Geometry::setElevation(unsigned short pLi, unsigned short pCo, float pElevation)
    {
        unsigned long   lOffset = pLi * (eGeometryDefaultLength + 1) + pCo;

        GEO::Vertex3&  Vertex = vertices3()[lOffset];
        Vertex[2] = F32(pElevation);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pOutside    Monde � rendre.
        @param pController Contr�leur du monde.
     */
    Renderer::Renderer(WorldPtr pOutside, ControllerPtr pController)
        : EDIT::Renderer(pOutside)
        , mRenderers()
        , mGrounds()
        , mOutside(pOutside)
        , mController(pController)
        , mSun(STL::makeShared<LIT::DirectionalLight>())
    {
        mSun->setDirection(MATH::V4(+k1F, +k1F, +k1F, k0F).normalized());
        mSun->setAmbient(CLR::RGBA(F32(0.2F), F32(0.2F), F32(0.2F), k1F));
        mSun->setDiffuse(CLR::RGBA(F32(0.8F), F32(0.8F), F32(0.8F), k1F));
        mSun->setSpecular(CLR::RGBA(k0F, k0F, k0F, k1F));
    }


    /*! Destructeur.
     */
    Renderer::~Renderer() { }


    /*!
     */
    void Renderer::created(EntityPtr /*pChild*/, RendererPtr /*pRenderer*/)
    {
        // Gestion des tuiles...
    }


    /*!
     */
    void Renderer::dumpAppearances()
    {
        std::shared_ptr<ControllerPtr::element_type>   lOutside    = mController.lock();
        MATH::V3                                       lTarget     = camera().lock()->viewed();

        lOutside->deliverTiles();

        float   lDistanceToGround = (camera().lock()->position() - lTarget).norm();
        if (lDistanceToGround < 40.0F)
        {
            QWOR::Data*    lData   = lOutside->data().get();
            if (lData != nullptr)
            {
                for(int lLi = -1; lLi <= 1; ++lLi)
                {
                    for(int lCo = -1; lCo <= 1; ++lCo)
                    {
                        TILE::Tile*    lTile = std::dynamic_pointer_cast<TILE::Tile>(lData->tile(lTarget.x + lCo * TILE::eEdgeSize, lTarget.y + lLi * TILE::eEdgeSize)).get();

                        if (lTile != nullptr)
                        {
                            TILE::Tile::AppearancePtr lElement;
                            MATH::M4                   lTransform;
                            for(lTile->firstApp(lElement, lTransform); lElement != nullptr; lTile->nextApp(lElement, lTransform))
                            {
                                if (lElement->litingLights()->count() == 0)
                                {
                                    lElement->getLit(mSun);
                                }
                                pushAppearance(lElement, lTransform);
                            }
                        }
                    }
                }
            }
            return;
        }

        const float         kMinZoom    = 10.0F * kOutsideScale;
        float               lZoom       = camera().lock()->zoom();

        if      (lZoom < k0F)
        {
            return;
        }
        else if (lZoom < kMinZoom)
        {
            lZoom = kMinZoom;
        }

        typedef std::weak_ptr<APP::Appearance>   AppearancePtr;

        unsigned long               lWishedLevel = static_cast<unsigned long>(log2(lZoom / kMinZoom));
        std::vector<AppearancePtr> lAppearances;

        lOutside->setCurrentDetailLevel(std::min(lWishedLevel, lOutside->detailLevelCount() - 1));
        lOutside->getAppearances(lAppearances, camera().lock()->visibleArea());

        MATH::M4   lPosition;
        lPosition.loadIdentity();

        for(auto lAppI = lAppearances.begin(); lAppI != lAppearances.end(); ++lAppI)
        {
            std::shared_ptr<AppearancePtr::element_type>   lApp = lAppI->lock();

            if (lApp->litingLights()->count() == 0)
            {
                lApp->getLit(mSun);
            }
            pushAppearance(lApp, lPosition);
        }
    }
}
