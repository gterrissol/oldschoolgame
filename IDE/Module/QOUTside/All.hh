/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_ALL_HH
#define QOUT_ALL_HH

/*! @file IDE/Module/QOUTside/All.hh
    @brief Interface publique du module @ref QOUTside.
    @author @ref Guillaume_Terrissol
    @date 1er D�cembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QOUT   //! Editeur de monde ext�rieur.
{
    /*! @namespace QOUT
        @version 0.2

     */

    /*! @defgroup QOUTside QOUTside : Edition des mondes ext�rieurs.
        <b>namespace</b> QOUT.
     */

}

#include "Controller.hh"
#include "Editor.hh"
#include "Renderer.hh"

#endif  // De QOUT_ALL_HH
