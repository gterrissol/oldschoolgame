/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Module/QOUTside/Editor.cc
    @brief M�thodes (non-inline) des classes QOUT::Editor::EditorPrivate & QOUT::Editor.
    @author @ref Guillaume_Terrissol
    @date 22 Juillet 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QButtonGroup>

#include "EDITion/Requests.hh"

#include "Controller.hh"
#include "Editor.ui.hh"
#include "Enum.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Editor.
        @version 0.3
     */
    class Editor::EditorPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
        //! @name Constructeurs
        //@{
                        EditorPrivate(Editor* parent, OSGi::Context* pContext); //!< Constructeur.
        void            init();                                                 //!< Initialisation.
        //@}
        void            updateUI();                                             //!< Mise � jour de l'interface.

        OSGi::Context*      mContext;
        Ui::Editor          mUI;                                                //!< Interface cr��e par Designer.

        using ControllerPtr         = EDIT::Controller::Ptr;                    //!< 
        using WorldControllerPtr    = std::weak_ptr<OUT::Controller>;           //!< 
        ControllerPtr       mController;                                        //!< 
        WorldControllerPtr  mWorld;                                             //!< 
        bool                mIsExpanding;                                       //!< Expansion activ�e ?
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent, OSGi::Context* pContext)
        : q{pParent}
        , mContext{pContext}
        , mUI{}
        , mController{}
        , mWorld{}
        , mIsExpanding{false}
    { }


    /*!
     */
    void Editor::EditorPrivate::init()
    {
        // Construit l'interface.
        q->from(q);
        mUI.setupUi(q);
        q->setObjectName(QString::null);    // Le widget pourra �tre correctement renomm� par UIService.

        QButtonGroup*   lModeButtonGroup  = new QButtonGroup(q->findChild<QGroupBox*>("modeButtonGroup"));
        lModeButtonGroup->addButton(mUI.expandButton, 0);
        lModeButtonGroup->addButton(mUI.reduceButton, 1);
        q->connect(lModeButtonGroup, SIGNAL(buttonClicked(int)), SLOT(modeSelected(int)));

        QButtonGroup*   lArrowButtonGroup = new QButtonGroup(q->findChild<QGroupBox*>("arrowButtonGroup"));
        lArrowButtonGroup->addButton(mUI.nwButton,  0);
        lArrowButtonGroup->addButton(mUI.neButton,  1);
        lArrowButtonGroup->addButton(mUI.swButton,  2);
        lArrowButtonGroup->addButton(mUI.seButton,  3);
        lArrowButtonGroup->addButton(mUI.allButton, 4);
        q->connect(lArrowButtonGroup, SIGNAL(buttonClicked(int)), SLOT(arrowSelected(int)));

        q->modeSelected(mUI.expandButton->isChecked() ? 0 : 1);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::EditorPrivate::updateUI()
    {
        if (auto lWorld = mWorld.lock())
        {
            const UTI::Rect&    lExtent = lWorld->extent();

            // Met � jour les coordonn�es.
            mUI.westLongitude->setText(QString::number(- lExtent.roundedLeft()));
            mUI.eastLongitude->setText(QString::number(  lExtent.roundedRight()));
            mUI.northLatitude->setText(QString::number(  lExtent.roundedTop()));
            mUI.southLatitude->setText(QString::number(- lExtent.roundedBottom()));

            // Met � jour les boutons fl�ch�s.
            if (mIsExpanding)
            {
                bool    lMaxSizeReached = OUT::eMaxTileCount <= lExtent.roundedWidth() ||
                                          OUT::eMaxTileCount <= lExtent.roundedHeight();
                mUI.nwButton->setDisabled(lMaxSizeReached);
                mUI.neButton->setDisabled(lMaxSizeReached);
                mUI.swButton->setDisabled(lMaxSizeReached);
                mUI.seButton->setDisabled(lMaxSizeReached);
                mUI.allButton->setDisabled(lMaxSizeReached);
            }
            else
            {
                bool    lMinSizeReached = lExtent.roundedWidth()  <= eGeometryDefaultLength ||
                                          lExtent.roundedHeight() <= eGeometryDefaultLength;
                mUI.nwButton->setDisabled(lMinSizeReached);
                mUI.neButton->setDisabled(lMinSizeReached);
                mUI.swButton->setDisabled(lMinSizeReached);
                mUI.seButton->setDisabled(lMinSizeReached);
                mUI.allButton->setDisabled(lMinSizeReached);
            }
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor(OSGi::Context* pContext)
        : Module::Editor{}
        , EDIT::Observer{}
        , CORE::Translate<Editor>{}
        , CORE::Synchronized{"file + enabled + edition + out;"}
        , pthis{this, pContext}
    {
        pthis->init();
    }


    /*! Destructeur
     */
    Editor::~Editor() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::modeSelected(int pID)
    {

        pthis->mIsExpanding = pID == 0;

        if (pthis->mIsExpanding)
        {
            pthis->mUI.nwButton->setIcon(QIcon(":/ide/module/icons/arrow up left"));
            pthis->mUI.neButton->setIcon(QIcon(":/ide/module/icons/arrow up right"));
            pthis->mUI.swButton->setIcon(QIcon(":/ide/module/icons/arrow down left"));
            pthis->mUI.seButton->setIcon(QIcon(":/ide/module/icons/arrow down right"));
        }
        else
        {
            pthis->mUI.nwButton->setIcon(QIcon(":/ide/module/icons/arrow down right"));
            pthis->mUI.neButton->setIcon(QIcon(":/ide/module/icons/arrow down left"));
            pthis->mUI.swButton->setIcon(QIcon(":/ide/module/icons/arrow up right"));
            pthis->mUI.seButton->setIcon(QIcon(":/ide/module/icons/arrow up left"));
        }
        pthis->mUI.allButton->setIcon(QIcon(":/ide/module/icons/arrow none"));

        pthis->updateUI();
    }


    /*!
     */
    void Editor::arrowSelected(int pID)
    {
        if (!pthis->mWorld.expired())
        {
            const UTI::Rect&    lExtent     = pthis->mWorld.lock()->extent();
            F32                 lWidth      = F32(pthis->mIsExpanding ? + lExtent.width()  : - 0.5F * lExtent.width());
            F32                 lHeight     = F32(pthis->mIsExpanding ? + lExtent.height() : - 0.5F * lExtent.height());
            UTI::Rect           lNewExtent  = lExtent;

            switch(pID)
            {
                case 0: // NW.
                    lNewExtent.setLeft(lExtent.left() - lWidth);
                    lNewExtent.setTop (lExtent.top()  + lHeight);
                    break;
                case 1: // NE.
                    lNewExtent.setRight(lExtent.right() + lWidth);
                    lNewExtent.setTop  (lExtent.top()   + lHeight);
                    break;
                case 2: //SW.
                    lNewExtent.setLeft  (lExtent.left()   - lWidth);
                    lNewExtent.setBottom(lExtent.bottom() - lHeight);
                    break;
                case 3: // SE.
                    lNewExtent.setRight (lExtent.right()  + lWidth);
                    lNewExtent.setBottom(lExtent.bottom() - lHeight);
                    break;
                case 4: // All.
                    default:
                    lWidth  *= F32(0.5F);
                    lHeight *= F32(0.5F);
                    lNewExtent = UTI::Rect(lExtent.left()   - lWidth, lExtent.top()    + lHeight,
                                           lExtent.right()  + lWidth, lExtent.bottom() - lHeight);
                    break;
            }

            if (lNewExtent.isEmpty())
            {
                lNewExtent.setRect(F32(- 0.5F * QOUT::eGeometryDefaultLength),
                                   F32(  0.5F * QOUT::eGeometryDefaultLength),
                                   F32(  0.5F * QOUT::eGeometryDefaultLength),
                                   F32(- 0.5F * QOUT::eGeometryDefaultLength));
            }

            EDIT::EditRequest<const UTI::Rect&> lRequest{lNewExtent};
            EDIT::SubjectList{EDIT::Controller::subject(pthis->mController)}.perform(&lRequest);

            pthis->updateUI();
        }
    }


//------------------------------------------------------------------------------
//                              Pertinence du sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canManage(EDIT::Controller::Ptr pDatum) const
    {
        return (std::dynamic_pointer_cast<EditorPrivate::WorldControllerPtr::element_type>(pDatum) != nullptr);
    }


    /*!
     */
    void Editor::manage(EDIT::Controller::Ptr pDatum)
    {
        // controller a �t� contr�l� en amont.
        // NB : il ne faut garder qu'une seule r�f�rence forte (et g�n�rique)
        // sur le contr�leur (pour EDIT::Contorler::subject(...)).
        pthis->mController  = pDatum;
        pthis->mWorld       = std::dynamic_pointer_cast<EditorPrivate::WorldControllerPtr::element_type>(pDatum);

        EDIT::Subject*  lWorld  = EDIT::Controller::subject(pthis->mController);

        watch(lWorld);

        // Mise � jour de l'interface en fonction du nouveau datum.
        pthis->updateUI();
    }


    /*!
     */
    void Editor::unmanage()
    {
        watch(nullptr);

        pthis->updateUI();
    }

    /*!
     */
    void Editor::manageNone()
    {
        pthis->mController.reset();
        pthis->mWorld.reset();

        watch(nullptr);

        pthis->updateUI();
    }


//------------------------------------------------------------------------------
//                                Ecoute du sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::listen(const EDIT::Answer* /*pAnswer*/)
    {
        // Met simplement � jour l'interface.
        pthis->updateUI();

        // Et la vue 3D.
        worldView()->update();
    }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    CORE::Translated::Updater Editor::buildTranslater()
    {
        return [=]()
        {
            pthis->mUI.retranslateUi(this);
        };
    }
}
