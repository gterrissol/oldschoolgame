/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/Module/QOUTside/Renderer.inl
    @brief M�thodes inline des classes QOUT::Geometry.
    @author @ref Guillaume_Terrissol
    @date 31 Ao�t 2003 - 22 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GEOmetry/Fields.hh"
#include "STL/Vector.hh"

#include "Enum.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//                        OutsideGeometry : Informations
//------------------------------------------------------------------------------

    /*! Permet de conna�tre la zone couverte par cette g�om�trie, par rapport au monde dont cette instance repr�sente une partie.
        @return La zone repr�sent�e par cette g�om�trie
     */
    inline const UTI::Rect& Geometry::area() const
    {
        return mArea;
    }


    /*! @return Le niveau de d�tail de cette g�om�trie
     */
    inline unsigned long Geometry::detailLevel() const
    {
        return mLevel;
    }


//------------------------------------------------------------------------------
//                   OutsideGeometry : D�finition des Donn�es
//------------------------------------------------------------------------------

    /*! Ajoute un vertex � la g�om�trie. Cette m�thode (ainsi que pushTexCoord() & pushNormal()) est juste l� pour encapsuler l'acc�s aux
        conteneurs de GEO::Geometry.
        @param pVertex Nouveau vertex
     */
    inline void Geometry::pushVertex3(GEO::Vertex3 pVertex)
    {
        vertices3().push_back(pVertex);
    }


    /*! Ajoute une normale � la g�om�trie. Cette m�thode (ainsi que pushVertex3() & pushTexCoord()) est juste l� pour encapsuler l'acc�s aux
        conteneurs de GEO::Geometry.
        @param pNormal Nouvelle normale
     */
    inline void Geometry::pushNormal(GEO::Normal pNormal)
    {
        normals().push_back(pNormal);
    }


    /*! Ajoute une coordonn�e de texture � la g�om�trie. Cette m�thode (ainsi que pushVertex3() & pushNormal()) est juste l� pour encapsuler
        l'acc�s aux conteneurs de GEO::Geometry.
        @param pTexCoord Nouvelle coordonn�e de texte
     */
    inline void Geometry::pushTexCoord(GEO::TexCoord0 pTexCoord)
    {
        texCoords0().push_back(pTexCoord);
    }


//------------------------------------------------------------------------------
//              OutsideGeometry : Calcul des Normales
//------------------------------------------------------------------------------

    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V N)
     */
    inline MATH::V3 Geometry::computeN(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetN    = (pLi - 1) * (eGeometryDefaultLength + 1) + pCo;
        const GEO::Vertex3&    lN          = vertices3()[lOffsetN];

        return MATH::V3(lN[0] - mCurrentVertex[0], lN[1] - mCurrentVertex[1], lN[2] - mCurrentVertex[2]);
    }


    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V NE)
     */
    inline MATH::V3 Geometry::computeNE(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetNE   = (pLi - 1) * (eGeometryDefaultLength + 1) + (pCo + 1);
        const GEO::Vertex3&    lNE         = vertices3()[lOffsetNE];

        return MATH::V3(lNE[0] - mCurrentVertex[0], lNE[1] - mCurrentVertex[1], lNE[2] - mCurrentVertex[2]);
    }


    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V E)
     */
    inline MATH::V3 Geometry::computeE(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetE    = pLi * (eGeometryDefaultLength + 1) + (pCo + 1);
        const GEO::Vertex3&    lE          = vertices3()[lOffsetE];

        return MATH::V3(lE[0] - mCurrentVertex[0], lE[1] - mCurrentVertex[1], lE[2] - mCurrentVertex[2]);
    }


    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V S)
     */
    inline MATH::V3 Geometry::computeS(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetS    = (pLi + 1) * (eGeometryDefaultLength + 1) + pCo;
        const GEO::Vertex3&    lS          = vertices3()[lOffsetS];

        return MATH::V3(lS[0] - mCurrentVertex[0], lS[1] - mCurrentVertex[1], lS[2] - mCurrentVertex[2]);
    }


    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V SW)
     */
    inline MATH::V3 Geometry::computeSW(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetSW   = (pLi + 1) * (eGeometryDefaultLength + 1) + (pCo - 1);
        const GEO::Vertex3&    lSW         = vertices3()[lOffsetSW];

        return MATH::V3(lSW[0] - mCurrentVertex[0], lSW[1] - mCurrentVertex[1], lSW[2] - mCurrentVertex[2]);
    }


    /*! @param pLi Ordonn�e du vertex (V) dont on cherche � calculer la normale
        @param pCo Abscisse du vertex (V) dont on cherche � calculer la normale
        @return Vecteur (V W)
     */
    inline MATH::V3 Geometry::computeW(unsigned short pLi, unsigned short pCo) const
    {
        unsigned long           lOffsetW    = pLi * (eGeometryDefaultLength + 1) + (pCo - 1);
        const GEO::Vertex3&    lW          = vertices3()[lOffsetW];

        return MATH::V3(lW[0] - mCurrentVertex[0], lW[1] - mCurrentVertex[1], lW[2] - mCurrentVertex[2]);
    }
}
