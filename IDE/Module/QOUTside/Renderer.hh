/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_RENDERER_HH
#define QOUT_RENDERER_HH

#include "QOUTside.hh"

/*! @file IDE/Module/QOUTside/Renderer.hh
    @brief En-t�te des classes QOUT::Geometry & QOUT::Renderer.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 22 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Module/Renderer.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "MATH/V3.hh"
#include "OUTside/OUTside.hh"
#include "Module/QT3D/Widget.hh"
#include "STL/Map.hh"
#include "UTIlity/Rect.hh"

namespace QOUT
{
    /*! @brief Fragment d'ext�rieur.
        @version 0.6

        L'apparence d'un monde ext�rieur est d�coup� en plusieurs petites g�om�tries, selon le niveau de
        d�tail, qui sont des instances de cette classe.
     */
    class Geometry : public GEO::Geometry
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                                    Geometry(const UTI::Rect& pArea,
                                              unsigned long pLevel);                            //!< Constructeur.
virtual                             ~Geometry();                                               //!< Destructeur
        //@}
        //! @name Informations
        //@{
        inline  const UTI::Rect&   area() const;                                               //!< Zone couverte par la g�om�trie.
        inline  unsigned long       detailLevel() const;                                        //!< Niveau de d�tail de la g�om�trie.
        //@}
        //! @name Mise � jour
        //@{
                void                updateNormal(unsigned short pLi,
                                                 unsigned short pCo);                           //!< Mise � jour d'une normale.
        //@}
        //! @name D�finition des donn�es
        //@{
                void                setElevation(unsigned short pLi,
                                                 unsigned short pCo,
                                                 float          pElevation);                    //!< Edition d'une �l�vation.


    private:

        inline  void                pushVertex3(GEO::Vertex3 pVertex);                         //!< Empilement d'un vertex.
        inline  void                pushNormal(GEO::Normal pNormal);                           //!< Empilement d'une normale.
        inline  void                pushTexCoord(GEO::TexCoord0 pTexCoord);                    //!< Empilement d'un couple de coordonn�es de texture.
        //@}
        //! @name Calcul des Normales
        //@{
        inline MATH::V3            computeN(unsigned short pLi, unsigned short pCo) const;     //!< Normale au Nord.
        inline MATH::V3            computeNE(unsigned short pLi, unsigned short pCo)   const;  //!< Normale au Nord-Est.
        inline MATH::V3            computeE(unsigned short pLi, unsigned short pCo) const;     //!< Normale � l'Est.
        inline MATH::V3            computeS(unsigned short pLi, unsigned short pCo) const;     //!< Normale au Sud.
        inline MATH::V3            computeSW(unsigned short pLi, unsigned short pCo) const;    //!< Normale au Sud-Ouest
        inline MATH::V3            computeW(unsigned short pLi, unsigned short pCo) const;     //!< Normale � l'Ouest.
        //@}
        //! @name Informations
        //@{
        UTI::Rect      mArea;                                                                  //!< Zone couverte par la g�om�trie.
        MATH::V3       mCurrentVertex;                                                         //!< Vertex trait� lors du calcul des normales.
        unsigned long   mLevel;                                                                 //!< Niveau de d�tail associ� � la g�om�trie.
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Afficheur de monde ext�rieur (�ditable).
        @version 0.5

        Pour le rendu de monde ext�rieur (dans l'�diteur).
     */
    class Renderer : public EDIT::Renderer
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef std::weak_ptr<OUT::World>      WorldPtr;          //!< Pointeur sur monde ext�rieur.
        typedef std::weak_ptr<OUT::Controller> ControllerPtr;     //!< Pointeur sur contr�leur de monde ext�rieur.
        typedef std::weak_ptr<APP::Appearance> AppearancePtr;     //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Renderer(WorldPtr      pOutside,
                          ControllerPtr pController);              //!< Constructeur.
virtual         ~Renderer();                                       //!< Destructeur.
        //@}

    private:

virtual void    created(EntityPtr pChild, RendererPtr pRenderer); //!< Slot sur cr�ation du renderer d'une sous-entit�.
        //! @name Rendu
        //@{
virtual void    dumpAppearances();                                  //!< Pr�paration du rendu.
        //@}

        std::map<EntityPtr, RendererPtr>      mRenderers;
        std::map<EntityPtr, AppearancePtr>    mGrounds;
        WorldPtr                               mOutside;           //!< Monde ext�rieur � rendre.
        ControllerPtr                          mController;        //!< Contr�leur du monde ext�rieur � rendre.
        const QT3D::Widget*                    mWindow;            //!< Fen�tre dans laquelle afficher le monde.
        std::shared_ptr<LIT::DirectionalLight> mSun;               //!< Lumi�re.
    };
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Renderer.inl"

#endif  // De QOUT_RENDERER_HH
