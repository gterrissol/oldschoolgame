    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QOUTside.hh"

/*! @file IDE/Module/QOUTside/QOUTside.cc
    @brief D�finitions diverses du module @ref QOUTside.
    @author @ref Guillaume_Terrissol
    @date 1er D�cembre 2008 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "ERRor/ErrMsg.hh"
#include "Module/QWORld/ImportExport.hh"
#include "Module/QWORld/WorldDataService.hh"
#include "PAcKage/LoaderSaver.hh"
#include "UTIlity/Rect.hh"
#include "WORld/Enum.hh"

#include "Controller.hh"
#include "Editor.hh"
#include "Enum.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//                          D�finition d'une Constante
//------------------------------------------------------------------------------

    const F32   kOutsideScale   = F32(TILE::eEdgeSize);


//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kCantReduceNonExtendedWorld,    "Ne peut r�duire un monde non pr�alablement �tendu.",   "Can't reduce a non-extended world.")
    REGISTER_ERR_MSG(kGeometryOutOfWorld,            "G�om�trie hors du monde.",                             "Geometry out of world.")
    REGISTER_ERR_MSG(kIncorrectDimensions,           "Dimensions incorrectes.",                              "Incorrect dimensions.")
    REGISTER_ERR_MSG(kInvalidWorldTooSmall,          "Le monde n'est pas valide : il est trop petit.",       "Invalid world : too small.")
    REGISTER_ERR_MSG(kUnexistingElevation,           "El�vation inexistante.",                               "Unexisting elevation.")
    REGISTER_ERR_MSG(kUnexistingTexel,               "Texel inexistant.",                                    "Unexisting texel.")


//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de monde ext�rieur.
        @version 0.25
     */
    class ImportExportOutside : public QWOR::ImportExport
    {
    public:

virtual         ~ImportExportOutside();                                             //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


//------------------------------------------------------------------------------
//                                Monde Ext�rieur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportOutside::~ImportExportOutside() { }


    /*!
     */
    bool ImportExportOutside::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        exportHeader(pWriter, pLoader);

        data()->write<eFileHdl>(pWriter, pLoader, "Tiles");

        U32 lTileRowCount    = k0UL;
        pLoader >> lTileRowCount;
        U32 lTileColumnCount = k0UL;
        pLoader >> lTileColumnCount;
        pLoader.rollback(U32(sizeof(lTileRowCount) + sizeof(lTileColumnCount)));

        data()->write<eU32>(pWriter, pLoader, "Tile row count");
        data()->write<eU32>(pWriter, pLoader, "Tile column count");

        data()->write<eRect>(pWriter, pLoader, "Zone");

        data()->begin(pWriter, pLoader, "Tiles");
        {
            for(U32 lR = k0UL; lR < lTileRowCount; ++lR)
            {
                for(U32 lC = k0UL; lC < lTileColumnCount; ++lC)
                {
                    data()->write<eKey>(pWriter, pLoader);
                }
            }
        }
        data()->end(pWriter, pLoader, "Texels");

        return true;
    }


    /*!
     */
    void ImportExportOutside::doMake(PAK::Saver& pSaver)
    {
        auto    lService    = data()->fileDB()->context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data");
        auto    lGroup      = lService->groupFilenameFromWorld(data()->fileDB()->packFileName(pSaver.fileHdl()));

        pSaver << data()->fileDB()->packFileHdl(lGroup);    // Handle des donn�es g�n�rales.
        pSaver << I32{WOR::eOutside};   // Type de monde.

        pSaver << data()->fileDB()->create("tiles.rsc");    // Gestionnaire de tuiles.

        pSaver << k0UL;                 // Nombre de lignes de tuiles.
        pSaver << k0UL;                 // Nombre de colonnes de tuiles.
        pSaver << UTI::Rect{};          // zone couverte.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportOutside>("out",  lExtensionDir);
                    pContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->checkIn("out", WOR::eOutside);

                    auto    lGEService = pContext->services()->findByTypeAndName<QT3D::GameEntityService>("fr.osg.ide.module.3d.game_entity");
                    lGEService->checkIn<OUT::World>(OUT::Controller::make);

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->registerType(QStringLiteral("out"));

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("module.editor.outside", [=]() { return CORE::UIService::Widget{new Editor{pContext}}; });
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("module.editor.outside");

                    pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie")->checkOut("out");
                    pContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->checkOut("out");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QOUT::Activator)
OSGI_END_REGISTER_ACTIVATORS()
