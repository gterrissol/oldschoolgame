/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_ENUM_HH
#define QOUT_ENUM_HH

/*! @file IDE/Module/QOUTside/Enum.hh
    @brief Enum�rations et constantes du module QOUT.
    @author @ref Guillaume_Terrissol
    @date 1er D�cembre 2008 - 6 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "TILE/Enum.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//                                  Dimensions
//------------------------------------------------------------------------------

    extern  const F32   kOutsideScale;                              //!< Echelle des g�om�tries de monde ext�rieur.

    enum
    {
        eTexelSize             = 4,                                 //!< Taille d'un �l�ment de texture de tuile de monde.
        eTexelPerTileSide      = TILE::eFragmentCount / eTexelSize, //!< Nombre d'�l�ments de texture par tuile de monde (pour un c�t�).
        eGeometryDefaultLength = 8                                  //!< Longueur par d�faut d'une g�om�trie de monde ext�rieur �ditable (en triangles - et tuiles).
    };
}

#endif // De QOUT_ENUM_HH
