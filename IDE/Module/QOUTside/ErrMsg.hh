/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_ERRMSG_HH
#define QOUT_ERRMSG_HH

/*! @file IDE/Module/QOUTside/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module QOUT.
    @author @ref Guillaume_Terrissol
    @date 1er D�cembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace QOUT
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kCantReduceNonExtendedWorld;
    extern  ERR::String kGeometryOutOfWorld;
    extern  ERR::String kIncorrectDimensions;
    extern  ERR::String kInvalidWorldTooSmall;
    extern  ERR::String kUnexistingElevation;
    extern  ERR::String kUnexistingTexel;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De QOUT_ERRMSG_HH
