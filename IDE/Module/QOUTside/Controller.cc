/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/Module/QOUTside/Controller.cc
    @brief M�thodes (non-inline) de la classe QOUT::Controller.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "APPearance/BaseAppearance.hh"
#include "CAMera/BaseCamera.hh"
#include "EDITion/Attributes.hh"
#include "MATerial/All.hh"
#include "CORE/ProgressService.hh"
#include "Module/QWORld/GenericBrush.tcc"
#include "Module/QWORld/WorldDataService.hh"
#include "STL/Shared.tcc"
#include "TILE/HeightField.tcc"

#include "Enum.hh"
#include "ErrMsg.hh"
#include "Renderer.hh"

namespace EDIT
{
#ifndef NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Cette m�thode doit �tre sp�cialis�e pour permettre la fusion de deux requ�tes (pour grouper les
        requ�tes dans le syst�me de undo/redo).
        @param pEvent Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return Une requ�te obtenue en fusionnant *<b>this</b> et <i>pEvent</i>, lorsque cela est valide,
        0 sinon
        @note Par d�faut, les requ�tes ne peuvent pas fusionner (il faut sp�cialiser)
     */
    template<>
    bool EditRequest<const QWOR::HeightGroup&>::mergeRequest(const EDIT::Request* pEvent)
    {
        const EditRequest<const QWOR::HeightGroup&>*  lRequest = dynamic_cast<const EditRequest<const QWOR::HeightGroup&>*>(pEvent);

        if (lRequest != nullptr)
        {
            const QWOR::HeightGroup&   lNewGroup   = lRequest->mValue;
            if (lNewGroup.count() == 0)
            {
                return false;
            }
            QWOR::HeightGroup&         lOldGroup   = mValue;

            for(U32 lN = k0UL; lN < lNewGroup.count(); ++lN)
            {
                const QWOR::Height&    lNew        = lNewGroup[lN];
                bool                    lMatching   = false;

                for(U32 lO = k0UL; lO < lOldGroup.count(); ++lO)
                {
                    QWOR::Height&  lOld    = lOldGroup[lO];

                    if ((lOld.col() == lNew.col()) && (lOld.row() == lNew.row()))
                    {
                        if (!mUndo)
                        {
                            lOld = lNew.data();
                        }
                        lMatching = true;
                        // Les choses �tant bien faites, d�s qu'il y a correspondance, on peut passer � la nouvelle �l�vation suivante.
                        break;
                    }
                }

                if (!lMatching)
                {
                    lOldGroup.insertInArea(lNew);
                }
            }

            return true;
        }

        return false;
    }

    /*! Cette m�thode doit �tre sp�cialis�e pour permettre la fusion de deux requ�tes (pour grouper les
        requ�tes dans le syst�me de undo/redo).
        @param pEvent Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return Une requ�te obtenue en fusionnant *<b>this</b> et <i>pEvent</i>, lorsque cela est valide, 0 sinon
        @note Par d�faut, les requ�tes ne peuvent pas fusionner (il faut sp�cialiser)
     */
    template<>
    bool EditRequest<const QWOR::TexelGroup&>::mergeRequest(const EDIT::Request* pEvent)
    {
        const EditRequest<const QWOR::TexelGroup&>*   lRequest = dynamic_cast<const EditRequest<const QWOR::TexelGroup&>*>(pEvent);

        if (lRequest != nullptr)
        {
            const QWOR::TexelGroup&    lNewGroup   = lRequest->mValue;
            if (lNewGroup.count() == 0)
            {
                return false;
            }
            QWOR::TexelGroup&          lOldGroup   = mValue;

            for(U32 lN = k0UL; lN < lNewGroup.count(); ++lN)
            {
                const QWOR::Texel&     lNew        = lNewGroup[lN];

                if (!TILE::FragmentMgr::get()->isTexelInactive(lNew.data()))
                {
                    bool    lMatching   = false;

                    for(U32 lO = k0UL; lO < lOldGroup.count(); ++lO)
                    {
                        QWOR::Texel&   lOld    = lOldGroup[lO];

                        if ((lOld.col() == lNew.col()) && (lOld.row() == lNew.row()))
                        {
                            if (!mUndo)
                            {
                                lOld = lNew.data();
                            }
                            lMatching = true;
                            // Les choses �tant bien faites, d�s qu'il y a correspondance, on peut passer au nouveau texel suivant.
                            break;
                        }
                    }

                    if (!lMatching)
                    {
                        lOldGroup.insertInArea(lNew);
                    }
                }
            }

            return true;
        }

        return false;
    }

#endif  // De NOT_FOR_DOXYGEN
}

namespace QWOR
{
#ifndef NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                Pinceau G�n�rique : Traitement sur les Donn�es
//------------------------------------------------------------------------------

    /*  El�vation � une position donn�e (sp�cialisation pour un texel).
     */
    template<>
    MATH::V4 GenericBrush<EnvironmentBrush, TexelGroup, OUT::Controller>::vertexForModel(const Texel& pPiece, float pWeight) const
    {
        // Initialement, une interpolation �tait r�alis�e sur la quatre sommets du "carr�" de terrain au-dessus duquel se trouvait la
        // position � traiter. Mais cela provoquait des intersections de l'apparance du pinceau avec le terrain dans certains cas, � cause de
        // la d�composition de ce carr� de terrain en 2 triangles pour le rendu. L'algorithme a donc chang� pour que l'apparence du pinceau
        // �pouse parfaitement la forme du terrain. Il n'y a pas de probl�me avec les tuiles, car, alors, le terrain est davantage "d�fini"
        // que la texture (unit� == fragment).
        HeightGroup    lElevations;
        TOffset         lCol    = pPiece.col();
        TOffset         lRow    = pPiece.row();

        float   lScaledCol          = float(lCol) / TILE::eFragmentCount;
        float   lScaledRow          = float(lRow) / TILE::eFragmentCount;
        TOffset lRoundedScaledCol   = TOffset(floorf(lScaledCol));
        TOffset lRoundedScaledRow   = TOffset(ceilf(lScaledRow));
        lScaledCol  -= lRoundedScaledCol;
        lScaledRow  -= lRoundedScaledRow;

        bool    lOnUpTr             = lScaledCol - lScaledRow < k1F;
        bool    lOnDownTr           = k1F < lScaledCol - lScaledRow;

        UTI::Rect              lValidExtent(this->validExtentForModel() * F32(k1F / QOUT::eTexelSize));
        std::vector<MATH::V3>  lVertices;
        lVertices.reserve(3);
        Height::TType          kZero(0);

        // El�vations � r�cup�rer, selon les cas.
        if      (lOnUpTr)
        {
            // Sur le triangle sup�rieur.
                lElevations.insert(Height(lRoundedScaledCol,     lRoundedScaledRow,     kZero));
                lVertices.push_back(MATH::V3(k0F,   k0F, k0F));

            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol + k1F), F32(lRoundedScaledRow))))
            {
                lElevations.insert(Height(lRoundedScaledCol + 1, lRoundedScaledRow,     kZero));
                lVertices.push_back(MATH::V3(k1F,   k0F, k0F));
            }

            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol), F32(lRoundedScaledRow - k1F))))
            {
                lElevations.insert(Height(lRoundedScaledCol,     lRoundedScaledRow - 1, kZero));
                lVertices.push_back(MATH::V3(k0F, - k1F, k0F));
            }
        }
        else if (lOnDownTr)
        {
            // Sur le triangle inf�rieur.
            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol + k1F), F32(lRoundedScaledRow))))
            {
                lElevations.insert(Height(lRoundedScaledCol + 1, lRoundedScaledRow,     kZero));
                lVertices.push_back(MATH::V3(k1F, k0F, k0F));
            }

            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol),       F32(lRoundedScaledRow - k1F))))
            {
                lElevations.insert(Height(lRoundedScaledCol,     lRoundedScaledRow - 1, kZero));
                lVertices.push_back(MATH::V3(k0F, -k1F, k0F));
            }

            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol + k1F), F32(lRoundedScaledRow - k1F))))
            {
                lElevations.insert(Height(lRoundedScaledCol + 1, lRoundedScaledRow - 1, kZero));
                lVertices.push_back(MATH::V3(k1F, -k1F, k0F));
            }
        }
        else    // (!lOnUpTr && !lOnDownTr)
        {
            // Sur la diagonale ou un des bords.
            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol),       F32(lRoundedScaledRow - k1F))))
            {
                lElevations.insert(Height(lRoundedScaledCol,     lRoundedScaledRow - 1, kZero));
                lVertices.push_back(MATH::V3(k0F, - k1F, k0F));
            }
            if (contains(lValidExtent, UTI::Pointf(F32(lRoundedScaledCol + k1F), F32(lRoundedScaledRow))))
            {
                lElevations.insert(Height(lRoundedScaledCol + 1, lRoundedScaledRow,     kZero));
                lVertices.push_back(MATH::V3(k1F,   k0F, k0F));
            }
        }

        mEdited.lock()->get(lElevations);

        // Interpolation pour calculer l'�l�vation finale.
        float   lAltitude = k0F;
        switch(lVertices.size())
        {
            case 1:
                lAltitude = lElevations[0].data();
                break;
            case 2:
            {
                // Sur la diagonale.
                lAltitude = (k1F - lScaledCol) * lElevations[0].data() +
                             lScaledCol        * lElevations[1].data();
                break;
            }
            case 3:
            {
                // Sur le triangle sup�rieur.
                MATH::V3 v0(lVertices[0].x, lVertices[0].y, F32(lElevations[0].data()));
                MATH::V3 v1(lVertices[1].x, lVertices[1].y, F32(lElevations[1].data()));
                MATH::V3 v2(lVertices[2].x, lVertices[2].y, F32(lElevations[2].data()));

                MATH::V3 vN = ((v1 - v0) ^ (v2 - v0)).normalized();
                lAltitude = (v0 * vN - lScaledCol * vN.x - lScaledRow * vN.y) / vN.z;
                break;
            }
            default:
                ASSERT(false, "Aucune �l�vation r�cup�r�e.")
                break;
        }

        return MATH::V4(F32(lCol), F32(lRow), F32(lAltitude), F32(pWeight));
    }

#endif  // De NOT_FOR_DOXYGEN
}

namespace QOUT
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Texture : public MAT::Image
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Texture(unsigned short pWidth,
                                 unsigned short pHeight,
                                 MAT::EFormat pPixelFormat);                //!< Constructeur.
virtual                 ~Texture();                                         //!< Destructeur.
        //@}
        inline  void    setLine(unsigned short pLine, const void* pData);   //!< D�finition des donn�es.
    };


    /*!
     */
    class Material : public MAT::Material
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        Material(MAT::Texture::ImagePtr pImage); //!< Constructeur.
virtual ~Material();                               //!< Destructeur.
        //@}
    private:

        /*!
         */
        class Texture : public MAT::Texture
        {
        public:
            //! @name Constructeur & destructeur
            //@{
            Texture(ImagePtr pImage);             //!< Constructeur.
    virtual ~Texture();                            //!< Destructeur.
            //@}
        };
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Texture::Texture(unsigned short pWidth, unsigned short pHeight, MAT::EFormat pPixelFormat)
        : MAT::Image()
    {
        setFormat(pPixelFormat);
        setWidth(U16(pWidth));
        setHeight(U16(pHeight));
        keepDataAfterBind(true);
    }


    /*! Destructeur.
     */
    Texture::~Texture() { }


    /*!
     */
    inline void Texture::setLine(unsigned short pLine, const void* pData)
    {
        MAT::Image::setLine(U16(pLine), pData);
    }


    /*! Constructeur.
     */
    Material::Material(MAT::Texture::ImagePtr pImage)
        : MAT::Material()
    {
        TexturePtr lTexture = std::make_shared<Texture>(pImage);
        addTexture(lTexture, "uTexture0");
    }


    /*! Destructeur.
     */
    Material::~Material() { }


    /*! Constructeur.
     */
    Material::Texture::Texture(ImagePtr pImage)
        : MAT::Texture()
    {
        setManualImage(pImage);
        pImage->bind();
    }


    /*! Destructeur.
     */
    Material::Texture::~Texture() { }
}

namespace OUT
{
//------------------------------------------------------------------------------
//                             P-Impl de Controller
//------------------------------------------------------------------------------

    /*! @brief P-Impl de OUT::Controller.
        @version 0.5
     */
    class Controller::Private
        : public EDIT::Subject
        , public EDIT::Value<const UTI::Rect&>
        , public EDIT::Value<const QWOR::HeightGroup&>
        , public EDIT::Value<const QWOR::TexelGroup&>
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef std::shared_ptr<APP::Appearance>   AppearancePtr;     //!< Pointeur sur apparence.
        typedef std::shared_ptr<QOUT::Geometry>    GeometryPtr;       //!< Pointeur sur g�om�trie d'ext�rieur.
        typedef std::shared_ptr<QOUT::Texture>     TexturePtr;        //!< Pointeur sur image �ditable.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Private(Controller* pParent);                         //!< Constructeur.
                ~Private();                                            //!< Destructeur.
        //@}
        //! @name Mise � jour des donn�es
        //@{
        bool    isEditable(const QWOR::Height& pHeight) const;         //!< Possibilit� d'�dition d'une �l�vation.
        bool    isEditable(const QWOR::Texel&  pTexel)  const;         //!< Possibilit� d'�dition d'un texel.
        void    build();                                                //!< Construction de l'apparence.
        void    clear();                                                //!< Destruction de l'apparence.
        void    update(GeometryPtr  pGeometry) const;                  //!< Mise � jour d'une g�om�trie �l�mentaire.
        void    update(TexturePtr   pTexture,
                       unsigned long pLevel,
                       unsigned long pRow,
                       unsigned long pCol) const;                       //!< Mise � jour d'une texture �l�mentaire.
        void    updateTileKeys();                                       //!< Mise � jour des clefs de tuile.
        //@}
        //! @name Cr�ation
        //@{
        void    createDefault();                                        //!< Cr�ation d'un monde par d�faut.
        //@}
        //! @name Donn�es "format�es" du monde
        //@{
        typedef std::vector<AppearancePtr>     AppearanceRow;         //!< Tableau d'apparences.
        typedef std::vector<AppearanceRow>     AppearanceArea;        //!< Tableau d'apparences � deux dimensions.

        typedef std::vector<GeometryPtr>       GeometryRow;           //!< Tableau de g�om�tries.
        typedef std::vector<GeometryRow>       GeometryArea;          //!< Tableau de g�om�tries � deux dimensions.

        typedef std::vector<TexturePtr>        TexelRow;              //!< Tableau d'images.
        typedef std::vector<TexelRow>          TexelArea;             //!< Tableau d'images � deux dimensions.

        std::vector<AppearanceArea>        mGround;                    //!< Apparences �l�mentaires.
        std::vector<GeometryArea>          mGeometries;                //!< G�om�tries �l�mentaires.
        std::vector<TexelArea>             mTextures;                  //!< Textures �l�mentaires.
        //@}
        //! @name Information g�n�rales
        //@{
        UTI::Rect                          mExtent;                    //!< Etendue du monde.
        unsigned long                       mCurrentDetailLevel;        //!< Niveau de d�tail affich�.
        //@}
        std::shared_ptr<QOUT::Renderer>    mRenderer;
        std::shared_ptr<Controller>        mParent;                    //!< Contr�leur propri�taire.
        QWOR::Data::Ptr                   mWorldData;                 //!< Donn�es de monde.

    private:

        void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo) override;   //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                Controller P-Impl : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Monde ext�rieur �ditable dont cette instance est l'"impl�mentation priv�e"
     */
    Controller::Private::Private(Controller* pParent)
        : EDIT::Subject{"module"}
        , INITIALIZE_VALUE(setExtent, extent)
        , INITIALIZE_VALUE(set,       get, const QWOR::HeightGroup&)
        , INITIALIZE_VALUE(set,       get, const QWOR::TexelGroup&)
        , mGround()
        , mGeometries()
        , mTextures()
        , mExtent(UTI::Pointf(), UTI::Sizef())
        , mCurrentDetailLevel(0)
        , mRenderer()
        , mParent(pParent, STL::null_delete())
        , mWorldData()
    { }


    /*! Destructeur.
     */
    Controller::Private::~Private()
    {
        clear();
    }


//------------------------------------------------------------------------------
//                  Controller P-Impl : Mise � Jour des Donn�es
//------------------------------------------------------------------------------

    /*! Permet de savoir si une �l�vation peut �tre modifi�e (la tuile associ�e n'est pas encore verrouill�e).
        @param pHeight El�vation dont on souhaite savoir si elle peut �tre modifi�e.
        @retval Vrai si l'�l�vation peut-�tre modifi�e,
        @retval Faux sinon.
     */
    bool Controller::Private::isEditable(const QWOR::Height& pHeight) const
    {
        const UTI::Rect&   lArea   = mParent->extent();
        unsigned short      lWidth  = lArea.roundedWidth();
        unsigned short      lHeight = lArea.roundedHeight();
        signed short        lX      = pHeight.col() - lArea.roundedLeft();
        signed short        lY      = lArea.roundedTop() - pHeight.row();
        signed short        lX_1    = lX - 1;
        signed short        lY_1    = lY - 1;

        if (0 <= lY_1)
        {
            if (0 <= lX_1)
            {
                if (mWorldData->isTileLocked(lX_1, lY_1))
                {
                    return false;
                }
            }
            if (lX < lWidth)
            {
                if (mWorldData->isTileLocked(lX, lY_1))
                {
                    return false;
                }
            }
        }
        if (lY < lHeight)
        {
            if (0 <= lX_1)
            {
                if (mWorldData->isTileLocked(lX_1, lY))
                {
                    return false;
                }
            }
            if (lX < lWidth)
            {
                if (mWorldData->isTileLocked(lX, lY))
                {
                    return false;
                }
            }
        }

        return true;
    }


    /*! Permet de savoir si un texel peut �tre modifi� (la tuile associ�e n'est pas encore verrouill�e).
        @param pTexel Texel dont on souhaite savoir s'il peut �tre modifi�.
        @retval Vrai si le texel peut-�tre modifi�,
        @retval Faux sinon.
     */
    bool Controller::Private::isEditable(const QWOR::Texel& pTexel) const
    {
        signed short    lX   = (float(pTexel.col()) / QOUT::eTexelPerTileSide) - mParent->extent().roundedLeft();
        signed short    lY   = mParent->extent().roundedTop() - (float(pTexel.row()) / QOUT::eTexelPerTileSide);

        return !mWorldData->isTileLocked(lX, lY);
    }


    /*! Construit le maillage du monde � partir des donn�es d'�l�vations et des texels.
     */
    void Controller::Private::build()
    {
        clear();    // Au cas o�...

        unsigned short  lCurrentWidth   = mParent->extent().roundedWidth();
        unsigned short  lCurrentHeight  = mParent->extent().roundedHeight();
        float           lCurrentTop     = mParent->extent().top();
        float           lCurrentLeft    = mParent->extent().left();
        unsigned short  lCurrentLevel   = 0;

        unsigned long   lStepCount      = 1;
        unsigned long   lStep           = 0;

        for(unsigned long lW = lCurrentWidth; QOUT::eGeometryDefaultLength <= lW; lW /= 2, lStepCount += lW / QOUT::eGeometryDefaultLength) { }

        auto    lProgress   = mParent->context()->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, 100);
        lProgress->advance(lStep);

        // Les niveaux sont cr��s � l'envers : 0 => le plus d�taill�.
        while((QOUT::eGeometryDefaultLength <= lCurrentWidth) && (QOUT::eGeometryDefaultLength <= lCurrentHeight))
        {
            // Nouveau niveau de d�tail.
            mGround.    push_back(AppearanceArea());
            mGeometries.push_back(GeometryArea());
            mTextures.  push_back(TexelArea());

            unsigned short  lGeoCountInHeight   = lCurrentHeight / QOUT::eGeometryDefaultLength;

            mGround.    back().resize(lGeoCountInHeight);
            mGeometries.back().resize(lGeoCountInHeight);
            mTextures.  back().resize(lGeoCountInHeight);

            for(unsigned short lGeoInHeight = 0; lGeoInHeight < lGeoCountInHeight; ++lGeoInHeight)
            {
                unsigned short  lGeoCountInWidth = lCurrentWidth / QOUT::eGeometryDefaultLength;
                mGround.back()    [lGeoInHeight].resize(lGeoCountInWidth);
                mGeometries.back()[lGeoInHeight].resize(lGeoCountInWidth);
                mTextures.back()  [lGeoInHeight].resize(lGeoCountInWidth);

                for(unsigned short lGeoInWidth = 0; lGeoInWidth < lGeoCountInWidth; ++lGeoInWidth)
                {
                    // Cr�e un triplet (apparence, g�om�trie, texture).
                    float       lLeft       = lCurrentLeft + lGeoInWidth  * (QOUT::eGeometryDefaultLength  << lCurrentLevel);
                    float       lTop        = lCurrentTop  - lGeoInHeight * (QOUT::eGeometryDefaultLength  << lCurrentLevel);
                    float       lWidth      = (QOUT::eGeometryDefaultLength << lCurrentLevel) + 1;
                    float       lHeight     = (QOUT::eGeometryDefaultLength << lCurrentLevel) + 1;
                    UTI::Rect   lGeoArea    = UTI::Rect(UTI::Pointf(F32(lLeft),  F32(lTop)),
                                                        UTI::Sizef (F32(lWidth), F32(lHeight)));

                    GeometryPtr lGeometry   = std::make_shared<QOUT::Geometry>(lGeoArea, lCurrentLevel);
                    update(lGeometry);

                    mGeometries.back()[lGeoInHeight][lGeoInWidth] = lGeometry;

                    TexturePtr lImage = std::make_shared<QOUT::Texture>(
                                            QOUT::eTexelPerTileSide * QOUT::eGeometryDefaultLength,
                                            QOUT::eTexelPerTileSide * QOUT::eGeometryDefaultLength,
                                            MAT::EFormat(MAT::e32Bits | MAT::eRGBA));
                    update(lImage, lCurrentLevel, lGeoInHeight, lGeoInWidth);

                    mTextures.back()[lGeoInHeight][lGeoInWidth] = lImage;

                    std::shared_ptr<QOUT::Material> lMaterial = std::make_shared<QOUT::Material>(lImage);

                    AppearancePtr   lAppearance = std::make_shared<APP::Appearance>(lGeometry, lMaterial);
                    mGround.back()[lGeoInHeight][lGeoInWidth] = lAppearance;
                    lAppearance->bind(mParent->edited().lock()->OBJ::GameEntity::shared_from_this());
                }

                lProgress->advance((++lStep * 100) / lStepCount);
            }

            lCurrentWidth >>= 0x01;
            lCurrentHeight  >>= 0x01;
            ++lCurrentLevel;
        }
    }


    /*! D�truit toutes les apparences (et les g�om�tries et images associ�es).
     */
    void Controller::Private::clear()
    {
        mGround.clear();
        mGeometries.clear();
        mTextures.clear();
    }


    /*! Met � jour les donn�es d'une g�om�trie (positionn�e sur le monde, avec un niveau de d�tail donn�).
        @param pGeometry G�om�trie � mettre � jour
     */
    void Controller::Private::update(GeometryPtr pGeometry) const
    {
        unsigned long   lGeoRealStep  = 0x00000001 << pGeometry->detailLevel();
        unsigned long   lFirstRow     = mParent->extent().roundedTop() - pGeometry->area().roundedTop();
        unsigned long   lFirstColumn  = pGeometry->area().roundedLeft() - mParent->extent().roundedLeft();
        unsigned long   lWorldWidth   = mParent->extent().roundedWidth() + 1;   // + 1 : nombre de tuiles => nombre d'�l�vations.
        unsigned long   lOffset       = lFirstRow * lWorldWidth + lFirstColumn;

        ASSERT_EX(lOffset < mWorldData->elevationCount(), QOUT::kGeometryOutOfWorld, return;)

        for(unsigned short lLi = 0; lLi <= QOUT::eGeometryDefaultLength; ++lLi, lOffset += lGeoRealStep * lWorldWidth)
        {
            for(unsigned short lCo = 0; lCo <= QOUT::eGeometryDefaultLength; ++lCo)
            {
                pGeometry->setElevation(lLi, lCo, mWorldData->elevation(lOffset + lCo * lGeoRealStep) * TILE::kVerticalStep);
            }
        }
        // Une fois toutes les �l�vations mises � jour, les normales peut �tre recalcul�es.
        for(unsigned short lLi = 0; lLi <= QOUT::eGeometryDefaultLength; ++lLi, lOffset += lGeoRealStep * lWorldWidth)
        {
            for(unsigned short lCo = 0; lCo <= QOUT::eGeometryDefaultLength; ++lCo)
            {
                pGeometry->updateNormal(lLi, lCo);
            }
        }
    }


    /*! Met � jour les donn�es d'une texture (positionn�e sur le monde, avec un niveau de d�tail donn�).
        @param pTexture Texture � mettre � jour
        @param pLevel   Niveau de d�tail auquel la texture est associ�e
        @param pRow     Position "verticale" de la texture sur la texture enti�re du monde
        @param pCol     Position "horizontale" de la texture sur la texture enti�re du monde
     */
    void Controller::Private::update(TexturePtr pTexture, unsigned long pLevel, unsigned long pRow, unsigned long pCol) const
    {
        unsigned long       lImgRealStep    = 0x00000001 << pLevel;
        unsigned long       lFirstLine      = pRow * lImgRealStep * QOUT::eGeometryDefaultLength * TILE::eFragmentCount;
        unsigned long       lFirstColumn    = pCol * lImgRealStep * QOUT::eGeometryDefaultLength * TILE::eFragmentCount;
        unsigned long       lWorldWidth     = mParent->extent().roundedWidth() * TILE::eFragmentCount;
        unsigned long       lOffset         = lFirstLine * lWorldWidth + lFirstColumn;

        unsigned short      lTextureWidth   = pTexture->width();
        unsigned short      lTextureHeight  = pTexture->height();
        std::vector<int>   lLine(lTextureWidth, 0);

        for(unsigned short lLi = 0; lLi < lTextureHeight; ++lLi, lOffset += lImgRealStep * lWorldWidth * QOUT::eTexelSize)
        {
            for(unsigned short lCo = 0; lCo < lTextureWidth; ++lCo)
            {
                lLine[lCo] = U32(TILE::FragmentMgr::get()->tilingColor(TILE::Tiling::Id(mWorldData->texel(lOffset + lCo * lImgRealStep * QOUT::eTexelSize).tilingId0)));
            }
            const auto* lLineData = &lLine[0];
            pTexture->setLine(U16(lLi), lLineData);
        }

        pTexture->bind();
    }


    /*!
     */
    void Controller::Private::updateTileKeys()
    {
        unsigned long   lWidth  = mExtent.roundedWidth();
        unsigned long   lHeight = mExtent.roundedHeight();
        for(U16 lLi = k0UW; lLi < lHeight; ++lLi)
        {
            for(U16 lCo = k0UW; lCo < lWidth; ++lCo)
            {
                TILE::Key      lKey    = mWorldData->tileKey(lCo, lLi);
                mParent->edited().lock()->setTileKey(lCo, lLi, lKey);
            }
        }
    }


//------------------------------------------------------------------------------
//                         Controller P-Impl : Cr�ations
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::Private::createDefault()
    {
        UTI::Rect  lDefaultArea(F32(- 0.5F * QOUT::eGeometryDefaultLength),
                                 F32(  0.5F * QOUT::eGeometryDefaultLength),
                                 F32(  0.5F * QOUT::eGeometryDefaultLength),
                                 F32(- 0.5F * QOUT::eGeometryDefaultLength));

        mParent->setExtent(lDefaultArea);

        ASSERT((mExtent.roundedWidth() == QOUT::eGeometryDefaultLength) && (mExtent.roundedHeight() == QOUT::eGeometryDefaultLength), QOUT::kInvalidWorldTooSmall);
    }


//------------------------------------------------------------------------------
//                         Controller P-Impl : 
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::Private::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                   Controller : Constructeurs & destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::Controller(EditedPtr pWorld, OSGi::Context* pContext)
        : QT3D::GenericController<World>(pWorld, pContext)
        , pthis(this)
    {
        pWorld.lock()->activateSaveSelf(true);

        pthis->mWorldData = context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->data(pWorld.lock()->group());

        if (pWorld.lock()->width() == 0)
        {
            pthis->createDefault();
        }
        else
        {
            pthis->mExtent = pWorld.lock()->area() * (k1F / QOUT::kOutsideScale);
            pthis->build();
        }

        pthis->mRenderer = std::make_shared<QOUT::Renderer>(pWorld, shared_from_this());
        pthis->updateTileKeys();
    }


    /*! Destructeur.
     */
    Controller::~Controller() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Controller::ControllerPtr Controller::make(EntityPtr pEntity, OSGi::Context* pContext)
    {
        return std::make_shared<Controller>(std::dynamic_pointer_cast<World>(pEntity.lock()), pContext);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QWOR::Data::Ptr Controller::data()
    {
        return pthis->mWorldData;
    }


    /*!
     */
    void Controller::deliverTiles()
    {
        edited().lock()->tileMgr()->deliverResources();
    }


//------------------------------------------------------------------------------
//                         Controller : Etendue du monde
//------------------------------------------------------------------------------

    /*! Permet de conna�tre l'�tendue du monde (position et dimensions).
        @return Un rectangle d�finissant l'�tendue du monde : les dimensions sont exprim�es en nombre de tuiles, les bords du rectangle sont
        ceux du monde, les coordonn�es verticales positives sont au nord
     */
    const UTI::Rect& Controller::extent() const
    {
        return pthis->mExtent;
    }


    /*!
     */
    UTI::Rect Controller::area() const
    {
        return QOUT::kOutsideScale * extent();
    }


//------------------------------------------------------------------------------
//                   Controller : Acc�s aux donn�es (lecture)
//------------------------------------------------------------------------------

    /*! Permet de r�cup�rer des �l�vations.
        @param pHeights Les �l�ments r�f�rencent les �l�vations � r�cup�rer : ils seront remplis avec les �l�vations correspondantes
     */
    void Controller::get(QWOR::HeightGroup& pHeights) const
    {
        signed short    lNorth  = getWorldExtent().roundedTop();
        signed short    lWest   = getWorldExtent().roundedLeft();

        for(U32 lEl = k0UL; lEl < pHeights.count(); ++lEl)
        {
            QWOR::Height&  lHeight = pHeights[lEl];
            QWOR::Height::TType    lValue = pthis->mWorldData->elevation(short(lHeight.col() - lWest), short(lNorth - lHeight.row()));
            lHeight = lValue;
        }
    }


    /*! Permet de r�cup�rer des texels.
        @param pTexels Les �l�ments r�f�rencent les texels � r�cup�rer : ils seront remplis avec les texels correspondants
     */
    void Controller::get(QWOR::TexelGroup& pTexels) const
    {
        signed short    lNorth  = getTextureExtent().roundedTop();
        signed short    lWest   = getTextureExtent().roundedLeft();

        for(U32 lEl = k0UL; lEl < pTexels.count(); ++lEl)
        {
            QWOR::Texel&   lTexel  = pTexels[lEl];
            lTexel = pthis->mWorldData->texel(short(lTexel.col() - lWest) * QOUT::eTexelSize, short(lNorth - lTexel.row()) * QOUT::eTexelSize);
        }
    }


//------------------------------------------------------------------------------
//                         Controller : Niveau de d�tail
//------------------------------------------------------------------------------

    /*! Etablit le niveau de d�tail � utiliser.
        @param pLevel Nouveau niveau de d�tail
     */
    void Controller::setCurrentDetailLevel(unsigned long pLevel)
    {
        if (pLevel < detailLevelCount())
        {
            pthis->mCurrentDetailLevel = pLevel;
        }
    }


    /*! @return Le niveau de d�tail actuellement �tabli
     */
    unsigned long Controller::currentDetailLevel() const
    {
        return pthis->mCurrentDetailLevel;
    }


    /*! @return le nombre de niveaux de d�tail disponibles
     */
    unsigned long Controller::detailLevelCount() const
    {
        return pthis->mGround.size();
    }


//------------------------------------------------------------------------------
//                              Controller : Rendu
//------------------------------------------------------------------------------

    /*! Recense les apparences � envoyer au moteur de rendu.
        @param pVisibles  Ce vecteur sera rempli avec les apparences � afficher
        @param pVisibleArea Zone de visibilit� actuelle (seules les apparences � l'int�rieur seront ins�r�es dans <i>pVisibles</i>)
     */
    void Controller::getAppearances(std::vector<AppearancePtr>& pVisibles, const UTI::Rect& pVisibleArea) const
    {
        typedef std::shared_ptr<Private::AppearancePtr::element_type const>   ConstAppearancePtr;
        typedef std::shared_ptr<Private::GeometryPtr::element_type const>     ConstOGeometryPtr;

        pVisibles.clear();

        if (!pthis->mGround.empty())
        {
            const Private::AppearanceArea&    lAppearances    = pthis->mGround[currentDetailLevel()];
            const Private::GeometryArea&      lGeometries     = pthis->mGeometries[currentDetailLevel()];

            for(auto lRowI = lAppearances.begin(); lRowI != lAppearances.end(); ++lRowI)
            {
                const Private::GeometryRow& lGeometryRow = lGeometries[lRowI - lAppearances.begin()];

                for(auto lAppI = lRowI->begin(); lAppI != lRowI->end(); ++lAppI)
                {
                    ConstAppearancePtr lApp = *lAppI;
                    ConstOGeometryPtr  lGeo = lGeometryRow[lAppI - lRowI->begin()];

                    if ((lGeo != nullptr) && (intersects(lGeo->area() * F32(TILE::eEdgeSize), pVisibleArea) != UTI::eOutside))
                    {
                        pVisibles.push_back(*lAppI);
                    }
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                         Controller : Etendue du monde
//------------------------------------------------------------------------------

    /*! D�finit la nouvelle zone du monde, qui peut �tre �tendu ou r�duit (extend(const UTI::Rect& pToExtent) ou
        reduce(const UTI::Rect& pToExtent) resp. seront alors appel�es pour mettre � jour les donn�es.
        @param pNewExtent Nouvelle zone du monde (unit� == tuile)
        @note Si la nouvelle zone n'englobe pas la zone actuelle (extension), ni n'est contenue enti�rement par elle (r�duction), rien n'est
        fait
     */
    void Controller::setExtent(const UTI::Rect& pNewExtent)
    {
        if (intersects(extent(), pNewExtent) == UTI::eInside)
        {
            extend(pNewExtent);
        }
        else if (intersects(pNewExtent, extent()) == UTI::eInside)
        {
            reduce(pNewExtent);
        }

        // Une fois les nouvelles donn�es d�finies, on peut les envoyer dans le monde ext�rieur.
        edited().lock()->setArea(QOUT::kOutsideScale * pNewExtent);
        // NB: pNewArea est assign�e � pthis->mExtent dans Extend et Reduce.
        pthis->updateTileKeys();
    }


    /*! Etend le monde. Des tuiles sont ajout�es sur les bords pour atteindre la taille souhait�e.
        @param pToExtent Zone sur laquelle s'�tendre (par rapport � l'origine)
        @note Si la zone actuellement occup�e par le monde n'est pas totalement incluse dans
        <i>pToExtent</i>, rien n'est fait
     */
    void Controller::extend(const UTI::Rect& pToExtent)
    {
        // La nouvelle zone doit englober la pr�c�dente.
        if (intersects(extent(), pToExtent) != UTI::eInside)
        {
            return;
        }

        ASSERT_EX(((pToExtent.roundedWidth()  % QOUT::eGeometryDefaultLength) == 0) &&
                  ((pToExtent.roundedHeight() % QOUT::eGeometryDefaultLength) == 0),
                    "Les longueurs et largeurs des extensions de terrain doivent �tre des multiples de QOUT::eGeometryDefaultLength.",
                    return;)
        // Taille minimale � respecter : QOUT::eGeometryDefaultLength x QOUT::eGeometryDefaultLength tuiles
        if ((pToExtent.roundedWidth() < int(QOUT::eGeometryDefaultLength)) || (pToExtent.roundedHeight() < int(QOUT::eGeometryDefaultLength)))
        {
            return;
        }

        // D�truit les g�om�tries.
        pthis->clear();

        // D�place les donn�es.
        pthis->mWorldData->moveData(pToExtent);
        pthis->mExtent = pToExtent;

        // Recr�e les g�om�tries (il y aurait des optimisations � faire, du genre ne d�truire que les
        // g�om�tries qui ont chang�, mais j'ai la flemme; et puis, ce n'est pas si facile (quoi que...)).
        pthis->build();
    }


    /*! R�duit le monde. Des tuiles sont enlev�es des bords pour atteindre la taille souhait�e.
        @param pToExtent Zone � conserver (par rapport � l'origine)
        @note Si <i>pToExtent</i> n'est pas totalement incluse dans la zone actuellement occup�e par le
        monde, rien n'est fait
     */
    void Controller::reduce(const UTI::Rect& pToExtent)
    {
        // La nouvelle zone doit �tre incluse dans la pr�c�dente.
        if (intersects(pToExtent, extent()) != UTI::eInside)
        {
            return;
        }

        ASSERT_EX(((pToExtent.roundedWidth()  % QOUT::eGeometryDefaultLength) == 0) &&
                    ((pToExtent.roundedHeight() % QOUT::eGeometryDefaultLength) == 0),
                    "Les longueurs et largeurs des extensions de terrain doivent �tre des multiples de QOUT::eGeometryDefaultLength.",
                    return;)
        ASSERT_EX(!extent().isNull(), QOUT::kCantReduceNonExtendedWorld, return;)

        // D�truit les g�om�tries.
        pthis->clear();

        // D�place les donn�es.
        pthis->mWorldData->moveData(pToExtent);
        pthis->mExtent = pToExtent;

        // Recr�e les g�om�tries (il y aurait des optimisations � faire, du genre ne d�truire que les
        // g�om�tries qui ont chang�, mais j'ai la flemme; et puis, ce n'est pas si facile (quoi que...)).
        pthis->build();
    }


//------------------------------------------------------------------------------
//                   Controller : Acc�s aux donn�es (�criture)
//------------------------------------------------------------------------------

    /*! Permet de modifier les �l�vations.
        @param pHeights Groupe d'�l�vations (et leur position) � mettre � jour dans le terrain
     */
    void Controller::set(const QWOR::HeightGroup& pHeights)
    {
        signed short    lNorth  = getWorldExtent().roundedTop();
        signed short    lWest   = getWorldExtent().roundedLeft();
        signed long     lWidth  = getWorldExtent().roundedWidth() + 1;

        for(U32 lEl = k0UL; lEl < pHeights.count(); ++lEl)
        {
            const QWOR::Height&    lHeight = pHeights[lEl];

            if (pthis->isEditable(lHeight))
            {
                unsigned long       lOffset = ((lNorth - lHeight.row()) * lWidth + (lHeight.col() - lWest));

                ASSERT_EX(lOffset < pthis->mWorldData->elevationCount(), QOUT::kUnexistingElevation, return;)

                pthis->mWorldData->setElevation(lOffset, lHeight.data());
            }
        }

        // Les g�om�tries modifi�es sont imm�diatement mises � jour.
        if (0 < pHeights.count())
        {
            updateGeometry(pHeights.area() * QOUT::kOutsideScale);
        }
    }



    /*! Permet de modifier les texels.
        @param pTexels Groupe de texels (et leur position) � mettre � jour dans la texture (environnement) du terrain
     */
    void Controller::set(const QWOR::TexelGroup& pTexels)
    {
        if (pTexels.count() == 0)
        {
            return;
        }

        int32_t lNorth  = getWorldExtent().roundedTop()   * TILE::eFragmentCount;
        int32_t lWest   = getWorldExtent().roundedLeft()  * TILE::eFragmentCount;
        int32_t lWidth  = getWorldExtent().roundedWidth() * TILE::eFragmentCount;

        QWOR::TexelGroup    lValidTexels;

        for(unsigned long lTx = 0; lTx < pTexels.count(); ++lTx)
        {
            const QWOR::Texel&  lTexel      = pTexels[lTx];
            TILE::TexelData     lTexelData  = lTexel.data();

            if (!TILE::FragmentMgr::get()->isTexelInactive(lTexelData) && pthis->isEditable(lTexel))
            {
                lValidTexels.insertInArea(lTexel);
                for(uint32_t lRow = 0; lRow < QOUT::eTexelSize; ++lRow)
                {
                    for(uint32_t lCol = 0; lCol < QOUT::eTexelSize; ++lCol)
                    {
                        QWOR::Texel lSubTexel(lTexel.col() * QOUT::eTexelSize + lCol, lTexel.row() * QOUT::eTexelSize - lRow, lTexelData);

                        uint32_t    lOffset = ((lNorth - lSubTexel.row()) * lWidth + (lSubTexel.col() - lWest));

                        ASSERT_EX(lOffset < pthis->mWorldData->texelCount(), QOUT::kUnexistingTexel, return;)
                        lTexelData.fragId = pthis->mWorldData->texel(lOffset).fragId;
                        pthis->mWorldData->setTexel(lOffset, lTexelData);
                    }
                }
            }
        }

        // Les textures modifi�es sont imm�diatement mises � jour.
        updateTexture(lValidTexels.area());
    }


//------------------------------------------------------------------------------
//                     Controller : Mise � Jour des Donn�es
//------------------------------------------------------------------------------

    /*! Met � jour la g�om�trie du monde sur une zone donn�e.
        @param pArea Zone (en coordonn�es du monde) pour laquelle les g�om�tries doivent �tre mises � jour (pour tous les niveaux de
        d�tail)
     */
    void Controller::updateGeometry(const UTI::Rect& pArea)
    {
        for(unsigned long lLevel = 0; lLevel < detailLevelCount(); ++lLevel)
        {
            long    lGeometryLength = static_cast<long>(QOUT::kOutsideScale * QOUT::eGeometryDefaultLength) << lLevel;
            long    lNorth = area().roundedTop();
            long    lWest  = area().roundedLeft();
            // La marge suppl�mentaire (+ / - 2) est utilis�e pour �viter que coup de pinceau sur le bord
            // d'une des g�om�tries n'ouvre une br�che en ne demandant pas le rafra�chissement de la
            // tuile adjacente, qui a pourtant �t� modifi�e.
            long    lLeft   = std::max((pArea.roundedLeft() - lWest    - 2) / lGeometryLength, 0L);
            long    lTop    = std::max((lNorth - pArea.roundedTop()    - 2) / lGeometryLength, 0L);
            long    lRight  = std::min<long>((pArea.roundedRight() - lWest   + 2) / lGeometryLength, area().roundedWidth()  / lGeometryLength - 1);
            long    lBottom = std::min<long>((lNorth - pArea.roundedBottom() + 2) / lGeometryLength, area().roundedHeight() / lGeometryLength - 1);

            // D�termine les g�om�tries � mettre � jour.
            for(long lRow = lTop; lRow <= lBottom; ++lRow)
            {
                for(int lCol = lLeft; lCol <= lRight; ++lCol)
                {
                    pthis->update(pthis->mGeometries[lLevel][lRow][lCol]);
                }
            }
        }
    }


    /*! Met � jour la texture du monde sur une zone donn�e.
        @param pArea Zone (en coordonn�es texture - niveau 0) pour laquelle les textures doivent �tre mises � jour (pour tous les niveaux
        de d�tail)
     */
    void Controller::updateTexture(const UTI::Rect& pArea)
    {
        if (!pArea.isValid())
        {
            return;
        }

        for(unsigned long lLevel = 0; lLevel < detailLevelCount(); ++lLevel)
        {
            long    lTextureLength = static_cast<long>(QOUT::eGeometryDefaultLength * QOUT::eTexelPerTileSide) << lLevel;
            long    lNorth = textureExtent().roundedTop();
            long    lWest  = textureExtent().roundedLeft();

            long    lLeft   = (pArea.roundedLeft() - lWest   ) / lTextureLength;
            long    lTop    = (lNorth - pArea.roundedTop()   ) / lTextureLength;
            long    lRight  = (pArea.roundedRight() - lWest  ) / lTextureLength;
            long    lBottom = (lNorth - pArea.roundedBottom()) / lTextureLength;

            // Petit contr�le suppl�mentaire.
            lRight  = std::max(0L, std::min<long>(lRight,  (textureExtent().roundedWidth())  / lTextureLength));
            lBottom = std::max(0L, std::min<long>(lBottom, (textureExtent().roundedHeight()) / lTextureLength));

            for(long lRow = lTop; lRow <= lBottom; ++lRow)
            {
                for(int lCol = lLeft; lCol <= lRight; ++lCol)
                {
                    pthis->update(pthis->mTextures[lLevel][lRow][lCol], lLevel, lRow, lCol);
                }
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    UTI::Rect Controller::getWorldExtent() const
    {
        return area() * (k1F / QOUT::kOutsideScale);
    }


    /*!
     */
    UTI::Rect Controller::getTextureExtent() const
    {
        UTI::Rect  lExtent = getWorldExtent();

        return UTI::Rect(F32(lExtent.left()   * QOUT::eTexelPerTileSide),
                         F32(lExtent.top()    * QOUT::eTexelPerTileSide),
                         F32(lExtent.right()  * QOUT::eTexelPerTileSide),
                         F32(lExtent.bottom() * QOUT::eTexelPerTileSide));
    }


    /*!
     */
    int Controller::getType() const
    {
        return rTTI();
    }


    /*!
     */
    Controller::CliffBrushPtr Controller::getCliffBrush()
    {
        CliffBrushPtr  lBrush  = CliffBrushPtr(new QWOR::GenericCliffBrush<QWOR::HeightGroup, Controller>(pthis->mParent));

        const float kOneScaledMeter = 8.0F / TILE::kVerticalStep;

        lBrush->setLevelHeight(static_cast<QWOR::Height::TType>(kOneScaledMeter));
        lBrush->setMaximumAmplitude(static_cast<QWOR::Height::TType>(4 * kOneScaledMeter));
        lBrush->setExtrema(static_cast<QWOR::Height::TType>(-550.0F  * kOneScaledMeter / TILE::eEdgeSize),
                           static_cast<QWOR::Height::TType>(1600.0F  * kOneScaledMeter / TILE::eEdgeSize));
        lBrush->setPlaneScale(QOUT::kOutsideScale);

        return lBrush;
    }


    /*!
     */
    Controller::SlideBrushPtr Controller::getSlideBrush()
    {
        SlideBrushPtr  lBrush  = SlideBrushPtr(new QWOR::GenericSlideBrush<QWOR::HeightGroup, Controller>(pthis->mParent));

        const float kOneScaledMeter = 8.0F / TILE::kVerticalStep;

        lBrush->setExtrema(static_cast<QWOR::Height::TType>(-550.0F * kOneScaledMeter / TILE::eEdgeSize),
                           static_cast<QWOR::Height::TType>(1600.0F * kOneScaledMeter / TILE::eEdgeSize));
        lBrush->setPlaneScale(QOUT::kOutsideScale);

        return lBrush;
    }


    /*!
     */
    Controller::EnvironmentBrushPtr Controller::getEnvironmentBrush()
    {
        EnvironmentBrushPtr    lBrush  = EnvironmentBrushPtr(new QWOR::GenericEnvironmentBrush<QWOR::TexelGroup, Controller>(pthis->mParent));

        lBrush->setPlaneScale(QOUT::eTexelSize);
        lBrush->setBlending(TILE::eFull);

        return lBrush;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    EDIT::Subject* Controller::asSubject()
    {
        return &(*pthis);
    }


    /*!
     */
    QString Controller::getExtension() const
    {
        return "out";
    }


    /*!
     */
    void Controller::doTakeOver()
    {
        pthis->updateTileKeys();
        updateGeometry(extent());
        updateTexture(extent());
    }


    /*!
     */
    void Controller::doSave() const
    {
        pthis->updateTileKeys();
        edited().lock()->saveSelf();
    }


    /*!
     */
    Controller::RendererPtr Controller::getRenderer()
    {
        return pthis->mRenderer;
    }
}
