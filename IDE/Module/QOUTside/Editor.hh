/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_EDITOR_HH
#define QOUT_EDITOR_HH

#include "QOUTside.hh"

/*! @file IDE/Module/QOUTside/Editor.hh
    @brief En-t�te de la classe QOUT::Editor.
    @author @ref Guillaume_Terrissol
    @date 22 Juillet 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Synchronize.hh"
#include "CORE/Translate.hh"
#include "EDITion/Observer.hh"
#include "Module/Editor.hh"
#include "OUTside/OUTside.hh"

namespace QOUT
{
//------------------------------------------------------------------------------
//                              Editeur d'Univers
//------------------------------------------------------------------------------

    /*! @brief Editeur de monde ext�rieur.
        @version 0.5

        Interface Qt d'�dition de monde ext�rieur.
     */
    class Editor : public Module::Editor, public EDIT::Observer, public CORE::Translate<Editor>, public CORE::Synchronized
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    Editor(OSGi::Context* pContext);                            //!< Constructeur par d�faut.
virtual             ~Editor();                                                  //!< Destructeur.
        //@}

    private slots:
        //! @name Slots:
        //@{
        void        modeSelected(int pID);                                      //!< 
        void        arrowSelected(int pID);                                     //!< 
        //@}

    private:
        //! @name Pertinence du sujet
        //@{
        bool        canManage(EDIT::Controller::Ptr pDatum) const override;     //!< Possibilit� de gestion d'un sujet.
        void        manage(EDIT::Controller::Ptr pDatum) override;              //!< Gestion effective d'un sujet.
        void        unmanage() override;                                        //!< Arr�t de la gestion du sujet.
        void        manageNone() override;                                      //!< Plus aucun sujet � g�rer.
        //@}
        //! @name Ecoute du sujet
        //@{
        void        listen(const EDIT::Answer* pAnswer) override;               //!< Ecoute effective de la cible.
        //@}
        //! @name Changement de langue
        //@{
        auto        buildTranslater() -> CORE::Translated::Updater override;    //!< Traduction de l'IHM.
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}

#endif  // De QOUT_EDITOR_HH
