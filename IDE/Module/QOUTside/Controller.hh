/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_CONTROLLER_HH
#define QOUT_CONTROLLER_HH

#include "QOUTside.hh"

/*! @file IDE/Module/QOUTside/Controller.hh
    @brief En-t�te de la classe QOUT::Controller.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <vector>

#include "Module/QT3D/Controller.hh"
#include "Module/QT3D/GameEntityService.hh"

#include "GEOmetry/BaseGeometry.hh"
#include "OUTside/World.hh"
#include "Module/QWORld/Brush.hh"
#include "Module/QWORld/Data.hh"
#include "STL/Shared.hh"
#include "UTIlity/UTIlity.hh"

namespace OUT
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Contr�leur de monde ext�rieur.
        @version 0.7

        Les m�thodes d'�dition de haut niveau n'ont pas vraiment de raison pour se trouver dans le moteur. Sans compter que les informations
        suppl�mentaires � sauver ne le seront qu'en mode �diteur. J'ai donc pr�f�r� cr�er cette classe ici afin de ne laisser dans
        OUT::World que la manipulation directe des instances de TILE::Tile.
     */
    class Controller
        : public STL::enable_shared_from_this<Controller>
        , public QT3D::GenericController<World>
        , public QWOR::Brushable
    {
        BEFRIEND_STD_ALLOCATOR()
    public:
        //! @name Types de pointeur
        //@{
        using   ConstAppearancePtr  = std::weak_ptr<APP::Appearance const>;         //!< Pointeur (constant) sur apparence.
        using   AppearancePtr       = std::weak_ptr<APP::Appearance>;               //!< Pointeur sur apparence.
        using   ControllerPtr       = QT3D::GameEntityService::ControllerPtr;       //!< Pointeur sur contr�leur.
        using   EntityPtr           = QT3D::GameEntityService::EntityPtr;           //!< Pointeur sur entit�.
        //@}
        //! @name Cr�ation
        //@{
 static Ptr                 make(EntityPtr pEntity, OSGi::Context* pContext);       //!< Cr�ation.
        //@}
        QWOR::Data::Ptr     data();                                                 //!< Donn�es communes.
        void                deliverTiles();                                         //!< Livraison des tuiles stream�es.
        //! @name Etendue du monde
        //@{
        const UTI::Rect&   extent() const;                                          //!< ... en tuiles.
        UTI::Rect          area() const;                                            //!< ... en coordonn�es globales.
        //@}
        //! @name Acc�s aux donn�es (lecture)
        //@{
        void                get(QWOR::HeightGroup& pHeights) const;                 //!< Acc�s aux �l�vations.
        void                get(QWOR::TexelGroup&  pTexels) const;                  //!< Acc�s aux texels.
        //@}
        //! @name Niveau de d�tail
        //@{
        void                setCurrentDetailLevel(unsigned long pLevel);            //!< Activation d'un niveau de d�tail.
        unsigned long       currentDetailLevel() const;                             //!< Niveau de d�tail actuel.
        unsigned long       detailLevelCount() const;                               //!< Nombre de niveaux de d�tail disponibles.
        //@}
        //! @name Rendu
        //@{
        void                getAppearances(std::vector<AppearancePtr>& pVisibles,
                                           const UTI::Rect& pVisibleArea) const;    //!< R�cup�ration des apparences.
        //@}
    protected:
        //! @name Constructeurs & destructeur
        //@{
                            Controller(EditedPtr pWorld, OSGi::Context* pContext);  //!< Constructeur.
virtual                     ~Controller();                                          //!< Destructeur.
        //@}
    private:
        //! @name Etendue du monde
        //@{
        void                setExtent(const UTI::Rect& pNewExtent);                 //!< Progression du monde.
        void                extend(const UTI::Rect& pToExtent);                     //!< Extension du monde.
        void                reduce(const UTI::Rect& pToExtent);                     //!< R�duction du monde.
        //@}
        //! @name Acc�s aux donn�es (�criture)
        //@{
        void                set(const QWOR::HeightGroup& pHeights);                 //!< Edition des �l�vations
        void                set(const QWOR::TexelGroup&  pTexels);                  //!< Edition des texels.
        //@}
        //! @name Mise � jour
        //@{
        void                updateGeometry(const UTI::Rect& pArea);                 //!< ... de la g�om�trie
        void                updateTexture(const UTI::Rect& pArea);                  //!< ... de la texture.
        //@}
        //! @name Impl�mentation de Brushable
        //@{
        UTI::Rect           getWorldExtent() const override;                        //!< 
        UTI::Rect           getTextureExtent() const override;                      //!< 
        int                 getType() const override;                               //!< 
        CliffBrushPtr       getCliffBrush() override;                               //!< 
        SlideBrushPtr       getSlideBrush() override;                               //!< 
        EnvironmentBrushPtr getEnvironmentBrush() override;                         //!< 
        //@}
        EDIT::Subject*      asSubject() override;                                   //!< R�cup�ration du sujet d'�dition.
        QString             getExtension() const override;                          //!< Extension associ�e � l'objet contr�l�
        void                doTakeOver() override;                                  //!< Reprise en main du contr�leur
        void                doSave() const override;                                //!< Sauvegarde de l'�tat de l'objet contr�l�.
        RendererPtr         getRenderer() override;                                 //!< R�cup�ration du rendrerer du sujet �dit�.

        FORBID_COPY(Controller)
        FREE_PIMPL()
    };
}

#endif  // De QOUT_CONTROLLER_HH
