/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOUT_QOUTSIDE_HH
#define QOUT_QOUTSIDE_HH

/*! @file IDE/Module/QOUTside/QOUTside.hh
    @brief Pr�-d�clarations du module @ref QOUTside.
    @author @ref Guillaume_Terrissol
    @date 1er D�cembre 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QOUT
{
    class Editor;
}

#endif  // De QOUT_QOUTSIDE_HH
