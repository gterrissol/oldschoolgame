<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Editor</name>
    <message>
        <source>Grow</source>
        <translation>Expansion</translation>
    </message>
    <message>
        <source>Shrink</source>
        <translation>Réduction</translation>
    </message>
    <message>
        <source>EAST</source>
        <oldsource>EST</oldsource>
        <translation>EST</translation>
    </message>
    <message>
        <source>WEST</source>
        <oldsource>OUEST</oldsource>
        <translation>OUEST</translation>
    </message>
    <message>
        <source>NORTH</source>
        <oldsource>NORD</oldsource>
        <translation>NORD</translation>
    </message>
    <message>
        <source>SOUTH</source>
        <translation>SUD</translation>
    </message>
</context>
</TS>
