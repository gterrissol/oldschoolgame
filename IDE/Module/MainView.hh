/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MODULE_MAINVIEW_HH
#define MODULE_MAINVIEW_HH

/*! @file IDE/Module/MainView.hh
    @brief En-t�te de la classe Module::MainView.
    @author @ref Guillaume_Terrissol
    @date 25 Novembre 2014 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

class QWidget;

namespace Module
{
    /*! @brief Vue principale sur le monde.
        @version 0.5

        La vue sur le monde est affich�e dans un widget cr�� dans un sous-module.@n
        Cette classe est un conteneur permettat l'acc�s � ce widget (@sa worldWiew()).
     */
    class MainView
    {
    public:
        //! @name Interface.
        //@{
virtual             ~MainView();                            //!< Destructeur.

        QWidget*    worldView() const;                      //!< Acc�s � la vue du monde
        //@}
    private:
        //! @name Impl�mentation.
        //@{
virtual QWidget*    getWorldView() const = 0;               //!< Acc�s � la vue du monde
        //@}
    };
}

#endif  // De MODULE_MAINVIEW_HH
