/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_EDITOR_HH
#define EDIT_EDITOR_HH

/*! @file IDE/Module/Editor.hh
    @brief En-t�te de la classe EDIT::QEditor.
    @author @ref Guillaume_Terrissol
    @date 15 Novembre 2005 - 31 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QTabWidget>
#include <QWidget>

#include "EDITion/Controller.hh"

namespace Module
{
    /*! @brief Outil/�diteur.
        @version 0.4

     */
    class Editor : public QWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    Editor();                                               //!< Constructeur par d�faut.
virtual             ~Editor();                                              //!< Destructeur.
        //@}
        QWidget*    worldView() const;                                      //!< Widget de vue sur le monde..

    public slots:

        void        applyOn(EDIT::Controller::Ptr pController);             //!< Ecoute d'un sujet.
        void        setWorldView(QWidget* pView);                           //!< D�finition de la vue sur le monde.

    private:
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(EDIT::Controller::Ptr controller) const = 0;  //!< Possibilit� de gestion d'un sujet.
virtual void        manage(EDIT::Controller::Ptr controller) = 0;           //!< Gestion effective d'un sujet.
virtual void        unmanage();                                             //!< Arr�t de la gestion du sujet.
virtual void        manageNone() = 0;                                       //!< Arr�t de la gestion du sujet.
        //@}
        QWidget*    mWorldView;                                             //!< Widget vur sur le monde.
    };
}

#endif  // De EDIT_EDITOR_HH
