/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "EditSurfaceMode.hh"

/*! @file IDE/Module/QWORld/EditSurfaceMode.cc
    @brief M�thodes (non-inline) de la classe QWORld::EditSurfaceMode.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2005 - 3 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include <QPoint>
#include <QPointF>
#include <QString>

#include "MATH/V3.hh"
#include "UTIlity/Rect.hh"

#include "Brush.hh"
#include "Const.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @copydoc QT3D::Mode::Mode(WidgetPtr pWindow, QString pName)
     */
    EditSurfaceMode::EditSurfaceMode(WidgetPtr pWindow, QString pName)
        : QT3D::Mode(pWindow, pName)
        , mBrush()
        , mScale(k1F)
        , mActivePen(false)
    { }


    /*! Destructeur.
     */
    EditSurfaceMode::~EditSurfaceMode() { }


//------------------------------------------------------------------------------
//                                 Configuration
//------------------------------------------------------------------------------

    /*! @param pBrushToUse Pinceau � utiliser pour �diter la surface du sujet actuellement �dit�
     */
    void EditSurfaceMode::setBrush(BrushPtr pBrushToUse)
    {
        mBrush = pBrushToUse;
        if (!mBrush.expired())
        {
            mBrush.lock()->setVisible(isActive());
        }
        updateWindow();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Active le "mouse tracking" sur la fen�tre (pour suivre le pinceau m�me hors application).
     */
    void EditSurfaceMode::onEnter()
    {
        if (!mBrush.expired())
        {
            mBrush.lock()->setVisible(true);
            updateWindow();
        }
    }


    /*! D�sactive le "mouse tracking" sur la fen�tre.
     */
    void EditSurfaceMode::onExit()
    {
        if (!mBrush.expired())
        {
            mBrush.lock()->setVisible(false);
            updateWindow();
        }
    }


    /*! Force la mise � jour du pinceau.
     */
    void EditSurfaceMode::doUpdate()
    {
        doDrag();
    }


//------------------------------------------------------------------------------
//                        R�actions aux Ev�nement Souris
//------------------------------------------------------------------------------

    /*! Applique le pinceau.
     */
    void EditSurfaceMode::press()
    {
        if  (auto lBrush = mBrush.lock())
        {
            MATH::V3    lPickedPoint = pickPoint(currentMousePosition());   // Coordonn�e monde.
            float       lScale       = lBrush->planeScale();

            mActivePen = lBrush->begin(floor(lPickedPoint.x / lScale), floor(lPickedPoint.y / lScale) + 1.0F);

            // Rafra�chit la fen�tre.
            updateWindow();
        }
    }


    /*! Rel�ve le pinceau.
     */
    void EditSurfaceMode::release()
    {
        if  (!mBrush.expired())
        {
            // "Eteint" le "pinceau".
            mActivePen = false;
        }
    }


    /*! A voir.
     */
    void EditSurfaceMode::doWheel()
    {
        // Rien pour l'instant.
    }


    /*! D�place le pinceau (appliqu� ou non).
     */
    void EditSurfaceMode::doDrag()
    {
        if  (auto lBrush = mBrush.lock())
        {
            MATH::V3    lPickedPoint = pickPoint(currentMousePosition());
            float       lScale       = lBrush->planeScale();
            QPointF     lPoint(floor(lPickedPoint.x / lScale), floor(lPickedPoint.y / lScale) + 1.0F);

            if (mActivePen)
            {
                lBrush->apply(lPoint.x(), lPoint.y());
            }
            lBrush->moveTo(lPoint.x(), lPoint.y());

            updateWindow();
        }
    }
}
