/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Brush.hh"

/*! @file IDE/Module/QWORld/Brush.cc
    @brief M�thodes (non-inline) des classes QWOR::Brush, QWOR::BrushGeometry, QWOR::ElevationBrush,
    QWOR::CliffBrush, QWOR::SlideBrush, QWOR::EnvironmentBrush, QWOR::DropBrush & QWOR::Brushable.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <limits>
#include <numeric>

#include "APPearance/BaseAppearance.hh"
#include "GEOmetry/Fields.hh"
#include "GEOmetry/List.hh"
#include "GEOmetry/List.tcc"
#include "MATH/Float.hh"

#include "ErrMsg.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                      Poids des Masques pour les Pinceaux
//------------------------------------------------------------------------------

    // B => Brush.
    // M => Mask (apparence de pinceau)

    Weights gDefautWeights =
    {
        // 1 x 1 : B.
        {
            { 1.0F }
        },
        // 2 x 2 : B + M.
        {
            { 0.631F, 0.631F },
            { 0.631F, 0.631F }
        },
        // 3 x 3 : B + M.
        {
            { 0.565F, 0.898F, 0.565F },
            { 0.914F, 1.0F,   0.914F },
            { 0.565F, 0.898F, 0.565F }
        },
        // 4 x 4 : M.
        {
            { 0.145F, 0.396F, 0.396F, 0.145F },
            { 0.396F, 0.898F, 0.898F, 0.396F },
            { 0.396F, 0.898F, 0.898F, 0.396F },
            { 0.145F, 0.396F, 0.396F, 0.145F }
        },
        // 5 x 5 : B + M.
        {
            { 0.235F, 0.651F, 0.761F, 0.651F, 0.235F },
            { 0.698F, 0.953F, 0.988F, 0.953F, 0.698F },
            { 0.953F, 0.992F, 1.0F,   0.992F, 0.953F },
            { 0.698F, 0.953F, 0.988F, 0.953F, 0.698F },
            { 0.235F, 0.651F, 0.761F, 0.651F, 0.235F }
        },
        // 6 x 6 : M.
        {
            { 0.208F, 0.624F, 0.856F, 0.856F, 0.624F, 0.208F },
            { 0.365F, 0.973F, 0.996F, 0.996F, 0.973F, 0.365F },
            { 0.922F, 0.996F, 1.0F,   1.0F,   0.996F, 0.922F },
            { 0.922F, 0.996F, 1.0F,   1.0F,   0.996F, 0.922F },
            { 0.365F, 0.973F, 0.996F, 0.996F, 0.973F, 0.365F },
            { 0.208F, 0.624F, 0.856F, 0.856F, 0.624F, 0.208F }
        },
        // 7 x 7 : M.
        {
            { 0.0F,   0.392F, 0.835F, 0.98F,  0.835F, 0.392F, 0.0F   },
            { 0.392F, 0.918F, 1.0F,   1.0F,   1.0F,   0.918F, 0.392F },
            { 0.835F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.835F },
            { 0.98,   1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.98F  },
            { 0.835F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.835F },
            { 0.392F, 0.918F, 1.0F,   1.0F,   1.0F,   0.918F, 0.392F },
            { 0.0F,   0.392F, 0.835F, 0.98F,  0.835F, 0.392F, 0.0F   },
        },
        // 8 x 8 : B.
        {
            { 0.000F, 0.098F, 0.306F, 0.459F, 0.459F, 0.306F, 0.098F, 0.000F },
            { 0.098F, 0.447F, 0.808F, 0.961F, 0.961F, 0.808F, 0.447F, 0.098F },
            { 0.306F, 0.808F, 1.0F,   1.0F,   1.0F,   1.0F,   0.808F, 0.306F },
            { 0.459F, 0.961F, 1.0F,   1.0F,   1.0F,   1.0F,   0.961F, 0.459F },
            { 0.459F, 0.961F, 1.0F,   1.0F,   1.0F,   1.0F,   0.961F, 0.459F },
            { 0.306F, 0.808F, 1.0F,   1.0F,   1.0F,   1.0F,   0.808F, 0.306F },
            { 0.098F, 0.447F, 0.808F, 0.961F, 0.961F, 0.808F, 0.447F, 0.098F },
            { 0.000F, 0.098F, 0.306F, 0.459F, 0.459F, 0.306F, 0.098F, 0.000F }
        },
        // 9 x 9 : M.
        {
            { 0.0F,   0.031F, 0.18F,  0.373F, 0.463F, 0.373F, 0.18F,  0.031F, 0.0F   },
            { 0.031F, 0.243F, 0.608F, 0.875F, 0.965F, 0.875F, 0.608F, 0.243F, 0.031F },
            { 0.18F,  0.608F, 0.914F, 1.0F,   1.0F,   1.0F,   0.914F, 0.608F, 0.18F  },
            { 0.373F, 0.875F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.875F, 0.373F },
            { 0.463F, 0.965F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.965F, 0.463F },
            { 0.373F, 0.875F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.875F, 0.373F },
            { 0.18F,  0.608F, 0.914F, 1.0F,   1.0F,   1.0F,   0.914F, 0.608F, 0.18F  },
            { 0.031F, 0.243F, 0.608F, 0.875F, 0.965F, 0.875F, 0.608F, 0.243F, 0.031F },
            { 0.0F,   0.031F, 0.18F,  0.373F, 0.463F, 0.373F, 0.18F,  0.031F, 0.0F   },
        },
        // 10 x 10 : M.
        {
            { 0.0F,   0.0F,   0.133F, 0.353F, 0.471F, 0.471F, 0.353F, 0.133F, 0.0F,   0.0F   },
            { 0.0F,   0.188F, 0.573F, 0.855F, 0.973F, 0.973F, 0.855F, 0.573F, 0.188F, 0.0F   },
            { 0.133F, 0.573F, 0.941F, 1.0F,   1.0F,   1.0F,   1.0F,   0.941F, 0.573F, 0.133F },
            { 0.353F, 0.855F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.855F, 0.353F },
            { 0.471F, 0.973F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.973F, 0.471F },
            { 0.471F, 0.973F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.973F, 0.471F },
            { 0.353F, 0.855F, 1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   1.0F,   0.855F, 0.353F },
            { 0.133F, 0.573F, 0.941F, 1.0F,   1.0F,   1.0F,   1.0F,   0.941F, 0.573F, 0.133F },
            { 0.0F,   0.188F, 0.573F, 0.855F, 0.973F, 0.973F, 0.855F, 0.573F, 0.188F, 0.0F   },
            { 0.0F,   0.0F,   0.133F, 0.353F, 0.471F, 0.471F, 0.353F, 0.133F, 0.0F,   0.0F   }
        }
    };


namespace
{
//------------------------------------------------------------------------------
//                              Fonction Assistante
//------------------------------------------------------------------------------

    /*! Op�rateur de comparaison de MATH::V4 sp�cifique.
        Les vecteurs 4D utilis�s dans module (pour les pinceaux) ne sont pas ordonn�s de fa�on standard;
        il a donc fallu coder un op�rateur de comparaison sp�cifique.
        @param pA Premier vecteur � comparer
        @param pB Second vecteur � comparer
        @return VRAI si pA < pB (selon l'ordre de QWOR), FAUX sinon
     */
    bool compareVectorsForBrushGeometry(const MATH::V4& pA, const MATH::V4& pB)
    {
        if      (pB.y < pA.y)
        {
            return true;
        }
        else if (pA.y < pB.y)
        {
            return false;
        }
        else
        {
            if      (pA.x < pB.x)
            {
                return true;
            }
            else if (pB.x < pA.x)
            {
                return false;
            }
            else
            {
                if      (pA.z < pB.z)
                {
                    return true;
                }
                else if (pB.z < pA.z)
                {
                    return false;
                }
                else
                {
                    return (pA.w < pB.w);
                }
            }
        }
    }
}


//------------------------------------------------------------------------------
//                            G�om�tries des pinceaux
//------------------------------------------------------------------------------

    /*! @brief Repr�sentation des pinceaux.
        @version 0.5
     */
    class BrushGeometry : public GEO::Geometry
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                BrushGeometry(Brush* pBrush);                   //!< Constructeur.
virtual         ~BrushGeometry();                               //!< Destructeur.
        //@}
        void    update(const Brush::ModelVertices& pVertices);  //!< Mise � jour.


    private:

        enum
        {
            eMaxMaskSize = Brush::eMaxBrushSize + 2             //!< Taille maximale de le g�om�trie.
        };
        Brush* mBrush;                                          //!< Pinceau auquel dont cette instance est l'apparence.
    };


//------------------------------------------------------------------------------
//             G�om�tries des pinceaux : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    BrushGeometry::BrushGeometry(Brush* pBrush)
        : mBrush(pBrush)
    {
        setFlags(    I32(GEO::eVertex3 | GEO::eRGBA | GEO::eTriangles));
        setAutoFlags(I32(GEO::eVertex3 | GEO::eRGBA | GEO::eTriangles));

        // Vertex.
        for(unsigned short lLi = 0; lLi <= eMaxMaskSize; ++lLi)
        {
            for(unsigned short lCo = 0; lCo <= eMaxMaskSize; ++lCo)
            {
                // Initialisation quelconque.
                F32 lVertex[3]  = { F32(lCo - eMaxMaskSize / 2), F32(eMaxMaskSize / 2 - lLi), k0F };
                vertices3().push_back(GEO::Vertex3(lVertex));
                F32 lRGBA[4]    = { F32(0.22F), F32(0.36F), F32(0.60F), k0F };
                colors4().push_back(GEO::RGBA(lRGBA));
            }
        }
        setVertexCount(U16(vertices3().size()));

        // Triangles.
        TriangleListPtr lCustomTriangleList = STL::makeShared<TriangleListPtr::element_type>(U16(2 * eMaxMaskSize * eMaxMaskSize));
        // D�finition simple : liste de "bandes" de triangles.
        for(unsigned long lLi = 0; lLi < eMaxMaskSize; ++lLi)
        {
            unsigned long   lUp   = lLi * (eMaxMaskSize + 1);
            unsigned long   lDown = lUp + (eMaxMaskSize + 1);

            for(unsigned long lCo = 0; lCo < eMaxMaskSize; ++lCo)
            {
                // Ces triangles doivent �tre les m�mes que ceux des tuiles.
                lCustomTriangleList->push(GEO::Triangle(U16(lUp   + lCo),     U16(lDown + lCo),     U16(lDown + lCo + 1)));
                lCustomTriangleList->push(GEO::Triangle(U16(lDown + lCo + 1), U16(lUp   + lCo + 1), U16(lUp   + lCo)));
            }
        }

        cloneTriangleList(std::const_pointer_cast<TriangleListPtr::element_type const>(lCustomTriangleList));
    }


    /*! Destructeur.
     */
    BrushGeometry::~BrushGeometry() { }


//------------------------------------------------------------------------------
//                     G�om�tries des pinceaux : Mise � Jour
//------------------------------------------------------------------------------

    /*! Met � jour la g�om�trie.
        @param pVertices Groupe d'�l�vations au-dessus desquelles positionner le pinceau
     */
    void BrushGeometry::update(const Brush::ModelVertices& pVertices)
    {
        // Si le travail a �t� bien fait jusqu'ici, les �l�vations sont class�es de gauche � droite, et de bas en haut.
        GEO::Vertex3    lLastVertex;
        long            lRow    = 0;
        for(auto lVertexI = pVertices.begin(); (lVertexI != pVertices.end()) && (lRow <= eMaxMaskSize); ++lRow)
        {
            long    lCol    = 0;

            for(F32 lCurrentRow = (*lVertexI)[1]; (lVertexI != pVertices.end()) && ((*lVertexI)[1] == lCurrentRow); ++lVertexI, ++lCol)
            {
                unsigned long   lOffset = lRow * (eMaxMaskSize + 1) + lCol;
                GEO::Vertex3&   lVertex = vertices3()[lOffset];
                lVertex[0]  =     (*lVertexI)[0];
                lVertex[1]  =     (*lVertexI)[1];
                lVertex[2]  = F32((*lVertexI)[2] * TILE::kVerticalStep + 0.05F);
                GEO::RGBA&      lRGBA   = colors4()[lOffset];
                lRGBA[3]    = F32((*lVertexI)[3] * (*lVertexI)[3] * 0.5F);
                lLastVertex = lVertex;
            }
            for(; lCol <= eMaxMaskSize; ++lCol)
            {
                unsigned long   lOffset = lRow * (eMaxMaskSize + 1) + lCol;
                GEO::Vertex3&   lVertex = vertices3()[lOffset];
                lVertex = lLastVertex;
                GEO::RGBA&      lRGBA   = colors4()[lOffset];
                lRGBA[3] = k0F;
            }
        }
        for(; lRow <= eMaxMaskSize; ++lRow)
        {
            for(long lCol = 0; lCol <= eMaxMaskSize; ++lCol)
            {
                unsigned long   lOffset = lRow * (eMaxMaskSize + 1) + lCol;
                GEO::Vertex3&   lVertex = vertices3()[lOffset];
                lVertex = lLastVertex;
                GEO::RGBA&      lRGBA   = colors4()[lOffset];
                lRGBA[3] = k0F;
            }
        }
    }


//------------------------------------------------------------------------------
//                     Pinceau : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Brush::Brush()
        : mWorkingExtent()
        , mShape(eSquare)
        , mScale(k1F)
        , mSize(eMinBrushSize)
        , mGeometry(STL::makeShared<BrushGeometry>(this))
        , mTexture(STL::makeShared<MAT::Material>())
        , mModel(STL::makeShared<APP::Appearance>(mGeometry, mTexture))
        , mVisible(true)
    { }


    /*! Destructeur.
     */
    Brush::~Brush() { }


//------------------------------------------------------------------------------
//                      Pinceau : Configuration "g�n�rique"
//------------------------------------------------------------------------------

    /*! Change la forme du pinceau.
        @param pBrushShape Nouvelle forme du pinceau
     */
    void Brush::setShape(EShape pBrushShape)
    {
        mShape = pBrushShape;
    }


    /*! Change la taille du pinceau.
        @param pSideLength Nouvelle taille du pinceau (comprise entre Brush::eMinBrushSize et Brush::eMaxBrushSize)
     */
    void Brush::setSize(unsigned short pSideLength)
    {
        mSize = pSideLength;
    }


    /*! Permet de modifier l'�chelle d'un pinceau pour l'appliquer sur une donn�e dont l'�chelle est elle-m�me modifi�e (m�me valeur).
        @param pScale Echelle (non-nulle !) � appliquer au pinceau
     */
    void Brush::setPlaneScale(float pScale)
    {
        if (!MATH::isNullEpsilon(F32(pScale)))
        {
            mScale = pScale;
        }
    }


    /*! @return L'�chelle appliqu�e au pinceau
        @sa setPlaneScale(float pScale)
     */
    float Brush::planeScale() const
    {
        return mScale;
    }


    /*!
     */
    void Brush::setExtent(const UTI::Rect& pExtent)
    {
        mWorkingExtent = pExtent;
    }


    /*!
     */
    const UTI::Rect& Brush::workingExtent() const
    {
        return mWorkingExtent;
    }


//------------------------------------------------------------------------------
//                              Pinceau : Apparence
//------------------------------------------------------------------------------

    /*! [D�s]Active le rendu de l'apparence du pinceau.
        @param pStatus VRAI pour afficher l'apparence du pinceau, FAUX pour la cacher
     */
    void Brush::setVisible(bool pStatus)
    {
        mVisible = pStatus;
    }


    /*! @return VRAI si l'apparence du pinceau doit �tre affich�e, FAUX sinon
     */
    bool Brush::isVisible() const
    {
        return mVisible;
    }


    /*! R�cup�re le mod�le 3D associ� au pinceau.
        @return L'apparence du pinceau
     */
    Brush::AppearancePtr Brush::retrieveModel() const
    {
        return mVisible ? mModel : nullptr;
    }


    /*! Mise � jour de l'"apparence" du pinceau.
        @param pVertices Groupe de vertex (et poids associ�s) au-dessus desquels placer le pinceau
     */
    void Brush::updateModel(ModelVertices& pVertices)
    {
        std::sort(pVertices.begin(), pVertices.end(), compareVectorsForBrushGeometry);

        std::dynamic_pointer_cast<BrushGeometry>(mGeometry)->update(pVertices);
    }


//------------------------------------------------------------------------------
//                      Pinceau : Configuration "g�n�rique"
//------------------------------------------------------------------------------

    /*! @return La forme du pinceau
     */
    Brush::EShape Brush::shape() const
    {
        return mShape;
    }


    /*! @return La taille du pinceau (diam�tre ou c�t�, selon EShape())
     */
    unsigned short Brush::size() const
    {
        return mSize;
    }


    /*! @copydoc Brush::forwardState(Ptr pFrom)
     */
    void Brush::doForwardState(Ptr) { }


//------------------------------------------------------------------------------
//     Interfaces des Pinceaux : Constructeurs, Destructeurs & Configuration
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    ElevationBrush::ElevationBrush()
        : mMinimum(std::numeric_limits<TType>::min())
        , mMaximum(std::numeric_limits<TType>::max())
    { }


    /*! Destructeur.
     */
    ElevationBrush::~ElevationBrush() { }


    /*! Permet de d�finir dans quelle plage de valeurs les donn�es doivent se trouver.
        @param pMininum Valeur minimale pour les donn�es
        @param pMaximum Valeur maximale pour les donn�es
     */
    void ElevationBrush::setExtrema(TType pMininum, TType pMaximum)
    {
        ASSERT_EX(pMininum < pMaximum, kInvalidExtrema, return;)

        mMinimum = pMininum;
        mMaximum = pMaximum;
    }


    /*! Constructeur.
     */
    CliffBrush::CliffBrush()
        : mCliffType(eSameLevel)
        , mOffset(0)
        , mOneLevelHeight(1)
        , mMaximumAmplitude(1)
    { }


    /*! Destructeur.
     */
    CliffBrush::~CliffBrush() { }


    /*!  D�finit le type de falaise � "cr�er". Il s'agit en fait de 'creuser' ou '�lever' une zone, de mani�re homog�ne, jusqu'� un niveau
        donn�.
        @param pType Identifie le type de falaise (voir ECliffs)
     */
    void CliffBrush::setCliff(ECliffs pType)
    {
        mCliffType = pType;

        switch(mCliffType)
        {
            case eDecreaseTwo:
                mOffset = TType(- 2 * mOneLevelHeight);
                break;
            case eDecreaseOne:
                mOffset = TType(- 1 * mOneLevelHeight);
                break;
            case eSameLevel:
                mOffset = TType(  0);
                break;
            case eIncreaseOne:
                mOffset = TType(+ 1 * mOneLevelHeight);
                break;
            case eIncreaseTwo:
                mOffset = TType(+ 2 * mOneLevelHeight);
                break;
            default:
                break;
        }
    }


    /*! Ce pinceau peut cr�er des falaises (�lever ou rabaisser des �l�vations de terrain par niveau). Mais � quelle hauteur un niveau
        correspond-il ? C'est � cette m�thode de le d�finir.
        @param pHeight Altitude
        @note Pour �viter certains comportements incoh�rents, il vaut mieux toujours utiliser la m�me valeur pour une instanciation avec les
        m�mes param�tres template (j'aurais pu utiliser plut�t une constante � sp�cialiser, mais �a me paraissait un peu lourd)
     */
    void CliffBrush::setLevelHeight(TType pHeight)
    {
        mOneLevelHeight = pHeight;
    }


    /*! Ce pinceau peut cr�er des falaises (�lever ou rabaisser des �l�vations de terrain par niveau). Afin de ne pas avoir de terrain trop
        d�lirant (entre autres), il �tait n�cessaire d'introduire une contrainte.
        @param pMaxAmplitude Diff�rence d'amplitude maximale entre deux �l�vations adjacentes
     */
    void CliffBrush::setMaximumAmplitude(TType pMaxAmplitude)
    {
        mMaximumAmplitude = pMaxAmplitude;
    }


    /*! Constructeur.
     */
    SlideBrush::SlideBrush()
        : mSmoothType(eSmooth)
    { }


    /*! Destructeur.
     */
    SlideBrush::~SlideBrush() { }


    /*! D�finit le type de modification � apporter aux donn�es.
     */
    void SlideBrush::setSmoothing(ESmoothing pType)
    {
        mSmoothType = pType;
    }


    /*! Constructeur.
     */
    EnvironmentBrush::EnvironmentBrush()
        : mTilingId{}
    { }


    /*! Destructeur.
     */
    EnvironmentBrush::~EnvironmentBrush() { }


    /*! Choix d'un Tiling.
        @param pInfo Identifie le tiling � utiliser pour peindre sur l'objet �dit�.
     */
    void EnvironmentBrush::setTilingId(TILE::TilingId pId)
    {
        mTilingId = pId;
    }


    /*!
     */
    void EnvironmentBrush::setBlending(TILE::EBlending pBlending)
    {
        mBlending = pBlending;
    }


    /*! Constructeur.
     */
    DropBrush::DropBrush()
        : mDropType(eRegular)
    {
        setVisible(false);
    }


    /*! Destructeur.
     */
    DropBrush::~DropBrush() { }


    /*! D�finit le type de modification � apporter aux donn�es.
     */
    void DropBrush::setType(EType pType)
    {
        mDropType = pType;
    }


    /*!
     */
    void DropBrush::setModel(AppearancePtr pModel)
    {
        ASSERT_EX(pModel != nullptr, kModelMustExist, return;)

        mModel = pModel;
    }


    /*! Commence � appliquer le pinceau sur une zone.<br>
        Les coordonn�es et la hauteur sous le pinceau sont m�moris�es : certaines 'techniques de peinture' requi�rent ces informations.
        @param pX Abscisse du coup de pinceau
        @param pY Ordonn�e du coup de pinceau
        @return Vrai si le pinceau a commenc� � dessiner, Faux sinon (e.g. l'utilisateur a piqu� ailleurs que sur le terrain)
     */
    bool DropBrush::doBegin(int pX, int pY)
    {
        // V�rifie que le point est bien sur le monde.
        if (!contains(this->workingExtent(), UTI::Pointf(F32(pX), F32(pY))))
        {
            return false;
        }

        insertModel(mModel, pX, pY);    // Pas mal d'�volution pr�vues ici.

        return false;
    }


    /*! Apr�s avoir 'pos� le pinceau', celui-ci est d�plac�.<br>
        Une zone fonction de la taille et de la forme du pinceau sera peinte (applyToPieceOfData() sera appell�e sur chaque donn�e concern�e)
        @param pX Nouvelle abscisse du pinceau
        @param pY Nouvelle ordonn�e du pinceau
     */
    void DropBrush::doApply(int, int)
    {
        return;
    }


    /*! Pr�pare l'apparence du pinceau pour son affichage � une [nouvelle] position.
        @param pX Nouvelle abscisse du pinceau
        @param pY Nouvelle ordonn�e du pinceau
     */
    void DropBrush::doMoveTo(int, int)
    {
        return;
    }


    /*!
     */
    void DropBrush::insertModel(AppearancePtr, float, float) { }


    /*!
     */
    void DropBrush::doForwardState(Ptr pFrom)
    {
        if (auto lFrom = std::dynamic_pointer_cast<DropBrush>(pFrom).get())
        {
            mModel = lFrom->mModel;
        }
    }


//------------------------------------------------------------------------------
//                                  "Brushable"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Brushable::~Brushable() { }


    /*! @return Un pinceau de d�p�t de mod�le nouvellement cr��
     */
    Brushable::DropBrushPtr Brushable::getDropBrush()
    {
        return std::make_shared<DropBrush>();
    }
}
