/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_ENUM_HH
#define QWOR_ENUM_HH

/*! @file IDE/Module/QWORld/Enum.hh
    @brief Enum�rations du module QWOR.
    @author @ref Guillaume_Terrissol
    @date 9 Ao�t 2006 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QWOR
{
    enum
    {
        eMaxLoadedTiles = 16   // Et pas moins !
    };

}

#endif // De QWOR_ENUM_HH
