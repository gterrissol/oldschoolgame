/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include <Qt>
#include <memory>
#include "PAcKage/Handle.hh"

//---------------------------------------------------------------------------
//                            Fonction Assistante
//---------------------------------------------------------------------------

/*! Fonction de hashage pour les handles de fichier.
 */
inline uint qHash(const PAK::FileHdl& pHdl);


#include "WorldDataService.hh"

/*! @file IDE/Module/QWORld/WorldDataService.cc
    @brief M�thodes (non-inline) de la classe QWOR::WorldDataService.
    @author @ref Guillaume_Terrissol
    @date 15 Novembre 2014 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

//---------------------------------------------------------------------------
//                                  En-t�tes
//---------------------------------------------------------------------------

#include <QHash>
#include <QRegExp>

#include "DATA/QPAcKage/FileService.hh"
#include "PAcKage/LoaderSaver.hh"


//------------------------------------------------------------------------------
//                            Fonction Assistante
//------------------------------------------------------------------------------

    /*! Fonction de hashage pour les handles de fichier.
     */
    inline uint qHash(const PAK::FileHdl& pHdl)
    {
        return qHash(static_cast<unsigned long>(pHdl.value()));
    }


namespace QWOR
{
//------------------------------------------------------------------------------
//                           WorldDataService : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QWOR::WorldDataService
        @version 0.5
     */
    class WorldDataService::WorldDataServicePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(WorldDataService)
    public:

        WorldDataServicePrivate(WorldDataService* pThat, OSGi::Context* pContext);  //!< Constructeur.

        OSGi::Context*                                              mContext;       //!< Contexte d'ex�cution.
        QHash<PAK::FileHdl, std::weak_ptr<Data::Ptr::element_type>> mInstances;     //!< Donn�es charg�es.
        QMap<WOR::EType, QString>                                   mTypes;         //!< Nom des types de mondes.
        QString                                                     mTileExtension; //!< Extension de tuile.
    };

    /*! @param pThat    Service fr.osg.ide.wor.data .
        @param pContext Contexte d'ex�cution.
     */
    WorldDataService::WorldDataServicePrivate::WorldDataServicePrivate(WorldDataService* pThat, OSGi::Context* pContext)
        : q{pThat}
        , mContext{pContext}
        , mTypes{}
        , mTileExtension{}
    { }



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pContext Contexte d'ex�cution.
     */
    WorldDataService::WorldDataService(OSGi::Context* pContext)
        : DATA::Store{"worldgroup"} // Recenser les univers et leurs groupes ?
        , pthis{this, pContext}
    { }

    //! Destructeur.
    WorldDataService::~WorldDataService() = default;


//------------------------------------------------------------------------------
//              Noms
//------------------------------------------------------------------------------

    /*!
     */
    QString WorldDataService::groupName(QString pGroupFilename) const
    {
        QRegExp lRE{R"((\w+).grp)"};
        if (lRE.exactMatch(pGroupFilename))
        {
            return lRE.cap(1);
        }
        else
        {
            return QString::null;
        }
    }


    /*! @param pWorldFilename Nom du fichier d'un monde.
        @return Le nom du fichier du groupe de mondes auquel @p pWorldFilename appartient.
     */
    QString WorldDataService::groupFilenameFromWorld(QString pWorldFilename) const
    {
        QRegExp lRE{R"((\w+)_.*\..*)"};
// TODO => voir Data::Private::createTileHdl(unsigned long pOffset) pour la cr�ation des noms de fichiers de tuiles
        if (lRE.exactMatch(pWorldFilename))
        {
            return QStringLiteral("%1.grp").arg(lRE.cap(1));
        }
        else
        {
            return QStringLiteral("xxx");
        }
    }


    /*! @param pTileFilename Nom du fichier d'un monde.
        @return Le nom du fichier du groupe de mondes auquel @p pTileFilename appartient.
     */
    QString WorldDataService::groupFilenameFromTile(QString pTileFilename) const
    {
        QRegExp lRE{R"((\w+)_\d+\.)" + tileExtension()};

        if (lRE.exactMatch(pTileFilename))
        {
            return QStringLiteral("%1.grp").arg(lRE.cap(1));
        }
        else
        {
            return QStringLiteral("xxx");
        }
    }


    /*! A appeler pour cr�er un fichier pour un nouveau monde de type @p pWorld, appartenant au group @p pGroup.
     */
    QString WorldDataService::newWorldFilename(QString pGroup, WOR::EType pWorld)
    {
        load();
        auto    lCounter = string("counter");
        auto    lValue   = lCounter.toInt() + 1;
        set("counter", QString::number(lValue));
        save();

        return QStringLiteral("%1_%2.%3").arg(pGroup).arg(lValue).arg(worldExtension(pWorld));
    }


    /*! A appeler pour cr�er un fichier pour une nouvelle tuile, appartenant au group @p pGroup.
     */
    QString WorldDataService::newTileFilename(QString pGroup)
    {
        load();
        auto    lCounter = string("tile counter");
        auto    lValue   = lCounter.toInt() + 1;
        set("tile counter", QString::number(lValue));
        save();

        return QStringLiteral("%1_%2.%3").arg(pGroup).arg(lValue).arg(tileExtension());
    }


    /*!
     */
    Data::Ptr WorldDataService::data(PAK::FileHdl pHdl)
    {
        auto    lIter = pthis->mInstances.find(pHdl);

        if (lIter != pthis->mInstances.end())
        {
            // D�j� enregistr�.
            if (!lIter.value().expired())
            {
                // Toujours pr�sent.
                return lIter.value().lock();
            }
        }
        // Absent ou d�j� d�truit : [re]cr�ation.

        auto    lNew = std::make_shared<Data>(pHdl, pthis->mContext);
        pthis->mInstances.insert(pHdl, lNew);

        return lNew;
    }

    /*!
     */
    void WorldDataService::checkIn(QString pExt, WOR::EType pEnum)
    {
        // Oui, dans ce sens : l'op�ration la plus courante devrait �tre la recherche par
        // �num�r�, pas grave si le retrait (voir ci-dessous) est un peu laborieux.
        pthis->mTypes[pEnum] = pExt;
    }

    /*!
     */
    void WorldDataService::checkOut(QString pExt)
    {
        for(auto lIt = pthis->mTypes.begin(); lIt != pthis->mTypes.end(); ++lIt)
        {
            if (lIt.value() == pExt)
            {
                pthis->mTypes.erase(lIt);
                return;
            }
        }
    }

    /*!
     */
    void WorldDataService::checkInTile(QString pExt)
    {
        pthis->mTileExtension = pExt;
    }

    /*!
     */
    QString WorldDataService::saveFolder() const
    {
        return pthis->mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }

    /*!
     */
    bool WorldDataService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }

    /*!
     */
    std::string WorldDataService::typeName() const
    {
        return typeid(*this).name();
    }

    /*! @param pWorld Monde dont on cherche l'extension de fichier.
        @return L'extension du monde de type @p pWorld.
     */
    QString WorldDataService::worldExtension(WOR::EType pWorld) const
    {
        return pthis->mTypes.value(pWorld, QStringLiteral("xxx"));
    }

    /*! @return L'extension des fichiers de tuile.
     */
    QString WorldDataService::tileExtension() const
    {
        return pthis->mTileExtension;
    }
}
