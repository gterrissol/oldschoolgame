/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_WORLDDATASERVICE_HH
#define QWOR_WORLDDATASERVICE_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/WorldDataService.hh
    @brief En-t�te de la classe QWOR::WorldDataService.
    @author @ref Guillaume_Terrissol
    @date 15 Novembre 2014 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <OSGi/Service.hh>

#include "DATA/Store.hh"
#include "WORld/Enum.hh"

#include "Data.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                      Service pour les Groupes de Mondes
//------------------------------------------------------------------------------

    /*! @brief Gestion des groupes de mondes.
        @version 0.6

        Dans la version pr�c�dente de l'�dition, les donn�es communes de mondes �taient g�r�es via un singleton.@n
        C'est maintenant un service qui assure ce "service".
     */
    class WorldDataService : public OSGi::Service, public DATA::Store
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    WorldDataService(OSGi::Context* pContext);              //!< Constructeur.
virtual             ~WorldDataService();                                    //!< Destructeur.
        //@}
        //! @name Noms
        //@{
        QString     groupName(QString pGroupFilename) const;                //!< Nom d'un groupe.
        QString     groupFilenameFromWorld(QString pWorldFilename) const;   //!< Nom du groupe d'un monde.
        QString     groupFilenameFromTile(QString pTileFilename) const;     //!< Nom du groupe d'une tuile.
        QString     newWorldFilename(QString pGroup, WOR::EType pWorld);    //!< Nom de fichier pour un nouveau monde.
        QString     newTileFilename(QString pGroup);                        //!< Nom de fichier pour un nouvelle tuile.
        //@}
        Data::Ptr   data(PAK::FileHdl pHdl);                                //!< Donn�es d'un groupe de mondes.
        //! @name Enregistrement
        //@{
        void        checkIn(QString pExt, WOR::EType pEnum);                //!< D�finition de l'extension d'un type de monde.
        void        checkInTile(QString pExt);                              //!< D�finition de l'extension des fichiers de tuiles.
        void        checkOut(QString pExt);                                 //!< Retrait d'une extension enregsitr�e.
        //@}
    protected:
        //! @name Impl�mentation
        //@{
        QString     saveFolder() const override;                            //!< R�peroire de sauvegarde des donn�es IDE.

        bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?

    private:

        std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
        QString     worldExtension(WOR::EType pWorld) const;                //!< Extension d'un type de monde.
        QString     tileExtension() const;                                  //!< Extension d'un fichier de tuile.

        Q_CUSTOM_DECLARE_PRIVATE(WorldDataService)
    };
}

#endif  // De QWOR_WORLDDATASERVICE_HH
