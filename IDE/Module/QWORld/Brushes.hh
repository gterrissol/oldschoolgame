/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_BRUSHES_HH
#define QWOR_BRUSHES_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/Brushes.hh
    @brief En-t�te de la classe QWOR::Brushes.
    @author @ref Guillaume_Terrissol
    @date 9 Novembre 2005 - 28 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "CORE/Synchronize.hh"
#include "CORE/Translate.hh"
#include "EDITion/Observer.hh"
#include "Module/Editor.hh"
#include "STL/STL.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                              Editeur d'Ext�rieur
//------------------------------------------------------------------------------

    /*! @brief Page de pinceaux pour l'�dition de terrains.
        @version 0.5

     */
    class Brushes : public Module::Editor, public EDIT::Observer, public CORE::Translate<Brushes>, public CORE::Synchronized
    {
        Q_OBJECT
    public:
        //! @name Type de pointeur
        //@{
        using AppearancePtr = std::shared_ptr<APP::Appearance>;             //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Brushes(OSGi::Context* pContext);                       //!< Constructeur.
virtual             ~Brushes();                                             //!< Destructeur.
        //@}

    public slots:

        void        updateEnvironmentBrushes();                             //!< Mise � jour de la liste d'environnements.
        void        removeEnvironmentBrushes();
        void        setDropModel(AppearancePtr pAppearance);

    protected:

        void        changeEvent(QEvent* pEvent) override;                   //!< Changement d'�tat.

private slots:
        //! @name Slots
        //@{
        void        brushChoiceChanged();                                   //!< S�lection d'un type de pinceau.
        void        environmentChoice(int pEnvID);                          //!< S�lection d'un type d'environnement.
        void        blendingChoice(int pBlending);                          //!< S�lection d'un type de m�lange.
        void        cliffChoice(int pCliffID);                              //!< S�lection d'une amplitude de falaise.
        void        altitudeChoice(int pAltID);                             //!< S�lection d'une variation d'altitude.
        void        dropChoice(int pAltID);                                 //!< S�lection d'un type de d�p�t d'objet.
        void        sizeChoice(int pSize);                                  //!< S�lection d'une taille de pinceau.
        void        shapeChoice(int pShapeID);                              //!< S�lection d'une forme de pinceau.
        //@}

    private:
        //! @name Pertinence du sujet
        //@{
        bool        canManage(EDIT::Controller::Ptr pDatum) const override; //!< Possibilit� de gestion d'un sujet.
        void        manage(EDIT::Controller::Ptr pDatum) override;          //!< Gestion effective d'un sujet.
        void        manageNone() override;                                  //!< Arr�t de la gestion du sujet.
        //@}
        //! @name Ecoute du sujet
        //@{
        void        listen(const EDIT::Answer* pAnswer) override;           //!< Ecoute effective de la cible.
        //@}
        //! @name Changement de langue
        //@{
        auto        buildTranslater() -> CORE::Translated::Updater override;
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(Brushes)
    };
}

#endif  // De QWOR_BRUSHES_HH
