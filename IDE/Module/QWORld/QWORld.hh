/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_QWORLD_HH
#define QWOR_QWORLD_HH

/*! @file IDE/Module/QWORld/QWORld.hh
    @brief Pr�-d�clarations du module @ref QWOR.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QWOR
{
    class Brush;
    class Brushable;
    class CliffBrush;
    class Data;
    class ElevationBrush;
    class EnvironmentBrush;
    class EditSurfaceMode;
    class ImportExport;
    class SlideBrush;
    class WorldDataService;

    template<class TBrush, class TGroup, class TController> class GenericBrush;
    template<              class TGroup, class TController> class GenericCliffBrush;
    template<class TBrush, class TGroup, class TController> class GenericElevationBrush;
    template<              class TGroup, class TController> class GenericEnvironmentBrush;
    template<              class TGroup, class TController> class GenericSlideBrush;
}

#endif  // De QWOR_QWORLD_HH
