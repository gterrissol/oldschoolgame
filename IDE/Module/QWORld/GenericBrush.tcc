/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GenericBrush.hh"

/*! @file IDE/Module/QWORld/GenericBrush.tcc
    @brief M�thodes des classes QWOR::GenericBrush, QWOR::GenericElevationBrush,
    QWOR::GenericCliffBrush, QWOR::GenericSlideBrush & QWOR::GenericEnvironmentBrush.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2005 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cmath>
#include <numeric>

#include "Module/QT3D/Controller.hh"
#include "EDITion/Requests.hh"
#include "TILE/Fragment.hh"

#include "ErrMsg.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                      Poids des Masques pour les Pinceaux
//------------------------------------------------------------------------------

    //! @name Poids pour les masques de pinceaux.
    //@{
    extern Weights gDefautWeights;
    //@}


//------------------------------------------------------------------------------
//                             Fonctions Assistantes
//------------------------------------------------------------------------------

    /*! Cr�e un masque rond pour les pinceaux.
        @param pExtent   Zone que le masque doit couvrir
        @param pElements Liste des �l�ments composant le masque
        @param pWeights  Poids respectifs des �l�ments de <i>pElements</i>
     */
    template<typename TOffset>
    void createRound(const UTI::Rect pExtent, std::vector<std::pair<TOffset, TOffset>>& pElements, Brush::Weights& pDataWeights, const Weights* pWeights)
    {
        unsigned short  lHeight = pExtent.roundedHeight();
        unsigned short  lWidth  = pExtent.roundedWidth();
        signed short    lTop    = pExtent.roundedTop();
        signed short    lLeft   = pExtent.roundedLeft();

        // Le pinceau doit vraiment �tre rond.
        if (lHeight == lWidth)
        {
            pElements.reserve(lWidth * lHeight);
            pDataWeights.reserve(lWidth * lHeight);

            for(unsigned short lLi = 0; lLi < lHeight; ++lLi)
            {
                TOffset lY = TOffset(lTop - lLi);
                for(unsigned short lCo = 0; lCo < lWidth; ++lCo)
                {
                    TOffset lX = TOffset(lLeft + lCo);
                    pElements.push_back(std::make_pair(lX, lY));
                    switch(lWidth)
                    {
                        case 1:
                            pDataWeights.push_back(pWeights->w1[lLi][lCo]);
                            break;
                        case 2:
                            pDataWeights.push_back(pWeights->w2[lLi][lCo]);
                            break;
                        case 3:
                            pDataWeights.push_back(pWeights->w3[lLi][lCo]);
                            break;
                        case 4:
                            pDataWeights.push_back(pWeights->w4[lLi][lCo]);
                            break;
                        case 5:
                            pDataWeights.push_back(pWeights->w5[lLi][lCo]);
                            break;
                        case 6:
                            pDataWeights.push_back(pWeights->w6[lLi][lCo]);
                            break;
                        case 7:
                            pDataWeights.push_back(pWeights->w7[lLi][lCo]);
                            break;
                        case 8:
                            pDataWeights.push_back(pWeights->w8[lLi][lCo]);
                            break;
                        case 9:
                            pDataWeights.push_back(pWeights->w9[lLi][lCo]);
                            break;
                        case 10:
                            pDataWeights.push_back(pWeights->w10[lLi][lCo]);
                            break;
                        default:
                            pDataWeights.push_back(k0F);
                            break;
                    }
                }
            }
        }
    }


    /*! Cr�e un masque carr� pour les pinceaux.
        @param pExtent   Zone que le masque doit couvrir
        @param pElements Liste des �l�ments composant le masque
        @param pWeights  Poids respectifs des �l�ments de <i>pElements</i>
     */
    template<typename TOffset>
    void createSquare(const UTI::Rect pExtent, std::vector<std::pair<TOffset, TOffset>>& pElements, Brush::Weights& pWeights)
    {
        unsigned short  lHeight = pExtent.roundedHeight();
        unsigned short  lWidth  = pExtent.roundedWidth();
        signed short    lTop    = pExtent.roundedTop();
        signed short    lLeft   = pExtent.roundedLeft();

        // Le pinceau doit vraiment �tre carr�.
        if (lHeight == lWidth)
        {
            pElements.reserve(lWidth * lHeight);
            pWeights.reserve(lWidth * lHeight);

            for(unsigned short lLi = 0; lLi < lHeight; ++lLi)
            {
                TOffset lY = TOffset(lTop - lLi);
                for(unsigned short lCo = 0; lCo < lWidth; ++lCo)
                {
                    TOffset lX = TOffset(lLeft + lCo);
                    pElements.push_back(std::make_pair(lX, lY));
                    pWeights.push_back(k1F);
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                Pinceau G�n�rique : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
        @note Par d�faut, le pinceau est de forme GenericBrush<TGroup, TController>::eSquare et de taille
        GenericBrush<TGroup, TController>::eMinBrushSize
     */
    template<class TBrush, class TGroup, class TController>
    GenericBrush<TBrush, TGroup, TController>::GenericBrush(TControllerPtr pEdited)
        : mEdited(pEdited)
        , mX{0}
        , mY{0}
        , mPieceOfData{}
    {
        ASSERT(!mEdited.expired(), kNoObjectToEdit)
    }


    /*! Destructeur.
     */
    template<class TBrush, class TGroup, class TController>
    GenericBrush<TBrush, TGroup, TController>::~GenericBrush() { }


//------------------------------------------------------------------------------
//                       Pinceau G�n�rique : Informations
//------------------------------------------------------------------------------

    /*! Permet d'acc�der � l'origine du coup de pinceau actuel.
        @return L'abscisse du premier point peint par le coup de pinceau actuel (ou le dernier donn�)
     */
    template<class TBrush, class TGroup, class TController>
    int GenericBrush<TBrush, TGroup, TController>::brushX() const
    {
        return mX;
    }


    /*! Permet d'acc�der � l'origine du coup de pinceau actuel.
        @return L'ordonn�e du premier point peint par le coup de pinceau actuel (ou le dernier donn�)
     */
    template<class TBrush, class TGroup, class TController>
    int GenericBrush<TBrush, TGroup, TController>::brushY() const
    {
        return mY;
    }


    /*! Donn�e au premier point d'application du pinceau.
        @return La donn�e au premier point d'application du coup de pinceau actuel (ou du dernier donn�)
     */
    template<class TBrush, class TGroup, class TController>
    typename GenericBrush<TBrush, TGroup, TController>::TType GenericBrush<TBrush, TGroup, TController>::brushData() const
    {
        return mPieceOfData;
    }


    /*! Permet de retrouver la derni�re zone modifi�e par un appel � apply().
        @return La zone modifi�e par apply()
     */
    template<class TBrush, class TGroup, class TController>
    UTI::Rect GenericBrush<TBrush, TGroup, TController>::modifiedExtent() const
    {
        return mLastModifiedExtent;
    }


    /*! R�cup�re une donn�e.
        @param pRequestedPiece La donn�e � r�cup�rer (la position permet de trouver la donn�e)
        @return VRAI si la donn�e a bien �t� r�cup�r�e, FAUX sinon (elle �tait en dehors du monde)
     */
    template<class TBrush, class TGroup, class TController>
    bool GenericBrush<TBrush, TGroup, TController>::retrievePieceOfData(TData& pRequestedPiece) const
    {
        if (contains(this->workingExtent(), UTI::Pointf(F32(pRequestedPiece.col()), F32(pRequestedPiece.row()))))
        {
            // Pr�pare un groupe avec un seul �lement.
            TGroup  lOneElementGroup;
            lOneElementGroup.insertInArea(pRequestedPiece);

            // R�cup�re la donn�e.
            retrieveData(lOneElementGroup);

            // Et la "retourne".
            pRequestedPiece = lOneElementGroup[0].data();

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! Cette m�thode a �t� introduite afin de permettre aux pinceaux d'environnement de r�cup�rer les
        �l�vations aux points d'application.
        @param pPieceOfData Un des points d'application du pinceau (couverts par l'apparence du pinceau)
        @param pWeight      Poids associ� au point d'application
        @note Une impl�mentation peut �tre fournie par sp�cialisation
     */
    template<class TBrush, class TGroup, class TController>
    MATH::V4 GenericBrush<TBrush, TGroup, TController>::vertexForModel(const typename TGroup::TElement& pPiece, float pWeight) const
    {
        // L'�l�vation est contenue dans la donn�e.
        return MATH::V4(F32(pPiece.col()), F32(pPiece.row()), F32(pPiece.data()), F32(pWeight));
    }


//------------------------------------------------------------------------------
//          Pinceau G�n�rique : Traitement sur les Donn�es (Algorithme)
//------------------------------------------------------------------------------

    /*! Commence � appliquer le pinceau sur une zone.<br>
        Les coordonn�es et la hauteur sous le pinceau sont m�moris�es : certaines 'techniques de peinture' requi�rent ces informations.
        @param pX Abscisse du coup de pinceau
        @param pY Ordonn�e du coup de pinceau
        @return Vrai si le pinceau a commenc� � dessiner, Faux sinon (e.g. l'utilisateur a piqu� ailleurs que sur le terrain)
     */
    template<class TBrush, class TGroup, class TController>
    bool GenericBrush<TBrush, TGroup, TController>::doBegin(int pX, int pY)
    {
        // V�rifie que le point est bien sur le monde.
        if (!contains(this->validExtentForModel(), UTI::Pointf(F32(pX), F32(pY))))
        {
            return false;
        }
        // R�cup�re des infos sur le point initial.
        mX    = pX;
        mY    = pY;
        TData   lElement    = TData(typename TData::TOffset(mX), typename TData::TOffset(mY), typename TData::TType{});
        retrievePieceOfData(lElement);
        mPieceOfData = lElement.data();

        // Introduit une rupture dans la fusion des requ�tes.
        validateData(TGroup());

        onBegin();

        // Puis "peint" � la position initiale.
        this->apply(pX, pY);

        return true;
    }


    /*! Apr�s avoir 'pos� le pinceau', celui-ci est d�plac�.<br>
        Une zone fonction de la taille et de la forme du pinceau sera peinte (applyToPiecesOfData() sera appell�e toutes les donn�es concern�es).
        @param pX Nouvelle abscisse du pinceau.
        @param pY Nouvelle ordonn�e du pinceau.
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::doApply(int pX, int pY)
    {
        // V�rifie que le point est bien sur le monde.
        if (!contains(this->validExtentForModel(), UTI::Pointf(F32(pX), F32(pY))))
        {
            return;
        }
        // 1�re �tape : construction d'un groupe de donn�es sur la zone � �diter.
        auto lWeightedGroup = createGroup(pX, pY, false);

        // Etape interm�diaire.
        beforeApply();

        // 2�me �tape : modification des donn�es (mode).
        applyToPiecesOfData(lWeightedGroup);

        // 3�me �tape : validation des donn�es modifi�es.
        const auto & lNewGroup = std::get<TGroup>(lWeightedGroup);
        if (0 < lNewGroup.count())
        {
            validateData(lNewGroup);
        }
    }


    /*! Pr�pare l'apparence du pinceau pour son affichage � une [nouvelle] position.
        @param pX Nouvelle abscisse du pinceau
        @param pY Nouvelle ordonn�e du pinceau
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::doMoveTo(int pX, int pY)
    {
        // V�rifie que le point est bien sur le monde.
        if (!contains(this->validExtentForModel(), UTI::Pointf(F32(pX), F32(pY))))
        {
            return;
        }
        // 1�re �tape : construction d'un groupe de donn�es sur la zone sous le pinceau.
        TWeights    lWeights;
        TGroup      lGroup;
        std::tie(lGroup, lWeights) = createGroup(pX, pY, true);

        // 2�me �tape :adaptation des poids au pinceau choisi.
        transformModel(lWeights);

        // 3�me �tape : mise � jour du mod�le du pinceau.
        typename Brush::ModelVertices lVertices;
        lVertices.reserve(lWeights.size());

        for(unsigned long lE = 0; lE < lGroup.count(); ++lE)
        {
            const TData&    lPieceOfData    = lGroup[lE];

            lVertices.push_back(vertexForModel(lPieceOfData, lWeights[lE]));
        }

        this->updateModel(lVertices);
    }


//------------------------------------------------------------------------------
//        Pinceau G�n�rique : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! Cr�e un groupe d'�l�vations correspondant � la forme, la taille et la position du pinceau.
        @param pX        Position du pinceau (X)
        @param pY        Position du pinceau (Y)
        @param pForModel VRAI si le group doit �tre construit pour un mod�le de pinceau, FAUX sinon (pour une �dition)
        @return Un tuple :
        - 1er �lement : groupe de donn�es sur la zone requise
        - 2nd �l�ment : poids associ�s aux donn�es du groupe
     */
    template<class TBrush, class TGroup, class TController>
    typename GenericBrush<TBrush, TGroup, TController>::TWeightedGroup
    GenericBrush<TBrush, TGroup, TController>::createGroup(int pX, int pY, bool pForModel) const
    {
        typedef std::vector<std::pair<TOffset, TOffset>>    Elements;
        // Cr�e le groupe donn�es (en fonction de la forme et de la taille du pinceau).
        UTI::Rect  lEditedExtent = pForModel ? this->extentUnderModel(pX, pY) : extentForEdition(pX, pY);
        Elements   lElements;
        TWeights   lWeights;

        switch(this->shape())
        {
            case TBrush::eRound:
                createRound(lEditedExtent, lElements, lWeights, pForModel ? modelWeights() : editionWeights());
                break;
            case TBrush::eSquare:
                createSquare(lEditedExtent, lElements, lWeights);
                break;
            default:
                ASSERT_EX(false, kUnknownBrushShape, return std::make_tuple(TGroup(), TWeights());)
                break;
        }

        // Applique les contraintes de position (limites du monde).
        UTI::Rect   lValidityExtent = this->workingExtent();
        TWeights    lFinalWeights;
        Elements    lValidElements;
        auto        lEltIter    = lElements.begin();
        auto        lWeightIter = lWeights.begin();

        while(lEltIter != lElements.end())
        {
            if (contains(lValidityExtent, UTI::Pointf(F32(lEltIter->first), F32(lEltIter->second))))
            {
                lValidElements.push_back(*lEltIter);
                lFinalWeights.push_back(*lWeightIter);
            }
            ++lEltIter;
            ++lWeightIter;
        }

        if (!pForModel)
        {
            mLastModifiedExtent = lEditedExtent & this->workingExtent();
        }

        // Cr�e le groupe.
        TGroup  lGroup;
        for(lEltIter = lValidElements.begin(); lEltIter != lValidElements.end(); ++lEltIter)
        {
            lGroup.insertInArea(TData(lEltIter->first, lEltIter->second, typename TData::TType{}));
        }

        // R�cup�re les donn�es utiles, au besoin.
        retrieveData(lGroup);

        return std::make_tuple(lGroup, lFinalWeights);
    }


    /*! R�cup�re un groupe de donn�es.
        @param pData Groupe de donn�es � remplir (en fait, seules des donn�es utiles sont � r�cup�rer aux
        positions de chaque �l�ment du groupe)
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::retrieveData(TGroup& pData) const
    {
        mEdited.lock()->get(pData);
    }


    /*! Par d�faut, doBegin() effectue tout le travail d'initialisation pour les pinceaux g�n�riques.<br>
        Il n'y a donc rien � faire dans l'impl�mentation de base d'onBegin().
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::onBegin() { }


    /*! Cette m�thode est appel�e par apply(), juste avant d'appeler applyToPieceOfData() sur chacune des donn�es � �diter.<br>
        Elle peut �tre r�impl�ment�e pour effectuer un travail quelconque (l'impl�mentation par d�faut ne fait rien).
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::beforeApply() { }


    /*! @fn void GenericBrush<TBrush, TGroup, TSubject>::applyToPieceOfData(TData& pPieceOfData, float pWeight) const
        L'essentiel du travail est r�alis� dans cette m�thode : son impl�mentation dans les classes
        d�riv�es doit modifier les donn�es pass�es en argument selon le type de pinceau.
        @param pPieceOfData Donn�e � modifier (la valeur est r�cup�r�e en sortie)
        @param pWeight      Poids associ� � la donn�e (le type d'�dition peut en tenir compte); compris
        entre k0F et k1F
     */


    /*! Une fois que les donn�es ont �t� modifi�es selon les param�tres [et le type] de l'instance, elles
        peuvent �tre appliqu�es sur l'objet �dit�.
        @param pData Groupe de donn�es �dit�es par un appel � apply() et pr�tes � �tre valid�es
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::validateData(const TGroup& pData)
    {
        EDIT::EditRequest<const TGroup&>    lRequest{pData};
        EDIT::Controller::Ptr               lEdited = mEdited.lock();
        EDIT::SubjectList{EDIT::Controller::subject(lEdited)}.perform(&lRequest);

    }


    /*! Permet de r�cup�rer la zone � �diter (fonction du type de pinceau).
        @param pX Abscisse du centre de la zone � �diter
        @param pY Ordonn�e du centre de la zone � �diter
        @return La zone � modifier suite � un coup de pinceau
        @note A ce niveau, aucun contr�le de validit� n'est effectu� (la zone retourn�e peut "d�border" du monde)
     */
    template<class TBrush, class TGroup, class TController>
    UTI::Rect GenericBrush<TBrush, TGroup, TController>::extentForEdition(int pX, int pY) const
    {
        unsigned short  lSize = this->size();

        return UTI::Rect(UTI::Pointf(F32(pX - lSize / 2), F32(pY + lSize / 2)), UTI::Sizef(F32(lSize), F32(lSize)));
    }


    /*! L'apparence du pinceau est d�termin�e en partie par les poids associ�s aux �l�vations. Cette m�thode permet de transformer ces poids
        (de mani�re tr�s simple).
        @param pWeights Poids associ�s aux �l�vations sous le pinceaux
        @note L'impl�mentation par d�faut ne fait rien
     */
    template<class TBrush, class TGroup, class TController>
    void GenericBrush<TBrush, TGroup, TController>::transformModel(TWeights&) const { }


    /*! @return Poids pour le mod�le de pinceau.
     */
    template<class TBrush, class TGroup, class TController>
    const Weights* GenericBrush<TBrush, TGroup, TController>::modelWeights() const
    {
        return &gDefautWeights;
    }


    /*! @return Poids pour l'�dition.
     */
    template<class TBrush, class TGroup, class TController>
    const Weights*GenericBrush<TBrush, TGroup, TController>::editionWeights() const
    {
        return &gDefautWeights;
    }


//------------------------------------------------------------------------------
//             Pinceau pour Elevations : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
        @note Par d�faut, le pinceau est de forme GenericBrush<TBrush, TGroup, TController>::eSquare et de taille
        GenericBrush<TBrush, TGroup, TController>::eMinBrushSize
     */
    template<class TBrush, class TGroup, class TController>
    GenericElevationBrush<TBrush, TGroup, TController>::GenericElevationBrush(TControllerPtr pEdited)
        : GenericBrush<TBrush, TGroup, TController>(pEdited)
    { }


    /*! Destructeur.
     */
    template<class TBrush, class TGroup, class TController>
    GenericElevationBrush<TBrush, TGroup, TController>::~GenericElevationBrush() { }


//------------------------------------------------------------------------------
//                  Pinceau pour Elevations : Acc�s aux Extr�ma
//------------------------------------------------------------------------------

    /*! Parmi les voisins de son argument, cette m�thode cherche l'altitude du point le plus �lev�.
        @param pPieceOfData Position autour de laquelle chercher le point le plus �lev�
        @return L'altitude la plus �lev�e autour de la position de <i>pPieceOfData</i>
     */
    template<class TBrush, class TGroup, class TController>
    typename GenericElevationBrush<TBrush, TGroup, TController>::TType GenericElevationBrush<TBrush, TGroup, TController>::localMaximum(const TData& pPieceOfData) const
    {
        std::vector<TType>  lElevations;

        getNeighbours(pPieceOfData, lElevations);

        return *std::max_element(lElevations.begin(), lElevations.end());
    }


    /*! Parmi les voisins de son argument, cette m�thode cherche l'altitude du point le plus bas.
        @param pPieceOfData Position autour de laquelle chercher le point le plus bas
        @return L'altitude la plus basse autour de la position de <i>pPieceOfData</i>
     */
    template<class TBrush, class TGroup, class TController>
    typename GenericElevationBrush<TBrush, TGroup, TController>::TType GenericElevationBrush<TBrush, TGroup, TController>::localMinimum(const TData& pPieceOfData) const
    {
        std::vector<TType>  lElevations;

        getNeighbours(pPieceOfData, lElevations);

        return *std::min_element(lElevations.begin(), lElevations.end());
    }


    /*! Tout �a devient un peu lourd, mais ce n'est pas [trop] grave. Cette m�thode sert juste � factoriser un peu le code.
        @param pPieceOfData Point autour duquel r�cup�rer les �l�vations
        @param pElevations Ce vecteur sera rempli avec les �l�vations des voisins de <i>pPieceOfData</i>
     */
    template<class TBrush, class TGroup, class TController>
    void GenericElevationBrush<TBrush, TGroup, TController>::getNeighbours(const TData& pPieceOfData, std::vector<TType>& pElevations) const
    {
        pElevations.clear();
        pElevations.reserve(8);

        const typename TData::TType   kZero(0);

        TOffset lCol        = pPieceOfData.col();
        TOffset lRow        = pPieceOfData.row();
        TData   lNeighbour  = TData(TOffset(lCol - 1), TOffset(lRow - 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(        lCol,      TOffset(lRow - 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(TOffset(lCol + 1), TOffset(lRow - 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(TOffset(lCol - 1),         lRow,     kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(TOffset(lCol + 1),         lRow,     kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(TOffset(lCol - 1), TOffset(lRow + 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(        lCol,      TOffset(lRow + 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        lNeighbour = TData(TOffset(lCol + 1), TOffset(lRow + 1), kZero);
        if (this->retrievePieceOfData(lNeighbour))
        {
            pElevations.push_back(lNeighbour.data());
        }
        // J'avais dit que c'�tait lourd.
    }


//------------------------------------------------------------------------------
//     Pinceau pour Elevations : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! Permet de r�cup�rer la zone sous l'apparence du pinceau (qui n'est pas forc�ment la m�me que la zone �dit�e).
        @param pX Abscisse du centre de la zone
        @param pY Ordonn�e du centre de la zone
        @return La zone sous l'apparence du pinceau
        @note A ce niveau, aucun contr�le de validit� n'est effectu� (la zone retourn�e peut "d�border" du monde)
     */
    template<class TBrush, class TGroup, class TController>
    UTI::Rect GenericElevationBrush<TBrush, TGroup, TController>::extentUnderModel(int pX, int pY) const
    {
        unsigned short  lSize = this->size() + 2;

        return UTI::Rect(UTI::Pointf(F32(pX - lSize / 2), F32(pY + lSize / 2)), UTI::Sizef(F32(lSize), F32(lSize)));
    }


    /*! Il peut arriver que la zone valide pour l'apparence du pinceau soit diff�rente de celle pour les donn�es peintes. Cette m�thode peut
        �tre r�impl�ment�e afin de conna�tre cette nouvelle zone.
        @return Par d�faut, workingExtent()
     */
    template<class TBrush, class TGroup, class TController>
    UTI::Rect GenericElevationBrush<TBrush, TGroup, TController>::validExtentForModel() const
    {
        return this->workingExtent();
    }


//------------------------------------------------------------------------------
//              Pinceau pour Falaises : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
        @note Par d�faut, ce pinceau aplanit (type de falaise == eSameLevel)
     */
    template<class TGroup, class TController>
    GenericCliffBrush<TGroup, TController>::GenericCliffBrush(TControllerPtr pEdited)
        : GenericElevationBrush<CliffBrush, TGroup, TController>(pEdited)
        , mCurrentReferenceAltitude(0)
    { }


    /*! Destructeur.
     */
    template<class TGroup, class TController>
    GenericCliffBrush<TGroup, TController>::~GenericCliffBrush() { }


//------------------------------------------------------------------------------
//      Pinceau pour Falaises : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! L'�l�vation par falaise doit amener les altitudes � des multiples de la hauteur �l�mentaire de falaise (je suis clair ?). Il faut
        donc s'assurer, avant de modifier les donn�es, qu'on travaille sur une bonne base.
     */
    template<class TGroup, class TController>
    void GenericCliffBrush<TGroup, TController>::onBegin()
    {
        mCurrentReferenceAltitude = (this->brushData() / this->oneLevelHeight()) * this->oneLevelHeight();
    }


    /*! Edition de donn�es.
        @param pData Donn�e pond�r�es � modifier (les valeurs sont r�cup�r�es en sortie).
     */
    template<class TGroup, class TController>
    void GenericCliffBrush<TGroup, TController>::applyToPiecesOfData(TWeightedGroup & pData) const
    {
        auto& [ lNewGroup, lWeights ] = pData;
        for(unsigned long lE = 0; lE < lNewGroup.count(); ++lE)
        {
            auto & lPieceOfData = lNewGroup[lE];
            float  lWeight      = lWeights[lE];

            if (0.45F < lWeight)
            {
                TType   lFinalElevation = mCurrentReferenceAltitude + this->offset();

                // Contr�le sur les extr�ma locaux.
                if (this->localMinimum(lPieceOfData) + this->maximumAmplitude() < lFinalElevation)
                {
                    lFinalElevation = this->localMinimum(lPieceOfData) + this->maximumAmplitude();
                }
                else if (lFinalElevation < this->localMaximum(lPieceOfData) - this->maximumAmplitude())
                {
                    lFinalElevation = this->localMaximum(lPieceOfData) - this->maximumAmplitude();
                }

                // Contr�le sur les extr�ma globaux.
                if (lFinalElevation < this->globalMinimum())
                {
                    lFinalElevation = this->globalMinimum();
                }
                if (this->globalMaximum() < lFinalElevation)
                {
                    lFinalElevation = this->globalMaximum();
                }

                lPieceOfData = lFinalElevation;
            }
        }
    }


    /*! L'apparence du pinceau est d�termin�e en partie par les poids associ�s aux �l�vations. Cette m�thode permet de transformer ces poids
        (de mani�re tr�s simple).
        @param pWeights Poids associ�s aux �l�vations sous le pinceaux
        @note L'impl�mentation par d�faut ne fait rien
     */
    template<class TGroup, class TController>
    void GenericCliffBrush<TGroup, TController>::transformModel(TWeights& pWeights) const
    {
        for(auto lWI = pWeights.begin(); lWI != pWeights.end(); ++lWI)
        {
            if (*lWI < 0.45F)
            {
                *lWI = k0F;
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @copydoc Brush::forwardState(Ptr pFrom)
     */
    template<class TGroup, class TController>
    void GenericCliffBrush<TGroup, TController>::doForwardState(Ptr pFrom)
    {
        GenericCliffBrush* lFrom = std::dynamic_pointer_cast<GenericCliffBrush>(pFrom).get();

        if (lFrom != nullptr)
        {
            mCurrentReferenceAltitude = lFrom->mCurrentReferenceAltitude;
        }
    }




//------------------------------------------------------------------------------
//                Pinceau Analogique : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
     */
    template<class TGroup, class TController>
    GenericSlideBrush<TGroup, TController>::GenericSlideBrush(TControllerPtr pEdited)
        : GenericElevationBrush<SlideBrush, TGroup, TController>(pEdited)
        , mMayApply(false)
    { }


    /*! Destructeur.
     */
    template<class TGroup, class TController>
    GenericSlideBrush<TGroup, TController>::~GenericSlideBrush() { }


//------------------------------------------------------------------------------
//       Pinceau Analogique : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! V�rifie que suffisament de temps s'est �coul� depuis la derni�re application du pinceau. Si ce n'est pas le cas, l'�dition sera
        temporairement d�sactiv�e.
     */
    template<class TGroup, class TController>
    void GenericSlideBrush<TGroup, TController>::beforeApply()
    {
        // V�rifie que suffisament de temps s'est �coul�.
        if (50000 <= mTimer.time())
        {
            mTimer.reset();
            mMayApply = true;
        }
        else
        {
            mMayApply = false;
        }
    }


    /*! Edition de donn�es.
        @param pData Donn�e pond�r�es � modifier (les valeurs sont r�cup�r�es en sortie).
     */
    template<class TGroup, class TController>
    void GenericSlideBrush<TGroup, TController>::applyToPiecesOfData(TWeightedGroup & pData) const
    {
        if (mMayApply)
        {
            auto& [ lNewGroup, lWeights ] = pData;
            for(unsigned long lE = 0; lE < lNewGroup.count(); ++lE)
            {
                auto & lPieceOfData = lNewGroup[lE];
                float  lWeight      = lWeights[lE];

                TType   lElevation  = lPieceOfData.data();
                static const float  kScaleFactor      = 0.02F / TILE::kVerticalStep;
                static const TType  kNoiseAmplitude   = TType(0.2F / TILE::kVerticalStep);

                switch(this->smoothing())
                {
                    case GenericSlideBrush::eRaise:
                        lElevation += TType(F32(lWeight * kScaleFactor * this->size()));
                        break;
                    case GenericSlideBrush::eLower:
                        lElevation -= TType(F32(lWeight * kScaleFactor * this->size()));
                        break;
                    case GenericSlideBrush::ePlateau:
                        lElevation = this->brushData();
                        break;
                    case GenericSlideBrush::eNoise:
                        lElevation += TType(F32(((rand() % kNoiseAmplitude) - kNoiseAmplitude / 2) * 0.25F * lWeight * this->size()));
                        break;
                    case GenericSlideBrush::eSmooth:
                        {
                            std::vector<TType>  lElevations;
                            this->getNeighbours(lPieceOfData, lElevations);
                            float               lAverage = (lWeight * std::accumulate(lElevations.begin(), lElevations.end(), 0)) / lElevations.size(); // size() != 0
                            lElevation = TType(F32((lElevation + lAverage) / (k1F + lWeight)));
                        }
                        break;
                    default:
                        break;
                }

                // Contr�le sur les extr�ma globaux.
                if (lElevation < this->globalMinimum())
                {
                    lElevation = this->globalMinimum();
                }
                if (this->globalMaximum() < lElevation)
                {
                    lElevation = this->globalMaximum();
                }

                lPieceOfData = lElevation;
            }
        }
    }


//------------------------------------------------------------------------------
//             Pinceau d'Environnement : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
     */
    template<class TGroup, class TController>
    GenericEnvironmentBrush<TGroup, TController>::GenericEnvironmentBrush(TControllerPtr pEdited)
        : GenericBrush<EnvironmentBrush, TGroup, TController>(pEdited)
    { }


    /*! Destructeur.
     */
    template<class TGroup, class TController>
    GenericEnvironmentBrush<TGroup, TController>::~GenericEnvironmentBrush() { }


//------------------------------------------------------------------------------
//     Pinceau d'Environnement : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! Edition de donn�es.
        @param pData Donn�e pond�r�es � modifier (les valeurs sont r�cup�r�es en sortie).
     */
    template<class TGroup, class TController>
    void GenericEnvironmentBrush<TGroup, TController>::applyToPiecesOfData(TWeightedGroup & pData) const
    {
        auto& [ lNewGroup, lWeights ] = pData;
        for(unsigned long lE = 0; lE < lNewGroup.count(); ++lE)
        {
            auto &                  lPieceOfData = lNewGroup[lE];
            typename TData::TType   lData        = lPieceOfData.data();

            if (0.45F < lWeights[lE])
            {
                lData.tilingId0 = lData.tilingId1 = lData.tilingId2 = lData.tilingId3 = this->tilingId();
                lPieceOfData = lData;
            }
        }
    }


    /*! Permet de r�cup�rer la zone � �diter (fonction du type de pinceau).
        @param pX Abscisse du centre de la zone � �diter
        @param pY Ordonn�e du centre de la zone � �diter
        @return La zone � modifier suite � un coup de pinceau
        @note A ce niveau, aucun contr�le de validit� n'est effectu� (la zone retourn�e peut "d�border" du monde)
     */
    template<class TGroup, class TController>
    UTI::Rect GenericEnvironmentBrush<TGroup, TController>::extentForEdition(int pX, int pY) const
    {
        unsigned short  lSize = this->size();

        return UTI::Rect(UTI::Pointf(F32(pX - (lSize - 1) / 2), F32(pY + (lSize - 1) / 2)), UTI::Sizef(F32(lSize), F32(lSize)));
    }


    /*! Permet de r�cup�rer la zone sous l'apparence du pinceau (qui n'est pas forc�ment la m�me que la zone �dit�e).
        @param pX Abscisse du centre de la zone
        @param pY Ordonn�e du centre de la zone
        @return La zone sous l'apparence du pinceau
        @note A ce niveau, aucun contr�le de validit� n'est effectu� (la zone retourn�e peut "d�border" du monde)
     */
    template<class TGroup, class TController>
    UTI::Rect GenericEnvironmentBrush<TGroup, TController>::extentUnderModel(int pX, int pY) const
    {
        unsigned short  lSize = this->size();

        return UTI::Rect(UTI::Pointf(F32(pX - (lSize - 1) / 2), F32(pY + (lSize - 1) / 2)), UTI::Sizef(F32(lSize + 1), F32(lSize + 1)));
    }


    /*! Pour l'apparence du pinceau d'environnement, la zone valide est �tendue d'une unit� � droite et en bas.
        @return La zone valide pour l'apparence du pinceau
     */
    template<class TGroup, class TController>
    UTI::Rect GenericEnvironmentBrush<TGroup, TController>::validExtentForModel() const
    {
        UTI::Rect    lExtent = this->workingExtent();
        lExtent.setWidth(lExtent.width() + k1F);
        lExtent.setHeight(lExtent.height() + k1F);

        return lExtent;
    }


    /*! L'apparence du pinceau est d�termin�e en partie par les poids associ�s aux �l�vations. Cette m�thode permet de transformer ces poids
        (de mani�re tr�s simple).
        @param pWeights Poids associ�s aux �l�vations sous le pinceaux
     */
    template<class TGroup, class TController>
    void GenericEnvironmentBrush<TGroup, TController>::transformModel(TWeights& pWeights) const
    {
        for(auto lWI = pWeights.begin(); lWI != pWeights.end(); ++lWI)
        {
            if (*lWI < 0.45F)
            {
                *lWI = k0F;
            }
        }
    }
}
