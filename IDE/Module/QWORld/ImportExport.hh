/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_IMPORTEXPORT_HH
#define QWOR_IMPORTEXPORT_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/ImportExport.hh
    @brief En-t�te de la classe QWOR::ImportExport.
    @author @ref Guillaume_Terrissol
    @date 31 Mai 2014 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATA/QPAcKage/ImportExport.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de monde.
        @version 0.3

        Factorisation de code li� � l'import/export de mondes.
     */
    class ImportExport : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExport();                                                //!< Destructeur.

    protected:

        void    exportHeader(QXmlStreamWriter& pWriter, PAK::Loader& pLoader);  //!< Donn�es d'en-t�te communes.
    };
}

#endif  // De QWOR_IMPORTEXPORT_HH
