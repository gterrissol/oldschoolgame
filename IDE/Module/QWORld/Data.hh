/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_DATA_HH
#define QWOR_DATA_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/Data.hh
    @brief En-t�te de la classe QWOR::Data.
    @author @ref Guillaume_Terrissol
    @date 3 D�cembre 2008 - 26 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

#include "PAcKage/Handle.hh"
#include "STL/STL.hh"
#include "TILE/BaseTile.hh"
#include "TILE/HeightField.hh"
#include "WORld/BaseWorld.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    using   Height      = TILE::PieceOfData<int16_t, TILE::Height::TType>;  //!< El�vation de monde ext�rieur.
    using   HeightGroup = TILE::Group<Height>;                              //!< Groupe d'�l�vations de monde ext�rieur.
    using   Texel       = TILE::PieceOfData<int16_t, TILE::Texel::TType>;   //!< Texel de monde ext�rieur.
    using   TexelGroup  = TILE::Group<Texel>;                               //!< Groupe de texels de monde ext�rieur.


//------------------------------------------------------------------------------
//                               Donn�es de Monde
//------------------------------------------------------------------------------

    /*! @brief Donn�es communes de mondes.
        @version 0.4

     */
    class Data
    {
        BEFRIEND_STD_ALLOCATOR()
    public:
        //! @name Types utilis�s
        //@{
        using   THeight     = Height::TType;                                    //!< Type des �l�vations de tuile.
        using   TTexel      = Texel::TType;                                     //!< Type manipulable des fragments de texture de tuile.
        using   TOffset     = uint16_t;                                         //!< Type d'offset pour r�f�rencer les tuiles.
        using   TileMgrPtr  = TILE::TileMgr::Ptr;                               //!< Gestionnaire de tuiles.
        //@}
        //! @name Types de pointeur
        //@{
        using Ptr       = std::shared_ptr<Data>;                                //!< 
        using TilePtr   = std::shared_ptr<TILE::Tile>;                          //!< 
        //@}
                        ~Data();                                                //!< Destructeur.
        TileMgrPtr      tileMgr() const;                                        //!< Gestionnaire de tuiles.
        //! @name Zone
        //@{
        void            moveData(const UTI::Rect& pToExtent);                   //!< D�placement des donn�es suite � un redimensionnement.
        UTI::Rect       tileDataZone() const;                                   //!< Zone couverte par les tuiles.
        //@}
        // !@name El�vations
        //@{
        uint32_t        elevationCount() const;                                 //!< Nombre d'�l�vations [de coin de tuile].
        THeight         elevation(uint32_t pN) const;                           //!< Acc�s � une �l�vation [de coin de tuile].
        THeight         elevation(TOffset pTileX, TOffset pTileY) const;        //!< Acc�s � une �l�vation [de coin de tuile].
        void            setElevation(uint32_t pN, THeight pNew);                //!< Edition d'une �l�vation [de coin de tuile].
        //@}
        //! @name "Texels"
        //@{
        uint32_t        texelCount() const;                                     //!< Nombre de fragments [de texture de tuile].
        TTexel          texel(uint32_t pN) const;                               //!< Acc�s � un fragment [de texture de tuile].
        TTexel          texel(TOffset pTexelX, TOffset pTexelY) const;          //!< Acc�s � un fragment [de texture de tuile].
        void            setTexel(uint32_t pN, TTexel pNew) const;               //!< Edition d'un fragment [de texture de tuile].
        //@}
        //! @name Tuiles
        //@{
        TILE::Key       tileKey(TOffset pTileX, TOffset pTileY) const;          //!< Clef de ressource tuile.
        TilePtr         tile(float pX, float pY);                               //!< R�cup�ration d'une tuile.
        void            lockTile(TOffset pTileX, TOffset pTileY, bool pOn);     //!< [D�]Verrouillage d'une tuile.
        bool            isTileLocked(TOffset pTileX, TOffset pTileY) const;     //!< Possibilit� d'�dition d'une tuile.
        void            modifyTile(TOffset pTileX, TOffset pTileY, bool pOn);   //!< [D�]Modification d'une tuile.
        bool            isTileModified(TOffset pTileX, TOffset pTileY) const;   //!< Tuile modifi�e ?
        //@}
    private:

                        Data(PAK::FileHdl pGroup, OSGi::Context* pContext);     //!< Constructeur.
        FORBID_COPY(Data)
        FREE_PIMPL()
    };
}

#endif  // De QWOR_DATA_HH
