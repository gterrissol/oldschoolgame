/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Brushes.hh"

/*! @file IDE/Module/QWORld/Brushes.cc
    @brief M�thodes (non-inline) des classes QWOR::Brushes::BrushesPrivate & QWOR::Brushes.
    @author @ref Guillaume_Terrissol
    @date 9 Novembre 2005 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QButtonGroup>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QMap>
#include <QRadioButton>
#include <QPushButton>
#include <QVBoxLayout>

#include "APPearance/BaseAppearance.hh"
#include "CoLoR/Colors.hh"
#include "CoLoR/RGB.hh"
#include "CoLoR/RGBA.hh"
#include "EDITion/Requests.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "MATH/M4.hh"
#include "Module/QT3D/Widget.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/Shared.tcc"
#include "TILE/Fragment.hh"
#include "TILE/HeightField.tcc"

#include "Brush.hh"
#include "Brushes.ui.hh"
#include "Const.hh"
#include "Data.hh"
#include "EditSurfaceMode.hh"
#include "ErrMsg.hh"


namespace QWOR
{
//------------------------------------------------------------------------------
//                             Apparence du Pinceau
//------------------------------------------------------------------------------

    /*! @brief Rendu visuel d'un pinceau
        @version 0.6

        Afin de repr�senter visuellement le pinceau (pour bien voir sa zone d'application), il a fallu cr�er un renderer sp�cifique
        (l'�diteur de monde ext�rieur n'affichant, par d�faut, que son objet �dit�).
     */
    class BrushRenderer : public RDR::Renderer, public STL::enable_shared_from_this<BrushRenderer>
    {
    public:
        //! @name Type de pointeur
        //@{
        using ConstBrushPtr = std::weak_ptr<Brush const>;   //!< Pointeur (constant) sur pinceau.
        //@}
        //! @name Constructeur & destructeur
        //@{
                BrushRenderer(const QT3D::Widget* pView);   //!< Constructeur par d�faut.
virtual         ~BrushRenderer();                           //!< Destructeur.
        //@}
        void    bindBrush(ConstBrushPtr pBrush);            //!< Liaison avec un pinceau.

    private:
        //! @name Rendu
        //@{
virtual void    dumpAppearances();                          //!< Pr�paration du rendu.
        //@}
        ConstBrushPtr       mBrush;                         //!< Pinceau � 'afficher'.
        const QT3D::Widget* mView;                          //!< Widget utilisant ce renderer.
    };


//------------------------------------------------------------------------------
//               Apparence du Pinceau : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    BrushRenderer::BrushRenderer(const QT3D::Widget* pView)
        : mBrush()
        , mView(pView)
    {
        const_cast<QT3D::Widget*>(mView)->attach(shared_from_this());
    }


    /*! Destructeur.
     */
    BrushRenderer::~BrushRenderer()
    {
        const_cast<QT3D::Widget*>(mView)->detach(shared_from_this());
    }


//------------------------------------------------------------------------------
//                 Apparence du Pinceau : Autre M�thode Publique
//------------------------------------------------------------------------------

    /*! Liaison avec un pinceau.
     */
    void BrushRenderer::bindBrush(ConstBrushPtr pBrush)
    {
        mBrush = pBrush;
        // Force la mise � jour de la fen�tre (pour cause de disparition ou apparition du pinceau).
        const_cast<QT3D::Widget*>(mView)->update();
    }


//------------------------------------------------------------------------------
//                         Apparence du Pinceau : Rendu
//------------------------------------------------------------------------------

    /*! Pr�paration du rendu.
     */
    void BrushRenderer::dumpAppearances()
    {
        if (!mBrush.expired())
        {
            Brush::AppearancePtr lModel = mBrush.lock()->retrieveModel();

            if (lModel != nullptr)
            {
                MATH::M4   lPosition;
                lPosition.loadIdentity();
                F32         lScale = F32(mBrush.lock()->planeScale());
                lPosition.init(lPosition.I * lScale, lPosition.J * lScale, lPosition.K /** lScale*/, lPosition.L);

                pushAppearance(lModel, lPosition);
            }
        }
    }


//------------------------------------------------------------------------------
//                      BrushesPrivate : P-Impl de Brushes
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Brushes.
        @version 0.4

     */
    class Brushes::BrushesPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Brushes)
    public:
        //! @name Types de pointeur
        //@{
        using BrushablePtr          = std::weak_ptr<Brushable>;             //!< Pointeur sur pinceau.
        using ControllerPtr         = std::weak_ptr<EDIT::Controller>;      //!< Pointeur sur contr�leur d'objet �dit�.
        using BrushPtr              = std::shared_ptr<Brush>;               //!< Pointeur sur pinceau.
        using WeakBrushPtr          = std::weak_ptr<Brush>;                 //!< Pointeur sur pinceau.
        using BrushRendererPtr      = std::shared_ptr<BrushRenderer>;       //!< Pointeur sur renderer de pinceau.
        using EditSurfaceModePtr    = std::shared_ptr<EditSurfaceMode>;     //!< Pointeur sur mode d'�dition de surface.
        using EnvironmentBrushPtr   = std::shared_ptr<EnvironmentBrush>;    //!< Pointeur sur pinceau d'environnement.
        using CliffBrushPtr         = std::shared_ptr<CliffBrush>;          //!< Pointeur sur pinceau de falaise pour tuile.
        using SlideBrushPtr         = std::shared_ptr<SlideBrush>;          //!< Pointeur sur pinceau d'altitude pour tuile.
        using DropBrushPtr          = std::shared_ptr<DropBrush>;           //!< Pointeur sur pinceau de d�p�t d'objets.
        //@}
        //! @name "Constructeurs" & destructeur
        //@{
                        BrushesPrivate(Brushes*       parent,
                                       OSGi::Context* pContext);            //!< Constructeur.
        void            init();                                             //!< Initialisation.
                        ~BrushesPrivate();                                  //!< Destructeur.
        //@}
        void            cleanBrushes();                                     //!< Suppression des pinceaux.
        bool            isEditingOutside() const;                           //!< Edition d'un monde ext�rieur ?
        bool            isEditingTile() const;                              //!< Edition d'une tuile ?
        void            setWorkingExtent(BrushablePtr pBrushable);          //!< D�finition de la "zone de travail".

        OSGi::Context*                  mContext;
        Ui::Brushes                     mUI;                                //!< Interface cr��e par Designer.
        //! @name Groupes de boutons
        //@{
        void                            updateEnvironmentButtons();         //!< 
        void                            removeEnvironmentButtons();         //!< 
        QButtonGroup*                   mEnvironmentButtons;                //!< Groupe des boutons de choix d'environnements.
        QButtonGroup*                   mMasksButtons;                      //!< Groupe des boutons de choix de m�lange.
        QButtonGroup*                   mCliffButtons;                      //!< Groupe des boutons de choix de falaises.
        QButtonGroup*                   mAltitudeButtons;                   //!< Groupe des boutons de choix de "pentes".
        QButtonGroup*                   mDropButtons;                       //!< Groupe de widgets de choix d'objets � d�poser.
        QButtonGroup*                   mSizeButtons;                       //!< Groupe des boutons de choix de taille.
        QButtonGroup*                   mShapeButtons;                      //!< Groupe des boutons de choix de forme.
        //@}
        QT3D::Viewer3D*                 mObjectViewer;                      //!< 
        //! @name Sujets
        //@{
        BrushablePtr                    mBrushable;                         //!< 
        ControllerPtr                   mEdited;                            //!< 
        //@}
        //! @name Pinceaux
        //@{
        EnvironmentBrushPtr             mTextureBrush;                      //!< ... de textures.
        CliffBrushPtr                   mCliffBrush;                        //!< ... de falaises.
        SlideBrushPtr                   mSmoothBrush;                       //!< ... 'analogique'.
        DropBrushPtr                    mDropBrush;                         //!< ... de d�p�t d'objets.

        WeakBrushPtr                    mCurrentBrush;                      //!< ... actuel.
        //@}
        //! @name Outils
        //@{
        QMap<int, EditSurfaceModePtr>   mSurfaceModes;                      //!< 
        EditSurfaceModePtr              mSurfaceMode;                       //!< 
        BrushRendererPtr                mBrushRenderer;                     //!< 
        //@}
        AppearancePtr                   mDropedObject;                      //!< 
    };


//------------------------------------------------------------------------------
//                BrushesPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    Brushes::BrushesPrivate::BrushesPrivate(Brushes* parent, OSGi::Context* pContext)
        : q{parent}
        , mContext{pContext}
        , mUI{}
        , mEnvironmentButtons{}
        , mMasksButtons{}
        , mCliffButtons{}
        , mAltitudeButtons{}
        , mDropButtons{}
        , mSizeButtons{}
        , mShapeButtons{}
        , mObjectViewer{}
        , mEdited{}
        , mTextureBrush{}
        , mCliffBrush{}
        , mSmoothBrush{}
        , mDropBrush{}
        , mCurrentBrush{}
        , mSurfaceModes{}
        , mSurfaceMode{}
        , mBrushRenderer{}
        , mDropedObject{}
    { }


    /*!
     */
    void Brushes::BrushesPrivate::init()
    {
        // Construit l'interface.
        q->from(q);
        mUI.setupUi(q);
        q->setObjectName(QString::null);    // Le widget pourra �tre correctement renomm� par UIService.

        // Cr�e les groupes de boutons et les connecte.
        mEnvironmentButtons = new QButtonGroup(mUI.envBrushes);
        mEnvironmentButtons->setExclusive(true);
        q->connect(mEnvironmentButtons, SIGNAL(buttonClicked(int)), SLOT(environmentChoice(int)));
        mMasksButtons = new QButtonGroup(mUI.masks);
        mMasksButtons->addButton(mUI.fullM,   TILE::eFull);
        mMasksButtons->addButton(mUI.hardM,   TILE::eHard);
        mMasksButtons->addButton(mUI.smoothM, TILE::eSmooth);
        mMasksButtons->addButton(mUI.softM,   TILE::eSoft);
        mMasksButtons->addButton(mUI.blurryM, TILE::eBlurry);
        mMasksButtons->addButton(mUI.emptyM,  TILE::eEmpty);
        q->connect(mMasksButtons, SIGNAL(buttonClicked(int)), SLOT(blendingChoice(int)));

        //----------
        mCliffButtons       = new QButtonGroup(mUI.cliffs);
        mCliffButtons->addButton(mUI.decrease2, CliffBrush::eDecreaseTwo);
        mCliffButtons->addButton(mUI.decrease1, CliffBrush::eDecreaseOne);
        mCliffButtons->addButton(mUI.nocrease,  CliffBrush::eSameLevel);
        mCliffButtons->addButton(mUI.increase1, CliffBrush::eIncreaseOne);
        mCliffButtons->addButton(mUI.increase2, CliffBrush::eIncreaseTwo);
        q->connect(mCliffButtons, SIGNAL(buttonClicked(int)), SLOT(cliffChoice(int)));

        //----------
        mAltitudeButtons    = new QButtonGroup(mUI.altitudes);
        mAltitudeButtons->addButton(mUI.raise,   SlideBrush::eRaise);
        mAltitudeButtons->addButton(mUI.lower,   SlideBrush::eLower);
        mAltitudeButtons->addButton(mUI.plateau, SlideBrush::ePlateau);
        mAltitudeButtons->addButton(mUI.noise,   SlideBrush::eNoise);
        mAltitudeButtons->addButton(mUI.smooth,  SlideBrush::eSmooth);
        q->connect(mAltitudeButtons, SIGNAL(buttonClicked(int)), SLOT(altitudeChoice(int)));

        //----------
        mDropButtons        = new QButtonGroup(mUI.objects);
        mDropButtons->addButton(mUI.drop);
        mDropButtons->addButton(mUI.random);
        mObjectViewer       = new QT3D::Viewer3D(mContext, mUI.viewFrame);
        QGridLayout*    lL  = new QGridLayout(mUI.viewFrame);
        lL->setMargin(0);
        lL->addWidget(mObjectViewer, 0, 0, 1, 1);

        //----------
        mSizeButtons         = new QButtonGroup(mUI.sizes);
        mSizeButtons->addButton(mUI.size1, 1);
        mSizeButtons->addButton(mUI.size2, 2);
        mSizeButtons->addButton(mUI.size3, 3);
        mSizeButtons->addButton(mUI.size5, 5);
        mSizeButtons->addButton(mUI.size8, 8);
        q->connect(mSizeButtons, SIGNAL(buttonClicked(int)), SLOT(sizeChoice(int)));

        //----------
        mShapeButtons        = new QButtonGroup(mUI.shapes);
        mShapeButtons->addButton(mUI.round,  Brush::eRound);
        mShapeButtons->addButton(mUI.square, Brush::eSquare);
        q->connect(mShapeButtons, SIGNAL(buttonClicked(int)), SLOT(shapeChoice(int)));

        //----------
        q->connect(mUI.environments, SIGNAL(toggled(bool)), q, SLOT(brushChoiceChanged()));
        q->connect(mUI.cliffs,       SIGNAL(toggled(bool)), q, SLOT(brushChoiceChanged()));
        q->connect(mUI.altitudes,    SIGNAL(toggled(bool)), q, SLOT(brushChoiceChanged()));
        q->connect(mUI.objects,      SIGNAL(toggled(bool)), q, SLOT(brushChoiceChanged()));
    }


    /*! Destructeur.
     */
    Brushes::BrushesPrivate::~BrushesPrivate()
    {
        cleanBrushes();
    }


//------------------------------------------------------------------------------
//                      BrushesPrivate : 
//------------------------------------------------------------------------------

    /*!
     */
    void Brushes::BrushesPrivate::cleanBrushes()
    {
        mBrushRenderer  = nullptr;

        mTextureBrush   = nullptr;
        mCliffBrush     = nullptr;
        mSmoothBrush    = nullptr;
        mDropBrush      = nullptr;
    }


    /*!
     */
    void Brushes::BrushesPrivate::setWorkingExtent(BrushablePtr pBrushable)
    {
        if (auto lBrushable = pBrushable.lock())
        {
            mTextureBrush->setExtent(lBrushable->textureExtent());
            mCliffBrush->setExtent(lBrushable->worldExtent());
            mSmoothBrush->setExtent(lBrushable->worldExtent());
            mDropBrush->setExtent(lBrushable->worldExtent());
        }
        else
        {
            mTextureBrush->setExtent(UTI::Rect());
            mCliffBrush->setExtent(UTI::Rect());
            mSmoothBrush->setExtent(UTI::Rect());
            mDropBrush->setExtent(UTI::Rect());
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! 
     */
    void Brushes::BrushesPrivate::updateEnvironmentButtons()
    {
        QPixmap         lPixmap(16, 16);
        QGridLayout*    lLayout         = qobject_cast<QGridLayout*>(mUI.envBrushes->layout());
        const int       kMaxButtonOnRow = 4;
        unsigned short lCurrentButton   = 0;

        for(unsigned short lT = 0; lT < TILE::FragmentMgr::get()->tilingCount(); ++lT)
        {
            CLR::RGBA      lColor  = CLR::RGBA(TILE::FragmentMgr::get()->tilingColor(TILE::Tiling::Id(lT)));
            if (lColor != CLR::RGBA(CLR::kWhite))
            {
                // Tiling actif : bouton associ�.
                if (mEnvironmentButtons->buttons().count() <= lCurrentButton)
                {
                    lPixmap.fill(QColor::fromRgbF(lColor.r(), lColor.g(), lColor.b()));
                    QPushButton*    lButton = new QPushButton(QIcon(lPixmap), "", mUI.envBrushes);
                    lButton->setCheckable(true);
                    lButton->setChecked(lT == 0);
                    lButton->setMinimumSize(QSize(mUI.envBrushes->width() / 4, lButton->minimumSize().height()));
                    lButton->setMaximumSize(QSize(mUI.envBrushes->width() / 4, lButton->maximumSize().height()));
                    mEnvironmentButtons->addButton(lButton, lT);
                    lLayout->addWidget(lButton, lT / kMaxButtonOnRow, lT % kMaxButtonOnRow);
                    ++lCurrentButton;
                }
                else
                {
                    if (QPushButton* lButton = qobject_cast<QPushButton*>(mEnvironmentButtons->button(lCurrentButton)))
                    {
                        lPixmap.fill(QColor::fromRgbF(lColor.r(), lColor.g(), lColor.b()));
                        lButton->setIcon(QIcon(lPixmap));
                        ++lCurrentButton;
                    }
                }
            }
        }
        for(unsigned short lT = lCurrentButton; lT < mEnvironmentButtons->buttons().count(); ++lT)
        {
            QAbstractButton* lButton = mEnvironmentButtons->button(lT);
            mEnvironmentButtons->removeButton(lButton);
            delete lButton;
        }
    }


    /*!
     */
    void Brushes::BrushesPrivate::removeEnvironmentButtons()
    {
        for(auto * lButton : mEnvironmentButtons->buttons())
        {
            mEnvironmentButtons->removeButton(lButton);
            delete lButton;
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Brushes::Brushes(OSGi::Context* pContext)
        : Module::Editor{}
        , EDIT::Observer{}
        , CORE::Translate<Brushes>{}
        , CORE::Synchronized{"file + enabled + edition + out; til"}
        , pthis{this, pContext}
    {
        pthis->init();
    }


    /*! Destructeur
     */
    Brushes::~Brushes() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Brushes::updateEnvironmentBrushes()
    {
        pthis->updateEnvironmentButtons();
    }


    /*!
     */
    void Brushes::removeEnvironmentBrushes()
    {
        pthis->removeEnvironmentButtons();
    }


    /*!
     */
    void Brushes::setDropModel(AppearancePtr pAppearance)
    {
        pthis->mDropedObject = pAppearance;
        pthis->mObjectViewer->display(pthis->mDropedObject);

        if (pthis->mDropBrush != nullptr)
        {
            pthis->mDropBrush->setModel(pthis->mDropedObject);
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Brushes::changeEvent(QEvent* pEvent)
    {
        Module::Editor::changeEvent(pEvent);

        if (pEvent->type() == QEvent::EnabledChange)
        {
            if (isEnabled())
            {
                updateEnvironmentBrushes(); // Pour la premi�re mise � jour, entre autres.
                if (pthis->mBrushRenderer != nullptr)
                {
                    pthis->setWorkingExtent(pthis->mBrushable);
                    pthis->mBrushRenderer->bindBrush(pthis->mCurrentBrush.lock());
                }
            }
            else
            {
                if (pthis->mBrushRenderer != nullptr)
                {
                    pthis->setWorkingExtent(BrushesPrivate::BrushablePtr());
                    pthis->mBrushRenderer->bindBrush(BrushesPrivate::BrushPtr());
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                              Brushes : Slots
//------------------------------------------------------------------------------

    /*! S�lection d'un type de pinceau.
     */
    void Brushes::brushChoiceChanged()
    {
        QGroupBox*                  lGroup = qobject_cast<QGroupBox*>(sender());
        BrushesPrivate::BrushPtr    lBrush{};

        // Agit sur l'activation d'un groupe, pas sur sa d�sactivation (sauf le cas tr�s particulier o� plus aucun groupe n'est s�lectionn�).
        if (!lGroup->isChecked())
        {
            if (!pthis->mUI.environments->isChecked() && !pthis->mUI.cliffs->isChecked() && !pthis->mUI.altitudes->isChecked() && !pthis->mUI.objects->isChecked())
            {
                // Plus de groupe s�lectionn� : d�sactive le mode.
                QT3D::Widget*  l3DWindow = dynamic_cast<QT3D::Widget*>(worldView());
                l3DWindow->activateMode(0);
            }
        }
        else
        {
            QT3D::Widget*  l3DWindow = dynamic_cast<QT3D::Widget*>(worldView());
            l3DWindow->activateMode(pthis->mBrushable.lock()->type());

            if      (lGroup == pthis->mUI.environments)
            {
                pthis->mUI.cliffs->setChecked(false);
                pthis->mUI.altitudes->setChecked(false);
                pthis->mUI.objects->setChecked(false);
                lBrush = pthis->mTextureBrush;

                environmentChoice(pthis->mEnvironmentButtons->checkedId());
                blendingChoice(pthis->mMasksButtons->checkedId());
            }
            else if (lGroup == pthis->mUI.cliffs)
            {
                pthis->mUI.environments->setChecked(false);
                pthis->mUI.altitudes->setChecked(false);
                pthis->mUI.objects->setChecked(false);
                lBrush = pthis->mCliffBrush;

                cliffChoice(pthis->mCliffButtons->checkedId());
            }
            else if (lGroup == pthis->mUI.altitudes)
            {
                pthis->mUI.environments->setChecked(false);
                pthis->mUI.cliffs->setChecked(false);
                pthis->mUI.objects->setChecked(false);
                lBrush = pthis->mSmoothBrush;

                altitudeChoice(pthis->mAltitudeButtons->checkedId());
            }
            else if (lGroup == pthis->mUI.objects)
            {
                pthis->mUI.altitudes->setChecked(false);
                pthis->mUI.cliffs->setChecked(false);
                pthis->mUI.environments->setChecked(false);
                lBrush = pthis->mDropBrush;

                dropChoice(pthis->mDropButtons->checkedId());
            }
        }

        // Autres mises � jour.
        if ((pthis->mSurfaceMode != nullptr) && (pthis->mBrushRenderer != nullptr))
        {
            pthis->mSurfaceMode->setBrush(lBrush);
            pthis->mBrushRenderer->bindBrush(lBrush);
        }
    }


    /*!
     */
    void Brushes::environmentChoice(int pEnvID)
    {
        ASSERT_EX(pthis->mTextureBrush != nullptr, kNoEnvironmentBrush, return;)

        if (pEnvID == -1)
        {
            return;
        }

        pthis->mTextureBrush->setTilingId(static_cast<TILE::TilingId>(pEnvID));
        pthis->mSurfaceMode->setBrush(pthis->mTextureBrush);
        pthis->mBrushRenderer->bindBrush(pthis->mTextureBrush);
        pthis->mCurrentBrush = pthis->mTextureBrush;
    }


    /*! @note L'initialisation du pinceau d'environnement a d� �tre d�j� r�alis� par environmentChoice().
     */
    void Brushes::blendingChoice(int pBlending)
    {
        ASSERT_EX(pthis->mTextureBrush != nullptr, kNoEnvironmentBrush, return;)

        if (pBlending == -1)
        {
            return;
        }

        // NB: Il faudrait n'autoriser que TILE::eFull pour le monde ext�rieur.
        pthis->mTextureBrush->setBlending(TILE::EBlending(pBlending));
    }


    /*! S�lection d'un bouton de falaise.
     */
    void Brushes::cliffChoice(int pCliffID)
    {
        ASSERT_EX(pthis->mCliffBrush, kNoCliffBrush, return;)

        pthis->mCliffBrush->setCliff(CliffBrush::ECliffs(pCliffID));
        pthis->mSurfaceMode->setBrush(pthis->mCliffBrush);
        pthis->mBrushRenderer->bindBrush(pthis->mCliffBrush);

        pthis->mCurrentBrush = pthis->mCliffBrush;
    }


    /*! S�lection d'un bouton d'altitude.
     */
    void Brushes::altitudeChoice(int pAltID)
    {
        ASSERT_EX(pthis->mSmoothBrush != nullptr, kNoSmoothBrush, return;)

        pthis->mSmoothBrush->setSmoothing(SlideBrush::ESmoothing(pAltID));
        pthis->mSurfaceMode->setBrush(pthis->mSmoothBrush);
        pthis->mBrushRenderer->bindBrush(pthis->mSmoothBrush);

        pthis->mCurrentBrush = pthis->mSmoothBrush;
    }


    /*! S�lection d'un bouton d'altitude.
     */
    void Brushes::dropChoice(int pDropID)
    {
        ASSERT_EX(pthis->mDropBrush != nullptr, kNoDropBrush, return;)

        pthis->mDropBrush->setType(DropBrush::EType(pDropID));

        if (pthis->mDropedObject != nullptr)
        {
            pthis->mDropBrush->setModel(pthis->mDropedObject);
        }
        pthis->mSurfaceMode->setBrush(pthis->mDropBrush);
        pthis->mBrushRenderer->bindBrush(pthis->mDropBrush);

        pthis->mCurrentBrush = pthis->mDropBrush;
    }


    /*! S�lection d'un bouton de taille.
     */
    void Brushes::sizeChoice(int pSize)
    {
        ASSERT_EX(!pthis->mEdited.expired(), kNoActiveSubject, return;)

        pthis->mCliffBrush->setSize(pSize);
        pthis->mSmoothBrush->setSize(pSize);
        pthis->mTextureBrush->setSize(pSize);
        pthis->mDropBrush->setSize(pSize);
    }


    /*! S�lection d'un bouton de forme.
     */
    void Brushes::shapeChoice(int pShapeID)
    {
        ASSERT_EX(!pthis->mEdited.expired(), kNoActiveSubject, return;)

        pthis->mCliffBrush->setShape(Brush::EShape(pShapeID));
        pthis->mSmoothBrush->setShape(Brush::EShape(pShapeID));
        pthis->mTextureBrush->setShape(Brush::EShape(pShapeID));
        pthis->mDropBrush->setShape(Brush::EShape(pShapeID));
    }


//------------------------------------------------------------------------------
//                          Pertinence du Sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Brushes::canManage(EDIT::Controller::Ptr pDatum) const
    {
        bool    lCanManage = (std::dynamic_pointer_cast<Brushable>(       pDatum) != nullptr) &&
                             (std::dynamic_pointer_cast<EDIT::Controller>(pDatum) != nullptr);

        return lCanManage;
    }


    /*!
     */
    void Brushes::manage(EDIT::Controller::Ptr pDatum)
    {
        QT3D::Widget* l3DWindow = dynamic_cast<QT3D::Widget*>(worldView());

        pthis->mBrushable   = std::dynamic_pointer_cast<Brushable>(pDatum);
        pthis->mEdited      = std::dynamic_pointer_cast<EDIT::Controller>(pDatum);

        if (!pthis->mBrushable.expired() && !pthis->mEdited.expired())
        {
            EDIT::Controller::Ptr   lEdited     = pthis->mEdited.lock();
            EDIT::Subject*          lSubject    = EDIT::Controller::subject(lEdited);
            watch(lSubject);

            BrushesPrivate::CliffBrushPtr         lNewCliffBrush   = pthis->mBrushable.lock()->cliffBrush();
            BrushesPrivate::SlideBrushPtr         lNewSmoothBrush  = pthis->mBrushable.lock()->slideBrush();
            BrushesPrivate::EnvironmentBrushPtr   lNewTextureBrush = pthis->mBrushable.lock()->environmentBrush();
            BrushesPrivate::DropBrushPtr          lNewDropBrush    = pthis->mBrushable.lock()->dropBrush();

            int lBrushableType = pthis->mBrushable.lock()->type();
            if (!pthis->mSurfaceModes.contains(lBrushableType))
            {
                QString lModeName   = QString("%1 : %2").arg(kEditSurfaceMode).arg(typeid(*pthis->mEdited.lock()).name());
                pthis->mSurfaceModes[lBrushableType] = pthis->mSurfaceMode = l3DWindow->buildMode<EditSurfaceMode>(lBrushableType, lModeName);
            }
            else
            {
                pthis->mSurfaceMode = pthis->mSurfaceModes[lBrushableType];

                // Si le mode �tait actif (i.e. en application), il faut r�cup�rer la configuration.
                if (pthis->mSurfaceMode->isActive())
                {
                    lNewTextureBrush->forwardState(pthis->mTextureBrush);
                    lNewCliffBrush->forwardState(pthis->mCliffBrush);
                    lNewSmoothBrush->forwardState(pthis->mSmoothBrush);
                    lNewDropBrush->forwardState(pthis->mDropBrush);
                }
            }

            pthis->mTextureBrush = lNewTextureBrush;
            pthis->mCliffBrush   = lNewCliffBrush;
            pthis->mSmoothBrush  = lNewSmoothBrush;
            pthis->mDropBrush    = lNewDropBrush;
            pthis->setWorkingExtent(pthis->mBrushable);

            if (pthis->mBrushRenderer == nullptr)
            {
                pthis->mBrushRenderer = std::make_shared<BrushRenderer>(l3DWindow);
            }
        }
        else
        {
            // l3DWindow->activateMode(0); ?
            return;
        }

        // Etat de l'interface.
        if      (pthis->mUI.environments->isChecked())
        {
            environmentChoice(pthis->mEnvironmentButtons->checkedId());
            blendingChoice(pthis->mMasksButtons->checkedId());
        }
        else if (pthis->mUI.cliffs->isChecked())
        {
            cliffChoice(pthis->mCliffButtons->checkedId());
        }
        else if (pthis->mUI.altitudes->isChecked())
        {
            altitudeChoice(pthis->mAltitudeButtons->checkedId());
        }
        else if (pthis->mUI.objects->isChecked())
        {
            dropChoice(pthis->mDropButtons->checkedId());
        }
        sizeChoice(pthis->mSizeButtons->checkedId());
        shapeChoice(pthis->mShapeButtons->checkedId());

        l3DWindow->activateMode(pthis->mBrushable.lock()->type());
        l3DWindow->update();
    }


    /*!
     */
    void Brushes::manageNone()
    {
        Q_D(const Brushes);

        BrushesPrivate*    lD = const_cast<BrushesPrivate*>(d);
        lD->cleanBrushes();
        lD->mBrushable.reset();
        lD->mEdited.reset();

        const_cast<Brushes*>(this)->watch(nullptr);
        lD->mSurfaceMode = nullptr;
        lD->mBrushRenderer = nullptr;
    }


//------------------------------------------------------------------------------
//                          Ecoute du Sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Brushes::listen(const EDIT::Answer* /*answer*/)
    {
        pthis->setWorkingExtent(pthis->mBrushable);
    }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    CORE::Translated::Updater Brushes::buildTranslater()
    {
        return [=]()
        {
            pthis->mUI.retranslateUi(this);
        };
    }
}
