/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Data.hh"

/*! @file IDE/Module/QWORld/Data.cc
    @brief M�thodes (non-inline) de la classe QWOR::Data.
    @author @ref Guillaume_Terrissol
    @date 3 D�cembre 2008 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

//---------------------------------------------------------------------------
//                                  En-t�tes
//---------------------------------------------------------------------------

#include <bitset>
#include <cstdint>
#include <cstdlib>
#include <deque>
#include <vector>

#include <QColor>
#include <QImage>
#include <QString>
#include <QStringList>

#include "CORE/GameService.hh"
#include "CORE/ProgressService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/FileService.hh"
#include "DATA/Store.hh"
#include "MATerial/Enum.hh"
#include "PAcKage/LoaderSaver.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "TILE/BaseTile.hh"
#include "TILE/HeightField.tcc"
#include "UTIlity/FileMapping.hh"
#include "UTIlity/Rect.hh"

#include "Enum.hh"
#include "ErrMsg.hh"
#include "WorldDataService.hh"

namespace
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pCorners Coin de la tuile (NW, NE, SW, SE - dans cet ordre) pour laquelle g�n�rer les �l�vations
        @return Les �l�vations pour la tuile
     */
    TILE::HeightGroup generateTileHeights(const TILE::HeightGroup& pCorners)
    {
        //                                            Ligne                 Colonne
        TILE::Height::TType::TType lElevations[TILE::eEdgeSize + 1][TILE::eEdgeSize + 1];

        lElevations[0]              [0]                 = pCorners[0].data();
        lElevations[0]              [TILE::eEdgeSize]   = pCorners[1].data();
        lElevations[TILE::eEdgeSize][0]                 = pCorners[2].data();
        lElevations[TILE::eEdgeSize][TILE::eEdgeSize]   = pCorners[3].data();

        // G�n�re des �l�vations sur les bords.
        for(short lE = 1; lE < TILE::eEdgeSize; ++lE)
        {
            lElevations[0]              [lE] = lElevations[0]              [0] + lE * (lElevations[0]              [TILE::eEdgeSize] - lElevations[0]              [0]) / TILE::eEdgeSize;
            lElevations[TILE::eEdgeSize][lE] = lElevations[TILE::eEdgeSize][0] + lE * (lElevations[TILE::eEdgeSize][TILE::eEdgeSize] - lElevations[TILE::eEdgeSize][0]) / TILE::eEdgeSize;

            lElevations[lE]              [0] = lElevations[0]              [0] + lE * (lElevations[TILE::eEdgeSize]              [0] - lElevations[0]              [0]) / TILE::eEdgeSize;
            lElevations[lE][TILE::eEdgeSize] = lElevations[0][TILE::eEdgeSize] + lE * (lElevations[TILE::eEdgeSize][TILE::eEdgeSize] - lElevations[0][TILE::eEdgeSize]) / TILE::eEdgeSize;
        }

        // Puis sur l'int�rieur.
        for(short lY = 1; lY < TILE::eEdgeSize; ++lY)
        {
            for(short lX = 1; lX < TILE::eEdgeSize; ++lX)
            {
                lElevations[lY][lX] = (lElevations[lY][0]  + lX * (lElevations[lY]             [TILE::eEdgeSize] - lElevations[lY][0])  / TILE::eEdgeSize      +
                                       lElevations[0] [lX] + lY * (lElevations[TILE::eEdgeSize][lX]              - lElevations[0] [lX]) / TILE::eEdgeSize) / 2 +
                                       (rand() % 3 - 1);
            }
        }

        TILE::HeightGroup   lTileElevations;
        short   lWest   = pCorners[0].col();
        short   lNorth  = pCorners[0].row();

        for(short lY = 0; lY <= TILE::eEdgeSize; ++lY)
        {
            for(short lX = 0; lX <= TILE::eEdgeSize; ++lX)
            {
                lTileElevations.insertInArea(TILE::Height(TILE::Height::TOffset(lWest  + lX),
                                                          TILE::Height::TOffset(lNorth - lY),
                                                          TILE::Height::TType(lElevations[lY][lX])));
            }
        }

        return lTileElevations;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    enum EDirections
    {
        eWest = 0,
        eNorth,
        eEast,
        eSouth,
        eDirections
    };

    enum EColors
    {
        eBlue = 0,
        eRed,
        eColors
    };

    /*! Codes couleurs pour les c�t� des fragIds.
        @sa http://www.pathofexile.com/forum/view-thread/55091
     */
    const EColors kCodes[TILE::eFragmentsInTiling][eDirections] =
    {
        { eRed,  eRed,  eRed,  eBlue },
        { eRed,  eRed,  eBlue, eBlue },
        { eBlue, eRed,  eBlue, eBlue },
        { eBlue, eRed,  eRed,  eBlue },

        { eRed,  eBlue, eRed,  eBlue },
        { eRed,  eBlue, eBlue, eBlue },
        { eBlue, eBlue, eBlue, eBlue },
        { eBlue, eBlue, eRed,  eBlue },

        { eRed,  eBlue, eRed,  eRed  },
        { eRed,  eBlue, eBlue, eRed  },
        { eBlue, eBlue, eBlue, eRed  },
        { eBlue, eBlue, eRed,  eRed  },

        { eRed,  eRed,  eRed,  eRed  },
        { eRed,  eRed,  eBlue, eRed  },
        { eBlue, eRed,  eBlue, eRed  },
        { eBlue, eRed,  eRed,  eRed  }
    };

    /*! @pTexel  Texel � modifier.
        @pFragId Nouvel id de fragment pour @p pTexel.
     */
    void setFragId(TILE::Texel::TType & pTexel, uint16_t pFragId)
    {
        pTexel.fragId  = pFragId;
        pTexel.fragDef = 1;
    }


    /*! @return Un nouveau fragId (al�atoire).
     */
    inline uint16_t newFragId()
    {
        return rand() % TILE::eFragmentsInTiling;
    }


    /*! @param pSide     C�t� contraint.
        @param pExpected Code couleur impos�.
        @return Un nouveau fragId (al�atoire), dont le c�t� @p pSide est @p pExpected.
     */
    uint16_t newFragIdWith(EDirections pSide, EColors pExpected)
    {
        uint16_t    lNew;
        do
        {
            lNew = newFragId();
        }
        while(kCodes[lNew][pSide] != pExpected);

        return lNew;
    }


    /*! @param pSide1     Premier c�t� contraint.
        @param pExpected1 Premier Code couleur impos�.
        @param pSide2     Second c�t� contraint.
        @param pExpected2 Second code couleur impos�.
        @return Un nouveau fragId (al�atoire), dont le c�t� @p pSide1 est @p pExpected1 et le c�t� @p pSide2 est @p pExpected2.
     */
    uint16_t newFragIdWith(EDirections pSide1, EColors pExpected1, EDirections pSide2, EColors pExpected2)
    {
        uint16_t    lNew;
        do
        {
            lNew = newFragId();
        }
        while((kCodes[lNew][pSide1] != pExpected1) || (kCodes[lNew][pSide2] != pExpected2));

        return lNew;
    }


    /*! D�finit les texels (champs fragId) dans une zone.
        @param pTexels Texels � modifier.
        @param pWidth  Largeur de la zone des texels.
        @param pWidth  Hauteur de la zone des texels.
     */
    void fillFrags(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight)
    {
        // Coin NO.
        setFragId(pTexels[0], newFragId());

        // 1�re ligne.
        for(uint32_t lCo = 1; lCo < pWidth; ++lCo)
        {
            setFragId(pTexels[lCo], newFragIdWith(eWest, kCodes[pTexels[lCo - 1].fragId][eEast]));
        }
        // 1�re colonne.
        for(uint32_t lLi = 1; lLi < pHeight; ++lLi)
        {
            setFragId(pTexels[lLi * pWidth], newFragIdWith(eNorth, kCodes[pTexels[(lLi - 1) * pWidth].fragId][eSouth]));
        }
        for(uint32_t lLi = 1; lLi < pHeight; ++lLi)
        {
            for(uint32_t lCo = 1; lCo < pWidth; ++lCo)
            {
                setFragId(pTexels[lLi * pWidth + lCo], newFragIdWith(eNorth, kCodes[pTexels[(lLi - 1) * pWidth + lCo    ].fragId][eSouth],
                                                                     eWest,  kCodes[pTexels[ lLi      * pWidth + lCo - 1].fragId][eEast]));
            }
        }
    }


    /*! Calcul d'un fragId pour un nouveau texel.
        @param pTexels Groupe des texels � modifier.
        @param pWidth  Largeur de la zone des texels.
        @param pWidth  Hauteur de la zone des texels.
        @param pOffset Position du texel dont calculer le fragId.
        @param pStepX Incr�ment horizontal pour le remplissage.
        @param pStepY Incr�ment vertital pour le remplissage.
     */
    uint16_t computeFrag(const std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pOffset, int32_t pStepX, int32_t pStepY)
    {
        int32_t lOffsetNeightbourY = pOffset - pStepY * pWidth;
        int32_t lOffsetNeightbourX = pOffset - pStepX;
        EDirections lNeightbourSideY = pStepY == 1 ? eSouth : eNorth;
        EDirections lNeightbourSideX = pStepX == 1 ? eEast  : eWest;
        EDirections lSideY = pStepY == 1 ? eNorth : eSouth;
        EDirections lSideX = pStepX == 1 ? eWest  : eEast;

        if      ((lOffsetNeightbourY < 0) || (pWidth * pHeight <= static_cast<uint32_t>(lOffsetNeightbourY)))
        {
            return newFragIdWith(lSideX, kCodes[pTexels[lOffsetNeightbourX].fragId][lNeightbourSideX]);
        }
        else if ((static_cast<int32_t>(pOffset % pWidth) + pStepX < 0) || (pWidth <= (pOffset % pWidth) + pStepX))
        {
            return newFragIdWith(lSideY, kCodes[pTexels[lOffsetNeightbourY].fragId][lNeightbourSideY]);
        }
        else
        {
            return newFragIdWith(lSideY, kCodes[pTexels[lOffsetNeightbourY].fragId][lNeightbourSideY],
                                 lSideX, kCodes[pTexels[lOffsetNeightbourX].fragId][lNeightbourSideX]);
        }
    }


    /*! @copydoc completeFrags(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pStart)
        @param pStepX Incr�ment horizontal pour le remplissage.
        @param pStepY Incr�ment vertital pour le remplissage.
     */
    void completeFrags(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pStart, int32_t pStepX, int32_t pStepY)
    {
        for(uint32_t lLi = 0; lLi < pHeight; ++lLi)
        {
            for(uint32_t lCo = 0; lCo < pWidth; ++lCo)
            {
                int32_t lOffset = pStart + pStepY * lLi * pWidth + pStepX * lCo;
                if (pTexels[lOffset].fragDef != 0) continue;
                setFragId(pTexels[lOffset], computeFrag(pTexels, pWidth, pHeight, lOffset, pStepX, pStepY));
            }
        }
    }


    /*! @copydoc completeFrags(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pStart)
        C'est la situation la plus complexe.
        7 8 8 8 8 8 8 8
        7 8 8 8 8 8 8 8
        6 6 O 0 0 0 1 1
        6 6 0 0 0 0 2 2
        6 6 0 0 0 0 2 2
        6 6 0 0 0 0 2 2
        6 6 4 4 4 4 4 3
        5 5 4 4 4 4 4 3
        La zone est remplie dans cet ordre. O est le "point" de d�part. Les 0 sont les texels d�j� d�finis.
        Les nouveau texels sont ensuite d�finis � leur tour, selon les num�ros croissants dans le sch�ma ci-dessus.
     */
    void completeFragsFromCenter(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pStart)
    {
        uint32_t lX0 = pStart % pWidth;
        uint32_t lY0 = pStart / pWidth;
        uint32_t lW0 = 0;
        uint32_t lH0 = 0;
        // Ligne 1.
        for(uint32_t lOffset = pStart; (lOffset % pWidth) != 0; ++lOffset)
        {
            if (pTexels[lOffset].fragDef != 0) continue;
            if (lW0 == 0) lW0 = lOffset - pStart;
            setFragId(pTexels[lOffset], newFragIdWith(eWest, kCodes[pTexels[lOffset - 1].fragId][eEast]));
        }
        // Bloc 2.
        for(uint32_t lLi = lY0 + 1; (lLi < pHeight) && (lH0 == 0); ++lLi)
        {
            for(uint32_t lCo = lX0 + lW0; lCo < pWidth; ++lCo)
            {
                uint32_t    lOffset = lLi * pWidth + lCo;
                if (pTexels[lOffset - 1].fragDef == 0)
                {
                    lH0 = lLi - lY0;
                    break;
                }
                setFragId(pTexels[lOffset], newFragIdWith(eNorth, kCodes[pTexels[lOffset - pWidth].fragId][eSouth],
                                                          eWest,  kCodes[pTexels[lOffset - 1].     fragId][eEast]));
            }
        }
        // Colonne 3.
        for(uint32_t lLi = lY0 + lH0; lLi < pHeight; ++lLi)
        {
            uint32_t lOffset = (lLi + 1) * pWidth - 1;
            setFragId(pTexels[lOffset], newFragIdWith(eNorth, kCodes[pTexels[lOffset - pWidth].fragId][eSouth]));
        }
        // Bloc 4.
        for(uint32_t lCo = pWidth - 2; lX0 <= lCo; --lCo)
        {
            for(uint32_t lLi = lY0 + lH0; lLi < pHeight; ++lLi)
            {
                uint32_t lOffset = lLi * pWidth + lCo;
                setFragId(pTexels[lOffset], newFragIdWith(eNorth, kCodes[pTexels[lOffset - pWidth].fragId][eSouth],
                                                          eEast,  kCodes[pTexels[lOffset + 1].     fragId][eWest]));
            }
        }
        // Ligne 5.
        for(int32_t lCo = lX0 - 1; 0 <= lCo; --lCo)
        {
            uint32_t    lOffset = (pHeight - 1) * pWidth + lCo;
            setFragId(pTexels[lOffset], newFragIdWith(eEast, kCodes[pTexels[lOffset + 1].fragId][eWest]));
        }
        // Bloc 6.
        for(uint32_t lLi = pHeight - 2; lY0 <= lLi; --lLi)
        {
            for(int32_t lCo = lX0 - 1; 0 <= lCo; --lCo)
            {
                uint32_t lOffset = lLi * pWidth + lCo;
                setFragId(pTexels[lOffset], newFragIdWith(eSouth, kCodes[pTexels[lOffset + pWidth].fragId][eNorth],
                                                          eEast,  kCodes[pTexels[lOffset + 1].     fragId][eWest]));
            }
        }
        // Colonne 7.
        for(int32_t lLi = lY0 - 1; 0 <= lLi; --lLi)
        {
            uint32_t lOffset = lLi * pWidth;
            setFragId(pTexels[lOffset], newFragIdWith(eSouth, kCodes[pTexels[lOffset + pWidth].fragId][eNorth]));
        }
        // Block 8.
        for(uint32_t lCo = 1; lCo < pWidth; ++lCo)
        {
            for(int32_t lLi = lY0 - 1; 0 <= lLi; --lLi)
            {
                uint32_t lOffset = lLi * pWidth + lCo;
                setFragId(pTexels[lOffset], newFragIdWith(eSouth, kCodes[pTexels[lOffset + pWidth].fragId][eNorth],
                                                          eWest,  kCodes[pTexels[lOffset - 1].     fragId][eEast]));
            }
        }
    }


    /*! Compl�te les texels (champs fragId) � partir d'une zone o� ils sont d�j� d�finis.
        @param pTexels Texels � modifier.
        @param pWidth  Largeur de la zone des texels.
        @param pWidth  Hauteur de la zone des texels.
        @param pStart  D�but de la zone d�j� d�finie (pr�s du centre, ou un des coins).
     */
    void completeFrags(std::vector<TILE::Texel::TType>& pTexels, uint32_t pWidth, uint32_t pHeight, uint32_t pStart)
    {
        if      (pStart == 0)
        {
            completeFrags(pTexels, pWidth, pHeight, pStart,  1,  1);
        }
        else if (pStart == pWidth - 1)
        {
            completeFrags(pTexels, pWidth, pHeight, pStart, -1,  1);
        }
        else if (pStart == (pHeight - 1) * pWidth)
        {
            completeFrags(pTexels, pWidth, pHeight, pStart,  1, -1);
        }
        else if (pStart == pHeight * pWidth - 1)
        {
            completeFrags(pTexels, pWidth, pHeight, pStart, -1, -1);
        }
        else
        {
            completeFragsFromCenter(pTexels, pWidth, pHeight, pStart);
        }
    }
}


namespace QWOR
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    enum EFlags
    {
        eDefault   = 0x00000000,
        eLocked    = 0x00000001,
        eModified  = 0x00000002,
        eMask      = 0xFFFFFFFF
    };

    enum EModified
    {
        eHeights,
        eTexels,
        eHdls,
        eFlags,
        eCount
    };

    namespace
    {
        inline int16_t fromColor(QColor&& pColor)
        {
            return static_cast<int16_t>(pColor.green() * 256 + pColor.blue() - 32768);
        }
        inline QColor fromHeight(int16_t pHeight)
        {
            auto    lH = static_cast<uint16_t>(32768 + pHeight);
            return QColor{0, lH / 256, lH % 256};
        }
    }

//------------------------------------------------------------------------------
//                                P-Impl de Data
//------------------------------------------------------------------------------

    /*! @brief PImpl de Data.
        @version 0.4

        Initialement, TileDataClient �tait un singleton. Afin de ne pas se limiter � un seul monde
        ext�rieur (ce qui �tait induit, de fait), j'ai quelque peu modifi� le syst�me de fa�on �
        pouvoir g�rer plusieurs donn�es de monde (voir comment �a va marcher...).
     */
    class Data::Private : public DATA::Store
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                                    Private(Data*          pThat,
                                            PAK::FileHdl   pGroup,
                                            OSGi::Context* pContext);                               //!< Constructeur par d�faut.
virtual                             ~Private();                                                     //!< Destructeur.
        //@}
        //! @name Permanence des donn�es
        //@{
                void                loadAll();                                                      //!< Chargement des donn�es.
                void                saveAll();                                                      //!< Sauvegarde des donn�es.
                void                saveVolatile();                                                 //!< Sauvegarde des donn�es volatiles.
        //@}
                PAK::FileHdl        createTileHdl(unsigned long pOffset);                           //!< Cr�ation d'un fichier pour une tuile.
                void                removeTileHdl(unsigned long pOffset);                           //!< Suppression du fichier d'une tuile.
        //! @name D�placement des donn�es
        //@{
                void                moveElevations(const UTI::Rect& pToExtent);                     //!< D�placement des �l�vations suite � un redimensionnement.
                void                moveTexels(const UTI::Rect& pToExtent);                         //!< D�placement des texels suite � un redimensionnement.
                void                moveTiles(const UTI::Rect& pToExtent);                          //!< D�placement des "tuiles" suite � un redimensionnement.
        //@}
        //! @name Gestion des flags
        //@{
        inline  void                setFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag);          //!< 
        inline  void                resetFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag);        //!< 
        inline  std::optional<bool> checkFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag) const;  //!< 
        //@}
        //! @name Types de conteneurs
        //@{
        using Heights       = std::vector<THeight>;                                                 //!< 
        using Texels        = std::vector<TTexel/*, UTI::Allocator<TTexel>*/>;                      //!< 
        using FileHdls      = std::vector<PAK::FileHdl>;                                            //!< 
        using Flags         = std::vector<uint32_t>;                                                //!< 
        using LoadedTiles   = std::deque<std::weak_ptr<TilePtr::element_type>>;                     //!< 
        //@}
        Data*               q;                                                                      //!< "Propri�taire" de l'instance.
        OSGi::Context*      mContext;                                                               //!< Contexte d'ex�cution.

        using TileMgrPtr = std::weak_ptr<TILE::TileMgr::Ptr::element_type>;
        TileMgrPtr          mTileMgr;                                                               //!< Gestionnaire de tuiles.
        PAK::FileHdl        mGroupHdl;
        //! @name Donn�es permanentes
        //@{
        UTI::Rect           mExtent;                                                                //!< Zone tuil�e (unit� = 1 tuile).
        Heights             mHeights;                                                               //!< Elevations des coins de tuiles.
        Texels              mTexels;                                                                //!< Texels.
        FileHdls            mTileHdls;                                                              //!< Fichiers de tuiles allou�s.
        Flags               mTileFlags;                                                             //!< "Drapeaux" des tuiles.
        LoadedTiles         mLoadedTiles;                                                           //!< Tuiles actuellement charg�es.
        PAK::FileHdl        mOutsideHdl;                                                            //!< 

        std::bitset<eCount> mChanges;                                                               //!< Identification des donn�es modifi�es.
        //@}
    protected:

        QString         saveFolder() const override;                                                //!< R�peroire de sauvegarde des donn�es IDE.
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    Data::Private::Private(Data* pThat, PAK::FileHdl pGroup, OSGi::Context* pContext)
        : DATA::Store{pContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileName(pGroup)}
        , q{pThat}
        , mContext{pContext}
        , mTileMgr{}
        , mGroupHdl{pGroup}
        , mExtent{}
        , mHeights{}
        , mTexels{}
        , mTileHdls{}
        , mTileFlags{}
        , mOutsideHdl{}
        , mChanges{}
    { }


    /*!
     */
    Data::Private::~Private() = default;


//------------------------------------------------------------------------------
//                                   "Magasin"
//------------------------------------------------------------------------------

    /*!
     */
    QString Data::Private::saveFolder() const
    {
        return mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }


//------------------------------------------------------------------------------
//                            Permanence des Donn�es
//------------------------------------------------------------------------------

    /*!
     */
    void Data::Private::loadAll()
    {
        auto    lExtent = string("Extent").split(";");
        mExtent = UTI::Rect(F32{lExtent[0].toFloat()}, F32{lExtent[1].toFloat()}, F32{lExtent[2].toFloat()}, F32{lExtent[3].toFloat()});

        mOutsideHdl = mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileHdl(string("Outside"));

        auto    lUniverse   = mContext->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->universe();
        mTileMgr            = lUniverse->tileMgr(mOutsideHdl);

        auto    lWidth  = static_cast<int>(mExtent.width());
        auto    lHeight = static_cast<int>(mExtent.height());

        mHeights.resize( (lWidth + 1)                    * (lHeight + 1),                    THeight(0));
        mTexels.resize(  (lWidth * TILE::eFragmentCount) * (lHeight * TILE::eFragmentCount), TTexel{});
        mTileHdls.resize( lWidth                         *  lHeight,                         PAK::eBadHdl);
        mTileFlags.resize(lWidth                         *  lHeight,                         eDefault);

        // Heights
        {
            auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lHeight);
            lProgress->advance(0);

            auto    lHeights = image("Heights").convertToFormat(QImage::Format_RGB32);
            if (lHeights.size() == QSize{lWidth + 1, lHeight + 1})
            {
                auto    lH = mHeights.begin();
                for(int j = 0; j < lHeights.height(); ++j)
                    for(int i = 0; i < lHeights.width(); ++i, ++lH)
                    {
                        (*lH) = THeight{fromColor(QColor{lHeights.pixel(i, j)})};
                    }
            }
            // Sinon, que faire ?

            mChanges.reset(EModified::eHeights);
        }
        // Texels.
        {
            auto    lTexels = image("Texels").convertToFormat(QImage::Format_ARGB32);

            //                                                                  / sizeof(ARGB32)
            constexpr int32_t kTexelSizeInPixel = (sizeof(TILE::TexelData) + 3) / 4;
            if (lTexels.size() == QSize{lWidth * TILE::eFragmentCount * kTexelSizeInPixel, lHeight * TILE::eFragmentCount})
            {
                auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lTexels.height() - 1);
                lProgress->advance(0);
                auto    lT = mTexels.begin();

                for(int j = 0; j < lTexels.height(); ++j)
                    for(int i = 0; i < lTexels.width() / kTexelSizeInPixel; ++i, ++lT)
                    {
                        uint32_t* lEnv = reinterpret_cast<uint32_t*>(&(*lT));
                        for(size_t lE = 0; lE < kTexelSizeInPixel; ++lE)
                        {
                            lEnv[lE] = lTexels.pixel(kTexelSizeInPixel * i + lE, j);
                        }
                    }
            }

            mChanges.reset(EModified::eTexels);
        }
        {
            QStringList lTileFileNames = list("Tile files");

            if (static_cast<size_t>(lTileFileNames.size()) == mTileHdls.size())
            {
                std::transform(lTileFileNames.begin(), lTileFileNames.end(), mTileHdls.begin(), [this](const QString& pName) {
                    return mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileHdl(pName);
                });
            }

            mChanges.reset(EModified::eHdls);
        }
        // Flags.
        {
            auto    lFlags  = image("Flags").convertToFormat(QImage::Format_RGB32);

            if (lFlags.size() == QSize{lWidth, lHeight})
            {
                auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lFlags.height() - 1);
                lProgress->advance(0);
                auto    lF = mTileFlags.begin();

                for(int j = 0; j < lFlags.height(); ++j)
                    for(int i = 0; i < lFlags.width(); ++i, ++lF)
                    {
                        (*lF) = uint32_t{lFlags.pixel(i, j)};
                    }
            }

            mChanges.reset(EModified::eFlags);
        }
    }


    /*!
     */
    void Data::Private::saveAll()
    {
        saveVolatile();

        set("Extent", QStringLiteral("%1;%2;%3;%4").
                        arg(mExtent.left()).
                        arg(mExtent.top()).
                        arg(mExtent.right()).
                        arg(mExtent.bottom()));

        set("Outside", mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileName(mOutsideHdl));

        auto    lWidth  = static_cast<int>(mExtent.width());
        auto    lHeight = static_cast<int>(mExtent.height());

        if (mChanges.test(EModified::eHeights))
        {
            auto    lHeights = QImage{lWidth + 1, lHeight + 1, QImage::Format_RGB32};
            auto*   lH = &mHeights[0];

            auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lHeights.height() - 1);
            lProgress->advance(0);

            for(int j = 0; j < lHeights.height(); ++j)
            {
                for(int i = 0; i < lHeights.width(); ++i, ++lH)
                {
                    lHeights.setPixel(i, j, fromHeight(*lH).rgb());
                }
                lProgress->advance(j);
            }

            set("Heights", lHeights);

            mChanges.reset(EModified::eHeights);
        }

        if (mChanges.test(EModified::eTexels))
        {
            //                                                                  / sizeof(ARGB32)
            constexpr int32_t kTexelSizeInPixel = (sizeof(TILE::TexelData) + 3) / 4;

            auto    lTexels = QImage{lWidth * TILE::eFragmentCount * kTexelSizeInPixel, lHeight * TILE::eFragmentCount, QImage::Format_ARGB32};
            auto*   lT      = &mTexels[0];

            auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lTexels.height() - 1);
            lProgress->advance(0);

            for(int j = 0; j < lTexels.height(); ++j)
            {
                for(int i = 0; i < lTexels.width() / kTexelSizeInPixel; ++i, ++lT)
                {
                    const uint32_t* lEnv = reinterpret_cast<const uint32_t*>(&(*lT));
                    for(size_t lE = 0; lE < kTexelSizeInPixel; ++lE)
                    {
                        lTexels.setPixel(kTexelSizeInPixel * i + lE, j, lEnv[lE]);
                    }
                }
                lProgress->advance(j);
            }

            set("Texels", lTexels);

            mChanges.reset(EModified::eTexels);
        }

        if (mChanges.test(EModified::eFlags))
        {
            auto    lFlags  = QImage{lWidth, lHeight, QImage::Format_RGB32};
            auto*   lF      = &mTileFlags[0];

            auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lFlags.height() - 1);
            lProgress->advance(0);

            for(int j = 0; j < lFlags.height(); ++j)
            {
                for(int i = 0; i < lFlags.width(); ++i, ++lF)
                {
                    lFlags.setPixel(i, j, *lF);
                }
                lProgress->advance(j);
            }

            set("Flags", lFlags);

            mChanges.reset(EModified::eFlags);
        }

        save();
    }


    /*!  Sauvegarde des donn�es volatiles.
     */
    void Data::Private::saveVolatile()
    {
        QStringList lTileFileNames;
        for(auto lH : mTileHdls)
        {
            lTileFileNames << mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileName(lH);
        }
        set("Tile files", lTileFileNames);
        save();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Cr�e un fichier pour une nouvelle tuile.
     */
    PAK::FileHdl Data::Private::createTileHdl(unsigned long pOffset)
    {
        QString lGroupFilename  = mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->packFileName(mGroupHdl);
        if (auto lWorldService  = mContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data"))
        {
            QString lFileName = lWorldService->newTileFilename(lWorldService->groupName(lGroupFilename));
            PAK::FileHdl   lTileHdl = mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->create(lFileName);
            if (lTileHdl != PAK::eBadHdl)
            {
                q->tileMgr()->insert(lTileHdl);
                q->tileMgr()->save();
                mTileHdls[pOffset] = lTileHdl;
                mChanges.set(EModified::eHdls);
                saveVolatile(); // Tr�s important !
            }

            return lTileHdl;
        }

        return PAK::eBadHdl;
    }


    /*!
     */
    void Data::Private::removeTileHdl(unsigned long pOffset)
    {
        PAK::FileHdl   lHdl = mTileHdls[pOffset];
        TILE::Key      lKey = q->tileMgr()->key(lHdl);

        if (!lKey.isNull())
        {
            // Il faut �viter de charger la ressource si elle ne l'�tait pas d�j�...
            if (!q->tileMgr()->remove(lKey))
            {
                // Supprime la ressource.
                RSC::ResourceMgr<TILE::TileRsc>::TResPtr  lTile   = q->tileMgr()->retrieve(lKey);
                q->tileMgr()->release(lTile);
                ASSERT_EX(lTile == nullptr, kReleaseResourceFailed, return;)
            }
            bool    lOK = q->tileMgr()->remove(lKey);
            ASSERT_EX(lOK, kDeleteResourceFailed, return;)
            q->tileMgr()->save();
            mTileHdls[pOffset] = PAK::eBadHdl;
            mChanges.set(EModified::eHdls);
            saveVolatile(); // Tr�s important !
        }
        mContext->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->erase(lHdl);
    }


//------------------------------------------------------------------------------
//                            D�placement des Donn�es
//------------------------------------------------------------------------------

    /*! Pour redimensionner un monde, les donn�es doivent �tre :
       - soit d�plac�es � l'int�rieur du nouveau monde (agrandissement),
       - soit, pour certaines d'entre elles, conserv�es dans le nouveau monde (r�duction).
       Cette m�thode g�re les deux cas, pour les �l�vations. Les anciens et nouveaux mondes sont d�finis, respectivement, par la zone
       actuelle et par le param�tre.
        @param pToExtent Zone du nouveau monde
     */
    void Data::Private::moveElevations(const UTI::Rect& pToExtent)
    {
        // Le terrain �tant redimensionn�, les donn�es existantes devront �tre d�plac�s dans le tableau mHeights, une fois que
        // celui-ci aura �t� redimensionn�. Pour ce faire, un tableau temporaire est introduit. Il est construit avec la taille finale
        // (apr�s extension ou r�duction).
        unsigned long   lNewWidth  = static_cast<long>(pToExtent.width())  + 1;
        unsigned long   lNewHeight = static_cast<long>(pToExtent.height()) + 1;
        // Ces 2 dimensions sont exprim�es en nombre d'�l�vations, alors que height() et
        // width() le sont en nombre de tuiles (2 x 2 �l�vations pour faire une tuile).

        // Nouvelles �l�vations (nulles).
        Heights        lNewHeights(lNewWidth * lNewHeight, THeight(0));

        if      (intersects(mExtent, pToExtent) == UTI::eInside)
        {
            // Extension.
            if (mExtent.isValid())
            {
                // Rappel : nombre d'�l�vations sur une ligne == nombre de tuiles + 1.
                unsigned long   lElevationLineWidth = static_cast<long>(mExtent.width())  + 1;
                unsigned long   lElevationLineCount = static_cast<long>(mExtent.height()) + 1;
                unsigned long   lHorizontalOffset   = static_cast<long>(mExtent.left())   - static_cast<long>(pToExtent.left());    // New left <= Old left.
                unsigned long   lVerticalOffset     = static_cast<long>(pToExtent.top())  - static_cast<long>(mExtent.top());       // New Top  >= Old Top.

                for(unsigned long lLi = 0; lLi < lElevationLineCount; ++lLi)
                {
                    const THeight*  lBegin  = &mHeights[lLi * lElevationLineWidth];
                    THeight*        lDst    = &lNewHeights[(lVerticalOffset + lLi) * lNewWidth + lHorizontalOffset];

                    memcpy(static_cast<void*>(lDst), static_cast<const void*>(lBegin), lElevationLineWidth * sizeof(THeight));
                }
            }
        }
        else if (intersects(pToExtent, mExtent) == UTI::eInside)
        {
            // Rappel : nombre d'�l�vations sur une ligne == nombre de tuiles + 1.
            unsigned long   lElevationLineWidth = static_cast<long>(mExtent.width())  + 1;
            unsigned long   lHorizontalOffset   = static_cast<long>(pToExtent.left()) - static_cast<long>(mExtent.left());  // New left >= Old left.
            unsigned long   lVerticalOffset     = static_cast<long>(mExtent.top())    - static_cast<long>(pToExtent.top()); // New Top  <= Old Top.
            // Copie des donn�es; elles sont stock�es du nord vers le sud.
            for(unsigned long lLi = 0; lLi < lNewWidth; ++lLi)
            {
                const THeight*  lBegin  = &mHeights[(lVerticalOffset + lLi) * lElevationLineWidth + lHorizontalOffset];
                THeight*        lDst    = &lNewHeights[lLi * lNewWidth];

                memcpy(static_cast<void*>(lDst), static_cast<const void*>(lBegin), lNewWidth * sizeof(THeight));
            }
        }

        // Etablit les nouvelles �l�vations.
        mHeights.swap(lNewHeights);

        mChanges.set(EModified::eHeights);
    }


    /*! Pour redimensionner un monde, les donn�es doivent �tre :
       - soit d�plac�es � l'int�rieur du nouveau monde (agrandissement),
       - soit, pour certaines d'entre elles, conserv�es dans le nouveau monde (r�duction).
       Cette m�thode g�re les deux cas, pour les texels. Les anciens et nouveaux mondes sont d�finis, respectivement, par la zone actuelle et
       par le param�tre.
        @param pToExtent Zone du nouveau monde
     */
    void Data::Private::moveTexels(const UTI::Rect& pToExtent)
    {
        // Le terrain �tant redimensionn�, les donn�es existantes devront �tre d�plac�s dans le tableau mTexels, une fois que
        // celui-ci aura �t� redimensionn�. Pour ce faire, un tableau temporaire est introduit. Il est construit avec la taille finale
        // (apr�s extension ou r�duction).
        unsigned long   lNewWidth  = static_cast<long>(pToExtent.width())  * TILE::eFragmentCount;
        unsigned long   lNewHeight = static_cast<long>(pToExtent.height()) * TILE::eFragmentCount;
        // Ces 2 dimensions sont exprim�es en nombre d'�l�vations, alors que width() et
        // height() le sont en nombre de tuiles (2 x 2 �l�vations pour faire une tuile).

        // Nouveaux texels ("vides").
        TTexel  lDefaultTexel{};
        lDefaultTexel.enabled = 1;
        Texels lNewTexels(lNewWidth * lNewHeight, lDefaultTexel);

        if      (intersects(mExtent, pToExtent) == UTI::eInside)
        {
            // Extension.
            if (mExtent.isValid())
            {
                // Copie des donn�es.
                unsigned long   lTexelLineWidth     =  static_cast<long>(mExtent.width())  * TILE::eFragmentCount;
                unsigned long   lTexelLineCount     =  static_cast<long>(mExtent.height()) * TILE::eFragmentCount;
                unsigned long   lHorizontalOffset   = (static_cast<long>(mExtent.left())   - static_cast<long>(pToExtent.left())) * TILE::eFragmentCount;  // New left <= Old left.
                unsigned long   lVerticalOffset     = (static_cast<long>(pToExtent.top())  - static_cast<long>(mExtent.top()))    * TILE::eFragmentCount;  // New Top  >= Old Top.
                // Copie des donn�es; elles sont stock�es du nord vers le sud.
                for(unsigned long lLi = 0; lLi < lTexelLineCount; ++lLi)
                {
                    const TTexel*   lBegin  = &mTexels[lLi * lTexelLineWidth];
                    TTexel*         lDst    = &lNewTexels[(lVerticalOffset + lLi) * lNewWidth + lHorizontalOffset];

                    memcpy(static_cast<void*>(lDst), static_cast<const void*>(lBegin), lTexelLineWidth * sizeof(TTexel));
                }
                if (!mTexels.empty())
                {
                    // Old/New: 1:ONN 2:NNN 3:NNO 4:NNN 5:NNN
                    //            NNN   NNN   NNN   NNN   NON
                    //            NNN   ONN   NNN   NNO   NNN
                    uint32_t lStart = lVerticalOffset * lNewWidth + lHorizontalOffset;  // D�faut : cas 5.
                    if (lHorizontalOffset == 0)
                    {   //                                1   2
                        lStart = (lVerticalOffset == 0) ? 0 : (lNewHeight - 1) * lNewWidth;
                    }
                    else if (lHorizontalOffset + lTexelLineWidth == lNewWidth)
                    {   //                                 3                 4
                        lStart = (lVerticalOffset == 0) ? (lNewWidth - 1) : (lNewHeight * lNewWidth - 1);
                    }
                    completeFrags(lNewTexels, lNewWidth, lNewHeight, lStart);
                }
            }
            else
            {
                fillFrags(lNewTexels, lNewWidth, lNewHeight);
            }
        }
        else if (intersects(pToExtent, mExtent) == UTI::eInside)
        {
            // Copie des donn�es.
            unsigned long   lTexelLineWidth     =  static_cast<long>(mExtent.width()) * TILE::eFragmentCount;
            unsigned long   lHorizontalOffset   = (static_cast<long>(pToExtent.left()) - static_cast<long>(mExtent.left()))  * TILE::eFragmentCount;   // New left >= Old left.
            unsigned long   lVerticalOffset     = (static_cast<long>(mExtent.top())    - static_cast<long>(pToExtent.top())) * TILE::eFragmentCount;   // New Top  <= Old Top.
            for(unsigned long lLi = 0; lLi < lNewWidth; ++lLi)
            {
                const TTexel*   lBegin  = &mTexels[(lVerticalOffset + lLi) * lTexelLineWidth + lHorizontalOffset];
                TTexel*         lDst    = &lNewTexels[lLi * lNewWidth];

                memcpy(static_cast<void*>(lDst), static_cast<const void*>(lBegin), lNewWidth * sizeof(TTexel));
            }
        }

        // Etablit les nouveaux texels.
        mTexels.swap(lNewTexels);
    }


    /*! Pour redimensionner un monde, les donn�es doivent �tre :
       - soit d�plac�es � l'int�rieur du nouveau monde (agrandissement),
       - soit, pour certaines d'entre elles, conserv�es dans le nouveau monde (r�duction).
       Cette m�thode g�re les deux cas, pour les handles de tuiles, les verrous et les sujets. Les anciens et nouveaux mondes sont d�finis,
       respectivement, par la zone actuelle et par le param�tre.
        @param pToExtent   Zone du nouveau monde (en coordonn�es de tuiles)
     */
    void Data::Private::moveTiles(const UTI::Rect& pToExtent)
    {
        unsigned long   lNewWidth  = static_cast<long>(pToExtent.width());
        unsigned long   lNewHeight = static_cast<long>(pToExtent.height());

        // Nouveaux handles (invalides).
        FileHdls   lNewHdls(lNewWidth * lNewHeight, PAK::eBadHdl);
        // Nouveaux verrous (ouverts).
        Flags      lNewFlags(lNewWidth * lNewHeight, eDefault);

        if      (intersects(mExtent, pToExtent) == UTI::eInside)
        {
            // Extension.
            if (mExtent.isValid())
            {
                unsigned long   lLineWidth        = static_cast<long>(mExtent.width());
                unsigned long   lLineCount        = static_cast<long>(mExtent.height());
                unsigned long   lHorizontalOffset = static_cast<long>(mExtent.left())  - static_cast<long>(pToExtent.left());   // New left <= Old left.
                unsigned long   lVerticalOffset   = static_cast<long>(pToExtent.top()) - static_cast<long>(mExtent.top());      // New Top  >= Old Top.

                for(unsigned long lLi = 0; lLi < lLineCount; ++lLi)
                {
                    const PAK::FileHdl*    lBegin  = &mTileHdls[lLi * lLineWidth];
                    PAK::FileHdl*          lDst    = &lNewHdls[(lVerticalOffset + lLi) * lNewWidth + lHorizontalOffset];

                    memcpy(static_cast<void*>(lDst), static_cast<const void*>(lBegin), lLineWidth * sizeof(PAK::FileHdl));

                    for(unsigned long lCo = 0; lCo < lLineWidth; ++lCo)
                    {
                        unsigned long   lOffsetFrom = lLi * lLineWidth + lCo;
                        unsigned long   lOffsetTo   = (lVerticalOffset + lLi) * lNewWidth + (lHorizontalOffset + lCo);

                        lNewHdls[lOffsetTo]     = mTileHdls[lOffsetFrom];
                        lNewFlags[lOffsetTo]    = mTileFlags[lOffsetFrom];
                    }
                }
            }
        }
        else if (intersects(pToExtent, mExtent) == UTI::eInside)
        {
            unsigned long   lLineWidth          = static_cast<long>(mExtent.width());
            unsigned long   lHeight             = static_cast<long>(mExtent.height());
            unsigned long   lHorizontalOffset   = static_cast<long>(pToExtent.left()) - static_cast<long>(mExtent.left());  // New left >= Old left.
            unsigned long   lVerticalOffset     = static_cast<long>(mExtent.top())    - static_cast<long>(pToExtent.top()); // New Top  <= Old Top.
            // Traitement en une passe (peut-�tre plus lourd, mais plus simple � coder).
            for(unsigned long lLi = 0, lOffset = 0; lLi < lHeight; ++lLi)
            {
                for(unsigned long lCo = 0; lCo < lLineWidth; ++lCo, ++lOffset)
                {
                    if ((lVerticalOffset   <= lLi) && (lLi < lVerticalOffset   + lNewHeight) &&
                        (lHorizontalOffset <= lCo) && (lCo < lHorizontalOffset + lNewWidth))
                    {
                        unsigned long   lCurrent = (lLi - lVerticalOffset) * lNewWidth + lCo - lHorizontalOffset;

                        lNewHdls[lCurrent]  = mTileHdls[lOffset];
                        lNewFlags[lCurrent] = mTileFlags[lOffset];
                    }
                    else
                    {
                        removeTileHdl(lOffset);
                    }
                }
            }
        }

        // Valide les nouveaux handles de fichiers, les nouveaux verrous, et la nouvelle zone.
        mTileHdls.swap(lNewHdls);
        mTileFlags.swap(lNewFlags);
    }


//------------------------------------------------------------------------------
//                               Gestion des Flags
//------------------------------------------------------------------------------

    /*!
     */
    inline void Data::Private::setFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag)
    {
        unsigned long   lWidth  = mExtent.roundedWidth();
        if (lWidth <= pTileX) return;
        unsigned long   lOffset = pTileY * lWidth + pTileX;
        if (mTileFlags.size() <= lOffset) return;

        mTileFlags[lOffset] |= pFlag;
        mChanges.set(EModified::eFlags);
    }


    /*!
     */
    inline void Data::Private::resetFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag)
    {
        unsigned long   lWidth  = mExtent.roundedWidth();
        if (lWidth <= pTileX) return;
        unsigned long   lOffset = pTileY * lWidth + pTileX;
        if (mTileFlags.size() <= lOffset) return;

        mTileFlags[lOffset] &= ~pFlag;
        mChanges.set(EModified::eFlags);
    }


    /*!
     */
    inline std::optional<bool> Data::Private::checkFlag(TOffset pTileX, TOffset pTileY, EFlags pFlag) const
    {
        unsigned long   lWidth  = mExtent.roundedWidth();
        if (lWidth <= pTileX) return {};
        unsigned long   lOffset = pTileY * lWidth + pTileX;
        if (mTileFlags.size() <= lOffset) return {};

        return ((mTileFlags[lOffset] & pFlag) == pFlag);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    Data::~Data()
    {
        if (pthis->mChanges.any())
        {
            pthis->saveAll();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    TILE::TileMgr::Ptr Data::tileMgr() const
    {
        return pthis->mTileMgr.lock();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Data::moveData(const UTI::Rect& pToExtent)
    {
        pthis->moveElevations(pToExtent);
        pthis->moveTexels(pToExtent);
        pthis->moveTiles(pToExtent);

        // Validation.
        pthis->mExtent = pToExtent;
        pthis->mChanges.set();
    }


    /*!
     */
    UTI::Rect Data::tileDataZone() const
    {
        return pthis->mExtent;
    }


//------------------------------------------------------------------------------
//                         El�vations
//------------------------------------------------------------------------------

    /*! @return Le nombre d'�l�vations (de coin de tuile)
     */
    uint32_t Data::elevationCount() const
    {
        return pthis->mHeights.size();
    }


    /*! @param pN Indice de l'�l�vation (de coin de tuile) � r�cup�rer
        @return La <i>pN</i>i�me �l�vation (de coin de tuile)
     */
    Data::THeight Data::elevation(uint32_t pN) const
    {
        return pthis->mHeights[pN];
    }


    /*! @param pTileX "Longitude" de l'�l�vation (unit� = 1 tuile); 0 est � l'ouest
        @param pTileY "Latitude" de l'�l�vation (unit� = 1 tuile); 0 est au nord
        @return L'�l�vation (de coin de tuile) situ�e � (<i>pTileX</i>, <i>pTileY</i>)
     */
    Data::THeight Data::elevation(TOffset pTileX, TOffset pTileY) const
    {
        uint32_t    lWidth = pthis->mExtent.roundedWidth() + 1;

        return elevation(pTileY * lWidth + pTileX);
    }


    /*! @param pN   Indice de l'�l�vation (de coin de tuile) � r�cup�rer
        @param pNew Nouvelle valeur de la <i>pN</i>i�me �l�vation (de coin de tuile)
     */
    void Data::setElevation(uint32_t pN, THeight pNew)
    {
        pthis->mHeights[pN] = pNew;
        pthis->mChanges.set(EModified::eHeights);
    }


//------------------------------------------------------------------------------
//                           Texels
//------------------------------------------------------------------------------

    /*! @return Le nombre de fragments (de texture de tuile)
     */
    uint32_t Data::texelCount() const
    {
        return pthis->mTexels.size();
    }


    /*! @param pN Indice du fragment (de texture de tuile) � r�cup�rer
        @return Le <i>pN</i>i�me fragment (de texture de tuile)
     */
    Data::TTexel Data::texel(uint32_t pN) const
    {
        return pthis->mTexels[pN];
    }


    /*!
        @param pTexelX "Longitude" du texel; 0 est � l'ouest
        @param pTexelY "Latitude" du texel; 0 est au nord
     */
    Data::TTexel Data::texel(TOffset pTexelX, TOffset pTexelY) const
    {
        uint32_t    lWidth = pthis->mExtent.roundedWidth() * TILE::eFragmentCount;

        return texel(pTexelY * lWidth + pTexelX);
    }


    /*! @param pN   Indice du fragment (de texture de tuile) � modifier
        @param pNew Nouvelle valeur du <i>pN</i>i�me fragment (de texture de tuile)
     */
    void Data::setTexel(uint32_t pN, TTexel pNew) const
    {
        pthis->mTexels[pN] = pNew;
        pthis->mChanges.set(EModified::eTexels);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pTileX "Longitude" de l'�l�vation (unit� = 1 tuile); 0 est � l'ouest
        @param pTileY "Latitude" de l'�l�vation (unit� = 1 tuile); 0 est au nord
        @return La clef de ressource de la tuile (<i>pTileX</i>, <i>pTileY</i>)
     */
    TILE::Key Data::tileKey(TOffset pTileX, TOffset pTileY) const
    {
        unsigned long   lWidth      = pthis->mExtent.roundedWidth();
        PAK::FileHdl   lTileHdl    = pthis->mTileHdls[pTileY * lWidth + pTileX];

        return tileMgr()->key(lTileHdl);
    }


    /*! @param pX Longitude (coordonn�e monde) de la tuile recherch�e.
        @param pY Latitude (coordonn�e monde) de la tuile recherch�e.
     */
    Data::TilePtr Data::tile(float pX, float pY)
    {
        const UTI::Rect&   lTileArea   = pthis->mExtent;
        UTI::Pointf        lPos(F32(pX / TILE::eEdgeSize), F32(pY / TILE::eEdgeSize));

        if (contains(lTileArea, lPos))
        {
            unsigned long   lTileX  = static_cast<unsigned long>(lPos.x - lTileArea.left());
            unsigned long   lTileY  = static_cast<unsigned long>(lTileArea.top() - lPos.y);
            unsigned long   lOffset = lTileY * lTileArea.roundedWidth() + lTileX;

            PAK::FileHdl   lFileHdl = pthis->mTileHdls[lOffset];

            if (lFileHdl == PAK::eBadHdl)
            {
                lFileHdl = pthis->createTileHdl(lOffset);

                if (lFileHdl == PAK::eBadHdl)
                {
                    return nullptr;
                }

                PAK::Saver         lSaver(lFileHdl);
                TILE::HeightGroup  lCorners;
                signed short    lX = lTileArea.left() + lTileX;
                signed short    lY = lTileArea.top() - lTileY;

                lCorners.insertInArea(TILE::Height(I16((lX    ) * TILE::eEdgeSize), I16((lY    ) * TILE::eEdgeSize), elevation(lTileX,     lTileY    )));
                lCorners.insertInArea(TILE::Height(I16((lX + 1) * TILE::eEdgeSize), I16((lY    ) * TILE::eEdgeSize), elevation(lTileX + 1, lTileY    )));
                lCorners.insertInArea(TILE::Height(I16((lX    ) * TILE::eEdgeSize), I16((lY - 1) * TILE::eEdgeSize), elevation(lTileX,     lTileY + 1)));
                lCorners.insertInArea(TILE::Height(I16((lX + 1) * TILE::eEdgeSize), I16((lY - 1) * TILE::eEdgeSize), elevation(lTileX + 1, lTileY + 1)));

                TILE::HeightGroup  lHeights    = generateTileHeights(lCorners);
                TILE::Tile::saveDefault(lSaver, lHeights, pthis->mOutsideHdl);
            }

            TILE::Key      lKey    = tileMgr()->key(lFileHdl);
            TilePtr        lTile   = tileMgr()->retrieve(lKey, true);

            Private::LoadedTiles& lLoadedTiles    = pthis->mLoadedTiles;
            bool                    lNew            = true;

            for(auto lTileI = lLoadedTiles.begin(); lNew && (lTileI != lLoadedTiles.end());)
            {
                if      (lTileI->expired())
                {
                    // Ce cas ne devrait arriver qu'apr�s avoir fait un peu tourner le moteur.
                    if (lTileI != lLoadedTiles.begin())
                    {
                        std::swap(*lTileI, lLoadedTiles.front());
                    }
                    lLoadedTiles.pop_front();
                }
                else if (lTileI->lock() == lTile)
                {
                    lNew = false;
                }
                else
                {
                    ++lTileI;
                }
            }
            if (lNew)
            {
                if (eMaxLoadedTiles <= lLoadedTiles.size())
                {
                    for(auto lTileI = lLoadedTiles.begin(); lTileI != lLoadedTiles.end(); ++lTileI)
                    {
                        TilePtr lT = lTileI->lock();
                        if (lT != lTile)
                        {
                            RSC::ResourceMgr<TILE::TileRsc>::TResPtr  lTileToRelease = std::dynamic_pointer_cast<TILE::TileRsc>(lT);
                            tileMgr()->release(lTileToRelease);
                            if (lTileI != lLoadedTiles.begin())
                            {
                                std::swap(*lTileI, lLoadedTiles.front());
                            }
                            lLoadedTiles.pop_front();
                            break;
                        }
                    }
                }
                lLoadedTiles.push_back(lTile);
            }

            return lTile;
        }
        else
        {
            return nullptr;
        }
    }


    /*! [D�]Verrouillage d'une tuile.
        @param pTileX "Longitude" de la tuile (unit� = 1 tuile); 0 est � l'ouest
        @param pTileY "Latitude" de la tuile (unit� = 1 tuile); 0 est au nord
        @param pOn    VRAI pour verouiller la tuile, FAUX pour la d�verrouiller
     */
    void Data::lockTile(TOffset pTileX, TOffset pTileY, bool pOn)
    {
        if (pOn)
        {
            pthis->setFlag(pTileX, pTileY, eLocked);
        }
        else
        {
            pthis->resetFlag(pTileX, pTileY, eLocked);
        }
    }


    /*! Permet de savoir si une tuile donn�e est �ditable (du point de vue du monde) ou verrouill�e.
        @param pTileX "Longitude" de la tuile (unit� = 1 tuile); 0 est � l'ouest.
        @param pTileY "Latitude" de la tuile (unit� = 1 tuile); 0 est au nord.
        @return VRAI si la tuile est verrouill�e ou n'existe pas, FAUX si elle peut encore �tre �dit�e.
     */
    bool Data::isTileLocked(TOffset pTileX, TOffset pTileY) const
    {
        auto lFlag = pthis->checkFlag(pTileX, pTileY, eLocked);
        return !lFlag || lFlag.value();
    }


    /*! [D�]Modification d'une tuile.
        @param pTileX  "Longitude" de la tuile (unit� = 1 tuile); 0 est � l'ouest
        @param pTileY  "Latitude" de la tuile (unit� = 1 tuile); 0 est au nord
        @param pOn VRAI pour signaler que la tuile a �t� modifi�e, FAUX pour la consid�rer comme non-modifi�e (dans les 2 cas, depuis la
                    derni�re sauvegarde)
     */
    void Data::modifyTile(TOffset pTileX, TOffset pTileY, bool pOn)
    {
        if (pOn)
        {
            pthis->setFlag(pTileX, pTileY, eModified);
        }
        else
        {
            pthis->resetFlag(pTileX, pTileY, eModified);
        }
        pthis->mChanges.set(EModified::eFlags);
    }


    /*! Permet de savoir si une tuile donn�e a �t� modifi�e pour sa cr�ation ou son chargement.
        @param pTileX "Longitude" de la tuile (unit� = 1 tuile); 0 est � l'ouest.
        @param pTileY "Latitude" de la tuile (unit� = 1 tuile); 0 est au nord.
        @return VRAI si la tuile a �t� modifi�e, FAUX sinon (y compris si elle n'existe pas).
     */
    bool Data::isTileModified(TOffset pTileX, TOffset pTileY) const
    {
        auto lFlag = pthis->checkFlag(pTileX, pTileY, eModified);
        return lFlag || lFlag.value();
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Data::Data(PAK::FileHdl pGroup, OSGi::Context* pContext)
        : pthis{this, pGroup, pContext}
    {
        pthis->load();

        if (pthis->string("Outside").isEmpty())
        {
            PAK::Loader         lLoader{pGroup};
            WOR::World::Group   lGroup{lLoader};
            pthis->mOutsideHdl = lGroup.worldHdls()[WOR::World::Group::eOutside];
            auto    lUniverse  = pContext->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game")->universe();
            pthis->mTileMgr    = lUniverse->tileMgr(pthis->mOutsideHdl);

            moveData(UTI::Rect(F32(-4.0F), F32(4.0F), F32(4.0F), F32(-4.0F)));
        }
        else
        {
            pthis->loadAll();
        }
    }
}
