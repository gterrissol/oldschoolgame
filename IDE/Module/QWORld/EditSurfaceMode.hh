/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_EDITSURFACEMODE_HH
#define QWOR_EDITSURFACEMODE_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/EditSurfaceMode.hh
    @brief En-t�te de la classe QWORld::EditSurfaceMode.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Module/QT3D/Mode.hh"
#include "STL/SharedPtr.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                         Mode d'Edition des El�vations
//------------------------------------------------------------------------------

    /*! @brief Mode d'�dition des �l�vations du monde.
        @version 0.5
     */
    class EditSurfaceMode : public QT3D::Mode
    {
    public:
        //! @name Type de pointeur
        //@{
        using BrushPtr  = std::weak_ptr<Brush>;                     //!< Pointeur sur pinceau.
        //@}
        //! @name Constructeur & destructeur
        //@{
                EditSurfaceMode(WidgetPtr pWindow, QString pName);  //!< Constructeur.
                ~EditSurfaceMode();                                 //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
        void    setBrush(BrushPtr pBrushToUse);                     //!< D�finition du pinceau � utiliser.
        //@}

    protected:

virtual void    onEnter();                                          //!< Pr�paration de l'activation du mode.
virtual void    onExit();                                           //!< Pr�paration de la sortie du mode.
virtual void    doUpdate();                                         //!< Mise � jour (forc�e).

    private:
        //! @name R�actions aux �v�nement souris
        //@{
virtual void    press();                                            //!< Pression bouton (utile) souris.
virtual void    release();                                          //!< Rel�chement bouton (utile) souris.
virtual void    doWheel();                                          //!< Utilisation de la molette.
virtual void    doDrag();                                           //!< Glissement de la souris.
        //@}
        BrushPtr    mBrush;                                         //!< Pinceau.
        float       mScale;                                         //!< Echelle.
        bool        mActivePen;                                     //!< Pinceau actif ?
    };
}

#endif  // De QWOR_EDITSURFACEMODE_HH
