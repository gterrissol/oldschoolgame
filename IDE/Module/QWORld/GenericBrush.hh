/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_GENERICBRUSH_HH
#define QWOR_GENERICBRUSH_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/GenericBrush.hh
    @brief En-t�te des classes QWOR::GenericBrush, QWOR::GenericElevationBrush,
    QWOR::GenericCliffBrush, QWOR::GenericSlideBrush & QWOR::GenericEnvironmentBrush.
    @author @ref Guillaume_Terrissol
    @date 21 F�vrier 2009 - 6 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "MATerial/BaseMaterial.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Tuple.hh"
#include "STL/Vector.hh"
#include "TILE/HeightField.hh"
#include "TIME/Timer.hh"
#include "UTIlity/Rect.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                       Interface des Pinceaux G�n�riques
//------------------------------------------------------------------------------

    /*! @brief "Pinceau" g�n�rique.
        @version 0.9

        Un pinceau peut "dessiner" des �l�vations (d'au moins deux mani�res), ou peindre une texture. La
        fa�on de proc�der �tant la m�me dans les deux cas, j'ai factoris�e leur impl�mentation au
        maximum. Chaque pinceau peut �tre compl�tement d�fini en fournissant une impl�mentation pour les
        m�thodes retrieveData(), applyToPieceOfData() et validateData(), et en instanciant cette classe
        par les param�tres template idoines (<i>TGroup</i> devrait �tre une instanciation de
        TILE::Group et <i>TController</i>, le type du sujet
        �dit�).
     */
    template<class TBrush, class TGroup, class TController>
    class GenericBrush : public TBrush
    {
    public:
        //! @name Alias de types
        //@{
        using TWeights          = typename TBrush::Weights;                     //!< Poids.
        using TData             = typename TGroup::TElement;                    //!< Donn�e �dit�e (compl�te).
        using TOffset           = typename TData::TOffset;                      //!< Type utilis� pour exprimer les coordonn�es des donn�es.
        using TType             = typename TData::TType;                        //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        using TControllerPtr    = typename std::weak_ptr<TController>;          //!< Contr�leur d'objet � peindre.
        using TWeightedGroup    = std::tuple<TGroup, TWeights>;                 //!< Groupe de donn�es pond�r�es.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        GenericBrush(TControllerPtr pEdited);                   //!< Constructeur.
virtual                 ~GenericBrush();                                        //!< Destructeur.
        //@}

    protected:
        //! @name Informations
        //@{
        int             brushX() const;                                         //!< Abscisse du point d'application du pinceau.
        int             brushY() const;                                         //!< Ordonn�e du point d'application du pinceau.
        TType           brushData() const;                                      //!< Donn�e au point d'application du pinceau.
        UTI::Rect       modifiedExtent() const;                                 //!< Derni�re zone modifi�e.
        bool            retrievePieceOfData(TData& pRequestedPiece) const;      //!< R�cup�ration d'une donn�e.
        MATH::V4        vertexForModel(const TData& pPieceOfData,
                                       float pWeight) const;                    //!< Vertex du mod�le pour une position donn�e.
        //@}

    private:
        //! @name Traitement sur les donn�es (algorithme)
        //@{
virtual bool            doBegin(int pX, int pY);                                //!< D�but de l'application du "pinceau" sur une zone.
virtual void            doApply(int pX, int pY);                                //!< Application du "pinceau" sur une zone.
virtual void            doMoveTo(int pX, int pY);                               //!< D�placement "simple" du pinceau.
        //@}
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
        TWeightedGroup  createGroup(int pX, int pY, bool pForModel) const;      //!< Pr�paratio d'un ensemble de donn�es � �diter.
        void            retrieveData(TGroup& pData) const;                      //!< R�cup�ration d'un groupe de donn�es.
virtual void            onBegin();                                              //!< Travail suppl�mentaire � r�aliser en d�but d'application du pinceau.
virtual void            beforeApply();                                          //!< Travail � r�aliser avant d'�diter les donn�es.
virtual void            applyToPiecesOfData(TWeightedGroup & pData) const = 0;  //!< Edition d'un groupe de donn�es.
        void            validateData(const TGroup& pData);                      //!< Validation d'un 'coup de pinceau'.

virtual UTI::Rect       extentForEdition(int pX, int pY) const;                 //!< Zone � �diter.
virtual UTI::Rect       extentUnderModel(int pX, int pY) const = 0;             //!< Zone couverte par l'apparence du pinceau.
virtual UTI::Rect       validExtentForModel() const = 0;                        //!< Zone valide pour l'apparence du pinceau.
virtual void            transformModel(TWeights& pWeights) const;               //!< Modification des poids pour l'apparence du pinceau.
virtual const Weights*  modelWeights() const;                                   //!< Poids pour l'apparence du pinceau.
virtual const Weights*  editionWeights() const;                                 //!< Poids pour l'�dition.
        //@}

        TControllerPtr      mEdited;                                            //!< Entit� �dit�e.
        //! @name Informations sur le "premier coup de pinceau"
        //@{
        int                 mX;                                                 //!< Abscisse du pinceau lors du premier appui.
        int                 mY;                                                 //!< Ordonn�e du pinceau lors du premier appui.
        TType               mPieceOfData;                                       //!< Donn�e sous le pinceau lors du premier appui.
        //@}
        mutable UTI::Rect   mLastModifiedExtent;                                //!< Zone (carr�e) modifi�e lors du dernier apply().
    };


//------------------------------------------------------------------------------
//                              Pinceaux G�n�riques
//------------------------------------------------------------------------------

    /*! @brief "Pinceau" g�n�rique pour des �l�vations.
        @version 0.9

        Ce pinceau est une premi�re sp�cialisation de GenericBrush. Deux pinceaux "finaux" manipulent des �l�vations donc, plut�t que de
        coder pour chacun d'eux des m�thodes identiques, j'ai cr�� cette classe pour factoriser le code.
        @sa GenericCliffBrush & GenericSlideBrush
     */
    template<class TBrush, class TGroup, class TController>
    class GenericElevationBrush : public GenericBrush<TBrush, TGroup, TController>
    {
    public:
        //! @name Alias de types
        //@{
        using TBase             = GenericBrush<TBrush, TGroup, TController>;    //!< Pinceau d�riv�.
        using TData             = typename TBase::TData;                        //!< Donn�e �dit�e (compl�te).
        using TOffset           = typename TBase::TOffset;                      //!< Type utilis� pour exprimer les coordonn�es des donn�es.
        using TType             = typename TBase::TType;                        //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        using TControllerPtr    = typename TBase::TControllerPtr;               //!< Contr�leur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    GenericElevationBrush(TControllerPtr pEdited);              //!< Constructeur.
virtual             ~GenericElevationBrush();                                   //!< Destructeur.
        //@}

    protected:
        //! @name Acc�s aux extr�ma
        //@{
        TType       localMaximum(const TData& pPieceOfData) const;              //!< Voisin le plus �lev�.
        TType       localMinimum(const TData& pPieceOfData) const;              //!< Voisin le moins �lev�
        void        getNeighbours(const TData& pPieceOfData,
                                  std::vector<TType>& pElevations) const;       //!< Elevations des voisins.
        //@}

    private:
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
virtual UTI::Rect   extentUnderModel(int pX, int pY) const;                     //!< Zone couverte par l'apparence du pinceau.
virtual UTI::Rect   validExtentForModel() const;                                //!< Zone valide pour l'apparence du pinceau.
        //@}
    };


    /*! @brief "Pinceau" g�n�rique pour falaises.
        @version 0.8

        Ce pinceau permet d'�diter des �l�vations par niveaux (de quelques m�tres), les 'fronti�res' avec des zones de niveaux diff�rents
        �tant franches.
        @sa SlideBrush
     */
    template<class TGroup, class TController>
    class GenericCliffBrush : public GenericElevationBrush<CliffBrush, TGroup, TController>
    {
    public:
        //! @name Alias de types
        //@{
        using TBase             = GenericElevationBrush<ElevationBrush, TGroup, TController>;   //!< Pinceau d�riv�.
        using TWeights          = typename TBase::TWeights;                                     //!< Poids.
        using TData             = typename TBase::TData;                                        //!< Donn�e �dit�e (compl�te).
        using TOffset           = typename TBase::TOffset;                                      //!< Type utilis� pour exprimer les coordonn�es des donn�es.
        using TType             = typename TBase::TType;                                        //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        using TControllerPtr    = typename TBase::TControllerPtr;                               //!< Contr�leur.
        using Ptr               = typename TBase::Ptr;                                          //!< Pointeur sur pinceau.
        using TWeightedGroup    = typename TBase::TWeightedGroup;                               //!< Groupe de donn�es pond�r�es.
        //@}
        //! @name Constructeur & destructeur
        //@{
                GenericCliffBrush(TControllerPtr pEdited);                                      //!< Constructeur.
virtual         ~GenericCliffBrush();                                                           //!< Destructeur.
        //@}

    private:
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
        void    onBegin() override;                                                             //!< Travail suppl�mentaire � r�aliser en d�but d'application du pinceau.
        void    applyToPiecesOfData(TWeightedGroup & pData) const override;                     //!< Edition d'un groupe de donn�es.
        void    transformModel(TWeights& pWeights) const override;                              //!< Modification des poids pour l'apparence du pinceau.
        //@}
        //! @name Configuration
        void    doForwardState(Ptr pFrom);                                                      //!< R�cup�ration de l'�tat d'�dition d'un autre pinceau.
        //@}
        //! @name Param�tre
        //@{
        TType   mCurrentReferenceAltitude;                                                      //!< Altitude de r�f�rence actuelle.
        //@}
    };


    /*! @brief "Pinceau" "analogique".
        @version 0.8

        Ce pinceau permet d'�diter des �l�vations de mani�re assez 'fine', en adoucissant les surfaces.
        @sa QWOR::CliffBrush
     */
    template<class TGroup, class TController>
    class GenericSlideBrush : public GenericElevationBrush<SlideBrush, TGroup, TController>
    {
    public:
        //! @name Alias de types
        //@{
        using TBase          = GenericElevationBrush<ElevationBrush, TGroup, TController>;  //!< Pinceau d�riv�.
        using TData          = typename TBase::TData;                                       //!< Donn�e �dit�e (compl�te).
        using TOffset        = typename TBase::TOffset;                                     //!< Type utilis� pour exprimer les coordonn�es des donn�es.
        using TType          = typename TBase::TType;                                       //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        using TControllerPtr = typename TBase::TControllerPtr;                              //!< Contr�leur.
        using TWeightedGroup = typename TBase::TWeightedGroup;                              //!< Groupe de donn�es pond�r�es.
        //@}
        //! @name Constructeur & destructeur
        //@{
                GenericSlideBrush(TControllerPtr pEdited);                                  //!< Constructeur.
virtual         ~GenericSlideBrush();                                                       //!< Destructeur.
        //@}

    private:
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
        void    beforeApply() override;                                                     //!< Travail � r�aliser avant d'�diter les donn�es.
        void    applyToPiecesOfData(TWeightedGroup & pData) const override;                 //!< Edition d'un groupe de donn�es.
        //@}
        //! @name Temporisation de l'application du pinceau
        //@{
        enum
        {
            eDelayInMicroSeconds = 50000                                                    //!< P�riode d'application du pinceau.
        };
        TIME::Timer mTimer;                                                                 //!< Timer pour chronom�trer la fr�quence d'utilisation du pinceau.
        bool        mMayApply;                                                              //!< Droit d'utilisation du pinceau.
        //@}
    };


    /*! @brief "Pinceau" d'environnement.
        @version 0.9

        Base pour pinceau d'environnement.
     */
    template<class TGroup, class TController>
    class GenericEnvironmentBrush : public GenericBrush<EnvironmentBrush, TGroup, TController>
    {
    public:
        //! @name Alias de types
        //@{
        using TBase          = GenericBrush<EnvironmentBrush, TGroup, TController>; //!< Pinceau d�riv�.
        using TWeights       = typename TBase::TWeights;                            //!< Poids.
        using TData          = typename TBase::TData;                               //!< Donn�e �dit�e (compl�te).
        using TOffset        = typename TBase::TOffset;                             //!< Type utilis� pour exprimer les coordonn�es des donn�es.
        using TType          = typename TBase::TType;                               //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        using TControllerPtr = typename TBase::TControllerPtr;                      //!< Contr�leur.
        using TWeightedGroup = typename TBase::TWeightedGroup;                      //!< Groupe de donn�es pond�r�es.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    GenericEnvironmentBrush(TControllerPtr pEdited);                //!< Constructeur.
virtual             ~GenericEnvironmentBrush();                                     //!< Destructeur.
        //@}

    private:
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
        void        applyToPiecesOfData(TWeightedGroup & pData) const override;     //!< Edition d'un groupe de donn�es.
        UTI::Rect   extentForEdition(int pX, int pY) const override;                //!< Zone � �diter.
        UTI::Rect   extentUnderModel(int pX, int pY) const override;                //!< Zone couverte par l'apparence du pinceau.
        UTI::Rect   validExtentForModel() const override;                           //!< Zone valide pour l'apparence du pinceau.
        void        transformModel(TWeights& pWeights) const override;              //!< Modification des poids pour l'apparence du pinceau.
        //@}
    };
}

#endif  // De QWOR_GENERICBRUSH_HH
