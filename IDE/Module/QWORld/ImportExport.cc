/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ImportExport.hh"

/*! @file IDE/Module/QWORld/ImportExport.cc
    @brief M�thodes (non-inline) de la classe QWOR::ImportExport.
    @author @ref Guillaume_Terrissol
    @date 31 Mai 2014 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QWOR
{
    /*! Par d�faut.
     */
    ImportExport::~ImportExport() = default;


    /*! Tous les types de monde commencent par exporter les m�mes donn�es : nom, type, etc.@n
        Cette m�thode permet de factoriser cet export commun.
     */
    void ImportExport::exportHeader(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        data()->write<eFileHdl>(pWriter, pLoader, "Common data");
        data()->write<eI32>(pWriter, pLoader, "World Type");
    }
}
