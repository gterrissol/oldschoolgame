/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_BRUSH_HH
#define QWOR_BRUSH_HH

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/Brush.hh
    @brief En-t�te des classes QWOR::Brush, QWOR::ElevationBrush, QWORl::CliffBrush,
    QWORl::SlideBrush, QWORl::EnvironmentBrush, QWOR::DropBrush & QWOR::Brushable.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 10 Mai2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "MATerial/BaseMaterial.hh"
#include "RenDeR/Renderer.hh"
#include "STL/Tuple.hh"
#include "STL/Vector.hh"
#include "TILE/HeightField.hh"
#include "TIME/Timer.hh"
#include "UTIlity/Rect.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                                    Pinceau
//------------------------------------------------------------------------------

    /*! @brief "Pinceau" pour l'�dition de monde.
        @version 0.92

        Les pinceaux s'appliquent sur des objets de type sans rapport. Afin de factoriser le code, j'ai
        donc introduit des template. Mais il �tait pr�f�rable de garder une base commune (pour tout ce
        qui ne rel�ve pas des template). Cette classe est donc la vraie racine de la hi�rarchie de
        pinceaux.
     */
    class Brush
    {
    public:
        //! @name Types de pointeur
        //@{
        using AppearancePtr = std::shared_ptr<APP::Appearance>;             //!< Pointeur sur apparence.
        using GeometryPtr   = std::shared_ptr<GEO::Geometry>;               //!< Pointeur sur g�om�trie.
        using MaterialPtr   = std::shared_ptr<MAT::Material>;               //!< Pointeur sur mat�riau.
        using Ptr           = std::shared_ptr<Brush>;                       //!< Pointeur sur pinceau.
        //@}
        //! @name Autres Types
        //@{
        using Weights       = std::vector<float>;                           //!< Poids.
        using ModelVertices = std::vector<MATH::V4>;                        //!< Groupe d'�l�vations pour la mise � jour de l'apparence du pinceau.
        //@}
        //! @name Configuration : �num�rations
        //@{
        //! Forme
        enum EShape
        {
            eRound,                                                         //!< Rond.
            eSquare                                                         //!< Carr�.
        };
        //! Tailles
        enum
        {
            eMinBrushSize  = 1,                                             //!< ... minimale (diam�tre ou c�t�, selon la forme).
            eMaxBrushSize  = 8                                              //!< ... maximale (diam�tre ou c�t�, selon la forme).
        };
        //@}
        //! @name Constructeur & destructeur
        //@{
                                    Brush();                                //!< Constructeur par d�faut.
virtual                             ~Brush();                               //!< Destructeur.
        //@}
        //! @name Configuration g�n�rique
        //@{
                void                setShape(EShape pBrushShape);           //!< Forme du pinceau.
                void                setSize(unsigned short pSideLength);    //!< Taille du pinceau.
                void                setPlaneScale(float pScale);            //!< Mise � l'�chelle horizontale.
                float               planeScale() const;                     //!< Echelle actuelle horizontale.
                void                setExtent(const UTI::Rect& pExtent);    //!< Zone � peindre.
                const UTI::Rect&    workingExtent() const;                  //!< Zone de travail.
        inline  void                forwardState(Ptr pFrom);                //!< R�cup�ration de l'�tat d'�dition d'un autre pinceau.
        //@}
        //! @name Traitement sur les donn�es (interface)
        //@{
        inline  bool                begin(int pX, int pY);                  //!< D�but de l'application du "pinceau" sur une zone.
        inline  void                apply(int pX, int pY);                  //!< Application du "pinceau" sur une zone.
        inline  void                moveTo(int pX, int pY);                 //!< D�placement "simple" du pinceau.
        //@}
        //! @name Apparence
        //@{
                void                setVisible(bool pStatus);               //!< Affichage de l'apparence.
                bool                isVisible() const;                      //!< Affichage de l'apparence ?
                AppearancePtr       retrieveModel() const;                  //!< R�cup�ration du mod�le 3D du pinceau.

    protected:

                void                updateModel(ModelVertices& pVertices);  //!< Mise � jour de l'"apparence" du pinceau.
        //@}
        //! @name Traitement sur les donn�es (algorithme)
        //@{
virtual         bool                doBegin(int pX, int pY) = 0;            //!< D�but de l'application du "pinceau" sur une zone.
virtual         void                doApply(int pX, int pY) = 0;            //!< Application du "pinceau" sur une zone.
virtual         void                doMoveTo(int pX, int pY) = 0;           //!< D�placement "simple" du pinceau.
        //@}
        //! @name Configuration
        //@{
                EShape              shape() const;                          //!< Forme du pinceau.
                unsigned short      size() const;                           //!< Taille du pinceau.

    private:

virtual         void                doForwardState(Ptr pFrom);              //!< R�cup�ration de l'�tat d'�dition d'un autre pinceau.

        UTI::Rect       mWorkingExtent;                                     //!< Zone de travail.
        EShape          mShape;                                             //!< Forme du pinceau.
        float           mScale;                                             //!< Echelle du pinceau.
        unsigned short  mSize;                                              //!< Taille du pinceau.
        //@}
        GeometryPtr     mGeometry;                                          //!< "G�om�trie" du pinceau.
        MaterialPtr     mTexture;                                           //!< "Mat�riau" du pinceau.
        AppearancePtr   mModel;                                             //!< "Appararence" du pinceau.
        bool            mVisible;                                           //!< Pinceau visible ?
    };


//------------------------------------------------------------------------------
//                            Interfaces des Pinceaux
//------------------------------------------------------------------------------

    /*! @brief "Pinceau" pour des �l�vations.
        @version 0.3
     */
    class ElevationBrush : public Brush
    {
    public:
        //! @name Alias de type
        //@{
        using   TType = TILE::Height::TType;                //!< Donn�e �dit�e �l�mentaire (�l�vation ou environnement).
        //@}
        //! @name Constructeur & destructeur
        //@{
                        ElevationBrush();                   //!< Constructeur.
virtual                 ~ElevationBrush();                  //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
                void    setExtrema(TType pMin, TType pMax); //!< Intervalle de valeurs pour les donn�es.
        //@}

    protected:
        //! @name Param�tres
        //@{
        inline  TType   globalMaximum() const;              //!< Maximum global.
        inline  TType   globalMinimum() const;              //!< Minimum global.


    private:

        TType   mMinimum;                                   //!< Borne inf�rieure de la plage de valeurs autoris�es.
        TType   mMaximum;                                   //!< Borne sup�rieure de la plage de valeurs autoris�es.
        //@}
    };


    /*! @brief "Pinceau" pour falaises.
        @version 0.3
     */
    class CliffBrush : public ElevationBrush
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        CliffBrush();                               //!< Constructeur.
virtual                 ~CliffBrush();                              //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
        //! Edition par niveaux.
        enum ECliffs
        {
            eDecreaseTwo,                                           //!< Abaissement de deux niveaux.
            eDecreaseOne,                                           //!< Abaissement d'un niveau.
            eSameLevel,                                             //!< Extension du niveau.
            eIncreaseOne,                                           //!< El�vation d'un niveau.
            eIncreaseTwo                                            //!< El�vation de deux niveaux.
        };
                void    setCliff(ECliffs pType);                    //!< D�finition du type de falaise � "cr�er".
                void    setLevelHeight(TType pHeight);              //!< Diff�rence de hauteur entre deux niveaux cons�cutifs.
                void    setMaximumAmplitude(TType pMaxAmplitude);   //!< Amplitude maximale entre deux �l�vations adjacentes.
        //@}

    protected:
        //! @name Param�tres
        //@{
        inline  TType   offset() const;                             //!< Offset � appliquer sur les �l�vations.
        inline  TType   oneLevelHeight() const;                     //!< Hauteur d'un niveau de falaise.
        inline  TType   maximumAmplitude() const;                   //!< D�clinaison/inclinaison maximale.

    private:

        ECliffs mCliffType;                                         //!< Type de falaise � 'creuser'.
        TType   mOffset;                                            //!< Offset � appliquer sur les �l�vations.
        TType   mOneLevelHeight;                                    //!< Hauteur d'un niveau de falaise.
        TType   mMaximumAmplitude;                                  //!< D�clinaison/inclinaison maximale.
        //@}
    };


    /*! @brief "Pinceau" "analogique".
        @version 0.3
     */
    class SlideBrush : public ElevationBrush
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                            SlideBrush();                   //!< Constructeur.
virtual                     ~SlideBrush();                  //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
        //!< Edition fine.
        enum ESmoothing
        {
            eRaise,                                         //!< El�vation.
            eLower,                                         //!< Abaissement.
            ePlateau,                                       //!< Egalisation.
            eNoise,                                         //!< Bruitage.
            eSmooth                                         //!< Adoucissement.
        };
                void        setSmoothing(ESmoothing pType); //!< D�finition de la m�thode d'adoucissement.
        //@}

    protected:
        //! @name Param�tre
        //@{
        inline  ESmoothing  smoothing() const;              //!< Type d'adoucissement � appliquer.


    private:

        ESmoothing  mSmoothType;                            //!< Type d'adoucissement � appliquer.
        //@}
    };


    /*! @brief "Pinceau" d'environnement.
        @version 0.4
     */
    class EnvironmentBrush : public Brush
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                                    EnvironmentBrush();                     //!< Constructeur.
virtual                             ~EnvironmentBrush();                    //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
                void                setTilingId(TILE::TilingId pId);        //!< Choix d'un tiling.
                void                setBlending(TILE::EBlending pBlending); //!< Choix d'un m�lange.
        //@}

    protected:
        //! @name Param�tres
        //@{
        inline  TILE::TilingId      tilingId() const;                       //!< Codage de texture.
        inline  TILE::EBlending     blending() const;                       //!< M�lange.

    private:

        TILE::TilingId      mTilingId;                                      //!< Id de tiling.
        TILE::EBlending     mBlending;                                      //!< M�lange.
        //@}
    };


    /*! @brief "Pinceau" de d�p�t d'objets.
        @version 0.3
     */
    class DropBrush : public Brush
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                DropBrush();                                            //!< Constructeur.
virtual         ~DropBrush();                                           //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
        //@{
        //!< D�p�t.
        enum EType
        {
            eRegular,                                                   //!< R�gulier.
            eRandom                                                     //!< Al�atoire.
        };
        void    setType(EType pType);                                   //!< D�finition de la m�thode de d�p�t.
        void    setModel(AppearancePtr pModel);                         //!< D�finition du mod�le � d�poser.
        //@}

    private:
        //! @name Traitement sur les donn�es (algorithme)
        //@{
virtual bool    doBegin(int pX, int pY);                                //!< D�but de l'application du "pinceau" sur une zone.
virtual void    doApply(int pX, int pY);                                //!< Application du "pinceau" sur une zone.
virtual void    doMoveTo(int pX, int pY);                               //!< D�placement "simple" du pinceau.
virtual void    insertModel(AppearancePtr pModel, float pX, float pY);  //!< Insertion d'un mod�le.
        //@}
virtual void    doForwardState(Ptr pFrom);                              //!< R�cup�ration de l'�tat d'�dition d'un autre pinceau.
        //! @name Param�tres
        //@{
        EType           mDropType;                                      //!< Type de d�p�t.
        AppearancePtr   mModel;                                         //!< Mod�le � d�poser.
        //@}
    };



//------------------------------------------------------------------------------
//                                  "Brushable"
//------------------------------------------------------------------------------

    /*! @brief Objet sur lequel appliquer des coups de pinceaux.
        @version 0.4

        D�river de cette classe les contr�leurs des objets sur lesquels les pinceaux doivent pouvoir �tre
        appliqu�s.
     */
    class Brushable
    {
    public:
        //! @name Types de pointeur
        //@{
        using CliffBrushPtr         = std::shared_ptr<CliffBrush>;          //!< Pointeur sur pinceau de falaise.
        using SlideBrushPtr         = std::shared_ptr<SlideBrush>;          //!< Pointeur sur pinceau "analogique".
        using EnvironmentBrushPtr   = std::shared_ptr<EnvironmentBrush>;    //!< Pointeur sur pinceau d'environnement.
        using DropBrushPtr          = std::shared_ptr<DropBrush>;           //!< Pointeur sur pinceau de d�p�t d'objets.
        //@}
virtual                             ~Brushable();                           //!< Destructeur.
        //! @name Informations
        //@{
        inline  UTI::Rect           worldExtent() const;                    //!< Zone � �diter.
        inline  UTI::Rect           textureExtent() const;                  //!< Zone de texture � �diter.
        inline  int                 type() const;                           //!< Identifiant.
        //@}
        //! @name Configration des pinceaux
        //@{
        inline  CliffBrushPtr       cliffBrush();                           //!< Pinceau de falaise.
        inline  SlideBrushPtr       slideBrush();                           //!< Pinceau "analogique"
        inline  EnvironmentBrushPtr environmentBrush();                     //!< Pinceau d'environnement.
        inline  DropBrushPtr        dropBrush();                            //!< Pinceau de d�p�t d'objets.
        //@}

    private:
        //! @name Impl�mentation
        //@{
virtual         UTI::Rect           getWorldExtent() const = 0;             //!< Zone � �diter.
virtual         UTI::Rect           getTextureExtent() const = 0;           //!< Zone de texture � �diter.
virtual         int                 getType() const = 0;                    //!< Identifiant.
virtual         CliffBrushPtr       getCliffBrush() = 0;                    //!< Pinceau de falaise.
virtual         SlideBrushPtr       getSlideBrush() = 0;                    //!< Pinceau "analogique".
virtual         EnvironmentBrushPtr getEnvironmentBrush() = 0;              //!< Pinceau d'environnement.
virtual         DropBrushPtr        getDropBrush();                         //!< Pinceau de d�p�t d'objets.
        //@}
    };


//------------------------------------------------------------------------------
//                      Poids des Masques pour les Pinceaux
//------------------------------------------------------------------------------

    //! @name Poids pour les masques de pinceaux.
    struct Weights
    {
        const float w1[1][1];       //!< Zone 1 x 1.
        const float w2[2][2];       //!< Zone 2 x 2.
        const float w3[3][3];       //!< Zone 3 x 3.
        const float w4[4][4];       //!< Zone 4 x 4.
        const float w5[5][5];       //!< Zone 5 x 5.
        const float w6[6][6];       //!< Zone 6 x 6.
        const float w7[7][7];       //!< Zone 7 x 7.
        const float w8[8][8];       //!< Zone 8 x 8.
        const float w9[9][9];       //!< Zone 9 x 9.
        const float w10[10][10];    //!< Zone 10 x 10.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Brush.inl"

#endif  // De QWOR_BRUSH_HH
