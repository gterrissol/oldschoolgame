/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QWOR_CONST_HH
#define QWOR_CONST_HH

/*! @file IDE/Module/QWORld/Const.hh
    @brief Enum�rations du module QWOR.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QWOR
{
//------------------------------------------------------------------------------
//                                  Nom du Mode
//------------------------------------------------------------------------------

    extern  const char* kEditSurfaceMode;    //!< Nom du mode d'�dition de surface.
}

#endif // De QWOR_CONST_HH
