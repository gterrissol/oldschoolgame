/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/Module/QWORld/Brush.inl
    @brief M�thodes inline des classes QWOR::Brush, QWOR::ElevationBrush, QWORl::CliffBrush,
    QWORl::SlideBrush, QWORl::EnvironmentBrush, QWOR::DropBrush & QWOR::Brushable.
    @author @ref Guillaume_Terrissol
    @date 15 F�vrier 2009 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QWOR
{
//------------------------------------------------------------------------------
//                      Pinceau : Configuration g�n�rique
//------------------------------------------------------------------------------

    /*! En cas de remplacement d'un pinceau par un autre du m�me type, il peut �tre int�ressant de r�cup�rer les informations de
        configuration sp�cifiques, afin de conserver le m�me comportement, sur un autre objet �dit�.
        @param pFrom Pinceau dont r�cup�rer l'�tat d'�dition, si cela est possible
     */
    inline void Brush::forwardState(Ptr pFrom)
    {
        doForwardState(pFrom);
    }


//------------------------------------------------------------------------------
//               Pinceau : Traitement sur les Donn�es (Interface)
//------------------------------------------------------------------------------

    /*! Commence � appliquer le pinceau sur une zone.<br>
        Les coordonn�es et la hauteur sous le pinceau sont m�moris�es : certaines 'techniques de peinture' requi�rent ces informations.
        @param pX Abscisse du coup de pinceau (coordonn�e locale)
        @param pY Ordonn�e du coup de pinceau (coordonn�e locale)
        @return Vrai si le pinceau a commenc� � dessiner, Faux sinon (e.g. l'utilisateur a piqu� ailleurs que sur le terrain)
        @note J'ai introduit ce patron de m�thode afin de permettre la fusion des pinceaux de tuiles et de monde ext�rieur (interface
        publique commune avant les d�riv�es template)
     */
    inline bool Brush::begin(int pX, int pY)
    {
        if (doBegin(pX, pY))
        {
            doMoveTo(pX, pY); // Met � jour la forme du pinceau.
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! Apr�s avoir 'pos� le pinceau', celui-ci est d�plac�.<br>
        Une zone fonction de la taille et de la forme du pinceau sera peinte (applyToPieceOfData() sera appell�e sur chaque donn�e concern�e)
        @param pX Nouvelle abscisse du pinceau (coordonn�e locale)
        @param pY Nouvelle ordonn�e du pinceau (coordonn�e locale)
        @note J'ai introduit ce patron de m�thode afin de permettre la fusion des pinceaux de tuiles et de monde ext�rieur (interface
        publique commune avant les d�riv�es template)
     */
    inline void Brush::apply(int pX, int pY)
    {
        doApply(pX, pY);
        moveTo(pX, pY); // Met � jour la forme du pinceau.
    }


    /*! Pr�pare l'apparence du pinceau pour son affichage � une [nouvelle] position.
        @param pX Nouvelle abscisse du pinceau (coordonn�e locale)
        @param pY Nouvelle ordonn�e du pinceau (coordonn�e locale)
        @note J'ai introduit ce patron de m�thode afin de permettre la fusion des pinceaux de tuiles et de monde ext�rieur (interface
        publique commune avant les d�riv�es template)
     */
    inline void Brush::moveTo(int pX, int pY)
    {
        doMoveTo(pX, pY);
    }


//------------------------------------------------------------------------------
//                    Accesseurs des Interfaces des Pinceaux
//------------------------------------------------------------------------------

    /*! @return La plus grande valeur autoris�e pour une �l�vation
     */
    inline ElevationBrush::TType ElevationBrush::globalMaximum() const
    {
        return mMaximum;
    }


    /*! @return La plus petite valeur autoris�e pour une �l�vation
     */
    inline ElevationBrush::TType ElevationBrush::globalMinimum() const
    {
        return mMinimum;
    }


    /*!
     */
    inline CliffBrush::TType CliffBrush::offset() const
    {
        return mOffset;
    }


    /*!
     */
    inline CliffBrush::TType CliffBrush::oneLevelHeight() const
    {
        return mOneLevelHeight;
    }


    /*!
     */
    inline CliffBrush::TType CliffBrush::maximumAmplitude() const
    {
        return mMaximumAmplitude;
    }


    /*!
     */
    inline SlideBrush::ESmoothing SlideBrush::smoothing() const
    {
        return mSmoothType;
    }


    /*!
     */
    inline TILE::TilingId EnvironmentBrush::tilingId() const
    {
        return mTilingId;
    }


    /*!
     */
    inline TILE::EBlending EnvironmentBrush::blending() const
    {
        return mBlending;
    }


//------------------------------------------------------------------------------
//                                  "Brushable"
//------------------------------------------------------------------------------

    /*! @return La zone couverte par l'objet peignable (coordonn�es locales)
     */
    inline UTI::Rect Brushable::worldExtent() const
    {
        return getWorldExtent();
    }


    /*! @return La zone couverte par la texture de l'objet peignable (coordonn�es locales)
     */
    inline UTI::Rect Brushable::textureExtent() const
    {
        return getTextureExtent();
    }


    /*! @return Le type associ� au mode de l'objet
     */
    inline int Brushable::type() const
    {
        return getType();
    }


    /*! @return Un pinceau de falaise nouvellement cr��
     */
    inline Brushable::CliffBrushPtr Brushable::cliffBrush()
    {
        return getCliffBrush();
    }


    /*! @return Un pinceau "analogique" nouvellement cr��
     */
    inline Brushable::SlideBrushPtr Brushable::slideBrush()
    {
        return getSlideBrush();
    }


    /*! @return Un pinceau d'environnement nouvellement cr��
     */
    inline Brushable::EnvironmentBrushPtr Brushable::environmentBrush()
    {
        return getEnvironmentBrush();
    }


    /*! @return Un pinceau de d�p�t de mod�le nouvellement cr��
     */
    inline Brushable::DropBrushPtr Brushable::dropBrush()
    {
        return getDropBrush();
    }
}
