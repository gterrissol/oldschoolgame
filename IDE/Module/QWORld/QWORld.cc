/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QWORld.hh"

/*! @file IDE/Module/QWORld/QWORld.cc
    @brief D�finitions diverses du module QWOR.
    @author @ref Guillaume_Terrissol
    @date 1er Septembre 2003 - 14 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QXmlStreamWriter>

#include "CORE/Activator.hh"
#include "CORE/ProgressService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "ERRor/ErrMsg.hh"
#include "PAcKage/LoaderSaver.hh"
#include "TILE/Enum.hh"
#include "UTIlity/Rect.hh"

#include "Brushes.hh"
#include "WorldDataService.hh"

namespace QWOR
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kDeleteResourceFailed,  "Echec de suppression de la ressource.",    "Delete resource failed.")
    REGISTER_ERR_MSG(kInvalidData,           "Donn�es invalides.",                       "Invalid data.")
    REGISTER_ERR_MSG(kInvalidExtrema,        "Extrema invalides.",                       "Invalid extrema.")
    REGISTER_ERR_MSG(kModelMustExist,        "Le mod�le doit exister.",                  "Model must exist.")
    REGISTER_ERR_MSG(kNoActiveSubject,       "Pas de sujet actif.",                      "No active subject.")
    REGISTER_ERR_MSG(kNoCliffBrush,          "Pinceau de falaises inexistant.",          "No cliff brush.")
    REGISTER_ERR_MSG(kNoDropBrush,           "Pinceau de d�p�t inexistant.",             "No drop brush.")
    REGISTER_ERR_MSG(kNoEnvironmentBrush,    "Pinceau d'environement inexistant.",       "No environment brush.")
    REGISTER_ERR_MSG(kNoObjectToEdit,        "Pas d'objet � �diter.",                    "No object to edit.")
    REGISTER_ERR_MSG(kNoSmoothBrush,         "Pinceau 'analogique' inexistant.",         "No smooth brush.")
    REGISTER_ERR_MSG(kReleaseResourceFailed, "Echec de lib�ration de la ressource.",     "Release resource failed.")
    REGISTER_ERR_MSG(kUnknownBrushShape,     "Forme du pinceau inconnue.",               "Unknown brush shape.")


//------------------------------------------------------------------------------
//                                  Nom du Mode
//------------------------------------------------------------------------------

#if     defined(FRANCAIS)
    const char* kEditSurfaceMode   = "Mode d'�dition des surfaces.";
#elif   defined(ENGLISH)
    const char* kEditSurfaceMode   = "Surface edition mode.";
#endif  // Du langage.


//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de monde ext�rieur.
        @version 0.25
     */
    class ImportExportWorldGroup : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportWorldGroup();                                          //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


//------------------------------------------------------------------------------
//                               Donn�es de Monde
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportWorldGroup::~ImportExportWorldGroup() { }


    /*!
     */
    bool ImportExportWorldGroup::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        pWriter.setAutoFormattingIndent(1);

        data()->write<eFileHdl>(pWriter, pLoader, "Common data");
        data()->write<eFileHdl>(pWriter, pLoader, "Outside");
        data()->write<eFileHdl>(pWriter, pLoader, "Underground");

        U32 lInsideCount = k0UL;
        pLoader >> lInsideCount;
        pLoader.rollback(U32(sizeof(lInsideCount)));

        data()->write<eU32>(pWriter, pLoader, "Inside world count");

        for(U32 lI = k0UL; lI < lInsideCount; ++lI)
        {
            data()->write<eFileHdl>(pWriter, pLoader, QString("Inside %1").arg(1 + lI, 4, 10, QChar(' ')));
        }

        /*if (0 < pLoader.remaining())
        {
            UTI::Rect  lZone;
            pLoader >> lZone;
            pLoader.rollback(U32(sizeof(lZone)));

            data()->write<eRect>(pWriter, pLoader, "Zone");
            data()->write<eFileHdl>(pWriter, pLoader, "Tile handles file");
            data()->write<eFileHdl>(pWriter, pLoader, "Tile counter file");

            long    lWidth  = static_cast<long>(lZone.width());
            long    lHeight = static_cast<long>(lZone.height());
            if ((lWidth != 0) && (lHeight != 0))
            {
                long    lHeightCount    = (lWidth + 1)                    * (lHeight + 1);
                long    lTexelCount     = (lWidth * TILE::eFragmentCount) * (lHeight * TILE::eFragmentCount);
                long    lTileFlagCount  =  lWidth                         *  lHeight;

                auto    lProgress   = data()->fileDB()->context()->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lHeightCount + lTexelCount + lTileFlagCount);
                lProgress->advance(0);

                data()->begin(pWriter, pLoader, "Heights");
                {
                    for(long lH = 0; lH < lHeightCount; ++lH)
                    {
                        data()->write<eI16>(pWriter, pLoader);
                    }
                }
                data()->end(pWriter, pLoader, "Heights");

                lProgress->advance(lHeightCount);

                data()->begin(pWriter, pLoader, "Texels");
                {
                    for(long lT = 0; lT < lTexelCount; ++lT)
                    {
                        data()->write<eHex>(pWriter, pLoader);
                        lProgress->advance(lHeightCount + lT);
                    }
                }
                data()->end(pWriter, pLoader, "Texels");

                data()->begin(pWriter, pLoader, "Flags");
                {
                    for(long lF = 0; lF < lTileFlagCount; ++lF)
                    {
                        data()->write<eHex>(pWriter, pLoader);
                        lProgress->advance(lHeightCount + lTexelCount + lF);
                    }
                }
                data()->end(pWriter, pLoader, "Flags");

                lProgress->advance(100);
            }
        }*/

        return true;
    }

    /*!
     */
    void ImportExportWorldGroup::doMake(PAK::Saver& pSaver)
    {
        // Ext�rieur et monde souterrain.
        pSaver << pSaver.fileHdl();
        auto    lService    = data()->fileDB()->context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data");
        pSaver << data()->fileDB()->create(lService->newWorldFilename("root", WOR::eOutside));
        pSaver << data()->fileDB()->create(lService->newWorldFilename("root", WOR::eUnderground));
        pSaver << k0UL; // Et pas de monde int�rieur.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

        void    checkInServices(OSGi::Context* pContext) override
                {
                    pContext->services()->checkIn("fr.osg.ide.wor.data", std::make_shared<WorldDataService>(pContext));
                }
        void    checkOutServices(OSGi::Context* pContext) override
                {
                    pContext->services()->checkOut("fr.osg.ide.wor.data");
                }

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportWorldGroup>("grp",  lExtensionDir);
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("module.tool.brushes", [=]() { return CORE::UIService::Widget{new Brushes{pContext}}; });
                    lUIService->connect("fragments.categories", SIGNAL(contentChanged()), "module.tool.brushes", SLOT(updateEnvironmentBrushes()));
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("module.tool.brushes");
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("grp");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QWOR::Activator)
OSGI_END_REGISTER_ACTIVATORS()
