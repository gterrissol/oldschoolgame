/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QTILe.hh"

/*! @file IDE/Module/QTILe/QTILe.cc
    @brief D�finitions diverses du module QTIL.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 10 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QXmlStreamWriter>

#include "CORE/Activator.hh"
#include "CORE/ProgressService.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "ERRor/ErrMsg.hh"
#include "PAcKage/LoaderSaver.hh"
#include "Module/QWORld/WorldDataService.hh"

#include "Controller.hh"
#include "Editor.hh"

namespace QTIL
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidItem,           "Item invalide.",               "Invalid item.")
    REGISTER_ERR_MSG(kIncorrectFamilyName,   "Nom de famille incorrect.",    "Incorrect family name.")


//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de tuile.
        @version 0.25
     */
    class ImportExportTile : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportTile() = default;                             //!< Destructeur.

    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader); //!<
virtual void    doMake(PAK::Saver& pSaver) override;                       //!< 
    };


//------------------------------------------------------------------------------
//                                     Tuile
//------------------------------------------------------------------------------

    /*!
     */
    bool ImportExportTile::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        //exportPlace(pWriter, pLoader) ?
        data()->write<eRect>(pWriter, pLoader, "Zone");
        data()->write<eFileHdl>(pWriter, pLoader, "Outside");

        data()->begin(pWriter, pLoader, "HeightField");
        {
            data()->write<eI16>(pWriter, pLoader, "Reference");
            data()->begin(pWriter, pLoader, "Heights");
            {
                for(unsigned long lH = 0; lH < (TILE::HeightField::eLength * TILE::HeightField::eLength); ++lH)
                {
                    data()->write<eI16>(pWriter, pLoader);
                }
            }
            data()->end(pWriter, pLoader, "Heights");
        }
        data()->end(pWriter, pLoader, "HeightField");

        data()->begin(pWriter, pLoader, "Texture");
        {
            U16 lSize = k0UW;
            pLoader >> lSize;
            pLoader.rollback(U32(sizeof(lSize)));

            data()->write<eU16>(pWriter, pLoader, "Size");

            data()->begin(pWriter, pLoader, "Data");
            {
                for(unsigned long lT = 0; lT < (sizeof(TILE::TexelData) / sizeof(U16)) * lSize; ++lT)
                {
                    data()->write<eU16>(pWriter, pLoader);
                }
            }
            data()->end(pWriter, pLoader, "Data");
        }
        data()->end(pWriter, pLoader, "Texture");

        data()->begin(pWriter, pLoader, "GameObjects");
        {
            U32 lCount = k0UL;
            pLoader >> lCount;
            pLoader.rollback(U32(sizeof(lCount)));

            data()->write<eU32>(pWriter, pLoader, "Count");

            for(unsigned long lE = 0; lE < lCount; ++lE)
            {
                data()->write<eGameObject>(pWriter, pLoader);
            }
        }
        data()->end(pWriter, pLoader, "GameObjects");

        return true;
    }


    /*! Cr�e les donn�es d'un gestionnaire vide (pour la cr�ation d'un <b>pak</b> file).
     */
    void ImportExportTile::doMake(PAK::Saver& pSaver)
    {
        auto    lService    = data()->fileDB()->context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data");
        auto    lGroup      = lService->groupFilenameFromTile(data()->fileDB()->packFileName(pSaver.fileHdl()));

        pSaver << UTI::Rect{};
        pSaver << data()->fileDB()->packFileHdl(lGroup);    // Handle des donn�es g�n�rales.
        pSaver << k0W;  // Altitude de r�f�rence.
        for(unsigned long lH = 0; lH < (TILE::HeightField::eLength * TILE::HeightField::eLength); ++lH) pSaver << k0W;
        pSaver << k0UW; // Encodage de la texture (vide).
        pSaver << k0UW; // Objets.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportTile>("til",  lExtensionDir);

                    pContext->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->checkInTile("til");

                    auto    lGEService = pContext->services()->findByTypeAndName<QT3D::GameEntityService>("fr.osg.ide.module.3d.game_entity");
                    lGEService->checkIn<TILE::Tile>(TILE::Controller::make);
                    lGEService->checkIn<TILE::TileRsc>(TILE::Controller::make);

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->registerType(QStringLiteral("til"));

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("module.editor.tile", [=]() { return CORE::UIService::Widget{new Editor{pContext}}; });
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("module.editor.tile");

                    pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie")->checkOut("til");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QTIL::Activator)
OSGI_END_REGISTER_ACTIVATORS()
