/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTIL_EDITOR_HH
#define QTIL_EDITOR_HH

#include "QTILe.hh"

/*! @file IDE/Module/QTILe/Editor.hh
    @brief En-t�te de la classe QTIL::Editor.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Synchronize.hh"
#include "CORE/Translate.hh"
#include "EDITion/Observer.hh"
#include "Module/Editor.hh"
#include "TILE/TILE.hh"

namespace QTIL
{
//------------------------------------------------------------------------------
//                              Editeur de Tuiles
//------------------------------------------------------------------------------

    /*! @brief Editeur de tuile.
        @version 0.5

        Interface QT d'�dition de tuile.
     */
    class Editor : public Module::Editor, public EDIT::Observer, public CORE::Translate<Editor>, public CORE::Synchronized
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Editor(OSGi::Context* pContext);                            //!< Constructeur par d�faut.
virtual             ~Editor();                                                  //!< Destructeur.
        //@}
    private:
        //! @name Pertinence du sujet
        //@{
        bool        canManage(EDIT::Controller::Ptr pDatum) const override;     //!< Possibilit� de gestion d'un sujet.
        void        manage(EDIT::Controller::Ptr pDatum) override;              //!< Gestion effective d'un sujet.
        void        unmanage() override;                                        //!< Arr�t de la gestion du sujet.
        void        manageNone() override;                                      //!< Plus aucun sujet � g�rer.
        //@}
        //! @name Ecoute du sujet
        //@{
        void        listen(const EDIT::Answer* pAnswer) override;               //!< Ecoute effective de la cible.
        //@}
        //! @name Changement de langue
        //@{
        auto        buildTranslater() -> CORE::Translated::Updater override;    //!< Traduction de l'IHM.
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}

#endif  // De QTIL_EDITOR_HH
