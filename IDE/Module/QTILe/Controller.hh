/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTIL_CONTROLLER_HH
#define QTIL_CONTROLLER_HH

#include "QTILe.hh"

/*! @file IDE/Module/QTILe/Controller.hh
    @brief En-t�te de la classe QTIL::Controller.
    @author @ref Guillaume_Terrissol
    @date 24 Septembre 2003 - 6 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdint>

#include "Module/Renderer.hh"
#include "Module/QT3D/Controller.hh"
#include "Module/QT3D/GameEntityService.hh"
#include "Module/QWORld/Brush.hh"
#include "Module/QWORld/Data.hh"
#include "Module/QWORld/GenericBrush.hh"
#include "STL/Shared.hh"
#include "TILE/BaseTile.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//                                 Tuile Edit�e
//------------------------------------------------------------------------------

    /*! @brief Tuile �ditable.
        @version 0.5

        Dans l'�diteur, cette classe doit �tre instanci�e pour manipuler chaque tuile.
     */
    class Controller :
        public STL::enable_shared_from_this<Controller>,
        public QT3D::GenericController<Tile>,
        public QWOR::Brushable
    {
        BEFRIEND_STD_ALLOCATOR()
    public:
        //! @name Types de pointeur
        //@{
        using   ConstAppearancePtr  = std::weak_ptr<APP::Appearance const>;         //!< Pointeur (constant) sur apparence.
        using   AppearancePtr       = std::weak_ptr<APP::Appearance>;               //!< Pointeur sur apparence.
        using   GameObjectPtr       = Tile::GameObjectPtr;                          //!< Pointeur sur game object.
        using   ControllerPtr       = QT3D::GameEntityService::ControllerPtr;       //!< Pointeur sur contr�leur.
        using   EntityPtr           = QT3D::GameEntityService::EntityPtr;           //!< Pointeur sur entit�.
        //@}
        //! @name Constructeurs & destructeur
        //@{
 static Ptr                 make(EntityPtr pEntity, OSGi::Context* pContext);       //!< Cr�ation.
        void                define(const UTI::Rect& pArea, PAK::FileHdl pOut);      //!< 
        //@}
        QWOR::Data::Ptr     data();                                                 //!< Donn�es communes.
        //! @name Coordonn�es de la tuile
        //@{
        int16_t             longitude() const;                                      //!< Longitude.
        int16_t             latitude() const;                                       //!< Latitude.
        UTI::Rect           area() const;                                           //!< Zone de la tuile.
        Tile::AppearancePtr ground();
        //@}
        //! @name M�thodes d'�dition
        //@{
        void                rebuild();                                              //!< 
        void                get(HeightGroup& pHeights) const;                       //!< R�cup�ration des �l�vations.
        void                get(TexelGroup&  pTexels) const;                        //!< R�cup�ration des texels.
        //@}
    protected:
        //! @name Constructeurs & destructeur
        //@{
                            Controller(EditedPtr pTile, OSGi::Context* pContext);   //!< Constructeur.
virtual                     ~Controller();                                          //!< Destructeur.
        //@}
    private:
        //! @name M�thodes d'�dition
        //@{
        void                set(const HeightGroup& pHeights);                       //!< Edition des �l�vations.
        void                set(const TexelGroup&  pTexels);                        //!< Edition des texels.
        bool                insert(GameObjectPtr pObject);                          //!< Insertion d'un objet sur la tuile.
        bool                remove(GameObjectPtr pObject);                          //!< Retrait d'un objet de la tuile.
        //@}
        void                setAround(const HeightGroup& pHeights);                 //!< Mise � jour des �l�vations des tuiles voisines.
        void                getAround();                                            //!< El�vations des tuiles voisines.
        void                lock();                                                 //!< Verrouillage de la tuile.
        void                regenerateTexture();                                    //!< Recr�e la texture de la tuile.
        //! @name Impl�mentation de Brushable
        //@{
        UTI::Rect           getWorldExtent() const override;                        //!< Zone � �diter.
        UTI::Rect           getTextureExtent() const override;                      //!< Zone de texture � �diter.
        int                 getType() const override;                               //!< Identifiant.
        CliffBrushPtr       getCliffBrush() override;                               //!< Pinceau de falaise.
        SlideBrushPtr       getSlideBrush() override;                               //!< Pinceau "analogique".
        EnvironmentBrushPtr getEnvironmentBrush() override;                         //!< Pinceau d'environnement.
        DropBrushPtr        getDropBrush() override;                                //!< Pinceau de d�p�t d'objets.
        //@}
        EDIT::Subject*      asSubject() override;                                   //!< R�cup�ration du sujet d'�dition.
        QString             getExtension() const override;                          //!< Extension associ�e � l'objet contr�l�
        void                doTakeOver() override;                                  //!< Reprise en main du contr�leur.
        void                doSave() const override;                                //!< Sauvegarde de l'�tat de l'objet contr�l�.
        RendererPtr         getRenderer() override;                                 //!< R�cup�ration du rendrerer du sujet �dit�.

        FORBID_COPY(Controller)
        FREE_PIMPL()
    };


//------------------------------------------------------------------------------
//                          Pinceau de Texture de Tuile
//------------------------------------------------------------------------------

    /*! @brief "Pinceau" d'environnement pour tuiles.
        @version 0.9

        Ce pinceau permet l'�dition de textures de tuiles.
     */
    class TileEnvironmentBrush : public QWOR::GenericEnvironmentBrush<TexelGroup, Controller>
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                                TileEnvironmentBrush(TControllerPtr pEdited);               //!< Constructeur.
virtual                         ~TileEnvironmentBrush();                                    //!< Destructeur.
        //@}

    private:
        //! @name Traitement sur les donn�es (impl�mentation)
        //@{
        void                    applyToPiecesOfData(TWeightedGroup & pData) const override; //!< Edition d'un groupe de donn�es.
        bool                    applyToPieceOfData(typename TData::TType& pPieceOfData,
                                                   int32_t                pBlending) const; //!< Edition d'une donn�e �l�mentaire.
        const QWOR::Weights*    editionWeights() const override;                            //!< Poids pour l'�dition.
        //@]
    };
}

//------------------------------------------------------------------------------
//                                Sp�cialisation
//------------------------------------------------------------------------------

namespace EDIT
{
#if 0
#ifndef DoxygenShouldSkipThis

    template<>
    bool EditRequest<const QWOR::HeightGroup&>::mergeRequest(const EDIT::QRequest* pEvent);
    template<>
    bool EditRequest<const QWOR::TexelGroup&>::mergeRequest(const EDIT::QRequest* pEvent);

#endif  // De DoxygenShouldSkipThis
#endif  // 0
}

#endif  // De QTIL_CONTROLLER_HH
