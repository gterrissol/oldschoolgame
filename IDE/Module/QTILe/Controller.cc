/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/Module/QTILe/Controller.cc
    @brief M�thodes (non-inline) de la classe QTIL::Controller.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 15 Mai 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>

#include "CAMera/BaseCamera.hh"
#include "CoLoR/RGB.hh"
#include "EDITion/Attributes.hh"
#include "MATerial/All.hh"
#include "MATH/M4.hh"
#include "MATH/V3.hh"
#include "OUTside/World.hh"
#include "PAcKage/Enum.hh"
#include "Module/QWORld/GenericBrush.tcc"
#include "Module/QWORld/WorldDataService.hh"
#include "STL/Shared.tcc"
#include "UTIlity/Rect.hh"
#include "TILE/HeightField.tcc"
#include "TILE/TileGeometry.hh"

namespace EDIT
{
#ifndef NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Cette m�thode doit �tre sp�cialis�e pour permettre la fusion de deux requ�tes (pour grouper les
        requ�tes dans le syst�me de undo/redo).
        @param pEvent Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return Une requ�te obtenue en fusionnant *<b>this</b> et <i>pEvent</i>, lorsque cela est valide,
        0 sinon
        @note Par d�faut, les requ�tes ne peuvent pas fusionner (il faut sp�cialiser)
     */
    template<>
    bool EditRequest<const TILE::HeightGroup&>::mergeRequest(const EDIT::Request* pEvent)
    {
        const EditRequest<const TILE::HeightGroup&>*  lRequest = dynamic_cast<const EditRequest<const TILE::HeightGroup&>*>(pEvent);

        if (lRequest != nullptr)
        {
            const TILE::HeightGroup&   lNewGroup   = lRequest->mValue;
            if (lNewGroup.count() == 0)
            {
                return false;
            }
            TILE::HeightGroup&         lOldGroup   = mValue;

            for(U32 lN = k0UL; lN < lNewGroup.count(); ++lN)
            {
                const TILE::Height&    lNew        = lNewGroup[lN];
                bool                    lMatching   = false;

                for(U32 lO = k0UL; lO < lOldGroup.count(); ++lO)
                {
                    TILE::Height&  lOld    = lOldGroup[lO];

                    if ((lOld.col() == lNew.col()) && (lOld.row() == lNew.row()))
                    {
                        if (!mUndo)
                        {
                            lOld = lNew.data();
                        }
                        lMatching = true;
                        // Les choses �tant bien faites, d�s qu'il y a correspondance, on peut passer � la nouvelle �l�vation suivante.
                        break;
                    }
                }

                if (!lMatching)
                {
                    lOldGroup.insertInArea(lNew);
                }
            }

            return true;
        }

        return false;
    }

    /*! Cette m�thode doit �tre sp�cialis�e pour permettre la fusion de deux requ�tes (pour grouper les
        requ�tes dans le syst�me de undo/redo).
        @param pEvent Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return Une requ�te obtenue en fusionnant *<b>this</b> et <i>pEvent</i>, lorsque cela est valide, 0 sinon
        @note Par d�faut, les requ�tes ne peuvent pas fusionner (il faut sp�cialiser)
     */
    template<>
    bool EditRequest<const TILE::TexelGroup&>::mergeRequest(const EDIT::Request* pEvent)
    {
        const EditRequest<const TILE::TexelGroup&>*   lRequest = dynamic_cast<const EditRequest<const TILE::TexelGroup&>*>(pEvent);

        if (lRequest != nullptr)
        {
            const TILE::TexelGroup&    lNewGroup   = lRequest->mValue;
            if (lNewGroup.count() == 0)
            {
                return false;
            }
            TILE::TexelGroup&          lOldGroup   = mValue;

            for(U32 lN = k0UL; lN < lNewGroup.count(); ++lN)
            {
                const TILE::Texel&     lNew        = lNewGroup[lN];

                if (!TILE::FragmentMgr::get()->isTexelInactive(lNew.data()))
                {
                    bool    lMatching   = false;

                    for(U32 lO = k0UL; lO < lOldGroup.count(); ++lO)
                    {
                        TILE::Texel&   lOld    = lOldGroup[lO];

                        if ((lOld.col() == lNew.col()) && (lOld.row() == lNew.row()))
                        {
                            if (!mUndo)
                            {
                                lOld = lNew.data();
                            }
                            lMatching = true;
                            // Les choses �tant bien faites, d�s qu'il y a correspondance, on peut passer au nouveau texel suivant.
                            break;
                        }
                    }

                    if (!lMatching)
                    {
                        lOldGroup.insertInArea(lNew);
                    }
                }
            }

            return true;
        }

        return false;
    }

#endif  // De NOT_FOR_DOXYGEN
}


namespace QWOR
{
#ifndef NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                Pinceau G�n�rique : Traitement sur les Donn�es
//------------------------------------------------------------------------------

    /*  El�vation � une position donn�e (sp�cialisation pour un texel).
     */
    template<>
    MATH::V4 GenericBrush<EnvironmentBrush, TILE::TexelGroup, TILE::Controller>::vertexForModel(const TILE::Texel& pPiece, float pWeight) const
    {
        TILE::HeightGroup  lHeights;
        lHeights.insert(TILE::Height(pPiece.col(), pPiece.row(), k0W));
        mEdited.lock()->get(lHeights);
        const TILE::Height&    lH = lHeights[0];

        return MATH::V4(F32(pPiece.col()), F32(pPiece.row()), F32(lH.data()), F32(pWeight));
    }

#endif  // De NOT_FOR_DOXYGEN
}

namespace TILE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Pinceau de d�p�t d'objets sur une tuile.
        @version 0.2
     */
    class DropOnTileBrush : public QWOR::DropBrush
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef std::shared_ptr<TILE::Controller>  Controller;          //!< Pointeur sur contr�leur de tuile.
        //@}
        //! @name Constructeur & destructeur
        //@{
                DropOnTileBrush(Controller pController);                //!< Constructeur.
virtual         ~DropOnTileBrush();                                     //!< Destructeur.
        //@}
virtual void    insertModel(AppearancePtr pModel, float pX, float pY);  //!< Insertion d'un mod�le.

    private:

        Controller mController;
    };


    /*!
     */
    DropOnTileBrush::DropOnTileBrush(Controller pController)
        : mController(pController)
    { }


    /*!
     */
    DropOnTileBrush::~DropOnTileBrush() { }


    /*!
     */
    void DropOnTileBrush::insertModel(AppearancePtr pModel, float pX, float pY)
    {
        typedef std::shared_ptr<OBJ::GameObject>   GameObjectPtr;

        GameObjectPtr  lGO = STL::makeShared<OBJ::GameObject>();
        lGO->setAppearance(pModel);

        lGO->setPosition(MATH::V3(F32(pX), F32(pY), k0F));

        I16 lA = I16(rand() % 360);
        lGO->setOrientation(MATH::Q(MATH::V3(k0F, k0F, k1F), F32(lA * kDegToRad)));

        EDIT::InsertRequest<GameObjectPtr> lRequest(lGO);
        EDIT::Controller::Ptr              lEdited = mController;
        EDIT::SubjectList{EDIT::Controller::subject(lEdited)}.perform(&lRequest);
    }


//------------------------------------------------------------------------------
//                             P-Impl de Controller
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::Controller.
        @version 0.5
     */
    class Controller::Private :
        public EDIT::Subject,
        public EDIT::Value<const HeightGroup&>,
        public EDIT::Value<const TexelGroup&>,
        public EDIT::Container<GameObjectPtr>
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef std::shared_ptr<APP::Appearance>    AppearancePtr;          //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Private(Controller* pParent);                               //!< Constructeur.
                ~Private();                                                 //!< Destructeur.
        //@}
        //! @name Information g�n�rales
        //@{
        //@}
        std::shared_ptr<Controller> mParent;                                //!< Contr�leur propri�taire.
        QWOR::Data::Ptr             mWorldData;                             //!< Donn�es de monde.
        int16_t                     mNorth;
        int16_t                     mWest;

    private:

virtual void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo);    //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                Controller P-Impl : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Tuile �ditable dont cette instance est l'"impl�mentation priv�e"
     */
    Controller::Private::Private(Controller* pParent)
        : EDIT::Subject{"module"}
        , INITIALIZE_VALUE(set, get, const HeightGroup&)
        , INITIALIZE_VALUE(set, get, const TexelGroup&)
        , INITIALIZE_CONTAINER(insert, remove, GameObjectPtr)
        , mParent(pParent, STL::null_delete())
        , mWorldData()
        , mNorth(0)
        , mWest(0)
    { }


    /*! Destructeur.
     */
    Controller::Private::~Private()
    {
        UTI::Rect   lTileDataZone   = mWorldData->tileDataZone();
        int16_t     lWorldNorth     = lTileDataZone.roundedTop();
        int16_t     lWorldWest      = lTileDataZone.roundedLeft();
        int16_t     lWest           = (mWest / eEdgeSize) - lWorldWest;
        int16_t     lNorth          = lWorldNorth - (mNorth / eEdgeSize);

        if (mWorldData->isTileModified(lWest, lNorth))
        {
            mWorldData->lockTile(lWest, lNorth, true);
        }
    }


//------------------------------------------------------------------------------
//                         Controller P-Impl : 
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::Private::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                   Controller : Constructeurs & destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::Controller(EditedPtr pTile, OSGi::Context* pContext)
        : QT3D::GenericController<Tile>(pTile, pContext)
        , pthis(this)
    {
        pTile.lock()->activateSaveSelf(true);
        std::shared_ptr<OUT::World>    lOutside = std::dynamic_pointer_cast<OUT::World>(pTile.lock()->outside().lock());
        ASSERT(lOutside != nullptr, "")
        PAK::FileHdl   lDataHdl = lOutside->group();
        pthis->mWorldData  = context()->services()->findByTypeAndName<QWOR::WorldDataService>("fr.osg.ide.wor.data")->data(lDataHdl);

        pthis->mNorth      = worldExtent().roundedTop();
        pthis->mWest       = worldExtent().roundedLeft();

        // NB: les �l�vations "cach�es" (i.e. correspondant aux bordures "int�rieures" des voisines) sont
        // mises � jour d�s modifications. Mais, au cas o� les voisines n'auraient pas �t� charg�es lors
        // de la modification, ou en l'absence de modification (tuiles construites, et c'est tout, une
        // mise � jour forc�e intervient, lors de l'intervention du contr�leur. Un peu lourd, comme
        // souvent, mais avec les donn�es stream�es...
        getAround();

        regenerateTexture();
    }


    /*! Destructeur.
     */
    Controller::~Controller() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Controller::ControllerPtr Controller::make(EntityPtr pEntity, OSGi::Context* pContext)
    {
        return std::make_shared<Controller>(std::dynamic_pointer_cast<Tile>(pEntity.lock()), pContext);
    }


    /*!
     */
    void Controller::define(const UTI::Rect& pArea, PAK::FileHdl pOut)
    {
        edited().lock()->build(pArea, pOut);
    }


    /*!
     */
    QWOR::Data::Ptr Controller::data()
    {
        return pthis->mWorldData;
    }


//------------------------------------------------------------------------------
//                            Coordonn�es de la Tuile
//------------------------------------------------------------------------------

    /*! @return Longitude de la tuile (unit� = 1 tuile); 0 est � l'ouest
        @note Pour la longitude en coordon�es monde, il suffit d'utiliser ro_Area()
     */
    int16_t Controller::longitude() const
    {
        return pthis->mWest;
    }


    /*! @return Latitude de la tuile (unit� = 1 tuile); 0 est au nord
        @note Pour la longitude en coordon�es monde, il suffit d'utiliser ro_Area()
     */
    int16_t Controller::latitude() const
    {
        return pthis->mNorth;
    }


    /*!
     */
    UTI::Rect Controller::area() const
    {
        return edited().lock()->area();
    }


    /*! @todo A revoir ?
     */
    Tile::AppearancePtr Controller::ground()
    {
        Tile::AppearancePtr lGround;
        MATH::M4            lFrame;
        edited().lock()->firstApp(lGround, lFrame);

        return lGround;
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*! R�cup�re [toutes] les �l�vations de la tuile.
        @param pHeights Cette instance sera remplie avec les �l�vations du height field de la tuile
     */
    void Controller::get(HeightGroup& pHeights) const
    {
        int16_t lNorth  = pthis->mNorth + 1;
        int16_t lWest   = pthis->mWest  - 1;

        Tile::ConstHeightFieldPtr::element_type&  lHeights = *edited().lock()->heights();

        for(U32 lEl = k0UL; lEl < pHeights.count(); ++lEl)
        {
            Height&        lHeight = pHeights[lEl];
            uint16_t  lLi = lNorth - lHeight.row();
            uint16_t  lCo = lHeight.col() - lWest;
            if ((lLi < HeightField::eLength) && (lCo < HeightField::eLength))
            {
                lHeight = lHeights[lLi][lCo];
            }
        }
    }


    /*! R�cup�re [tous] les texels de la tuile.
        @param pTexels Cette instance sera remplie avec les texels de la tuile
     */
    void Controller::get(TexelGroup& pTexels) const
    {
        QWOR::Data*     lData           = pthis->mWorldData.get();
        UTI::Rect       lTileDataZone   = lData->tileDataZone();
        unsigned long   lTexelCount     = lData->texelCount();
        int16_t         lNorth          = lTileDataZone.roundedTop()   * eFragmentCount;
        int16_t         lWest           = lTileDataZone.roundedLeft()  * eFragmentCount;
        int16_t         lWidth          = lTileDataZone.roundedWidth() * eFragmentCount;

        for(U32 lEl = k0UL; lEl < pTexels.count(); ++lEl)
        {
            Texel&         lTexel  = pTexels[lEl];
            unsigned long   lOffset = (lNorth - lTexel.row()) * lWidth + (lTexel.col() - lWest);
            if (lOffset < lTexelCount)
            {
                lTexel = lData->texel(lOffset);
            }
        }
    }


    /*! @param pNewHeights Nouvelles �l�vations
     */
    void Controller::set(const HeightGroup& pNewHeights)
    {
        Tile::HeightFieldPtr::element_type&   lHeights    = *edited().lock()->heights();

        QWOR::Data* lData           = pthis->mWorldData.get();
        UTI::Rect   lTileDataZone   = lData->tileDataZone();
        int16_t     lWorldNorth     = lTileDataZone.roundedTop();
        int16_t     lWorldWest      = lTileDataZone.roundedLeft();
        int32_t     lWorldWidth     = lTileDataZone.roundedWidth() + 1;
        int16_t     lNorth          = pthis->mNorth + 1;
        int16_t     lWest           = pthis->mWest  - 1;

        for(U32 lEl = k0UL; lEl < pNewHeights.count(); ++lEl)
        {
            const Height&   lHeight = pNewHeights[lEl];
            int16_t         lRow    = lHeight.row();
            int16_t         lCol    = lHeight.col();
            uint16_t        lLi     = lNorth - lRow;
            uint16_t        lCo     = lCol - lWest;
            if ((lLi < HeightField::eLength) && (lCo < HeightField::eLength))
            {
                // lHeights est un tableau de (1 + eEdgeSize + 1) lignes sur (1 + (eEdgeSize + 1)+ 1) colonnes.
                lHeights[lLi][lCo] = lHeight.data();

                if ((lRow % eEdgeSize == 0) && (lCol % eEdgeSize == 0))
                {
                    unsigned long   lOffset = (lWorldNorth - lRow / eEdgeSize) * lWorldWidth + (lCol / eEdgeSize - lWorldWest);
                    lData->setElevation(lOffset, lHeight.data());
                }
            }
        }

        setAround(pNewHeights);

        // Et demande la mise � jour de la g�ometrie.
        edited().lock()->ground()->update(edited().lock()->heights());

        lData->modifyTile((pthis->mWest / eEdgeSize) - lWorldWest, lWorldNorth - (pthis->mNorth / eEdgeSize), true);
    }


    /*! Modifie les texels.
        @param pNewTexels Nouveux texels
     */
    void Controller::set(const TexelGroup& pNewTexels)
    {
        if (pNewTexels.count() == 0)
        {
            return;
        }

        // Position.
        QWOR::Data*     lData           = pthis->mWorldData.get();
        UTI::Rect       lTileDataZone   = lData->tileDataZone();
        unsigned long   lTexelCount     = lData->texelCount();
        int16_t         lWorldNorth     = lTileDataZone.roundedTop();
        int16_t         lWorldWest      = lTileDataZone.roundedLeft();
        int16_t         lNorth          = lWorldNorth                  * eFragmentCount;
        int16_t         lWest           = lWorldWest                   * eFragmentCount;
        int16_t         lWidth          = lTileDataZone.roundedWidth() * eFragmentCount;

        for(U32 lEl = k0UL; lEl < pNewTexels.count(); ++lEl)
        {
            const Texel&    lTexel      = pNewTexels[lEl];
            TILE::TexelData lTexelData  = lTexel.data();

            unsigned long   lOffset = (lNorth - lTexel.row()) * lWidth + (lTexel.col() - lWest);

            if (lOffset < lTexelCount)
            {
                lData->setTexel(lOffset, lTexelData);
            }
        }

        regenerateTexture();

        lData->modifyTile((pthis->mWest / eEdgeSize) - lWorldWest, lWorldNorth - (pthis->mNorth / eEdgeSize), true);
    }


    /*!
     */
    bool Controller::insert(GameObjectPtr pObject)
    {
        edited().lock()->addElement(pObject);

        return true;
    }


    /*!
     */
    bool Controller::remove(GameObjectPtr pObject)
    {
        edited().lock()->removeElement(pObject);

        return false;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::setAround(const HeightGroup& pHeights)
    {
        QWOR::Data*         lData   = pthis->mWorldData.get();
        int16_t             lN      = pthis->mNorth - eEdgeSize / 2;
        int16_t             lW      = pthis->mWest  + eEdgeSize / 2;

        QWOR::Data::TilePtr lNeighbours[eNeighbourCount] =
        {
            lData->tile(lW - eEdgeSize, lN + eEdgeSize),    // eNorthWest.
            lData->tile(lW,             lN + eEdgeSize),    // eNorth.
            lData->tile(lW + eEdgeSize, lN + eEdgeSize),    // eNorthEast.
            lData->tile(lW + eEdgeSize, lN),                // eEast.
            lData->tile(lW + eEdgeSize, lN - eEdgeSize),    // eSouthEast.
            lData->tile(lW,             lN - eEdgeSize),    // eSouth.
            lData->tile(lW - eEdgeSize, lN - eEdgeSize),    // eSouthWest.
            lData->tile(lW - eEdgeSize, lN)                 // eWest.
        };
        std::vector<HeightGroup>    lHeightsOnNeighbours(eNeighbourCount);
        UTI::Rect                   lTileDataZone   = lData->tileDataZone();
        int16_t                     lWorldNorth     = lTileDataZone.roundedTop();
        int16_t                     lWorldWest      = lTileDataZone.roundedLeft();
        signed long                 lNorth          = pthis->mNorth;
        signed long                 lSouth          = lNorth - eEdgeSize;
        signed long                 lWest           = pthis->mWest;
        signed long                 lEast           = lWest + eEdgeSize;

        for(U32  lEl = k0UL; lEl < pHeights.count(); ++lEl)
        {
            const Height&  lHeight = pHeights[lEl];

            if      ((lHeight.row() == lNorth) || (lHeight.row() == lNorth - 1))
            {
                lHeightsOnNeighbours[eNorth].insert(lHeight);

                if      ((lHeight.col() == lWest) || (lHeight.col() == lWest + 1))
                {
                    lHeightsOnNeighbours[eNorthWest].insert(lHeight);
                }
                else if ((lHeight.col() == lEast) || (lHeight.col() == lEast - 1))
                {
                    lHeightsOnNeighbours[eNorthEast].insert(lHeight);
                }
            }
            else if ((lHeight.row() == lSouth) || (lHeight.row() == lSouth + 1))
            {
                lHeightsOnNeighbours[eSouth].insert(lHeight);

                if      ((lHeight.col() == lWest) || (lHeight.col() == lWest + 1))
                {
                    lHeightsOnNeighbours[eSouthWest].insert(lHeight);
                }
                else if ((lHeight.col() == lEast) || (lHeight.col() == lEast - 1))
                {
                    lHeightsOnNeighbours[eSouthEast].insert(lHeight);
                }
            }
            if ((lHeight.col() == lWest) || (lHeight.col() == lWest + 1))
            {
                lHeightsOnNeighbours[eWest].insert(lHeight);
            }
            else if ((lHeight.col() == lEast) || (lHeight.col() == lEast - 1))
            {
                lHeightsOnNeighbours[eEast].insert(lHeight);
            }
        }

        // Pour chaque tuile voisine...
        for(unsigned long lN = k0UL; lN < lHeightsOnNeighbours.size(); ++lN)
        {
            const HeightGroup& lNeighbourHeights   = lHeightsOnNeighbours[lN];

            if (lNeighbourHeights.count() != 0)
            {
                Tile*  lNeighbour  = lNeighbours[lN].get();

                if (lNeighbour != nullptr)
                {
                    signed long     lNeighbourNorth         = lNeighbour->area().roundedTop()  + 1;
                    signed long     lNeighbourWest          = lNeighbour->area().roundedLeft() - 1;
                    HeightField*   lNeighbourHeightField   = lNeighbour->heights().get();

                    // Modifie les �l�vations.
                    for(U32 lEl = k0UL; lEl < lNeighbourHeights.count(); ++lEl)
                    {
                        const Height&  lNeighbourHeight = lNeighbourHeights[lEl];
                        // + 1 + 1 => *po_Elevations() est un tableau de (1 + eEdgeSize + 1) lignes sur  (1 + eEdgeSize + 1) colonnes.
                        size_t  lLi = lNeighbourNorth - lNeighbourHeight.row();
                        size_t  lCo = lNeighbourHeight.col() - lNeighbourWest;
                        if ((lLi < HeightField::eLength) && (lCo < HeightField::eLength))
                        {
                            (*lNeighbourHeightField)[lLi]
                                                    [lCo] = lNeighbourHeight.data();
                        }
                    }

                    // Et demande la mise � jour de la g�ometrie.
                    lNeighbour->ground()->update(lNeighbour->heights());

                    lData->modifyTile((lNeighbourWest / eEdgeSize) - lWorldWest, lWorldNorth - (lNeighbourNorth / eEdgeSize), true);
                }
            }
        }
    }


    /*! R�cup�re les �l�vations des bordures des tuiles voisines.
     */
    void Controller::getAround()
    {
        QWOR::Data*            lData   = pthis->mWorldData.get();
        int16_t            lN      = pthis->mNorth - eEdgeSize / 2;
        int16_t            lW      = pthis->mWest  + eEdgeSize / 2;

        QWOR::Data::TilePtr   lNeighbours[eNeighbourCount] =
        {
            lData->tile(lW - eEdgeSize, lN + eEdgeSize),    // eNorthWest.
            lData->tile(lW,             lN + eEdgeSize),    // eNorth.
            lData->tile(lW + eEdgeSize, lN + eEdgeSize),    // eNorthEast.
            lData->tile(lW + eEdgeSize, lN),                // eEast.
            lData->tile(lW + eEdgeSize, lN - eEdgeSize),    // eSouthEast.
            lData->tile(lW,             lN - eEdgeSize),    // eSouth.
            lData->tile(lW - eEdgeSize, lN - eEdgeSize),    // eSouthWest.
            lData->tile(lW - eEdgeSize, lN)                 // eWest.
        };
        HeightField*           lNeighbourHeightField[eNeighbourCount] =
        {
            (lNeighbours[eNorthWest] != nullptr) ? lNeighbours[eNorthWest]->heights().get() : nullptr,
            (lNeighbours[eNorth    ] != nullptr) ? lNeighbours[eNorth    ]->heights().get() : nullptr,
            (lNeighbours[eNorthEast] != nullptr) ? lNeighbours[eNorthEast]->heights().get() : nullptr,
            (lNeighbours[eEast     ] != nullptr) ? lNeighbours[eEast     ]->heights().get() : nullptr,
            (lNeighbours[eSouthEast] != nullptr) ? lNeighbours[eSouthEast]->heights().get() : nullptr,
            (lNeighbours[eSouth    ] != nullptr) ? lNeighbours[eSouth    ]->heights().get() : nullptr,
            (lNeighbours[eSouthWest] != nullptr) ? lNeighbours[eSouthWest]->heights().get() : nullptr,
            (lNeighbours[eWest     ] != nullptr) ? lNeighbours[eWest     ]->heights().get() : nullptr
        };
        HeightField&    lTileHeights    = *edited().lock()->heights().get();
        UTI::Rect       lTileDataZone   = lData->tileDataZone();
        int16_t         lWorldNorth     = lTileDataZone.roundedTop()    * eEdgeSize;
        int16_t         lWorldWest      = lTileDataZone.roundedLeft()   * eEdgeSize;
        int16_t         lWorldSouth     = lTileDataZone.roundedBottom() * eEdgeSize;
        int16_t         lWorldEast      = lTileDataZone.roundedRight()  * eEdgeSize;
        int32_t         lNorth          = pthis->mNorth;
        int32_t         lSouth          = lNorth - eEdgeSize;
        int32_t         lWest           = pthis->mWest;
        int32_t         lEast           = lWest + eEdgeSize;
        bool            lIsAtTop        = lNorth == lWorldNorth;
        bool            lIsAtLeft       = lWest  == lWorldWest;
        bool            lIsAtBottom     = lSouth == lWorldSouth;
        bool            lIsAtRight      = lEast  == lWorldEast;

        // North West.
        if      (HeightField* lHeightField = lNeighbourHeightField[eNorthWest])
        {
            lTileHeights    [0]                [0]                  = (*lHeightField)   [1 + eEdgeSize]    [1 + eEdgeSize];
        }
        else if (lIsAtTop && lIsAtLeft)
        {
            lTileHeights    [0]                [0]                  = lTileHeights      [1]                [1];
        }
        // North.
        if      (HeightField* lHeightField = lNeighbourHeightField[eNorth])
        {
            for(size_t lCol = 0; lCol <= eEdgeSize; ++lCol)
            {
                lTileHeights[0]                [1 + lCol]           = (*lHeightField)   [1 + eEdgeSize - 1][1 + lCol];
                lTileHeights[1]                [1 + lCol]           = (*lHeightField)   [1 + eEdgeSize    ][1 + lCol];
                lTileHeights[2]                [1 + lCol]           = (*lHeightField)   [1 + eEdgeSize + 1][1 + lCol];
            }
        }
        else if (lIsAtTop)
        {
            for(size_t lCol = 0; lCol <= eEdgeSize; ++lCol)
            {
                lTileHeights[0]                [1 + lCol]           = lTileHeights      [1]                [1 + lCol];
            }
        }
        // North East.
        if      (HeightField* lHeightField = lNeighbourHeightField[eNorthEast])
        {
            lTileHeights    [0]                [1 + eEdgeSize + 1]  = (*lHeightField)   [1 + eEdgeSize]    [1];
        }
        else if (lIsAtTop && lIsAtRight)
        {
            lTileHeights    [0]                [1 + eEdgeSize + 1]  = lTileHeights      [1]                [1 + eEdgeSize];
        }
        // East.
        if      (HeightField* lHeightField = lNeighbourHeightField[eEast])
        {
            for(size_t lRow = 0; lRow <= eEdgeSize; ++lRow)
            {
                lTileHeights[1 + lRow]         [1 + eEdgeSize - 1]  = (*lHeightField)   [1 + lRow]         [0];
                lTileHeights[1 + lRow]         [1 + eEdgeSize    ]  = (*lHeightField)   [1 + lRow]         [1];
                lTileHeights[1 + lRow]         [1 + eEdgeSize + 1]  = (*lHeightField)   [1 + lRow]         [2];
            }
        }
        else if (lIsAtRight)
        {
            for(size_t lRow = 0; lRow <= eEdgeSize; ++lRow)
            {
                lTileHeights[1 + lRow]         [1 + eEdgeSize + 1]  = lTileHeights      [1 + lRow]         [1 + eEdgeSize];
            }
        }
        // South East.
        if      (HeightField* lHeightField = lNeighbourHeightField[eSouthEast])
        {
            lTileHeights    [1 + eEdgeSize + 1][1 + eEdgeSize + 1]  = (*lHeightField)   [1]                [1];
        }
        else if (lIsAtBottom && lIsAtRight)
        {
            lTileHeights    [1 + eEdgeSize + 1][1 + eEdgeSize + 1]  = lTileHeights      [1 + eEdgeSize]    [1 + eEdgeSize];
        }
        // South.
        if (HeightField* lHeightField = lNeighbourHeightField[eSouth])
        {
            for(size_t lCol = 0; lCol <= eEdgeSize; ++lCol)
            {
                lTileHeights[1 + eEdgeSize - 1][1 + lCol]           = (*lHeightField)   [0]                [1 + lCol];
                lTileHeights[1 + eEdgeSize    ][1 + lCol]           = (*lHeightField)   [1]                [1 + lCol];
                lTileHeights[1 + eEdgeSize + 1][1 + lCol]           = (*lHeightField)   [2]                [1 + lCol];
            }
        }
        else if (lIsAtBottom)
        {
            for(size_t lCol = 0; lCol <= eEdgeSize; ++lCol)
            {
                lTileHeights[1 + eEdgeSize + 1][1 + lCol]           = lTileHeights      [1 + eEdgeSize]    [1 + lCol];
            }
        }
        // South West.
        if      (HeightField* lHeightField = lNeighbourHeightField[eSouthWest])
        {
            lTileHeights    [1 + eEdgeSize + 1][0]                  = (*lHeightField)   [1]                [1 + eEdgeSize];
        }
        else if (lIsAtBottom && lIsAtLeft)
        {
            lTileHeights    [1 + eEdgeSize + 1][0]                  = lTileHeights      [1 + eEdgeSize]    [1];
        }
        // West.
        if      (HeightField* lHeightField = lNeighbourHeightField[eWest])
        {
            for(size_t lRow = 0; lRow <= eEdgeSize; ++lRow)
            {
                lTileHeights[1 + lRow]         [0]                  = (*lHeightField)   [1 + lRow]         [1 + eEdgeSize - 1];
                lTileHeights[1 + lRow]         [1]                  = (*lHeightField)   [1 + lRow]         [1 + eEdgeSize    ];
                lTileHeights[1 + lRow]         [2]                  = (*lHeightField)   [1 + lRow]         [1 + eEdgeSize + 1];
            }
        }
        else if (lIsAtLeft)
        {
            for(size_t lRow = 0; lRow <= eEdgeSize; ++lRow)
            {
                lTileHeights[1 + lRow]         [0]                  = lTileHeights      [1 + lRow]         [1];
            }
        }
    }


    /*! Reg�n�re la texture de la tuile (et la configuration qui va avec).
     */
    void Controller::regenerateTexture()
    {
        QWOR::Data* lData           = pthis->mWorldData.get();
        UTI::Rect   lTileDataZone   = lData->tileDataZone();
        int32_t     lNorth          = lTileDataZone.roundedTop()   * eFragmentCount;
        int32_t     lWest           = lTileDataZone.roundedLeft()  * eFragmentCount;
        int32_t     lWidth          = static_cast<int32_t>(lTileDataZone.roundedWidth() * eFragmentCount);
        int32_t     lHeight         = static_cast<int32_t>(lTileDataZone.roundedWidth() * eFragmentCount);
        UTI::Rect   lTextureExtent  = textureExtent();
        int16_t     lTexelY         = static_cast<int16_t>(lNorth - lTextureExtent.roundedTop());
        int16_t     lTexelX         = static_cast<int16_t>(lTextureExtent.roundedLeft() - lWest);
        TexelGroup  lTexels;

        // [ -1 ; 32 + 1 ]
        for(int16_t lLi = -1; lLi <= eFragmentCount + 1; ++lLi)
        {
            Texel::TOffset lTxLi = Texel::TOffset(lTexelY + lLi);
            for(int16_t lCo = -1; lCo <= eFragmentCount + 1; ++lCo)
            {
                Texel::TOffset lTxCo = Texel::TOffset(lTexelX + lCo);
                if      (lTxLi < 0)        lTxLi = Texel::TOffset(0);
                else if (lHeight <= lTxLi) lTxLi = Texel::TOffset(lHeight - 1);
                if      (lTxCo < 0)        lTxCo = Texel::TOffset(0);
                else if (lWidth <= lTxCo)  lTxCo = Texel::TOffset(lWidth - 1);
                int32_t lOffset = lTxLi * static_cast<int32_t>(lWidth) + lTxCo;
                lTexels.insert(Texel(lTxCo, lTxLi, lData->texel(lOffset)));
            }
        }

        edited().lock()->setTexture(lTexels);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    UTI::Rect Controller::getWorldExtent() const
    {
        UTI::Rect lExtent = area();
        lExtent.setBottomRight(UTI::Pointf(lExtent.right(), lExtent.bottom()));
        return lExtent;
    }


    /*!
     */
    UTI::Rect Controller::getTextureExtent() const
    {
        return area();
    }


    /*!
     */
    int Controller::getType() const
    {
        return rTTI();
    }


    /*!
     */
    Controller::CliffBrushPtr Controller::getCliffBrush()
    {
        CliffBrushPtr  lBrush  = std::make_shared<QWOR::GenericCliffBrush<HeightGroup, Controller>>(pthis->mParent);

        const float kOneScaledMeter = k1F / kVerticalStep;

        lBrush->setLevelHeight(static_cast<Height::TType>(kOneScaledMeter));
        lBrush->setMaximumAmplitude(static_cast<Height::TType>(4 * kOneScaledMeter));
        lBrush->setExtrema(static_cast<Height::TType>(-55.0F  * kOneScaledMeter),
                           static_cast<Height::TType>(200.0F  * kOneScaledMeter));
        lBrush->setPlaneScale(k1F);

        return lBrush;
    }


    /*!
     */
    Controller::SlideBrushPtr Controller::getSlideBrush()
    {
        SlideBrushPtr  lBrush  = std::make_shared<QWOR::GenericSlideBrush<HeightGroup, Controller>>(pthis->mParent);

        const float kOneScaledMeter = k1F / kVerticalStep;

        lBrush->setExtrema(static_cast<Height::TType>(-55.0F * kOneScaledMeter),
                           static_cast<Height::TType>(160.0F * kOneScaledMeter));
        lBrush->setPlaneScale(k1F);

        return lBrush;
    }


    /*!
     */
    Controller::EnvironmentBrushPtr Controller::getEnvironmentBrush()
    {
        EnvironmentBrushPtr    lBrush  = std::make_shared<TileEnvironmentBrush>(pthis->mParent);

        lBrush->setPlaneScale(k1F);

        return lBrush;
    }


    /*! @return Un pinceau de d�p�t de mod�le nouvellement cr��
     */
    Controller::DropBrushPtr Controller::getDropBrush()
    {
        DropBrushPtr   lBrush = std::make_shared<DropOnTileBrush>(pthis->mParent);

        lBrush->setPlaneScale(k1F);

        return lBrush;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    EDIT::Subject* Controller::asSubject()
    {
        return &(*pthis);
    }


    /*!
     */
    QString Controller::getExtension() const
    {
        return "til";
    }


    /*!
     */
    void Controller::doTakeOver()
    {
        getAround();
    }


    /*!
     */
    void Controller::doSave() const
    {
        edited().lock()->saveSelf();
    }


    /*!
     */
    Controller::RendererPtr Controller::getRenderer()
    {
        return Controller::RendererPtr();
    }


//------------------------------------------------------------------------------
//                      Poids des Masques pour les Pinceaux
//------------------------------------------------------------------------------


    /*! L'application de pinceaux sur les texels de tuile se fait avec ces masques. Les valeurs permettent d'appliquer,
        ou non, sur chaque quartier de texel. x : applique; . : laisse tel quel.
        0.0  0.05  0.10  0.15  0.20  0.25  0.30  0.35  0.40  0.45  0.50  0.55  0.60  1.0
        ..   x.    .x    ..    ..    xx    ..    x.    .x    xx    xx    .x    x.    xx
        ..   ..    ..    x.    .x    ..    xx    x.    .x    x.    .x    xx    xx    xx
     */
    QWOR::Weights gEditTexelWeights =
    {
        {   // 1 x 1
            { 1.0F }
        },
        {   // 2 x 2
            { 0.200F, 0.150F },
            { 0.100F, 0.050F }
        },
        {   // 3 x 3
            { 0.200F, 0.300F, 0.150F },
            { 0.400F, 1.0F,   0.350F },
            { 0.100F, 0.250F, 0.050F }
        },
        {   // 4 x 4
            { 0.000F, 0.000F, 0.000F, 0.000F },
            { 0.000F, 0.000F, 0.000F, 0.000F },
            { 0.000F, 0.000F, 0.000F, 0.000F },
            { 0.000F, 0.000F, 0.000F, 0.000F }
        },
        {   // 5 x 5
            { 0.000F, 0.200F, 0.300F, 0.150F, 0.000F },
            { 0.200F, 0.550F, 1.000F, 0.600F, 0.150F },
            { 0.400F, 1.000F, 1.0F,   1.000F, 0.350F },
            { 0.100F, 0.500F, 1.000F, 0.450F, 0.050F },
            { 0.000F, 0.100F, 0.250F, 0.050F, 0.000F }
        },
        {   // 6 x 6
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F }
        },
        {   // 7 x 7
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F }
        },
        {   // 8 x 8
            { 0.000F, 0.000F, 0.200F, 0.300F, 0.300F, 0.150F, 0.000F, 0.000F },
            { 0.000F, 0.200F, 0.550F, 1.000F, 1.000F, 0.600F, 0.150F, 0.000F },
            { 0.200F, 0.550F, 1.0F,   1.0F,   1.0F,   1.0F,   0.600F, 0.150F },
            { 0.400F, 1.000F, 1.0F,   1.0F,   1.0F,   1.0F,   1.000F, 0.350F },
            { 0.400F, 1.000F, 1.0F,   1.0F,   1.0F,   1.0F,   1.000F, 0.350F },
            { 0.100F, 0.500F, 1.0F,   1.0F,   1.0F,   1.0F,   0.450F, 0.050F },
            { 0.000F, 0.100F, 0.500F, 1.001F, 1.000F, 0.450F, 0.050F, 0.000F },
            { 0.000F, 0.0000, 0.100F, 0.250F, 0.250F, 0.050F, 0.000F, 0.000F }
        },
        {   // 9 x 9
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F }
        },
        {   // 10 x 10
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F },
            { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F }
        }
    };


//------------------------------------------------------------------------------
//           Pinceau de Texture de Tuile : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited Objet (non-nul) � 'peindre' (<i>TController</i> est pr�vu pour �tre QWOR::TileSubject ou QWOR::OutsideSubject)
     */
    TileEnvironmentBrush::TileEnvironmentBrush(TControllerPtr pEdited)
        : GenericEnvironmentBrush<TexelGroup, Controller>(pEdited)
    { }


    /*! Destructeur.
     */
    TileEnvironmentBrush::~TileEnvironmentBrush() { }


//------------------------------------------------------------------------------
//   Pinceau de Texture de Tuile : Traitement sur les Donn�es (Impl�mentation)
//------------------------------------------------------------------------------

    /*! Edition de donn�es.
        @param pData Donn�e pond�r�es � modifier (les valeurs sont r�cup�r�es en sortie).
     */
    void TileEnvironmentBrush::applyToPiecesOfData(TWeightedGroup & pData) const
    {
        auto& [ lNewGroup, lWeights ] = pData;
        for(unsigned long lE = 0; lE < lNewGroup.count(); ++lE)
        {
            auto & lPieceOfData = lNewGroup[lE];
            float  lWeight      = (this->blending() == TILE::eFull ? k1F : lWeights[lE]);

            typename TData::TType   lData = lPieceOfData.data();
            if (this->blending() != TILE::eEmpty)
            {
                if (applyToPieceOfData(lData, static_cast<int32_t>(20.0F * lWeight + 0.1F)))    // + 0.1 : pour les probl�mes de pr�cision.
                {
                    lPieceOfData = lData;
                }
            }
            else
            {
                if (lWeight == k1F)
                {
                    lData.enabled = false;
                    lPieceOfData = lData;
                }
            }
        }
    }


    /*! Tente d'appliquer le pinceau d'environnement sur un texel.
        @retval true  Le pinceau a �t� appliqu� sur le texel.
        @retval false Le texel est laiss� inchang�.
     */
    bool TileEnvironmentBrush::applyToPieceOfData(typename TData::TType& pPieceOfData, int32_t pBlending) const
    {
        auto lTilingId = this->tilingId();
        switch(pBlending)
        {
        case 0:
            break;
        case 1:
            pPieceOfData.tilingId0 = lTilingId;
            break;
        case 2:
            pPieceOfData.tilingId1 = lTilingId;
            break;
        case 3:
            pPieceOfData.tilingId2 = lTilingId;
            break;
        case 4:
            pPieceOfData.tilingId3 = lTilingId;
            break;
        case 5:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId1 = lTilingId;
            break;
        case 6:
            pPieceOfData.tilingId2 = pPieceOfData.tilingId3 = lTilingId;
            break;
        case 7:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId2 = lTilingId;
            break;
        case 8:
            pPieceOfData.tilingId1 = pPieceOfData.tilingId3 = lTilingId;
            break;
        case 9:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId1 = pPieceOfData.tilingId2 = lTilingId;
            break;
        case 10:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId1 = pPieceOfData.tilingId3 = lTilingId;
            break;
        case 11:
            pPieceOfData.tilingId1 = pPieceOfData.tilingId2 = pPieceOfData.tilingId3 = lTilingId;
            break;
        case 12:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId2 = pPieceOfData.tilingId3 = lTilingId;
            break;
        case 20:
            pPieceOfData.tilingId0 = pPieceOfData.tilingId1 = pPieceOfData.tilingId2 = pPieceOfData.tilingId3 = lTilingId;
            break;
        default:
            return false;
        }

        pPieceOfData.defined = 1;
        pPieceOfData.enabled = true;
        pPieceOfData.blending = this->blending();

        return true;
    }


    /*! Les pinceaux d'environnement utilisent les poids diff�remment (encodage du m�lange).
        @return Poids pour l'�dition.
     */
    const QWOR::Weights* TileEnvironmentBrush::editionWeights() const
    {
        return &gEditTexelWeights;
    }
}
