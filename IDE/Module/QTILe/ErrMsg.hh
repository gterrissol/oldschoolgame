/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTIL_ERRMSG_HH
#define QTIL_ERRMSG_HH

/*! @file IDE/Module/QTILe/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module QTIL.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QTIL
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kIncorrectFamilyName;
    extern  ERR::String kInvalidItem;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De QTIL_ERRMSG_HH
