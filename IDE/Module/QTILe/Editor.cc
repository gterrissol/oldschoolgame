/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/Module/QTILe/Editor.cc
    @brief M�thodes (non-inline) des classes QTIL::Editor::EditorPrivate & QTIL::Editor.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <deque>

#include "Module/QWORld/Enum.hh"

#include "Controller.hh"
#include "Editor.ui.hh"

namespace QTIL
{
    enum
    {
        eMaxCachedControllers  = QWOR::eMaxLoadedTiles
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QTIL::Editor.
        @version 0.2
     */
    class Editor::EditorPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
        //! @name Constructeurs
        //@{
                EditorPrivate(Editor* pParent, OSGi::Context* pContext);    //!< Constructeur.
        void    init();                                                     //!< Initialisation.
        //@}
        void    updateUI();                                                 //!< Mise � jour de l'interface.

        OSGi::Context*  mContext;

        using ControllerPtr     = EDIT::Controller::Ptr;                    //!< 
        using TileControllerPtr = std::weak_ptr<TILE::Controller>;          //!< 

        std::deque<ControllerPtr>   mCachedControllers;                     //!< 
        ControllerPtr               mController;                            //!< 
        TileControllerPtr           mTile;                                  //!< 
        Ui::TileEditor              mUI;                                    //!< Interface cr��e par Designer.
    };


    /*!
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent, OSGi::Context* pContext)
        : q(pParent)
        , mContext{pContext}
        , mController{}
        , mTile{}
        , mUI{}
    { }


    /*!
     */
    void Editor::EditorPrivate::init()
    {
        Q_Q(Editor);

        // Construit l'interface.
        q->from(q);
        mUI.setupUi(q);
        q->setObjectName(QString::null);    // Le widget pourra �tre correctement renomm� par UIService.
    }


    void Editor::EditorPrivate::updateUI()
    {
        if (!mTile.expired())
        {
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor(OSGi::Context* pContext)
        : Module::Editor{}
        , EDIT::Observer{}
        , CORE::Translate<Editor>{}
        , CORE::Synchronized{"file + enabled + edition + til;"}
        , pthis{this, pContext}
    {
        Q_D(Editor);

        d->init();
    }


    /*! Destructeur
     */
    Editor::~Editor() { }


//------------------------------------------------------------------------------
//                              Pertinence du sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canManage(EDIT::Controller::Ptr pDatum) const
    {
        return (std::dynamic_pointer_cast<EditorPrivate::TileControllerPtr::element_type>(pDatum) != nullptr);
    }


    /*!
     */
    void Editor::manage(EDIT::Controller::Ptr pDatum)
    {
        Q_D(Editor);

        // controller a �t� contr�l� en amont.
        // NB : il ne faut guarder qu'une seul r�f�rence forte (et g�n�rique)
        // sur le contr�leur (pour DAT::Contorler::subject(...)).
        d->mController  = pDatum;
        d->mTile        = std::dynamic_pointer_cast<EditorPrivate::TileControllerPtr::element_type>(pDatum);

        // Mise en cache du contr�leur.
        bool    lNew    = (std::find(d->mCachedControllers.begin(), d->mCachedControllers.end(), d->mController) == d->mCachedControllers.end());

        if (lNew)
        {
            if (eMaxCachedControllers <= d->mCachedControllers.size())
            {
                for(size_t lIndex = 0; lIndex < d->mCachedControllers.size(); ++lIndex)
                {
                    if (d->mCachedControllers[lIndex] != d->mController)
                    {
                        std::swap(d->mCachedControllers[lIndex], d->mCachedControllers.front());
                        d->mCachedControllers.pop_front();
                        break;
                    }
                }
            }
            d->mCachedControllers.push_back(d->mController);
        }

        EDIT::Subject*  lTile  = EDIT::Controller::subject(pthis->mController);

        watch(lTile);

        // Mise � jour de l'interface en fonction du nouveau datum.
        d->updateUI();
    }


    /*!
     */
    void Editor::unmanage()
    {
        watch(nullptr);

        pthis->updateUI();
    }


    /*!
     */
    void Editor::manageNone()
    {
        pthis->mCachedControllers.clear();
        pthis->mController.reset();
        pthis->mTile.reset();

        watch(nullptr);

        pthis->updateUI();
    }


//------------------------------------------------------------------------------
//                                Ecoute du sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::listen(const EDIT::Answer* /*pAnswer*/)
    {
        Q_D(Editor);

        // Met simplement � jour l'interface.
        d->updateUI();

        // Et la vue 3D.
        worldView()->update();
    }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    CORE::Translated::Updater Editor::buildTranslater()
    {
        return [=]()
        {
            pthis->mUI.retranslateUi(this);
        };
    }
}
