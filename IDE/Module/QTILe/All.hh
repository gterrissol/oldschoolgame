/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTIL_ALL_HH
#define QTIL_ALL_HH

/*! @file IDE/Module/QTILe/All.hh
    @brief Interface publique du module @ref QTILe.
    @author @ref Guillaume_Terrissol
    @date 24 Juillet 2006 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QTIL  //! Editeurs de tuiles.
{
    /*! @namespace QTIL
        @version 0.35

     */

    /*! @defgroup QTILe QTILe : Editeur de tuiles de monde ext�rieur
        <b>namespace</b> QTILe.
     */
}

#include "Controller.hh"
#include "Editor.hh"
#include "FragmentMgr.hh"

#endif  // De QTIL_ALL_HH
