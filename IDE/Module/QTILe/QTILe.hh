/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTIL_QTILE_HH
#define QTIL_QTILE_HH

/*! @file IDE/Module/QTILe/QTILe.hh
    @brief Pr�-d�clarations du module @ref QTILe.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QTIL
{
    class Editor;
    class FragmentMgr;
}

#endif  // De QTIL_QTILE_HH
