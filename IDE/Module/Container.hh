/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MODULE_CONTAINER_HH
#define MODULE_CONTAINER_HH

/*! @file IDE/Module/Container.hh
    @brief En-t�te de la classe Module::Container.
    @author @ref Guillaume_Terrissol
    @date 16 Janvier 2013 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Container.hh"
#include "CORE/Private.hh"
#include "EDITion/Controller.hh"

namespace Module
{
    /*! @brief Cat�gorie @ref category_module "Univers".
        @version 0.2
     */
    class Container : public CORE::Container
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    Container(OSGi::Context* pContext);                     //!< Constructeur.
virtual             ~Container();                                           //!< Destructeur.
        //@}
        void        engine(QString pAction);                                //!< Menu "Moteur".
        //! @name Vue sur le monde
        //@{
        QWidget*    worldView() const;                                      //!< Vue principale (3D).

    signals:

        void        worldViewChanged(QWidget* pNew);                        //!< Sur changement de widget 3D.
        //@}
    protected:

        void        addWidget(Widget pWidget) override;                     //!< Insertion d'un widget.

    private slots:

        void        onEdit(EDIT::Controller::Ptr pController);              //!< Edition d'une donn�e.
        void        onLeave(EDIT::Controller::Ptr pController);             //!< Arr�t de l'�dition d'une donn�e.

    private:

        QStringList sortWidgets(const QStringList& pNames) const override;  //!< Tri des widgets.

        Q_CUSTOM_DECLARE_PRIVATE(Container)
    };
}

#endif  // MODULE_CONTAINER_HH
