/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Editor.hh"

/*! @file IDE/QOBJect/Editor.cc
    @brief M�thodes (non-inline) de la classe QOBJ::Editor.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <deque>

#include "QT/MainWindow.hh"
#include "UI/GameObjectEditor.hh"

#include "Controller.hh"

SPECIALIZE_EDITOR(OBJ::GameObject, QT_TR_NOOP("Objet de jeu"), QT_TR_NOOP("Editeur d'objet de jeu"), ":/types/appearance")

namespace QOBJ
{
    enum
    {
        eMaxCachedControllers = 32
    };

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QTIL::Editor.
        @version 0.2
     */
    class Editor::EditorPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Editor)
    public:
                        EditorPrivate(Editor* pParent);                       //!< Constructeur.
 static QTAB::QPage*    create();                                               //!< Cr�ation d'un onglet.
        void            init();                                                 //!< Initialisation.
        void            updateUI();                                             //!< Mise � jour de l'interface.

        typedef std::shared_ptr<DAT::Controller>   ControllerPtr;             //!< 
        typedef std::weak_ptr<OBJ::Controller>     GameObjectControllerPtr;   //!< 
        std::deque<ControllerPtr>  mCachedControllers;                         //!< 
        ControllerPtr              mController;                                //!< 
        GameObjectControllerPtr    mGameObject;                                //!< 
        Ui::GameObjectEditor        mUI;                                        //!< Interface cr��e par Designer.
    };


    /*!
     */
    Editor::EditorPrivate::EditorPrivate(Editor* pParent)
        : q_custom_ptr(pParent)
        , mController()
        , mGameObject()
        , mUI()
    { }


    /*!
     */
    QTAB::QPage* Editor::EditorPrivate::create()
    {
        return new Editor();
    }


    /*!
     */
    void Editor::EditorPrivate::init()
    {
        Q_Q(Editor);

        // Construit l'interface.
        mUI.setupUi(q);

        QT::QMainWindow::synchronize(q, "file + enabled + edition");
    }


    void Editor::EditorPrivate::updateUI()
    {
        if (!mGameObject.expired())
        {
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    Editor::Editor()
        : QPNL::QGenericEditor<OBJ::GameObject>()
        , QT::QObserver(nullptr)
        , d_custom_ptr(new EditorPrivate(this))
    {
        Q_D(Editor);

        d->init();
    }


    /*!
     */
    QTAB::QTab* Editor::builder()
    {
        return new QTAB::QTab(&Editor::name, &Editor::title, QIcon(":/types/appearance"), &EditorPrivate::create);
    }


    /*! Destructeur
     */
    Editor::~Editor() { }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    void Editor::onLanguageChange()
    {
        Q_D(Editor);

        d->mUI.retranslateUi(this);
    }


//------------------------------------------------------------------------------
//                             Comportement de page
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::manageMenuEntry(QAction* /*entry*/)
    {
        // Rien pour l'instant.
    }


    /*!
     */
    void Editor::fillPageMenu(QMenu*& /*pageMenu*/) const
    {
        // Rien pour l'instant.
    }


    /*!
     */
    QIcon Editor::statusIcon() const
    {
        return QIcon(":/types/appearance");
    }


//------------------------------------------------------------------------------
//                              Pertinence du sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool Editor::canManage(ControllerPtr controller) const
    {
        return (std::dynamic_pointer_cast<EditorPrivate::GameObjectControllerPtr::element_type>(controller) != nullptr);
    }


    /*!
     */
    void Editor::manage(ControllerPtr controller)
    {
        Q_D(Editor);

        // controller a �t� contr�l� en amont.
        // NB : il ne faut guarder qu'une seul r�f�rence forte (et g�n�rique)
        // sur le contr�leur (pour DAT::Contorler::subject(...)).
        d->mController  = controller;
        d->mGameObject  = std::dynamic_pointer_cast<EditorPrivate::GameObjectControllerPtr::element_type>(controller);

        // Mise en cache du contr�leur.
        bool    lNew    = (std::find(d->mCachedControllers.begin(), d->mCachedControllers.end(), d->mController) == d->mCachedControllers.end());

        if (lNew)
        {
            if (eMaxCachedControllers <= d->mCachedControllers.size())
            {
                for(size_t lIndex = 0; lIndex < d->mCachedControllers.size(); ++lIndex)
                {
                    if (d->mCachedControllers[lIndex] != d->mController)
                    {
                        std::swap(d->mCachedControllers[lIndex], d->mCachedControllers.front());
                        d->mCachedControllers.pop_front();
                        break;
                    }
                }
            }
            d->mCachedControllers.push_back(d->mController);
        }

        QT::QSubject*   lGameObject  = DAT::Controller::subject(d->mController);

        watch(lGameObject);

        // Mise � jour de l'interface en fonction du nouveau datum.
        d->updateUI();
    }


//------------------------------------------------------------------------------
//  Ecoute du sujet
//------------------------------------------------------------------------------

    /*!
     */
    void Editor::listen(const QT::QAnswer*)
    {
        Q_D(Editor);

        // Met simplement � jour l'interface.
        d->updateUI();

        // Et la vue 3D.
        workingWidget()->update();
    }
}
