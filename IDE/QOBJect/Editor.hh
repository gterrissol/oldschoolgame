/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOBJ_EDITOR_HH
#define QOBJ_EDITOR_HH

#include "QOBJect.hh"

/*! @file IDE/QOBJect/Editor.hh
    @brief En-t�te de la classe QOBJ::Editor.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "OBJect/GameObject.hh"
#include "QPaNeL/Editor.hh"
#include "QT/Observer.hh"

namespace QOBJ
{
    /*! @brief Editeur de tuile.
        @version 0.4

        Interface QT d'�dition de tuile.
     */
    class Editor : public QPNL::QGenericEditor<OBJ::GameObject>, public QT::QObserver
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Editor();                                  //!< Constructeur par d�faut.
static  QTAB::QTab* builder();                                  //!< 
virtual             ~Editor();                                 //!< Destructeur.
        //@}

    protected:
        //! @name Changement de langue
        //@{
virtual void        onLanguageChange();                         //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private:
        //! @name Comportement de page
        //@{
virtual void        manageMenuEntry(QAction* entry);            //!< Gestion d'une entr�e du menu du widget.
virtual void        fillPageMenu(QMenu*& pageMenu) const;       //!< Cr�ation du menu du widget.
virtual QIcon       statusIcon() const;                         //!< Ic�ne repr�sentant l'�tat actuel du widget.
        //@}
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(ControllerPtr controller) const; //!< Possibilit� de gestion d'un sujet.
virtual void        manage(ControllerPtr controller);          //!< Gestion effective d'un sujet.
        //@}
        //! @name Ecoute du sujet
        //@{
virtual void        listen(const QT::QAnswer* answer = 0);      //!< Ecoute effective de la cible.
        //@}
        Q_DISABLE_COPY(Editor)
        Q_CUSTOM_DECLARE_PRIVATE(Editor)
    };
}

#endif  // De QOBJ_EDITOR_HH
