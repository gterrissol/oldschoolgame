/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOBJ_MODELLIBRARY_HH
#define QOBJ_MODELLIBRARY_HH

#include "QOBJect.hh"

/*! @file IDE/QOBJect/ModelLibrary.hh
    @brief En-t�te de la classe QOBJ::ModelLibrary.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "QPaNeL/Editor.hh"
#include "STL/STL.hh"

namespace QOBJ
{
    /*! @brief Biblioth�que de mod�les.
        @version 0.2

        Cet outil permet d'importer, classer et visualiser des modl�les g�om�triques.
     */
    class ModelLibrary : public QPNL::QGenericEditor<GEO::Geometry>
    {
        Q_OBJECT
    public :
        //! @name Type de pointeur
        //@{
        typedef std::shared_ptr<APP::Appearance>   AppearancePtr; //!< Pointeur sur apparence.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    ModelLibrary();                        //!< Constructeur par d�faut.
static  QTAB::QTab* builder();                              //!< Cr�ation.
virtual             ~ModelLibrary();                       //!< Destructeur.
        //@}

    signals:

        void        modelSelected(AppearancePtr pModel);

    protected:
        //! @name Changement de langue
        //@{
virtual void        onLanguageChange();                     //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private slots:
        //! @name Slots
        //@{
        void        categorySelected(int pIndex);
        void        addCategory();
        void        modelSelected(int pIndex);
        void        addModel();
        void        manageMaterial();
        //@}
        void        checkCategoryName(const QString& pName);    //!< 
        void        checkModelName(const QString& pName);       //!< 
        void        selectModelFile();                          //!<

    private:
        //! @name Comportement de page
        //@{
virtual void        manageMenuEntry(QAction* entry);        //!< Gestion d'une entr�e du menu du widget.
virtual void        fillPageMenu(QMenu*& pageMenu) const;   //!< Cr�ation du menu du widget.
virtual QIcon       statusIcon() const;                     //!< Ic�ne repr�sentant l'�tat actuel du widget.
        //@}
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(ControllerPtr pDatum) const; //!< Possibilit� de gestion d'un sujet.
virtual void        manage(ControllerPtr pDatum);          //!< Gestion effective d'un sujet.
        //@}
        Q_DISABLE_COPY(ModelLibrary)
        Q_CUSTOM_DECLARE_PRIVATE(ModelLibrary)
    };
}

#endif  // De QOBJ_MODELLIBRARY_HH
