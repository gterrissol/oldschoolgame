/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ModelLibrary.hh"

/*! @file IDE/QOBJect/ModelLibrary.cc
    @brief M�thodes (non-inline) de la classe QOBJ::ModelLibrary.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 26 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QCoreApplication>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QIcon>
#include <QLineEdit>
#include <QMap>

#include "APPearance/CustomApp.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "PAcKage/LoaderSaver.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATH/Const.hh"
#include "QPAcKage/FileDB.hh"
#include "QT/MainWindow.hh"
#include "QT3D/Widget.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/String.hh"

#include "UI/ModelLibrary.hh"

SPECIALIZE_EDITOR(GEO::Geometry, QT_TR_NOOP("Mod�les"), QT_TR_NOOP("Biblioth�que de mod�les"), ":/types/geometry")

namespace QOBJ
{
//------------------------------------------------------------------------------
//                            Interfaces Assistantes
//------------------------------------------------------------------------------

    /*! @brief Interface de saisie des noms de cat�gorie.
        @version 0.4
     */
    class NewCategoryDialog : public QDialog
    {
    public:

        NewCategoryDialog(QWidget* pLibraryUI, QWidget* pParent)
            : QDialog(pParent)
            , mLineEdit(nullptr)
            , mButtonBox(nullptr)
        {
            setWindowTitle(tr("Nouvelle cat�gorie"));
            setModal(true);
            resize(269, 77);

            QGridLayout*    lGridLayout         = new QGridLayout(this);

            QLabel*         lLabel              = new QLabel(this);
            lLabel->setText(tr("Nom"));
            lGridLayout->addWidget(lLabel, 0, 0, 1, 1);

                            mLineEdit           = new QLineEdit(this);
            mLineEdit->setFocus(Qt::OtherFocusReason);
            lGridLayout->addWidget(mLineEdit, 0, 1, 1, 1);

            QSpacerItem*    lHorizontalSpacer   = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Maximum);
            lGridLayout->addItem(lHorizontalSpacer, 0, 2, 1, 1);

                            mButtonBox          = new QDialogButtonBox(this);
            mButtonBox->setOrientation(Qt::Vertical);
            mButtonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
            mButtonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            lGridLayout->addWidget(mButtonBox, 0, 2, 2, 1);

            QSpacerItem*    lVerticalSpacer     = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Maximum);
            lGridLayout->addItem(lVerticalSpacer, 1, 0, 1, 1);

            connect(mButtonBox, SIGNAL(accepted()),                             SLOT(accept()));
            connect(mButtonBox, SIGNAL(rejected()),                             SLOT(reject()));
            connect(mLineEdit,  SIGNAL(textEdited(const QString&)), pLibraryUI, SLOT(checkCategoryName(const QString&)));
        }

        virtual ~NewCategoryDialog() { }

        void setOkButtonEnabled(bool pEnabled)
        {
            mButtonBox->button(QDialogButtonBox::Ok)->setEnabled(pEnabled);
        }

        QString getCategoryName() const
        {
            return mLineEdit->text();
        }


    private:

        QLineEdit*          mLineEdit;
        QDialogButtonBox*   mButtonBox;
    };


    /*! @brief Interface de saisie des noms de cat�gorie.
        @version 0.4
     */
    class NewModelDialog : public QDialog
    {
    public:

        NewModelDialog(QWidget* pLibraryUI, QWidget* pParent)
            : QDialog(pParent)
            , mLineEdit1(nullptr)
            , mLineEdit2(nullptr)
            , mButtonBox(nullptr)
        {
            setWindowTitle(tr("Nouveau mod�le"));
            setModal(true);
            resize(314, 98);

            QGridLayout*    lGridLayout         = new QGridLayout(this);

            QLabel*         lLabel1             = new QLabel(this);
            lLabel1->setText(tr("Nom"));
            lGridLayout->addWidget(lLabel1, 0, 0, 1, 1);

                            mLineEdit1          = new QLineEdit(this);
            mLineEdit1->setFocus(Qt::OtherFocusReason);
            lGridLayout->addWidget(mLineEdit1, 0, 1, 1, 1);

            QSpacerItem*    lHorizontalSpacer   = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Maximum);
            lGridLayout->addItem(lHorizontalSpacer, 0, 2, 1, 1);

            QLabel*         lLabel2 = new QLabel(this);
            lLabel2->setText(tr("Fichier"));
            lGridLayout->addWidget(lLabel2, 1, 0, 1, 1);

                            mLineEdit2          = new QLineEdit(this);
            mLineEdit2->setEnabled(false);
            lGridLayout->addWidget(mLineEdit2, 1, 1, 1, 1);

            QPushButton*    lPushButton = new QPushButton(this);
            QSizePolicy     lSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            lSizePolicy.setHorizontalStretch(0);
            lSizePolicy.setVerticalStretch(0);
            lSizePolicy.setHeightForWidth(lPushButton->sizePolicy().hasHeightForWidth());
            lPushButton->setSizePolicy(lSizePolicy);
            lPushButton->setMaximumSize(QSize(24, 24));
            lPushButton->setText("...");

            lGridLayout->addWidget(lPushButton, 1, 2, 1, 1);

                            mButtonBox          = new QDialogButtonBox(this);
            mButtonBox->setOrientation(Qt::Vertical);
            mButtonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
            mButtonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            lGridLayout->addWidget(mButtonBox, 0, 3, 3, 1);

            QSpacerItem*    lVerticalSpacer     = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Maximum);
            lGridLayout->addItem(lVerticalSpacer, 2, 0, 1, 1);

            connect(mButtonBox,  SIGNAL(accepted()),                             SLOT(accept()));
            connect(mButtonBox,  SIGNAL(rejected()),                             SLOT(reject()));
            connect(mLineEdit1,  SIGNAL(textEdited(const QString&)), pLibraryUI, SLOT(checkModelName(const QString&)));
            connect(lPushButton, SIGNAL(clicked()),                  pLibraryUI, SLOT(selectModelFile()));
        }

        virtual ~NewModelDialog() { }

        void setOkButtonEnabled(bool pEnabled)
        {
            mButtonBox->button(QDialogButtonBox::Ok)->setEnabled(pEnabled);
        }

        QString getModelName() const
        {
            return mLineEdit1->text();
        }

        QString getFileName() const
        {
            return mLineEdit2->text();
        }

        void setFileName(const QString& pName)
        {
            mLineEdit2->setText(pName);
        }

    private:

        QLineEdit*          mLineEdit1;
        QLineEdit*          mLineEdit2;
        QDialogButtonBox*   mButtonBox;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief
        @version 0.3
     */
    class Model
    {
    public:
        //! @name Constructeurs
        //@{
                Model();                           //!< Par d�faut.
                Model(GEO::Key pKey);             //!< Cr�ation.
                Model(PAK::Loader& pLoader);      //!< Chargement.
        //@}
        void    save(PAK::Saver& pSaver) const;    //!< 
        //! @name Informations
        //@{
        void        setModelSize(U32 pSize)
        {
            mSize = pSize;
        }
        U32         modelSize() const
        {
            return mSize;
        }
        GEO::Key   geoKey() const
        {
            return mGeoKey;
        }
        std::shared_ptr<MAT::Material> material(size_t pIndex = 0) const
        {
            if (pIndex < static_cast<size_t>(mMaterials.size()))
            {
                return STL::makeShared<MAT::Material>(mMaterials[pIndex]);
            }
            else
            {
                return STL::makeShared<MAT::Material>(MAT::Material());
            }
        }
        //@}

    private:
        //! @name Attributs
        //@{
        QVector<MAT::Material> mMaterials;
        GEO::Key               mGeoKey;
        U32                     mSize;
        //@}
    };


//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

    /*!
     */
    Model::Model()
        : mMaterials(1)
        , mGeoKey()
        , mSize(0)
    { }


    /*!
     */
    Model::Model(GEO::Key pKey)
        : mMaterials(1)
        , mGeoKey(pKey)
        , mSize(0)
    { }


    /*!
     */
    Model::Model(PAK::Loader& pLoader)
        : mMaterials()
        , mGeoKey()
        , mSize(0)
    {
        pLoader >> mGeoKey;
        pLoader >> mSize;

        U32 lCount = k0UL;
        pLoader >> lCount;
        for(U32 lM = k0UL; lM < lCount; ++lM)
        {
            mMaterials.push_back(MAT::Material(pLoader));
        }
        if (mMaterials.empty())
        {
            mMaterials.push_back(MAT::Material()); // Toujours le mat�riau par d�faut.
        }
    }


//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

    /*!
     */
    void Model::save(PAK::Saver& pSaver) const
    {
        pSaver << mGeoKey;
        pSaver << mSize;
        pSaver << U32(mMaterials.count());
        for(int lM = 0; lM < mMaterials.count(); ++lM)
        {
            mMaterials[lM].store(pSaver);
        }
    }


//------------------------------------------------------------------------------
//                          Informations
//------------------------------------------------------------------------------

 

//------------------------------------------------------------------------------
//                      ModelLibraryPrivate : P-Impl de ModelLibrary
//------------------------------------------------------------------------------

    /*! @brief P-Impl de ModelLibrary.
        @version 0.2

     */
    class ModelLibrary::ModelLibraryPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(ModelLibrary)
    public:
        //! @name "Constructeurs" & destructeur
        //@{
                        ModelLibraryPrivate(ModelLibrary* parent);    //!< Constructeur.
        void            init();                                         //!< Initialisation.
 static QTAB::QPage*    create();                                       //!< Cr�ation.
                        ~ModelLibraryPrivate();                        //!< Destructeur.
        //@}
        void            updateOnEvent();                                //!< Mise � jour de l'interface.
        //! @name Donn�es
        //@{
        void            loadData();                                     //!< 
        void            saveData() const;                               //!< 
        typedef QMap<QString, Model>           ModelContainer;        //!< 
        typedef QMap<QString, ModelContainer>  CategoryContainer;     //!< 
        CategoryContainer  mCategories;                                //!< Biblioth�que de mod�les.
        //@}
        void            clearCategories();                              //!< 
        void            clearModels();                                  //!< 
        void            clearView();                                    //!< 
        Ui::ModelLibrary            mUI;                                //!< Interface cr��e par Designer.
        NewCategoryDialog*         mNewCategoryDialog;                 //!< 
        NewModelDialog*            mNewModelDialog;                    //!< 
        QString                     mCurrentCategoryName;               //!< 
        QString                     mCurrentModelName;                  //!< 
        //! @name Widgets
        //@{
        QT3D::Viewer3D*            mViewer3D;                          //!< 
        //@}
    };


//------------------------------------------------------------------------------
//                    ModelLibraryPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    ModelLibrary::ModelLibraryPrivate::ModelLibraryPrivate(ModelLibrary* parent)
        : q_custom_ptr(parent)
        , mCategories()
        , mUI()
        , mNewCategoryDialog(nullptr)
        , mNewModelDialog(nullptr)
        , mViewer3D(nullptr)
    { }


    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::init()
    {
        Q_Q(ModelLibrary);

        // Construit l'interface.
        mUI.setupUi(q);

        mViewer3D   = new QT3D::Viewer3D(mUI.modelViewFrame);

        //----------
        QGridLayout*    lLayout = new QGridLayout(mUI.modelViewFrame);
        lLayout->setMargin(0);
        lLayout->addWidget(mViewer3D, 0, 0, 1, 1);

        //----------
        q->connect(mUI.categoryList,       SIGNAL(currentRowChanged(int)),   SLOT(categorySelected(int)));
        q->connect(mUI.addCategoryButton,  SIGNAL(clicked()),                SLOT(addCategory()));
        q->connect(mUI.modelList,          SIGNAL(currentRowChanged(int)),   SLOT(modelSelected(int)));
        q->connect(mUI.addModelButton,     SIGNAL(clicked()),                SLOT(addModel()));
           connect(mUI.animateButton,      SIGNAL(toggled(bool)), mViewer3D, SLOT(animate(bool)));
           connect(mUI.lightButton,        SIGNAL(toggled(bool)), mViewer3D, SLOT(light(bool)));
        q->connect(mUI.prevMaterialButton, SIGNAL(clicked()),                SLOT(manageMaterial()));
        q->connect(mUI.newMaterialButton,  SIGNAL(clicked()),                SLOT(manageMaterial()));
        q->connect(mUI.nextMaterialButton, SIGNAL(clicked()),                SLOT(manageMaterial()));

        QT::QMainWindow::synchronize(q, "file + enabled + edition");// + til;");

        loadData();
    }


    /*!
     */
    QTAB::QPage* ModelLibrary::ModelLibraryPrivate::create()
    {
        return new ModelLibrary();
    }


    /*! Destructeur.
     */
    ModelLibrary::ModelLibraryPrivate::~ModelLibraryPrivate()
    {
        saveData();
    }


//------------------------------------------------------------------------------
//                    ModelLibraryPrivate :
//------------------------------------------------------------------------------

    /*! Mise � jour de l'interface.
     */
    void ModelLibrary::ModelLibraryPrivate::updateOnEvent()
    {
        /*if (mCategoryList->currentRow() != -1)
        {
            mModelFrame->setEnabled(true);

            ModelContainer     lModels     = d->mCategories[lName];
            fore(ModelContainer::const_iterator
            {
                mModelList->addItem(
            }

            if (mModelList->currentRow() != -1)
            {
                mViewFrame->setEnabled(true);
                // ... Sutie � venir
            }
            else
            {
                mViewFrame->setEnabled(false);
            }
        }
        else
        {
            mModelFrame->setEnabled(false);
            mViewFrame->setEnabled(false);
        }*/
    }


//------------------------------------------------------------------------------
//                    ModelLibraryPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::loadData()
    {
        clearCategories();

        PAK::FileHdl   lDataHdl = QPAK::FileDB::get()->fileHdl("objects.mdl");
        PAK::Loader    lLoader(lDataHdl);

        U32 lCategoryCount  = k0UL;
        lLoader >> lCategoryCount;

        for(U32 lCategory = k0UL; lCategory < lCategoryCount; ++lCategory)
        {
            String  lCategoryReadName;
            lLoader >> lCategoryReadName;
            QString lCategoryName(lCategoryReadName.c_str());

            U32     lModelCount;
            lLoader >> lModelCount;

            ModelContainer&    lModelContainer = mCategories[lCategoryName];

            for(U32 lModel = k0UL; lModel < lModelCount; ++lModel)
            {
                String  lModelReadName;
                lLoader >> lModelReadName;
                QString lModelName(lModelReadName.c_str());

                lModelContainer[lModelName] = Model(lLoader);
            }

            mUI.categoryList->addItem(lCategoryName);
        }
    }


    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::saveData() const
    {
        PAK::FileHdl   lDataHdl = QPAK::FileDB::get()->fileHdl("objects.mdl");
        PAK::Saver     lSaver(lDataHdl);

        lSaver << U32(mCategories.count());

        for(auto lCategoryI = mCategories.begin(); lCategoryI != mCategories.end(); ++lCategoryI)
        {
            lSaver << String(lCategoryI.key().toAscii().constData());

            const ModelContainer&  lModels = lCategoryI.value();
            lSaver << U32(lModels.count());

            for(auto lModelI = lModels.begin(); lModelI != lModels.end(); ++lModelI)
            {
                lSaver << String(lModelI.key().toAscii().constData());
                lModelI.value().save(lSaver);
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::clearCategories()
    {
        mCategories.clear();
        mUI.categoryList->clear();

        clearModels();
    }


    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::clearModels()
    {
        mUI.modelFrame->setEnabled(false);
        mUI.modelList->clear();
        clearView();
    }


    /*!
     */
    void ModelLibrary::ModelLibraryPrivate::clearView()
    {
        mUI.viewFrame->setEnabled(false);
        mUI.modelNameLabel->setText("");
        mUI.triangleCountLabel->setText("0");
        mUI.sizeInBytesLabel->setText("0");
        mUI.prevMaterialButton->setEnabled(false);
        mUI.nextMaterialButton->setEnabled(false);
        mViewer3D->display(nullptr);
    }


//------------------------------------------------------------------------------
//                     ModelLibrary : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    ModelLibrary::ModelLibrary()
        : QPNL::QGenericEditor<GEO::Geometry>()
        , d_custom_ptr(new ModelLibraryPrivate(this))
    {
        Q_D(ModelLibrary);

        d->init();
    }


    /*!
     */
    QTAB::QTab* ModelLibrary::builder()
    {
        return new QTAB::QTab(&ModelLibrary::name, &ModelLibrary::title, QIcon(":/types/geometry"), &ModelLibraryPrivate::create);
    }


    /*! Destructeur.
     */
    ModelLibrary::~ModelLibrary() { }


//------------------------------------------------------------------------------
//                             Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    void ModelLibrary::onLanguageChange()
    {
        Q_D(ModelLibrary);

        d->mUI.retranslateUi(this);
    }


//------------------------------------------------------------------------------
//                                     Slots
//------------------------------------------------------------------------------

    /*!
     */
    void ModelLibrary::categorySelected(int pIndex)
    {
        if (pIndex < 0)
        {
            return;
        }

        Q_D(ModelLibrary);

        typedef ModelLibraryPrivate::ModelContainer   ModelContainer;

        QListWidgetItem*        lSelected   = d->mUI.categoryList->item(pIndex);
        d->mCurrentCategoryName             = lSelected->text();

        const ModelContainer&  lModels     = d->mCategories[d->mCurrentCategoryName];

        d->clearModels();

        for(auto lModelI = lModels.begin(); lModelI != lModels.end(); ++lModelI)
        {
            d->mUI.modelList->addItem(lModelI.key());
        }
        if (0 < d->mUI.modelList->count())
        {
            d->mUI.modelList->setCurrentRow(0);
        }

        d->mUI.modelFrame->setEnabled(true);
    }


    /*!
     */
    void ModelLibrary::addCategory()
    {
        Q_D(ModelLibrary);

        Q_ASSERT_X(d->mNewCategoryDialog == nullptr, "void ModelLibrary::addCategory(const QString& pName)", "Unexpected existing dialog");

        d->mNewCategoryDialog = new NewCategoryDialog(this, d->mUI.addCategoryButton);

        if (d->mNewCategoryDialog->exec() == QDialog::Accepted)
        {
            QString             lName   = d->mNewCategoryDialog->getCategoryName();
            QListWidgetItem*    lItem   = new QListWidgetItem(lName);

            d->mUI.categoryList->addItem(lItem);
            d->mUI.categoryList->setCurrentRow(d->mUI.categoryList->row(lItem));
            d->mCategories[lName];
        }

        delete d->mNewCategoryDialog;
        d->mNewCategoryDialog = nullptr;
    }


    /*!
     */
    void ModelLibrary::modelSelected(int pIndex)
    {
        if (pIndex < 0)
        {
            return;
        }

        Q_D(ModelLibrary);

        typedef ModelLibraryPrivate::ModelContainer   ModelContainer;

        QListWidgetItem*        lSelected   = d->mUI.modelList->item(pIndex);
        d->mCurrentModelName                = lSelected->text();
        const Model&           lModel      = d->mCategories[d->mCurrentCategoryName][d->mCurrentModelName];

        d->mUI.viewFrame->setEnabled(true);

        std::shared_ptr<GEO::GeometryRsc>   lGeo = GeometryMgr->retrieve(lModel.geoKey(), true);
        QT3D::Viewer3D::AppearancePtr       lApp = STL::makeShared<APP::CustomApp>(lGeo, lModel.material(0));

        d->mViewer3D->display(lApp);
        d->mUI.modelNameLabel->setText(d->mCurrentModelName);
        d->mUI.triangleCountLabel->setText(QString("%1").arg(lGeo->triangleCount()));
        d->mUI.sizeInBytesLabel->setText(QString("%1").arg(lModel.modelSize()));

        emit modelSelected(lApp);
    }


    /*!
     */
    void ModelLibrary::addModel()
    {
        Q_D(ModelLibrary);

        Q_ASSERT_X(d->mNewModelDialog == nullptr, "void ModelLibrary::addModel(const QString& pName)", "Unexpected existing dialog");

        d->mNewModelDialog = new NewModelDialog(this, d->mUI.addModelButton);

        if (d->mNewModelDialog->exec() == QDialog::Accepted)
        {
            d->clearView();

            QString             lName       = d->mNewModelDialog->getModelName();
            QListWidgetItem*    lItem       = new QListWidgetItem(lName);

            QString             lFile       = d->mNewModelDialog->getFileName();
            PAK::FileHdl       lHdl        = QPAK::FileDB::get()->import(lFile);
            GEO::Key           lKey        = GeometryMgr->insert(lHdl);
            GeometryMgr->save();
            Model              lModel(lKey);
            lModel.setModelSize(QPAK::FileDB::get()->fileSize(lHdl));

            d->mCategories[d->mCurrentCategoryName][lName] = lModel;
            d->saveData();

            d->mUI.modelList->addItem(lItem);
            d->mUI.modelList->setCurrentRow(d->mUI.modelList->row(lItem));
        }

        delete d->mNewModelDialog;
        d->mNewModelDialog = nullptr;
    }


    /*!
     */
    void ModelLibrary::manageMaterial()
    {
        Q_D(ModelLibrary);

        if      (sender() == d->mUI.newMaterialButton)
        {
            // ...
        }
        else if (sender() == d->mUI.prevMaterialButton)
        {
            // ...
        }
        else if (sender() == d->mUI.nextMaterialButton)
        {
            // ...
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! 
     */
    void ModelLibrary::checkCategoryName(const QString& pName)
    {
        Q_D(ModelLibrary);

        Q_ASSERT_X(d->mNewCategoryDialog != nullptr, "void ModelLibrary::checkCategoryName(const QString& pName)", "Expected existing dialog");

        d->mNewCategoryDialog->setOkButtonEnabled(!pName.isEmpty() && d->mUI.categoryList->findItems(pName, Qt::MatchExactly).empty());
    }


    /*!
     */
    void ModelLibrary::checkModelName(const QString& pName)
    {
        Q_D(ModelLibrary);

        Q_ASSERT_X(d->mNewModelDialog != nullptr, "void ModelLibrary::checkModelName(const QString& pName)", "Expected existing dialog");

        bool    lOkEnabled = !pName.isEmpty()                               &&
                             !d->mNewModelDialog->getFileName().isEmpty()   &&
                              d->mUI.modelList->findItems(pName, Qt::MatchExactly).empty();
        d->mNewModelDialog->setOkButtonEnabled(lOkEnabled);
    }


    /*!
     */
    void ModelLibrary::selectModelFile()
    {
        Q_D(ModelLibrary);

        QString pFile   = QFileDialog::getOpenFileName(nullptr, tr("S�lectionnez un fichier de donn�es"), "", tr("G�om�tries (*.geo)"));

        if (!pFile.isEmpty())
        {
            d->mNewModelDialog->setFileName(pFile);

            bool    lOkEnabled = !d->mNewModelDialog->getModelName().isEmpty() &&
                                 !d->mNewModelDialog->getFileName().isEmpty();
            d->mNewModelDialog->setOkButtonEnabled(lOkEnabled);
        }
    }


//------------------------------------------------------------------------------
//                             Comportement de Page
//------------------------------------------------------------------------------

    /*!
     */
    void ModelLibrary::manageMenuEntry(QAction* /*entry*/)
    {
        // A venir.
    }


    /*!
     */
    void ModelLibrary::fillPageMenu(QMenu*& /*pageMenu*/) const
    {
        // A venir.
    }


    /*!
     */
    QIcon ModelLibrary::statusIcon() const
    {
        return QIcon(":/types/geometry");
    }


//------------------------------------------------------------------------------
//                          Pertinence du Sujet
//------------------------------------------------------------------------------

    /*!
     */
    bool ModelLibrary::canManage(ControllerPtr /*pDatum*/) const
    {
        // Aucun contr�leur n'est g�r�.
        return false;
    }


    /*!
     */
    void ModelLibrary::manage(ControllerPtr /*pDatum*/) { }
}
