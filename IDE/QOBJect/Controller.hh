/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOBJ_CONTROLLER_HH
#define QOBJ_CONTROLLER_HH

#include "QOBJect.hh"

/*! @file IDE/QOBJect/Controller.hh
    @brief En-t�te de la classe QOBJ::Controller.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATum/MobileController.hh"
#include "MATH/MATH.hh"
#include "OBJect/GameObject.hh"
#include "STL/Shared.hh"

namespace OBJ
{
    /*! @brief Conr�leur de Game Object.
        @version 0.2

     */
    class Controller :
        public STL::enable_shared_from_this<Controller>,
        public DAT::GenericController<GameObject, DAT::MobileController>
    {
    public :
        //! @name Types de pointeur
        //@{
        typedef DAT::GameEntityMgr::ControllerPtr ControllerPtr; //!< Pointeur sur contr�leur.
        typedef DAT::GameEntityMgr::EntityPtr     EntityPtr;     //!< Pointeur sur entit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        Controller(TEditedPtr pObject);            //!< Constructeur.
virtual                 ~Controller();                             //!< Destructeur.
 static ControllerPtr  make(EntityPtr pEntity);                   //!< Cr�ation.
        //@}

    private:
        //! @name Comportement de mobile
        //@{
virtual AppearancePtr  getAppearance() const;                      //!<
virtual MATH::V3       getPosition() const;                        //!< 
virtual MATH::Q        getOrientation() const;                     //!< 
virtual MATH::V3       getScale() const;                           //!< 
virtual void            doSetPosition(MATH::V3 pNew);              //!< 
virtual void            doSetOrientation(MATH::Q pNew);            //!< 
virtual void            doSetScale(MATH::V3 pNew);                 //!< 
        //@}
virtual QT::QSubject*   asSubject();                                //!< R�cup�ration du sujet d'�dition.
virtual QString         getExtension() const;                       //!< Extension associ�e � l'objet contr�l�
virtual RendererPtr    getRenderer();                              //!< R�cup�ration du rendrerer du sujet �dit�.
        FORBID_COPY(Controller)
        FREE_PIMPL()
    };
}

#endif  // De QOBJ_CONTROLLER_HH
