/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/QOBJect/Controller.cc
    @brief M�thodes (non-inline) de la classe QOBJ::Controller.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 21 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATum/Attributes.hh"
#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "STL/Shared.tcc"

namespace OBJ
{
//------------------------------------------------------------------------------
//                             P-Impl de Controller
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TILE::Controller.
        @version 0.5
     */
    class Controller::Private :
        public QT::QSubject,
        public DAT::Variation<MATH::V3, 0>,
        public DAT::Variation<MATH::Q>,
        public DAT::Variation<MATH::V3, 1>,
        public DAT::Value<MATH::V3, 0>,
        public DAT::Value<MATH::Q>,
        public DAT::Value<MATH::V3, 1>
    {
    public:

                Private(Controller* pParent);                         //!< 
virtual         ~Private();                                            //!< 

    private:

virtual void    sendAnswer(QT::QAnswer* pAnswer, QT::QRequest* pUndo);  //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                Controller P-Impl : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Controller::Private::Private(Controller* pParent)
        : QT::QSubject()
        , INTIALIZE_VARIATION_OVERLOAD(Controller, MATH::V3, translate,      position,   0)
        , INTIALIZE_VARIATION(         Controller, MATH::Q,  rotate,         orientation  )
        , INTIALIZE_VARIATION_OVERLOAD(Controller, MATH::V3, stretch,        scale,      1)
        , INTIALIZE_VALUE_OVERLOAD(    Controller, MATH::V3, setPosition,    position,   0)
        , INTIALIZE_VALUE(             Controller, MATH::Q,  setOrientation, orientation  )
        , INTIALIZE_VALUE_OVERLOAD(    Controller, MATH::V3, setScale,       scale,      1)
    { }


    /*! Destructeur.
     */
    Controller::Private::~Private() { }


//------------------------------------------------------------------------------
//                       Controller P-Impl : Autre M�thode
//------------------------------------------------------------------------------

    /*!
     */
    void Controller::Private::sendAnswer(QT::QAnswer* pAnswer, QT::QRequest* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                     Controller : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Controller::Controller(TEditedPtr pObject)
        : DAT::GenericController<GameObject, DAT::MobileController>(pObject)
        , pthis(this)
    { }



    /*! Destructeur.
     */
    Controller::~Controller() { }


    /*!
     */
    Controller::ControllerPtr Controller::make(EntityPtr pEntity)
    {
        return std::make_shared<Controller>(std::dynamic_pointer_cast<GameObject>(pEntity.lock()));
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    auto Controller::getAppearance() const -> AppearancePtr
    {
        return edited().lock()->appearance();
    }


    /*!
     */
    MATH::V3 Controller::getPosition() const
    {
        return edited().lock()->referencePosition();
    }


    /*!
     */
    MATH::Q Controller::getOrientation() const
    {
        return edited().lock()->referenceOrientation();
    }


    /*!
     */
    MATH::V3 Controller::getScale() const
    {
        return edited().lock()->referenceScale();
    }


    /*!
     */
    void Controller::doSetPosition(MATH::V3 pNew)
    {
        edited().lock()->setReferencePosition(pNew);
    }


    /*!
     */
    void Controller::doSetOrientation(MATH::Q pNew)
    {
        edited().lock()->setReferenceOrientation(pNew);
    }


    /*!
     */
    void Controller::doSetScale(MATH::V3 pNew)
    {
        edited().lock()->setReferenceScale(pNew);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QT::QSubject* Controller::asSubject()
    {
        return &(*pthis);
    }


    /*!
     */
    QString Controller::getExtension() const
    {
        return "obj";
    }


    /*!
     */
    Controller::RendererPtr Controller::getRenderer()
    {
        return Controller::RendererPtr();
    }
}
