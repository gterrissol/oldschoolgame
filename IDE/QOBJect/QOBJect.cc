    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QOBJect.hh"

/*! @file IDE/QOBJect/QOBJect.cc
    @brief D�finitions diverses du module @ref QOBJect.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 3 Mai 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QOBJ
{
}
