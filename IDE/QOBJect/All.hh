/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QOBJ_ALL_HH
#define QOBJ_ALL_HH

/*! @file IDE/QOBJect/All.hh
    @brief Interface publique du module @ref QOBJect.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2009 - 3 Mai 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QOBJ   //! <Descriptif du module>
{
    /*! @namespace QOBJ
        @version 0.2

        <Descriptif d�taill� du module>
     */

    /*! @defgroup QOBJect QOBJect : <Descriptif>
        <b>namespace</b> QOBJ et <...>.
     */

}

#include "Controller.hh"
#include "Editor.hh"
#include "ModelLibrary.hh"

#endif  // De QOBJ_ALL_HH
