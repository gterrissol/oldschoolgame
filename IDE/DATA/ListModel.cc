/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ListModel.hh"

/*! @file IDE/DATA/ListModel.cc
    @brief M�thodes (non-inline) de la classe DATA::ListModel.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include <QDebug>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMessageBox>
#include <QMimeData>
#include <QStringList>

#include "CORE/SyncService.hh"
#include "EDITion/Attributes.hh"
#include "EDITion/Controller.hh"
#include "EDITion/Observer.hh"

#include "List.hh"

namespace DATA
{
    using CategoryIdx = std::pair<QString, int>;

//------------------------------------------------------------------------------
//                              ListModel : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::ListModel.
        @version 0.5
     */
    class ListModel::ListModelPrivate : public EDIT::GenericController<ListModel>, public EDIT::Observer
    {
        Q_CUSTOM_DECLARE_PUBLIC(ListModel)
    public:
                            ListModelPrivate(ListModel* pThat, List* pWidget, OSGi::Context* pContext); //!< Constructeur.

        bool                moveDraggedRows(const QMimeData* pData, int pToRow);                        //!< D�placement d'items.
        bool                importData(const QMimeData* pData, const QModelIndex&);                     //!< Import de donn�es.
        //! @name M�thodes d'�dition
        //@{
        const QStringList&  itemNames() const;
        void                setItemNames(const QStringList& pNames);
        bool                addCategory(CategoryIdx pAdd);
        bool                delCategory(CategoryIdx pDel);
        //@}

        OSGi::Context*                      mContext;                                                   //!< Contexte d'ex�cution.
        List*                               mWidget;                                                    //!< Widget auquel le mod�le est associ�.
        QStringList                         mItemNames;                                                 //!< Noms des items.
        QMap<QString, CategoryModel::Ptr>   mCategories;                                                //!< Dictionnaire des cat�gories.
        QStringList                         mExtensions;                                                //!< Extensions possibles des fichiers � importer.
        int                                 mMaxRowCount;                                               //!< Nombre maximum de lignes autoris�.
        QItemSelectionModel*                mSelectionModel;                                            //!< Mod�le de s�lection des items.
        int                                 mCounter;                                                   //!< Compteur pour les noms des cat�gories.
        bool                                mIsEnabled;                                                 //!< Etat d'activation.
        //! @name Gestion du sujet
        //{
        void                initSubject();                                                              //!< 
        void                freeSubject();                                                              //!< 

    protected:

        EDIT::Subject*      asSubject() override;                                                       //!< 

    private:

        void                listen(const EDIT::Answer* pAnswer) override;                               //!< Ecoute.

        class Subject;
        std::unique_ptr<Subject>            mSubject;
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Sujet d'�dition pour ListModel.
        @version 0.5
     */
    class ListModel::ListModelPrivate::Subject
        : public EDIT::Subject
        , public EDIT::Value<const QStringList&>
        , public EDIT::Container<CategoryIdx>
    {
    public:
                Subject(ListModelPrivate* pParent);
virtual         ~Subject();
        void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo) override;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pThat    Mod�le dont l'instance est le p-impl.
        @param pWidget  Widget auquel est associ� @p pThat.
        @param pContext Contexte d'ex�cution.
     */
    ListModel::ListModelPrivate::ListModelPrivate(ListModel* pThat, List* pWidget, OSGi::Context* pContext)
        : EDIT::GenericController<ListModel>{pThat, pContext}
        , q{pThat}
        , mContext{pContext}
        , mWidget{pWidget}
        , mItemNames{}
        , mCategories{}
        , mExtensions{}
        , mMaxRowCount{999}
        , mSelectionModel{}
        , mCounter{}
        , mIsEnabled{false}
        , mSubject{}
    { }


    /*! @param pData  Lignes � d�placer
        @param pToRow Ligne � laquelle d�placer les items encod�s dans @p pData
     */
    bool ListModel::ListModelPrivate::moveDraggedRows(const QMimeData* pData, int pToRow)
    {
        QByteArray          lEncoded    = pData->data(q->mimeTypes().at(0));
        QDataStream         lStream(&lEncoded, QIODevice::ReadOnly);
        QMap<int, QString>  lRows{};
        int                 lLower      = 0;

        while(!lStream.atEnd())
        {
            int                 r{}, c{};
            QMap<int, QVariant> v{};

            lStream >> r >> c >> v;

            if (r < pToRow) ++lLower;
            lRows[r] = mItemNames.at(r);
        }
        pToRow -= lLower;

        QStringList lNewItems = mItemNames;
        auto    lR = lRows.uniqueKeys();
        while(!lR.isEmpty()) lNewItems.removeAt(lR.takeLast());
        auto    lN = lRows.values();
        while(!lN.isEmpty()) lNewItems.insert(pToRow, lN.takeLast());

        EDIT::EditRequest<const QStringList&>   lSetNames{lNewItems};
        EDIT::SubjectList{EDIT::Controller::subject(this)}.perform(&lSetNames);

        return false;
    }


    /*! @param pData   URLs des fichiers � importer
        @param pParent Identifie la cat�gorie pour laquelle importer les fichiers de @p pData
        @retval true Si l'import a r�ussi (au moins en partie)
        @retval false En cas d'�chec complet
     */
    bool ListModel::ListModelPrivate::importData(const QMimeData* pData, const QModelIndex& pParent)
    {
        if (pData->hasUrls() && pParent.isValid())
        {
            auto    lName       = mItemNames[pParent.row()];
            auto    lCategory   = mCategories[lName];
            QStringList lFiles;
            for(auto lURL : pData->urls())
            {
                auto    lFile = lURL.path();
#ifdef MINGW
                if (!lFile.isEmpty() && (lFile[0] == QChar('/')))
                {
                    lFile = lFile.mid(1);
                }
#endif  // MINGW
                lFiles << lFile;
            }

            return lCategory->import(lFiles);
        }
        else
        {
            return false;
        }
    }


    //!
    void ListModel::ListModelPrivate::initSubject()
    {
        mSubject = std::make_unique<Subject>(this);
        watch(mSubject.get());
    }

    //!
    void ListModel::ListModelPrivate::freeSubject()
    {
        watch(nullptr);
        mSubject.reset();
    }

    //!
    EDIT::Subject* ListModel::ListModelPrivate::asSubject()
    {
        return mSubject.get();
    }

    //!
    void ListModel::ListModelPrivate::listen(const EDIT::Answer* pAnswer)
    {
        if      (dynamic_cast<const EDIT::GenericAnswer<const QStringList&>*>(pAnswer))
        {
            q->refresh();
        }
        else if (dynamic_cast<const EDIT::GenericAnswer<CategoryIdx>*>(pAnswer))
        {
            q->refresh();
        }
        else
        {
            QMessageBox::warning(nullptr, tr("Failure"), tr("The operation failed."));
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    ListModel::ListModelPrivate::Subject::Subject(ListModelPrivate* pParent)
        : EDIT::Subject{"data"}
        , INITIALIZE_VALUE(setItemNames, itemNames)
        , INITIALIZE_CONTAINER(addCategory, delCategory)
    { }

    //!
    ListModel::ListModelPrivate::Subject::~Subject() = default;

    //!
    void ListModel::ListModelPrivate::Subject::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                              M�thodes d'�dition
//------------------------------------------------------------------------------

    /*!
     */
    const QStringList& ListModel::ListModelPrivate::itemNames() const
    {
        return mItemNames;
    }

    /*!
     */
    void ListModel::ListModelPrivate::setItemNames(const QStringList& pNames)
    {
        mItemNames = pNames;
    }


    /*!
     */
    bool ListModel::ListModelPrivate::addCategory(CategoryIdx pAdd)
    {
        if (pAdd.second + 1 <= q->maxRowCount())
        {
            auto    lIndex = q->createIndex(pAdd.second, 0);
            return q->doAddCategory(lIndex, pAdd.first);
        }
        else
        {
            return false;
        }

    }


    /*!
     */
    bool ListModel::ListModelPrivate::delCategory(CategoryIdx pDel)
    {
        auto lRow = q->categoryIndex(pDel.first).row();

        if (lRow != -1)
        {
            auto    lIndex = q->createIndex(lRow, 0);
            return q->doDelCategory(lIndex);
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                    ListModel : Constructeur & destructeur
//------------------------------------------------------------------------------

    /*  Passer par doImportRaw des ImportExport
        Passer par le service fr.osg.ide.pak.file pour cr�er les fichiers import�s.
        Il faut donc passer le contexte...
     */

    /*! @param pWidget  Widget auquel le mod�le est associ�.
        @param pName    Nom � utiliser pour le sotckage des donn�es IDE.
        @param pContext Contexte d'ex�cution.
     */
    ListModel::ListModel(List* pWidget, QString pName, OSGi::Context* pContext)
        : QAbstractListModel{pWidget}
        , Store{pName}
        , pthis{this, pWidget, pContext}
    {
        pWidget->setModel(this);
        pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->bind(this, "file+edition+hardware");
    }


    /*! Par d�faut.
     */
    ListModel::~ListModel() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    OSGi::Context* ListModel::context() const
    {
        return pthis->mContext;
    }


//------------------------------------------------------------------------------
//                 ListModel : Extensions des Donn�es Import�es
//------------------------------------------------------------------------------


    /*! @param pList Liste d'extensions des fichiers importables par les cat�gories du mod�le
     */
    void ListModel::setFileExtensions(QStringList pList)
    {
        pthis->mExtensions = std::move(pList);
    }


    /*! @return La liste d'extensions des fichiers importables
     */
    QStringList ListModel::fileExtensions() const
    {
        return pthis->mExtensions;
    }


//------------------------------------------------------------------------------
//                        ListModel : Gestion des Lignes
//------------------------------------------------------------------------------

    /*! @param pCount Nombre maximum de cat�gories qu'une instance de ce mod�le peut g�rer
     */
    void ListModel::setMaxRowCount(int pCount)
    {
        pthis->mMaxRowCount = pCount;
    }


    /*!
     */
    int ListModel::maxRowCount() const
    {
        return pthis->mMaxRowCount;
    }


    /*! @return Le nombre de cat�gories d�finies dans le mod�le
     */
    int ListModel::rowCount(const QModelIndex&) const
    {
        return pthis->mItemNames.count();
    }


//------------------------------------------------------------------------------
//                         ListModel : Contenu des Items
//------------------------------------------------------------------------------

    /*! @param pIndex Item dont r�cup�rer les propri�t�s
        @return Propri�t�s de l'item identifi� par @p pIndex
     */
    Qt::ItemFlags ListModel::flags(const QModelIndex& pIndex) const
    {
        if (pIndex.isValid())
        {
            return Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
        }
        else
        {
            return Qt::ItemIsEnabled |                         Qt::ItemIsDropEnabled;
        }
    }


    /*! @param pIndex Item dont r�cup�rer une donn�
        @param pRole  Role de la donn�e � r�cup�rer
        @return La donn�e de l'item identifi� par @p pIndex, dont le role est @p pRole
     */
    QVariant ListModel::data(const QModelIndex& pIndex, int pRole) const
    {
        if (!pIndex.isValid())
        {
            return {};
        }

        if (pthis->mItemNames.count() <= pIndex.row())
        {
            return {};
        }

        if ((pRole == Qt::DisplayRole) || (pRole == Qt::EditRole))
        {
            return pthis->mItemNames.at(pIndex.row());
        }
        else
        {
            return {};
        }
    }


    /*! @param pIndex Item dont d�finir une donn�
        @param pValue Valeur de la donn�e � d�finir
        @param pRole  Role de la donn�e � d�finir
        @retval true Si la d�finition a r�ussi
        @retval false En cas d'�chec
     */
    bool ListModel::setData(const QModelIndex& pIndex, const QVariant& pValue, int pRole)
    {
        if (pIndex.isValid())
        {
            if ((pRole == Qt::EditRole) || (pRole == Qt::DisplayRole))
            {
                const auto  lValue = pValue.toString();
                if (lValue != data(pIndex).toString())  // Si la valeur ne change pas, pas la peine d'aller plus loin.
                {
                    if ((pRole == Qt::DisplayRole) || !pthis->mItemNames.contains(lValue))
                    {
                        pthis->mItemNames.replace(pIndex.row(), pValue.toString());
                        emit dataChanged(pIndex, pIndex);
                        return true;
                    }
                    else
                    {
                        QMessageBox::warning(nullptr, tr("Invalid name"), tr("Such a name already exist"));
                    }
                }
            }
        }

        return false;
    }


    /*! @param pIndex Item destin� � la suppression
        @retval true Si l'item identifi� par @p pIndex peut �tre supprim�
        @retval false Sinon (e.g. certaines de ses donn�es sont d�j� utilis�es)
     */
    bool ListModel::mayRemove(const QModelIndex& pIndex) const
    {
        return pIndex.isValid() && isRemovable(pIndex);
    }


//------------------------------------------------------------------------------
//                      ListModel : Gestion du Drag'n'Drop
//------------------------------------------------------------------------------

    /*! @return La liste des types g�r�s par drag'n'drop
     */
    QStringList ListModel::mimeTypes() const
    {
        return QAbstractListModel::mimeTypes() << "text/uri-list";
    }


    /*! @param pData   Donn�es � d�poser
        @param pAction Type de d�p�t
        @retval true Si le d�p�t est possible
        @retval false Sinon
     */
    bool ListModel::canDropMimeData(const QMimeData* pData, Qt::DropAction pAction, int, int, const QModelIndex&) const
    {
        if (pAction == Qt::MoveAction) return true;

        if ((pAction == Qt::CopyAction) && pData->hasUrls())
        {
            QRegExp lRE{QStringLiteral(R"(.*\.(\w+))")};
            bool    lDropOk = true;
            for(auto lURL : pData->urls())
            {
                if (!lRE.exactMatch(lURL.path()) || !pthis->mExtensions.contains(QStringLiteral("*.%1").arg(lRE.cap(1))))
                {
                    qWarning() << tr("Can't handle file %1").arg(lURL.path());
                    lDropOk = false;
                }
            }
            if (lDropOk)
            {
                return true;
            }
        }

        return false;
    }


    /*! @param pData   Donn�es � d�poser
        @param pAction Type de d�p�t
        @param pRow    Identifie la ligne o� effectuer le d�p�t
        @param pColumn Identifie la colonne o� effectuer le d�p�t
        @param pParent Identifie l'item o� effectuer le d�p�t
        @retval true Si le d�p�t a r�ussi
        @retval false Sinon
     */
    bool ListModel::dropMimeData(const QMimeData* pData, Qt::DropAction pAction, int pRow, int pColumn, const QModelIndex& pParent)
    {
        if      (pAction == Qt::MoveAction)
        {
            if (pData->hasFormat(mimeTypes().at(0)))    // "application/x-qabstractitemmodeldatalist"
            {
                if      (pParent.isValid())
                {
                    return pthis->moveDraggedRows(pData, pParent.row());
                }
                else if ((pRow != -1) && (pColumn == 0))
                {
                    return pthis->moveDraggedRows(pData, pRow);
                }
            }

            return false;
        }
        else if (pAction == Qt::CopyAction)
        {
            auto    lIndex = pParent;
            if (!lIndex.isValid())
            {
                auto    lRowCount = rowCount();
                addCategory();
                lIndex = index(lRowCount, 0);
            }
            pthis->mSelectionModel->reset();
            pthis->mSelectionModel->select(lIndex, QItemSelectionModel::SelectCurrent);
            return pthis->importData(pData, lIndex);
        }

        return false;
    }


    /*! Glisser ne peut que d�placer des items.
     */
    Qt::DropActions ListModel::supportedDragActions() const
    {
        return Qt::MoveAction;
    }


    /*! Peuvent �tre d�pos�s des �l�ments internes ou externes
     */
    Qt::DropActions ListModel::supportedDropActions() const
    {
        return Qt::CopyAction | Qt::MoveAction;
    }


//------------------------------------------------------------------------------
//                            ListModels : Cat�gories
//------------------------------------------------------------------------------

    /*!
     */
    void ListModel::addCategory()
    {
        EDIT::InsertRequest<CategoryIdx>    lAddCategory{{nameForNewCategory(), rowCount()}};
        EDIT::SubjectList{EDIT::Controller::subject(pthis.get())}.perform(&lAddCategory);
    }


    /*!
     */
    void ListModel::delCategories()
    {
        QMap<int, QString>  lRemovable{};   // Une map est utilis�e pour obtenir les �l�ments � supprimer dans l'ordre d'affichage.
        QStringList         lDiscarded{};

        for(auto lItem : pthis->mWidget->selectionModel()->selectedIndexes())
        {
            auto    lName = data(lItem).toString();
            if (mayRemove(lItem))
            {
                lRemovable[lItem.row()] = lName;
            }
            else
            {
                lDiscarded << lName;
            }
        }
        auto    lToDelete = lRemovable.values();
        while(!lToDelete.isEmpty())
        {
            auto    lName = lToDelete.takeLast();
            EDIT::RemoveRequest<CategoryIdx>    lDelCategory{{lName, categoryIndex(lName).row()}};  // Suppression � partir de la fin.
            EDIT::SubjectList{EDIT::Controller::subject(pthis.get())}.perform(&lDelCategory);
        }
        if (!lDiscarded.isEmpty())
        {
            QMessageBox::warning(pthis->mWidget, tr("Failed deletion"), tr("Couldn't delete :") +"\n" + lDiscarded.join("\n"));
        }
    }


    /*! @param pName Nom de la cat�gorie � r�cup�rer
        @return La cat�gorie nomm�e @p pName, si elle existe, @b nullptr sinon
     */
    CategoryModel* ListModel::category(QString pName)
    {
        auto lCategory = pthis->mCategories.find(pName);
        if (lCategory != pthis->mCategories.end())
        {
            return lCategory.value().get();
        }
        else
        {
            return {};
        }
    }


    /*! @param pModel Nouveau mod�le de s�lection.
     */
    void ListModel::setSelectionModel(QItemSelectionModel* pModel)
    {
        pthis->mSelectionModel = pModel;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    bool ListModel::isEnabled() const
    {
        return pthis->mIsEnabled;
    }

    /*!
     */
    void ListModel::setEnabled(bool pEnabled)
    {
        if (pEnabled)
        {
            pthis->initSubject();
            // Pak file ouvert : on peut charger les donn�es.
            load();
            pthis->mCounter = string("counter").toInt();
            auto    lItems  = list("items");
            lItems.removeAll(QString::null);    // Rappel : Store conserve les �l�ments de liste vides.
            pthis->setItemNames(lItems);

            refresh();

            auto    lIds = list("ids");
            for(auto name : pthis->mItemNames)
            {
                pthis->mCategories[name] = makeCategory(name, lIds.front().toInt());
                pthis->mCategories[name]->setEnabled(true);
                lIds.pop_front();
            }

            editNow();
        }
        else
        {
            endEdit();

            QStringList lIds;
            for(auto name : pthis->mItemNames)
            {
                pthis->mCategories[name]->setEnabled(false);
                lIds << QString::number(pthis->mCategories[name]->id());
            }
            set("ids", lIds);

            // Le pak file va �tre ferm� : on sauvegarde.
            set("counter", QString::number(pthis->mCounter));
            set("items", pthis->mItemNames);
            save();

            // Le rafra�chissement doit avoir lieu avant le nettoyage des �l�ments, sinon les signaux pr�vus ne seront pas envoy�s.
            refresh();

            // Et on nettoie.
            beginResetModel();
            pthis->mCategories.clear();
            pthis->setItemNames({});
            endResetModel();
            pthis->mCounter = 0;

            pthis->freeSubject();
        }
    }


    /*! @note Les donn�es � charger ont �t� lues depuis le "magasin".
     */
    void ListModel::editNow()
    {
        pthis->mIsEnabled = true;
    }


    /*! @note Les donn�es � sauvegarder peuvent �tre plac�es dans le "magasin".
     */
    void ListModel::endEdit()
    {
        pthis->mIsEnabled = false;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void ListModel::refresh()
    {
        emit dataChanged(index(0), index(pthis->mItemNames.count() - 1));   // On rafra�chit tout.
        pthis->mWidget->selectionModel()->clearSelection();                 // Et plus de s�lection.
    }


    /*!
     */
    QModelIndex ListModel::categoryIndex(const QString& pName)
    {
        return createIndex(pthis->mItemNames.indexOf(pName), 0);    // -1 si pas trouv�, c'est tr�s bien.
    }


    /*!
     */
    bool ListModel::doAddCategory(const QModelIndex& pIndex, const QString& pAdded)
    {
        pthis->mItemNames.insert(pIndex.row(), pAdded);
        auto    lId = newCategoryId();
        if (lId != -1)
        {
            auto    lCategory   = pthis->mCategories[pAdded] = makeCategory(pAdded, lId);
            lCategory->setEnabled(true);
            refresh();
            return true;
        }

        return true;
    }


    /*!
     */
    bool ListModel::doDelCategory(const QModelIndex& pIndex)
    {
        auto    lName = pthis->mItemNames.takeAt(pIndex.row());
        auto    lCategory = pthis->mCategories[lName];
        freeCategoryId(lCategory->id());
        lCategory->setEnabled(false);
        pthis->mCategories.remove(lName);
        refresh();

        return true;
    }


    /*! @return Un nom unique pour une nouvelle cat�gorie
        @sa categoryName()
     */
    QString ListModel::nameForNewCategory() const
    {
        return QStringLiteral("%1 %2").arg(categoryName()).arg(++pthis->mCounter, 1 + log10(pthis->mMaxRowCount));
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @fn CategoryModel::Ptr ListModel::makeCategory(QString pName, int pId)
        A impl�menter par : @code return std::make_shared< RealCategoryModel >(this, pName, pId); @endcode
     */


    /*! @fn QString ListModel::categoryName() const
        A impl�menter par : @code return tr("Nom de la cat�gorie"); @endcode
     */
}
