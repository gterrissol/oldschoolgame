/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "DisplayImage.hh"

/*! @file IDE/DATA/DisplayImage.cc
    @brief M�thodes (non-inline) de la classe DATA::DisplayImage.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 11 Novembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QLabel>
#include <QLayout>

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    DisplayImage::DisplayImage(QWidget* pParent)
        : Display{pParent}
        , mImage{}
    {
        mImage = new QLabel{this};
        mImage->setAlignment(Qt::AlignCenter);
        layout()->addWidget(mImage);
    }


    /*!
     */
    DisplayImage::~DisplayImage() = default;


    /*!
     */
    void DisplayImage::render(QString pFilename)
    {
        mImage->setPixmap(QPixmap{pFilename});
    }


    /*!
     */
    void DisplayImage::reset()
    {
        mImage->setPixmap(QPixmap{});
    }
}
