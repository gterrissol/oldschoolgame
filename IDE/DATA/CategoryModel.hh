/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_CATEGORYMODEL_HH
#define DATA_CATEGORYMODEL_HH

#include "DATA.hh"

/*! @file IDE/DATA/CategoryModel.hh
    @brief En-t�te de la classe DATA::CategoryModel.
    @author @ref Guillaume_Terrissol
    @date 20 Novembre 2013 - 27 Septembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QAbstractItemModel>

#include "CORE/Private.hh"

#include "Store.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour une cat�gorie de donn�es.
        @version 0.4
     */
    class CategoryModel : public QAbstractItemModel, public Store
    {
        Q_OBJECT
    public:
        //! @name Pointeur
        //@{
        using Ptr = std::shared_ptr<CategoryModel>;                                                                 //!< ... sur mod�le.
        //@}
        //! @name Constante
        //@{
        static const QSize  kSnapshotSize;                                                                          //!< Taille des snapshots.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    CategoryModel(QAbstractItemModel* pParent, QString pName, int pId);                             //!< Constructeur.
virtual             ~CategoryModel();                                                                               //!< Destructeur.
        //@}
        int         id() const;                                                                                     //!< Identifiant de la cat�gorie.
        //! @name Nombre de lignes et de colonnes
        //@{
        void        setMaxRowCount(int pCount);                                                                     //!< D�finition du nombre maximum de lignes.
        int         maxRowCount() const;                                                                            //!< Nombre maximum de lignes.
        bool        insertRows(int pRow, int pCount, const QModelIndex& = QModelIndex()) override;                  //!< Insertion de lignes.
        bool        removeRows(int pRow, int pCount, const QModelIndex& = QModelIndex()) override;                  //!< Suppression de lignes.
        int         rowCount(const QModelIndex& = QModelIndex()) const override;                                    //!< Nombre actuel de lignes.

        void        setMaxColumnCount(int pCount);                                                                  //!< D�finition du nombre maximum de colonnes.
        int         maxColumnCount() const;                                                                         //!< Nombre maximum de colonnes.
        bool        insertColumns(int pColumn, int pCount, const QModelIndex& pParent = QModelIndex()) override;    //!< Insertion de colonnes.
        bool        removeColumns(int pColumn, int pCount, const QModelIndex& pParent = QModelIndex()) override;    //!< Supression de colonnes.
        int         columnCount(const QModelIndex& = QModelIndex()) const override;                                 //!< Nombre actuel de colonnes.
        //@}
        //! @name Gestion des donn�es
        //@{
        bool        import(QStringList pFilenames);                                                                 //!< Import d'un ensemble de fichiers.
        void        remove(QModelIndexList pIndexes);                                                               //!< Suppression de donn�es import�es.
        //@}
        //! @name Impl�mentation minimale du mod�le
        //@{
        QModelIndex index(int pRow, int pColumn, const QModelIndex& pParent = QModelIndex()) const override;        //!< Index d'un item.
        QModelIndex parent(const QModelIndex& pIndex) const override;                                               //!< Parent d'un item.
        QVariant    data(const QModelIndex& pIndex, int pRole = Qt::DisplayRole) const override;                    //!< [Quelques] Donn�es d'un item.
        bool        setData(const QModelIndex& pIndex, const QVariant& pValue, int pRole) override;                 //!< D�finition de [quelques] donn�es d'un item.
        //@}
        //! @name Activation
        //@{
    public slots:

        void        setEnabled(bool pEnabled);                                                                      //!< Disponibilit� de la cat�gorie.

    protected:

virtual void        editNow();                                                                                      //!< Activation du mod�le.
virtual void        endEdit();                                                                                      //!< D�sactivation du mod�le.
        //@}
virtual void        fillAtCreation();                                                                               //!< D�finitions initiales.
        //! @name Chargement des donn�es
        //@{
        /*! @brief Proxy pour le chargement des donn�es par commandes.
            @version 1.0
         */
        class Datum
        {
        public:
            using Ptr = std::shared_ptr<Datum>; //!< Pointeur dur donn�e.
    virtual ~Datum();                           //!< Destructeur.
        };

virtual Datum::Ptr  make(QString pFilename) = 0;                                                                    //!< Chargment extene.
virtual Datum::Ptr  take(const QModelIndex& pIndex) = 0;                                                            //!< Chargment interne.
        //@}
        void        refresh();                                                                                      //!< Rafra�chissement IHM.
        //! @name  Acc�s au mod�le parent
        //@{
        ListModel*  listModel() const;                                                                              //!< Mod�le parent.
        //@}
    private:
        //! @name Gestion des donn�es
        //@{
virtual QImage      doImport(const QModelIndex& pIndex, Datum::Ptr pAdded) = 0;                                     //!< Import de fichier.
virtual void        doRemove(const QModelIndex& pIndex, Datum::Ptr pDeleted) = 0;                                   //!< Suppression de donn�es.
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(CategoryModel)
    };
}

#endif  // DATA_CATEGORYMODEL_HH
