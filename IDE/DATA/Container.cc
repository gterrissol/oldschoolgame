/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/DATA/Container.cc
    @brief M�thodes (non-inline) de la classe DATA::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>
#include <QTabWidget>

namespace DATA
{
#if 0

//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de g�om�trie.
        @version 0.25
     */
    class ImportExportGeometry : public ImportExport
    {
    public:

virtual         ~ImportExportGeometry();                                   //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader); //!<
    };


    /*! @brief Import/export de biblioth�que de mod�les.
        @version 0.25
     */
    class ImportExportModelLibrary : public ImportExport
    {
    public:

virtual         ~ImportExportModelLibrary();                               //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader); //!<
    };


//------------------------------------------------------------------------------
//                                   G�om�trie
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportGeometry::~ImportExportGeometry() { }


    /*!
     */
    bool ImportExportGeometry::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        I32 lFlags          = k0L;
        I32 lAutoFlags      = k0L;
        U16 lVertexCount    = k0UW;
        U16 lTriangleCount  = k0UW;
        U16 lSegmentCount   = k0UW;

        pLoader >> lFlags;
        pLoader >> lAutoFlags;
        pLoader >> lVertexCount;
        pLoader >> lTriangleCount;
        pLoader >> lSegmentCount;
        pLoader.rollback(U32(sizeof(lFlags)         +
                             sizeof(lAutoFlags)     +
                             sizeof(lVertexCount)   +
                             sizeof(lTriangleCount) +
                             sizeof(lSegmentCount)));

        Generic::exportHex(pWriter, pLoader, "Flags");
        Generic::exportHex(pWriter, pLoader, "Autoflags");
        Generic::exportU16(pWriter, pLoader, "Vertex count");
        Generic::exportU16(pWriter, pLoader, "Triangle count");
        Generic::exportU16(pWriter, pLoader, "Segment count");

        Generic::beginContainer(pWriter, pLoader, "Vertices");
        {
            for(U16 lVx = k0UW; lVx < lVertexCount; ++lVx)
            {
                Generic::beginContainer(pWriter, pLoader, "Vertex");
                {
                    if      ((lAutoFlags & GEO::eVertex3) == GEO::eVertex3)
                    {
                        Generic::beginContainer(pWriter, pLoader, "Vertex3");
                        {
                            Generic::exportF32(pWriter, pLoader, "X");
                            Generic::exportF32(pWriter, pLoader, "Y");
                            Generic::exportF32(pWriter, pLoader, "Z");
                        }
                        Generic::endContainer(pWriter, pLoader, "Vertex3");
                    }
                    else if ((lAutoFlags & GEO::eVertex4) == GEO::eVertex4)
                    {
                        Generic::beginContainer(pWriter, pLoader, "Vertex4");
                        {
                            Generic::exportF32(pWriter, pLoader, "X");
                            Generic::exportF32(pWriter, pLoader, "Y");
                            Generic::exportF32(pWriter, pLoader, "Z");
                            Generic::exportF32(pWriter, pLoader, "w");
                        }
                        Generic::endContainer(pWriter, pLoader, "Vertex4");
                    }
                    if      ((lAutoFlags & GEO::eNormal) == GEO::eNormal)
                    {
                        Generic::beginContainer(pWriter, pLoader, "Normal");
                        {
                            Generic::exportF32(pWriter, pLoader, "X");
                            Generic::exportF32(pWriter, pLoader, "Y");
                            Generic::exportF32(pWriter, pLoader, "Z");
                        }
                        Generic::endContainer(pWriter, pLoader, "Normal");
                    }
                    if      ((lAutoFlags & GEO::eRGBA) == GEO::eRGBA)
                    {
                        Generic::exportRGB(pWriter, pLoader, "Colors4");
                    }
                    else if ((lAutoFlags & GEO::eRGB) == GEO::eRGB)
                    {
                        Generic::exportRGB(pWriter, pLoader, "Colors3");
                    }
                    if      ((lAutoFlags & GEO::eTexCoord1) == GEO::eTexCoord1)
                    {
                        Generic::beginContainer(pWriter, pLoader, "TexCoord1");
                        {
                            Generic::exportF32(pWriter, pLoader, "U");
                            Generic::exportF32(pWriter, pLoader, "V");
                        }
                        Generic::endContainer(pWriter, pLoader, "TexCoords1");
                    }
                    if      ((lAutoFlags & GEO::eTexCoord2) == GEO::eTexCoord2)
                    {
                        Generic::beginContainer(pWriter, pLoader, "TexCoord2");
                        {
                            Generic::exportF32(pWriter, pLoader, "U");
                            Generic::exportF32(pWriter, pLoader, "V");
                        }
                        Generic::endContainer(pWriter, pLoader, "TexCoords2");
                    }
                    if      ((lAutoFlags & GEO::eSkinCoord) == GEO::eSkinCoord)
                    {
                        Generic::beginContainer(pWriter, pLoader, "SkinCoord");
                        {
                            Generic::exportU8(pWriter, pLoader, "WeightCount");
                            Generic::beginContainer(pWriter, pLoader, "Indices");
                            {
                                Generic::exportU8(pWriter, pLoader);
                                Generic::exportU8(pWriter, pLoader);
                                Generic::exportU8(pWriter, pLoader);
                            }
                            Generic::endContainer(pWriter, pLoader, "Indices");
                            Generic::beginContainer(pWriter, pLoader, "Weights");
                            {
                                Generic::exportF32(pWriter, pLoader);
                                Generic::exportF32(pWriter, pLoader);
                                Generic::exportF32(pWriter, pLoader);
                            }
                            Generic::endContainer(pWriter, pLoader, "Weights");
                        }
                        Generic::endContainer(pWriter, pLoader, "SkinCoord");
                    }
                    if      ((lAutoFlags & GEO::eFogCoord) == GEO::eFogCoord)
                    {
                        Generic::exportF32(pWriter, pLoader, "FogCoord");
                    }
                }
                Generic::endContainer(pWriter, pLoader, "Vertex");
            }
        }
        Generic::endContainer(pWriter, pLoader, "Vertices");

        if      ((lAutoFlags & GEO::eTriangles) == GEO::eTriangles)
        {
            Generic::beginContainer(pWriter, pLoader, "Triangles");
            {
                for(U16 lTr = k0UW; lTr < lTriangleCount; ++lTr)
                {
                    Generic::beginContainer(pWriter, pLoader, "Triangle");
                    {
                        Generic::exportU16(pWriter, pLoader, "A");
                        Generic::exportU16(pWriter, pLoader, "B");
                        Generic::exportU16(pWriter, pLoader, "C");
                    }
                    Generic::endContainer(pWriter, pLoader, "Triangle");
                }
                if  ((lAutoFlags & GEO::eTOM) == GEO::eTOM)
                {
                    Generic::beginContainer(pWriter, pLoader, "Substitutes");
                    {
                        for(U16 lS = k0UW; lS < lVertexCount; ++lS)
                        {
                            Generic::exportU16(pWriter, pLoader);
                        }
                    }
                    Generic::endContainer(pWriter, pLoader, "Substitutes");
                }
            }
            Generic::endContainer(pWriter, pLoader, "Triangles");
        }
        else if ((lAutoFlags & GEO::eLines) == GEO::eLines)
        {
            Generic::beginContainer(pWriter, pLoader, "Segments");
                for(U16 lSg = k0UW; lSg < lSegmentCount; ++lSg)
                {
                    Generic::beginContainer(pWriter, pLoader, "Segment");
                    {
                        Generic::exportU16(pWriter, pLoader, "A");
                        Generic::exportU16(pWriter, pLoader, "B");
                    }
                    Generic::endContainer(pWriter, pLoader, "Segment");
                }
            Generic::beginContainer(pWriter, pLoader, "Segments");
        }

        return true;
    }


//------------------------------------------------------------------------------
//                            Biblioth�que de Mod�les
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportModelLibrary::~ImportExportModelLibrary() { }


    /*!
     */
    bool ImportExportModelLibrary::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        U32 lCategoryCount  = k0UL;
        pLoader >> lCategoryCount;
        pLoader.rollback(U32(sizeof(lCategoryCount)));

        Generic::exportU32(pWriter, pLoader, "Category count");

        for(U32 lCategory = k0UL; lCategory < lCategoryCount; ++lCategory)
        {
            Generic::beginContainer(pWriter, pLoader, "Category");
            {
                Generic::exportString(pWriter, pLoader, "Name");

                U32 lModelCount  = k0UL;
                pLoader >> lModelCount;
                pLoader.rollback(U32(sizeof(lModelCount)));
                Generic::exportU32(pWriter, pLoader, "Model count");

                for(U32 lModel = k0UL; lModel < lModelCount; ++lModel)
                {
                    Generic::beginContainer(pWriter, pLoader, "Model");
                    {
                        Generic::exportString(pWriter, pLoader, "Name");
                        Generic::exportKey(pWriter, pLoader, "GeoKey");
                        Generic::exportU32(pWriter, pLoader, "Size");

                        U32 lMaterialCount  = k0UL;
                        pLoader >> lMaterialCount;
                        pLoader.rollback(U32(sizeof(lMaterialCount)));

                        Generic::exportU32(pWriter, pLoader, "Material Count");

                        Generic::exportMaterial(pWriter, pLoader);
                    }
                    Generic::endContainer(pWriter, pLoader, "Model");
                }
            }
            Generic::endContainer(pWriter, pLoader, "Category");
        }

        return true;
    }

#endif  // 0

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::Container.
        @version 0.2
     */
    class Container::ContainerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Container)
    public:
        ContainerPrivate(Container* pThat);

        QTabWidget* mDataTabs;
    };


    /*!
     */
    Container::ContainerPrivate::ContainerPrivate(Container* pThat)
        : q{pThat}
        , mDataTabs(nullptr)
    {
        mDataTabs = new QTabWidget(pThat);
        q->setCentralWidget(mDataTabs);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Container::Container(OSGi::Context* pContext)
        : CORE::Container("data", tr("Database"), QStringLiteral(":/ide/data/icons/Database"), pContext)
        , pthis{this}
    { }


    /*!
     */
    Container::~Container() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::addWidget(Widget pWidget)
    {
        Q_D(Container);

        int lIndex = d->mDataTabs->addTab(pWidget.get(), pWidget->windowTitle());
        if (dynamic_cast<CORE::Translated*>(pWidget.get()) != nullptr)
        {
            QWidget*    lW = pWidget.get();
            add([=]() { d->mDataTabs->setTabText(lIndex, lW->windowTitle()); });
        }
        pWidget.release();
    }


    /*!
     */
    QStringList Container::sortWidgets(const QStringList& pNames) const
    {
        auto lOrderedTabs = QString::fromStdString(context()->properties(BUNDLE_NAME)->get("tabs", "")).split(",") + pNames;
        lOrderedTabs.removeDuplicates();

        return lOrderedTabs;
    }
}
