add_ide_bundle(fr.osg.ide.data VERSION 1.2.0 SOURCES
                       Browser.ui                                                                Base.ui Viewer.ui
    DATA.hh Service.hh            DisplayImage.hh Store.hh CategoryModel.hh ListModel.hh List.hh Base.hh Viewer.hh Container.hh
    DATA.cc Service.cc            DisplayImage.cc Store.cc CategoryModel.cc ListModel.cc List.cc Base.cc Viewer.cc Container.cc
)

foreach(module QPAcKage FRAGment QGEOmetry)
    add_subdirectory(${module})
endforeach()
