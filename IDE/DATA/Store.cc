/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Store.hh"

/*! @file IDE/DATA/Store.cc
    @brief M�thodes (non-inline) de la classe DATA::Store.
    @author @ref Guillaume_Terrissol
    @date 16 D�cembre 2013 - 16 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDir>
#include <QFile>
#include <QImage>
#include <QMap>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

namespace DATA
{
//------------------------------------------------------------------------------
//                                Store : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::Store.
        @version 0.5
     */
    class Store::StorePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Store)
    public:
                StorePrivate(Store* pThat, QString pName);  //!< Constructeur.

        QString filename();                                 //!< Nom du fichier.
        QString dataFilename(QString pName);                //!< Nom d'un fichier de donn�es associ�.
        
        QString                     mName;                  //!< Nom de l'instance (et du fichier associ�).
        //! @name Fichier et chemin
        //@{
        QString                     mFilename;              //!< Nom du fichier d'associations.
        QString                     mDataFolder;            //!< R�pertoire des donn�es suppl�mentaires.
        //@}
        //! @name Donn�es
        //@{
        QMap<QString, QString>      mStrings;               //!< Dictionnaire de cha�nes.
        QMap<QString, QStringList>  mLists;                 //!< Dictionnaire de liste de cha�nes.
        QMap<QString, QString>      mImages;                //!< Dictionnaire d'images.
        //@}
        static const QString kListSplitter;                 //!< Marqueur de s�paration pour les listes.
    };

    const QString Store::StorePrivate::kListSplitter = QStringLiteral("\n");

    /*! @param pThat "Magasin" dont l'instance est le p-impl
        @param pName Nom du "magasin"
     */
    Store::StorePrivate::StorePrivate(Store* pThat, QString pName)
        : q{pThat}
        , mName{pName}
        , mFilename{}
        , mDataFolder{}
        , mStrings{}
        , mLists{}
        , mImages{}
    { }


    /*! @return Le nom du fichier d'associations (avec chemin)
        @note Le r�pertoire dans le lequel stocker le fichier est cr�� s'il n'existe pas
     */
    QString Store::StorePrivate::filename()
    {
        if (mFilename.isEmpty())
        {
            auto    lDir    = QDir{q->saveFolder()};

            if (!lDir.exists("./ide") && !lDir.mkdir("./ide"))
            {   // Le r�pertoire n'existe pas et n'a pu �tre cr�� : �chec.
                return {};
            }
        }

        return (mFilename = QStringLiteral("%1/ide/%2.xml").arg(q->saveFolder()).arg(mName));
    }


    /*! @param pName Fichier de donn�es suppl�mentaires
        @return Le nom du fichier avec le chemin
        @note Le r�pertoire dans le lequel stocker le fichier est cr�� s'il n'existe pas
     */
    QString Store::StorePrivate::dataFilename(QString pName)
    {
        if (mDataFolder.isEmpty())
        {
            auto    lDir        = QDir{q->saveFolder()};
            auto    lDataFolder = QStringLiteral("./ide/%1").arg(mName);
            if (!lDir.exists(lDataFolder) && !lDir.mkpath(lDataFolder))
            {   // Le r�pertoire n'existe pas et n'a pu �tre cr�� : �chec.
                return {};
            }
        }
        return (mDataFolder = QStringLiteral("%1/ide/%2/%3").arg(q->saveFolder()).arg(mName).arg(pName));
    }


//------------------------------------------------------------------------------
//                      Store : Constructeur & destructeur
//------------------------------------------------------------------------------

    /*! @param pName Nom du magasin.
     */
    Store::Store(QString pName)
        : pthis{this, pName}
    { }


    /*! Par d�faut.
     */
    Store::~Store() = default;


//------------------------------------------------------------------------------
//                            Store : Gestion du Nom
//------------------------------------------------------------------------------

    /*! @return Le nom du "magasin"
     */
    QString Store::name() const
    {
        return pthis->mName;
    }


    /*! @param pNewName Nouveau nom du "magasin"
        @note Le fichier et le r�pertoire des donn�es sont renomm�s
        @retval pName Si le renommage sur disque a r�ussi
        @retval name() en cas d'�chec
        @todo D�placement du r�pertoire s'il existe. Annulation du renommage en cas d'�chec
     */
    QString Store::rename(QString pNewName)
    {
        return pthis->mName = pNewName;
    }


//------------------------------------------------------------------------------
//                              Store : Permanence
//------------------------------------------------------------------------------

    /*! 
     */
    void Store::load()
    {
        // Nettoyage complet pour commencer.
        pthis->mStrings.clear();
        pthis->mLists.clear();
        pthis->mImages.clear();

        // La lecture se fait sans v�rification (on verra plus tard si j'en ajoute).
        QFile   lFile{pthis->filename()};
        if (lFile.open(QFile::ReadOnly))
        {
            static QMap<QString, std::function<void (QXmlStreamReader&, StorePrivate*)>> sFunctors =
            {
                { QStringLiteral("strings"), [](QXmlStreamReader&, StorePrivate*) { } },
                { QStringLiteral("lists"),   [](QXmlStreamReader&, StorePrivate*) { } },
                { QStringLiteral("images"),  [](QXmlStreamReader&, StorePrivate*) { } },
                { QStringLiteral("string"),  [](QXmlStreamReader& pReader, StorePrivate* pThis) {
                        auto    lName = pReader.attributes().value("name").toString();
                        if (!lName.isEmpty()) { pThis->mStrings[lName] = pReader.readElementText(); } } },
                { QStringLiteral("list"),    [](QXmlStreamReader& pReader, StorePrivate* pThis) {
                        auto    lName = pReader.attributes().value("name").toString();
                        if (!lName.isEmpty()) { pThis->mLists[lName] = pReader.readElementText().split(StorePrivate::kListSplitter); } } },
                { QStringLiteral("image"),   [](QXmlStreamReader& pReader, StorePrivate* pThis)  {
                        auto    lName = pReader.attributes().value("name").toString();
                        if (!lName.isEmpty()) { pThis->mImages[lName] = pReader.readElementText(); } } }
            };
            QXmlStreamReader    lReader{&lFile};

            while(!lReader.atEnd())
            {
                lReader.readNext();
                if      (lReader.isStartElement())
                {
                    auto    lIter = sFunctors.find(lReader.name().toString());
                    if (lIter != sFunctors.end())
                    {
                        lIter.value()(lReader, pthis.get());
                    }
                }
            }
        }
        else
        {
            fillAtCreation();
        }
    }


    /*! 
     */
    void Store::save()
    {
        QFile   lFile{pthis->filename()};
        if (lFile.open(QFile::WriteOnly))
        {
            QXmlStreamWriter    lWriter{&lFile};

            lWriter.setAutoFormatting(true);
            lWriter.setAutoFormattingIndent(2);
            lWriter.writeStartDocument();

            lWriter.writeStartElement("data");
            {
                lWriter.writeStartElement("strings");
                for(auto s : pthis->mStrings.keys())
                {
                    lWriter.writeStartElement("string");
                    lWriter.writeAttribute("name", s);
                    lWriter.writeCharacters(pthis->mStrings[s]);
                    lWriter.writeEndElement();
                }
                lWriter.writeEndElement();

                lWriter.writeStartElement("lists");
                for(auto l : pthis->mLists.keys())
                {
                    lWriter.writeStartElement("list");
                    lWriter.writeAttribute("name", l);
                    lWriter.writeCharacters(pthis->mLists[l].join(StorePrivate::kListSplitter));
                    lWriter.writeEndElement();
                }
                lWriter.writeEndElement();

                lWriter.writeStartElement("images");
                for(auto i : pthis->mImages.keys())
                {
                    lWriter.writeStartElement("image");
                    lWriter.writeAttribute("name", i);
                    lWriter.writeCharacters(pthis->mImages[i]);
                    lWriter.writeEndElement();
                }
                lWriter.writeEndElement();
            }
            lWriter.writeEndElement();

            lWriter.writeEndDocument();
        }
    }


//------------------------------------------------------------------------------
//                               Store : Interface
//------------------------------------------------------------------------------

    /*! 
     */
    void Store::set(QString pKey, QString pValue)
    {
        pthis->mStrings[pKey] = pValue;
    }


    /*! 
     */
    void Store::set(QString pKey, QStringList pValue)
    {
        pthis->mLists[pKey] = pValue;
    }


    /*! 
     */
    void Store::set(QString pKey, QImage pValue)
    {
        auto    lImageFilename = pthis->dataFilename(QStringLiteral("%1.png").arg(pKey));
        pValue.save(lImageFilename, "PNG", 0);
        pthis->mImages[pKey] = lImageFilename;
    }


    QString Store::string(QString pKey) const
    {
        auto    lValueIt = pthis->mStrings.find(pKey);
        if (lValueIt != pthis->mStrings.end())
        {
            return lValueIt.value();
        }
        else
        {
            return {};
        }
    }


    /*! 
     */
    QStringList Store::list(QString pKey) const
    {
        auto    lValueIt = pthis->mLists.find(pKey);
        if (lValueIt != pthis->mLists.end())
        {
            return lValueIt.value();
        }
        else
        {
            return {};
        }
    }


    /*! 
     */
    QImage Store::image(QString pKey) const
    {
        auto    lValueIt = pthis->mImages.find(pKey);
        if (lValueIt != pthis->mImages.end())
        {
            return QImage{lValueIt.value()};
        }
        else
        {
            return {};
        }
    }

//------------------------------------------------------------------------------
//                             Store : Autre M�thode
//------------------------------------------------------------------------------

    /*! Par d�faut, aucune donn�es initialies.
     */
    void Store::fillAtCreation() { }
}
