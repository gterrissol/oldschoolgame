/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_DISPLAYIMAGE_HH
#define DATA_DISPLAYIMAGE_HH

#include "DATA.hh"

/*! @file IDE/DATA/DisplayImage.hh
    @brief En-t�te de la classe DATA::DisplayImage.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 11 Novembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

class QLabel;

#include "CORE/Display.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Widget pour l'affichage d'images (fichiers externes).
        @version 0.5
     */
    class DisplayImage : public CORE::Display
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                DisplayImage(QWidget* pParent = nullptr);   //!< Constructeur.
virtual         ~DisplayImage();                            //!< Destructeur.
        //@}

    private:
        //! @name Rendu
        //@{
virtual void    render(QString pFilename) override;         //!< Rendu de la donn�e.
virtual void    reset() override;                           //!< Nettoyage de l'image affich�e.
        //@}
        QLabel* mImage;                                     //!< Label pour l'affichage de l'image.
    };
}

#endif  // DATA_DISPLAYIMAGE_HH
