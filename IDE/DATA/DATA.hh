/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_DATA_HH
#define DATA_DATA_HH

/*! @file IDE/DATA/DATA.hh
    @brief Pr�-d�clarations du module @ref DATA.
    @author @ref Guillaume_Terrissol
    @date 25 Septembre 2013 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace DATA  //!< Base de donn�es de l'IDE.
{
    // Composants d'IHM.
    class Base;
    class CategoryViewer;
    class Container;
    class DisplayImage;
    class List;
    class Viewer;

    // Services.
    class Service;

    // Autres.
    class CategoryModel;
    class ListModel;
    class Store;
}

#endif  // De DATA_DATA_HH
