<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Base</name>
    <message>
        <source>Base</source>
        <translation>Base</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Browser</source>
        <translation>Navigateur</translation>
    </message>
</context>
<context>
    <name>DATA::Base</name>
    <message>
        <source>Missing Viewer</source>
        <translation>Visualisateur manquant</translation>
    </message>
    <message>
        <source>Missing List Model</source>
        <translation>Modèle de liste manquant</translation>
    </message>
    <message>
        <source>Missing List widget</source>
        <translation>Widget de liste manquant</translation>
    </message>
</context>
<context>
    <name>DATA::CategoryModel</name>
    <message>
        <source>Failure</source>
        <translation>Echec</translation>
    </message>
    <message>
        <source>The operation failed.</source>
        <translation>L&apos;opération a échoué.</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vide</translation>
    </message>
</context>
<context>
    <name>DATA::Container</name>
    <message>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
</context>
<context>
    <name>DATA::List</name>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>&amp;Renommer</translation>
    </message>
</context>
<context>
    <name>DATA::ListModel</name>
    <message>
        <source>Failure</source>
        <translation>Echec</translation>
    </message>
    <message>
        <source>The operation failed.</source>
        <translation>L&apos;opération a échoué.</translation>
    </message>
    <message>
        <source>Invalid name</source>
        <translation>Nom invalide</translation>
    </message>
    <message>
        <source>Such a name already exist</source>
        <translation>Un tel nom existe déjà</translation>
    </message>
    <message>
        <source>Can&apos;t handle file %1</source>
        <translation>Ne peut traiter le fichier %1</translation>
    </message>
    <message>
        <source>Failed deletion</source>
        <translation>Echec de la suppression</translation>
    </message>
    <message>
        <source>Couldn&apos;t delete :</source>
        <translation>N&apos;a pu supprimer : </translation>
    </message>
</context>
<context>
    <name>DATA::Service</name>
    <message>
        <source>Missing %1</source>
        <oldsource>Missing</oldsource>
        <translation>%1 manquant</translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <source>View</source>
        <translation>Vue</translation>
    </message>
</context>
</TS>
