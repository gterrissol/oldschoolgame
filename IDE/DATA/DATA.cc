/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/DATA/DATA.cc
    @brief Enregistrement de l'activateur du module DATA.
    @author @ref Guillaume_Terrissol
    @date 25 Septembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"

#include "Container.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @page category_data Base de donn�es de l'IDE.
        @todo A r�diger
     */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("data.container", [=]() { return CORE::UIService::Widget{new Container(pContext)}; });

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->declare(CORE::Synchronizer("data.undo").enter("positive data.undo").leave("null data.undo"));
                    lSyncService->declare(CORE::Synchronizer("data.redo").enter("positive data.redo").leave("null data.redo"));
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("data.container");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(DATA::Activator)
OSGI_END_REGISTER_ACTIVATORS()
