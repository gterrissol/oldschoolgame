/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_SERVICE_HH
#define DATA_SERVICE_HH

#include "DATA.hh"

/*! @file IDE/DATA/Service.hh
    @brief En-t�te de la classe DATA::Service.
    @author @ref Guillaume_Terrissol
    @date 20 Ao�t 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

#include "CORE/UIService.hh"

namespace DATA
{
    /*! @brief Service associ� � un type d'une base de donn�es.
        @version 0.3

        Pour chaque cat�gorie de donn�es, cette classe doit �tre d�riv�e afin de fournir un ensemble d'informations.
     */
    class Service : public OSGi::Service
    {
    public:
        //! @name Pointeurs et alias
        //@{
        using Ptr     = std::shared_ptr<Service>;                   //!< Pointeur sur service.
        using Widget  = CORE::UIService::Widget;                    //!< Pointeur sur widget.
        using Aliases = std::map<QString, QString>;                 //!< Dictionnaire d'informations diverses.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Service(OSGi::Context* pContext, Aliases pAliases); //!< Constructeur.
virtual         ~Service();                                         //!< Destructeur.
        //@}
        //! @name Informations pour la gestion du type de donn�e
        //@{
        Widget  widget(QString pWhich) const;                       //!< Widgets..
        QString info(QString pWhich) const;                         //!< Autres informations.
        //@}

    private:
        //! @name Impl�mentation
        //@{
virtual QString dataTypeName() const = 0;                           //!< Nom du type de donn�e.
        //@}
        OSGi::Context*  mContext;                                   //!< Contexte d'ex�cution.
        Aliases         mAliases;                                   //!< Informations pour la gestion du type de donn�e.
    };
}

#endif  // DATA_SERVICE_HH
