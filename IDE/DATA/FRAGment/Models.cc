/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Models.hh"

/*! @file IDE/DATA/FRAGment/Models.cc
    @brief M�thodes (non-inline) des classes TILE::FragmentsListModel & TILE::FragmentsModel.
    @author @ref Guillaume_Terrissol
    @date 5 D�cembre - 27 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/GameService.hh"
#include "DATA/QPAcKage/FileService.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pContext Contexte d'ex�cution.
        @return Le gestionnaire de fragements de textures de tuiles.
     */
    FragmentMgr::Ptr fragmentMgr(OSGi::Context* pContext)
    {
        if (auto lGameService = pContext->services()->findByTypeAndName<CORE::GameService>("fr.osg.ide.game"))
        {
            if (auto lUniverse = lGameService->universe())
            {
                return lUniverse->fragments();
            }
        }
        return {};
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Contr�leur de gestionnaire de fragments de texture de tuile.
        @version 0.5
     */
    class FragmentMgrController
    {
    public:

        using Ptr = std::shared_ptr<FragmentMgrController>;
        //! @name Constructeurs & destructeur
        //@{
                        FragmentMgrController(FragmentMgr::Ptr   pFragmentMgr,
                                              OSGi::Context* pContext); //!< Constructeur.
virtual                 ~FragmentMgrController();                       //!< Destructeur.
        //@}
        //! @name M�thodes d'�dition
        //@{
        void            save();
        int             createTiling();                                 //!< Cr�ation d'une famille.
        Tiling::Ptr     tiling(int pId);                                //!< Groupe de fragments.
        CLR::RGBA       computeTilingColor(int pId);                    //!< Calcul de la couleur moyenne.
        void            removeTiling(int pId);                          //!< Suppression d'une famille.
        //@}
    private:

        FragmentMgr::Ptr    mEdited;                                    //!< Gestionnaire.
        OSGi::Context*      mContext;                                   //!< Contexte d'ex�cution.
        bool                mHasChangedSinceSaved;                      //!< Requ�te de sauvegarde.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    FragmentMgrController::FragmentMgrController(FragmentMgr::Ptr pFragmentMgr, OSGi::Context* pContext)
        : mEdited(pFragmentMgr)
        , mContext{pContext}
        , mHasChangedSinceSaved{false}
    { }


    //!
    FragmentMgrController::~FragmentMgrController() = default;


    //!
    void FragmentMgrController::save()
    {
        if (mHasChangedSinceSaved)
        {
            mEdited->save();
            mHasChangedSinceSaved = false;
        }
    }

    //!
    int FragmentMgrController::createTiling()
    {
        mHasChangedSinceSaved = true;
        return mEdited->createTiling();
    }

    //!
    Tiling::Ptr FragmentMgrController::tiling(int pId)
    {
        mHasChangedSinceSaved = true;
        return mEdited->tiling(Tiling::Id(pId));
    }

    //!
    CLR::RGBA FragmentMgrController::computeTilingColor(int pId)
    {
        return mEdited->tilingColor(Tiling::Id(pId));
    }

    //!
    void FragmentMgrController::removeTiling(int pId)
    {
        mHasChangedSinceSaved = true;
        mEdited->removeTiling(Tiling::Id(pId));
    }


//------------------------------------------------------------------------------
//                          FragmentsListModel : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::FragmentsListModel.
        @version 0.2
     */
    class FragmentsListModel::FragmentsListModelPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(FragmentsListModel)
    public:
        FragmentsListModelPrivate(FragmentsListModel* pThat);   //!< Constructeur.

        FragmentMgrController::Ptr  mController;                //!< Contr�leur du gestionnaire de fragments de texture de tuile.
    };


    /*! @param pThat Widget dont l'instance est le p-impl
     */
    FragmentsListModel::FragmentsListModelPrivate::FragmentsListModelPrivate(FragmentsListModel* pThat)
        : q{pThat}
        , mController{}
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pParent  La vue des cat�gories de fragments de texture de tuile.
        @param pContext Contexte d'ex�cution.
     */
    FragmentsListModel::FragmentsListModel(DATA::List* pParent, OSGi::Context* pContext)
        : DATA::ListModel{pParent, QStringLiteral("FragmentList"), pContext}
        , pthis{this}
    {
        setMaxRowCount(std::numeric_limits<Tiling::Id::TType>::max());
    }


    /*!
     */
    FragmentsListModel::~FragmentsListModel()
    { }


    /*!
     */
    QString FragmentsListModel::saveFolder() const
    {
        return context()->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }

    //!
    void FragmentsListModel::editNow()
    {
        DATA::ListModel::editNow();
        pthis->mController = std::make_shared<FragmentMgrController>(fragmentMgr(context()), context());
    }

    //!
    void FragmentsListModel::endEdit()
    {
        pthis->mController->save();
        pthis->mController.reset();
        DATA::ListModel::endEdit();
    }

    //!
    int FragmentsListModel::newCategoryId()
    {
        return pthis->mController->createTiling();
    }

    //!
    void FragmentsListModel::freeCategoryId(int pId)
    {
        pthis->mController->removeTiling(U32(pId));
    }

    /*! @param pName Nom de la cat�gorie de fragments � cr�er.
        @param pId   Identifiant de la cat�gorie.
        @return Une nouvelle cat�gorie nomm� @p pName.
     */
    DATA::CategoryModel::Ptr FragmentsListModel::makeCategory(QString pName, int pId)
    {
        return std::make_shared<FragmentsModel>(this, pName, pId);
    }

    /*! @return Le nom du type "cat�gorie de fragments de texture de tuile"
     */
    QString FragmentsListModel::categoryName() const
    {
        return tr("Tiling");
    }

    /*! @retval true Pour l'instant
     */
    bool FragmentsListModel::isRemovable(const QModelIndex&) const
    {
        return true;
    }


//------------------------------------------------------------------------------
//                            FragmentsModel : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::FragmentsModel.
        @version 0.2
     */
    class FragmentsModel::FragmentsModelPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(FragmentsModel)
    public:
        FragmentsModelPrivate(FragmentsModel* pThat);   //!< Constructeur.

        FragmentMgrController::Ptr  mController;        //!< Contr�leur du gestionnaire de fragments de texture de tuile.
    };


    /*! @param pThat Widget dont l'instance est le p-impl
     */
    FragmentsModel::FragmentsModelPrivate::FragmentsModelPrivate(FragmentsModel* pThat)
        : q{pThat}
        , mController{}
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pParent Mod�le du type de donn�e dont une cat�gorie va �tre g�r�e par une instance de ce mod�le
        @param pName   Nom de la cat�gorie
        @param pId     Identifiant de la cat�gorie
     */
    FragmentsModel::FragmentsModel(FragmentsListModel* pParent, QString pName, int pId)
        : DATA::CategoryModel{pParent, pName, pId}
        , pthis{this}
    { }


    /*!
     */
    FragmentsModel::~FragmentsModel() = default;


    /*!
     */
    Qt::ItemFlags FragmentsModel::flags(const QModelIndex& pIndex) const
    {
        auto f = CategoryModel::flags(pIndex);
        if (pIndex.isValid())
        {
            f |= Qt::ItemNeverHasChildren | Qt::ItemIsDropEnabled;
            f &= ~Qt::ItemIsUserCheckable;
        }
        return f;
    }


    /*! @return false
        @sa La doc de Qt
     */
    bool FragmentsModel::hasChildren(const QModelIndex&) const
    {
        return false;
    }

    /*!
     */
    QString FragmentsModel::saveFolder() const
    {
        return listModel()->context()->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }

    /*!
     */
    void FragmentsModel::editNow()
    {
        CategoryModel::editNow();

        auto    lContext = listModel()->context();
        pthis->mController = std::make_shared<FragmentMgrController>(fragmentMgr(lContext), lContext);
    }

    /*!
     */
    void FragmentsModel::endEdit()
    {
        CategoryModel::endEdit();

        // Lors de la fermeture du pak file, le mod�le parent sera d�sactiv� en arrivant ici : d�sactiver la sauvegarde permet de ne pas r��crire toutes
        // les donn�es du gestionnaire pour chaque cat�gorie : cela ne doit �tre fait que lors de la destruction (suppression) d'une cat�gorie.
        if (listModel()->isEnabled())
        {
            pthis->mController->save();
        }
        pthis->mController.reset();
    }

//------------------------------------------------------------------------------
//                            Chargement des Donn�es
//------------------------------------------------------------------------------

    /*! @brief Donn�es de cat�gorie pour les fragments.
        @version 0.5
     */
    class FragmentsModel::FragDatum : public CategoryModel::Datum
    {
    public:
        FragDatum(QString pFilename);
        FragDatum(QImage pImage);
virtual ~FragDatum();

        QImage  mImage;
    };

    //!
    FragmentsModel::FragDatum::FragDatum(QString pFilename)
            : mImage{pFilename}
        { }

    //!
    FragmentsModel::FragDatum::FragDatum(QImage pImage)
            : mImage(pImage)
        { }

    //!
    FragmentsModel::FragDatum::~FragDatum() = default;

    //!
    FragmentsModel::Datum::Ptr FragmentsModel::make(QString pFilename)
    {
        return std::make_shared<FragDatum>(pFilename);
    }

    //!
    FragmentsModel::Datum::Ptr FragmentsModel::take(const QModelIndex&)
    {
        auto    lTiling = pthis->mController->tiling(id());
        auto    lImage  = QImage(QSize(lTiling->width(), lTiling->height()), QImage::Format_RGB32);

        for(int j = 0; j < lImage.height(); ++j)
        {
            auto    lLine = lTiling->line(U32(j));
            for(int i = 0; i < lImage.width(); ++i)
            {
                lImage.setPixel(i, j, lLine[i]);
            }
        }

        return std::make_shared<FragDatum>(lImage);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    QImage FragmentsModel::doImport(const QModelIndex&, Datum::Ptr pAdded)
    {
        if (auto lAdded = std::dynamic_pointer_cast<FragDatum>(pAdded))
        {
            auto    lImage = lAdded->mImage.convertToFormat(QImage::Format_RGB32);
            auto    lTiling = pthis->mController->tiling(id());
            insertRow(rowCount());

            for(int j = 0; j < lImage.height(); ++j)
            {
                auto    lLine = lTiling->line(U32(j));
                for(int i = 0; i < lImage.width(); ++i)
                {
                    auto    lPixel = lImage.pixel(i, j);
                    lLine[i] = U32((0xFFUL << 24) + (qBlue(lPixel) << 16) + (qGreen(lPixel) << 8) + (qRed(lPixel) << 0));
                }
            }

            pthis->mController->computeTilingColor(id());

            return lAdded->mImage;
        }
        else
        {
            return {};
        }
    }


    /*!
     */
    void FragmentsModel::doRemove(const QModelIndex&, Datum::Ptr)
    {
        removeRow(0);   // Un seul �l�ment...
    }
}
