/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Masks.hh"

/*! @file IDE/DATA/FRAGment/Masks.cc
    @brief M�thodes (non-inline) des classes TILE::FragmentsListModel & TILE::FragmentsModel.
    @author @ref Guillaume_Terrissol
    @date 14 Ao�t 2015 - 17 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>
#include <functional>

#include <QColor>
#include <QImage>

#include "TILE/Enum.hh"

namespace TILE
{
    namespace
    {
        const float kMaskSizef = eMaskSize - 1;
    }

    using Func1 = float (*)(int);
    using Func2 = float (*)(int, int);

    float hard(int x)
    {
        float a = 1.0F - x / kMaskSizef;
        if (a < 0.5F)
        {
            return 0.0F;
        }
        else
        {
            return 1.0F;
        }
    }
    float soft(int x)
    {
        float a = 1.0F - x / kMaskSizef;
        if (a < 0.25F)
        {
            return 0.0F;
        }
        if (0.75F < a)
        {
            return 1.0F;
        }

        return 0.25F + a * 0.5F;
    }
    float smooth(int x)
    {
        return 1.0F - x / kMaskSizef;
    }

    float blurry(int x, int y)
    {
        static const int kValues[eMaskSize][eMaskSize] = {
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,      2,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,      1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,      1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,      2,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,      3,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,      2,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,      1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,  1,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  3,  2,      1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   3,  4,  4,  4,  4,  4,  3,  2,  3,  4,  4,  4,  4,  4,  4,  3,      2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   2,  3,  4,  4,  4,  3,  2,  1,  2,  3,  4,  4,  4,  3,  2,  4,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },

            {   1,  2,  3,  4,  3,  2,  1,  0,  1,  2,  3,  4,  3,  2,  1,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  1,  2,  3,  2,  1,  0,  0,  0,  1,  2,  3,  2,  1,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  1,  2,  1,  0,  0,  0,  0,  0,  1,  2,  1,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
            {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
        };
        return kValues[y][x] / 4.0F;
    }

    /*! Si les fragments des familles de fragments sont r�p�t�es sur les "bords" de la texture associ�es,
        les masques, eux, sont "prolong�s".
        @note Le code est loin d'�tre optimal, mais marche suffisamment bien pour �tre acceptable...
     */
    QImage extendMask(QImage pMask)
    {
        QImage      lExtend{QSize{2 * eMaskSize, 2 * eMaskSize}, QImage::Format_Alpha8};
        lExtend.fill(0);
        const int   lHalfSize = eMaskSize / 2;

        for(int j = 0; j < eMaskSize; ++j)
        {
            const auto* lSrc = pMask.constScanLine(j);
            auto*       lDst = lExtend.scanLine(lHalfSize + j);

            for(int i = 0; i < eMaskSize; ++i)
                lDst[lHalfSize + i] = lSrc[i];
            // C�t�s
            for(int i = 0; i < lHalfSize; ++i)
            {
                lDst[i] = lSrc[0];
                lDst[lHalfSize + eMaskSize + i] = lSrc[eMaskSize - 1];
            }
        }
        // Haut et bas.
        for(int j = 0; j < lHalfSize; ++j)
        {
            const auto* lSrcUp  = pMask.constScanLine(0);
            auto*       lDstUp  = lExtend.scanLine(j);
            const auto* lSrcBt  = pMask.constScanLine(eMaskSize - 1);
            auto*       lDstBt  = lExtend.scanLine(lHalfSize + eMaskSize - 1 + j);

            for(int i = 0; i < eMaskSize; ++i)
            {
                lDstUp[lHalfSize + i] = lSrcUp[i];
                lDstBt[lHalfSize + i] = lSrcBt[i];
            }
        }
        // Angles
        for(int j = 0; j < lHalfSize; ++j)
        {
            auto    lSrcTL = pMask.constScanLine(0)[0];
            auto*   lDstTL = lExtend.scanLine(j);
            auto    lSrcTR = pMask.constScanLine(0)[eMaskSize - 1];
            auto*   lDstTR = lExtend.scanLine(j);
            auto    lSrcBL = pMask.constScanLine(eMaskSize - 1)[0];
            auto*   lDstBL = lExtend.scanLine(lHalfSize + eMaskSize - 1 + j);
            auto    lSrcBR = pMask.constScanLine(eMaskSize - 1)[eMaskSize - 1];
            auto*   lDstBR = lExtend.scanLine(lHalfSize + eMaskSize - 1 + j);

            for(int i = 0; i < lHalfSize; ++i)
            {
                lDstTL[i] = lSrcTL;
                lDstTR[lHalfSize + eMaskSize + i] = lSrcTR;
                lDstBL[i] = lSrcBL;
                lDstBR[lHalfSize + eMaskSize + i] = lSrcBR;
            }
        }

        return lExtend;
    }

    QImage  makeMasks()
    {
        QImage  lMasks{QSize{eMaskSlotCount * eMaskSize, eMaskTypeCount * eMaskSize}, QImage::Format_Alpha8};

        static_assert(eMaskTypeCount == 4, "Invlaid mask type count");

        Func1 fs1[] = {
            &hard,
            &smooth,
            &soft
        };
        int lY = 0;
        for(auto f : fs1)
        {
            float   lM[eMaskSlotCount][eMaskSize][eMaskSize] = { };

            for(int j = 0; j < eMaskSize; ++j)
                for(int i = 0; i < eMaskSize; ++i)
                    lM[0][j][i] = lM[1][j][eMaskSize - 1 - i] = lM[2][eMaskSize - 1 - j][i] = lM[3][eMaskSize - 1 - j][eMaskSize - 1 - i] = f(i) * f(j);

            int lX = 0;
            for(int slot = 0; slot < eMaskSlotCount; ++slot, lX += eMaskSize)
            {
                for(int j = 0; j < eMaskSize; ++j)
                {
                    uchar*  lLine = lMasks.scanLine(lY + j);
                    for(int i = 0; i < eMaskSize; ++i)
                    {
                        lLine[lX + i] = static_cast<uchar>(255.0F * lM[slot][j][i]);
                    }
                }
            }
            lY += eMaskSize;
        }

        Func2 fs2[] = {
            &blurry
        };
        for(auto f : fs2)
        {
            float   lM[eMaskSlotCount][eMaskSize][eMaskSize] = { };

            for(int j = 0; j < eMaskSize; ++j)
                for(int i = 0; i < eMaskSize; ++i)
                {
                    float   lV = f(i, j);
                    lM[0][j][i] = lM[1][i][eMaskSize - 1 - j] = lM[2][eMaskSize - 1 - i][j] = lM[3][eMaskSize - 1 - j][eMaskSize - 1 - i] = lV;
                }

            int lX = 0;
            for(int slot = 0; slot < eMaskSlotCount; ++slot, lX += eMaskSize)
            {
                for(int j = 0; j < eMaskSize; ++j)
                {
                    uchar*  lLine = lMasks.scanLine(lY + j);
                    for(int i = 0; i < eMaskSize; ++i)
                    {
                        lLine[lX + i] = static_cast<uchar>(255.0F * lM[slot][j][i]);
                    }
                }
            }
        }

        QImage  lExtendedMasks{QSize{2 * eMaskSlotCount * eMaskSize, 2 * eMaskTypeCount * eMaskSize}, QImage::Format_Alpha8};
        lExtendedMasks.fill(0);

        for(int j = 0; j < eMaskTypeCount; ++j)
        {
            for(int i = 0; i < eMaskSlotCount; ++i)
            {
                auto    lExtend = extendMask(lMasks.copy(i * eMaskSize, j * eMaskSize, eMaskSize, eMaskSize));
                for(int y = 0; y < 2 * eMaskSize; ++y)
                {
                    const auto* lSrc = lExtend.constScanLine(y);
                    auto*       lDst = lExtendedMasks.scanLine(2 * j * eMaskSize + y);

                    for(int x = 0; x < 2 * eMaskSize; ++x)
                    {
                        lDst[2 * i * eMaskSize + x] = lSrc[x];
                    }
                }
            }
        }

        return lExtendedMasks;
    }
}
