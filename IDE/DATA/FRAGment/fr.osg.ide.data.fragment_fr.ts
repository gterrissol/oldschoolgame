<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Manager</name>
    <message>
        <source>Tile fragments</source>
        <translation>Fragments de tuile</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation>Environnement</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>New environment</source>
        <translation>Nouvel environnement</translation>
    </message>
</context>
<context>
    <name>TILE::FragmentsListModel</name>
    <message>
        <source>Tiling</source>
        <translation>Carrelage</translation>
    </message>
</context>
<context>
    <name>TILE::Service</name>
    <message>
        <source>Tile fragments</source>
        <translation>Fragments de tuile</translation>
    </message>
</context>
</TS>
