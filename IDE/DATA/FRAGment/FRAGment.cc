/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/DATA/FRAGment/FRAGment.cc
    @brief M�thodes (non-inline) des classes Data::Fragment, Data::FragmentActivator.
    @author @ref Guillaume_Terrissol
    @date 9 Juin 2013 - 27 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>
#include <QMenu>
#include <QMouseEvent>
#include <QPainter>
#include <QTableView>
#include <QWidget>
#include <QXmlStreamWriter>

#include "CoLoR/Colors.hh"
#include "CoLoR/RGBA.hh"
#include "CORE/Activator.hh"
#include "CORE/Container.hh"
#include "CORE/ProgressService.hh"
#include "CORE/UIService.hh"
#include "DATA/QPAcKage/FileDB.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "DATA/Base.hh"
#include "DATA/DisplayImage.hh"
#include "DATA/List.hh"
#include "DATA/Service.hh"
#include "DATA/Viewer.hh"
#include "PAcKage/LoaderSaver.hh"
#include "TILE/Fragment.hh"

#include "Manager.ui.hh"
#include "Models.hh"
#include "Masks.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de gestionnaire de fragments de texture de tuile.
        @version 0.25
     */
    class ImportExportFragmentMgr : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportFragmentMgr() = default;                               //!< Destructeur.


    private:

        bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
        void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


    /*! @todo A optimiser plus tard (sauvegarde partielle ?)
     */
    bool ImportExportFragmentMgr::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        U32 lTilingCount;
        pLoader >> lTilingCount;
        pLoader.rollback(U32{sizeof(lTilingCount)});

        pWriter.setAutoFormattingIndent(1);
        auto    lProgress   = data()->fileDB()->context()->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, lTilingCount);
        lProgress->advance(0);

        // Nombre de familles.
        data()->write<eU32>(pWriter, pLoader, "Tiling count");

        for(U32::TType lT = 0; lT < lTilingCount; ++lT)
        {
            data()->begin(pWriter, pLoader, QString("Tiling %1").arg(1 + lT, 4, 10, QChar(' ')));

                bool    lIsDefined;
                pLoader >> lIsDefined;
                pLoader.rollback(U32{sizeof(bool)});
                data()->write<eBool>(pWriter, pLoader, "Defined");
                if (lIsDefined)
                {
                    data()->begin(pWriter, pLoader, "Data");
                        for(size_t lT = 0; lT < (eFragmentsInTilingSide * eFragmentSize) * (eFragmentsInTilingSide * eFragmentSize); ++lT)
                        {
                            data()->write<eHex>(pWriter, pLoader);
                        }
                    data()->end(pWriter, pLoader, "Data");

                    data()->write<eRGBA>(pWriter, pLoader, "Color");
                }

            data()->end(pWriter, pLoader, "Tiling");
            lProgress->advance(lT);
        }

        U32 lMasksByteCount{eMaskSlotCount * eMaskSize * eMaskTypeCount * eMaskSize};

        data()->begin(pWriter, pLoader, "Masks");
            data()->write<eMask32>(pWriter, pLoader, "Format");
            data()->write<eU16>(pWriter, pLoader, "Width");
            data()->write<eU16>(pWriter, pLoader, "Height");
            data()->begin(pWriter, pLoader, "Data");
                for(U32 lM = k0UL; lM < lMasksByteCount; ++lM)
                {
                    data()->write<eU8>(pWriter, pLoader);
                }
            data()->end(pWriter, pLoader, "Data");
        data()->end(pWriter, pLoader, "Masks");

        lProgress->advance(lTilingCount);

        return true;
    }


    /*! Cr�e les donn�es d'un gestionnaire vide (pour la cr�ation d'un <b>pak</b> file).
     */
    void ImportExportFragmentMgr::doMake(PAK::Saver& pSaver)
    {
        /*/ "Tiling" par d�faut. A r�activer plus tard
        pSaver << U32{1};
        CLR::RGBA   lDefaultColor{CLR::kWhite};
        U32         lColor  = lDefaultColor;
        pSaver << true;
        for(size_t lT = 0; lT < (eFragmentsInTilingSide * eFragmentSize) * (eFragmentsInTilingSide * eFragmentSize); ++lT)
        {
            pSaver << lColor;
        }
        pSaver << lDefaultColor;/*/
        const int       kCount = 4;
        const QColor    kColors[kCount] = { Qt::black, Qt::red, Qt::green, Qt::blue };
        QImage          lImage{QSize{eFragmentsInTilingSide * eFragmentSize, eFragmentsInTilingSide * eFragmentSize}, QImage::Format_ARGB32};
        QPen            lPen1{QBrush{Qt::darkGray},  1.0};
        QPen            lPen2{QBrush{Qt::lightGray}, 1.0};

        pSaver << U32{kCount};
        for(int c = 0; c < kCount; ++c)
        {
            QColor col = Qt::white;
            switch(c) { case 1: col = QColor(0xFFC0C0); break; case 2: col = QColor(0xC0FFC0); break; case 3: col = QColor(0xC0C0FF); default: break; }
            lImage.fill(col);
            QPainter    lPainter(&lImage);
            QFont       lFont = lPainter.font();
            lFont.setPixelSize(3 * TILE::eFragmentSize / 4);
            lPainter.setFont(lFont);

            // G�n�re une image : grille 4x4, cases cern�es avec couleurs altern�es.
            // Nombres croissants dans les cases, color�es.
            for(int j = 0; j < TILE::eMaskTypeCount; ++j)
            {
                for(int i = 0; i < TILE::eMaskSlotCount; ++i)
                {
                    const auto lRect = QRect(i * TILE::eFragmentSize, j * TILE::eFragmentSize, TILE::eFragmentSize - 1, TILE::eFragmentSize - 1);
                    lPainter.setPen(((i + j) % 2 == 0) ? lPen1 : lPen2);
                    lPainter.drawRect(lRect);
                    lPainter.setPen(QPen{QBrush{kColors[c]}, 1.0});
                    lPainter.drawText(lRect, Qt::AlignCenter, QString::number(1 + j * TILE::eMaskSlotCount + i));
                }
            }
            lPainter.end();

            pSaver << true;
            for(int j = 0; j < lImage.height(); ++j)
            {
                for(int i = 0; i < lImage.width(); ++i)
                {
                    auto    lPixel = lImage.pixel(i, j);
                    pSaver << U32((0xFFUL << 24) + (qBlue(lPixel) << 16) + (qGreen(lPixel) << 8) + (qRed(lPixel) << 0));
                }
            }
            pSaver << ((c == 0) ? CLR::RGBA{CLR::kWhite} : CLR::RGBA{F32(kColors[c].redF()), F32(kColors[c].greenF()), F32(kColors[c].blueF()), F32{1.0F}});
        }//*/

        const auto  lMasks      = makeMasks();
        uint32_t    lByteCount  = lMasks.byteCount();
        const auto* lTexels     = lMasks.bits();
        pSaver << U32{MAT::eR | MAT::e8Bits};
        pSaver << U16(lMasks.width());
        pSaver << U16(lMasks.height());
        for(uint32_t lB = 0; lB < lByteCount; ++lB)
        {
            pSaver << U8{lTexels[lB]};
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Service : public DATA::Service
    {
    public:
        //! @name Constructeur & destructeur
        Service(OSGi::Context* pContext);
virtual ~Service();
        //@}
        //! @name Impl�mentation
        //@{
    protected:

        bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

        std::string typeName() const override;                              //!< Nom du type (r�el).

        QString     dataTypeName() const override;                          //!< Nom du type de donn�e.
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Service::Service(OSGi::Context* pContext)
        : DATA::Service(pContext, Aliases{ { "list",    "fragments.categories" }
                                         , { "viewer",  "fragments.viewer" }
                                         , { "sync",    "file + enabled" }
                                         , { "filter",  pContext->properties(BUNDLE_NAME)->get("fragments.filter", "").c_str() }
                                         , { "preview", "fragments.preview" }
                                         })
    { }


    /*!
     */
    Service::~Service() { }


    /*!
     */
    bool Service::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || DATA::Service::isType(pTypeName);
    }


    /*!
     */
    std::string Service::typeName() const
    {
        return typeid(*this).name();
    }


    /*!
     */
    QString Service::dataTypeName() const
    {
        return QCoreApplication::translate("TILE::Service", "Tile fragments");
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Vue sous forme de table.
        @version 0.5
     */
    class FragmentsTable : public QTableView
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                FragmentsTable(QWidget* pParent = nullptr); //!< Constructeur.
virtual         ~FragmentsTable();                          //!< Destructeur.
        //@}
        //! @name Taille des cellules
        //@{
        int     sizeHintForColumn(int) const override;      //!< ... en largeur.
        int     sizeHintForRow(int) const override;         //!< ... en hauteur.
        //@
    protected:

        void    keyPressEvent(QKeyEvent* pEvent) override;  //!< Action clavier.
        void    updateGeometries() override;                //!< Mise � jour de la g�om�trie.
    };


    /*! @param pParent Widget parent
     */
    FragmentsTable::FragmentsTable(QWidget* pParent)
        : QTableView{pParent}
    {
        setMinimumSize(QSize{eFragmentsInTilingSide * eFragmentSize, eFragmentsInTilingSide * eFragmentSize});
        horizontalHeader()->hide();
        verticalHeader()->hide();
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setDragEnabled(false);
    }


    /*! Par d�faut.
     */
    FragmentsTable::~FragmentsTable() = default;


    /*! @return La largeur attendue pour les snapshots
     */
    int FragmentsTable::sizeHintForColumn(int) const
    {
        return eFragmentsInTilingSide * eFragmentSize;
    }


    /*! @return La hauteur attendue pour les snapshots
     */
    int FragmentsTable::sizeHintForRow(int) const
    {
        return eFragmentsInTilingSide * eFragmentSize;
    }


    /*! Un seul �v�nement est g�r� pour le moment : la suppression de fragments par appui sur @b DEL .
        @param pEvent Ev�nement � g�rer
     */
    void FragmentsTable::keyPressEvent(QKeyEvent* pEvent)
    {
        QTableView::keyPressEvent(pEvent);

        if (pEvent->key() == Qt::Key_Delete)
        {
            if (selectionModel()->hasSelection())
            {
                for(auto lIndex : selectionModel()->selectedIndexes())
                {
                    model()->setData(lIndex, QPixmap{}, Qt::DecorationRole);
                }
            }
        }
    }


    /*! Cette m�thode est r�impl�ment�e afin de redimensionner correctement les cellules.
     */
    void FragmentsTable::updateGeometries()
    {
        QTableView::updateGeometries();
        resizeRowsToContents();
        resizeColumnsToContents();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Afficheur sp�cialis� pour les fragments de texture de tuile.
        @version 0.5
     */
    class Tilings : public DATA::Viewer, public CORE::Synchronized
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Tilings();                                    //!< Constructeur.
virtual         ~Tilings();                                   //!< Destructeur.
        //@}
    protected:

        QModelIndexList selection() override;                           //!< 

    private:

        void    manage(DATA::CategoryModel* pCurrent) override; //!< Gestion d'une cat�gorie.

        QTableView* mTable;                                     //!< Affichage des fragments.
    };


    /*! Par d�faut.
     */
    Tilings::Tilings()
        : DATA::Viewer()
        , CORE::Synchronized{"enabled + file"}
        , mTable{}
    {
        mTable = new FragmentsTable{this};
        auto    lL  = new QGridLayout{this};
        lL->addWidget(mTable);
    }


    /*! Par d�faut.
     */
    Tilings::~Tilings() = default;


    /*!
     */
    QModelIndexList Tilings::selection()
    {
        return {};
    }


    /*! @param pCurrent Cat�gorie dont afficher les �l�ments
     */
    void Tilings::manage(DATA::CategoryModel* pCurrent)
    {
        mTable->setModel(pCurrent);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class FragmentActivator : public CORE::IDEActivator
    {
    public:
                FragmentActivator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~FragmentActivator() { }

    private:

        void    checkInComponents(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkIn("fr.osg.ide.data.fragments", std::make_shared<Service>(pContext));

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("data.fragments", [=]() { return CORE::UIService::Widget{new DATA::Base{pContext, "fragments"}}; });
                    lUIService->checkIn("fragments.categories", [=]()
                    {
                        auto lList = std::make_unique<DATA::List>();
                        (new FragmentsListModel{lList.get(), pContext})->setMaxRowCount(pContext->properties(BUNDLE_NAME)->getInteger("fragments.list_max_size", 64));
                        return lList;
                    });
                    lUIService->checkIn("fragments.viewer", [=]()
                    {
                        return std::make_unique<Tilings>();
                    });
                    lUIService->checkIn("fragments.preview", [=]() { return std::make_unique<DATA::DisplayImage>(); });

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportFragmentMgr>("ttm", lExtensionDir);
                }
        void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("data.fragments");
                    lUIService->checkOut("fragments.categories");
                    lUIService->checkOut("fragments.view");

                    pContext->services()->checkOut("fr.osg.ide.data.fragments");

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("ttm");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(TILE::FragmentActivator)
OSGI_END_REGISTER_ACTIVATORS()
