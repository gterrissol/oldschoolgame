/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef FRAGMENTS_MODELS_HH
#define FRAGMENTS_MODELS_HH

/*! @file IDE/DATA/FRAGment/Models.hh
    @brief En-t�te des classes TILE::FragmentsListModel & TILE::FragmentsModel.
    @author @ref Guillaume_Terrissol
    @date 5 D�cembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QPixmap>
#include <QVector>

#include "CORE/Private.hh"
#include "DATA/CategoryModel.hh"
#include "DATA/ListModel.hh"

namespace TILE
{
//------------------------------------------------------------------------------
//           Mod�les pour la Gestion des Fragments de Texture de Tuile
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour la liste des fragments de texture de tuile.
        @version 0.4
     */
    class FragmentsListModel : public DATA::ListModel
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                                    FragmentsListModel(DATA::List* pParent, OSGi::Context* pContext);   //!< Constructeur.
virtual                             ~FragmentsListModel();                                              //!< Destructeur.
        //@}
    protected:

        QString                     saveFolder() const override;                                        //!< R�pertoire de sauvegarde.
        void                        editNow() override;                                                 //!< Activation du mod�le.
        void                        endEdit() override;                                                 //!< D�sactivation du mod�le.

    private:
        //! @name Cat�gories
        //@{
        int                         newCategoryId() override;                                           //!< Cr�ation d'un Id pour une nouvelle cat�gorie.
        void                        freeCategoryId(int pId) override;                                   //!< Suppression d'une cat�gorie.
        DATA::CategoryModel::Ptr    makeCategory(QString pName, int pId) override;                      //!< Cr�ation d'une nouvelle cat�gorie.
        QString                     categoryName() const override;                                      //!< Nom du type de cat�gorie.
        bool                        isRemovable(const QModelIndex&) const override;                     //!< Possibilit� de suppression.
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(FragmentsListModel)
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour une cat�gorie de fragments de texture de tuile.
        @version 0.4

        Les fragments sont affich�s sous la forme d'une grille.
     */
    class FragmentsModel : public DATA::CategoryModel
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                        FragmentsModel(FragmentsListModel* pParent, QString pName, int pId);                            //!< Constructeur.
virtual                 ~FragmentsModel();                                                                              //!< Destructeur.
        //@}
        //! @name Contenu des items
        //@{
        Qt::ItemFlags   flags(const QModelIndex& pIndex) const override;                                                //!< Propri�t�s des items.
        bool            hasChildren(const QModelIndex&) const override;                                                 //!< Des enfants ?
        //@}
    protected:

        QString         saveFolder() const override;                                                                    //!< R�pertoire de sauvegarde.
        void            editNow() override;                                                                             //!< Activation du mod�le.
        void            endEdit() override;                                                                             //!< D�sactivation du mod�le.
        //! @name Chargement des donn�es
        //@{
        class FragDatum;
        Datum::Ptr      make(QString pFilename) override;                                                               //!< Chargement externe.
        Datum::Ptr      take(const QModelIndex& pIndex) override;                                                       //!< Chargement interne.
        //@}
    private:

        QImage          doImport(const QModelIndex& pIndex, Datum::Ptr pAdded) override;                                //!< Import de fichier.
        void            doRemove(const QModelIndex& pIndex, Datum::Ptr pDeleted) override;                              //!< Suppression de donn�es.

        Q_CUSTOM_DECLARE_PRIVATE(FragmentsModel)
    };
}

#endif  // FRAGMENTS_MODELS_HH
