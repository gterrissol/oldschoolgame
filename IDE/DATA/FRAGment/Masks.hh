/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef FRAGMENTS_MASKS_HH
#define FRAGMENTS_MASKS_HH

/*! @file IDE/DATA/FRAGment/Masks.hh
    @brief En-t�te des classes TILE::FragmentsListModel & TILE::FragmentsModel.
    @author @ref Guillaume_Terrissol
    @date 14 Ao�t 2015 - 14 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

class QImage;

namespace TILE
{
    QImage  makeMasks();
}

#endif  // FRAGMENTS_MASKS_HH
