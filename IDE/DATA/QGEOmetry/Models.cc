/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Models.hh"

/*! @file IDE/DATA/QGEOmetry/Models.cc
    @brief M�thodes (non-inline) des classes QGEO::Models, QGEO::Activator.
    @author @ref Guillaume_Terrissol
    @date 1er Juillet 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QPixmap>

#include "DATA/QPAcKage/FileService.hh"

namespace QGEO
{
//------------------------------------------------------------------------------
//                             M�thodes des Mod�les
//------------------------------------------------------------------------------

    /*! @param pContext Contexte d'ex�cution.
        @param pParent  La vue des cat�gories de g�om�tries.
     */
    GeometriesListModel::GeometriesListModel(DATA::List* pParent, OSGi::Context* pContext)
        : DATA::ListModel{pParent, QStringLiteral("GeometryList"), pContext}
    { }


    /*!
     */
    QString GeometriesListModel::saveFolder() const
    {
        return context()->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }

    //!
    int GeometriesListModel::newCategoryId()
    {
        return -1;
    }

    //!
    void GeometriesListModel::freeCategoryId(int) { }

    /*! @param pName Nom de la cat�gorie de g�om�trie � cr�er.
        @param pId   Identifiant de la cat�gorie.
        @return Une nouvelle cat�gorie nomm� @p pName.
     */
    DATA::CategoryModel::Ptr GeometriesListModel::makeCategory(QString pName, int pId)
    {
        auto    lPtr = std::make_shared<GeometriesModel>(this, pName, pId);
        lPtr->setMaxRowCount(32);
        return lPtr;
    }

    /*! @return Le nom du type "cat�gorie de g�om�tries"
     */
    QString GeometriesListModel::categoryName() const
    {
        return tr("Models bank");
    }

    /*! @retval true Pour l'instant
     */
    bool GeometriesListModel::isRemovable(const QModelIndex&) const
    {
        return true;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-impl de GeometriesListModel.
        @version 0.5
     */
    class GeometriesModel::GeometriesModelPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(GeometriesModel)
    public:

        GeometriesModelPrivate(GeometriesModel* pThat); //!< Constructeur.
    };


    /*!
     */
    GeometriesModel::GeometriesModelPrivate::GeometriesModelPrivate(GeometriesModel* pThat)
        : q{pThat}
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pParent Mod�le du type de donn�e dont une cat�gorie va �tre g�r�e par une instance de ce mod�le.
        @param pName   Nom de la cat�gorie.
        @param pId     Identifiant de la cat�gorie.
     */
    GeometriesModel::GeometriesModel(GeometriesListModel* pParent, QString pName, int pId)
        : DATA::CategoryModel{pParent, pName, pId}
        , pthis{this}
    { }


    //!
    GeometriesModel::~GeometriesModel() = default;


    /*! 
     */
    Qt::ItemFlags GeometriesModel::flags(const QModelIndex& pIndex) const
    {
        return CategoryModel::flags(pIndex);
    }


    /*!
     */
    QString GeometriesModel::saveFolder() const
    {
        return listModel()->context()->services()->findByTypeAndName<QPAK::FileService>("fr.osg.ide.pak.file")->root();
    }


//------------------------------------------------------------------------------
//                            Chargement des Donn�es
//------------------------------------------------------------------------------

    //!
    GeometriesModel::Datum::Ptr GeometriesModel::make(QString /*pFilename*/)
    {
        return {};
    }

    //!
    GeometriesModel::Datum::Ptr GeometriesModel::take(const QModelIndex& /*pIndex*/)
    {
        /// Construit un Datum � partir des donn�es du contr�leur de fragments.
        return {};
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QImage GeometriesModel::doImport(const QModelIndex& /*pIndex*/, Datum::Ptr /*pAdded*/)
    {
        return {};
    }


    /*!
     */
    void GeometriesModel::doRemove(const QModelIndex& /*pIndex*/, Datum::Ptr /*pDeleted*/)
    {
    }
}
