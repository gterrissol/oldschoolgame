/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/DATA/QGEOmetry/QGEOmetry.cc
    @brief Enregistrement de l'activateur du module QGEOmetry.
    @author @ref Guillaume_Terrissol
    @date 8 Juin 2014 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/Display.hh"
#include "CORE/UIService.hh"
#include "DATA/Base.hh"
#include "DATA/List.hh"
#include "DATA/Service.hh"
#include "DATA/Viewer.hh"
#include "DATA/QPAcKage/FileService.hh"
#include "DATA/QPAcKage/ImportExport.hh"
#include "DATA/QPAcKage/ImportExportService.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "PAcKage/LoaderSaver.hh"

#include "Models.hh"
#include "Models.ui.hh"

namespace QGEO
{
//------------------------------------------------------------------------------
//                                 Import/Export
//------------------------------------------------------------------------------

    /*! @brief Import/export de g�om�trie.
        @version 0.25
     */
    class ImportExportGeometry : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportGeometry();                                            //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


    /*! @brief Import/export de biblioth�que de mod�les.
        @version 0.25
     */
    class ImportExportModelLibrary : public QPAK::ImportExport
    {
    public:

virtual         ~ImportExportModelLibrary();                                        //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


//------------------------------------------------------------------------------
//                                   G�om�trie
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportGeometry::~ImportExportGeometry() { }


    /*!
     */
    bool ImportExportGeometry::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        I32 lFlags          = k0L;
        I32 lAutoFlags      = k0L;
        U16 lVertexCount    = k0UW;
        U16 lTriangleCount  = k0UW;
        U16 lSegmentCount   = k0UW;

        pLoader >> lFlags;
        pLoader >> lAutoFlags;
        pLoader >> lVertexCount;
        pLoader >> lTriangleCount;
        pLoader >> lSegmentCount;
        pLoader.rollback(U32(sizeof(lFlags)         +
                             sizeof(lAutoFlags)     +
                             sizeof(lVertexCount)   +
                             sizeof(lTriangleCount) +
                             sizeof(lSegmentCount)));

        data()->write<eHex>(pWriter, pLoader, "Flags");
        data()->write<eHex>(pWriter, pLoader, "Autoflags");
        data()->write<eU16>(pWriter, pLoader, "Vertex count");
        data()->write<eU16>(pWriter, pLoader, "Triangle count");
        data()->write<eU16>(pWriter, pLoader, "Segment count");

        data()->begin(pWriter, pLoader, "Vertices");
        {
            for(U16 lVx = k0UW; lVx < lVertexCount; ++lVx)
            {
                data()->begin(pWriter, pLoader, "Vertex");
                {
                    if      ((lAutoFlags & GEO::eVertex3) == GEO::eVertex3)
                    {
                        data()->begin(pWriter, pLoader, "Vertex3");
                        {
                            data()->write<eF32>(pWriter, pLoader, "X");
                            data()->write<eF32>(pWriter, pLoader, "Y");
                            data()->write<eF32>(pWriter, pLoader, "Z");
                        }
                        data()->end(pWriter, pLoader, "Vertex3");
                    }
                    else if ((lAutoFlags & GEO::eVertex4) == GEO::eVertex4)
                    {
                        data()->begin(pWriter, pLoader, "Vertex4");
                        {
                            data()->write<eF32>(pWriter, pLoader, "X");
                            data()->write<eF32>(pWriter, pLoader, "Y");
                            data()->write<eF32>(pWriter, pLoader, "Z");
                            data()->write<eF32>(pWriter, pLoader, "w");
                        }
                        data()->end(pWriter, pLoader, "Vertex4");
                    }
                    if      ((lAutoFlags & GEO::eNormal) == GEO::eNormal)
                    {
                        data()->begin(pWriter, pLoader, "Normal");
                        {
                            data()->write<eF32>(pWriter, pLoader, "X");
                            data()->write<eF32>(pWriter, pLoader, "Y");
                            data()->write<eF32>(pWriter, pLoader, "Z");
                        }
                        data()->end(pWriter, pLoader, "Normal");
                    }
                    if      ((lAutoFlags & GEO::eRGBA) == GEO::eRGBA)
                    {
                        data()->write<eRGB>(pWriter, pLoader, "Colors4");
                    }
                    else if ((lAutoFlags & GEO::eRGB) == GEO::eRGB)
                    {
                        data()->write<eRGB>(pWriter, pLoader, "Colors3");
                    }
                    if      ((lAutoFlags & GEO::eTexCoord0) == GEO::eTexCoord0)
                    {
                        data()->begin(pWriter, pLoader, "TexCoord0");
                        {
                            data()->write<eF32>(pWriter, pLoader, "U");
                            data()->write<eF32>(pWriter, pLoader, "V");
                        }
                        data()->end(pWriter, pLoader, "TexCoords0");
                    }
                    if      ((lAutoFlags & GEO::eTexCoord1) == GEO::eTexCoord1)
                    {
                        data()->begin(pWriter, pLoader, "TexCoord1");
                        {
                            data()->write<eF32>(pWriter, pLoader, "U");
                            data()->write<eF32>(pWriter, pLoader, "V");
                        }
                        data()->end(pWriter, pLoader, "TexCoords1");
                    }
                    if      ((lAutoFlags & GEO::eSkinCoord) == GEO::eSkinCoord)
                    {
                        data()->begin(pWriter, pLoader, "SkinCoord");
                        {
                            data()->write<eU8>(pWriter, pLoader, "WeightCount");
                            data()->begin(pWriter, pLoader, "Indices");
                            {
                                data()->write<eU8>(pWriter, pLoader);
                                data()->write<eU8>(pWriter, pLoader);
                                data()->write<eU8>(pWriter, pLoader);
                            }
                            data()->end(pWriter, pLoader, "Indices");
                            data()->begin(pWriter, pLoader, "Weights");
                            {
                                data()->write<eF32>(pWriter, pLoader);
                                data()->write<eF32>(pWriter, pLoader);
                                data()->write<eF32>(pWriter, pLoader);
                            }
                            data()->end(pWriter, pLoader, "Weights");
                        }
                        data()->end(pWriter, pLoader, "SkinCoord");
                    }
                    if      ((lAutoFlags & GEO::eFogCoord) == GEO::eFogCoord)
                    {
                        data()->write<eF32>(pWriter, pLoader, "FogCoord");
                    }
                }
                data()->end(pWriter, pLoader, "Vertex");
            }
        }
        data()->end(pWriter, pLoader, "Vertices");

        if      ((lAutoFlags & GEO::eTriangles) == GEO::eTriangles)
        {
            data()->begin(pWriter, pLoader, "Triangles");
            {
                for(U16 lTr = k0UW; lTr < lTriangleCount; ++lTr)
                {
                    data()->begin(pWriter, pLoader, "Triangle");
                    {
                        data()->write<eU16>(pWriter, pLoader, "A");
                        data()->write<eU16>(pWriter, pLoader, "B");
                        data()->write<eU16>(pWriter, pLoader, "C");
                    }
                    data()->end(pWriter, pLoader, "Triangle");
                }
                if  ((lAutoFlags & GEO::eTOM) == GEO::eTOM)
                {
                    data()->begin(pWriter, pLoader, "Substitutes");
                    {
                        for(U16 lS = k0UW; lS < lVertexCount; ++lS)
                        {
                            data()->write<eU16>(pWriter, pLoader);
                        }
                    }
                    data()->end(pWriter, pLoader, "Substitutes");
                }
            }
            data()->end(pWriter, pLoader, "Triangles");
        }
        else if ((lAutoFlags & GEO::eLines) == GEO::eLines)
        {
            data()->begin(pWriter, pLoader, "Segments");
                for(U16 lSg = k0UW; lSg < lSegmentCount; ++lSg)
                {
                    data()->begin(pWriter, pLoader, "Segment");
                    {
                        data()->write<eU16>(pWriter, pLoader, "A");
                        data()->write<eU16>(pWriter, pLoader, "B");
                    }
                    data()->end(pWriter, pLoader, "Segment");
                }
            data()->begin(pWriter, pLoader, "Segments");
        }

        return true;
    }


    /*!
     */
    void ImportExportGeometry::doMake(PAK::Saver& pSaver)
    {
        // Par d�faut, aucune donn�e (tous les compteurs � 0).
        pSaver << k0L;
        pSaver << k0L;
        pSaver << k0UW;
        pSaver << k0UW;
        pSaver << k0UW;
    }


//------------------------------------------------------------------------------
//                            Biblioth�que de Mod�les
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportModelLibrary::~ImportExportModelLibrary() { }


    /*!
     */
    bool ImportExportModelLibrary::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        U32 lCategoryCount  = k0UL;
        pLoader >> lCategoryCount;
        pLoader.rollback(U32(sizeof(lCategoryCount)));

        data()->write<eU32>(pWriter, pLoader, "Category count");

        for(U32 lCategory = k0UL; lCategory < lCategoryCount; ++lCategory)
        {
            data()->begin(pWriter, pLoader, "Category");
            {
                data()->write<eString>(pWriter, pLoader, "Name");

                U32 lModelCount  = k0UL;
                pLoader >> lModelCount;
                pLoader.rollback(U32(sizeof(lModelCount)));
                data()->write<eU32>(pWriter, pLoader, "Model count");

                for(U32 lModel = k0UL; lModel < lModelCount; ++lModel)
                {
                    data()->begin(pWriter, pLoader, "Model");
                    {
                        data()->write<eString>(pWriter, pLoader, "Name");
                        data()->write<eKey>(pWriter, pLoader, "GeoKey");
                        data()->write<eU32>(pWriter, pLoader, "Size");

                        U32 lMaterialCount  = k0UL;
                        pLoader >> lMaterialCount;
                        pLoader.rollback(U32(sizeof(lMaterialCount)));

                        data()->write<eU32>(pWriter, pLoader, "Material Count");

                        data()->write<eMaterial>(pWriter, pLoader);
                    }
                    data()->end(pWriter, pLoader, "Model");
                }
            }
            data()->end(pWriter, pLoader, "Category");
        }

        return true;
    }


    /*!
     */
    void ImportExportModelLibrary::doMake(PAK::Saver& pSaver)
    {
        // Par d�faut, aucune cat�gorie.
        pSaver << k0UL;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Service : public DATA::Service
    {
    public:
        //! @name Constructeur & destructeur
        Service(OSGi::Context* pContext);
virtual ~Service();
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).

virtual QString     dataTypeName() const override;                          //!< Nom du type de donn�e.
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Service::Service(OSGi::Context* pContext)
        : DATA::Service(pContext, Aliases{ { "list",    "models.categories" }
                                         , { "viewer",  "models.viewer" }
                                         , { "sync",    "file + enabled" }
                                         , { "filter",  pContext->properties(BUNDLE_NAME)->get("models.filter", "").c_str() }
                                         , { "preview", "models.preview" }
                                         })
    { }


    /*!
     */
    Service::~Service() { }


    /*!
     */
    bool Service::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || DATA::Service::isType(pTypeName);
    }


    /*!
     */
    std::string Service::typeName() const
    {
        return typeid(*this).name();
    }


    /*!
     */
    QString Service::dataTypeName() const
    {
        return QCoreApplication::translate("QGEO::Service", "Models library");
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Models : public DATA::CategoryViewer, public CORE::Translate<Models>, public CORE::Synchronized
    {
    public:
                Models();
virtual         ~Models();


    private:

        auto    buildTranslater() -> CORE::Translated::Updater override;

        Ui::ModelLibrary    mUI;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Models::Models()
        : DATA::CategoryViewer{}
        , CORE::Translate<Models>()
        , CORE::Synchronized{"enabled + file"}
        , mUI{}
    {
        if (auto lElementFrame = findChild<QFrame*>("elementFrame"))
        {
            from(lElementFrame);
            mUI.setupUi(lElementFrame);
        }
    }


    /*!
     */
    Models::~Models() { }


    /*!
     */
    CORE::Translated::Updater Models::buildTranslater()
    {
        return [=]()
        {
            if (auto lElementFrame = findChild<QFrame*>("elementFrame"))
            {
                mUI.retranslateUi(lElementFrame);
            }
        };
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Pr�visualisation de g�om�tries.
        @version 0.5
     */
    class DisplayModel : public CORE::Display
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                DisplayModel(QWidget* pParent = nullptr);   //!< Constructeur.
virtual         ~DisplayModel();                            //!< Destructeur.
        //@}
    private:
        //! @name Rendu
        //@{
virtual void    render(QString pFilename) override;         //!< Rendu de la donn�e.
virtual void    reset() override;                           //!< Nettoyage de l'image affich�e.
        //@}
        int                 mNodeId;                        //!< Id du noeud � rendre.
    };


    /*! @param pParent Widget parent
     */
    DisplayModel::DisplayModel(QWidget* pParent)
        : Display{pParent}
        , mNodeId{}
    {
        // Cr�er widget de rendu et l'ajouter au layout.
    }


    /*! Un peu de nettoyage.
     */
    DisplayModel::~DisplayModel()
    {
        DisplayModel::reset();
    }


    /*!
     */
    void DisplayModel::render(QString /*pFilename*/)
    {
        clear();

        // Charger et afficher le mod�le...
    }


    /*!
     */
    void DisplayModel::reset()
    {
        // Nettoyage.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkIn("fr.osg.ide.data.models", std::make_shared<Service>(pContext));

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("data.models", [=]() { return CORE::UIService::Widget{new DATA::Base{pContext, "models"}}; });
                    lUIService->checkIn("models.categories", [=]()
                    {
                        auto lList = std::make_unique<DATA::List>();
                        (new GeometriesListModel{lList.get(), pContext})->setMaxRowCount(pContext->properties(BUNDLE_NAME)->getInteger("models.list_max_size", 64));
                        return lList;
                    });
                    lUIService->checkIn("models.viewer", [=]() { return CORE::UIService::Widget{new Models{}}; });
                    lUIService->checkIn("models.preview", [=]() { return CORE::UIService::Widget{new QGEO::DisplayModel{}}; });

                    auto    lIEService      = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    auto    lExtensionDir   = lIEService->extensionDir(BUNDLE_NAME, pContext);
                    lIEService->checkIn<ImportExportGeometry>(    "geo", lExtensionDir);
                    lIEService->checkIn<ImportExportModelLibrary>("mdl", lExtensionDir);
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkOut("fr.osg.ide.data.models");

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("models.preview");
                    lUIService->checkOut("models.viewer");
                    lUIService->checkOut("models.categories");
                    lUIService->checkOut("data.models");

                    auto    lIEService = pContext->services()->findByTypeAndName<QPAK::ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("geo");
                    lIEService->checkOut("mdl");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QGEO::Activator)
OSGI_END_REGISTER_ACTIVATORS()
