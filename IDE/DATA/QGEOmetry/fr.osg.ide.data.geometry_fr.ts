<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ModelLibrary</name>
    <message>
        <source>Models library</source>
        <translation>Bibliothèque de modèles</translation>
    </message>
    <message>
        <source>triangles</source>
        <translation>triangles</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>octets</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Lumière</translation>
    </message>
    <message>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <source>New material</source>
        <translation>Nouveau matériau</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
</context>
<context>
    <name>QGEO::GeometriesListModel</name>
    <message>
        <source>Models bank</source>
        <translation>Banque de modèles</translation>
    </message>
</context>
<context>
    <name>QGEO::Service</name>
    <message>
        <source>Models library</source>
        <translation>Bibliothèque de modèles</translation>
    </message>
</context>
</TS>
