/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QGEOMETRY_MODELS_HH
#define QGEOMETRY_MODELS_HH

/*! @file IDE/DATA/QGEOmetry/Models.hh
    @brief En-t�te des classes QGEO::GeometriesListModel & QGEO::GeometriesModel.
    @author @ref Guillaume_Terrissol
    @date 8 Juin 2014 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DATA/ListModel.hh"

namespace QGEO
{
//------------------------------------------------------------------------------
//                    Mod�les pour la Gestion des G�om�tries
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour la liste des g�om�tries (d'un type donn� : mod�les, LEGO, etc).
        @version 0.5
     */
    class GeometriesListModel : public DATA::ListModel
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                                    GeometriesListModel(DATA::List* pParent, OSGi::Context* pContext);  //!< Constructeur.
virtual                             ~GeometriesListModel() = default;                                   //!< Destructeur.
        //@}
    protected:

        QString                     saveFolder() const override;                                        //!< R�pertoire de sauvegarde.

    private:
        //! @name Cat�gories
        //@{
        int                         newCategoryId() override;                                           //!< Cr�ation d'un Id pour une nouvelle cat�gorie.
        void                        freeCategoryId(int pId) override;                                   //!< Suppression d'une cat�gorie.
        DATA::CategoryModel::Ptr    makeCategory(QString pName, int pId) override;                      //!< Cr�ation d'une nouvelle cat�gorie.
        QString                     categoryName() const override;                                      //!< Nom du type de cat�gorie.
        bool                        isRemovable(const QModelIndex&) const override;                     //!< Possibilit� de suppression.
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour une cat�gorie de g�om�tries.
        @version 0.5

        C'est une premi�re version du mod�le, o� l'import dans les donn�es de jeu n'est pas encore effectif.
     */
    class GeometriesModel : public DATA::CategoryModel
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                        GeometriesModel(GeometriesListModel* pParent, QString pName, int pId);                          //!< Constructeur.
virtual                 ~GeometriesModel();                                                                             //!< Destructeur.
        //@}
        //! @name Contenu des items
        //@{
        Qt::ItemFlags   flags(const QModelIndex& pIndex) const override;                                                //!< Propri�t�s des items.
        //@}
    protected:

        QString         saveFolder() const override;                                                                    //!< R�pertoire de sauvegarde.
        //! @name Chargement des donn�es
        //@{
        Datum::Ptr      make(QString pFilename) override;                                                               //!< Chargement externe.
        Datum::Ptr      take(const QModelIndex& pIndex) override;                                                       //!< Chargement interne.
        //@}
    private:

        QImage          doImport(const QModelIndex& pIndex, Datum::Ptr pAdded) override;                                //!< Import de fichier.
        void            doRemove(const QModelIndex& pIndex, Datum::Ptr pDeleted) override;                              //!< Suppression de donn�es.

        Q_CUSTOM_DECLARE_PRIVATE(GeometriesModel)
    };
}

#endif  // QGEOMETRY_MODELS_HH
