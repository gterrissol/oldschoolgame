/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "List.hh"

/*! @file IDE/DATA/List.cc
    @brief M�thodes (non-inline) de la classe DATA::List.
    @author @ref Guillaume_Terrissol
    @date 10 Novembre 2013 - 11 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMenu>
#include <QMessageBox>
#include <QMimeData>

#include "ListModel.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Par d�faut.
     */
    List::List()
        : QListView{}
    {
        setDragDropMode(QAbstractItemView::DragDrop);
        setMovement(QListView::Snap);
        setDropIndicatorShown(true);

        setContextMenuPolicy(Qt::CustomContextMenu);
        setSelectionMode(ExtendedSelection);
        //setEditTriggers(AllEditTriggers);

        connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(callContextMenu(const QPoint&)));
    }


    /*! Par d�faut.
     */
    List::~List() = default;


//------------------------------------------------------------------------------
//                             D�finition du Mod�le
//------------------------------------------------------------------------------

    /*! @param pModel Nouveau mod�le pour la liste (doit �tre d'une classe d�riv�e de DATA::ListModel)
     */
    void List::setModel(QAbstractItemModel* pModel)
    {
        QListView::setModel(mModel = dynamic_cast<ListModel*>(pModel));
        Q_ASSERT_X(mModel != nullptr, "void List::setModel(QAbstractItemModel* pModel)", "Invalid model");
        mModel->setSelectionModel(selectionModel());

        connect(selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(handleSelectionChange()));
    }


//------------------------------------------------------------------------------
//                            Gestion des Cat�gories
//------------------------------------------------------------------------------

    /*! Ajoute une cat�gorie en fin de liste, s'il reste un emplacement disponible (voir ListModel::setMaxRowCount()).
     */
    void List::insertCategory()
    {
        qobject_cast<ListModel*>(model())->addCategory();
        emit contentChanged();
    }


    /*! Supprime toutes les cat�gories s�lectionn�es, du moins, celles qui peuvent l'�tre : si des donn�es sont d�j� utilis�es dans le
        moteur, cela ne sera pas possible (voir ListModel::mayRemove()).
     */
    void List::deleteCategory()
    {
        qobject_cast<ListModel*>(model())->delCategories();
        emit contentChanged();
    }


    /*! Active l'�dition du nom de la cat�gorie s�lectionn�e.
     */
    void List::renameCategory()
    {
        edit(currentIndex());
        emit contentChanged();
    }


//------------------------------------------------------------------------------
//                                Autres Gestions
//------------------------------------------------------------------------------

    /*! Un seul �v�nement est g�r� pour le moment : la suppression de cat�gories par appui sur @b DEL .
        @param pEvent Ev�nement � g�rer
     */
    void List::keyPressEvent(QKeyEvent* pEvent)
    {
        QListView::keyPressEvent(pEvent);

        if (pEvent->key() == Qt::Key_Delete)
        {
            deleteCategory();
        }
    }


    /*! Rafra�chit les composants IHM concern�s par le changement de s�lection.
     */
    void List::handleSelectionChange()
    {
        auto lSelection = selectionModel()->selectedIndexes();

        emit selectionChange(!lSelection.isEmpty());
        if (lSelection.count() == 1)
        {
            auto    lName   = mModel->data(lSelection.first()).toString();
            emit selected(mModel->category(lName));
        }
        else
        {
            emit selected(nullptr);
        }
    }


    /*! Affiche un menu contextuel, et ex�cute l'action choisie
        @param pPos Position du menu
     */
    void List::callContextMenu(const QPoint& pPos)
    {
        QMenu   lPopup;
        if (true)   // Encore de la place pour de nouvelles cat�gories
        {
            lPopup.addAction(tr("&New"), this, SLOT(insertCategory()));
        }
        if (selectionModel()->hasSelection())
        {
            lPopup.addAction(tr("&Delete"), this, SLOT(deleteCategory()));

            if (selectionModel()->selection().count() == 1)
            {
                lPopup.addAction(tr("&Rename"), this, SLOT(renameCategory()));
            }
        }

        if (!lPopup.isEmpty())
        {
            lPopup.exec(mapToGlobal(pPos));
        }
    }
}
