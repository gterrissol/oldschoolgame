/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "CategoryModel.hh"

/*! @file IDE/DATA/CategoryModel.cc
    @brief M�thodes (non-inline) de la classe DATA::CategoryModel.
    @author @ref Guillaume_Terrissol
    @date 20 Novembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMessageBox>
#include <QStringList>

#include "CORE/SyncService.hh"
#include "EDITion/Attributes.hh"
#include "EDITion/Controller.hh"
#include "EDITion/Observer.hh"
#include "EDITion/Subject.hh"

#include "ListModel.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    const QSize CategoryModel::kSnapshotSize{96, 96};


//------------------------------------------------------------------------------
//                            CategoryModel : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::CategoryModel.
        @version 0.5
     */
    class CategoryModel::CategoryModelPrivate : public EDIT::GenericController<CategoryModel>, public EDIT::Observer
    {
        Q_CUSTOM_DECLARE_PUBLIC(CategoryModel)
    public:

        using ElementIdx = std::pair<CategoryModel::Datum::Ptr, QModelIndex>;

                        CategoryModelPrivate(CategoryModel* pThat,
                                             OSGi::Context* pContext,
                                             int            pId);       //!< Constructeur.
        //! @name M�thodes d'�dition
        //@{
        bool            import(ElementIdx pAdd);                        //!< 
        bool            remove(ElementIdx pDel);                        //!< 
        //@}
        //! @name Gestion du sujet
        //{
        void            initSubject();                                  //!< 
        void            freeSubject();                                  //!< 

    protected:

        EDIT::Subject*  asSubject() override;                           //!< 

    private:

        void            listen(const EDIT::Answer* pAnswer) override;   //!< Ecoute.

        class Subject;
        std::unique_ptr<Subject>    mSubject;                           //!< Sujet d'�dition.
        //@}
        QMap<QModelIndex, QImage>   mSnapshots;                         //!< Pr�visualisation des �l�ments.
        int                         mId;                                //!< Identifiant de la cat�gorie.
        int                         mMaxRowCount;                       //!< Nombre maximum de lignes.
        int                         mRowCount;                          //!< Nombre actuel de lignes.
        int                         mMaxColumnCount;                    //!< Nombre maximum de colonnes.
        int                         mColumnCount;                       //!< Nombre actuel de colonnes.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Sujet d'�dition pour CategoryModel.
        @version 0.5
     */
    class CategoryModel::CategoryModelPrivate::Subject
        : public EDIT::Subject
        , public EDIT::Container<ElementIdx>
    {
    public:
                Subject(CategoryModelPrivate* pParent);
virtual         ~Subject();

        void    sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo) override;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pThat    Mod�le dont l'instance est le p-impl.
        @param pContext Contexte d'ex�cution.
        @param pId      Identifiant de la cat�gorie.
     */
    CategoryModel::CategoryModelPrivate::CategoryModelPrivate(CategoryModel* pThat, OSGi::Context* pContext, int pId)
        : EDIT::GenericController<CategoryModel>{pThat, pContext}
        , q{pThat}
        , mSubject{}
        , mSnapshots{}
        , mId{pId}
        , mMaxRowCount{1}
        , mRowCount{0}
        , mMaxColumnCount{1}
        , mColumnCount{1}
    { }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    bool CategoryModel::CategoryModelPrivate::import(ElementIdx pAdd)
    {
        auto    lIndex    = pAdd.second;
        auto    lSnapshot = q->doImport(lIndex, pAdd.first);

        if (!lSnapshot.isNull())
        {
            q->setData(lIndex, QPixmap::fromImage(lSnapshot), Qt::DecorationRole);

            return true;
        }

        return false;
    }


    /*!
     */
    bool CategoryModel::CategoryModelPrivate::remove(ElementIdx pDel)
    {
        q->doRemove(pDel.second, pDel.first);
        q->setData(pDel.second, QVariant{}, Qt::DecorationRole);

        return true;
    }


//------------------------------------------------------------------------------
//                               Gestion du Sujet
//------------------------------------------------------------------------------

    //!
    void CategoryModel::CategoryModelPrivate::initSubject()
    {
        mSubject = std::make_unique<Subject>(this);
        watch(mSubject.get());
    }

    //!
    void CategoryModel::CategoryModelPrivate::freeSubject()
    {
        watch(nullptr);
        mSubject.reset();
    }

    //!
    EDIT::Subject* CategoryModel::CategoryModelPrivate::asSubject()
    {
        return mSubject.get();
    }

    //!
    void CategoryModel::CategoryModelPrivate::listen(const EDIT::Answer* pAnswer)
    {
        if (dynamic_cast<const EDIT::GenericAnswer<ElementIdx>*>(pAnswer))
        {
            q->refresh();
        }
        else
        {
            QMessageBox::warning(nullptr, tr("Failure"), tr("The operation failed."));
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    CategoryModel::CategoryModelPrivate::Subject::Subject(CategoryModelPrivate* pParent)
        : EDIT::Subject{"data"}
        , INITIALIZE_CONTAINER(import, remove)
    { }

    //!
    CategoryModel::CategoryModelPrivate::Subject::~Subject() = default;

    //!
    void CategoryModel::CategoryModelPrivate::Subject::sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
    {
        notify(pAnswer, pUndo);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param pParent Mod�le du type de donn�e dont une cat�gorie va �tre g�r�e par une instance de ce mod�le
        @param pName   Nom de la cat�gorie
        @param pId     Identifiant de la cat�gorie
     */
    CategoryModel::CategoryModel(QAbstractItemModel* pParent, QString pName, int pId)
        : QAbstractItemModel{pParent}
        , Store{pName}
        , pthis{this, dynamic_cast<ListModel*>(pParent)->context(), pId}
    {
        dynamic_cast<ListModel*>(pParent)->context()->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->bind(this, "file+edition+hardware");
    }


    //! Par d�faut.
    CategoryModel::~CategoryModel() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    int CategoryModel::id() const
    {
        return pthis->mId;
    }


//------------------------------------------------------------------------------
//                CategoryModel : Nombre de Lignes et de Colonnes
//------------------------------------------------------------------------------

    /*! @param pCount Nombre maximum de lignes d'�l�ments que cette cat�gorie peut g�rer.
     */
    void CategoryModel::setMaxRowCount(int pCount)
    {
        pthis->mMaxRowCount = pCount;
    }


    /*! @return Le nombre maximum de lignes d'�l�ments que cette cat�gorie peut g�rer.
     */
    int CategoryModel::maxRowCount() const
    {
        return pthis->mMaxRowCount;
    }

    /*! 
     */
    bool CategoryModel::insertRows(int pRow, int pCount, const QModelIndex&)
    {
        // Ins�re seulement � la fin.
        if ((pRow == pthis->mRowCount) && (pthis->mRowCount + pCount <= pthis->mMaxRowCount))
        {
            beginInsertRows(QModelIndex(), pRow, pRow + pCount - 1);
            for(int r = pRow; r < pRow + pCount; ++r)
            {
                for(int c = 0; c < pthis->mColumnCount; ++c)
                {
                    pthis->mSnapshots.insert(createIndex(r, c), QImage{});
                }
            }
            pthis->mRowCount += pCount;
            endInsertRows();

            return true;
        }
        else
        {
            return false;
        }
    }

    /*! 
     */
    bool CategoryModel::removeRows(int pRow, int pCount, const QModelIndex& pParent)
    {
        if (pRow + pCount == pthis->mRowCount)
        {
            beginRemoveRows(pParent, pRow, pRow + pCount - 1);
            for(int r = pRow; r < pRow + pCount; ++r)
            {
                for(int c = 0; c < pthis->mColumnCount; ++c)
                {
                    pthis->mSnapshots.take(createIndex(r, c));
                }
            }
            pthis->mRowCount -= pCount;
            endRemoveRows();

            return true;
        }
        else
        {
            return false;
        }
    }


    /*! @return Le nombre de lignes d'�lements.
     */
    int CategoryModel::rowCount(const QModelIndex&) const
    {
        return pthis->mRowCount;
    }

    /*! @param pCount Nombre maximum de colonnes d'�l�ments que cette cat�gorie peut g�rer.
     */
    void CategoryModel::setMaxColumnCount(int pCount)
    {
        pthis->mMaxColumnCount = pCount;
    }


    /*! @return Le nombre maximum de colonnes d'�l�ments que cette cat�gorie peut g�rer.
     */
    int CategoryModel::maxColumnCount() const
    {
        return pthis->mMaxColumnCount;
    }

    /*!
     */
    bool CategoryModel::insertColumns(int pColumn, int pCount, const QModelIndex& pParent)
    {
        if ((pColumn == pthis->mColumnCount) && (pColumn + pCount <= pthis->mMaxColumnCount))
        {
            beginInsertColumns(pParent, pColumn, pColumn + pCount);
            for(int c = pColumn; c < pColumn + pCount; ++c)
            {
                for(int r = 0; r < pthis->mRowCount; ++r)
                {
                    pthis->mSnapshots.insert(createIndex(r, c), QImage{});
                }
            }
            pthis->mColumnCount += pCount;
            endInsertColumns();

            return true;
        }
        else
        {
            return false;
        }
    }

    /*!
     */
    bool CategoryModel::removeColumns(int pColumn, int pCount, const QModelIndex& pParent)
    {
        if (pColumn + pCount == pthis->mColumnCount)
        {
            beginRemoveColumns(pParent, pColumn, pColumn + pCount - 1);
            for(int c = pColumn; c < pColumn + pCount; ++c)
            {
                for(int r = 0; r < pthis->mRowCount; ++r)
                {
                    pthis->mSnapshots.take(createIndex(r, c));
                }
            }
            pthis->mColumnCount -= pCount;
            endRemoveColumns();

            return true;
        }
        else
        {
            return false;
        }
    }

    /*! @return Le nombre de colonnes d'�lements.
     */
    int CategoryModel::columnCount(const QModelIndex&) const
    {
        return pthis->mColumnCount;
    }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! @param pFilenames Fichiers dont importer les donn�es
        @retval true Si l'import a r�ussi
        @retval false En cas d'erreur
     */
    bool CategoryModel::import(QStringList pFilenames)
    {
        int lCol = 0;
        int lRow = 0;
        for(auto f : pFilenames)
        {
            QModelIndex lIndex{};
            QVariant    lData{};

            for(; !lIndex.isValid() && lData.isNull() && lRow < maxRowCount(); ++lRow, lCol = 0)
            {
                for(; !lIndex.isValid() && lData.isNull() && lCol < maxColumnCount(); ++lCol)
                {
                    lIndex = createIndex(lRow, lCol);
                    lData = data(lIndex, Qt::DecorationRole);
                }
            }

            if (lIndex.isValid() && lData.isNull())
            {
                EDIT::InsertRequest<CategoryModelPrivate::ElementIdx>   lAddTiling{{make(f), lIndex}};
                EDIT::SubjectList{EDIT::Controller::subject(pthis.get())}.perform(&lAddTiling);
            }
        }
        return true;    // TODO: return false si tout est en �chec.
    }


    /*!
     */
    void CategoryModel::remove(QModelIndexList pIndexes)
    {
        for(auto i : pIndexes)
        {
            EDIT::RemoveRequest<CategoryModelPrivate::ElementIdx>   lDelTiling{{take(i), i}};
            EDIT::SubjectList{EDIT::Controller::subject(pthis.get())}.perform(&lDelTiling);
        }
    }


//------------------------------------------------------------------------------
//                       Impl�mentation Minimale du Mod�le
//------------------------------------------------------------------------------

    /*! @param pRow    Ligne de l'item
        @param pColumn Colonne de l'item
        @param pParent Parent de l'item
        @return Un index pour l'item sp�cifi� par les arguments
     */
    QModelIndex CategoryModel::index(int pRow, int pColumn, const QModelIndex& pParent) const
    {
        return hasIndex(pRow, pColumn, pParent) ? createIndex(pRow, pColumn) : QModelIndex{};
    }


    /*! @return Un index invalide
     */
    QModelIndex CategoryModel::parent(const QModelIndex&) const
    {
        return {};
    }


    /*!
     */
    QVariant CategoryModel::data(const QModelIndex& pIndex, int pRole) const
    {
        if (pIndex.isValid())
        {
            auto    lSnapshot = pthis->mSnapshots.find(pIndex);
            if (lSnapshot != pthis->mSnapshots.end())
            {
                if      (pRole == Qt::DecorationRole)
                {
                    return *lSnapshot;
                }
            }
            else
            {
                if (pRole == Qt::DisplayRole)
                {
                    return tr("Empty");
                }
            }
        }

        return {};
    }


    /*!
     */
    bool CategoryModel::setData(const QModelIndex& pIndex, const QVariant& pValue, int pRole)
    {
        if (pIndex.isValid() && (pRole == Qt::DecorationRole))
        {
            auto    lSnapshot = pValue.value<QImage>();
            if (!lSnapshot.isNull())
            {
                pthis->mSnapshots[pIndex] = lSnapshot;
            }
            else
            {
                pthis->mSnapshots.remove(pIndex);
            }
            emit dataChanged(pIndex, pIndex);
        }

        return false;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    CategoryModel::Datum::~Datum() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void CategoryModel::setEnabled(bool pEnabled)
    {
        if (pEnabled)
        {
            pthis->initSubject();
            load();
            editNow();
        }
        else
        {
            endEdit();
            save();
            pthis->freeSubject();
        }
    }


    /*!
     */
    void CategoryModel::editNow()
    {
        pthis->mRowCount    = string("rowCount").toInt();
        pthis->mColumnCount = string("columnCount").toInt();
        auto    lSnapshots  = list("snapshots");
        QRegExp lRE(R"(Snapshot (\d+) (\d+))");
        for(auto n : lSnapshots)
        {
            if (lRE.exactMatch(n))
            {
                auto    lIndex = createIndex(lRE.cap(1).toInt(), lRE.cap(2).toInt());
                setData(lIndex, image(n), Qt::DecorationRole);
            }
        }
    }


    /*!
     */
    void CategoryModel::endEdit()
    {
        set("rowCount",    QString::number(pthis->mRowCount));
        set("columnCount", QString::number(pthis->mColumnCount));
        QStringList lSnapshots;
        for(const auto& i : pthis->mSnapshots.keys())
        {
            auto    lName = QStringLiteral("Snapshot %1 %2").arg(i.column()).arg(i.row());
            lSnapshots << lName;
            set(lName, pthis->mSnapshots[i]);
        }
        set("snapshots", lSnapshots);
    }


    //!
    void CategoryModel::fillAtCreation()
    {
        set("rowCount",    "0");
        set("columnCount", "1");
    }


//------------------------------------------------------------------------------
//                                      UI
//------------------------------------------------------------------------------


    /*!
     */
    void CategoryModel::refresh()
    {
        emit dataChanged(index(0, 0), index(maxColumnCount(), maxRowCount()));  // On rafra�chit tout.
//        pthis->mWidget->selectionModel()->clearSelection();                   // Et plus de s�lection.
    }

//------------------------------------------------------------------------------
//                            Acc�s au Mod�le Parent
//------------------------------------------------------------------------------

    /*! @return Le parent (au sens QObject) du mod�le (qui est un mod�le de liste)
     */
    ListModel* CategoryModel::listModel() const
    {
        return dynamic_cast<ListModel*>(QAbstractItemModel::parent());
    }
}
