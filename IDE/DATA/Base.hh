/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_BASE_HH
#define DATA_BASE_HH

#include "DATA.hh"

/*! @file IDE/DATA/Base.hh
    @brief En-t�te de la classe DATA::Base.
    @author @ref Guillaume_Terrissol
    @date 20 Ao�t 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

#include <OSGi/Context.hh>

#include "CORE/Private.hh"
#include "CORE/Synchronize.hh"
#include "CORE/Translate.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief IHM pour un type de donn�e de la BD.
        @version 0.3

        Dans l'onget "Base de donn�es", les diff�rents types de donn�es sont affich�s en utilisant le m�me type de pr�sentation :
        - � gauche, une liste affiche les cat�gories d�j� d�finies pour le type de donn�es (un widget n�cessairement d�riv� de List),
        - � droite, un composant sp�cifique, permettant un affichage plus d�taill� de la famille s�lectionn�e (e.g. CategoryViewer).
        Ces widgets � utiliser sont d�finis via leur nom, et construits par CORE::UIService.@n
        Leur nom sont r�cup�r� via un service d�di�, nomm�, par convention, "fr.osg.ide.data.<nom de base>" (voir @ref Service).
     */
    class Base : public QWidget, public CORE::Translate<Base>, public CORE::Synchronized
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
        Base(OSGi::Context* pContext, QString pTypeName);   //!< Constructeur.
virtual ~Base();                                            //!< Destructeur.
        //@}
    private:

virtual auto    buildTranslater() -> CORE::Translated::Updater override;

        Q_CUSTOM_DECLARE_PRIVATE(Base)
    };
}

#endif  // DATA_BASE_HH
