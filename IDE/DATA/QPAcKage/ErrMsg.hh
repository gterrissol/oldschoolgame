/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_ERRMSG_HH
#define QPAK_ERRMSG_HH

/*! @file IDE/DATA/QPAcKage/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module QPAK.
    @author @ref Guillaume_Terrissol
    @date 19 Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace QPAK
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kAlreadyExistingName;
    extern  ERR::String kCantOpenFileDB;
    extern  ERR::String kCantSaveFile;
    extern  ERR::String kImportFileFailed;
    extern  ERR::String kIncompleteExtension;
    extern  ERR::String kInvalidFileCount;
    extern  ERR::String kInvalidFileDB_ImpossibleRecovery;
    extern  ERR::String kInvalidFileName;
    extern  ERR::String kInvalidMBR;
    extern  ERR::String kNoMoreRoomAvailable;
    extern  ERR::String kNotReadingAnExtension;
    extern  ERR::String kTooLongFilename;
    extern  ERR::String kUninitializedBlockMgr;
    extern  ERR::String kUnknownExtension;
    extern  ERR::String kUnknownMarkup;
    extern  ERR::String kXMLError_InvalidEnd;
    extern  ERR::String kXMLError_NoFileCount;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De QPAK_ERRMSG_HH
