/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BlockMgr.hh"

/*! @file IDE/DATA/QPAcKage/BlockMgr.cc
    @brief M�thodes (non-inline) de la classe QPAK::BlockMgr.
    @author @ref Guillaume_Terrissol
    @date 25 Juillet 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PAcKage/Enum.hh"
#include "PAcKage/File.hh"
#include "STL/Map.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                              P-Impl de BlockMgr
//------------------------------------------------------------------------------

    /*! @brief P-Impl de PAK::BlockMgr.
        @version 0.4

        @todo Utiliser une hash_map ?
     */
    class BlockMgr::Private
    {
    public:
        //! @name Constructeur
        //@{
                    Private(PAK::BlockHdl pBlockHdl,
                             const Vector<PAK::File>& pFiles); //!< Constructeur.
        //@}
        //! @name Autre m�thode
        //@{
        inline  U32 remainingSize() const;                  //!< Nombre d'octets libres.
        //@}
        //! @name Attributs
        //@{
        typedef Map<PAK::BlockHdl, U32>     Container;      //!< Dictionnaire de "trous".
        typedef Container::iterator         Iterator;       //!< It�rateur sur la map.
        typedef Container::const_iterator   ConstIterator;  //!< It�rateur constant sur la map.
        Container                           mResources;     //!< Ressources (trous).
        U32                                 mRemainingSize; //!< Nombre d'octets libres.
        //@}
    };


//------------------------------------------------------------------------------
//                     P-Impl de BlockMgr : "Constructeurs"
//------------------------------------------------------------------------------

    /*! @copydoc BlockMgr::BlockMgr(PAK::BlockHdl pBlockHdl, const Vector<PAK::File>& pFiles)
     */
    BlockMgr::Private::Private(PAK::BlockHdl pBlockHdl, const Vector<PAK::File>& pFiles)
        : mResources()
        , mRemainingSize(PAK::eMaxPakFileSize)
    {
        // Reconstruction des "trous" � partir des donn�es des fichiers (�a risque d'�tre
        // un peu long, d'o� l'importance de d�fragmenter aussi souvent que possible).
        Container  lAllocatedBlocks;
        // 1�re �tape : insertion des blocs de donn�es (et pas des trous).
        for(U32 lB = k0UL; lB < pFiles.size(); ++lB)
        {
            const PAK::File&   lCurrentFile = pFiles[lB];

            // Il ne faut consid�rer que les fichiers valides
            // (et qui ont d�j� �t� sauvegard�s - i.e. dont le handle de block est valide).
            if (lCurrentFile.isValid())
            {
                lAllocatedBlocks.insert(std::make_pair(lCurrentFile.dataHdl(), lCurrentFile.size()));
            }
        }

        // 2�me �tape : inversion des blocs.
        PAK::BlockHdl  lCurrentHdl     = pBlockHdl;
        U32             lMaxSize        = U32(PAK::eMaxPakFileSize - pBlockHdl.value());
        U32             lCurrentSize    = lMaxSize;
        mRemainingSize -= U32(pBlockHdl.value());

        for(auto lBlockI = lAllocatedBlocks.begin(); lBlockI != lAllocatedBlocks.end(); ++lBlockI)
        {
            if (lCurrentHdl == lBlockI->first)
            {
                lCurrentHdl = PAK::BlockHdl(I32(lCurrentHdl.value() + lBlockI->second));
            }
            else
            {
                mResources.insert(std::make_pair(lCurrentHdl, U32(lBlockI->first.value() - lCurrentHdl.value())));
                lCurrentHdl = PAK::BlockHdl(I32(lBlockI->first.value() + lBlockI->second));
            }
            lCurrentSize    = U32(lMaxSize - lCurrentHdl.value());
            mRemainingSize -= lBlockI->second;
        }

        if (lCurrentSize != 0)
        {
            mResources.insert(std::make_pair(lCurrentHdl, lCurrentSize));
        }
    }


//------------------------------------------------------------------------------
//                      P-Impl de BlockMgr : Autre M�thode
//------------------------------------------------------------------------------

    /*! @return Le nombre total d'octets libres
        @sa mRemainingSize
     */
    inline U32 BlockMgr::Private::remainingSize() const
    {
        return mRemainingSize;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur � partir des donn�es d'un pak file.<br>
        Ce constructeur doit �tre appel� pour un pak file existant. La liste des fichiers permet de
        retrouver les �ventuels "trous".
        @param pBlockHdl Handle sur le d�but des donn�es dans le <b>pak</b> file (le d�but �tant r�serv�
        � la FAT)
        @param pFiles    Fichiers du pak file dont les "ressources" seront g�r�es par l'instance
     */
    BlockMgr::BlockMgr(PAK::BlockHdl pBlockHdl, const Vector<PAK::File>& pFiles)
        : pthis(pBlockHdl, pFiles)
    { }


    /*! Destructeur.
     */
    BlockMgr::~BlockMgr() { }


//------------------------------------------------------------------------------
//                         Gestion des Blocs de Donn�es
//------------------------------------------------------------------------------

    /*! Acquisition d'un bloc.
        @param pBlockSize Taile du bloc � allouer
        @return Un handle sur le bloc allou�
        @exception ERR::Exception lorsqu'aucun bloc assez grand n'a pu �tre trouv�
     */
    PAK::BlockHdl BlockMgr::acquireBlock(U32 pBlockSize)
    {
        if (pBlockSize == 0)
        {
            // Bloc vide : handle nul.
            return PAK::BlockHdl(PAK::eBadBlockHdl);
        }

        for(auto lResI = pthis->mResources.begin(); lResI != pthis->mResources.end(); ++lResI)
        {
            // Si le "trou" actuel est suffisamment grand...
            if (pBlockSize <= lResI->second)
            {
                // Acquisition.
                U32 lRemainingBytes = lResI->second - pBlockSize;
                lResI->second = pBlockSize;

                // Mise � jour du "trou".
                PAK::BlockHdl   lAcquiredHdl(lResI->first);                            // Handle du bloc acquis.
                PAK::BlockHdl   lNewBlockHdl(I32(lAcquiredHdl.value() + pBlockSize));  // Nouveau handle.

                pthis->mRemainingSize -= pBlockSize;                                   // Taille totale restante.
                pthis->mResources.erase(lResI);                                        // Suppression de la map.

                if (0 < lRemainingBytes)                                                // Si tout le bloc n'a pas �t� utilis�...
                {
                    pthis->mResources.insert(std::make_pair(lNewBlockHdl, lRemainingBytes)); // R�insertion (avec une nouvelle taille).
                }

                return lAcquiredHdl;
            }
        }

        // Pas de bloc assez grand trouv� : rien ne va plus (une d�fragmentation r�soudra le probl�me s'il reste quand m�me assez de place).
        LAUNCH_EXCEPTION(kNoMoreRoomAvailable)
    }


    /*! Redimensionnement d'un bloc.
        @param pBlockHdl     Handle du bloc � redimensionner
        @param pBlockSize    Taille actuelle du bloc
        @param pNewBlockSize Nouvelle taille du bloc
        @return Un handle sur la nouvelle position du bloc (qui a pu changer)
     */
    PAK::BlockHdl BlockMgr::resizeBlock(PAK::BlockHdl pBlockHdl, U32 pBlockSize, U32 pNewBlockSize)
    {
        // La m�thode n'est peut-�tre pas optimale, mais elle
        // a le m�tire d'�tre simple : lib�ration du bloc.
        releaseBlock(pBlockHdl, pBlockSize);
        // ... et nouvelle allocation.
        return acquireBlock(pNewBlockSize);
        // Autre solution d'impl�mentation :
        // 1�re �tape : simulation d'insertion (il faudra retirer l'it�rateur � la fin).
        // 2�me �tape : estimation de la taille allouable (gr�ce au bloc suivant).
        // 3�me �tape : conclusion
        // -> simple redimmensionement par r�duction du bloc sup�rieur ou
        // -> nouvelle acquisition
        // 4�me Suppression de l'int�rateur temporaire.
    }


    /*! Lib�ration d'un bloc : tentative de fusion du nouveau trou avec d'autres (d�fragmentation)
        @param pBlockHdl  Handle du bloc � lib�rer
        @param pBlockSize Taille du bloc � lib�rer
     */
    void BlockMgr::releaseBlock(PAK::BlockHdl pBlockHdl, U32 pBlockSize)
    {
        // Si le bloc est vide ou invalide, il n'y a rien � faire.
        if ((pBlockSize == 0) || (pBlockHdl == PAK::eBadBlockHdl))
        {
            return;
        }

        // 1�re �tape : insertion du bloc lib�r� dans la map.
        std::pair<Private::Iterator, bool>    lIter = pthis->mResources.insert(std::make_pair(pBlockHdl, pBlockSize));

        // 2e �tape : si le bloc a bien �t� ins�r�, essaie de
        // fusionner avec les blocs sup�rieur et inf�rieur.
        if (lIter.second)
        {
            // Tout d'abord, ne pas oublier de mettre � jour le nombre d'octets libre.
            pthis->mRemainingSize += pBlockSize;

            Private::Iterator lIterNewB = lIter.first;  // New Block (et Newbie ;-p)

            Private::Iterator lIterPrev = lIterNewB;
            if (lIterPrev != pthis->mResources.begin())
            {
                --lIterPrev;                    // Bloc inf�rieur (si NewB est le premier bloc, Prev aussi : le 1er "if" sera faux).
            }
            Private::Iterator lIterNext = lIterNewB;
            ++lIterNext;                        // Bloc sup�rieur (si NewB est le dernier bloc, Next aussi : le 2nd "if" sera faux).
            if (lIterNext == pthis->mResources.end())
            {
                lIterNext = lIterNewB;          // Remarque : les 2 "if" seront faux (cf ci-dessus) car la taille du bloc est non-nulle).
            }

            if (I32(lIterPrev->first.value() + lIterPrev->second) == lIterNewB->first.value())
            {
                // Blocs adjacents : fusion. Le cas est simple, il suffit de modifier la taille du bloc inf�rieur.
                lIterPrev->second += pBlockSize;
                // Retrait du "nouveau" bloc (qui vient de fusionner).
                pthis->mResources.erase(lIterNewB);
                // Les blocs ont fusionn�s : newb =- prev.
                lIterNewB = lIterPrev;
            }
            if (I32(lIterNewB->first.value() + lIterNewB->second) == lIterNext->first.value())
            {
                // Blocs adjacents : fusion. Il suffit de modifier la taille du nouveau bloc (qui est peut-�tre l'ancien bloc inf�rieur).
                lIterNewB->second += lIterNext->second;
                // Retrait du bloc "suivant" (qui vient de fusionner).
                pthis->mResources.erase(lIterNext);
                // Les blocs ont fusionn�s : next = newb (mais �a ne sert � rien ici, il n'y a plus rien � faire).
            }
        }
    }


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /*! @var QPAK::BlockMgr::Private::mResources
        <b>Clef</b>   : position d'un bloc de donn�es libre dans un pak file<br>
        <b>Valeur</b> : taille du bloc libre<br>
        L'avantage d'utiliser une std::map est que les "ressources" (en fait, des trous), sont tri�s
        automatiquement.
     */
    /*! @var QPAK::BlockMgr::Private::mRemainingSize
        L'ensemble des blocs m�moris�s par cette classe repr�sentent les zones du pak file non-utilis�es :
        celles-ci peuvent donc recevoir des donn�es. Il peut arriver (� cause, essentiellement, d'une
        fragmentation importante) qu'aucun bloc de taille suffisamment grande pour recevoir de nouvelles
        donn�es ne soit disponible. Ces nouvelles donn�es pourraient toutefois �tre �crites si la taille
        cumul�e des blocs de donn�es libres �tait assez importante. C'est cette taille que repr�sente cet
        attribut : elle est accessible gr�ce � la m�thode remainingSize().
     */
    /*! @typedef QPAK::BlockMgr::Private::Iterator
        Le seul int�r�t de ce typedef, outre la simplication de certaines lignes de code, r�side dans la
        possibilit� d'utiliser l'it�rateur dans une std::pair (� cause des des insertions manuelles avec
        test (Map::insert)).
     */
}
