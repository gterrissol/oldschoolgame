/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Fat.hh"

/*! @file IDE/DATA/QPAcKage/Fat.cc
    @brief M�thodes (non-inline) de la classe QPAK::Fat.
    @author @ref Guillaume_Terrissol
    @date 30 Avril 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDateTime>
#include <QFile>
#include <QHash>
#include <QString>
#include <QTextStream>
#include <QXmlStreamReader>

#include "ERRor/Assert.hh"
#include "ERRor/Exception.hh"
#include "PAcKage/Handle.hh"

#include "ErrMsg.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                                  Fat Editeur
//------------------------------------------------------------------------------

    namespace
    {
        //! @name Format du fichier FAT
        //@{
        const QString   kHeader         = QStringLiteral(
                                            "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                                            "<files\n"
                                            "  xmlns=\"osg\"\n"
                                            "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                                            "  xsi:schemaLocation=\"osg FileDB.xsd\"\n"
                                            "  count=\"%1\"\n"
                                            ">\n");

        const QString   kFooter         = QStringLiteral("</files>\n");

        const QString   kFileName       = QStringLiteral("name");
        const QString   kImported       = QStringLiteral("imported");
        const QString   kExported       = QStringLiteral("exported");
        const QString   kFileSize       = QStringLiteral("size");

        const QString   kNoFileName     = QStringLiteral("????????????????????");
        const QString   kNoTimeNorDate  = QStringLiteral("yyyy-MM-ddThh:mm:ss");
        const QString   kNoFileSize     = QStringLiteral("          ");

        const QString   kLine           = QStringLiteral("<file %1=\"%5\" %2=\"%6\" %3=\"%6\" %4=\"%7\"/>\n").
                                            arg(kFileName).
                                            arg(kImported).
                                            arg(kExported).
                                            arg(kFileSize).
                                            arg(kNoFileName).
                                            arg(kNoTimeNorDate).
                                            arg(kNoFileSize);

        const int       kFileNameMaxLen = kNoFileName.count(QChar('?'));
        const int       kFileSizeMaxLen = kNoFileSize.count(QChar(' '));
        const int       kFileNameInLine = kLine.indexOf(kFileName);
        const int       kImportedInLine = kLine.indexOf(kImported);
        const int       kExportedInLine = kLine.indexOf(kExported);
        const int       kFileSizeInLine = kLine.indexOf(kFileSize);
        //@}
    }


//------------------------------------------------------------------------------
//                                 P-Impl de Fat
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPAK::Fat.
        @version 0.4
     */
    class Fat::Private
    {
    public:
        //! @name Constructeurs
        //@{
                Private(QString pFileName, int pFileCount); //!< Constructeur (cr�ation).
                Private(QString pFileName);                 //!< Constructeur. (chargement).
        //@}
        void    open();                                     //!< Ouverture.
        void    load();                                     //!< Chargement.
        void    generate();                                 //!< Premi�re cr�ation.
        bool    check();                                    //!< Contr�le.
        int     newHandle() const;                          //!< "Allocation" d'un handle.
        void    freeHandle(int pHdl);                       //!< Lib�ration d'un handle.

        /*! @brief POD regroupant les attributs de fichiers pour la FAT �diteur
            @version 1.0
         */
        struct FileAttr
        {
            inline FileAttr();                              //!< Constructeur par d�faut.
            inline bool isValid() const;                    //!< Validit�.

            QString     mName;                              //!< Nom du fichier dans la FAT.
            QDateTime   mImported;                          //!< Date du dernier import.
            QDateTime   mExported;                          //!< Date du dernier export.
            qint32      mSize;                              //!< Taille du fichier sur disque.
        };

        using FileContainer = QVector<FileAttr>;            //!< Container de "fichiers"
        FileContainer   mFiles;                             //!< Fichiers g�r�s par la FAT.
        using FileHdls      = QHash<QString, int>;          //!< Dictionnaire de handles de fichiers.
        FileHdls        mHandles;                           //!< Handles des fichiers g�r�s par la FAT.

        QFile           mFatFile;                           //!< Fichier FAT.
        int             mNameOffset;                        //!< Position de la valeur de l'attribut Name.
        int             mImportOffset;                      //!< Position de la valeur de l'attribut Import.
        int             mExportOffset;                      //!< Position de la valeur de l'attribut Export.
        int             mSizeOffset;                        //!< Position de la valeur de l'attribut Size.
        int             mDistance;                          //!< "Distance" entre deux lignes cons�cutives.
        int             mCurrentFileCount;                  //!< Nombre de fichiers cr��s.
        qint64          mTotalSize;                         //!< Taille totale des fichiers cr��s.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur (cr�ation).
     */
    Fat::Private::Private(QString pFileName, int pFileCount)
        : mFiles{}
        , mFatFile{pFileName}
        , mNameOffset{0}
        , mImportOffset{0}
        , mExportOffset{0}
        , mDistance{0}
        , mCurrentFileCount{0}
        , mTotalSize{0}
    {
        ASSERT((0 < pFileCount) && (pFileCount <= PAK::eMaxFileCount), kInvalidFileCount)

        mFiles.resize(pFileCount);

        generate();
    }


    /*! Constructeur (chargement).
     */
    Fat::Private::Private(QString pFileName)
        : mFiles{}
        , mFatFile{pFileName}
        , mNameOffset{0}
        , mImportOffset{0}
        , mExportOffset{0}
        , mDistance{0}
        , mCurrentFileCount{0}
        , mTotalSize{0}
    {
        open();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Fat::Private::open()
    {
        if (mFatFile.isOpen())
        {
            mFatFile.close();
        }
        if (mFatFile.open(QFile::ReadWrite | QFile::Text | QFile::Unbuffered))
        {
            load();
        }
        else
        {
            LAUNCH_EXCEPTION(kCantOpenFileDB)
        }
    }


    /*! M�thode bourrine (� optimiser) pour trouver un nouveau handle de fichier.
     */
    int Fat::Private::newHandle() const
    {
        for(auto lFileI = mFiles.begin(); lFileI != mFiles.end(); ++lFileI)
        {
            if (!(*lFileI).isValid())
            {
                return lFileI - mFiles.begin();
            }
        }

        return PAK::eBadHdl;
    }


    /*!
     */
    void Fat::Private::freeHandle(int pHdl)
    {
        if (pHdl < mFiles.size())
        {
            mHandles.remove(mFiles[pHdl].mName.trimmed());
            mFiles[pHdl].mName = kNoFileName;
        }
    }


    /*!
     */
    void Fat::Private::load()
    {
        QXmlStreamReader    lReader;
        lReader.setDevice(&mFatFile);

        for(int lHdl = 0; !lReader.atEnd(); lReader.readNext())
        {
            if      (lReader.isStartElement())
            {
                if      (lReader.name() == "files")
                {
                    QStringRef  lCount  = lReader.attributes().value("count");
                    if (lCount.length() == 0)
                    {
                        LAUNCH_EXCEPTION(kXMLError_NoFileCount)
                    }
                    else
                    {
                        int lFileCount = lCount.toString().toInt();
                        mFiles.resize(lFileCount);
                    }
                }
                else if (lReader.name() == "file")
                {
                    QStringRef  lName       = lReader.attributes().value(kFileName);
                    QStringRef  lImported   = lReader.attributes().value(kImported);
                    QStringRef  lExported   = lReader.attributes().value(kExported);
                    QStringRef  lSize       = lReader.attributes().value(kFileSize);

                    QString     lFileName   = lName.toString().trimmed();
                    if (!lFileName.contains('?') && mHandles.contains(lFileName))
                    {
                        LAUNCH_EXCEPTION(kAlreadyExistingName)
                    }
                    mFiles[lHdl].mName = lFileName;
                    if (mFiles[lHdl].isValid())
                    {
                        mFiles[lHdl].mImported  = QDateTime::fromString(lImported.toString(), Qt::ISODate);
                        mFiles[lHdl].mExported  = QDateTime::fromString(lExported.toString(), Qt::ISODate);
                        mTotalSize += 
                        mFiles[lHdl].mSize      = lSize.toString().toUInt();
                        ++mCurrentFileCount;
                        mHandles[lFileName] = lHdl;
                    }
                    ++lHdl;
                }
            }
            else if (lReader.isEndElement())
            {
                if (lReader.name() == "files")
                {
                    if (lHdl != mFiles.count())
                    {
                        LAUNCH_EXCEPTION(kXMLError_InvalidEnd)
                    }
                }
            }
        }

        if (!check())
        {
            generate();

            // Si, apr�s un r�g�n�ration, le fichier est toujours invalide, pas la peine d'insister.
            if (!check())
            {
                LAUNCH_EXCEPTION(kInvalidFileDB_ImpossibleRecovery)
            }
        }
    }


    /*!
     */
    void Fat::Private::generate()
    {
        if (mFatFile.isOpen())
        {
            mFatFile.close();
        }
        if (mFatFile.open(QFile::WriteOnly | QFile::Truncate | QFile::Text | QFile::Unbuffered))
        {
            QTextStream lStream(&mFatFile);
            lStream << kHeader.arg(mFiles.count());

            for(auto lFile : mFiles)
            {
                QString lLineText = kLine;

                if (lFile.isValid())
                {
                    QString lName       = lFile.mName;
                    lLineText.replace(lLineText.indexOf("????"), kFileNameMaxLen, lName);
                    QString lImported   = lFile.mImported.toString(Qt::ISODate);
                    lLineText.replace(lLineText.indexOf("yyyy"), lImported.length(), lImported);
                    QString lExported   = lFile.mExported.toString(Qt::ISODate);
                    lLineText.replace(lLineText.indexOf("yyyy"), lExported.length(), lExported);
                    QString lSize       = QString::number(lFile.mSize);
                    lLineText.replace(lLineText.indexOf("    "), kFileSizeMaxLen, lSize);
                }

                lStream << lLineText;
            }
            lStream << kFooter;

            open();
        }
        else
        {
            LAUNCH_EXCEPTION(kCantOpenFileDB) 
        }
    }


    /*!
     */
    bool Fat::Private::check()
    {
        // Une fois le contenu du fichier lu, il faut rechercher :
        // - 1 : l'emplacement du nom du premier fichier de la fat,
        // - 2 : la "distance" entre deux noms de fichier cons�cutifs.
        QTextStream lStream(&mFatFile);
        lStream.seek(0);
        QString     lLine;
        do
        {
            lLine = lStream.readLine();
        }
        while(!lLine.contains(kFileName) && !lStream.atEnd());

        if (!lStream.atEnd())
        {
            int lLineLength = lLine.length();

            // Il faut faire ce petit calcul, et pas se contenter de prendre lLineLength
            // car le caract�re de fin de ligne n'est pas stock� dans lLine.
            int lPos    = lStream.pos();
            lStream.readLine();
            mDistance   = lStream.pos() - lPos;
            int lStartLine  = lPos - mDistance;

            mNameOffset     = lStartLine + kFileNameInLine + QString(kFileName).length() + 2;   // ...="...
            mImportOffset   = lStartLine + kImportedInLine + QString(kImported).length() + 2;   //    ''
            mExportOffset   = lStartLine + kExportedInLine + QString(kExported).length() + 2;   //    ''
            mSizeOffset     = lStartLine + kFileSizeInLine + QString(kFileSize).length() + 2;   //    ''

            // V�rification de toutes les lignes (longueur et position des attributs).
            for(;lLine.contains(kFileName); lLine = lStream.readLine())
            {
                if ((lLine.length()           != lLineLength)     ||
                    (lLine.indexOf(kFileName) != kFileNameInLine) ||
                    (lLine.indexOf(kImported) != kImportedInLine) ||
                    (lLine.indexOf(kExported) != kExportedInLine) ||
                    (lLine.indexOf(kFileSize) != kFileSizeInLine))
                {
                    return false;
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    inline Fat::Private::FileAttr::FileAttr()
        : mName{kFileNameMaxLen, QChar('?')}
        , mImported{}
        , mExported{}
        , mSize{}
    { }


    /*! @return VRAI si le fichier est valide, d'apr�s ses attributs, FAUX sinon
     */
    inline bool Fat::Private::FileAttr::isValid() const
    {
        return mName[0] != '?';
    }


//------------------------------------------------------------------------------
//                       Fat : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur : cr�ation d'une nouvelle FAT.
        @param pFileName  Nom du fichier dans lequel �crire les donn�es de la FAT
        @param pFileCount Nombre maximal de fichiers que la FAT devra g�rer
     */
    Fat::Fat(QString pFileName, int pFileCount)
        : pthis(pFileName, pFileCount)
    { }


    /*! Constructeur : chargement d'une FAT existante.
        @param pFileName Nom du fichier des donn�es de la FAT
     */
    Fat::Fat(QString pFileName)
        : pthis(pFileName)
    { }


    /*! Destructeur.
     */
    Fat::~Fat() { }


//------------------------------------------------------------------------------
//                           Fat : Gestion de Fichiers
//------------------------------------------------------------------------------

    /*!
     */
    int Fat::createFile(QString pName)
    {
        if (pName.contains('?'))
        {
            return PAK::eBadHdl;
        }

        int lHdl = pthis->newHandle();

        if ((lHdl != int(PAK::eBadHdl))    &&
            (lHdl < pthis->mFiles.count()) &&
            !pthis->mFiles[lHdl].isValid())
        {
            // C'est le nom exact qui doit �tre utilis� pour indexer les handles.
            pthis->mHandles[pName] = lHdl;

            if (kFileNameMaxLen < pName.length())
            {
                LAUNCH_EXCEPTION(kTooLongFilename)
            }
            else
            {
                pName = pName.leftJustified(kFileNameMaxLen, QChar(' '));
            }

            Private::FileAttr&    lFileAttr   = pthis->mFiles[lHdl];
            lFileAttr.mName     = pName;
            lFileAttr.mImported = QDateTime::currentDateTime();
            lFileAttr.mExported = QDateTime::currentDateTime();
            lFileAttr.mSize     = 0;    // La taille sera renseign�e plus tard, via updateDiskFileSize().

            QTextStream lStream(&pthis->mFatFile);
            lStream.seek(pthis->mNameOffset   + lHdl * pthis->mDistance);
            lStream << lFileAttr.mName;

            lStream.seek(pthis->mImportOffset + lHdl * pthis->mDistance);
            lStream << lFileAttr.mImported.toString(Qt::ISODate);

            lStream.seek(pthis->mExportOffset + lHdl * pthis->mDistance);
            lStream << lFileAttr.mExported.toString(Qt::ISODate);

            lStream.seek(pthis->mSizeOffset + lHdl * pthis->mDistance);
            lStream << lFileAttr.mSize;

            lStream.flush();    // Ceci a l'air d'�tre n�cessaire sous windows...

            ++pthis->mCurrentFileCount;
            pthis->mTotalSize += lFileAttr.mSize;

            return lHdl;
        }
        else
        {
            return PAK::eBadHdl;
        }
    }


    /*!
     */
    void Fat::eraseFile(QString pName)
    {
        int lToEraseHdl = fileHdl(pName);

        eraseFile(lToEraseHdl);
    }


    /*!
     */
    void Fat::eraseFile(int pHdl)
    {
        if (pHdl != static_cast<int>(PAK::eBadHdl))
        {
            updateImportTime(pHdl, QDateTime{});
            updateExportTime(pHdl, QDateTime{});
            updateDiskFileSize(pHdl, 0);
            pthis->freeHandle(pHdl);
            --pthis->mCurrentFileCount;
        }
    }


//------------------------------------------------------------------------------
//                        Fat : Import/Export de Fichiers
//------------------------------------------------------------------------------

    /*!
     */
    void Fat::updateImportTime(int pHdl, QDateTime pTime)
    {
        QTextStream             lStream(&pthis->mFatFile);
        Private::FileAttr&    lFileAttr   = pthis->mFiles[pHdl];

        lFileAttr.mImported = pTime;

        lStream.seek(pHdl * pthis->mDistance + pthis->mImportOffset);

        if (pTime.isValid())
        {
            lStream << lFileAttr.mImported.toString(Qt::ISODate);
        }
        else
        {
            lStream << kNoTimeNorDate;
        }
    }


    /*!
     */
    void Fat::updateExportTime(int pHdl, QDateTime pTime)
    {
        QTextStream         lStream(&pthis->mFatFile);
        Private::FileAttr&  lFileAttr   = pthis->mFiles[pHdl];

        lFileAttr.mExported = pTime;

        lStream.seek(pHdl * pthis->mDistance + pthis->mExportOffset);
        if (pTime.isValid())
        {
            lStream << lFileAttr.mExported.toString(Qt::ISODate);
        }
        else
        {
            lStream << kNoTimeNorDate;
        }
    }


    /*!
     */
    bool Fat::needToImport(int pHdl, QDateTime pFileTime) const
    {
        if ((0 <= pHdl) && (pHdl < pthis->mFiles.size()))
        {
            Private::FileAttr&    lFileAttr   = pthis->mFiles[pHdl];

            return  lFileAttr.isValid()              &&
                   (lFileAttr.mExported < pFileTime) &&
                   (lFileAttr.mImported < pFileTime);
        }

        return false;
    }


//------------------------------------------------------------------------------
//                           Fat : Taille des Fichiers
//------------------------------------------------------------------------------

    /*!
     */
    void Fat::updateDiskFileSize(int pHdl, qint32 pSize)
    {
        QTextStream lStream{&pthis->mFatFile};
        auto&       lFileAttr   = pthis->mFiles[pHdl];
        auto        lTotalSize  = pthis->mTotalSize - lFileAttr.mSize + pSize;

        lFileAttr.mSize = pSize;
        lStream.seek(pHdl * pthis->mDistance + pthis->mSizeOffset);
        if (pSize != 0)
        {
            lStream << lFileAttr.mSize;
        }
        else
        {
            lStream << kNoFileSize;
        }

        pthis->mTotalSize = lTotalSize;
    }

    /*!
     */
    qint64 Fat::diskFileSize(int pHdl) const
    {
        return qint64{pthis->mFiles[pHdl].mSize};
    }


    /*!
     */
    qint64 Fat::totalDiskSize() const
    {
        return pthis->mTotalSize;
    }


//------------------------------------------------------------------------------
//                    Fat : Informations - Nombre de Fichiers
//------------------------------------------------------------------------------

    /*! @return Le nombre de fichiers maximum autoris� par cette instance de FAT.
     */
    int Fat::totalFileCount() const
    {
        return pthis->mFiles.size();
    }


    /*!
     */
    int Fat::currentFileCount() const
    {
        return pthis->mCurrentFileCount;
    }


//------------------------------------------------------------------------------
//               Fat : Informations - Identification des Fichiers
//------------------------------------------------------------------------------

    /*!
     */
    int Fat::fileHdl(QString pFileName) const
    {
        Private::FileHdls::const_iterator lIter = pthis->mHandles.find(pFileName);

        if (lIter != pthis->mHandles.end())
        {
            return lIter.value();
        }
        else
        {
            return -1;
        }
    }


    /*!
     */
    QString Fat::fileName(int pHdl) const
    {
        if ((0 <= pHdl) && (pHdl < pthis->mFiles.size()))
        {
            const Private::FileAttr&  lFileAttr   = pthis->mFiles[pHdl];

            if (lFileAttr.isValid())
            {
                return lFileAttr.mName.simplified();
            }
        }

        return QString::null;
    }
}
