    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/QPAcKage.cc
    @brief D�finitions diverses du module @ref QPAcKage.
    @author @ref Guillaume_Terrissol
    @date 19 Janvier 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/UIService.hh"
#include "ERRor/ErrMsg.hh"

#include "FileService.hh"
#include "ImportExport.hh"
#include "ImportExportService.hh"
#include "View.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlreadyExistingName,               "Fichier d�j� pr�sent.",                                "Already existing name.")
    REGISTER_ERR_MSG(kCantOpenFileDB,                    "Ne peut ouvrir FileDB.",                               "Can't open FileDB.")
    REGISTER_ERR_MSG(kCantSaveFile,                      "Ne peut sauvegarder le fichier.",                      "Can't save file.")
    REGISTER_ERR_MSG(kImportFileFailed,                  "L'import d'un fichier a �chou�.",                      "Import file failed.")
    REGISTER_ERR_MSG(kIncompleteExtension,               "Extension incompl�te.",                                "Incomplete extension.")
    REGISTER_ERR_MSG(kInvalidFileCount,                  "Nombre de fichiers invalide.",                         "Invalid file count.")
    REGISTER_ERR_MSG(kInvalidFileDB_ImpossibleRecovery,  "FileDB invalide et impossible � r�cup�rer.",           "invalid FileDB, impossible recovery.")
    REGISTER_ERR_MSG(kInvalidFileName,                   "Nom de fichier invalide.",                             "Invalid file name.")
    REGISTER_ERR_MSG(kInvalidMBR,                        "MBR invalide.",                                        "Invalid MBR.")
    REGISTER_ERR_MSG(kNoMoreRoomAvailable,               "Plus de place disponible.",                            "No more space available.")
    REGISTER_ERR_MSG(kNotReadingAnExtension,             "N'est pas en train de lire pas une extension.",        "Not reading an extension.")
    REGISTER_ERR_MSG(kTooLongFilename,                   "Nom de fichier trop long.",                            "Too long filename.")
    REGISTER_ERR_MSG(kUninitializedBlockMgr,             "Gestionnaire de blocs non-initialis�.",                "Uninitialized block manager.")
    REGISTER_ERR_MSG(kUnknownExtension,                  "Extension inconnue.",                                  "Unknown extension.")
    REGISTER_ERR_MSG(kUnknownMarkup,                     "Balise inconnue.",                                     "Unknown markup.")
    REGISTER_ERR_MSG(kXMLError_InvalidEnd,               "Erreur de lecture XML : fin invalide.",                "XML read error : invalid end.")
    REGISTER_ERR_MSG(kXMLError_NoFileCount,              "Erreur de format XML : pas de nombre de fichiers.",    "XML format error : no file count.")


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("data.pakfileview", [=]() { return CORE::UIService::Widget{new View{pContext}}; });

                    pContext->services()->checkIn("fr.osg.ide.pak.file", std::make_shared<FileService>());

                    auto    lIEService = std::make_shared<ImportExportService>();
                    pContext->services()->checkIn("fr.osg.ide.pak.ie", lIEService);
                    auto    lExtensionDir = lIEService->extensionDir(BUNDLE_NAME, pContext);

                    lIEService->checkIn<ImportExportFileHdl>(    "hdl",  lExtensionDir);
                    lIEService->checkIn<ImportExportMBR>(        "mbr",  lExtensionDir);
                    lIEService->checkIn<ImportExportResourceMgr>("rsc",  lExtensionDir);
                    lIEService->checkIn<ImportExportU32>(        "u32",  lExtensionDir);
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("data.pakfileview");

                    auto    lIEService = pContext->services()->findByTypeAndName<ImportExportService>("fr.osg.ide.pak.ie");
                    lIEService->checkOut("hdl");
                    lIEService->checkOut("mbr");
                    lIEService->checkOut("rsc");
                    lIEService->checkOut("u32");

                    pContext->services()->checkOut("fr.osg.ide.pak.ie");
                    pContext->services()->checkOut("fr.osg.ide.pak.file");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(QPAK::Activator)
OSGI_END_REGISTER_ACTIVATORS()

