/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_FILEMGR_HH
#define QPAK_FILEMGR_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/FileDB.hh
    @brief En-t�te de la classe QPAK::FileDB.
    @author @ref Guillaume_Terrissol
    @date 27 Janvier 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

#include "MEMory/PImpl.hh"
#include "MEMory/Singleton.hh"
#include "PAcKage/Handle.hh"

class QString;

namespace QPAK
{
    /*! @brief Gestionnaire de fichiers �diteur.
        @version 0.4

     */
    class FileDB
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        FileDB(OSGi::Context* pContext);            //!< Constructeur
                        ~FileDB();                                  //!< Destructeur.
        //@}
        //! @name Op�rations g�n�rales
        //@{
        QString         make();                                     //!< Cr�ation d'un pak file.
        void            open(QString pFileName);                    //!< Ouverture d'un pak file.
        void            close();                                    //!< Fermeture d'un pak file.
        //@}
        //! @name Cr�ation / suppression
        //@{
        PAK::FileHdl    create(QString pFileName);                  //!< Cr�ation d'un fichier.
        PAK::FileHdl    import(QString pSysFile);                   //!< Import d'un fichier.
        void            erase(QString pFileName);                   //!< Suppression d'un fichier.
        void            erase(PAK::FileHdl pHdl);                   //!< Suppression d'un fichier.
        //@}
        //! @name Informations g�n�rales
        //@{
        OSGi::Context*  context() const;                            //!< Contexte d'ex�cution.
        QString         pakFilesDir() const;                        //!< Chemin par d�faut des @b pak files.
        QString         openedPakFileDir() const;                   //!< Chemin des donn�es du @b pak file ouvert.
        U32             totalFileCount() const;                     //!< Nombre maximum de fichiers.
        U32             currentFileCount() const;                   //!< Nombre de fichiers cr��s.
        //@}
        //! @name Information d'un fichier du pak file
        //@{
        PAK::FileHdl    packFileHdl(QString pFileName) const;       //!< Handle.
        QString         packFileName(PAK::FileHdl pHdl) const;      //!< Nom.
        U32             packFileSize(PAK::FileHdl pHdl) const;      //!< Taille.
        QString         packFileType(PAK::FileHdl pHdl) const;      //!< Type.
        QString         packFileVersion(PAK::FileHdl pHdl) const;   //!< Version.
        //@}
        //! @name Taille d'un fichier sur disque
        //@{
        void            diskFileSize(PAK::FileHdl pHdl, I64 pSize); //!< Mise � jour.
        I64             diskFileSize(PAK::FileHdl pHdl) const;      //!< R�cup�ration.
        I64             totalDiskSize() const;                      //!< Taille exhaustive.
        //@}

    private:

        FORBID_COPY(FileDB)
        FREE_PIMPL()
    };
}


#endif  // De QPAK_FILEDB_HH
