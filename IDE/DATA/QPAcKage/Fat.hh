/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_FAT_HH
#define QPAK_FAT_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/Fat.hh
    @brief En-t�te de la classe QPAK::Fat.
    @author @ref Guillaume_Terrissol
    @date 30 Avril 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QtGlobal>

#include "MEMory/PImpl.hh"

class QDateTime;
class QString;

namespace QPAK
{
    /*! @brief Fat �diteur
        @version 0.8
     */
    class Fat
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                Fat(QString pFileName, int pFileCount);             //!< Constructeur (cr�ation).
                Fat(QString pFileName);                             //!< Constructeur (chargement).
                ~Fat();                                             //!< Destructeur.
        //@}
        //! @name Gestion de fichiers
        //@{
        int     createFile(QString pName);                          //!< Cr�ation d'un fichier.
        void    eraseFile(QString pName);                           //!< Suppression d'un fichier.
        void    eraseFile(int pHdl);                                //!< Suppression d'un fichier.
        //@}
        //! @name Import/Export de fichiers
        //@{
        void    updateImportTime(int pHdl, QDateTime pTime);        //!< Mise � jour de la date d'import.
        void    updateExportTime(int pHdl, QDateTime pTime);        //!< Mise � jour de la date d'export.
        bool    needToImport(int pHdl, QDateTime pFileTime) const;  //!< Besoin d'un r�import du fichier ?
        //@}
        //! @name Taille des fichiers
        //@{
        void    updateDiskFileSize(int pHdl, qint32 pSize);         //!< Mise � jour de la taille d'un fichier.
        qint64  diskFileSize(int pHdl) const;                       //!< Taille d'un fichier sur disque.
        qint64  totalDiskSize() const;                              //!< Taille totale des fichiers sur disque.
        //@}
        //! @name Informations : nombre de fichiers
        //@{
        int     totalFileCount() const;                             //!< ... total.
        int     currentFileCount() const;                           //!< ... cr��s.
        //@}
        //! @name Informations : identification des fichiers
        //@{
        int     fileHdl(QString pFileName) const;                   //!< Handle d'un fichier.
        QString fileName(int pHdl) const;                           //!< Nom d'un fichier.
        //@}

    private:

        FORBID_COPY(Fat)
        FREE_PIMPL()
    };
}

#endif  // De QPAK_FAT_HH
