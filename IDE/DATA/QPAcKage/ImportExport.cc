/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ImportExport.hh"

/*! @file IDE/DATA/QPAcKage/ImportExport.cc
    @brief M�thodes (non-inline) des classes QPAK::ImportExport & d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 1er Mai 2008 - 27 Septembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>
#include <QFile>
#include <QMap>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "CORE/ProgressService.hh"
#include "PAcKage/Handle.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/String.hh"

#include "ErrMsg.hh"
#include "FileDB.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                                  Constantes
//------------------------------------------------------------------------------

    namespace
    {
        const QString   kData       = QStringLiteral("Data");
        const QString   kInfo       = QStringLiteral("Info");
        const QString   kBool       = QStringLiteral("Boolean");
        const QString   kMask       = QStringLiteral("Bitmask");
        const QString   kI8         = QStringLiteral("I8");
        const QString   kU8         = QStringLiteral("U8");
        const QString   kI16        = QStringLiteral("I16");
        const QString   kU16        = QStringLiteral("U16");
        const QString   kI32        = QStringLiteral("I32");
        const QString   kU32        = QStringLiteral("U32");
        const QString   kHex        = QStringLiteral("Hex");
        const QString   kI64        = QStringLiteral("I64");
        const QString   kU64        = QStringLiteral("U64");
        const QString   kF32        = QStringLiteral("F32");
        const QString   kF64        = QStringLiteral("F64");
        const QString   kFileHdl    = QStringLiteral("File");
        const QString   kNoFile     = QStringLiteral("no file");
        const QString   kKey        = QStringLiteral("Key");
        const QString   kString     = QStringLiteral("String");
        const QString   kRGB        = QStringLiteral("RGB");
        const QString   kRGBA       = QStringLiteral("RGBA");
        const QString   kR          = QStringLiteral("Red channel");
        const QString   kG          = QStringLiteral("Green channel");
        const QString   kB          = QStringLiteral("Blue channel");
        const QString   kA          = QStringLiteral("Alpha channel");
        const QString   kX          = QStringLiteral("X");
        const QString   kY          = QStringLiteral("Y");
        const QString   kZ          = QStringLiteral("Z");
        const QString   kW          = QStringLiteral("W");
        const QString   kRect       = QStringLiteral("Rectangle");
        const QString   kV3         = QStringLiteral("Vector3D");
        const QString   kQ          = QStringLiteral("Quaternion");
        const QString   kMat        = QStringLiteral("Material");
        const QString   kApp        = QStringLiteral("Appearance");
        const QString   kGO         = QStringLiteral("GameObject");
        const QString   kLeft       = QStringLiteral("Left");
        const QString   kTop        = QStringLiteral("Top");
        const QString   kRight      = QStringLiteral("Right");
        const QString   kBottom     = QStringLiteral("Bottom");
        const QString   kContainer  = QStringLiteral("Container");
        const QString   kVersion    = QStringLiteral("Version");

        const int       kVer        = 1;
    }


//------------------------------------------------------------------------------
//                                     Data
//------------------------------------------------------------------------------

    /*! @param pDB Base de donn�es utilis�e pour les imports/exports
     */
    ImportExport::Data::Data(FileDB* pDB)
        : mDB(pDB)
    { }

#ifndef NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                                Sp�cialisations
//------------------------------------------------------------------------------

    template<>
    void ImportExport::Data::read<ImportExport::eBool>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        QString lBool = pReader.readElementText();
        pSaver << ((lBool == QStringLiteral("1")) || (lBool == QStringLiteral("true")));
    }
    template<>
    void ImportExport::Data::read<ImportExport::eMask64>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        auto    lValue = pReader.readElementText();
        if      (lValue.length() <= 16)
        {
            pSaver << U16{lValue.toUShort(0, 2)};
        }
        else if (lValue.length() <= 32)
        {
            pSaver << U32{lValue.toUInt(0, 2)};
        }
        else
        {
            pSaver << U64{lValue.toULongLong(0, 2)};
        }
    }
    template<>
    void ImportExport::Data::read<ImportExport::eI8>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << I8(pReader.readElementText().toInt());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eU8>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << U8(pReader.readElementText().toUInt());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eI16>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << I16(pReader.readElementText().toShort());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eU16>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << U16(pReader.readElementText().toUShort());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eI32>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << I32(pReader.readElementText().toLong());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eU32>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << U32(pReader.readElementText().toULong());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eHex>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << U32(pReader.readElementText().toULong(0, 16));
    }
    template<>
    void ImportExport::Data::read<ImportExport::eI64>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << I64(pReader.readElementText().toLongLong());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eU64>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << U64(pReader.readElementText().toULongLong());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eF32>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << F32(pReader.readElementText().toFloat());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eF64>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << F64(pReader.readElementText().toDouble());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eFileHdl>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        QString lFileName = pReader.readElementText();
        pSaver << PAK::FileHdl((lFileName != kNoFile) ? mDB->packFileHdl(lFileName) : PAK::eBadHdl);
    }
    template<>
    void ImportExport::Data::read<ImportExport::eKey>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << I32(pReader.readElementText().toLong());
    }
    template<>
    void ImportExport::Data::read<ImportExport::eString>(QXmlStreamReader& pReader, PAK::Saver& pSaver)
    {
        pSaver << String(qPrintable(pReader.readElementText()));
    }
    template<>
    void ImportExport::Data::read<ImportExport::eRGB>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eRGBA>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eRect>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eContainer>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eVector3>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eQuaternion>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eMaterial>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eAppearance>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }
    template<>
    void ImportExport::Data::read<ImportExport::eGameObject>(QXmlStreamReader&, PAK::Saver&)
    {
        // Juste un classifieur...
    }

    template<>
    void ImportExport::Data::write<ImportExport::eBool>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        bool    lBool;
        pLoader >> lBool;
        pWriter.writeStartElement(kBool);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(lBool ? QStringLiteral("true") : QStringLiteral("false"));
        pWriter.writeEndElement();
    }
    namespace
    {
        template<typename TD>
        inline void writeMask(QXmlStreamWriter& pWriter, TD pValue, const QString& pName)
        {
            pWriter.writeStartElement(kMask);
            if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            pWriter.writeCharacters(QStringLiteral("%1").arg(pValue, sizeof(TD) * 8, 2, QChar('0')));
            pWriter.writeEndElement();
        }
    }
    template<>
    void ImportExport::Data::write<ImportExport::eMask16>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U16 lMask;
        pLoader >> lMask;
        writeMask(pWriter, lMask, pName);
    }
    template<>
    void ImportExport::Data::write<ImportExport::eMask32>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U32 lMask;
        pLoader >> lMask;
        writeMask(pWriter, lMask, pName);
    }
    template<>
    void ImportExport::Data::write<ImportExport::eMask64>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U64 lMask;
        pLoader >> lMask;
        writeMask(pWriter, lMask, pName);
    }
    template<>
    void ImportExport::Data::write<ImportExport::eI8>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        I8  lI8(0);
        pLoader >> lI8;
        pWriter.writeStartElement(kI8);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(int(lI8)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eU8>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U8  lU8(0);
        pLoader >> lU8;
        pWriter.writeStartElement(kU8);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(int(lU8)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eI16>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        I16 lI16(0);
        pLoader >> lI16;
        pWriter.writeStartElement(kI16);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(long(lI16)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eU16>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U16 lU16(0);
        pLoader >> lU16;
        pWriter.writeStartElement(kU16);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(ulong(lU16)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eI32>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        I32 lI32(0);
        pLoader >> lI32;
        pWriter.writeStartElement(kI32);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(long(lI32)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eU32>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U32 lU32(0);
        pLoader >> lU32;
        pWriter.writeStartElement(kU32);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(ulong(lU32)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eHex>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U32 lU32(0);
        pLoader >> lU32;
        pWriter.writeStartElement(kHex);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(ulong(lU32), 16));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eI64>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        I64 lI64(0);
        pLoader >> lI64;
        pWriter.writeStartElement(kI64);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(qlonglong(lI64)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eU64>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        U64 lU64(0);
        pLoader >> lU64;
        pWriter.writeStartElement(kU64);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(qulonglong(lU64)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eF32>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        F32 lF32(0.0F);
        pLoader >> lF32;
        pWriter.writeStartElement(kF32);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(double(lF32), 'g', 12));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eF64>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        F64 lF64(0);
        pLoader >> lF64;
        pWriter.writeStartElement(kF64);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(double(lF64), 'g', 12));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eFileHdl>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        PAK::FileHdl   lHdl = PAK::FileHdl(PAK::eBadHdl);
        pLoader >> lHdl;
        pWriter.writeStartElement(kFileHdl);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters((lHdl != PAK::eBadHdl) ? mDB->packFileName(lHdl) : QString(kNoFile));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eKey>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        I32 lKeyValue = k0L;
        pLoader >> lKeyValue;
        pWriter.writeStartElement(kKey);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(QString::number(ulong(lKeyValue)));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eString>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        String  lString;
        pLoader >> lString;
        pWriter.writeStartElement(kString);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
        pWriter.writeCharacters(lString.c_str());
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eRGB>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kRGB);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eF32>(pWriter, pLoader, kR);
            write<eF32>(pWriter, pLoader, kG);
            write<eF32>(pWriter, pLoader, kB);
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eRGBA>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kRGBA);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eF32>(pWriter, pLoader, kR);
            write<eF32>(pWriter, pLoader, kG);
            write<eF32>(pWriter, pLoader, kB);
            write<eF32>(pWriter, pLoader, kA);
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eRect>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kRect);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eF32>(pWriter, pLoader, kLeft);
            write<eF32>(pWriter, pLoader, kTop);
            write<eF32>(pWriter, pLoader, kRight);
            write<eF32>(pWriter, pLoader, kBottom);
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eVector3>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kV3);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eF32>(pWriter, pLoader, kX);
            write<eF32>(pWriter, pLoader, kY);
            write<eF32>(pWriter, pLoader, kZ);
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eQuaternion>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kQ);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eF32>(pWriter, pLoader, kX);
            write<eF32>(pWriter, pLoader, kY);
            write<eF32>(pWriter, pLoader, kZ);
            write<eF32>(pWriter, pLoader, kW);
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eMaterial>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kMat);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eRGBA>(pWriter, pLoader, QStringLiteral("Ambient"));
            write<eRGBA>(pWriter, pLoader, QStringLiteral("Diffuse"));
            write<eRGBA>(pWriter, pLoader, QStringLiteral("Specular"));
            write<eRGBA>(pWriter, pLoader, QStringLiteral("Emitted"));
            write<eF32>( pWriter, pLoader, QStringLiteral("Alpha"));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eAppearance>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kApp);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eKey>(pWriter, pLoader, QStringLiteral("Geometry"));
            write<eMaterial>(pWriter, pLoader);
            write<eF32>(pWriter, pLoader, QStringLiteral("Shadow coefficient"));
            write<eF32>(pWriter, pLoader, QStringLiteral("Reflection coefficient"));
            write<eF32>(pWriter, pLoader, QStringLiteral("Transparency coefficient"));
        pWriter.writeEndElement();
    }
    template<>
    void ImportExport::Data::write<ImportExport::eGameObject>(QXmlStreamWriter& pWriter, PAK::Loader& pLoader, const QString& pName)
    {
        pWriter.writeStartElement(kGO);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
            write<eU32>(pWriter, pLoader, QStringLiteral("Children"));
            write<eAppearance>(pWriter, pLoader);
            write<eVector3>(pWriter, pLoader, QStringLiteral("Reference position"));
            write<eQuaternion>(pWriter, pLoader, QStringLiteral("Reference orientation"));
            write<eVector3>(pWriter, pLoader, QStringLiteral("Reference scale"));
        pWriter.writeEndElement();
    }

#endif  // NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                              Gestion de Groupes
//------------------------------------------------------------------------------

    /*! @param pWriter Flux de sortie XML
        @param pName   Nom de groupe [optionel]
     */
    void ImportExport::Data::begin(QXmlStreamWriter& pWriter, PAK::Loader&, const QString& pName)
    {
        pWriter.writeStartElement(kContainer);
        if (!pName.isEmpty()) pWriter.writeAttribute(kInfo, pName);
    }


    /*! @param pWriter Flux de sortie XML
     */
    void ImportExport::Data::end(QXmlStreamWriter& pWriter, PAK::Loader&, const QString&)
    {
        pWriter.writeEndElement();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return La base de donn�es utilis�e
     */
    FileDB* ImportExport::Data::fileDB() const
    {
        return mDB;
    }


//------------------------------------------------------------------------------
//                                Import / Export
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExport::~ImportExport() { }


    /*! Cr�e un fichier (� la fois dans le @b pak file et sur disque) contenant le minimum de donn�es pour un fichier valide.
        @param pSystemFile Fichier cr�� sur disque � remplir
        @param pPackedFile Handle du fichier dans le @b pak file
        @note Cette m�thode a �t� introduite afin de supprimer les m�thodes saveDefault() de bon nombre d'autres classes
     */
    void ImportExport::make(QFile* pSystemFile, PAK::FileHdl pPackedFile)
    {
        // D�finition des donn�es par d�faut.
        {
            PAK::Saver  lSaver{pPackedFile};
            doMake(lSaver);
        }
        // Export vers un fichier sur disque.
        {
            PAK::Loader lLoader{pPackedFile};
            exports(pSystemFile, lLoader);
        }
        // NB: Import dans le pak file (via lSaver).
    }


    /*! Importe les donn�es d'un fichier [pr�alablement export�] dans le <b>pak</b> file.
        @param pFile  Fichier syst�me dans lequel se trouvent les donn�es � importer
        @param pSaver Saver dans lequel empiler les donn�es lues, dans le format binaire interne du type
        de fichier import�
        @return VRAI si l'import a r�ussi, FAUX sinon
     */
    bool ImportExport::imports(QFile* pFile, PAK::Saver& pSaver)
    {
        using namespace std::placeholders;

        QXmlStreamReader    lReader{pFile};
        qint64              lSize       = pFile->size();

        static auto         lFunctors   = std::map<QString, std::function<void (QXmlStreamReader&, PAK::Saver&)>>
        {
            { kBool,        std::bind(&Data::read<eBool>,       data(), _1, _2) },
            { kMask,        std::bind(&Data::read<eMask64>,     data(), _1, _2) },
            { kI8,          std::bind(&Data::read<eI8>,         data(), _1, _2) },
            { kU8,          std::bind(&Data::read<eU8>,         data(), _1, _2) },
            { kI16,         std::bind(&Data::read<eI16>,        data(), _1, _2) },
            { kU16,         std::bind(&Data::read<eU16>,        data(), _1, _2) },
            { kI32,         std::bind(&Data::read<eI32>,        data(), _1, _2) },
            { kU32,         std::bind(&Data::read<eU32>,        data(), _1, _2) },
            { kHex,         std::bind(&Data::read<eHex>,        data(), _1, _2) },
            { kI64,         std::bind(&Data::read<eI64>,        data(), _1, _2) },
            { kU64,         std::bind(&Data::read<eU64>,        data(), _1, _2) },
            { kF32,         std::bind(&Data::read<eF32>,        data(), _1, _2) },
            { kF64,         std::bind(&Data::read<eF64>,        data(), _1, _2) },
            { kFileHdl,     std::bind(&Data::read<eFileHdl>,    data(), _1, _2) },
            { kKey,         std::bind(&Data::read<eKey>,        data(), _1, _2) },
            { kString,      std::bind(&Data::read<eString>,     data(), _1, _2) },
            { kRGB,         std::bind(&Data::read<eRGB>,        data(), _1, _2) },
            { kRGBA,        std::bind(&Data::read<eRGBA>,       data(), _1, _2) },
            { kRect,        std::bind(&Data::read<eRect>,       data(), _1, _2) },
            { kV3,          std::bind(&Data::read<eVector3>,    data(), _1, _2) },
            { kQ,           std::bind(&Data::read<eQuaternion>, data(), _1, _2) },
            { kApp,         std::bind(&Data::read<eAppearance>, data(), _1, _2) },
            { kMat,         std::bind(&Data::read<eMaterial>,   data(), _1, _2) },
            { kGO,          std::bind(&Data::read<eGameObject>, data(), _1, _2) },
            { kContainer,   std::bind(&Data::read<eContainer>,  data(), _1, _2) },
        };

        int lFileVersion    = kVer;

        auto    lProgress   = data()->fileDB()->context()->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, static_cast<int>(lSize));
        lProgress->advance(0);

        while (!lReader.atEnd())
        {
            lReader.readNext();

            if      (lReader.isStartElement())
            {
                QStringRef  lType = lReader.name();

                if (!lType.isEmpty())
                {
                    auto    lIter = lFunctors.find(lType.toString());
                    if      (lIter != lFunctors.end())
                    {
                        lIter->second(lReader, pSaver);
                    }
                    else if (lType.toString() == kData)
                    {
                        QXmlStreamAttributes    lAttributes = lReader.attributes();
                        QStringRef              lVersion    = lAttributes.value(kVersion);

                        if (!lVersion.isEmpty())
                        {
                            lFileVersion = lVersion.toString().toInt();
                        }
                    }
                    else
                    {
                        qDebug() << QObject::tr("The element %1 in file %2 is unknown ").
                                            arg(lType.toString()).
                                            arg(pFile->fileName());

                        LAUNCH_EXCEPTION(kUnknownMarkup)
                    }
                }
            }

            lProgress->advance(static_cast<int>(pFile->pos()));
        }

        // Mise � jour du format des donn�es ?
        if (lFileVersion != version())
        {
            convertFromVersion(pSaver, lFileVersion);
        }

        return true;
    }


    /*! Importe les donn�es d'un fichier externe dans le <b>pak</b> file.
        @param pFile  Fichier syst�me dans lequel se trouvent les donn�es � importer
        @param pSaver Saver dans lequel empiler les donn�es lues, dans le format binaire interne du type
        de fichier import�
        @return VRAI si l'import a r�ussi, FAUX sinon
     */
    bool ImportExport::importsRaw(QFile* pFile, PAK::Saver& pSaver)
    {
        return doImportRaw(pFile, pSaver);
    }


    /*! Exporte les donn�es d'un fichier depuis le <b>pak</b> file au format texte.
        @param pFile   Fichier syst�me dans lequel �crire les donn�es � exporter
        @param pLoader Loader dans lequel se trouvent les donn�es � exporter, disponible dans le format
        binaire interne du type de fichier export�
        @return VRAI si l'export a r�ussi, FAUX sinon
     */
    bool ImportExport::exports(QFile* pFile, PAK::Loader& pLoader)
    {
        QXmlStreamWriter lWriter(pFile);

        // Header.
        lWriter.setAutoFormatting(true);
        lWriter.setCodec("UTF-8");
        lWriter.writeStartDocument("1.0");

        // Version.
        lWriter.writeStartElement(kData);
        lWriter.writeAttribute(kVersion, QString::number(version()));

        // Data.
        bool    lRes = doExport(lWriter, pLoader);

        lWriter.writeEndDocument();

        data()->fileDB()->diskFileSize(pLoader.fileHdl(), I64{pFile->size()});

        return lRes;
    }


    /*! Exporte les donn�es d'un fichier depuis le <b>pak</b> file au format binaire.
        @param pFile Fichier syst�me dans lequel �crire les donn�es � exporter
        @param pData Donn�es � exporter
        @return VRAI si l'export a r�ussi, FAUX sinon
     */
    bool ImportExport::exports(QFile* pFile, const Vector<U8>& pData)
    {
        QString lFileName       = pFile->fileName();
        int     lExtensionLen   = lFileName.length() - lFileName.lastIndexOf('.') - 1;
        QString lExtension      = lFileName.right(lExtensionLen);

        if      (lExtension == QStringLiteral("ttf"))
        {
            // Les fontes sont stock�es dans le pak file sous le format binaire natif.
            return pFile->write(reinterpret_cast<const char*>(&pData[0]), pData.size());
        }
        else if (lExtension == QStringLiteral("png"))
        {
            // QImage{Reader|Writer} + description (QImageWriter::setText + QImageReader::text).
            // 
            return true;
        }
        else
        {
            // Format non-pris en compte.
            return false;
        }
    }


    /*!
     */
    void ImportExport::bind(FileDB* pDB)
    {
        mData.reset(new Data(pDB));
    }


    /*!
     */
    ImportExport::Data* ImportExport::data() const
    {
        return mData.get();
    }


    /*! Le format binaire d'un type de fichier peut �voluer. En ce cas, un r�import est n�cessaire.
     */
    int ImportExport::version() const
    {
        return getVersion();
    }


    /*! @param pSaver   Donn�es � convertir (les donn�es au nouveau format devront �craser les anciennes)
        @param pVersion Version des donn�es dans <i>pSaver</i>
     */
    void ImportExport::convertFromVersion(PAK::Saver& /*pSaver*/, int /*pVersion*/)
    {
        // A impl�menter au besoin...
    }


    /*!
     */
    bool ImportExport::doImportRaw(QFile* /*pFile*/, PAK::Saver& /*pSaver*/)
    {
        // Par d�faut, pas d'import brut.
        return false;
    }


    /*! @fn void ImportExport::doMake(PAK::Saver& pSaver)
        @param pSaver Saver cr�� sur un fichier dans lequel envoyer les donn�es minimales pour le type de fichier associ� � l'instance
     */


    /*!
     */
    int ImportExport::getVersion() const
    {
        return kVer;
    }


//------------------------------------------------------------------------------
//                              Master Boot Record
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportMBR::~ImportExportMBR() { }


    /*!
     */
    bool ImportExportMBR::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        data()->write<eFileHdl>(pWriter, pLoader, "Game loop");
        data()->write<eFileHdl>(pWriter, pLoader, "Load menu");
        data()->write<eFileHdl>(pWriter, pLoader, "Game menu");
        data()->write<eFileHdl>(pWriter, pLoader, "Movie loop");
        data()->write<eFileHdl>(pWriter, pLoader, "Pause game");
        data()->write<eFileHdl>(pWriter, pLoader, "Run game");
        data()->write<eFileHdl>(pWriter, pLoader, "Save menu");
        data()->write<eFileHdl>(pWriter, pLoader, "Start menu");
        data()->write<eFileHdl>(pWriter, pLoader, "Sysconfig menu");
        data()->write<eFileHdl>(pWriter, pLoader, "Universe");
        data()->write<eFileHdl>(pWriter, pLoader, "Geometries");
        data()->write<eFileHdl>(pWriter, pLoader, "Images");
        data()->write<eFileHdl>(pWriter, pLoader, "Root world");

        return true;
    }


    /*!
     */
    void ImportExportMBR::doMake(PAK::Saver& pSaver)
    {
        Vector<PAK::FileHdl>   lHandles;

        // Etats de jeu.
        lHandles.push_back(data()->fileDB()->create("main.game"));
        lHandles.push_back(data()->fileDB()->create("load.game"));
        lHandles.push_back(data()->fileDB()->create("menu.game"));
        lHandles.push_back(data()->fileDB()->create("movie.game"));
        lHandles.push_back(data()->fileDB()->create("pause.game"));
        lHandles.push_back(data()->fileDB()->create("run.game"));
        lHandles.push_back(data()->fileDB()->create("save.game"));
        lHandles.push_back(data()->fileDB()->create("start.game"));
        lHandles.push_back(data()->fileDB()->create("sysconfig.game"));

        // Univers et mondes "racine".
        lHandles.push_back(data()->fileDB()->create("game.uni"));

        // Gestionnaires de ressources.
        lHandles.push_back(data()->fileDB()->create("geometry.rsc"));
        lHandles.push_back(data()->fileDB()->create("images.rsc"));

        lHandles.push_back(data()->fileDB()->create("engine.shd"));

        /* A (re)venir...
        if (true)   // Type de jeu == action-rpg (� mettre au point).
        {
            lHandles.push_back(create("main.out"));
            PAK::FileHdl   lOutHdl = lHandles.back();
            lHandles.push_back(create("main.und"));
            PAK::FileHdl   lUndHdl = lHandles.back();
            lHandles.push_back(create("main.wet"));
            PAK::FileHdl   lWetHdl = lHandles.back();
            {
                PAK::Saver lSaver(lOutHdl);
                lSaver << lWetHdl;
                OUT::World::saveDefault(lSaver);
            }
            {
                PAK::Saver lSaver(lUndHdl);
                lSaver << lWetHdl;
                UND::World::saveDefault(lSaver);
            }
            {
                PAK::Saver lSaver(lWetHdl);
                lSaver << lOutHdl;
                lSaver << lUndHdl;
            }
        }*/

        for(auto lH : lHandles)
        {
            pSaver << lH;
        }
    }


//------------------------------------------------------------------------------
//                          Gestionnaire de Ressources
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportResourceMgr::~ImportExportResourceMgr() { }


    /*!
     */
    bool ImportExportResourceMgr::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        U32 lKeyCount   = k0UL;
        pLoader >> lKeyCount;
        pLoader.rollback(U32(sizeof(lKeyCount)));

        data()->write<eU32>(pWriter, pLoader, "Key count");

        data()->begin(pWriter, pLoader, "File and Keys");
        {
            for(U32 lKey = k0UL; lKey < lKeyCount; ++lKey)
            {
                data()->write<eFileHdl>(pWriter, pLoader, "File");
                data()->write<eI32>(pWriter, pLoader, "Key");
            }        
        }
        data()->end(pWriter, pLoader, "File and Keys");

        data()->begin(pWriter, pLoader, "Handles");
        {
            U32 lUserData   = k0UL;
            pLoader >> lUserData;
            U32 lFreeSlots  = k0UL;
            pLoader >> lFreeSlots;
            pLoader.rollback(U32(sizeof(lUserData) + sizeof(lFreeSlots)));
            data()->write<eU32>(pWriter, pLoader, "Magic numbers");
            data()->write<eU32>(pWriter, pLoader, "Free slots");

            data()->begin(pWriter, pLoader, "Magic numbers");
            {
                for(U32 lM = k0UL; lM < lUserData; ++lM)
                {
                    data()->write<eU32>(pWriter, pLoader);
                }
            }
            data()->end(pWriter, pLoader, "Magic numbers");

            data()->begin(pWriter, pLoader, "Free slots");
            {
                for(U32 lF = k0UL; lF < lFreeSlots; ++lF)
                {
                    data()->write<eU32>(pWriter, pLoader);
                }
            }
            data()->end(pWriter, pLoader, "Free slots");
        }
        data()->end(pWriter, pLoader, "Handles");

        return true;
    }


    /*!
     */
    void ImportExportResourceMgr::doMake(PAK::Saver& pSaver)
    {
        pSaver << k0UL; // Nombre de clefs.

        pSaver << k0UL; // Nombres magiques.
        pSaver << k0UL; // Emplacement libres.
    }


//------------------------------------------------------------------------------
//                                      U32
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportU32::~ImportExportU32() { }


    /*!
     */
    bool ImportExportU32::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        data()->begin(pWriter, pLoader, "Data");
        {
            while(sizeof(U32) <= pLoader.remaining())
            {
                data()->write<eU32>(pWriter, pLoader);
            }
        }
        data()->end(pWriter, pLoader, "Data");

        return (pLoader.remaining() == 0);
    }


    /*!
     */
    void ImportExportU32::doMake(PAK::Saver&)
    {
        // Par d�faut, rien.
    }

    
//------------------------------------------------------------------------------
//                              Handles de Fichiers
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    ImportExportFileHdl::~ImportExportFileHdl() { }


    /*!
     */
    bool ImportExportFileHdl::doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader)
    {
        data()->begin(pWriter, pLoader, "Data");
        {
            while(sizeof(PAK::FileHdl) <= pLoader.remaining())
            {
                data()->write<eFileHdl>(pWriter, pLoader);
            }
        }
        data()->end(pWriter, pLoader, "Data");

        return (pLoader.remaining() == 0);
    }


    /*!
     */
    void ImportExportFileHdl::doMake(PAK::Saver&)
    {
        // Par d�faut, aucun handle de fichier.
    }
}
