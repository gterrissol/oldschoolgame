/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "FileService.hh"

/*! @file IDE/DATA/QPAcKage/FileService.cc
    @brief M�thodes (non-inline) de la classe QPAK::FileService.
    @author @ref Guillaume_Terrissol
    @date 22 Ao�t 2013 - 29 D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "FileDB.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! 
     */
    FileService::FileService()
        : OSGi::Service()
        , mDB{nullptr}
    { }


    /*! Defaulted.
     */
    FileService::~FileService() = default;


//------------------------------------------------------------------------------
//                             Gestion des Fichiers
//------------------------------------------------------------------------------

    /*!
     */
    PAK::FileHdl FileService::create(QString pFilename)
    {
        if (mDB != nullptr)
        {
            return mDB->create(pFilename);
        }
        else
        {
            return PAK::eBadHdl;
        }
    }


    /*!
     */
    PAK::FileHdl FileService::import(QString pSysFile)
    {
        if (mDB != nullptr)
        {
            return mDB->import(pSysFile);
        }
        else
        {
            return PAK::eBadHdl;
        }
    }


    /*!
     */
    void FileService::erase(QString pFilename)
    {
        if (mDB != nullptr)
        {
            return mDB->erase(pFilename);
        }
    }


    /*!
     */
    void FileService::erase(PAK::FileHdl pHdl)
    {
        if (mDB != nullptr)
        {
            return mDB->erase(pHdl);
        }
    }


    /*!
     */
    PAK::FileHdl FileService::packFileHdl(QString pFileName) const
    {
        if (mDB != nullptr)
        {
            return mDB->packFileHdl(pFileName);
        }
        else
        {
            return PAK::eBadHdl;
        }
    }

    /*!
     */
    QString FileService::packFileName(PAK::FileHdl pHdl) const
    {
        if (mDB != nullptr)
        {
            return mDB->packFileName(pHdl);
        }
        else
        {
            return {};
        }
    }

    /*!
     */
    QString FileService::root() const
    {
        if (mDB != nullptr)
        {
            return mDB->openedPakFileDir();
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void FileService::bind(FileDB* pDB)
    {
        mDB = pDB;
    }


//------------------------------------------------------------------------------
//                                Impl�mentation
//------------------------------------------------------------------------------

    /*!
     */
    bool FileService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string FileService::typeName() const
    {
        return typeid(*this).name();
    }
}
