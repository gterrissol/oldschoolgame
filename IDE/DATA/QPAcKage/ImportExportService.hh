/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_IMPORTEXPORTSERVICE_HH
#define QPAK_IMPORTEXPORTSERVICE_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/ImportExportService.hh
    @brief En-t�te de la classe QPAK::ImportExportService.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <QString>
#include <QStringList>

#include <OSGi/Context.hh>
#include <OSGi/Service.hh>

namespace QPAK
{
    /*! @brief Service : import/Export de fichier de pak file.
        @version 0.6

        Ce service est enregistr� sous le nom @b fr.osg.ide.pak.ie .
     */
    class ImportExportService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr           = std::shared_ptr<ImportExportService>;                 //!< Pointeur sur service.
        using Element       = std::shared_ptr<ImportExport>;                        //!< Pointeur sur instance d'import/export.
        using ContextPtr    = OSGi::Context*;                                       //!< Pointeur sur contexte.
        //@}    
        //! @name Constructeur & destructeur
        //@{    
                    ImportExportService();                                          //!< Constructeur par d�faut.
virtual             ~ImportExportService();                                         //!< Destructeur.
        //@}
        //! @name Fichiers d'extension
        //@{
        QString     extensionDir(QString pBundleName, ContextPtr pContext) const;   //!< R�pertoire d'extensions.
        QStringList extensionFiles() const;                                         //!< Liste des extensions enregistr�es.
        //@}
        //! @name Gestion des �l�ments
        //@{
        template<class TIE>
        void        checkIn(QString pExtension, QString pExtensionDir);             //!< Enregistrement d'une instance d'import/export.
        void        checkOut(QString pExtension);                                   //!< Retrait d'une instance d'import/export.
        bool        manages(QString pExtension) const;                              //!< Gestion d'une instance d'import/export ?
        Element     get(QString pExtension) const;                                  //!< R�cup�ration d'une instance d'import/export.
        //@}
        void        bind(FileDB* pDB);                                              //!< Liaison avec les fichiers de pak file.
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;            //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                                      //!< Nom du type (r�el).
        void        checkIn(QString pExtension, QString pFile, Element pInstance);  //!< Enregistrement d'une instance d'import/export.
        //@}
        QMap<QString, Element>  mElements;                                          //!< Instances d'import/export.
        QStringList             mExtensionFiles;                                    //!< Fichiers d'extension.
        FileDB*                 mDB;                                                //!< Syst�me de fichiers (@b pak file).
    };


    /*! @tparam TIE           Type d'import/exporteur � enregistrer
        @param  pExtension    Nom de l'extension li�e � l'import/exporteur
        @param  pExtensionDir R�pertoire contenant la d�finition d'extension li�e � l'import/exporteur
     */
    template<class TIE>
    void ImportExportService::checkIn(QString pExtension, QString pExtensionDir)
    {
        return checkIn(pExtension, QString{"%1/%2.xml"}.arg(pExtensionDir).arg(pExtension), std::make_shared<TIE>());
    }
}

#endif  // De QPAK_IMPORTEXPORTSERVICE_HH
