/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_QPACKAGE_HH
#define QPAK_QPACKAGE_HH

/*! @file IDE/DATA/QPAcKage/QPAcKage.hh
    @brief Pr�-d�clarations du module @ref QPAcKage.
    @author @ref Guillaume_Terrissol
    @date 19 Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QPAK
{
    class FileService;
    class ImportExport;
    class ImportExportService;

    class BlockMgr;
    class Builder;
    class Fat;
    class FileDB;
}

#endif  // De QPAK_QPACKAGE_HH
