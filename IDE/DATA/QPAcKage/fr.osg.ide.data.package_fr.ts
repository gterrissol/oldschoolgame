<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Builder</name>
    <message>
        <source>Project creation</source>
        <translation>Création de projet</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>New project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <source>FAT size</source>
        <translation>Taille de la FAT</translation>
    </message>
    <message>
        <source> files</source>
        <translation> fichiers</translation>
    </message>
    <message>
        <source>Memory overload</source>
        <translation>Occupation mémoire</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>Extension::Extension(QString)</name>
    <message>
        <source>Invalid extension file : %1</source>
        <translation>Fichier d&apos;extension invalide : %1</translation>
    </message>
    <message>
        <source>Couldn&apos;t open file %1</source>
        <translation>N&apos;a pu ouvrir le fichier %1</translation>
    </message>
</context>
<context>
    <name>ExtensionMgr::load(QStringList)</name>
    <message>
        <source>Invalid extension %1 defined in %2</source>
        <translation>Extension %1 définie dans %2 invalide</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The element %1 in file %2 is unknown </source>
        <translation>Dans le fichier %2, la balise %1 est inconnue</translation>
    </message>
</context>
<context>
    <name>QPAK::Builder</name>
    <message>
        <source> %1 bytes</source>
        <translation> %1 octets</translation>
    </message>
    <message>
        <source>Chose a database</source>
        <translation>Choisissez une base de données</translation>
    </message>
    <message>
        <source>Pak file FAT (FileDB.xml)</source>
        <translation>FAT de pak file (FileDB.xml)</translation>
    </message>
</context>
<context>
    <name>QPAK::View</name>
    <message>
        <source>Chose a pak file</source>
        <translation>Choisissez un pak file</translation>
    </message>
    <message>
        <source>Pak file opening</source>
        <translation>Ouverture de pak file</translation>
    </message>
    <message>
        <source>Opening file %1 failed</source>
        <translation>L&apos;ouverture du fichier %1 a échoué</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Répertoire</translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Contents</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <source>PAK file</source>
        <translation>Fichier PAK</translation>
    </message>
    <message>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <source>PAK size</source>
        <translation>Taille PAK</translation>
    </message>
    <message>
        <source>Disk size</source>
        <translation>Taille disque</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation>Mémoire</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Created</source>
        <translation>Créé</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <source> kB</source>
        <translation> ko</translation>
    </message>
    <message>
        <source>yyyy/MM/dd HH:mm</source>
        <translation>dd/MM/yyyy HH:mm</translation>
    </message>
</context>
<context>
    <name>operator&lt;&lt;(LOG::Log&amp;, const Extension&amp;)</name>
    <message>
        <source>missing key</source>
        <translation>Clef manquante</translation>
    </message>
    <message>
        <source>missing filename</source>
        <translation>Nom de fichier manquant</translation>
    </message>
    <message>
        <source>missing comment</source>
        <translation>Commentaire manquant</translation>
    </message>
    <message>
        <source>missing folder</source>
        <translation>Répertoire manquant</translation>
    </message>
    <message>
        <source>missing format</source>
        <translation>Format manquant</translation>
    </message>
</context>
</TS>
