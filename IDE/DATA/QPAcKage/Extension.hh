/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_EXTENSION_HH
#define QPAK_EXTENSION_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/Extension.hh
    @brief En-t�te des classes QPAK::Extension & QPAK::ExtensionMgr.
    @author @ref Guillaume_Terrissol
    @date 1er Mai 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QString>

#include "ERRor/ERRor.hh"
#include "MEMory/PImpl.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                            Gestion des Extensions
//------------------------------------------------------------------------------

    /*! @brief Extension de fichier "in pak".
        @version 0.5

        Une extension correspond � un type de donn�e g�r�e par le moteur (voir l'�diteur).@n
        Une extension permet de d�finir quelques informations sur la fa�on de traiter les donn�es dans l'�diteur.
     */
    class Extension
    {
    public:
        //! @name Constructeurs
        //@{
                    Extension();                    //!< ... par d�faut.
        explicit    Extension(QString pFileName);   //!< D�finition d'une extension.
        //@}
        //! @name R�cup�ration des attributs
        //@{
        QString     key() const;                    //!< Clef.
        QString     fileName() const;               //!< Nom de fichier.
        QString     comment() const;                //!< Commentaire.
        QString     folder() const;                 //!< R�pertoire.
        QString     format() const;                 //!< Format.
        //@}
        //! @name Informations
        //@{
        bool        isFilled() const;               //!< Est compl�tement renseign� ?
        bool        isValid() const;                //!< Est valide ?
        //@}

    private:
        //! @name Attributs
        //@{
        QString     mKey;                           //!< Clef.
        QString     mFileName;                      //!< Nom de fichier.
        QString     mComment;                       //!< Commantaire.
        QString     mFolder;                        //!< R�pertoire.
        QString     mFormat;                        //!< Format.
        //@}
    };


    /*! Affichage d'une extension.
        @param pLog  Log sur lequel afficher <i>pExt</i>
        @param pExt Extension dont afficher les champs [d�finis]
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const Extension& pExt);


    /*! @brief Gestionnaire d'extensions "in pak".
        @version 0.7

        Ce gestionnaire fait le lien entre les extensions des fichiers contenus dans un @b pak file, et le placement sur disque de
        leur pendant xml (ou brut).
     */
    class ExtensionMgr
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    ExtensionMgr();                             //!< Constructeur.
                    ~ExtensionMgr();                            //!< Destructeur.
        //@}
        //! @name Gestion de l'arborescence
        //@{
        void        loadDefinitions(QStringList pFileNames);    //!< Chargement des fichiers d'extensions.
        QStringList tree() const;                               //!< Arborescence.
        //@}
        //! @name
        //@{
        QString     diskFile(QString pPackFile) const;          //!< Nom d'un fichier syst�me � partir du nom dans le @b pak file.
        QString     packFile(QString pDiskFile) const;          //!< Nom d'un fichier du @b pak file � partir du nom syst�me.
        QString     comment(QString pPackFile) const;           //!< Commentaire associ� � l'extension d'un fichier.
 static QString     extension(QString pPackFile);               //!< Extension d'un fichier du @b pak file.
 static QString     radical(QString pPackFile);                 //!< Radical du nom d'un fichier du @b pak file.
        //@}
        //! @name Autre information
        //@{
        bool        exists(QString pExtension) const;           //!< Existence d'une extension.
        //@}

    private:

        FORBID_COPY(ExtensionMgr)
        FREE_PIMPL()
    };
}

#endif  // De QPAK_EXTENSION_HH
