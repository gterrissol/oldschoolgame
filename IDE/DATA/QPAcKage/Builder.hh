/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_BUILDER_HH
#define QPAK_BUILDER_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/Builder.hh
    @brief En-t�te de la classe QPAK::Builder.
    @author @ref Guillaume_Terrissol
    @date 30 Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDialog>

#include "CORE/Private.hh"

namespace QPAK
{
    /*! @brief Interface de cr�ation d'un projet (<b>pak</b> file).
        @version 0.5

     */
    class Builder : public QDialog
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
        Builder(QString pDefaultPath, QWidget* pParent = 0);   //!< Constructeur
virtual ~Builder();                                            //!< Destructeur.
        //@}
        //! @name Informations saisies
        //@{
        int     fatSize() const;                                //!< 
        QString dataBase() const;                               //!< 
        QString pakFileName() const;                            //!< 
        //@}
    private slots:

        void    onFileCountChange(int pCount);                  //!< 
        void    onDataBaseSelect();                             //!< 
        void    onGroupSelected();                              //!< 
        void    validatePakFileName(const QString& pFileName);  //!< 

    private:

        Q_CUSTOM_DECLARE_PRIVATE(Builder)
    };
}


#endif  // De QPAK_BUILDER_HH
