/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_ALL_HH
#define QPAK_ALL_HH

/*! @file IDE/DATA/QPAcKage/All.hh
    @brief Interface publique du module @ref QPAcKage.
    @author @ref Guillaume_Terrissol
    @date 19 Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QPAK   //! Gestion du <b>pak</b> file par l'IDE.
{
    /*! @namespace QPAK
        @version 0.2

        Extension de PAK pour l'IDE.
     */

    /*! @defgroup QPAcKage QPAcKage : Extension de @ref PAcKage pour l'IDE
        <b>namespace</b> QPAK.
     */
}

#include "FileDB.hh"

#endif  // De QPAK_ALL_HH
