/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Builder.hh"

/*! @file IDE/DATA/QPAcKage/Builder.cc
    @brief M�thodes (non-inline) de la classe QPAK::Builder.
    @author @ref Guillaume_Terrissol
    @date 30 Janvier 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDir>
#include <QFileDialog>
#include <QPushButton>

#include "PAcKage/Enum.hh"
#include "PAcKage/File.hh"

#include "Builder.ui.hh"

namespace
{
    const QString   kPak = PAK::kPakFileExtension;
}

namespace QPAK
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Builder.
        @version 0.4
     */
    class Builder::BuilderPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Builder)
    public:
        //! @name "Constructeurs"
        //@{
                    BuilderPrivate(Builder* pThat); //!< Constructeur.
        void        init(QString pDefaultPath);     //!< Initialisation.
        //@}
        Ui::Builder     m;                          //!< Interface cr��e par Designer.
        QPushButton*    mOkButton;                  //!< 
        QString         mDefaultPath;               //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Builder::BuilderPrivate::BuilderPrivate(Builder* pThat)
        : q{pThat}
        , m{}
        , mOkButton{nullptr}
        , mDefaultPath{""}
    { }


    /*!
     */
    void Builder::BuilderPrivate::init(QString pDefaultPath)
    {
        Q_ASSERT_X(QDir(pDefaultPath).exists(), "void Builder::BuilderPrivate::init(QString pDefaultPath)", "Pak file path must exist");

        Q_Q(Builder);

        m.setupUi(q);

        mOkButton       = q->findChild<QDialogButtonBox*>("buttonBox")->button(QDialogButtonBox::Ok);

        q->connect(m.projectName,          SIGNAL(textEdited(const QString&)), SLOT(validatePakFileName(const QString&)));
        q->connect(m.newProject,           SIGNAL(clicked(bool)),              SLOT(onGroupSelected()));
        q->connect(m.fileCount,            SIGNAL(valueChanged(int)),          SLOT(onFileCountChange(int)));
        q->connect(m.dataBase,             SIGNAL(clicked(bool)),              SLOT(onGroupSelected()));
        q->connect(m.selectDataBaseButton, SIGNAL(clicked()),                  SLOT(onDataBaseSelect()));

        q->onFileCountChange(m.fileCount->value());

        mDefaultPath = pDefaultPath;

        QString lDefaultFileName    = "osg_000";
        QString lTestFileName       = mDefaultPath + "/osg_000" + kPak;
        QDir    lPakDirectory(mDefaultPath, QString("osg_*%1").arg(kPak), QDir::Name | QDir::Reversed);

        if (lPakDirectory.count() != 0)
        {
            QString lLastFileName   = lPakDirectory[0];   // Rappel : les fichiers sont tri�s � l'envers.
            int     iNumber         = lLastFileName.remove("osg_").remove(kPak).toInt();
            lDefaultFileName        = QString("osg_%1").arg(iNumber + 1, 3, 10, QChar('0'));
        }
        m.projectName->setText(lDefaultFileName);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Builder::Builder(QString pDefaultPath, QWidget* pParent)
        : QDialog(pParent)
        , pthis{this}
    {
        Q_D(Builder);

        d->init(pDefaultPath);
    }


    /*! Destructeur.
     */
    Builder::~Builder() { }


//------------------------------------------------------------------------------
//                                  
//------------------------------------------------------------------------------

    /*!
     */
    int Builder::fatSize() const
    {
        Q_D(const Builder);

        return d->m.fileCount->value();
    }


    /*!
     */
    QString Builder::dataBase() const
    {
        Q_D(const Builder);

        return d->m.dataBaseDir->text();
    }


    /*!
     */
    QString Builder::pakFileName() const
    {
        Q_D(const Builder);

        return QString("%1/%2%3").arg(d->mDefaultPath).arg(d->m.projectName->text()).arg(kPak);
    }


//------------------------------------------------------------------------------
//                                  
//------------------------------------------------------------------------------

    /*!
     */
    void Builder::onFileCountChange(int pCount)
    {
        Q_D(Builder);

        d->m.memoryOverload->setText(tr(" %1 bytes").arg(pCount * sizeof(PAK::File)));
    }


    /*!
     */
    void Builder::onDataBaseSelect()
    {
        Q_D(Builder);

        QString lFileName   = QFileDialog::getOpenFileName(this,
                                                           tr("Chose a database"),
                                                           d->mDefaultPath,
                                                           tr("Pak file FAT (FileDB.xml)"));

        if (!lFileName.isEmpty())
        {
            d->m.dataBaseDir->setText(lFileName);
        }

        d->mOkButton->setEnabled(!d->m.dataBaseDir->text().isEmpty());
    }


    /*!
     */
    void Builder::onGroupSelected()
    {
        Q_D(Builder);

        if (sender() == d->m.newProject)
        {
            d->m.dataBase->setChecked(false);
            d->mOkButton->setEnabled(true);
        }
        else    // (sender() == d->m.dataBase)
        {
            d->m.newProject->setChecked(false);
            d->mOkButton->setEnabled(!d->m.dataBaseDir->text().isEmpty());
        }
    }


    /*! En fonction du nom de fichier choisi pour le <b>pak</b> file, active ou d�sactive le bouton de
        validation du dialogue.
        @param pFileName Nom de fichier choisi pour le <b>pak</b> file
     */
    void Builder::validatePakFileName(const QString& pFileName)
    {
        Q_D(Builder);

        QString lCorrectedName  = pFileName;
        lCorrectedName.replace(QRegExp("\\W"), "_");

        if (lCorrectedName != pFileName)
        {
            d->m.projectName->setText(lCorrectedName);
        }
    }
}
