/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ImportExportService.hh"

/*! @file IDE/DATA/QPAcKage/ImportExportService.cc
    @brief M�thodes (non-inline) de la classe QPAK::ImportExportService.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 5 Juillet 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <stdexcept>

#include <QDebug>

#include "ImportExport.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! 
     */
    ImportExportService::ImportExportService()
        : OSGi::Service()
        , mElements{}
        , mDB{nullptr}
    { }


    /*! Defaulted.
     */
    ImportExportService::~ImportExportService() = default;


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*! @param pBundleName Nom du bundle d�finissant l'import/Exporteur dont r�cup�rer le fichier d'extension
        @param pContext    Contexte d'ex�cution
        @return Le r�pertoire contenant les fihciers d'extension du bundle @p pBundleName
     */
    QString ImportExportService::extensionDir(QString pBundleName, ContextPtr pContext) const
    {
        return QStringLiteral("%1/%2/extensions").arg(pContext->temporaryPath().c_str()).arg(pBundleName);
    }


    /*! @return La liste des fichiers d'extensions pour les import/exporteurs enregistr�s
     */
    QStringList ImportExportService::extensionFiles() const
    {
        return mExtensionFiles;
    }


    /*! @param pExtension Nom de l'extension
        @param pFile      Fichier de l'extension
        @param pInstance  L'import/exporteur proprement dit
     */
    void ImportExportService::checkIn(QString pExtension, QString pFile, Element pInstance)
    {
        if (!manages(pExtension))
        {
            pInstance->bind(mDB);
            mElements[pExtension] = pInstance;
            mExtensionFiles << pFile;
        }
        else
        {
            throw std::runtime_error(qPrintable(QStringLiteral("Extension %1 already registered").arg(pExtension)));
        }
    }


    /*! @param pExtension Nom d'extension li�e � l'import/exporteur � supprimer
     */
    void ImportExportService::checkOut(QString pExtension)
    {
        mElements.remove(pExtension);
    }


    /*! @param pExtension Extension � contr�ler
        @retval true  Si l'extension @p pExtension � �t� enregistr�e
        @retval false Sinon
     */
    bool ImportExportService::manages(QString pExtension) const
    {
        return mElements.contains(pExtension);
    }


    /*! @param pExtension Extension dont r�cup�rer l'import/exporteur
        @return L'import/exporteur li� � @p pExtension
        @throw ERR::Exception si @p pExtension n'a pas �t� enregsitr�e
     */
    ImportExportService::Element ImportExportService::get(QString pExtension) const
    {
        auto lElement = mElements.find(pExtension);
        if (lElement != mElements.end())
        {
            return *lElement;
        }
        else
        {
            LAUNCH_EXCEPTION(qPrintable(QStringLiteral("No extension %1 registered").arg(pExtension)));
        }
    }


    /*! @param pDB Base de donn�es d'un @b pak file � lier aux import/exporteurs enregistr�s
     */
    void ImportExportService::bind(FileDB* pDB)
    {
        mDB = pDB;
        for(auto lElt : mElements)
        {
            lElt->bind(mDB);
        }
    }


//------------------------------------------------------------------------------
//                                Impl�mentation
//------------------------------------------------------------------------------

    /*!
     */
    bool ImportExportService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string ImportExportService::typeName() const
    {
        return typeid(*this).name();
    }
}
