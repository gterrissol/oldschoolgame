/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_IMPORTEXPORT_HH
#define QPAK_IMPORTEXPORT_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/ImportExport.hh
    @brief En-t�te des classes QPAK::ImportExport & d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 1er Mai 2008 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QString>

#include "MEMory/BuiltIn.hh"
#include "PAcKage/Handle.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/Vector.hh"

class QFile;
class QXmlStreamReader;
class QXmlStreamWriter;

namespace QPAK
{
    /*! @brief Fonctions d'import/export pour un type de donn�es.
        @version 0.5

        Classe � d�river pour chaque type de fichier, afin de fournir les m�canismes d'import/export (vers et depuis le pak file) et
        de versionnement de ces types.
     */
    class ImportExport
    {
    public:

virtual         ~ImportExport();                                                //!< Destructeur.
        //! @name "D�finition"
        //@{
        void    make(QFile* pSystemFile, PAK::FileHdl pPackedFile);             //!< Cr�ation d'un fichier par d�faut.
        //@}
        //! @name Import/export
        //@{
        bool    imports(QFile* pFile, PAK::Saver& pSaver);                      //!< Import d'un fichier "format�".
        bool    importsRaw(QFile* pFile, PAK::Saver& pSaver);                   //!< Import d'un fichier "brut".
        bool    exports(QFile* pFile, PAK::Loader& pLoader);                    //!< Export en format texte.
        bool    exports(QFile* pFile, const Vector<U8>& pData);                 //!< Export en format binaire.
        int     version() const;                                                //!< Version du format d'import/Export.
        //@}
        void    bind(FileDB* pDB);                                              //!< Liaison avec une base de donn�es fichier.


    protected:

        enum EType                                                              //!< Types des donn�es "�l�mentaires".
        {
            eBool,                                                              //!< Bool�en.
            eMask16,                                                            //!< Masque binaire 16 bits.
            eMask32,                                                            //!< Masque binaire 32 bits.
            eMask64,                                                            //!< Masque binaire 64 bits.
            eI8,                                                                //!< Entier sign� 8 bits.
            eU8,                                                                //!< Entier non-sign� 8 bits.
            eI16,                                                               //!< Entier sign� 16 bits.
            eU16,                                                               //!< Entier non-sign� 16 bits.
            eI32,                                                               //!< Entier sign� 32 bits.
            eU32,                                                               //!< Entier non-sign� 32 bits.
            eHex,                                                               //!< Entier hexad�cimal 32 bits.
            eI64,                                                               //!< Entier sign� 64 bits.
            eU64,                                                               //!< Entier non-sign� 64 bits.
            eF32,                                                               //!< Flottant 32 bits.
            eF64,                                                               //!< Flottant 64 bits.
            eFileHdl,                                                           //!< Handle de fichier.
            eKey,                                                               //!< Clef.
            eString,                                                            //!< Cha�ne C++.
            eRGB,                                                               //!< Couleur RGB.
            eRGBA,                                                              //!< Couleur RGBA.
            eRect,                                                              //!< Rectangle.
            eContainer,                                                         //!< Conteneur (groupe).
            eVector3,                                                           //!< Vecteur 3D.
            eQuaternion,                                                        //!< Quaternion.
            eMaterial,                                                          //!< Mat�riau.
            eAppearance,                                                        //!< Apparance.
            eGameObject                                                         //!< Objet de jeu.
        };

        /*! @brief Gestion des types de donn�es �l�mentaires.
            @version 0.2

            Cette classe regroupe les fonctions d'import/export des types de donn�es �l�mentaires.@n
            Afin de limiter l'interface publique, seules deux m�thodes template sont fournies; elles sont sp�cialis�es pour les valeurs
            de EType.
         */
        class Data
        {
        public:

            using EType = ImportExport::EType;                                  //!< Simple alias.

                    Data(FileDB* pDB);                                          //!< Constructeur.
            FileDB* fileDB() const;                                             //!< Acc�s � la base de donn�es.
            //! @name Manipulation des donn�es
            //@{
            template<EType>
            void    read(QXmlStreamReader&  pReader, PAK::Saver&  pSaver);      //!< Lecture.
            template<EType>
            void    write(QXmlStreamWriter& pWriter, PAK::Loader& pLoader,
                          const QString& pName = QString::null);                //!< Ecriture.
            //@}
            //! @name Groupement de donn�es
            //@{
            void    begin(QXmlStreamWriter& pWriter, PAK::Loader&,
                          const QString& pName = QString::null);                //!< D�but de groupe.
            void    end(QXmlStreamWriter& pWriter, PAK::Loader&,
                        const QString& = QString::null);                        //!< Fin de groupe.
            //@}

        private:

            FileDB* mDB;                                                        //!< Base de donn�es.
        };

        Data*   data() const;                                                   //!< Manipulateur de donn�es.

    private:
        //! @name Impl�mentation
        //@{
virtual void    convertFromVersion(PAK::Saver& pSaver, int pVersion);           //!< Conversion des donn�es depuis une version ant�rieure.
virtual bool    doImportRaw(QFile* pFile, PAK::Saver& pSaver);                  //!< Import de donn�es brutes.
virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) = 0;  //!< Export.
virtual void    doMake(PAK::Saver& pSaver) = 0;                                 //!< Enregistrement de donn�es minimales.
virtual int     getVersion() const;                                             //!< Version du format d'import/Export.
        //@}

        std::unique_ptr<Data>   mData;                                          //!< Manipulateur de donn�es.
    };


    /*! @brief Import/export de MBR.
        @version 0.25
     */
    class ImportExportMBR : public ImportExport
    {
    public:

virtual         ~ImportExportMBR();                                                 //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


    /*! @brief Import/export de gestionnaire de ressources.
        @version 0.25
     */
    class ImportExportResourceMgr : public ImportExport
    {
    public:

virtual         ~ImportExportResourceMgr();                                         //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!<
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< 
    };


    /*! @brief Import/export de donn�es enti�res.
        @version 0.25
     */
    class ImportExportU32 : public ImportExport
    {
    public:

virtual         ~ImportExportU32();                                                 //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!< @copybrief ImportExport::doExport(QXmlStreamWriter&, PAK::Loader&)
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< @copybrief ImportExport::doMake(PAK::Saver&)
    };


    /*! @brief Import/export de handles de fichier.
        @version 0.25
     */
    class ImportExportFileHdl : public ImportExport
    {
    public:

virtual         ~ImportExportFileHdl();                                             //!< Destructeur.


    private:

virtual bool    doExport(QXmlStreamWriter& pWriter, PAK::Loader& pLoader) override; //!< @copybrief ImportExport::doExport(QXmlStreamWriter&, PAK::Loader&)
virtual void    doMake(PAK::Saver& pSaver) override;                                //!< @copybrief ImportExport::doMake(PAK::Saver&)
    };
}

#endif  // De QPAK_IMPORTEXPORT_HH
