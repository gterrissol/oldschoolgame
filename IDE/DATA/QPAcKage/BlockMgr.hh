/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_BLOCKMGR_HH
#define QPAK_BLOCKMGR_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/BlockMgr.hh
    @brief En-t�te de la classe QPAK::BlockMgr.
    @author @ref Guillaume_Terrissol
    @date 25 Juillet 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/Handle.hh"
#include "STL/Vector.hh"

namespace QPAK
{
    /*! @brief Gestionnaire de ressources (blocs de donn�es dans le pak file).
        @version 0.95

        Cette classe a pour but de g�rer les blocs de donn�es d'un pak file : allocation, suppression,
        d�placement,...
     */
    class BlockMgr : public MEM::OnHeap
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        BlockMgr(PAK::BlockHdl pBlockHdl,
                                  const Vector<PAK::File>& pFiles);    //!< Constructeur.
                        ~BlockMgr();                                   //!< Destructeur.
        //@}
        //! @name Gestion des blocs de donn�es
        //@{
        PAK::BlockHdl  acquireBlock(U32 pBlockSize);                   //!< Acquisition d'un bloc.
        PAK::BlockHdl  resizeBlock(PAK::BlockHdl pBlockHdl,
                                    U32 pBlockSize,
                                    U32 pNewBlockSize);                 //!< Redimensionnement d'un bloc.
        void            releaseBlock(PAK::BlockHdl pBlockHdl,
                                     U32 pBlockSize);                   //!< Lib�ration d'un bloc.
        //@}

    private:

        FORBID_COPY(BlockMgr)
        FREE_PIMPL()
    };
}

#endif  // De QPAK_BLOCKMGR_HH
