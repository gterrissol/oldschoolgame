/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Extension.hh"

/*! @file IDE/DATA/QPAcKage/Extension.cc
    @brief M�thodes (non-inline) des classes QPAK::Extension & QPAK::ExtensionMgr.
    @author @ref Guillaume_Terrissol
    @date 1er Mai 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QApplication>
#include <QDateTime>
#include <QFile>
#include <QMap>
#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QXmlStreamReader>

#include "ERRor/Assert.hh"
#include "ERRor/Exception.hh"
#include "ERRor/Log.hh"
#include "PAcKage/Handle.hh"

#include "ErrMsg.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//                  Extension : 
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    Extension::Extension() = default;


    /*! @param pFileName Fichier contenant les donn�es de l'extension
     */
    Extension::Extension(QString pFileName)
        : mKey()
        , mFileName()
        , mComment()
        , mFolder()
        , mFormat()
    {
        static const char* kErrorReading = QT_TRANSLATE_NOOP("Extension::Extension(QString)", "Invalid extension file : %1");
        static const char* kErrorOpening = QT_TRANSLATE_NOOP("Extension::Extension(QString)", "Couldn't open file %1");

        QFile   lFile(pFileName);

        if (lFile.open(QFile::ReadOnly | QFile::Text))
        {
            QXmlStreamReader    lReader;
            lReader.setDevice(&lFile);
            lReader.readNext();

            if (lReader.isStartDocument())  // <?xml version="1.0" encoding="UTF-8" ?>
            {
                lReader.readNext();

                if (lReader.isStartElement() && (lReader.name() == "extension"))    // <extension type="...">
                {
                    while(!lReader.atEnd())
                    {
                        lReader.readNext();

                        if (lReader.isStartElement())
                        {
                            if      (lReader.name() == "key")
                            {
                                mKey = lReader.readElementText();
                            }
                            else if (lReader.name() == "filename")
                            {
                                mFileName = lReader.readElementText();
                            }
                            else if (lReader.name() == "comment")
                            {
                                mComment = lReader.readElementText();
                            }
                            else if (lReader.name() == "folder")
                            {
                                mFolder = lReader.readElementText();
                            }
                            else if (lReader.name() == "format")
                            {
                                mFormat = lReader.readElementText();
                            }
                        }
                        else if (lReader.isEndElement())
                        {
                            if (lReader.name() == "extension")
                            {
                                if (isFilled())
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            LAUNCH_EXCEPTION(qPrintable(qApp->translate("Extension::Extension(QString)", kErrorReading).arg(pFileName)))
        }
        else
        {
            LAUNCH_EXCEPTION(qPrintable(qApp->translate("Extension::Extension(QString)", kErrorOpening).arg(pFileName)))
        }
    }


//------------------------------------------------------------------------------
//                  Extension : R�cup�ration des attributs
//------------------------------------------------------------------------------

    /*!
     */
    QString Extension::key() const
    {
        return mKey;
    }


    /*!
     */
    QString Extension::fileName() const
    {
        return mFileName;
    }


    /*!
     */
    QString Extension::comment() const
    {
        return mComment;
    }


    /*!
     */
    QString Extension::folder() const
    {
        return mFolder;
    }


    /*!
     */
    QString Extension::format() const
    {
        return mFormat;
    }


//------------------------------------------------------------------------------
//                           Extension : Informations
//------------------------------------------------------------------------------

    /*!
     */
    bool Extension::isFilled() const
    {
        return !(mKey.isEmpty()      ||
                 mFileName.isEmpty() ||
                 mComment.isEmpty()  ||
                 mFolder.isEmpty()   ||
                 mFormat.isEmpty());
    }


    /*!
     */
    bool Extension::isValid() const
    {
        return isFilled();
    }


//------------------------------------------------------------------------------
//                           Affichage d'une Extension
//------------------------------------------------------------------------------

    /*! Affichage d'une extension.
        @param pLog Log sur lequel afficher <i>pExt</i>
        @param pExt Extension dont afficher les champs [d�finis]
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const Extension& pExt)
    {
        static const char* lErrMessages[] =
        {
            QT_TRANSLATE_NOOP("operator<<(LOG::Log&, const Extension&)", "missing key"),
            QT_TRANSLATE_NOOP("operator<<(LOG::Log&, const Extension&)", "missing filename"),
            QT_TRANSLATE_NOOP("operator<<(LOG::Log&, const Extension&)", "missing comment"),
            QT_TRANSLATE_NOOP("operator<<(LOG::Log&, const Extension&)", "missing folder"),
            QT_TRANSLATE_NOOP("operator<<(LOG::Log&, const Extension&)", "missing format")
        };

        QString lText = "< ";
        lText += (!pExt.key().isEmpty()      ? pExt.key()      : qApp->translate("operator<<(LOG::Log&, const Extension&)", lErrMessages[0])) + "; ";
        lText += (!pExt.fileName().isEmpty() ? pExt.fileName() : qApp->translate("operator<<(LOG::Log&, const Extension&)", lErrMessages[1])) + "; ";
        lText += (!pExt.comment().isEmpty()  ? pExt.comment()  : qApp->translate("operator<<(LOG::Log&, const Extension&)", lErrMessages[2])) + "; ";
        lText += (!pExt.folder().isEmpty()   ? pExt.folder()   : qApp->translate("operator<<(LOG::Log&, const Extension&)", lErrMessages[3])) + "; ";
        lText += (!pExt.format().isEmpty()   ? pExt.format()   : qApp->translate("operator<<(LOG::Log&, const Extension&)", lErrMessages[4])) + " >";

        pLog << qPrintable(lText);

        return pLog;
    }


//------------------------------------------------------------------------------
//                             P-Impl d'ExtensionMgr
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPAK::ExtensionMgr.
        @version 0.4
     */
    class ExtensionMgr::Private
    {
    public:
        //! @name Attribut
        //@{
        using Container = QMap<QString, Extension>; //!< 
        Container   mExtensions;                    //!< 
        //@}
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    ExtensionMgr::ExtensionMgr()
        : pthis{}
    { }


    /*! Destructeur.
     */
    ExtensionMgr::~ExtensionMgr() = default;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void ExtensionMgr::loadDefinitions(QStringList pFileNames)
    {
        static const char*  kInvalidExtension = QT_TRANSLATE_NOOP("ExtensionMgr::load(QStringList)", "Invalid extension %1 defined in %2");

        QRegExp lRE(R"(.*(\w+).xml$)");

        for(auto lFilename : pFileNames)
        {
            if (-1 < lRE.indexIn(lFilename))
            {
                QString lKey = lRE.cap(1);
                auto    lExtension = Extension{lFilename};
                if (lKey == lExtension.key())
                {
                    pthis->mExtensions[lKey] = lExtension;
                }
                else
                {
                    LAUNCH_EXCEPTION(qPrintable(qApp->translate("ExtensionMgr::load(QStringList)", kInvalidExtension).arg(lExtension.key()).arg(lFilename)))
                }
            }
        }
    }


    /*!
     */
    QStringList ExtensionMgr::tree() const
    {
        QStringList lTree;

        for(auto lExtension : pthis->mExtensions)
        {
            lTree << lExtension.folder();
        }

        return lTree;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QString ExtensionMgr::diskFile(QString pPackFile) const
    {
        QString lKey = extension(pPackFile);

        if (pthis->mExtensions.contains(lKey))
        {
            const auto& lExtension  = pthis->mExtensions[lKey];
            QString     lDiskFile   = radical(pPackFile) + "." + extension(lExtension.fileName());

            ASSERT_EX(QRegExp(lExtension.fileName()).exactMatch(lDiskFile), kInvalidFileName, return QString::null;)

            return lExtension.folder() + lDiskFile;
        }
        else
        {
            return QString::null;
        }
    }


    /*!
     */
    QString ExtensionMgr::packFile(QString pDiskFile) const
    {
        for(auto lExtension : pthis->mExtensions)
        {
            QRegExp lRegExp(lExtension.fileName());
            int     lPos = lRegExp.indexIn(pDiskFile);
            if (-1 < lPos)
            {
                QString lPackFile = lRegExp.cap(1) + "." + lExtension.key();
                return lPackFile;
            }
        }

        return QString::null;
    }


        
    /*!
     */
    QString ExtensionMgr::comment(QString pPackFile) const
    {
        QString lKey = extension(pPackFile);

        if (pthis->mExtensions.contains(lKey))
        {
            return pthis->mExtensions[lKey].comment();
        }
        else
        {
            return QString::null;
        }
    }


    /*!
     */
    QString ExtensionMgr::extension(QString pPackFile)
    {
        int lExtensionLen = pPackFile.length() - pPackFile.lastIndexOf(".") - 1;
        return pPackFile.right(lExtensionLen);
    }


    /*!
     */
    QString ExtensionMgr::radical(QString pPackFile)
    {
        int lBeforeDot = pPackFile.lastIndexOf(".");
        return pPackFile.left(lBeforeDot);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool ExtensionMgr::exists(QString pExtension) const
    {
        return pthis->mExtensions.contains(pExtension);
    }
}
