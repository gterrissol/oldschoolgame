/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "View.hh"

/*! @file IDE/DATA/QPAcKage/View.cc
    @brief M�thodes (non-inline) de la classe QPAK::View.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QKeyEvent>
#include <QMessageBox>

#include "CORE/MenuService.hh"
#include "CORE/SyncService.hh"
#include "PAcKage/File.hh"

#include "FileDB.hh"
#include "View.ui.hh"

namespace QPAK
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    namespace
    {
        const QDateTime kNullDate{QDate{1970, 1, 1}};
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de View.
        @version 0.4
     */
    class View::ViewPrivate : public QObject
    {
        Q_CUSTOM_DECLARE_PUBLIC(View)
    public:
        //! @name "Constructeurs"
        //@{
                ViewPrivate(View* pThat, OSGi::Context* pContext);  //!< Constructeur.

        //@}

        //! @name "Slots"
        //@{
        void    onNewFile();
        void    onOpenFile();
        void    onSaveFile();
        void    onCloseFile();

        void    onActivatePakFile(const QModelIndex&);
        void    onSelectedPakFile(const QModelIndex&);
        void    onSelectedFile(const QModelIndex&);
        //@}
        //! @name Mise � jour "activante" : pak file
        //@{
        void    setPakFilename(QString pValue);
        void    resetPakFilename();
        void    setPakFileCounts(int pCurrent, int pTotal);
        void    resetPakFileCounts();
        void    setAllPakSize(qint64 pValue);
        void    resetAllPakSize();
        void    setAllDiskSize(qint64 pValue);
        void    resetAllDiskSize();
        void    setMemory(int pValue);
        void    resetMemory();
        void    setPakDates(QDateTime pCreate, QDateTime pModify);
        void    resetPakDates();
        //@}
        //! @name Mise � jour "activante" : fichier
        //@{
        void    setFilename(QString pValue);
        void    resetFilename();
        void    setFileSize(qint64 pValue);
        void    resetFileSize();
        void    setDiskSize(qint64 pValue);
        void    resetDiskSize();
        void    setFileType(QString pValue);
        void    resetFileType();
        void    setFileVersion(QString pValue);
        void    resetFileVersion();
        void    setFileDates(QDateTime pCreate, QDateTime pModify);
        void    resetFileDates();
        void    setFileContents(QString pText);
        void    resetFileContents();
        //@}


    protected:

virtual bool eventFilter(QObject* pObject, QEvent* pEvent) override;

    private:

        QString createFile();
        bool    openFile(QString pFilename);
        void    saveFile();
        void    closeFile();

        void    showPakFiles();
        void    showPakTree(QString pDirName);

        void    updatePakFileInfo(QString pPakFilename = QString::null);
        void    updateFileInfo(QString pFilename = QString::null);

        Ui::View            m;                                          //!< Interface cr��e par Designer.
        FileDB              mDB;
        OSGi::Context*      mContext;
        QFileSystemModel*   mListModel;
        QFileSystemModel*   mTreeModel;

        bool                mFileOpened;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    View::ViewPrivate::ViewPrivate(View* pThat, OSGi::Context* pContext)
        : q{pThat}
        , m{}
        , mDB{pContext}
        , mContext{pContext}
        , mTreeModel{nullptr}
        , mFileOpened{false}
    {
        q->from(q);
        m.setupUi(q);

        mListModel = new QFileSystemModel{m.pakFilesList};
        mListModel->setRootPath(mDB.pakFilesDir());
        mListModel->setReadOnly(true);
        mListModel->setFilter(QDir::Files);
        mListModel->setNameFilters(QStringList{QString{"*.pak"}});
        mListModel->setNameFilterDisables(false);
        m.pakFilesList->setModel(mListModel);
        m.pakFilesList->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

        mTreeModel = new QFileSystemModel{m.pakFileTreeView};
        mTreeModel->setRootPath(mDB.pakFilesDir());
        mTreeModel->setReadOnly(true);
        mTreeModel->setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
        m.pakFileTreeView->setModel(mTreeModel);
        m.pakFileTreeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

        auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
        lSyncService->bind(m.fileInformation, "enabled + file");

        // Navigation.
        QObject::connect(m.pakFilesList,                      &QTreeView::activated,                this, &ViewPrivate::onActivatePakFile);
        QObject::connect(m.pakFilesList->selectionModel(),    &QItemSelectionModel::currentChanged, this, &ViewPrivate::onSelectedPakFile);
        QObject::connect(m.pakFileTreeView->selectionModel(), &QItemSelectionModel::currentChanged, this, &ViewPrivate::onSelectedFile);
        m.pakFileTreeView->installEventFilter(this);

        showPakFiles();
        updatePakFileInfo();
        updateFileInfo();
    }


//------------------------------------------------------------------------------
//                                     Slots
//------------------------------------------------------------------------------

    /*!
     */
    void View::ViewPrivate::onNewFile()
    {
        QString lNewFile = createFile();

        if (!lNewFile.isEmpty())
        {
            if (openFile(lNewFile))
            {
                // Quand on cr�e un fichier, c'est pour l'�diter.
                //��enableEdit(true);
            }
        }
    }


    /*!
     */
    void View::ViewPrivate::onOpenFile()
    {
        QString lFile = QFileDialog::getOpenFileName(
                            q,
                            qApp->translate("QPAK::View", "Chose a pak file"),
                            mDB.pakFilesDir(),
                            "Pak Files (*.pak)");

        if (!lFile.isEmpty())
        {
            if (openFile(lFile))
            {
                // A l'ouverture d'un fichier, v�rifier si le mode �dition est activ� par d�faut.
                //��enableEdit(qApp->isEditEnabledByDefault());
            }
        }
    }


    /*!
     */
    void View::ViewPrivate::onSaveFile()
    {
        saveFile();
    }


    /*!
     */
    void View::ViewPrivate::onCloseFile()
    {
        if (mFileOpened)
        {
            q->setUpdatesEnabled(false);
            //��q->closeDocuments();
            closeFile();
            mContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->perform("close file");
            q->setUpdatesEnabled(true);
        }
    }


    /*!
     */
    void View::ViewPrivate::onActivatePakFile(const QModelIndex& pIndex)
    {
        if (pIndex.isValid())
        {
            if (!mFileOpened)
            {
                auto    lFilename = mListModel->filePath(pIndex);

                if (!lFilename.isEmpty())
                {
                    openFile(lFilename);
                }
            }
        }
    }


    /*!
     */
    void View::ViewPrivate::onSelectedPakFile(const QModelIndex& pIndex)
    {
        if (pIndex.isValid())
        {
            updatePakFileInfo(mTreeModel->filePath(pIndex));
        }
    }


    /*!
     */
    void View::ViewPrivate::onSelectedFile(const QModelIndex& pIndex)
    {
        if (pIndex.isValid())
        {
            updateFileInfo(mTreeModel->filePath(pIndex));
        }
    }


//------------------------------------------------------------------------------
//                      Mise � Jour "Activante" : Pak File
//------------------------------------------------------------------------------

    /*!
     */
    void View::ViewPrivate::setPakFilename(QString pValue)
    {
        m.pakFilename->setText(pValue);
        m.pakFilename->setEnabled(!pValue.isEmpty());
    }


    /*!
     */
    void View::ViewPrivate::resetPakFilename()
    {
        setPakFilename(QString::null);
    }


    /*!
     */
    void View::ViewPrivate::setPakFileCounts(int pCurrent, int pTotal)
    {
        if ((pCurrent != 0) && (pTotal != 0))
        {
            m.fileCounts->setText(QString{"%1 / %2"}.arg(pCurrent).arg(pTotal));
            m.fileCounts->setEnabled(true);
        }
        else
        {
            m.fileCounts->setText(QString::null);
            m.fileCounts->setEnabled(false);
        }
    }


    /*!
     */
    void View::ViewPrivate::resetPakFileCounts()
    {
        setPakFileCounts(0, 0);
    }


    /*!
     */
    void View::ViewPrivate::setAllPakSize(qint64 pValue)
    {
        m.allPackSize->setValue(pValue / 1048576.0);
        m.allPackSize->setEnabled(pValue != 0LL);
    }


    /*!
     */
    void View::ViewPrivate::resetAllPakSize()
    {
        setAllPakSize(0LL);
    }


    /*!
     */
    void View::ViewPrivate::setAllDiskSize(qint64 pValue)
    {
        m.allDiskSize->setValue(pValue / 1048576.0);
        m.allDiskSize->setEnabled(pValue != 0LL);
    }


    /*!
     */
    void View::ViewPrivate::resetAllDiskSize()
    {
        setAllDiskSize(0LL);
    }


    /*!
     */
    void View::ViewPrivate::setMemory(int pValue)
    {
        m.memory->setValue(pValue / 1024.0);
        m.memory->setEnabled(pValue != 0);
    }


    /*!
     */
    void View::ViewPrivate::resetMemory()
    {
        setMemory(0);
    }


    /*!
     */
    void View::ViewPrivate::setPakDates(QDateTime pCreate, QDateTime pModify)
    {
        m.allCreation->setDateTime(pCreate);
        m.allCreation->setEnabled(pCreate != kNullDate);

        m.allModify->setDateTime(pModify);
        m.allModify->setEnabled(pModify != kNullDate);
    }


    /*!
     */
    void View::ViewPrivate::resetPakDates()
    {
        setPakDates(kNullDate, kNullDate);
    }


//------------------------------------------------------------------------------
//                       Mise � Jour "Activante" : Fichier
//------------------------------------------------------------------------------

    /*!
     */
    void View::ViewPrivate::setFilename(QString pValue)
    {
        m.filename->setText(pValue);
        m.filename->setEnabled(!pValue.isEmpty());
    }


    /*!
     */
    void View::ViewPrivate::resetFilename()
    {
        setFilename(QString::null);
    }


    /*!
     */
    void View::ViewPrivate::setFileSize(qint64 pValue)
    {
        m.packSize->setValue(pValue / 1048576.0);
        m.packSize->setEnabled(pValue != 0LL);
    }


    /*!
     */
    void View::ViewPrivate::resetFileSize()
    {
        setFileSize(0LL);
    }


    /*!
     */
    void View::ViewPrivate::setDiskSize(qint64 pValue)
    {
        m.diskSize->setValue(pValue / 1048576.0);
        m.diskSize->setEnabled(pValue != 0LL);
    }


    /*!
     */
    void View::ViewPrivate::resetDiskSize()
    {
        setDiskSize(0LL);
    }


    /*!
     */
    void View::ViewPrivate::setFileType(QString pValue)
    {
        m.fileType->setText(pValue); 
        m.fileType->setEnabled(!pValue.isEmpty());
    }


    /*!
     */
    void View::ViewPrivate::resetFileType()
    {
        setFileType(QString::null);
    }


    /*!
     */
    void View::ViewPrivate::setFileVersion(QString pValue)
    {
        m.version->setText(pValue);
        m.version->setEnabled(!pValue.isEmpty());
    }


    /*!
     */
    void View::ViewPrivate::resetFileVersion()
    {
        setFileVersion(QString::null);
    }


    /*!
     */
    void View::ViewPrivate::setFileDates(QDateTime pCreate, QDateTime pModify)
    {
        m.creation->setDateTime(pCreate);
        m.creation->setEnabled(pCreate != kNullDate);

        m.modify->setDateTime(pModify);
        m.modify->setEnabled(pModify != kNullDate);
    }


    /*!
     */
    void View::ViewPrivate::resetFileDates()
    {
        setFileDates(kNullDate, kNullDate);
    }


    /*!
     */
    void View::ViewPrivate::setFileContents(QString pText)
    {
        m.fileContents->setPlainText(pText);
        m.fileContents->setEnabled(!pText.isEmpty());
    }


    /*!
     */
    void View::ViewPrivate::resetFileContents()
    {
        setFileContents(QString::null);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool View::ViewPrivate::eventFilter(QObject* pObject, QEvent* pEvent)
    {
        if (mFileOpened)
        {
            if (pObject == m.pakFileTreeView)
            {
                if (pEvent->type() == QEvent::KeyPress)
                {
                    QKeyEvent*  lKeyEvent   = dynamic_cast<QKeyEvent*>(pEvent);
                    if (lKeyEvent->key() == Qt::Key_Backspace)
                    {
                        closeFile();
                    }
                }
            }
        }

        return false;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QString View::ViewPrivate::createFile()
    {
        return mDB.make();
    }

    /*!
     */
    bool View::ViewPrivate::openFile(QString pFilename)
    {
        closeFile();

        try
        {
            mDB.open(pFilename);

            showPakTree(pFilename.left(pFilename.lastIndexOf(".pak")));

            mContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->perform("open file");
            mFileOpened = true;
            updatePakFileInfo(pFilename);
            //d->mGameView->load(file);
        }
        catch(std::exception& pE)
        {
            qDebug() << pE.what();

            QMessageBox::critical(nullptr,
                                  //MainEditor::tr("Ouverture d'un pak file"), tr("L'ouverture du fichier %1 a �chou�").arg(file),
                                  View::tr("Pak file opening"), View::tr("Opening file %1 failed").arg(pFilename),
                                  QMessageBox::Ok, QMessageBox::NoButton);
            return false;
        }

        /*��d->mGameView->boot();
        centralWidget()->setEnabled(true);
        d->displayTools();
        d->displayEditors();
        d->mGameView->loaded();*/

        return true;
    }


    /*!
     */
    void View::ViewPrivate::saveFile()
    {
        //��
    }


    /*!
     */
    void View::ViewPrivate::closeFile()
    {
        saveFile();

        // Le signal de fermeture est envoy� un peu avant la fermeture effective, afin de pouvoir proc�der � des sauvegardes.
        mContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync")->perform("close file");
        mDB.close();

        mFileOpened = false;

        // Mise � jour de l'IHM.
        showPakFiles();

        /*centralWidget()->setEnabled(false);
        d->closePanels();

        TileMgr->save();
        ImageMgr->save();
        GeometryMgr->save();

        d->mGameView->cleanUp();*/
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void View::ViewPrivate::showPakFiles()
    {
        m.treeViews->setCurrentIndex(0);
        m.pakFilesList->setRootIndex(mListModel->setRootPath(mDB.pakFilesDir()));
        if (!m.pakFilename->text().isEmpty())
        {
            QString lPakFilename    = mDB.pakFilesDir() + "/" + m.pakFilename->text();
            QString lLastSelected   = mListModel->filePath(m.pakFilesList->selectionModel()->currentIndex());
            if (lPakFilename != lLastSelected)
            {
                // Dans la liste de pak files, res�lectionne le dernier pak file ouvert (via menu).
                QModelIndex lIndex = mListModel->index(lPakFilename);
                m.pakFilesList->selectionModel()->clearSelection();
                m.pakFilesList->selectionModel()->select(         lIndex, QItemSelectionModel::Select | QItemSelectionModel::Rows);
                m.pakFilesList->selectionModel()->setCurrentIndex(lIndex, QItemSelectionModel::Current);
            }
            else
            {
                // Juste une mise � jour des infos.
                updatePakFileInfo(lPakFilename);
            }
        }
        updateFileInfo();
    }


    /*!
     */
    void View::ViewPrivate::showPakTree(QString pDirName)
    {
        m.treeViews->setCurrentIndex(1);
        m.pakFileTreeView->reset(); // Supprime toute s�lection pr�c�dente.
        m.pakFileTreeView->setRootIndex(mTreeModel->setRootPath(pDirName));
        updateFileInfo();
    }


    /*! @param pPakFilename Nom de pak file (chemin complet).
     */
    void View::ViewPrivate::updatePakFileInfo(QString pPakFilename)
    {
        if (!pPakFilename.isEmpty())
        {
            QFileInfo   lInfo(pPakFilename);
            setPakFilename(lInfo.fileName());
            setAllPakSize(lInfo.size());
            if (mFileOpened)
            {
                // Ces donn�es ne sont accessibles qu'une fois un pak file ouvert.
                setPakFileCounts(mDB.currentFileCount(), mDB.totalFileCount());
                setAllDiskSize(mDB.totalDiskSize());
                setMemory(mDB.totalFileCount() * sizeof(PAK::File));
            }
            else
            {
                resetPakFileCounts();
                resetAllDiskSize();
                resetMemory();
            }
            setPakDates(lInfo.created(), lInfo.lastModified());
        }
        else
        {
            resetPakFilename();
            resetPakFileCounts();
            resetAllPakSize();
            resetAllDiskSize();
            resetMemory();
            resetPakDates();
        }
    }


    /*! @param pFilename Nom de fichier dans l'arborescence syst�me du pak file (chemin relatif)
     */
    void View::ViewPrivate::updateFileInfo(QString pFilename)
    {
        if (!pFilename.isEmpty())
        {
            QFileInfo   lInfo{pFilename};
            setFilename(lInfo.fileName());
            if      (lInfo.isDir())
            {
                setFileSize(0);
                setDiskSize(lInfo.size());
                setFileType(tr("Folder"));
                resetFileVersion();
                resetFileContents();
            }
            else if (lInfo.isFile())
            {
                auto    lHdl = PAK::FileHdl(mDB.packFileHdl(pFilename.right(pFilename.length() - pFilename.lastIndexOf('/') - 1)));
                if (lHdl.isValid())
                {
                    setFileSize(mDB.packFileSize(lHdl));
                }
                setDiskSize(lInfo.size());
                setFileType(mDB.packFileType(lHdl));
                setFileVersion(mDB.packFileVersion(lHdl));
                QFile   lFile(pFilename);
                if (lFile.open(QFile::ReadOnly))
                {
                    QTextStream lReader(&lFile);
                    if (lFile.size() < 2048)
                    {
                        setFileContents(lReader.readAll());
                    }
                    else
                    {
                        setFileContents(lReader.read(2048) + "\n...");
                    }
                }
            }
            setFileDates(lInfo.created(), lInfo.lastModified());
        }
        else
        {
            resetFilename();
            resetFileSize();
            resetDiskSize();
            resetFileType();
            resetFileVersion();
            resetFileDates();
            resetFileContents();
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    View::View(OSGi::Context* pContext)
        : QWidget{}
        , pthis{this, pContext}
    {
        Q_D(View);

        auto    lMenuService = pContext->services()->findByTypeAndName<CORE::MenuService>("fr.osg.ide.menu");
        lMenuService->connect("data.file.new",   CORE::MenuService::Slot{[=]() { d->onNewFile(); }});
        lMenuService->connect("data.file.open",  CORE::MenuService::Slot{[=]() { d->onOpenFile(); }});
        lMenuService->connect("data.file.save",  CORE::MenuService::Slot{[=]() { d->onSaveFile(); }});
        lMenuService->connect("data.file.close", CORE::MenuService::Slot{[=]() { d->onCloseFile(); }});
    }


    /*! Destructeur.
     */
    View::~View() { }


//------------------------------------------------------------------------------
//                                  
//------------------------------------------------------------------------------

    /*!
     */
    CORE::Translated::Updater View::buildTranslater()
    {
        Q_D(View);

        return [=]()
        {
            d->m.retranslateUi(this);
        };
    }
}
