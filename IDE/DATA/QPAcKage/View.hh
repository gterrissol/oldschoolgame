/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_VIEW_HH
#define QPAK_VIEW_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/View.hh
    @brief En-t�te de la classe QPAK::View.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

#include <OSGi/Context.hh>

#include "CORE/Private.hh"
#include "CORE/Translate.hh"

namespace QPAK
{
    /*! @brief Interface de visualisation des @b pak files.
        @version 0.5

     */
    class View : public QWidget, public CORE::Translate<View>
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                View(OSGi::Context* pContext);                  //!< Constructeur
virtual         ~View();                                        //!< Destructeur.
        //@}
    private:

virtual CORE::Translated::Updater   buildTranslater() override; //!< @copybrief CORE::Translated::buildTranslater()

        Q_CUSTOM_DECLARE_PRIVATE(View)
    };
}


#endif  // De QPAK_VIEW_HH
