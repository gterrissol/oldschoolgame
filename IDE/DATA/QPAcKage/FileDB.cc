/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "FileDB.hh"

/*! @file IDE/DATA/QPAcKage/FileDB.cc
    @brief M�thodes (non-inline) de la classe QPAK::FileDB.
    @author @ref Guillaume_Terrissol
    @date 30 Janvier 2008 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QTextStream>
#include <QXmlStreamReader>

#include "CORE/ProgressService.hh"
#include "PAcKage/File.hh"
#include "PAcKage/LoaderSaver.hh"
#include "PAcKage/PakFileMgr.hh"
#include "PROGram/GameSteps.hh"
#include "STL/Vector.hh"

#include "BlockMgr.hh"
#include "Builder.hh"
#include "ErrMsg.hh"
#include "Extension.hh"
#include "Fat.hh"
#include "FileService.hh"
#include "ImportExport.hh"
#include "ImportExportService.hh"

/*
#   include "GEOmetry/All.hh"
#   include "CoLoR/All.hh"
namespace
{
    U16 lLastVx = U16(18);
    U16 lLastTr = U16(22);

    const float kVertices[] =
    {
        -3.0F, -6.0F, 0.0F,
        -3.0F, -6.0F, 3.0F,
         0.0F, -6.0F, 3.0F,
         3.0F, -6.0F, 3.0F,
         3.0F, -6.0F, 0.0F,
         1.0F, -6.0F, 0.0F,
         1.0F, -6.0F, 2.0F,
        -1.0F, -6.0F, 2.0F,
        -1.0F, -6.0F, 0.0F,

        -3.0F,  6.0F, 0.0F,
        -3.0F,  6.0F, 3.0F,
         0.0F,  6.0F, 3.0F,
         3.0F,  6.0F, 3.0F,
         3.0F,  6.0F, 0.0F,
         1.0F,  6.0F, 0.0F,
         1.0F,  6.0F, 2.0F,
        -1.0F,  6.0F, 2.0F,
        -1.0F,  6.0F, 0.0F
    };


    unsigned short kTriangles[] =
    {
         7,  0,  8,
         7,  1,  0,
         7,  2,  1,
         7,  6,  2,
         2,  6,  3,
         3,  6,  4,
         4,  5,  6,

        15, 13, 14,
        15, 12, 13,
        15, 11, 12,
        16, 11, 15,
        16, 10, 11,
        16,  9, 10,
        16, 17,  9,

        10,  9,  1,
         1,  9,  0,

         3,  4, 12,
        12,  4, 13,

        10,  1, 11,
        11,  1,  2,
        11,  2, 12,
        12,  2,  3
    };


    class MyGeo : public GEO::Geometry
    {
    public:

        MyGeo()
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));

            const U16   kVxCount    = U16(lLastVx + 0);
            const U16   kTrCount    = U16(lLastTr + 0);
            setVertexCount(kVxCount);
            setTriangleCount(kTrCount);

            const CLR::RGB kGrey(F32(0.6F), F32(0.6F), F32(0.6F));

            for(U16 lVx = k0UW; lVx < kVxCount; ++lVx)
            {
                F32 lVertex[]   = { F32(kVertices[lVx * 3 + 0]), F32(kVertices[lVx * 3 + 1]), F32(kVertices[lVx * 3 + 2]) };
                vertices3().push_back(GEO::Vertex3(lVertex));

                MATH::V3   lNormal = MATH::kNullV3;
                {
                    for(U16 lT = k0UW; lT < kTrCount; ++lT)
                    {
                        for(U16 lV = k0UW; lV < 3; ++lV)
                        {
                            if (kTriangles[lT * 3 + lV] == lVx)  // Le vertex appartient � ce triangle.
                            {
                                int         lI0 = U16(kTriangles[lT * 3 + 0]);
                                MATH::V3   v0  = MATH::V3(F32(kVertices[lI0 * 3 + 2]),
                                                            F32(kVertices[lI0 * 3 + 0]),
                                                            F32(kVertices[lI0 * 3 + 1]));
                                int         lI1 = U16(kTriangles[lT * 3 + 1]);
                                MATH::V3   v1  = MATH::V3(F32(kVertices[lI1 * 3 + 2]),
                                                            F32(kVertices[lI1 * 3 + 0]),
                                                            F32(kVertices[lI1 * 3 + 1]));
                                int         lI2 = U16(kTriangles[lT * 3 + 2]);
                                MATH::V3   v2  = MATH::V3(F32(kVertices[lI2 * 3 + 2]),
                                                            F32(kVertices[lI2 * 3 + 0]),
                                                            F32(kVertices[lI2 * 3 + 1]));

                                MATH::V3    l01(v1 - v0);
                                MATH::V3    l02(v2 - v0);
                                l01.normalize();
                                l02.normalize();
                                MATH::V3    lCross = l01 ^ l02;
                                lCross.normalize();
                                lNormal += lCross;

                                break;
                            }
                        }
                    }
                    lNormal.normalize();
                }
                normals().push_back(GEO::Normal(&lNormal[0]));

                colors3().push_back(kGrey);
            }

            TriangleListPtr    lTrList = std::make_shared<TriangleListPtr::element_type>(triangleCount());
            for(U16 lTr = k0UW; lTr < kTrCount; ++lTr)
            {
                lTrList->push(GEO::Triangle(U16(kTriangles[lTr * 3 + 0]), U16(kTriangles[lTr * 3 + 1]), U16(kTriangles[lTr * 3 + 2])));
            }
            cloneTriangleList(ConstTriangleListPtr(lTrList));
        }

virtual ~MyGeo() { }

virtual void    store(PAK::Saver& pSaver) const
{
    GEO::Geometry::store(pSaver);
}
    };
}
*/
namespace QPAK
{
//------------------------------------------------------------------------------
//                                  Constantes
//------------------------------------------------------------------------------

    namespace
    {
        const QString   kSchema        = QStringLiteral(
                                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                            "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\n"
                                            "           targetNamespace=\"osg\"\n"
                                            "           xmlns=\"osg\"\n"
                                            "           elementFormDefault=\"qualified\"\n"
                                            ">\n"
                                            "  <!-- Attributes definitions -->\n"
                                            "  <xs:simpleType name=\"nameAttr\">\n"
                                            "    <xs:restriction base=\"xs:string\">\n"
                                            "      <xs:pattern value=\"(?){20}|([a-zA-Z0-9_])+.([a-zA-Z0-9_]){3}\"/>\n"
                                            "      <xs:whiteSpace value=\"preserve\"/>\n"
                                            "      <xs:length value=\"20\"/>\n"
                                            "    </xs:restriction>\n"
                                            "  </xs:simpleType>\n"
                                            "\n"
                                            "  <!-- Complex elements definitions -->\n"
                                            "  <xs:complexType name=\"fileType\">\n"
                                            "    <xs:attribute name=\"name\" type=\"nameAttr\" use=\"required\"/>\n"
                                            "    <xs:attribute name=\"imported\" type=\"xs:dateTime\" use=\"required\"/>\n"
                                            "    <xs:attribute name=\"exported\" type=\"xs:dateTime\" use=\"required\"/>\n"
                                            "  </xs:complexType>\n"
                                            "\n"
                                            "  <xs:complexType name=\"files\">\n"
                                            "    <xs:sequence>\n"
                                            "      <xs:element name=\"file\" type=\"fileType\" minOccurs=\"1\" maxOccurs=\"unbounded\"/>\n"
                                            "    </xs:sequence>\n"
                                            "    <xs:attribute name=\"count\" type=\"xs:integer\" use=\"required\"/>\n"
                                            "  </xs:complexType>\n"
                                            "\n"
                                            "</xs:schema>\n");

        //! @name Chemins et noms de fichiers
        //@{
        const QString   kPakFilesPath  = "/pak";
        const QString   kFileDBName    = "FileDB.xml";
        //@}
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPAK::FileDB.
        @version 0.5
     */
    class FileDB::Private
    {
        Q_CUSTOM_DECLARE_PUBLIC(FileDB)
    public:
        //! @name Constructeur & destructeur
        //@{
                        Private(FileDB* pThat, OSGi::Context* pContext);                //!< Constructeur.
                        ~Private();                                                     //!< Destructeur.
        //@}
        QString         create(const Builder& pBuilder);                                //!< Cr�ation d'un nouveau pak file.
        QString         import(const Builder& pBuilder);                                //!< Import d'une arborescence XML.
        void            buildTree();                                                    //!< 
        void            buildFat(int pFileCount);                                       //!< 
        void            loadFat();                                                      //!< 
        void            closeFat();                                                     //!< 
        void            updateTree();                                                   //!< 
        PAK::FileHdl    create(QString pFileName);                                      //!< 
        PAK::FileHdl    import(QString pSysFile);                                       //!< 
        void            exportFile(PAK::FileHdl pHdl, const U8* pData, U32 pCount);     //!< 


        /*! @brief "Gestionnaire" de fichier de <b>pak</b> file
            @version 0.5
         */
        class FileManager : public PAK::PakFileMgr::Manager
        {
        public:
            //! @name Constructeur & destructeur
            //@{
                    FileManager(Private* pOwner);                                       //!< Constructeur.
    virtual         ~FileManager();                                                     //!< Destructeur.
            //@}
            using PAK::PakFileMgr::Manager::fileSize;

        private:
            //! @name Impl�mentation
            //@{
    virtual void    manage(PAK::FileHdl pHdl, const U8* pData, U32 pCount);             //!< 
    virtual void    setFileData(const Vector<PAK::File>& pFiles);                       //!< 
            //@}
            //! @name Attributs
            //@{
            Private*                    mDB;                                            //!< "Parent".
            std::unique_ptr<BlockMgr>   mBlockMgr;                                      //!< Gestionnaire de blocs.
            //@}
        };

        typedef std::weak_ptr<FileManager>      FileManagerPtr;                         //!< 

        FileManagerPtr              mManager;                                           //!< 
        std::unique_ptr<Fat>        mFat;                                               //!< 
        ExtensionMgr                mExtensionMgr;                                      //!< 
        ImportExportService*        mIEService;                                         //!< 
        QString                     mRootPath;                                          //!< 
        QString                     mFileName;                                          //!< 
        OSGi::Context*              mContext;                                           //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    FileDB::Private::Private(FileDB* pThat, OSGi::Context* pContext)
        : q{pThat}
        , mManager()
        , mFat{}
        , mExtensionMgr{}
        , mIEService{}
        , mRootPath{""}
        , mFileName{""}
        , mContext{pContext}
    {
        if (auto lFileService = pContext->services()->findByTypeAndName<FileService>("fr.osg.ide.pak.file"))
        {
            lFileService->bind(pThat);
        }
        // A ce moment, les noms de fichier d'extension doivent �tre connus.
        mIEService = pContext->services()->findByTypeAndName<ImportExportService>("fr.osg.ide.pak.ie");
        mIEService->bind(pThat);
        mExtensionMgr.loadDefinitions(mIEService->extensionFiles());
    }


    /*!
     */
    FileDB::Private::~Private()
    {
        mIEService->bind(nullptr);
        if (auto lFileService = mContext->services()->findByTypeAndName<FileService>("fr.osg.ide.pak.file"))
        {
            lFileService->bind(nullptr);
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Cr�e un nouveau <b>pak</b> file.
     */
    QString FileDB::Private::create(const Builder& pBuilder)
    {
        mFileName = pBuilder.pakFileName();
        QFileInfo   lInfoPak(mFileName);
        mRootPath = lInfoPak.absolutePath() + "/" + lInfoPak.baseName();

        auto    lManager = std::make_shared<FileManager>(this);
        PAK::PakFileMgr::get()->setManager(lManager);
        mManager = lManager;

        PAK::PakFileMgr::get()->createFile(qPrintable(mFileName), I32(pBuilder.fatSize()));
        buildTree();
        buildFat(pBuilder.fatSize());

        // Le fichier MBR est cr�� par le pak file mgr (pour g�rer correctement le block mgr).
        // Mais ses donn�es doivent �tre cr��es ici.
        PAK::FileHdl    lMBR = create(qPrintable(mExtensionMgr.packFile(QString{mFileName}.replace(PAK::kPakFileExtension, ".mbr"))));
        ASSERT(lMBR == PAK::eRoot, kInvalidMBR)

        // Fichiers �diteur.
        create("objects.mdl");

        return mFileName;
    }


    /*! Importe les donn�es d'une arborescence XML pour cr�er un nouveau <b>pak</b> file.
     */
    QString FileDB::Private::import(const Builder& pBuilder)
    {
        mFileName = pBuilder.pakFileName();
        QFileInfo   lInfoPak(mFileName);
        mRootPath = lInfoPak.absolutePath() + "/" + lInfoPak.baseName();

        QString     lDBFileName     = pBuilder.dataBase();
        QFileInfo   lInfoDB(lDBFileName);
        QString     lDataBasePath   = lInfoDB.absolutePath();

        try
        {
            I32 lFileCount = k0L;

            for(int lPass = 1; lPass <= 2; ++lPass)
            {
                QFile               lFileDB(lDBFileName);
                lFileDB.open(QFile::ReadOnly | QFile::Text);
                QXmlStreamReader    lReader(&lFileDB);

                for(int lHdl = 0; !lReader.atEnd(); lReader.readNext())
                {
                    if      (lReader.isStartElement())
                    {
                        if      (lReader.name() == "files")
                        {
                            if (lPass == 1)
                            {
                                QStringRef  lCount  = lReader.attributes().value("count");
                                if (lCount.length() == 0)
                                {
                                    LAUNCH_EXCEPTION(kXMLError_NoFileCount)
                                }
                                else
                                {
                                    lFileCount = I32(lCount.toString().toInt());
                                    PAK::PakFileMgr::get()->setManager(std::make_shared<Private::FileManager>(&(*this)));
                                    PAK::PakFileMgr::get()->createFile(qPrintable(mFileName), lFileCount);
                                    buildTree();
                                    buildFat(lFileCount);
                                }
                            }
                        }
                        else if (lReader.name() == "file")
                        {
                            QString lFileName   = lReader.attributes().value("name").toString().simplified();
                            QString lExtension  = mExtensionMgr.extension(lFileName);

                            if      (lFileName.contains('?'))
                            {
                                // Message d'erreur ?
                            }
                            else if (mIEService->manages(lExtension))
                            {
                                QString lSysFile    = mExtensionMgr.diskFile(lFileName);
                                QString lOriginFile = lDataBasePath + lSysFile;
                                lSysFile = mRootPath + lSysFile;

                                if (lPass == 1)
                                {
                                    create(lFileName);
                                }
                                else if (lPass == 2)
                                {
                                    PAK::FileHdl   lFileHdl(static_cast<PAK::EHdls>(mFat->fileHdl(lFileName)));
                                    PAK::Saver     lSaver(lFileHdl);
                                    QFile           lFile(lOriginFile);
                                    if (!lFile.open(QIODevice::ReadOnly) || !mIEService->get(lExtension)->imports(&lFile, lSaver))
                                    {
                                        LAUNCH_EXCEPTION(kImportFileFailed)
                                    }
                                }
                            }
                            else
                            {
                                qDebug() << QString("Ne peut g�rer le fichier %1").arg(lFileName);
                                LAUNCH_EXCEPTION("Fichier ing�rable");
                            }

                            ++lHdl;
                        }
                    }
                    else if (lReader.isEndElement())
                    {
                        if (lReader.name() == "files")
                        {
                            if (lHdl != lFileCount)
                            {
                                LAUNCH_EXCEPTION(kXMLError_InvalidEnd)
                            }
                        }
                    }
                }
            }
        }
        catch(ERR::Exception& pE)
        {
            // Tout effacer !
            qDebug() << pE.what();

            //QDir    lIncompleteDir(mRootPath); ...

            return "";
        }
        catch(...)
        {
            // Tout effacer !
            qDebug() << "Exception inconnue captur�e";
            return "";
        }

        return mFileName;
    }


    /*!
     */
    void FileDB::Private::buildTree()
    {
        // Construit l'arborescence li�e au pak file.
        QStringList lFolders = mExtensionMgr.tree();
        QDir        lRoot(mRootPath);

        for(int lF = 0; lF < lFolders.count(); ++lF)
        {
            lRoot.mkpath(mRootPath + lFolders[lF]);
        }

        // Ajoute le sch�ma de FileDB.xml dans la racine.
        QFile       lFileDBxsd(mRootPath + "/FileDB.xsd");

        if (lFileDBxsd.open(QFile::WriteOnly | QFile::Text))
        {
            QTextStream lStream(&lFileDBxsd);
            lStream << kSchema;
        }
    }


    /*!
     */
    void FileDB::Private::buildFat(int pFileCount)
    {
        mFat.reset(new Fat{mRootPath + "/" + kFileDBName, pFileCount});
    }


    /*!
     */
    void FileDB::Private::loadFat()
    {
        mFat.reset(new Fat{mRootPath + "/" + kFileDBName});
    }


    /*!
     */
    void FileDB::Private::closeFat()
    {
        mFat.reset();
    }

    /*!
     */
    void FileDB::Private::updateTree()
    {
        auto    lProgress   = mContext->services()->findByTypeAndName<CORE::ProgressService>("fr.osg.ide.progress")->create(0, mFat->totalFileCount());
        lProgress->advance(0);

        for(int lHdl = 0; lHdl < mFat->totalFileCount(); ++lHdl)
        {
            QString     lFileName       = mFat->fileName(lHdl);
            QString     lSysFile        = mExtensionMgr.diskFile(lFileName);
            QFileInfo   lInfo(mRootPath + "/" + lSysFile);
            QDateTime   lLastModified   =  lInfo.lastModified().addMSecs(-lInfo.lastModified().time().msec());  // Dans la FAT, les ms ne sont pas conserv�es. Elle sont donc supprim�es aussi pour la comparaison.
            if (mFat->needToImport(lHdl, lLastModified))
            {
                qDebug() << QString("Updating %1 (%2) from external XML data").arg(lSysFile).arg(lFileName);
                QString     lExtension  = mExtensionMgr.extension(lFileName);
                if (mIEService->manages(lExtension))
                {
                    PAK::FileHdl   lFileHdl = PAK::FileHdl(U16(lHdl));
                    PAK::Saver     lSaver(lFileHdl);
                    QFile           lFile(mRootPath + "/" + lSysFile);
                    if (lFile.open(QIODevice::ReadOnly) &&
                        mIEService->get(lExtension)->imports(&lFile, lSaver))
                    {
                        mFat->updateImportTime(lHdl, lLastModified);
                    }
                }
            }

            lProgress->advance(lHdl);
        }
    }


    /*!
     */
    PAK::FileHdl FileDB::Private::create(QString pFileName)
    {
        QString lExtension  = mExtensionMgr.extension(pFileName);

        if (mExtensionMgr.exists(lExtension))
        {
            // Il faut bien v�rifier que le fichier n'existe pas d�j�...
            if (mFat->fileHdl(pFileName) == -1)
            {
                auto    lIEService  = mContext->services()->findByTypeAndName<ImportExportService>("fr.osg.ide.pak.ie");
                auto    lDefaulter  = lIEService->get(lExtension);
                auto    lNew        = PAK::FileHdl{U16(mFat->createFile(pFileName))};
                QFile   lFile{mRootPath + "/" + mExtensionMgr.diskFile(pFileName)};
                lDefaulter->make(&lFile, lNew);

                return lNew;
            }
        }

        return PAK::eBadHdl;
    }


    /*!
     */
    PAK::FileHdl FileDB::Private::import(QString pSysFile)
    {
        QFileInfo   lFileInfo(pSysFile);
        QString     lNewFile    = mExtensionMgr.packFile(pSysFile);
        if (!lNewFile.isEmpty())
        {
            QString         lExtension  = ExtensionMgr::extension(lNewFile);
            PAK::FileHdl   lFileHdl    = create(lNewFile);
            if (!lFileHdl.isValid())
            {
                LAUNCH_EXCEPTION(kImportFileFailed)
            }
            PAK::Saver     lSaver(lFileHdl);
            QFile           lFile(pSysFile);
            if (!lFile.open(QIODevice::ReadOnly) || !mIEService->get(lExtension)->imports(&lFile, lSaver))
            {
                LAUNCH_EXCEPTION(kImportFileFailed)
            }

            return lFileHdl;
        }

        return PAK::eBadHdl;
    }


    /*!
     */
    void FileDB::Private::exportFile(PAK::FileHdl pHdl, const U8* pData, U32 pCount)
    {
        //
        QString lFileName   = mFat->fileName(pHdl.value());
        QString lSysFile    = mExtensionMgr.diskFile(lFileName);
        QString lExtension  = mExtensionMgr.extension(lFileName);

        QFile   lFile(mRootPath + "/" + lSysFile);
        if (lFile.open(QIODevice::WriteOnly))
        {
            if (mIEService->manages(lExtension))
            {
                PAK::Loader    lLoader(pData, pCount, pHdl);

                if (mIEService->get(lExtension)->exports(&lFile, lLoader))
                {
                    lFile.close();
                    QFileInfo   lInfo(mRootPath + "/" + lSysFile);
                    mFat->updateExportTime(pHdl.value(), lInfo.lastModified());
                }
                else
                {
                    LAUNCH_EXCEPTION(kCantSaveFile)
                }
            }
        }
        else
        {
            LAUNCH_EXCEPTION(kCantSaveFile)
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    FileDB::Private::FileManager::FileManager(Private* pOwner)
        : mDB(pOwner)
        , mBlockMgr()
    {
        ASSERT(mDB != nullptr, "")
    }


    /*! Destructeur.
     */
    FileDB::Private::FileManager::~FileManager() { }


    /*!
     */
    void FileDB::Private::FileManager::manage(PAK::FileHdl pHdl, const U8* pData, U32 pCount)
    {
        ASSERT_EX(mBlockMgr != nullptr, kUninitializedBlockMgr, return;)

        auto    lBlockHdl   = blockHdl(pHdl);
        U32     lFileSize   = fileSize(pHdl);
        U32     lNewSize    = pCount;
        bool    lNewBlock   = true;

        // D�termination de l'emplacement des donn�es
        if      (lFileSize == 0)
        {
            // Aucune donn�e n'est �crite, il faut r�server de la place dans le fichier.
            lBlockHdl = mBlockMgr->acquireBlock(lNewSize);
        }
        else if (lFileSize != lNewSize)
        {
            // La taille du fichier a chang�, il faut redimensionner le bloc de donn�es dans le pak file.
            lBlockHdl = mBlockMgr->resizeBlock(lBlockHdl, lFileSize, lNewSize);
        }
        else
        {
            lNewBlock = false;
        }
        if (lNewBlock)
        {
            defineFile(pHdl, PAK::File(lNewSize, lBlockHdl));
        }
        // Exporte les donn�es.
        mDB->exportFile(pHdl, pData, pCount);
    }


    /*!
     */
    void FileDB::Private::FileManager::setFileData(const Vector<PAK::File>& pFiles)
    {
        mBlockMgr.reset(new BlockMgr{dataStart(), pFiles});
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    FileDB::FileDB(OSGi::Context* pContext)
        : pthis{this, pContext}
    { }


    /*! Destructeur.
     */
    FileDB::~FileDB() { }


//------------------------------------------------------------------------------
//                                  Op�rations
//------------------------------------------------------------------------------

    /*!
     */
    QString FileDB::make()
    {
        Builder    lDialog(pakFilesDir());

        if (lDialog.exec() == QDialog::Accepted)
        {
            if (!lDialog.dataBase().isEmpty())
            {
                return pthis->import(lDialog);
            }
            else
            {
                return pthis->create(lDialog);
            }
        }
        else
        {
            return QString::null;
        }
    }


    /*! @param pFileName Nom du pak file � ouvrir
     */
    void FileDB::open(QString pFileName)
    {
        pthis->mFileName = pFileName;
        pthis->mRootPath = ExtensionMgr::radical(pthis->mFileName);

        auto    lManager = std::make_shared<Private::FileManager>(&*pthis);
        PAK::PakFileMgr::get()->setManager(lManager);
        pthis->mManager = lManager;

        PAK::PakFileMgr::get()->editFile(qPrintable(pthis->mFileName));
        pthis->loadFat();
        pthis->updateTree();
/*
MyGeo  lMyGeo;
PAK::FileHdl   lTileHdl = create("house.geo");
PAK::Saver lSaver(lTileHdl);
lMyGeo.store(lSaver);
*/
    }


    /*!
     */
    void FileDB::close()
    {
        pthis->closeFat();
        PAK::PakFileMgr::get()->closeFile();
    }


//------------------------------------------------------------------------------
//                            Cr�ation / Suppression
//------------------------------------------------------------------------------

    /*!
     */
    PAK::FileHdl FileDB::create(QString pFileName)
    {
        return pthis->create(pFileName);
    }


    /*!
     */
    PAK::FileHdl FileDB::import(QString pSysFile)
    {
        try
        {
            return pthis->import(pSysFile);
        }
        catch(ERR::Exception& pE)
        {
            return PAK::eBadHdl;
        }
    }


    /*!
     */
    void FileDB::erase(QString pFileName)
    {
        pthis->mFat->eraseFile(pFileName);
    }


    /*!
     */
    void FileDB::erase(PAK::FileHdl pHdl)
    {
        pthis->mFat->eraseFile(pHdl.value());
    }


//------------------------------------------------------------------------------
//                            Informations G�n�rales
//------------------------------------------------------------------------------

    /*!
     */
    OSGi::Context* FileDB::context() const
    {
        return pthis->mContext;
    }


    /*!
     */
    QString FileDB::pakFilesDir() const
    {
        return QApplication::applicationDirPath() + kPakFilesPath;
    }


    /*!
     */
    QString FileDB::openedPakFileDir() const
    {
        return pthis->mRootPath;
    }


    /*!
     */
    U32 FileDB::totalFileCount() const
    {
        return U32(pthis->mFat->totalFileCount());
    }


    /*!
     */
    U32 FileDB::currentFileCount() const
    {
        return U32(pthis->mFat->currentFileCount());
    }


//------------------------------------------------------------------------------
//                     Information d'un Fichier du Pak File
//------------------------------------------------------------------------------

    /*!
     */
    PAK::FileHdl FileDB::packFileHdl(QString pFileName) const
    {
        return PAK::FileHdl(static_cast<PAK::EHdls>(pthis->mFat->fileHdl(pFileName)));
    }


    /*!
     */
    QString FileDB::packFileName(PAK::FileHdl pHdl) const
    {
        return pthis->mFat->fileName(pHdl.value());
    }


    /*!
     */
    U32 FileDB::packFileSize(PAK::FileHdl pHdl) const
    {
        return pthis->mManager.lock()->fileSize(pHdl);
    }


    /*!
     */
    QString FileDB::packFileType(PAK::FileHdl pHdl) const
    {
        return pthis->mExtensionMgr.comment(pthis->mFat->fileName(pHdl.value()));
    }


    /*!
     */
    QString FileDB::packFileVersion(PAK::FileHdl pHdl) const
    {
        auto    lExtension = pthis->mExtensionMgr.extension(pthis->mFat->fileName(pHdl.value()));
        if (pthis->mIEService->manages(lExtension))
        {
            return QString::number(pthis->mIEService->get(lExtension)->version());
        }
        else
        {
            return QString::null;
        }
    }


//------------------------------------------------------------------------------
//                        Taille d'un fichier sur disque
//------------------------------------------------------------------------------

    /*!
     */
    void FileDB::diskFileSize(PAK::FileHdl pHdl, I64 pSize)
    {
        pthis->mFat->updateDiskFileSize(pHdl.value(), pSize);
    }


    /*!
     */
    I64 FileDB::diskFileSize(PAK::FileHdl pHdl) const
    {
        return I64{pthis->mFat->diskFileSize(pHdl.value())};
    }


    /*!
     */
    I64 FileDB::totalDiskSize() const
    {
        return I64{pthis->mFat->totalDiskSize()};
    }
}
