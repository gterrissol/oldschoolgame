/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPAK_FILEERVICE_HH
#define QPAK_FILEERVICE_HH

#include "QPAcKage.hh"

/*! @file IDE/DATA/QPAcKage/FileService.hh
    @brief En-t�te de la classe QPAK::FileService.
    @author @ref Guillaume_Terrissol
    @date 22 Ao�t 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QString>

#include <OSGi/Context.hh>
#include <OSGi/Service.hh>

#include "PAcKage/Handle.hh"

namespace QPAK
{
    /*! @brief Service : gestion de base des fichiers.
        @version 0.3

        Ce service est enregistr� sous le nom @b fr.osg.ide.pak.file .
     */
    class FileService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr           = std::shared_ptr<FileService>;                     //!< Pointeur sur service.
        using ContextPtr    = OSGi::Context*;                                   //!< Pointeur sur contexte.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        FileService();                                          //!< Constructeur par d�faut.
virtual                 ~FileService();                                         //!< Destructeur.
        //@}
        //! @name Gestion des fichiers
        //@{
        PAK::FileHdl    create(QString pFilename);                              //!< Cr�ation d'un fichier.
        PAK::FileHdl    import(QString pSysFile);                               //!< Import d'un fichier.
        void            erase(QString pFilename);                               //!< Suppression d'un fichier.
        void            erase(PAK::FileHdl pHdl);                               //!< Suppression d'un fichier.
        PAK::FileHdl    packFileHdl(QString pFileName) const;                   //!< Handle.
        QString         packFileName(PAK::FileHdl pHdl) const;                  //!< Nom.
        QString         root() const;                                           //!< R�pertoire des donn�es du @b pak file ouvert.
        //@}
        void            bind(FileDB* pDB);                                      //!< Liaison avec les fichiers de @b pak file.
        //! @name Impl�mentation
        //@{
    protected:

virtual bool            isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string     typeName() const override;                              //!< Nom du type (r�el).
        //@}
        FileDB* mDB;                                                            //!< Syst�me de fichiers (@b pak file).
    };
}

#endif  // De QPAK_IMPORTEXPORTSERVICE_HH
