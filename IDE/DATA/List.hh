/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_LIST_HH
#define DATA_LIST_HH

#include "DATA.hh"

/*! @file IDE/DATA/List.hh
    @brief En-t�te de la classe DATA::List.
    @author @ref Guillaume_Terrissol
    @date 10 Novembre 2013 - 11 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QListView>

namespace DATA
{
//------------------------------------------------------------------------------
//                          Widget Liste de Cat�gories
//------------------------------------------------------------------------------

    /*! @brief List de cat�gories pour un type de donn�es.
        @version 0.5

        @sa Base
     */
    class List : public QListView
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                List();                                         //!< Constructeur.
virtual         ~List();                                        //!< Destructeur.
        //@}
virtual void    setModel(QAbstractItemModel* pModel) override;  //!< D�finition du mod�l�.

    public slots:
        //! @name Gestion des cat�gories
        //@{
        void    insertCategory();                               //!< Insertion.
        void    deleteCategory();                               //!< Suppression.
        void    renameCategory();                               //!< Renommage.
        //@}
    signals:
        //! @name Signaux
        //@{
        void    selectionChange(bool pSelected);                //!< Changement de s�lection.
        void    selected(CategoryModel* pNewSelection);         //!< S�lection d'une cat�gorie.
        void    contentChanged();                               //!< Changement de contenu.
        //@}
    protected:
        // @name Autres gestions
        //@{
virtual void    keyPressEvent(QKeyEvent* pEvent) override;      //!< ... des �v�nements clavier.

    private slots:

        void    handleSelectionChange();                        //!< ... d'un changement de s�lection.
        void    callContextMenu(const QPoint& pPos);            //!< ... du menu contextuel
        //@}
    private:

        ListModel*  mModel;                                     //!< Mod�le.
    };
}

#endif  // DATA_LIST_HH
