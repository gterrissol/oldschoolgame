/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Base.hh"

/*! @file IDE/DATA/Base.cc
    @brief M�thodes (non-inline) de la classe DATA::Base.
    @author @ref Guillaume_Terrissol
    @date 20 Ao�t 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <stdexcept>

#include <QDebug>
#include <QFileSystemModel>

#include "CORE/Display.hh"

#include "Base.ui.hh"
#include "Browser.ui.hh"
#include "List.hh"
#include "ListModel.hh"
#include "Service.hh"
#include "Viewer.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Browser de fichiers � importer dans la BD.
        @version 0.5

        @note En partie inspir� du tutorial http://www.bogotobogo.com/Qt/Qt5_QTreeView_QFileSystemModel_ModelView_MVC.php
     */
    class Browser : public QWidget
    {
    public:
                Browser(QWidget* pParent = nullptr);
virtual         ~Browser();

        void    setNameFilters(const QStringList& pFilters);
        void    setPreviewer(Service::Widget pWidget);


    private:

        void    onDirSelected(const QModelIndex& pIndex);
        void    onFilesSelected();

        Ui::Browser         m;
        QFileSystemModel*   mDirModel;
        QFileSystemModel*   mFileModel;
        CORE::Display*      mPreviewer;
    };


    /*!
     */
    Browser::Browser(QWidget* pParent)
        : QWidget{pParent}
        , m{}
        , mDirModel{}
        , mFileModel{}
        , mPreviewer{}
    {
        m.setupUi(this);

        mDirModel = new QFileSystemModel{this};
        mDirModel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);
        mDirModel->setRootPath(QStringLiteral(""));
        m.dirs->setModel(mDirModel);
        m.dirs->setRootIndex(mDirModel->index(QDir::currentPath()));
        m.dirs->setSelectionMode(QAbstractItemView::SingleSelection);
        for(auto lCol = mDirModel->columnCount() - 1; 0 < lCol; --lCol) m.dirs->hideColumn(lCol);
        connect(m.dirs, &QTreeView::activated, this, &Browser::onDirSelected);
        connect(m.dirs, &QTreeView::clicked,   this, &Browser::onDirSelected);
        QObject::connect(m.dirs->selectionModel(), &QItemSelectionModel::currentChanged, this, &Browser::onDirSelected);

        mFileModel = new QFileSystemModel{this};
        mFileModel->setFilter(QDir::NoDotAndDotDot | QDir::Files);
        mFileModel->setRootPath(QStringLiteral(""));
        mFileModel->setNameFilterDisables(false);
        m.files->setModel(mFileModel);
        auto    lFileSelectionModel = new QItemSelectionModel(mFileModel);
        m.files->setSelectionModel(lFileSelectionModel);
        m.files->setSelectionMode(QAbstractItemView::ExtendedSelection);
        connect(lFileSelectionModel, &QItemSelectionModel::selectionChanged, this, &Browser::onFilesSelected);
        onDirSelected(mDirModel->index(QDir::currentPath()));

        auto    lLayout = new QGridLayout{m.previewFrame};
        lLayout->setContentsMargins(0, 0, 0, 0);

        m.files->setDragDropMode(QAbstractItemView::DragOnly);
    }


    /*!
     */
    Browser::~Browser() { }


    /*!
     */
    void Browser::setNameFilters(const QStringList& pFilters)
    {
        mFileModel->setNameFilters(pFilters);
    }


    /*!
     */
    void Browser::setPreviewer(Service::Widget pWidget)
    {
        delete mPreviewer;

        if (pWidget)
        {
            if ((mPreviewer = dynamic_cast<CORE::Display*>(pWidget.get())))
            {
                pWidget.release();
                mPreviewer->setParent(m.previewFrame);
                m.previewFrame->layout()->addWidget(mPreviewer);
                m.previewFrame->show();
                return;
            }
        }

        m.previewFrame->hide();
    }


    /*!
     */
    void Browser::onDirSelected(const QModelIndex& pIndex)
    {
        m.files->setRootIndex(mFileModel->setRootPath(mDirModel->fileInfo(pIndex).absoluteFilePath()));
        mFileModel->setNameFilters(mFileModel->nameFilters());  // Cette ligne semble suffisant epour corriger un petit bug Qt.
    }


    /*!
     */
    void Browser::onFilesSelected()
    {
        if (mPreviewer)
        {
            auto    lIndexes = m.files->selectionModel()->selectedIndexes();

            if (lIndexes.count() == 1)
            {
                mPreviewer->load(mFileModel->filePath(lIndexes.first()));
            }
            else
            {
                mPreviewer->clear();
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::Base.
        @version 0.2
     */
    class Base::BasePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Base)
    public:
        BasePrivate(Base* pThat, OSGi::Context* pContext, QString pTypeName);

        void            insertIntoFrame(Service::Widget pWidget, QFrame* pFrame);
 static DATA::Service*  service(QString pTypeName, OSGi::Context* pContext);

        DATA::Service*  mService;
        Ui::Base        m;
        List*           mList;
        Viewer*         mView;
        Browser*        mBrowser;
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Base::BasePrivate::BasePrivate(Base* pThat, OSGi::Context* pContext, QString pTypeName)
        : q{pThat}
        , mService{service(pTypeName, pContext)}
        , m{}
        , mList{}
        , mView{}
        , mBrowser{}
    {
        q->from(q);
        m.setupUi(q);

        auto    lList = mService->widget("list");
        if ((mList = dynamic_cast<List*>(lList.get())))
        {
            insertIntoFrame(std::move(lList), m.listFrame);
            if (auto lListModel = dynamic_cast<ListModel*>(mList->model()))
            {
                lListModel->setFileExtensions(mService->info("filter").split(";"));

                auto    lView = mService->widget("viewer");
                if ((mView = dynamic_cast<Viewer*>(lView.get())))
                {
                    insertIntoFrame(std::move(lView), m.viewFrame);
                    mView->setListModel(lListModel);

                    auto    lBrowser  = std::make_unique<Browser>();
                    if ((mBrowser = lBrowser.get()))
                    {
                        insertIntoFrame(std::move(lBrowser), m.browserFrame);
                        mBrowser->setNameFilters(mService->info("filter").split(";"));
                        mBrowser->setPreviewer(mService->widget("preview"));

                        q->setWindowTitle(mService->info("dataName"));

                        q->connect(mList,            SIGNAL(selectionChange(bool)),    m.removeCategory, SLOT(setEnabled(bool)));
                        q->connect(mList,            SIGNAL(selected(CategoryModel*)), mView,            SLOT(onCategorySelection(CategoryModel*)));
                        q->connect(m.addCategory,    SIGNAL(clicked()),                mList,            SLOT(insertCategory()));
                        q->connect(m.removeCategory, SIGNAL(clicked()),                mList,            SLOT(deleteCategory()));
                    }
                }
                else
                {
                    throw std::runtime_error(qPrintable(Base::tr("Missing Viewer")));
                }
            }
            else
            {
                throw std::runtime_error(qPrintable(Base::tr("Missing List Model")));
            }
        }
        else
        {
            throw std::runtime_error(qPrintable(Base::tr("Missing List widget")));
        }
    }


    /*!
     */
    void Base::BasePrivate::insertIntoFrame(Service::Widget pWidget, QFrame* pFrame)
    {
        pWidget->setParent(pFrame);
        auto    lLayout = new QVBoxLayout{pFrame};
        lLayout->setContentsMargins(0, 0, 0, 0);
        lLayout->addWidget(pWidget.get());
        pWidget.release();
    }


    /*!
     */
    DATA::Service* Base::BasePrivate::service(QString pTypeName, OSGi::Context* pContext)
    {
        return pContext->services()->findByTypeAndName<DATA::Service>(std::string{"fr.osg.ide.data."} + qPrintable(pTypeName));
    }


    /*! @param pContext  Contexte d'ex�cution.
        @param pTypeName Nom de la cat�gorie, utilis�, par convention, pour nommer le service associ� � ce widget.
     */
    Base::Base(OSGi::Context* pContext, QString pTypeName)
        : QWidget()
        , CORE::Translate<Base>()
        , CORE::Synchronized(BasePrivate::service(pTypeName, pContext)->info("sync"))
        , pthis{this, pContext, pTypeName}
    { }


    /*!
     */
    Base::~Base() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    CORE::Translated::Updater Base::buildTranslater()
    {
        return [=]()
        {
            pthis->m.retranslateUi(this);
            setWindowTitle(pthis->mService->info("dataName"));
            if (auto lList = dynamic_cast<Translated*>(pthis->mList))
            {
                lList->translater()();
            }
            if (auto lView = dynamic_cast<Translated*>(pthis->mView))
            {
                lView->translater()();
            }
            if (auto lBrowser = dynamic_cast<Translated*>(pthis->mBrowser))
            {
                lBrowser->translater()();
            }
        };
    }
}
