/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_VIEWER_HH
#define DATA_VIEWER_HH

#include "DATA.hh"

/*! @file IDE/DATA/Viewer.hh
    @brief En-t�te de la classe DATA::Viewer.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 22 Juin 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QModelIndex>
#include <QWidget>

#include "CORE/Private.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Base pour l'affichage d'une cat�gorie de donn�es.
        @version 0.5

        Lors de la s�lection d'une cat�gorie dans un widget liste, les donn�es de cette cat�gorie vont �tre affich�es de mani�re
        d�taill�e dans une instance de ce widget (ou plut�t, une des ses classes d�riv�es).@n
     */
    class Viewer : public QWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                        Viewer();                                           //!< Constructeur.
virtual                 ~Viewer();                                          //!< Destructeur.
        //@}
        void            setListModel(ListModel* pModel);                    //!< Mod�le de cat�gories associ�.

    public slots:
        //! @name Gestion d'une cat�gorie
        //@{
        void            onCategorySelection(CategoryModel* pSelected);      //!< D�finition de la cat�gorie � g�rer.

    protected:

        CategoryModel*  category();                                         //!< Cat�gorie actuellement g�r�e.
        //@}
virtual QModelIndexList selection() = 0;                                    //!< 
virtual void            keyPressEvent(QKeyEvent* pEvent);                   //!< 
        //! @name Gestion du drag'n'drop
        //@{
virtual void            dragEnterEvent(QDragEnterEvent* pEvent) override;   //!< D�but d'un glisser/d�poser.
virtual void            dropEvent(QDropEvent* pEvent) override;             //!< D�p�t.
        //@}
    protected slots:

        void            removeSelection();                                  //!< 

    private:

virtual void            manage(CategoryModel* pCurrent) = 0;                //!< Gestion d'une cat�gorie.
        Q_CUSTOM_DECLARE_PRIVATE(Viewer)
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Affichage d'une cat�gorie de donn�e.
        @version 0.5
     */
    class CategoryViewer : public Viewer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        CategoryViewer();                           //!< Constructeur.
virtual                 ~CategoryViewer();                          //!< Destructeur.
        //@}
    protected:

virtual QModelIndexList selection() override;                       //!< 

    private:

virtual void            manage(CategoryModel* pCurrent) override;   //!< Gestion d'une cat�gorie.

        Q_CUSTOM_DECLARE_PRIVATE(CategoryViewer)
    };
}

#endif  // DATA_VIEWER_HH
