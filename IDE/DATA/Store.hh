/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_STORE_HH
#define DATA_STORE_HH

#include "DATA.hh"

/*! @file IDE/DATA/Store.hh
    @brief En-t�te de la classe DATA::Store.
    @author @ref Guillaume_Terrissol
    @date 16 D�cembre 2013 - 16 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

class QImage;
class QString;
class QStringList;

#include "CORE/Private.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//                                   "Magasin"
//------------------------------------------------------------------------------

    /*! @brief Permanence de donn�es purement �diteur.
        @version 0.5

        Il peut �tre indispensable, pour certains composants IHM, de sauvegarder quelques donn�es qui n'ont rien � faire dans le
        @b pak file. D�river de cette classe apporte des fonctionnalit�s de sauvegarde/restauration de donn�es � usage simple. @n
        Les donn�es sont stock�es sous la forme de paires clef/valeur.
     */
    class Store
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Store(QString pName);                   //!< Constructeur
                    ~Store();                               //!< Destructeur.
        //@}
        //! @name Gestion du nom
        //@{
        QString     name() const;                           //!< Nom actuel.
        QString     rename(QString pNewName);               //!< Renommage.
        //@}
        //! @name Permanence
        //@{
        void        load();                                 //!< Chargement des donn�es.
        void        save();                                 //!< Sauvegarde des donn�es
        //@}
        //! @name Interface 
        //@{
        void        set(QString pKey, QString pValue);      //!< D�finition d'une cha�ne.
        void        set(QString pKey, QStringList pValue);  //!< D�finition d'une liste de cha�ne.
        void        set(QString pKey, QImage pValue);       //!< D�finition d'une image.

        QString     string(QString pKey) const;             //!< R�cup�ration d'une cha�ne.
        QStringList list(QString pKey) const;               //!< R�cup�ration d'une liste de cha�ne.
        QImage      image(QString pKey) const;              //!< R�cup�ration d'une image.
        //@}
    protected:

virtual QString     saveFolder() const = 0;                 //!< R�pertoire de sauvegarde.
virtual void        fillAtCreation();                       //!< D�finitions initiales.

    private:
        Q_CUSTOM_DECLARE_PRIVATE(Store)
    };
}

#endif  // DATA_STORE_HH
