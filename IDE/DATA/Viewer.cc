/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Viewer.hh"

/*! @file IDE/DATA/Viewer.cc
    @brief M�thodes (non-inline) de la classe DATA::Viewer.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 21 Juin 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMessageBox>
#include <QMimeData>
#include <QScrollBar>

#include "ListModel.hh"
#include "Viewer.ui.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//                                Viewer : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::Viewer.
        @version 0.5
     */
    class Viewer::ViewerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Viewer)
    public:
                ViewerPrivate(Viewer* pThat);   //!< Constructeur.

        bool    import(QString pFile);          //!< Import d'une donn�e.


        ListModel*      mModel;                 //!< Mod�le des cat�gories.
        CategoryModel*  mCategory;              //!< Cat�gorie.
    };


    /*! @param pThat Widget dont l'instance est le p-impl
     */
    Viewer::ViewerPrivate::ViewerPrivate(Viewer* pThat)
        : q{pThat}
        , mModel{}
        , mCategory{}
    { }



    /*! @param pFile Fichier � importer
        @retval true Si l'import a r�ussi (au moins en partie)
        @retval false En cas d'�chec complet
     */
    bool Viewer::ViewerPrivate::import(QString pFile)
    {
        // Encore une v�rification.
        QRegExp lRE{QStringLiteral(R"(.*\.(\w+))")};
        if (lRE.exactMatch(pFile) && mModel->fileExtensions().contains(QStringLiteral("*.%1").arg(lRE.cap(1))))
        {
            return mCategory->import(QStringList{pFile});
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                      Viewer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Par d�faut.
     */
    Viewer::Viewer()
        : QWidget()
        , pthis{this}
    {
        setAcceptDrops(true);
    }


    /*! Par d�faut.
     */
    Viewer::~Viewer() = default;


//------------------------------------------------------------------------------
//                            Viewer : Autre M�thode
//------------------------------------------------------------------------------

    /*! @param pModel Mod�le des cat�gories
     */
    void Viewer::setListModel(ListModel* pModel)
    {
        pthis->mModel = pModel;
    }


//------------------------------------------------------------------------------
//                       Viewer : Gestion d'une Cat�gorie
//------------------------------------------------------------------------------

    /*! @param pSelected Cat�gorie � afficher
     */
    void Viewer::onCategorySelection(CategoryModel* pSelected)
    {
        manage(pthis->mCategory = pSelected);
    }


    /*! @return La cat�gorie g�r�e
     */
    CategoryModel* Viewer::category()
    {
        return pthis->mCategory;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Un seul �v�nement est g�r� pour le moment : la suppression de cat�gories par appui sur @b DEL .
        @param pEvent Ev�nement � g�rer
     */
    void Viewer::keyPressEvent(QKeyEvent* pEvent)
    {
        QWidget::keyPressEvent(pEvent);

        if (pEvent->key() == Qt::Key_Delete)
        {
            removeSelection();
        }
    }


//------------------------------------------------------------------------------
//                        Viewer : Gestion du Drag'n'Drop
//------------------------------------------------------------------------------

    /*! @param pEvent Ev�nement d'entr�e en glisser/d�poser
        @note Si le type d'un des fichiers � d�poser n'est pas connu, l'op�ration est refus�e
     */
    void Viewer::dragEnterEvent(QDragEnterEvent* pEvent)
    {
        // Si une cat�gorie est affich�e, c'est bon; sinon, refus de drop.
        if ((pthis->mCategory != nullptr) && (pEvent->mimeData()->hasUrls()))
        {
            QRegExp lRE{QStringLiteral(R"(.*\.(\w+))")};
            bool    lDropOk = true;
            for(auto lURL : pEvent->mimeData()->urls())
            {
                if (!lRE.exactMatch(lURL.path()) || !pthis->mModel->fileExtensions().contains(QStringLiteral("*.%1").arg(lRE.cap(1))))
                {
                    lDropOk = false;
                }
            }
            if (lDropOk)
            {
                pEvent->acceptProposedAction();
            }
        }
    }


    /*! Le d�p�t consiste � importer les donn�es d�poser.
        @param pEvent Ev�nement de d�p�t
        @note En cas d'�chec, les fichiers posant probl�me seront indiqu�s dans une bo�te de dialogue
     */
    void Viewer::dropEvent(QDropEvent* pEvent)
    {
        if (pEvent->mimeData()->hasUrls())
        {
            for(auto lURL : pEvent->mimeData()->urls())
            {
                auto    lFile = lURL.path();
#ifdef MINGW    
                if (!lFile.isEmpty() && (lFile[0] == QChar('/')))
                {
                    lFile = lFile.mid(1);
                }
#endif  // MINGW
                pthis->import(lFile);
            }

            pEvent->acceptProposedAction();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Viewer::removeSelection()
    {
        if (pthis->mCategory != nullptr)
        {
            pthis->mCategory->remove(selection());
        }
    }


//------------------------------------------------------------------------------
//                     Viewer : Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn void Viewer::manage(CategoryModel* pCurrent)
        @param pCurrent Cat�gorie � afficher
     */


//------------------------------------------------------------------------------
//                            CategoryViewer : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de DATA::CategoryViewer.
        @version 0.2
     */
    class CategoryViewer::CategoryViewerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(CategoryViewer)
    public:

        CategoryViewerPrivate(CategoryViewer* pThat);   //!< Constructeur.

        Ui::Viewer  m;                                  //!< Interface cr��e par Designer.
    };


    /*! @param pThat Afficheur dont l'instance est le p-impl
     */
    CategoryViewer::CategoryViewerPrivate::CategoryViewerPrivate(CategoryViewer* pThat)
        : q{pThat}
        , m{}
    {
        m.setupUi(q);
        auto    lMaxSize = m.elements->maximumSize();
        lMaxSize.setHeight(CategoryModel::kSnapshotSize.height() + m.elements->horizontalScrollBar()->height());
        m.elements->setMaximumSize(lMaxSize);
    }


//------------------------------------------------------------------------------
//                   CategoryViewer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Par d�faut.
     */
    CategoryViewer::CategoryViewer()
        : Viewer()
        , pthis{this}
    { }


    /*! Par d�faut.
     */
    CategoryViewer::~CategoryViewer() = default;


//------------------------------------------------------------------------------
//                       CategoryViewer : Autres M�thodes
//------------------------------------------------------------------------------

    /*!
     */
    QModelIndexList CategoryViewer::selection()
    {
        return pthis->m.elements->selectionModel()->selectedIndexes();
    }


    /*! @param pCurrent Cat�gorie � afficher
     */
    void CategoryViewer::manage(CategoryModel* pCurrent)
    {
        pthis->m.elements->setModel(pCurrent);
    }
}
