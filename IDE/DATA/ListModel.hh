/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DATA_LISTMODEL_HH
#define DATA_LISTMODEL_HH

#include "DATA.hh"

/*! @file IDE/DATA/ListModel.hh
    @brief En-t�te de la classe DATA::ListModel.
    @author @ref Guillaume_Terrissol
    @date 11 Novembre 2013 - 22 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QAbstractListModel>
class QItemSelectionModel;

#include <OSGi/Context.hh>

#include "CORE/Private.hh"

#include "CategoryModel.hh"
#include "Store.hh"

namespace DATA
{
//------------------------------------------------------------------------------
//                      Mod�le pour une Liste de Cat�gorie
//------------------------------------------------------------------------------

    /*! @brief Mod�le pour une liste de cat�gories (affich�e dans DATA::List).
        @version 0.5
     */
    class ListModel : public QAbstractListModel, public Store
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                            ListModel(List* pWidget, QString pName, OSGi::Context* pContext);                               //!< Constructeur.
virtual                     ~ListModel();                                                                                   //!< Destructeur.
        //@}
        OSGi::Context*      context() const;                                                                                //!< Contexte d'ex�cution.
        //! @name Extensions des donn�es import�es
        //@{
        void                setFileExtensions(QStringList pList);                                                           //!< D�finition des extensions de fichier
        QStringList         fileExtensions() const;                                                                         //!< 
        //@}
        //! @name Nombre de lignes
        //@{
        void                setMaxRowCount(int pCount);                                                                     //!< D�finition du nombre maximum.
        int                 maxRowCount() const;                                                                            //!< Nombre maximum.
        int                 rowCount(const QModelIndex& = QModelIndex()) const override;                                    //!< Nombre actuel de lignes.
        //@}
        //! @name Contenu des items
        //@{
        Qt::ItemFlags       flags(const QModelIndex& pIndex) const override;                                                //!< Propri�t�s des items.
        QVariant            data(const QModelIndex& pIndex, int pRole = Qt::DisplayRole) const override;                    //!< [Quelques] Donn�es d'un item.
        bool                setData(const QModelIndex& pIndex, const QVariant& pValue, int pRole = Qt::EditRole) override;  //!< D�finition de [quelques] donn�es d'un item.
        bool                mayRemove(const QModelIndex& pIndex) const;                                                     //!< Possibilit� de suppression.
        //@}
        //! @name Gestion du drag'n'drop
        //@{
        QStringList         mimeTypes() const override;                                                                     //!< Types acceptables.
        bool                canDropMimeData(const QMimeData* pData, Qt::DropAction pAction,
                                            int, int, const QModelIndex&) const override;                                   //!< Possibilit� de d�p�t.
        bool                dropMimeData(const QMimeData* pData, Qt::DropAction pAction,
                                         int pRow, int pColumn, const QModelIndex& pParent) override;                       //!< D�p�t.
        Qt::DropActions     supportedDragActions() const override;                                                          //!< Actions de glissement disponibles.
        Qt::DropActions     supportedDropActions() const override;                                                          //!< Actions de d�p�t disponibles.
        //@}
        //! @name Cat�gories (fonctions IHM)
        //@{
        void                addCategory();                                                                                  //!< Ajout d'une nouvelle cat�gorie.
        void                delCategories();                                                                                //!< Suppression des cat�gories s�lectionn�es.
        CategoryModel*      category(QString pName);                                                                        //!< R�cup�ration d'une cat�gorie.
        void                setSelectionModel(QItemSelectionModel* pModel);                                                 //!< D�finition du mod�le de s�lection.
        //@}
        //! @name Activation
        //@{
        bool                isEnabled() const;                                                                              //!< Activ� ?

    protected slots:

        void                setEnabled(bool pEnabled);                                                                      //!< Disponibilit� des cat�gories.

    protected:

virtual void                editNow();                                                                                      //!< Activation du mod�le.
virtual void                endEdit();                                                                                      //!< D�sactivation du mod�le.
        //@}
        void                refresh();
        //! @name Cat�gories (fonctions du mod�le)
        //@{
        QModelIndex         categoryIndex(const QString& pName);

    private:

        bool                doAddCategory(const QModelIndex& pIndex, const QString& pAdded);                                //!< Ajout d'une cat�gorie.
        bool                doDelCategory(const QModelIndex& pIndex);                                                       //!< Suppression d'une cat�gorie 

        QString             nameForNewCategory() const;                                                                     //!< Nom pour une nouvelle cat�gorie.
        //@}
        //! @name Cat�gories (fonctions des mod�les sp�cialis�s)
        //@{
virtual int                 newCategoryId() = 0;
virtual void                freeCategoryId(int pId) = 0;

virtual CategoryModel::Ptr  makeCategory(QString pName, int pId) = 0;                                                       //!< Instantiation d'une cat�gorie.
virtual QString             categoryName() const = 0;                                                                       //!< Nom du type de cat�gorie.
virtual bool                isRemovable(const QModelIndex& pIndex) const = 0;                                               //!< Possibilit� de suppression.
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(ListModel)
    };
}

#endif  // DATA_LISTMODEL_HH
