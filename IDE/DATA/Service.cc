/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Service.hh"

/*! @file IDE/DATA/Service.cc
    @brief M�thodes (non-inline) de la classe DATA::Service.
    @author @ref Guillaume_Terrissol
    @date 20 Ao�t 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <stdexcept>

#include <QCoreApplication>
#include <QWidget>

namespace DATA
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Service::Service(OSGi::Context* pContext, Aliases pAliases)
        : mContext(pContext)
        , mAliases{pAliases}
    {
        for(auto a : QStringList{ "list", "viewer", "sync" })
        {
            if (mAliases.count(a) == 0)
            {
                throw std::invalid_argument(qPrintable(QCoreApplication::translate("DATA::Service", "Missing %1").arg(a)));
            }
        }
    }


    /*!
     */
    Service::~Service() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pWhich Type de widget � construire. Par d�faut, doivent au moins �tre fournis des widgets pour la liste de cat�gories
        ("list"), et pour l'affichage sp�cialis� ("view")
     */
    Service::Widget Service::widget(QString pWhich) const
    {
        auto lName = mAliases.find(pWhich);
        if (lName != mAliases.end())
        {
            return mContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui")->build(lName->second);
        }
        else
        {
            return Widget{};
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Etats de synchronisation de l'IHM associ�e ("sync")
     */
    QString Service::info(QString pWhich) const
    {
        if (pWhich == QStringLiteral("dataName"))
        {
            return dataTypeName();
        }
        else
        {
            auto lName = mAliases.find(pWhich);
            if (lName != mAliases.end())
            {
                return lName->second;
            }
            else
            {
                return QString::null;
            }
        }
    }


    /*! @fn QString Service::dataTypeName() const
        Contrairement aux autres informations, le nom du type doit �tre traduit, et ne peut donc �tre d�fini statiquement comme les
        autres. D'o� une m�thode � r�impl�menter.
     */
}
