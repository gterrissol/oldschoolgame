/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef UI_UI_HH
#define UI_UI_HH

/*! @file IDE/UI/UI.hh
    @brief En-t�te du module UI.
    @author @ref Guillaume_Terrissol
    @date 28 F�vrier 2004 - 3 Mai 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
    @note Ce fichier n'existe que pour permettre la compilation des fichier moc.
 */

#endif  // De UI_UI_HH
