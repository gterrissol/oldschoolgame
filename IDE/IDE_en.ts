<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name></name>
    <message>
        <source>Tuile</source>
        <translation type="obsolete">Tile</translation>
    </message>
    <message>
        <source>Editeur de tuile</source>
        <translation type="obsolete">Tile editor</translation>
    </message>
    <message>
        <source>Fragments de texture</source>
        <translation type="obsolete">Texture fragments</translation>
    </message>
    <message>
        <source>Fragments de texture de tuile</source>
        <translation type="obsolete">Tile texture fragments</translation>
    </message>
    <message>
        <source>Univers</source>
        <translation type="obsolete">Universe</translation>
    </message>
    <message>
        <source>Editeur d&apos;univers</source>
        <translation type="obsolete">Universe editor</translation>
    </message>
    <message>
        <source>Pinceaux</source>
        <translation type="obsolete">Brushes</translation>
    </message>
    <message>
        <source>Monde extérieur</source>
        <translation type="obsolete">Outside world</translation>
    </message>
    <message>
        <source>Editeur de monde extérieur</source>
        <translation type="obsolete">Outside world editor</translation>
    </message>
    <message>
        <source>Ressources système</source>
        <translation type="obsolete">System resources</translation>
    </message>
    <message>
        <source>Utilisation des ressources système</source>
        <translation type="obsolete">System resources use</translation>
    </message>
</context>
<context>
    <name>AboutBox</name>
    <message>
        <location filename="UI/AboutBox.ui" line="16"/>
        <source>A propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="UI/AboutBox.ui" line="148"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message utf8="true">
        <location filename="UI/AboutBox.ui" line="98"/>
        <source>Projet Old School Game :&lt;br&gt;Environnement de Développement Intégré (IDE)</source>
        <translation>Old School Game Project :&lt;br&gt;Integrated Development Environment (IDE)</translation>
    </message>
    <message utf8="true">
        <location filename="UI/AboutBox.ui" line="108"/>
        <source>Développeurs :&lt;br&gt;Bug (Guillaume Terrissol)&lt;br&gt;Vincoof (Vincent David)</source>
        <translation>Coders :&lt;br&gt;Bug (Guillaume Terrissol)&lt;br&gt;Vincoof (Vincent David)</translation>
    </message>
</context>
<context>
    <name>Brushes</name>
    <message>
        <location filename="UI/Brushes.ui" line="27"/>
        <source>Environnements</source>
        <translation>Environments</translation>
    </message>
    <message>
        <location filename="UI/Brushes.ui" line="54"/>
        <source>Falaises</source>
        <translation>Cliffs</translation>
    </message>
    <message>
        <location filename="UI/Brushes.ui" line="272"/>
        <source>Altitudes</source>
        <translation>Altitudes</translation>
    </message>
    <message>
        <location filename="UI/Brushes.ui" line="484"/>
        <source>Objets</source>
        <translation>Objects</translation>
    </message>
    <message>
        <location filename="UI/Brushes.ui" line="642"/>
        <source>Taille</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="UI/Brushes.ui" line="869"/>
        <source>Forme</source>
        <translation>Shape</translation>
    </message>
</context>
<context>
    <name>EDIT::MainEditor</name>
    <message>
        <location filename="EDITor/MainEditor.cc" line="327"/>
        <source>Outils</source>
        <translation type="unfinished">Tools</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="365"/>
        <source>Editeurs</source>
        <translation type="unfinished">Editors</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="531"/>
        <source>&amp;Démarrer</source>
        <translation type="unfinished">&amp;Launch</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="532"/>
        <source>F5</source>
        <comment>Moteur | Démarrer</comment>
        <translation type="unfinished">F5</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="533"/>
        <source>Lance le moteur</source>
        <translation type="unfinished">Launches the engine</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="535"/>
        <source>&amp;Arrêter</source>
        <translation type="unfinished">&amp;Stop</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="536"/>
        <source>F5</source>
        <comment>Moteur | Arrêter</comment>
        <translation type="unfinished">F5</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="537"/>
        <source>Stoppe le moteur</source>
        <translation type="unfinished">Stops the engine</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="539"/>
        <source>&amp;Redémarrer</source>
        <translation type="unfinished">&amp;Restart</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="540"/>
        <source>F7</source>
        <comment>Moteur | Redémarrer</comment>
        <translation type="unfinished">F7</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="541"/>
        <source>Arrête puis relance le moteur</source>
        <translation type="unfinished">Stops then restarts the engine</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="543"/>
        <source>Boîte à ou&amp;tils</source>
        <translation type="unfinished">&amp;Toolbox</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="544"/>
        <source>Ctrl+T</source>
        <comment>Outils | Boîte à outils</comment>
        <translation type="unfinished">Ctrl+T</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="545"/>
        <source>Ouvre la boîte à outils</source>
        <translation type="unfinished">Opens the toolbox</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="547"/>
        <source>Edite&amp;ur</source>
        <translation type="unfinished">Vis&amp;ualizer</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="548"/>
        <source>Ctrl+U</source>
        <comment>Outils | Editeurs</comment>
        <translation type="unfinished">Ctrl+U</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="549"/>
        <source>Ouvre l&apos;éditeur de données</source>
        <translation type="unfinished">Opens the data visualizer</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="554"/>
        <source>&amp;Moteur</source>
        <translation type="unfinished">En&amp;gine</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="555"/>
        <source>&amp;Outils</source>
        <translation type="unfinished">T&amp;ools</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="615"/>
        <location filename="EDITor/MainEditor.cc" line="622"/>
        <source>Ouverture d&apos;un pak file</source>
        <translation type="unfinished">Pak file opening</translation>
    </message>
    <message>
        <location filename="EDITor/MainEditor.cc" line="615"/>
        <location filename="EDITor/MainEditor.cc" line="622"/>
        <source>L&apos;ouverture du fichier %1 a échoué</source>
        <translation type="unfinished">Pak file %1 opening failed</translation>
    </message>
</context>
<context>
    <name>EDIT::OMainEditor</name>
    <message>
        <source>&amp;Démarrer</source>
        <translation type="obsolete">&amp;Launch</translation>
    </message>
    <message>
        <source>F5</source>
        <comment>Moteur | Démarrer</comment>
        <translation type="obsolete">F5</translation>
    </message>
    <message>
        <source>Lance le moteur</source>
        <translation type="obsolete">Launches the engine</translation>
    </message>
    <message>
        <source>&amp;Arrêter</source>
        <translation type="obsolete">&amp;Stop</translation>
    </message>
    <message>
        <source>F5</source>
        <comment>Moteur | Arrêter</comment>
        <translation type="obsolete">F5</translation>
    </message>
    <message>
        <source>Stoppe le moteur</source>
        <translation type="obsolete">Stops the engine</translation>
    </message>
    <message>
        <source>&amp;Redémarrer</source>
        <translation type="obsolete">&amp;Restart</translation>
    </message>
    <message>
        <source>F7</source>
        <comment>Moteur | Redémarrer</comment>
        <translation type="obsolete">F7</translation>
    </message>
    <message>
        <source>Arrête puis relance le moteur</source>
        <translation type="obsolete">Stops then restarts the engine</translation>
    </message>
    <message>
        <source>Boîte à ou&amp;tils</source>
        <translation type="obsolete">&amp;Toolbox</translation>
    </message>
    <message>
        <source>Ctrl+T</source>
        <comment>Outils | Boîte à outils</comment>
        <translation type="obsolete">Ctrl+T</translation>
    </message>
    <message>
        <source>Ouvre la boîte à outils</source>
        <translation type="obsolete">Opens the toolbox</translation>
    </message>
    <message>
        <source>Edite&amp;ur</source>
        <translation type="obsolete">Vis&amp;ualizer</translation>
    </message>
    <message>
        <source>Ctrl+U</source>
        <comment>Outils | Editeurs</comment>
        <translation type="obsolete">Ctrl+U</translation>
    </message>
    <message>
        <source>Ouvre l&apos;éditeur de données</source>
        <translation type="obsolete">Opens the data visualizer</translation>
    </message>
    <message>
        <source>&amp;Moteur</source>
        <translation type="obsolete">En&amp;gine</translation>
    </message>
    <message>
        <source>&amp;Outils</source>
        <translation type="obsolete">T&amp;ools</translation>
    </message>
    <message>
        <source>L&apos;ouverture du fichier %1 a échoué</source>
        <translation type="obsolete">Pak file %1 opening failed</translation>
    </message>
</context>
<context>
    <name>EDIT::OSystem</name>
    <message>
        <source>&lt;qt&gt;%1 Mo &lt;font color=&quot;#FF8000&quot;&gt;(%2 ko)&lt;/font&gt;&lt;/qt&gt;</source>
        <translation type="obsolete">&lt;qt&gt;%1 MB &lt;font color=&quot;#FF8000&quot;&gt;(%2 kB)&lt;/font&gt;&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>EDIT::System</name>
    <message>
        <location filename="EDITor/System.cc" line="660"/>
        <source>&lt;qt&gt;%1 Mo &lt;font color=&quot;#FF8000&quot;&gt;(%2 ko)&lt;/font&gt;&lt;/qt&gt;</source>
        <translation type="unfinished">&lt;qt&gt;%1 MB &lt;font color=&quot;#FF8000&quot;&gt;(%2 kB)&lt;/font&gt;&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>FamilyDialog</name>
    <message>
        <location filename="UI/FamilyDialog.ui" line="47"/>
        <source>Nouvelle famille</source>
        <translation>New family</translation>
    </message>
    <message>
        <location filename="UI/FamilyDialog.ui" line="192"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="UI/FamilyDialog.ui" line="199"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="UI/FamilyDialog.ui" line="91"/>
        <source>Couleur</source>
        <translation>Color</translation>
    </message>
</context>
<context>
    <name>FragmentDialog</name>
    <message>
        <location filename="UI/FragmentDialog.ui" line="83"/>
        <source>Fragments</source>
        <translation>Fragments</translation>
    </message>
    <message>
        <location filename="UI/FragmentDialog.ui" line="311"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="UI/FragmentDialog.ui" line="318"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>ModelLibrary</name>
    <message utf8="true">
        <location filename="UI/ModelLibrary.ui" line="20"/>
        <source>Catégories</source>
        <translation>Categories</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ModelLibrary.ui" line="30"/>
        <source>Modèles</source>
        <translation>Models</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ModelLibrary.ui" line="185"/>
        <source>Lumière</source>
        <translation>Light</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="138"/>
        <source>triangles</source>
        <translation>triangles</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="167"/>
        <source>octets</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="332"/>
        <location filename="UI/ModelLibrary.ui" line="414"/>
        <source>Ajouter</source>
        <translation>Add</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ModelLibrary.ui" line="14"/>
        <source>Bibliothèque de modèles</source>
        <translation>Models library</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="204"/>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="236"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ModelLibrary.ui" line="243"/>
        <source>Nouveau Matériau</source>
        <translation>New material</translation>
    </message>
    <message>
        <location filename="UI/ModelLibrary.ui" line="253"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
</context>
<context>
    <name>OFragmentDialog</name>
    <message>
        <source>Choisissez une image de taille %1</source>
        <translation type="obsolete">Choose an image of size %1</translation>
    </message>
    <message>
        <source>Images %1</source>
        <translation type="obsolete">Images %1</translation>
    </message>
    <message>
        <source>Image incorrecte</source>
        <translation type="obsolete">Invalid image</translation>
    </message>
    <message>
        <source>L&apos;image n&apos;est pas de taille %1. Choisissez-en une autre.</source>
        <translation type="obsolete">The image size isn&apos;t %1. Choose another one.</translation>
    </message>
</context>
<context>
    <name>OFragmentMgr</name>
    <message>
        <source>Pas de fragment</source>
        <translation type="obsolete">No fragment</translation>
    </message>
    <message>
        <source>1 fragment</source>
        <translation type="obsolete">1 fragment</translation>
    </message>
    <message>
        <source>%1 fragments</source>
        <translation type="obsolete">%1 fragments</translation>
    </message>
</context>
<context>
    <name>OMainEditor</name>
    <message>
        <source>Outils</source>
        <translation type="obsolete">Tools</translation>
    </message>
    <message>
        <source>Editeurs</source>
        <translation type="obsolete">Editors</translation>
    </message>
    <message>
        <source>Ouverture d&apos;un pak file</source>
        <translation type="obsolete">Pak file opening</translation>
    </message>
</context>
<context>
    <name>OutsideEditor</name>
    <message>
        <location filename="UI/OutsideEditor.ui" line="51"/>
        <source>Expansion</source>
        <translation>Expansion</translation>
    </message>
    <message utf8="true">
        <location filename="UI/OutsideEditor.ui" line="67"/>
        <source>Réduction</source>
        <translation>Reduction</translation>
    </message>
    <message>
        <location filename="UI/OutsideEditor.ui" line="331"/>
        <source>EST</source>
        <translation>EAST</translation>
    </message>
    <message>
        <location filename="UI/OutsideEditor.ui" line="341"/>
        <source>OUEST</source>
        <translation>WEST</translation>
    </message>
    <message>
        <location filename="UI/OutsideEditor.ui" line="351"/>
        <source>NORD</source>
        <translation>NORTH</translation>
    </message>
    <message>
        <location filename="UI/OutsideEditor.ui" line="381"/>
        <source>SUD</source>
        <translation>SOUTH</translation>
    </message>
</context>
<context>
    <name>PreferencesBox</name>
    <message>
        <location filename="UI/PreferencesBox.ui" line="81"/>
        <source>Langue</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="UI/PreferencesBox.ui" line="91"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="UI/PreferencesBox.ui" line="128"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="UI/PreferencesBox.ui" line="135"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message utf8="true">
        <location filename="UI/PreferencesBox.ui" line="27"/>
        <source>Préférences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="UI/PreferencesBox.ui" line="55"/>
        <source>Edition</source>
        <translation>Edition</translation>
    </message>
    <message>
        <location filename="UI/PreferencesBox.ui" line="60"/>
        <source>Visualisation</source>
        <translation>Visualization</translation>
    </message>
    <message utf8="true">
        <location filename="UI/PreferencesBox.ui" line="71"/>
        <source>Mode par défaut</source>
        <translation>Default mode</translation>
    </message>
</context>
<context>
    <name>ProjectBuilder</name>
    <message utf8="true">
        <location filename="UI/ProjectBuilder.ui" line="14"/>
        <source>Création de projet</source>
        <translation>Project creation</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="22"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="51"/>
        <source>Nouveau projet</source>
        <translation>New project</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="63"/>
        <source>Taille de la FAT</source>
        <translation>FAT size</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="70"/>
        <source> fichiers</source>
        <translation> files</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ProjectBuilder.ui" line="108"/>
        <source>Occupation mémoire</source>
        <translation>Memory load</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="125"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ProjectBuilder.ui" line="143"/>
        <source>Bases de données</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="UI/ProjectBuilder.ui" line="157"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Veuillez patienter</source>
        <comment>Progress Bar</comment>
        <translation type="obsolete">Please, wait</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="QPAcKage/ImportExport.cc" line="504"/>
        <source>Dans le fichier %1, la balise %2 est inconnue</source>
        <translation>Unknown tag %2 in file %1</translation>
    </message>
</context>
<context>
    <name>QPAK::Builder</name>
    <message>
        <location filename="QPAcKage/Builder.cc" line="172"/>
        <source> %1 octets</source>
        <translation type="unfinished"> %1 bytes</translation>
    </message>
    <message>
        <location filename="QPAcKage/Builder.cc" line="183"/>
        <source>Choisissez une base de donnée</source>
        <translation type="unfinished">Choose a database</translation>
    </message>
    <message>
        <location filename="QPAcKage/Builder.cc" line="185"/>
        <source>Pak file fat (FileDB.xml)</source>
        <translation type="unfinished">Pak file fat (FileDB.xml)</translation>
    </message>
</context>
<context>
    <name>QPAK::OBuilder</name>
    <message>
        <source> %1 octets</source>
        <translation type="obsolete"> %1 bytes</translation>
    </message>
    <message>
        <source>Choisissez une base de donnée</source>
        <translation type="obsolete">Choose a database</translation>
    </message>
    <message>
        <source>Pak file fat (FileDB.xml)</source>
        <translation type="obsolete">Pak file fat (FileDB.xml)</translation>
    </message>
</context>
<context>
    <name>QT3D::OWidget::OWidgetPrivate</name>
    <message>
        <source>Vue 3D</source>
        <translation type="obsolete">3D View</translation>
    </message>
</context>
<context>
    <name>QT3D::Widget</name>
    <message>
        <location filename="QT3D/Widget.cc" line="118"/>
        <source>Vue 3D</source>
        <translation type="unfinished">3D View</translation>
    </message>
</context>
<context>
    <name>QT::QApplication</name>
    <message>
        <location filename="QT/Application.cc" line="206"/>
        <source>Veuillez patienter</source>
        <comment>Progress Bar</comment>
        <translation type="unfinished">Please, wait</translation>
    </message>
</context>
<context>
    <name>QT::QMainWindow</name>
    <message>
        <location filename="QT/MainWindow.cc" line="454"/>
        <source>Choisissez un pak file</source>
        <translation>Choose a pak file</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="865"/>
        <source>&amp;Nouveau</source>
        <translation>&amp;New</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="866"/>
        <source>Ctrl+N</source>
        <comment>Fichier | Nouveau</comment>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="867"/>
        <source>Crée un nouveau fichier</source>
        <translation>Creates a new file</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="868"/>
        <source>Sélectionnez cette entrée pour créer un nouveau fichier</source>
        <translation>Select this entry to create a new file </translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="870"/>
        <source>&amp;Ouvrir...</source>
        <translation>&amp;Open...</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="871"/>
        <source>Ctrl+O</source>
        <comment>Fichier | Ouvrir</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="872"/>
        <source>Ouvre un fichier</source>
        <translation>Opens a file</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="873"/>
        <source>Sélectionnez cette entrée pour ouvrir un fichier existant</source>
        <translation>Select this entry to open an existing file</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="875"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="876"/>
        <source>Ctrl+S</source>
        <comment>Fichier | Enregistrer</comment>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="877"/>
        <source>Enregistre les modifications actuelles</source>
        <translation>Save current modifications</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="878"/>
        <source>Sélectionnez cette entrée pour forcer l&apos;enregistrement des modifications</source>
        <translation>Select this entry to force modifications recording</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="880"/>
        <source>&amp;Fermer</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="881"/>
        <source>Ctrl+W</source>
        <comment>Fichier | Fermer</comment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="882"/>
        <source>Ferme le fichier ouvert</source>
        <translation>Closes the opened file</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="883"/>
        <source>Sélectionnez cette entrée pour terminer l&apos;édition du fichier ouvert</source>
        <translation>Select this entry to end the opened file editing</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="885"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="886"/>
        <source>Ctrl+Q</source>
        <comment>Fichier | Quitter</comment>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="887"/>
        <source>Quitte l&apos;application</source>
        <translation>Quits the application</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="888"/>
        <source>Sélectionnez cette entrée pour quitter l&apos;application</source>
        <translation>Select this entry to quit the application</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="890"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="891"/>
        <source>Ctrl+Z</source>
        <comment>Edition | Undo</comment>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="892"/>
        <source>Annule la dernière action</source>
        <translation>Undoes the last action</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="893"/>
        <source>Sélectionnez cette entrée pour annuler la dernière action</source>
        <translation>Select this entry to undo the last action</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="895"/>
        <source>Rétablir</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="896"/>
        <source>Shift+Ctrl+Z</source>
        <comment>Edition | Redo</comment>
        <translation>Shift+Ctrl+Z</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="897"/>
        <source>Réeffectue la dernière action</source>
        <translation>Redoes the last action</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="898"/>
        <source>Sélectionnez cette entrée pour refaire la dernière action annulée</source>
        <translation>Select this entry to redo the last undone action</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="900"/>
        <location filename="QT/MainWindow.cc" line="927"/>
        <source>&amp;Edition</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="901"/>
        <source>Ctrl+E</source>
        <comment>Edition | Edition</comment>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="902"/>
        <source>Active le mode édition</source>
        <translation>Activates the edition mode</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="903"/>
        <source>Sélectionnez cette entré pour basculer de l&apos;édition (cochée) à la simple visualisation (décochée) des données</source>
        <translation>Select this entry to alternate data edition (checked) and visualization (unchecked)</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="905"/>
        <source>&amp;Préférences</source>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="906"/>
        <source>Ctrl+P</source>
        <comment>Edition | Préférences</comment>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="907"/>
        <source>Edite les préférences</source>
        <translation>Sets the preferences</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="908"/>
        <source>Sélectionnez cette entrée pour éditer les préférences de l&apos;application</source>
        <translation>Select this entry to edit the application preferences</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="910"/>
        <source>&amp;Manuel</source>
        <translation>&amp;Manual</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="911"/>
        <source>F1</source>
        <comment>Aide | Manuel</comment>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="912"/>
        <source>Ouvre l&apos;aide en ligne</source>
        <translation>Opens the on-line help</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="913"/>
        <source>Sélectionnez cette entrée pour ouvrir le manuel de l&apos;application</source>
        <translation>Select this entry to open the application manual</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="915"/>
        <source>&amp;Qu&apos;est-ce que c&apos;est ?</source>
        <translation>&amp;What&apos;s this ?</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="916"/>
        <source>Active le mode &quot;Qu&apos;est-ce-que c&apos;est ?&quot;</source>
        <translation>Activates the &quot;What&apos;s this mode ?&quot;</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="917"/>
        <source>Sélectionnez cette entrée pour afficher des informations sur les composants de l&apos;interface</source>
        <translation>Select this entry to display pieces of informations about the interface components</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="919"/>
        <source>&amp;A propos</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="920"/>
        <source>Affiche le copyright</source>
        <translation>Displays the copyright</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="921"/>
        <source>Sélectionnez cette entrée pour afficher les informations de copyright</source>
        <translation>Select this entry to display the copyright information</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="926"/>
        <source>&amp;Fichier</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="QT/MainWindow.cc" line="928"/>
        <source>&amp;Aide</source>
        <translation>&amp;Help</translation>
    </message>
</context>
<context>
    <name>QT::QWizard</name>
    <message>
        <location filename="QT/Wizard.cc" line="199"/>
        <source>Wizard</source>
        <comment>Titre du wizard</comment>
        <translation>Wizard</translation>
    </message>
    <message>
        <location filename="QT/Wizard.cc" line="200"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="QT/Wizard.cc" line="201"/>
        <source>&lt; &amp;Retour</source>
        <translation>&lt; &amp;Back</translation>
    </message>
    <message>
        <location filename="QT/Wizard.cc" line="202"/>
        <source>&amp;Suivant &gt;</source>
        <translation>&amp;Next &gt;</translation>
    </message>
    <message>
        <location filename="QT/Wizard.cc" line="203"/>
        <source>&amp;Terminer</source>
        <translation>&amp;Finish</translation>
    </message>
    <message>
        <location filename="QT/Wizard.cc" line="274"/>
        <source>Etape %1 sur %2</source>
        <translation>Step %1 of %2</translation>
    </message>
</context>
<context>
    <name>QTAB::QDock</name>
    <message>
        <location filename="QTAB/Dock.cc" line="747"/>
        <source>&amp;Ajouter un onglet</source>
        <translation>&amp;Add a tab</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="765"/>
        <source>Taille de l&apos;aperç&amp;u</source>
        <translation>Previe&amp;w size</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="768"/>
        <source>Min&amp;uscule</source>
        <translation>Min&amp;uscule</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="769"/>
        <source>Très pe&amp;tit</source>
        <translation>Very li&amp;ttle</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="770"/>
        <source>&amp;Petit</source>
        <translation>&amp;Little</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="771"/>
        <source>&amp;Moyen</source>
        <translation>A&amp;verage</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="772"/>
        <source>&amp;Grand</source>
        <translation>La&amp;rge</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="773"/>
        <source>Très gr&amp;and</source>
        <translation>Very l&amp;arge</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="774"/>
        <source>&amp;Enorme</source>
        <translation>&amp;Huge</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="775"/>
        <source>&amp;Immense</source>
        <translation>Boundle&amp;ss</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="776"/>
        <source>Gigantes&amp;que</source>
        <translation>&amp;Gigantic</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="802"/>
        <source>&amp;Style d&apos;onglet</source>
        <translation>Tab &amp;style</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="805"/>
        <source>&amp;Icône</source>
        <translation>&amp;Icon</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="806"/>
        <source>&amp;Etat courant</source>
        <translation>Current stat&amp;e</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="807"/>
        <source>&amp;Texte</source>
        <translation>&amp;Text</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="808"/>
        <source>I&amp;cône et texte</source>
        <translation>I&amp;con and text</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="809"/>
        <source>St&amp;atut et texte</source>
        <translation>St&amp;atus and text</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="833"/>
        <source>Style de la &amp;barre</source>
        <translation>&amp;Bar style</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="836"/>
        <source>&amp;Moderne</source>
        <translation>&amp;Modern</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="841"/>
        <source>&amp;Classique</source>
        <translation>&amp;Classic</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="887"/>
        <source>&amp;Ferme l&apos;onglet</source>
        <translation>&amp;Close the tab</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="889"/>
        <source>&amp;Détacher l&apos;onglet</source>
        <translation>&amp;Dedock the tab</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="903"/>
        <source>Voir comme une &amp;liste</source>
        <translation>See as a &amp;list</translation>
    </message>
    <message>
        <location filename="QTAB/Dock.cc" line="911"/>
        <source>Voir comme une &amp;grille</source>
        <translation>See as a &amp;grid</translation>
    </message>
</context>
<context>
    <name>QTAB::QPage</name>
    <message>
        <location filename="QTAB/Page.cc" line="191"/>
        <source>Menu de %1</source>
        <translation>%1 menu</translation>
    </message>
</context>
<context>
    <name>QTIL::FragmentDialog</name>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="835"/>
        <source>Choisissez une image de taille %1</source>
        <translation type="unfinished">Choose an image of size %1</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="837"/>
        <source>Images %1</source>
        <translation type="unfinished">Images %1</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="852"/>
        <source>Image incorrecte</source>
        <translation type="unfinished">Invalid image</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="853"/>
        <source>L&apos;image n&apos;est pas de taille %1. Choisissez-en une autre.</source>
        <translation type="unfinished">The image size isn&apos;t %1. Choose another one.</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="900"/>
        <source>Création d&apos;un fragment</source>
        <translation type="unfinished">Fragment creation</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="915"/>
        <source>Edition d&apos;un fragment</source>
        <translation type="unfinished">Fragment edition</translation>
    </message>
</context>
<context>
    <name>QTIL::FragmentMgr</name>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="186"/>
        <source>Pas de fragment</source>
        <translation type="unfinished">No fragment</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="189"/>
        <source>1 fragment</source>
        <translation type="unfinished">1 fragment</translation>
    </message>
    <message>
        <location filename="QTILe/FragmentMgr.cc" line="192"/>
        <source>%1 fragments</source>
        <translation type="unfinished">%1 fragments</translation>
    </message>
</context>
<context>
    <name>QTIL::OFragmentDialog</name>
    <message>
        <source>Création d&apos;un fragment</source>
        <translation type="obsolete">Fragment creation</translation>
    </message>
    <message>
        <source>Edition d&apos;un fragment</source>
        <translation type="obsolete">Fragment edition</translation>
    </message>
</context>
<context>
    <name>ResourceViewer</name>
    <message utf8="true">
        <location filename="UI/ResourceViewer.ui" line="20"/>
        <source>Mémoire</source>
        <translation>Memory</translation>
    </message>
    <message>
        <location filename="UI/ResourceViewer.ui" line="78"/>
        <source>Utilisation</source>
        <translation>Use</translation>
    </message>
    <message utf8="true">
        <location filename="UI/ResourceViewer.ui" line="212"/>
        <source>&lt;qt&gt;&lt;font color=#40FF40&gt;Mémoire allouée&lt;/font&gt;&lt;br&gt;&lt;font color=#FF8000&gt;Mémoire inutilisée&lt;/font&gt;&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;&lt;font color=#40FF40&gt;Allocated memory&lt;/font&gt;&lt;br&gt;&lt;font color=#FF8000&gt;Unused memory&lt;/font&gt;&lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="UI/ResourceViewer.ui" line="274"/>
        <source>Historique</source>
        <translation>History</translation>
    </message>
</context>
<context>
    <name>operator&lt;&lt;(LOG::Log&amp;, const Extension&amp;</name>
    <message>
        <location filename="QPAcKage/Extension.cc" line="177"/>
        <source>clef manquante</source>
        <translation type="unfinished">missing key</translation>
    </message>
    <message>
        <location filename="QPAcKage/Extension.cc" line="178"/>
        <source>nom de fichier manquant</source>
        <translation type="unfinished">missing file name</translation>
    </message>
    <message>
        <location filename="QPAcKage/Extension.cc" line="179"/>
        <source>commentaire manquant</source>
        <translation type="unfinished">missing comment</translation>
    </message>
    <message>
        <location filename="QPAcKage/Extension.cc" line="180"/>
        <source>répertoire manquant</source>
        <translation type="unfinished">missing directory</translation>
    </message>
    <message>
        <location filename="QPAcKage/Extension.cc" line="181"/>
        <source>format manquant</source>
        <translation type="unfinished">missing format</translation>
    </message>
</context>
<context>
    <name>operator&lt;&lt;(LOG::OLog&amp;, const OExtension&amp;</name>
    <message>
        <source>clef manquante</source>
        <translation type="obsolete">missing key</translation>
    </message>
    <message>
        <source>nom de fichier manquant</source>
        <translation type="obsolete">missing file name</translation>
    </message>
    <message>
        <source>commentaire manquant</source>
        <translation type="obsolete">missing comment</translation>
    </message>
    <message>
        <source>répertoire manquant</source>
        <translation type="obsolete">missing directory</translation>
    </message>
    <message>
        <source>format manquant</source>
        <translation type="obsolete">missing format</translation>
    </message>
</context>
</TS>
