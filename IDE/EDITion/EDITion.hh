/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_EDITION_HH
#define EDIT_EDITION_HH

/*! @file IDE/EDITion/EDITion.hh
    @brief Pr�-d�clarations du module @ref EDIT.
    @author @ref Guillaume_Terrissol
    @date 3 Mai 2008 - 17 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
    class Answer;
    class Observer;
    class Request;
    class Subject;
    class SubjectList;
    class SubjectManager;

    class AnswerSender;
    class Controller;
    class MobileController;
    class Renderer;

    template<class TData, int TEnum>        class GenericAnswer;
    template<class TData, int TEnum>        class Container;
    template<class TEdited, class TCtrler>  class GenericController;
    template<class TData, int TEnum>        class EditRequest;
    template<class TData, int TEnum>        class InsertRequest;
    template<class TData, int TEnum>        class ModulateRequest;
    template<class TData, int TEnum>        class RemoveRequest;
    template<class TData, int TEnum>        class Value;
    template<class TData, int TEnum>        class Variation;
}

#endif  // De EDIT_EDITION_HH
