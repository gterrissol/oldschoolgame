/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/EDITion/Requests.inl
    @brief M�thodes inline des classes EDIT::EditRequest, EDIT::ModulateRequest, EDIT::InsertRequest &
    EDIT::RemoveRequest.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 16 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Attributes.hh"
#include "Subject.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                  Edit Request : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pValue Nouvelle valeur souhait�e pour l'attribut �dit�
        @param pUndo  VRAI si cette requ�te correspond � un <b>undo</b>, FAUX sinon
     */
    template<class TData, int TEnum>
    EditRequest<TData, TEnum>::EditRequest(TData pValue, bool pUndo)
        : mValue(pValue)
        , mUndo{pUndo}
    { }


    /*! Constructeur virtuel.
        @return Une copie conforme de l'instance
     */
    template<class TData, int TEnum>
    EditRequest<TData, TEnum>* EditRequest<TData, TEnum>::clone() const
    {
        return new EditRequest{*this};
    }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    EditRequest<TData, TEnum>::~EditRequest() { }


//------------------------------------------------------------------------------
//                          Edit Request : Identifiant
//------------------------------------------------------------------------------

    /*! @return Un identifiant unique pour le type de la requ�te.
     */
    template<class TData, int TEnum>
    inline long EditRequest<TData, TEnum>::type()
    {
        static long sRTTI = 0;
        return reinterpret_cast<long>(&sRTTI);
    }


//------------------------------------------------------------------------------
//                             Edit Request : Action
//------------------------------------------------------------------------------

    /*! Envoie la requ�te vers une liste de sujets.
        @param pTargets Cibles de la requ�te
     */
    template<class TData, int TEnum>
    void EditRequest<TData, TEnum>::actOnSubjects(const SubjectList& pTargets) const
    {
        foreach(Subject* lT, pTargets)
        {
            // La requ�te n'est envoy�e que si la cible est du type attendu.
            if (auto lSubject = dynamic_cast<Value<TData, TEnum>*>(lT))
            {
                lSubject->set(mValue);
            }
        }
    }


    /*! Tente de fusionner deux requ�tes (pour les regrouper dans le syst�me de <b>undo</b>/<b>redo</b>).
        @param pOther Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return VRAI si la fusion a r�ussi, FAUX sinon
        @note Par d�faut, les requ�tes fusionnent si elles sont de m�me type (la donn�e port�e devient
        celle de <i>pOther</i> - il faut sp�cialiser pour modifier ce comportement)
     */
    template<class TData, int TEnum>
    bool EditRequest<TData, TEnum>::mergeRequest(const Request* pOther)
    {
        if (auto lOther = dynamic_cast<const EditRequest<TData, TEnum>*>(pOther))
        {
            if (!mUndo)
            {
                mValue = lOther->mValue;    // [Re]Do : derni�re valeur d�finie / Undo : valeur initiale.
            }

            return true;
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                Modulate Request : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pValue Valeur avec laquelle "moduler" l'attribut � modifier
        @param pUndo  VRAI si cette requ�te correspond � un <b>undo</b>, FAUX sinon
     */
    template<class TData, int TEnum>
    ModulateRequest<TData, TEnum>::ModulateRequest(TData pValue, bool pUndo)
        : mValue{pValue}
        , mUndo{pUndo}
    { }


    /*! Constructeur virtuel.
        @return Une copie conforme de l'instance
     */
    template<class TData, int TEnum>
    ModulateRequest<TData, TEnum>* ModulateRequest<TData, TEnum>::clone() const
    {
        return new ModulateRequest{*this};
    }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    ModulateRequest<TData, TEnum>::~ModulateRequest() { }


//------------------------------------------------------------------------------
//                        Modulate Request : Identifiant
//------------------------------------------------------------------------------

    /*! @return Un identifiant unique pour le type de la requ�te.
     */
    template<class TData, int TEnum>
    inline long ModulateRequest<TData, TEnum>::type()
    {
        static long sRTTI = 0;
        return reinterpret_cast<long>(&sRTTI);
    }


//------------------------------------------------------------------------------
//                           Modulate Request : Action
//------------------------------------------------------------------------------

    /*! Envoie la requ�te vers une liste de sujets.
        @param pTargets Cibles de la requ�te
     */
    template<class TData, int TEnum>
    void ModulateRequest<TData, TEnum>::actOnSubjects(const SubjectList& pTargets) const
    {
        foreach(Subject* lT, pTargets)
        {
            // La requ�te n'est envoy�e que si la cible est du type attendu.
            if (auto lSubject = dynamic_cast<Variation<TData, TEnum>*>(lT))
            {
                lSubject->change(mValue);
            }
        }
    }


    /*! Tente de fusionner deux requ�tes (pour les regrouper dans le syst�me de <b>undo</b>/<b>redo</b>).
        @param pOther Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return VRAI si la fusion a r�ussi, FAUX sinon
        @note Par d�faut, les requ�tes fusionnent si elles sont de m�me type (la donn�e port�e devient la
        somme des deux - il faut sp�cialiser pour modifier ce comportement)
     */
    template<class TData, int TEnum>
    bool ModulateRequest<TData, TEnum>::mergeRequest(const Request* pOther)
    {
        if (auto lOther = dynamic_cast<const ModulateRequest<TData, TEnum>*>(pOther))
        {
            // La distinction vaut surtout pour les quaternions...
            if (mUndo)
            {
                mValue = plus(lOther->mValue, mValue);
            }
            else
            {
                mValue = plus(mValue, lOther->mValue);
            }

            return true;
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                 Insert Request  : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pElement El�ment � ins�rer
        @param pUndo    VRAI si cette requ�te correspond � un <b>undo</b>, FAUX sinon
     */
    template<class TData, int TEnum>
    InsertRequest<TData, TEnum>::InsertRequest(TData pElement, bool pUndo)
        : mElementList{}
        , mUndo{pUndo}
    {
        mElementList << pElement;
    }


    /*! Constructeur virtuel.
        @return Une copie conforme de l'instance
     */
    template<class TData, int TEnum>
    InsertRequest<TData, TEnum>* InsertRequest<TData, TEnum>::clone() const
    {
        return new InsertRequest{*this};
    }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    InsertRequest<TData, TEnum>::~InsertRequest() { }


//------------------------------------------------------------------------------
//                         Insert Request : Identifiant
//------------------------------------------------------------------------------

    /*! @return Un identifiant unique pour le type de la requ�te.
     */
    template<class TData, int TEnum>
    inline long InsertRequest<TData, TEnum>::type()
    {
        static long sRTTI = 0;
        return reinterpret_cast<long>(&sRTTI);
    }


//------------------------------------------------------------------------------
//                           Insert Request  : Action
//------------------------------------------------------------------------------

    /*! Envoie la requ�te vers une liste de sujets.
        @param pTargets Cibles de la requ�te
     */
    template<class TData, int TEnum>
    void InsertRequest<TData, TEnum>::actOnSubjects(const SubjectList& pTargets) const
    {
        foreach(Subject* lT, pTargets)
        {
            // La requ�te n'est envoy�e que si la cible est du type attendu.
            if (auto lSubject = dynamic_cast<Container<TData, TEnum>*>(lT))
            {
                foreach(TDataNoRef element, mElementList)
                {
                    lSubject->insert(element);
                }
            }
        }
    }


    /*! Tente de fusionner deux requ�tes (pour les regrouper dans le syst�me de <b>undo</b>/<b>redo</b>).
        @param pOther Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return VRAI si la fusion a r�ussi, FAUX sinon
        @note Par d�faut, les requ�tes fusionnent si elles sont de m�me type (les �l�ments ins�r�s sont
        agr�g�s - il faut sp�cialiser pour modifier ce comportement)
     */
    template<class TData, int TEnum>
    bool InsertRequest<TData, TEnum>::mergeRequest(const Request* pOther)
    {
        if (auto lOther = dynamic_cast<const InsertRequest<TData, TEnum>*>(pOther))
        {
            if (mUndo)
            {
                mElementList = lOther->mElementList + mElementList;
            }
            else
            {
                mElementList += lOther->mElementList;
            }

            return true;
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                 Remove Request  : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pElement El�ment � retirer
        @param pUndo    VRAI si cette requ�te correspond � un <b>undo</b>, FAUX sinon
     */
    template<class TData, int TEnum>
    RemoveRequest<TData, TEnum>::RemoveRequest(TData pElement, bool pUndo)
        : mElementList{}
        , mUndo{pUndo}
    {
        mElementList << pElement;
    }


    /*! Constructeur virtuel.
        @return Une copie conforme de l'instance
     */
    template<class TData, int TEnum>
    RemoveRequest<TData, TEnum>* RemoveRequest<TData, TEnum>::clone() const
    {
        return new RemoveRequest{*this};
    }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    RemoveRequest<TData, TEnum>::~RemoveRequest() { }


//------------------------------------------------------------------------------
//                         Remove Request : Identifiant
//------------------------------------------------------------------------------

    /*! @return Un identifiant unique pour le type de la requ�te.
     */
    template<class TData, int TEnum>
    inline long RemoveRequest<TData, TEnum>::type()
    {
        static long sRTTI = 0;
        return reinterpret_cast<long>(&sRTTI);
    }


//------------------------------------------------------------------------------
//                           Remove Request  : Action
//------------------------------------------------------------------------------

    /*! Envoie la requ�te vers une liste de sujets.
        @param pTargets Cibles de la requ�te
     */
    template<class TData, int TEnum>
    void RemoveRequest<TData, TEnum>::actOnSubjects(const SubjectList& pTargets) const
    {
        foreach(Subject* lT, pTargets)
        {
            // La requ�te n'est envoy�e que si la cible est du type attendu.
            if (auto lSubject = dynamic_cast<Container<TData, TEnum>*>(lT))
            {
                foreach(TDataNoRef element, mElementList)
                {
                    lSubject->remove(element);
                }
            }
        }
    }


    /*! Tente de fusionner deux requ�tes (pour les regrouper dans le syst�me de <b>undo</b>/<b>redo</b>).
        @param pOther Requ�te qu'il faut tenter de fusionner avec *<b>this</b>
        @return Vrai si la fusion a r�ussi, Faux sinon
        @note Par d�faut, les requ�tes fusionnent si elles sont de m�me type (les �l�ments retir�s sont
        agr�g�s - il faut sp�cialiser pour modifier ce comportement)
     */
    template<class TData, int TEnum>
    bool RemoveRequest<TData, TEnum>::mergeRequest(const Request* pOther)
    {
        if (auto lOther = dynamic_cast<const RemoveRequest<TData, TEnum>*>(pOther))
        {
            if (mUndo)
            {
                mElementList = lOther->mElementList + mElementList;
            }
            else
            {
                mElementList += lOther->mElementList;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
