/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_OBSERVER_HH
#define QT_OBSERVER_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Observer.hh
    @brief En-t�te de la classe EDIT::Observer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 20 Septembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
    /*! @brief Observateur.
        @version 0.5
     */
    class Observer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    Observer(Subject* target = nullptr);    //!< Constructeur.
virtual             ~Observer();                            //!< Destructeur.
        //@}
        //! @name Ecoute
        //@{
        void        watch(Subject* target);                 //!< Assignation d'une cible.
        Subject*    watched() const;                        //!< Cible.
        void        update(const Answer* answer = nullptr); //!< Ecoute de la cible.

    private:

virtual void        listen(const Answer* answer) = 0;       //!< Ecoute effective de la cible.

        Subject*    subject;                                //!< Cible.
        //@}
    };
}

#endif  // De QT_OBSERVER_HH
