/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_SUBJECT_HH
#define EDIT_SUBJECT_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Subject.hh
    @brief En-t�te de la classe EDIT::Subject, EDIT::SubjectList & EDIT::SubjectManager.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 20 Septembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QList>
#include <QObject>

#include "CORE/Private.hh"

namespace EDIT
{
    /*! @brief Sujet "observable".
        @version 0.7

        @warning Chaque "�dition" doit �tre suivie d'un appel � notify().<br>
                 Si possible, donnez aussi peu d'information que possible sur les modifications
     */
    class Subject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                        Subject(QString manager);       //!< Constructeur par d�faut.
virtual                 ~Subject();                     //!< Destructeur.
        //@}
        SubjectManager* manager() const;                //!< Gestionnaire.
        //! @name Ecoute
        //@{
        void            addObserver(Observer* o);       //!< Ajout d'un observateur.
        void            removeObserver(Observer* o);    //!< Retrait d'un observateur.

        bool            isWatched() const;              //!< Observ� ?

    protected:

        void            notify(const Answer*  answer,
                               const Request* undo);    //!< R�ponse � une requ�te.
        //@}
    private:

        Q_CUSTOM_DECLARE_PRIVATE(Subject)
    };


    /*! @brief Liste de sujets.
        @version 0.2
     */
    class SubjectList : public QList<Subject*>
    {
    public:
        //! @name Constructeurs
        //@{
                        SubjectList();              //!< ... par d�faut.
                        SubjectList(Subject* s);    //!< ... sur un �l�ment.
        SubjectList&    operator<<(Subject* s);     //!< ... ajout d'�l�ment.
        //@}
        SubjectManager* manager() const;                //!< Gestionnaire.
        //! @name Modification
        //@{
        void            perform(const Request* r);  //!< Demande de modification.
        //@}
    };


    /*! @brief Gestionnaire de sujets.
        @version 0.4

        Gestionnaire de sujets observables.
     */
    class SubjectManager : public QObject
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                            SubjectManager(QString name, QObject* parent);  //!< Constructeur.
virtual                     ~SubjectManager();                              //!< Destructeur.
        //@}
        //! @name Undo /redo
        //@{
        void                redo();                                         //!< Refaire.
        void                undo();                                         //!< D�faire.
        //@}
 static SubjectManager*     instance(const QString& name);                  //!< Singleton.
        //! @name Activation
        //@{
        bool                isEnabled() const;                              //!< Est actif ?

    public slots:

        void                setEnabled(bool enabled);                       //!< [D�s]Activation.
        //@}
    signals:

        void                countChanged(int undo, int redo);               //!< Variation du nombre d'undo et/ou redo.
        void                undoDone();                                     //!< Undo effectu�.
        void                redoDone();                                     //!< Redo effectu�.

    private:

        Q_CUSTOM_DECLARE_PRIVATE(SubjectManager)
        friend class SubjectHelper;                                         //!< Pour l'acc�s � SubjectManagerPrivate.
    };
}

#endif  // De EDIT_SUBJECT_HH
