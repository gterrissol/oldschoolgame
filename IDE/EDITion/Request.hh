/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_REQUEST_HH
#define EDIT_REQUEST_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Request.hh
    @brief En-t�te de la classe EDIT::Request.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 22 Novembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
    /*! @brief 
        @version 0.4
     */
    class Request
    {
    public:
        //! @name Constructeur & destructeur
        //@{
virtual Request*    clone() const = 0;                                      //!< Constructeur "virtuel" (clonage).
virtual             ~Request();                                             //!< Destructeur.
        //@}
        //! @name Action
        //@{
        void        actOn(const SubjectList& targets) const;                //!< Application de de la requ�te sur un sujet.
        bool        merge(const Request* e);                                //!< Fusion de requ�tes.
        //@}
    private:
        //! @name Comportement � d�finir
        //@{
virtual void        actOnSubjects(const SubjectList& targets) const = 0;    //!< Application de de la requ�te sur des sujets.
virtual bool        mergeRequest(const Request* e) = 0;                     //!< Fusion de requ�tes.
        //@}
    };
}

#endif  // De EDIT_REQUEST_HH
