/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/EDITion/Answer.inl
    @brief M�thodes inline de la classe EDIT::GenericAnswer.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 18 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
//------------------------------------------------------------------------------
//                                  Pseudo-RTTI
//------------------------------------------------------------------------------

    /*! Permet de conna�tre le type r�el de l'instance via un syst�me d'identifiants uniques.
        @return Une pseudo-rtti associ�e au type r�el de l'instance
        @sa instanceRTTI()
     */
    inline long Answer::rtti() const
    {
        return instanceRTTI();
    }


//------------------------------------------------------------------------------
//                          Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @param pData    Valeur � transmettre en r�ponse � une requ�te  (nouvelle valeur de l'attribut �dit�
        ou modifi�, �l�ment ins�r� ou retir�).
        @param pRequest Identifie la requ�te � l'origine de cette r�ponse.
     */
    template<class TData, int TEnum>
    GenericAnswer<TData, TEnum>::GenericAnswer(TData pData, long pRequest)
        : mData(pData)
        , mFrom{pRequest}
    { }


    /*! Constructeur virtuel.
        @return Une copie conforme de l'instance
     */
    template<class TData, int TEnum>
    GenericAnswer<TData, TEnum>* GenericAnswer<TData, TEnum>::clone() const
    {
        return new GenericAnswer{*this};
    }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    GenericAnswer<TData, TEnum>::~GenericAnswer() { }


//------------------------------------------------------------------------------
//                              El�ments de r�ponse
//------------------------------------------------------------------------------

    /*! Cette m�thode permet de conna�tre la r�ponse du sujet � une requ�te qui lui a �t� appliqu�e.
        @return Selon le type de la r�ponse :
        - la valeur de l'attribut apr�s �dition,
        - la valeur de l'attribut apr�s modulation,
        - l'�l�ment ins�r�,
        - l'�l�men retir�
     */
    template<class TData, int TEnum>
    auto GenericAnswer<TData, TEnum>::data() const -> const TDataNoRef&
    {
        return mData;
    }


    /*! @return Une valeur identifiant le type de requ�te � l'origine de cette r�ponse.
     */
    template<class TData, int TEnum>
    long GenericAnswer<TData, TEnum>::from() const
    {
        return mFrom;
    }


    /*! Cette m�thode se contente de retourner la valeur du param�tre template <i>TEnum</i>
        @return <i>TEnum</i>
     */
    template<class TData, int TEnum>
    int GenericAnswer<TData, TEnum>::type() const
    {
        return TEnum;
    }


//------------------------------------------------------------------------------
//                                     RTTI
//------------------------------------------------------------------------------

    /*! Il peut �tre utile (voire indispensable) de conna�tre la valuer de rtti d'un type de r�ponse sans
        passer par une instanciation (Compile-Time Type Information).
        @return La valeur retourn�e par instanceRTTI() si elle �tait appel�e pour le m�me type de r�ponse
        (m�mes param�tres template)
     */
    template<class TData, int TEnum>
    inline long GenericAnswer<TData, TEnum>::key()
    {
        return reinterpret_cast<long>(&smRTTI);
    }


    /*! A chaque instanciation de cette classe (pour les deux param�tres template), une nouvelle valeur
        de rtti est automatiquement g�n�r�e.<br>
        Cela permet de simplement identifier les nombreux types diff�rents de r�ponse.
        @return Une valeur enti�re distincte pour chaque type de r�ponse
     */
    template<class TData, int TEnum>
    long GenericAnswer<TData, TEnum>::instanceRTTI() const
    {
        return key();
    }


//------------------------------------------------------------------------------
//                                   Singleton
//------------------------------------------------------------------------------

    /*! Cette instance sera d�finie automatiquement lors de toute instanciation de GenericAnswer pour un
        nouveau couple de param�tres template (type, entier).<br>
        Cette instantiation automatique permet de g�n�rer des variables distinctes (d'adresses uniques)
        et donc des RTTI uniques (tout abus est dangereux pour la sant� de votre application : �
        consommer avec mod�ration).
     */
    template<class TData, int TEnum> unsigned long  GenericAnswer<TData, TEnum>::smRTTI = 0;
}
