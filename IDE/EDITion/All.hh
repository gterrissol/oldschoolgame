/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_ALL_HH
#define EDIT_ALL_HH

/*! @file IDE/EDITion/All.hh
    @brief Interface publique du module @ref EDITion.
    @author @ref Guillaume_Terrissol
    @date 27 Juin 2003 - 9 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT  //! Collection de classes pour les <b>Datum</b>s.
{
    /*! @namespace EDIT
        @version 0.9

        Cet espace de noms regroupe :
        - les objets �lementaires destin�s � manipuler les attributs des objets �dit�s,
        - les classes "�v�nements" correspondant aux requ�tes sur les datums et � leur r�ponses,
     */


    /*! @defgroup EDITion EDITion : Edition des objets du moteur
        <b>namespace</b> EDIT.
     */
}

#include "Answer.hh"
#include "Attributes.hh"
#include "Controller.hh"
#include "Requests.hh"

#endif  // De EDIT_DATUM_HH
