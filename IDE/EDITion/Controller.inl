/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/EDITion/Controller.inl
    @brief M�thodes inline des classes EDIT::Controller & EDIT::GenericController.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Assert.hh"
#include "Subject.hh"

#include "ErrMsg.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                            Controller : Interface
//------------------------------------------------------------------------------

    /*! @return Le contexte d'ex�cution.
     */
    inline OSGi::Context* Controller:: context() const
    {
        return mContext;
    }

    /*! Permet de conna�tre le type r�el de l'instance via un syst�me d'identifiants uniques.
        @return Une pseudo-rtti associ�e au type r�el de l'instance
        @sa instanceRTTI()
     */
    inline long Controller::rTTI() const
    {
        return instanceRTTI();
    }


    /*! Permet de conna�tre l'extension de fichier du type de l'objet �dit�.
        @return Une extension de fichier (voir pak/extension/ *.xml)
        @sa getExtension()
     */
    inline QString Controller::extension() const
    {
        return getExtension();
    }


    /*! Permet d'indiquer au contr�leur qu'il a � nouveau la main
        @sa doTakeOver()
     */
    inline void Controller::takeOver()
    {
        doTakeOver();
    }


    /*! Permet de demander au contr�leur de proc�der � la sauvegarde aniticip�e de l'objet contr�l� (la
        sauvegarde intervient � la destruction de l'objet).
        @sa doSave()
     */
    inline void Controller::save() const
    {
        doSave();
    }


//------------------------------------------------------------------------------
//                          Controller : Objet Contr�l�
//------------------------------------------------------------------------------

    /*!
     */
    inline bool Controller::manages(GameObjectPtr pObject) const
    {
        return doesManage(pObject);
    }


    /*!
     */
    inline bool Controller::equals(Ptr pOther) const
    {
        return isEqual(pOther);
    }


//------------------------------------------------------------------------------
//                   Controller : Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn Subject* EDIT::Controller::asSubject()
        R�cup�re le sujet d'�dition [d'un objet du moteur] g�r� par cette instance de contr�leur.
        @return Le sujet d'�dition associ�
     */


    /*! @fn long EDIT::Controller::instanceRTTI() const
        Les impl�mentations de cette m�thode doivent retourner un identifiant unique pour chaque type de
        classe d�riv�e.
        @return Une pseudo-rtti pour l'instance
     */


    /*! @fn bool EDIT::Controller::isSubjectValid() const
        @return VRAI si le sujet d'�dition est toujours valide, FAUX sinon
     */


    /*! @fn void EDIT::Controller::doTakeOver()
        Les impl�mentations de cette m�thode permettent d'effectuer toute mise � jour n�cessaire lorsque
        le contr�leur reprend la main.
     */


//------------------------------------------------------------------------------
//                Generic Controller : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pEdited  Pointeur "brut" vers l'objet � "contr�ler"
        @param pContext Contexte d'ex�cution
        @note L'int�r�t de ce constructeur est qu'il permet de cr�er des contr�leurs sur des objets non-g�r�s par smart pointer.
     */
    template<class TEdited, class TCtrler>
    GenericController<TEdited, TCtrler>::GenericController(TEdited* pEdited, OSGi::Context* pContext)
        : TCtrler(pContext)
        , mProxy{pEdited, [](TEdited*){}}   // Deleter nul.
        , mEdited{mProxy}
    {
        ASSERT(!mEdited.expired(), kInvalidObject)
    }


    /*! Constructeur.
        @param pEdited  Pointeur vers l'objet du moteur � "contr�ler"
        @param pContext Contexte d'ex�cution
     */
    template<class TEdited, class TCtrler>
    GenericController<TEdited, TCtrler>::GenericController(EditedPtr pEdited, OSGi::Context* pContext)
        : TCtrler(pContext)
        , mProxy{}
        , mEdited{pEdited}
    {
        ASSERT(!mEdited.expired(), kInvalidObject)
    }


    /*! Destructeur.
     */
    template<class TEdited, class TCtrler>
    GenericController<TEdited, TCtrler>::~GenericController() { }


//------------------------------------------------------------------------------
//                       Generic Controller : Informations
//------------------------------------------------------------------------------

    /*! @return Une "pseudo-rtti" pour ce type de contr�leur
     */
    template<class TEdited, class TCtrler>
    long GenericController<TEdited, TCtrler>::type()
    {
        return reinterpret_cast<long>(&smRTTI);
    }


    /*! @return Un identifiant destin� au syst�me de picking (en fait, un pointeur sur le smart pointer
        portant l'objet moteur contr�l�)
     */
    template<class TEdited, class TCtrler>
    const void* GenericController<TEdited, TCtrler>::identifier() const
    {
        return &mEdited;
    }


    /*! @param pObject Objet dont on cherche � savoir s'il est contr�l� par cette instance
        @return VRAI si <i>pObject</i> est contr�l� par <b>this</b>, FAUX sinon
     */
    template<class TEdited, class TCtrler>
    bool GenericController<TEdited, TCtrler>::manages(EditedPtr pObject)
    {
        return (pObject == edited());
    }


//------------------------------------------------------------------------------
//                       Generic Controller : Objet Edit�
//------------------------------------------------------------------------------

    /*! @return Un smart pointer sur l'objet �dit�
     */
    template<class TEdited, class TCtrler>
    auto GenericController<TEdited, TCtrler>::edited() const -> EditedPtr
    {
        return mEdited;
    }


//------------------------------------------------------------------------------
//                       Generic Controller : Informations
//------------------------------------------------------------------------------

    /*! @return Une "pseudo-rtti" pour l'instance
     */
    template<class TEdited, class TCtrler>
    long GenericController<TEdited, TCtrler>::instanceRTTI() const
    {
        return type();
    }


    /*! @return VRAI si le sujet d'�dition est toujours valide, FAUX sinon
     */
    template<class TEdited, class TCtrler>
    bool GenericController<TEdited, TCtrler>::isSubjectValid() const
    {
        return !mEdited.expired();
    }


//------------------------------------------------------------------------------
//                      Generic Controller : Objet Contr�l�
//------------------------------------------------------------------------------

    /*!
     */
    template<class TEdited, class TCtrler>
    bool GenericController<TEdited, TCtrler>::doesManage(const GameObjectPtr pObject) const
    {
        return this->compare(pObject, edited().lock().get());
    }


    /*!
     */
    template<class TEdited, class TCtrler>
    bool GenericController<TEdited, TCtrler>::isEqual(Controller::Ptr pOther) const
    {
        using TPtr = GenericController<TEdited, TCtrler>;

        if (auto lOther = std::dynamic_pointer_cast<TPtr>(pOther))
        {
            return (lOther->edited().lock() == edited().lock());
        }
        else
        {
            return false;
        }
    }


//------------------------------------------------------------------------------
//                                   Singleton
//------------------------------------------------------------------------------

    /*! Cette instance sera d�finie automatiquement lors de toute instanciation de GenericController pour
        un nouveau param�tre template.<br>
        Cette instantiation automatique permet de g�n�rer des variables distinctes (d'adresses uniques)
        et donc des RTTI uniques (tout abus est dangereux pour la sant� de votre application : �
        consommer avec mod�ration).
     */
    template<class TEdited, class TCtrler>
    unsigned long   GenericController<TEdited, TCtrler>::smRTTI    = 0;
}
