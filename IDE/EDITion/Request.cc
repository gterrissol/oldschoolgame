/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Request.hh"

/*! @file IDE/EDITion/Request.cc
    @brief M�thodes (non-inline) de la classe EDIT::Request.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 22 Novembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Subject.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Request::~Request() { }


//------------------------------------------------------------------------------
//                                    Action                                    
//------------------------------------------------------------------------------

    /*! Ex�cute la requ�te sur une liste de sujets.
        @param targets Cibles de la requ�te
        @sa actOnSubject()
     */
    void Request::actOn(const SubjectList& targets) const
    {
        if (targets.manager()->isEnabled())
        {
            actOnSubjects(targets);
        }
    }


    /*! Lorsque c'est possible, fusionne les requ�te (i.e. produit une requ�te �quivalent � l'ex�cution
        successive de cette instance et de <i>e</i>).
        @param e Requ�te � fusionner � l'instance
        @return VRAI si la fusion a r�ussi : l'instance porte alors les informations provenant de
        <i>e</i>, FAUX en cas d'�chec
        @sa mergeRequest()
     */
    bool Request::merge(const Request* e)
    {
        // Pas de test sur l'activation du gestionnaire : la fusion n'intervient que lorsque le m�canisme est d�j� actif (Cf actOn() ci-dessus).
        return mergeRequest(e);
    }


//------------------------------------------------------------------------------
//                                 Documentation
//------------------------------------------------------------------------------

    /*! @fn Request* Request::clone() const
        Les impl�mentations de cette m�thode doivent retourner une copie compl�te de l'instance (appeler
        un constructeur par copie d�fini devrait suffire).
        @return Un clone de l'instance
        @note Bien que virtuelle, cette m�thode est d�clar� publique afin de profiter de la covariance
     */


    /*! @fn void Request::actOnSubjects(const SubjectList& targets) const
        Impl�mentez cette m�thode pour envoyer une requ�te (cette instance) vers les sujets (le
        param�tre).
        @param targets Sujets sur lequel envoyer une requ�te (port�e par <b>this</b>)
        @warning targets ne doit pas �tre vide
     */


    /*! @fn Request* Request::mergeRequest(const Request* e)
        Cette m�thode fusionne l'instance avec son param�tre (lorsque c'est possible) pour qu'elle
        corresponde � l'action cons�cutive des deux.
        @param e Requ�te avec laquelle fusionner l'instance
        @return Vrai si la fusion a r�ussi, Faux sinon
     */
}
