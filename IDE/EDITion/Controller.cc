/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Controller.hh"

/*! @file IDE/EDITion/Controller.cc
    @brief M�thodes (non-inline) de la classe EDIT::Controller.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <map>
#include <string>

#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "ReSourCe/Object.hh"
#include "STL/Shared.tcc"

namespace EDIT
{
//------------------------------------------------------------------------------
//                            Controller : Interface
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Controller::~Controller() { }


    /*! R�cup�re le sujet g�r� par un contr�leur donn�.
        @param pController Contr�leur dont r�cup�r� le sujet
        @note <i>pController</i> sera mis � nul si le sujet d'�dition n'est plus valide (i.e. l'objet du
        moteur n'existe plus)
     */
    Subject* Controller::subject(Ptr& pController)
    {
        // V�rification du contr�leur.
        if (pController)
        {
            if (pController->isSubjectValid())
            {
                return pController->asSubject();
            }
            else
            {
                pController = nullptr;
            }
        }

        return nullptr;
    }


    /*! R�cup�re le sujet g�r� par un contr�leur donn�.
        @param pController Contr�leur dont r�cup�r� le sujet
        @note Contrairement � la m�thode surcharg� prenant un Ptr en param�tre, <i>pController</i> ne sera pas supprim� si le sujet d'�dition n'est plus valide
     */
    Subject* Controller::subject(Controller* pController)
    {
        // V�rification du contr�leur.
        if (pController && pController->isSubjectValid())
        {
            return pController->asSubject();
        }

        return {};
    }


//------------------------------------------------------------------------------
//                          Controller : Objet Contr�l�
//------------------------------------------------------------------------------

    /*!
     */
    bool Controller::compare(GameObjectPtr pObject, GameObjectPtr pEdited) const
    {
        return (pEdited != nullptr) && (pObject == pEdited);
    }


    /*!
     */
    bool Controller::compare(GameObjectPtr, const void*) const
    {
        return false;
    }

//------------------------------------------------------------------------------
//                           Controller : Constructeur
//------------------------------------------------------------------------------

    /*! @param pContext Contexte d'ex�cution
     */
    Controller::Controller(OSGi::Context* pContext)
        : mContext{pContext}
    { }


//------------------------------------------------------------------------------
//                          Controller : Impl�mentation
//------------------------------------------------------------------------------

    /*! @note Les objets du moteur ne sont pas les seuls � �tre contr�l�s
     */
    QString Controller::getExtension() const
    {
        return {};
    }


    /*!
     */
    void Controller::doTakeOver()
    {
        // Par d�faut, ne fait rien.
    }


    /*!
     */
    void Controller::doSave() const
    {
        // Par d�faut, ne fait rien.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pList       Liste de sujets � enrichir
        @param pController Contr�leur dont le sujet est � placet dans @p pList
        @return @p pList
     */
    SubjectList operator<<(SubjectList&& pList, Controller::Ptr& pController)
    {
        pList.append(Controller::subject(pController));
        return pList;
    }


    /*! @param pList       Liste de sujets � enrichir
        @param pController Contr�leur dont le sujet est � placet dans @p pList
        @return @p pList
     */
    SubjectList operator<<(SubjectList&& pList, Controller* pController)
    {
        pList.append(Controller::subject(pController));
        return pList;
    }
}
