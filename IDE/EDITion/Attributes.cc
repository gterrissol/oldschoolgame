/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Attributes.hh"

/*! @file IDE/EDITion/Attributes.cc
    @brief M�thodes (non-inline) de la classe EDIT::AnswerSender.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    AnswerSender::~AnswerSender() { }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn void AnswerSender::sendAnswer(Answer* pAnswer, Request* pUndo)
        Cette m�thode doit �tre impl�ment�e pour envoyer une r�ponse suite � une requ�te (pour en
        conna�tre les effets).
        @param pAnswer R�ponse proprement dite; elle indique quels ont �t� les effets de la requ�te
        @param pUndo   Requ�te � �mettre pour annuler les modifications engendr�es par la requ�te initiale
        @note AnswerSender �tant destin�e � �tre d�riv�e de mani�re priv�e, je n'ai pas utilis� l'idiome
        <i>template method</i>
     */

    /*! @def INITIALIZE_VALUE(setter, getter,...)
        @param setter Nom (sans "fioriture") de la m�thode d'"�criture" de l'attribut
        @param getter Nom (sans "fioriture") de la m�thode de "lecture" de l'attribut
        @note Une valeur peut �tre pass�e en dernier argument, afin de permettre une surcharge (m�me op�ration, attribut diff�rent)
        @sa @ref EDIT_Attributes_Page
     */

    /*! @def INITIALIZE_VARIATION(changer, getter,...)
        @param changer Nom (sans "fioriture") de la m�thode de "modulation" de l'attribut
        @param getter  Nom (sans "fioriture") de la m�thode de "lecture" de l'attribut
        @note Une valeur peut �tre pass�e en dernier argument, afin de permettre une surcharge (m�me op�ration, attribut diff�rent)
        @sa @ref EDIT_Attributes_Page
     */

    /*! @def INITIALIZE_CONTAINER(inserter, remover,...)
        @param inserter Nom (sans "fioriture") de la m�thode d'insertion d'�l�ment
        @param remover  Nom (sans "fioriture") de la m�thode de retrait d'�l�ment
        @note Une valeur peut �tre pass�e en dernier argument, afin de permettre une surcharge (m�me op�ration, attribut diff�rent)
        @sa @ref EDIT_Attributes_Page
     */
}
