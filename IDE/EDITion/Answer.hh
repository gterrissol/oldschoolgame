/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_ANSWER_HH
#define EDIT_ANSWER_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Answer.hh
    @brief En-t�te des classes EDIT::Answer & EDIT::GenericAnswer.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 18 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <type_traits>

namespace EDIT
{
    /*! @brief Modification g�n�rique.
        @version 1.0
     */
    class Answer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
virtual         Answer* clone() const = 0;          //!< Constructeur "virtuel" (clonage).
virtual                 ~Answer();                  //!< Destructeur.
        //@}
        inline  long    rtti() const;               //!< RTTI personnalis�e.

    private:
        //! @name Comportement � d�finir
        //@{
virtual         long    instanceRTTI() const = 0;   //!< RTTI personnalis�e.
        //@}
    };


//------------------------------------------------------------------------------
//                                    R�ponse
//------------------------------------------------------------------------------

    /*! @brief R�ponse � l'�dition d'un attribut.
        @version 1.1

        Pour tout type de requ�te �mise, il n'existe qu'un seul type de r�ponse, impl�ment� par cette
        classe. Si, pour le type <i>TData</i>, il existe plusieurs attributs distincts, utilisez le
        param�tre template <i>TEnum</i> pour les diff�rencier (la valeur par d�faut permettant de
        simplifier les �critures le cas �ch�ant).
     */
    template<class TData, int TEnum = -1>
    class GenericAnswer : public Answer
    {
    public:
        //! @name Type
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                  //!< Donn�e par valeur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                                    GenericAnswer(TData pData,
                                                  long  pRequest);  //!< Constructeur.
virtual         GenericAnswer*      clone() const override;         //!< Constructeur virtuel.
virtual                             ~GenericAnswer();               //!< Destructeur.
        //@}
        //! @name El�ments de r�ponse
        //@{
                const TDataNoRef&   data() const;                   //!< Valeur en r�ponse.
                long                from() const;                   //!< Type de requ�te � l'origine de la r�ponse.
                int                 type() const;                   //!< Discriminant.
        //@}
        //! @name RTTI
        //@{
 static inline  long                key();                          //!< Pseudo-rtti de la classe.

    private:

virtual         long                instanceRTTI() const override;  //!< Pseudo-rtti de la classe.
        //@}
 static unsigned long   smRTTI;                                     //!< "rtti".
        TDataNoRef      mData;                                      //!< "Valeur" de la r�ponse.
        long            mFrom;                                      //!< Type de requ�te � l'origine de la r�ponse.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Answer.inl"

#endif  // De EDIT_ANSWER_HH
