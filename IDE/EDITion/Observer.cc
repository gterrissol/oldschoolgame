/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Observer.hh"

/*! @file IDE/EDITion/Observer.cc
    @brief M�thodes (non-inline) de la classe EDIT::Observer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 17 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Subject.hh"

namespace EDIT
{
    /*! Constructeur.
        @param target Sujet � observer
     */
    Observer::Observer(Subject* target)
        : subject{}
    {
        watch(target);
    }


    /*! Destructeur.
     */
    Observer::~Observer()
    {
        watch(nullptr);
    }


    /*! Assigne une cible � l'observateur, qui devra traiter les r�ponses de cette cible aux
        modifications dont elle fera l'objet.
        @param target Cible � surveiller
        @note Si <i>target</i> est nul, l'observateur surveillera tout
     */
    void Observer::watch(Subject* target)
    {
        if (subject == target)
        {
            return;
        }

        if (subject != nullptr)
        {
            subject->removeObserver(this);
        }
        if (target != nullptr)
        {
            target->addObserver(this);
        }
        subject = target;
    }


    /*! Permet de conna�tre le sujet observ� par cette instance
        @return Le sujet "surveill�"
     */
    Subject* Observer::watched() const
    {
        return subject;
    }


    /*! Traite un �v�nement lanc� par un sujet, en r�ponse � une demande de modification.<br>
        L'"�coute" proprement dite sera r�alis�e par une impl�mentation de la m�thode listen(const Answer*).
        @param answer R�ponse � traiter
     */
    void Observer::update(const Answer* answer)
    {
        listen(answer);
    }


    /*! @fn void Observer::listen(const Answer* answer)
        Toute r�impl�mentation doit mettre  � jour l'observateur pour tenir compte de la r�ponse �mise
        par un sujet apr�s une modification.
        @param answer R�ponse de <b>watched()</b> � une requ�te de modification
    }*/
}
