/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_ATTRIBUTES_HH
#define EDIT_ATTRIBUTES_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Attributes.hh
    @brief En-t�te des classes EDIT::AnswerSender, EDIT::Value, EDIT::Variation & EDIT::Container.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 20 Septembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Function.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                                    R�ponse
//------------------------------------------------------------------------------

    /*! @brief Interface de "r�pondeur"
        @version 1.0

        Cette classe propose une interface pour toute classe capable d'�mettre une r�ponse � une requ�te.
        @sa Answer
     */
    class AnswerSender
    {
    public:
virtual         ~AnswerSender();                                    //!< Destructeur.
virtual void    sendAnswer(Answer* pAnswer, Request* pUndo) = 0;    //!< Envoi d'une r�ponse.
    };


//------------------------------------------------------------------------------
//                      Modificateurs de Donn�es : Edition
//------------------------------------------------------------------------------

    /*! @brief Edition d'un attribut.
        @version 1.0

        L'�dition g�n�rique par l'IDE d'un attribut d'un objet du moteur est r�alis� en d�rivant de cette
        classe le "sujet d'�dition" de l'objet concern�.
        @note L'initialisation de la classe sujet est facilit�e par la macro INTIALIZE_VALUE
        @note Le param�tre template <i>TData</i> peut sp�cifier un type, une r�f�rence ou un pointeur
        (const ou non)
        @note Afin de permettre l'�dition d'attributs de m�me type, le param�tre template <i>TEnum</i>
        fait office de discriminant (via le recours, typiquement, aux valeurs d'une �num�ration). La
        valeur par d�faut permet de simplifier les �critures en cas d'attribut unique pour un type donn�
     */
    template<class TData, int TEnum = -1>
    class Value : private AnswerSender
    {
    public:
        //! @name Types de foncteurs
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;              //!< Donn�e par valeur.

        using Setter     = std::function<void (TData)>;         //!< Editeur.
        using Getter     = std::function<TData ()>;             //!< Accesseur par retour.
        using GetterVoid = std::function<void (TDataNoRef&)>;   //!< Accesseur par param�tre.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                        Value(Setter pSet, Getter pGet);        //!< Constructeur.
                        Value(Setter pSet, GetterVoid pGet);    //!< Constructeur.
virtual                 ~Value();                               //!< Destructeur.
        //@}
        //! @name Op�rations
        //@{
                void    set(TData pValue);                      //!< Edition d'un attribut.
                TData   get() const;                            //!< R�cup�ration d'un attribut.
                void    get(TDataNoRef& pValue) const;          //!< R�cup�ration d'un attribut.

    private:

        inline  bool    isGetVoid() const;                      //!< Accesseur par param�tre ?
        //@}
        //! @name Foncteurs
        //@{
        Setter      mSet;                                       //!< Ecriture.
        Getter      mGet;                                       //!< Lecture par valeur de retour.
        GetterVoid  mVoidGet;                                   //!< Lecture par r�f�rence.
        //@}
    };


//------------------------------------------------------------------------------
//                     Modificateurs de Donn�es : Modulation
//------------------------------------------------------------------------------

    /*! @brief Modulation d'un attribut.
        @version 0.2

        La modulation (modification par variations successives, plut�t qu'�dition compl�te) par l'IDE
        d'un attribut d'un objet du moteur est r�alis� en d�rivant de cette classe le "sujet d'�dition"
        de l'objet concern�.
        @note L'initialisation de la classe sujet est facilit�e par la macro INTIALIZE_VARIATION
        @note Le param�tre template <i>TData</i> peut sp�cifier un type, une r�f�rence ou un pointeur
        (const ou non)
        @note Afin de permettre la modulation d'attributs de m�me type, le param�tre template
        <i>TEnum</i> fait office de discriminant (via le recours, typiquement, aux valeurs d'une
        �num�ration). La valeur par d�faut permet de simplifier les �critures en cas d'attribut unique
        pour un type donn�
     */
    template<class TData, int TEnum = -1>
    class Variation : private AnswerSender
    {
    public:
        //! @name Types de foncteurs
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                  //!< Donn�e par valeur.

        using Changer = std::function<void (TData)>;                //!< Modificateur.
        using Getter  = std::function<TData ()>;                    //!< Accesseur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        Variation(Changer pChange, Getter pGet);    //!< Constructeur.
virtual                 ~Variation();                               //!< Destructeur.
        //@}
        //! @name Op�rations
        //@{
                void    change(TData pValue);                       //!< Modification d'un attribut.
                TData   get() const;                                //!< R�cup�ration d'un attribut.
        //@}
    private:
        //! @name Foncteurs
        //@{
        Changer mChange;                                            //!< Modification.
        Getter  mGet;                                               //!< Lecture par valeur de retour.
        //@}
    };


    //! @name Op�rateurs
    //@{
    template<class TData>
    inline  TData   minus(TData pL, TData pR);  //!< Diff�rence de deux donn�es.
    template<class TData>
    inline  TData   plus(TData pL, TData pR);   //!< Addition de deux donn�es.
    //@}


//------------------------------------------------------------------------------
//                Modificateurs de Donn�es : Insertion & Retrait
//------------------------------------------------------------------------------

    /*! @brief Insertion et retrait d'�l�m�nts.
        @version 1.0

        L'insertion et le retrait g�n�riques d'�l�ments par l'IDE dans et d'un objet du moteur est
        r�alis� en d�rivant de cette classe le "sujet d'�dition" de l'objet concern�.
        @note L'initialisation de la classe sujet est facilit�e par la macro INITIALIZE_CONTAINER
        @note Le param�tre template <i>TData</i> peut sp�cifier un type, une r�f�rence ou un pointeur
        (const ou non)
        @note Afin de permettre l'insertion et le retrait d'�l�ments de m�me type, le param�tre template
        <i>TEnum</i> fait office de discriminant (via le recours, typiquement, aux valeurs d'une
        �num�ration). La valeur par d�faut permet de simplifier les �critures en cas d'attribut unique
        pour un type donn�
     */
    template<class TData, int TEnum = -1>
    class Container : private AnswerSender
    {
    public:
        //! @name Types de foncteurs
        //@{
        using Inserter  = std::function<bool (TData)>;          //!< Insertion
        using Remover   = std::function<bool (TData)>;          //!< Retrait.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Container(Inserter pInsert, Remover pRemove);   //!< Constructeur.
virtual         ~Container();                                   //!< Destructeur.
        //@}
        //! @name Op�rations
        //@{
        void    insert(TData pElement);                         //!< Insertion d'un �l�ment.
        void    remove(TData pElement);                         //!< Retrait d'un �l�ment.
        //@}
    private:
        //! @name Foncteurs
        //@{
        Inserter    mInsert;                                    //!< Insertion.
        Remover     mRemove;                                    //!< Retrait.
        //@}
    };


//------------------------------------------------------------------------------
//                       Modificateurs de Donn�es : Macros
//------------------------------------------------------------------------------

#define INITIALIZE_VALUE(setter, getter,...)                                                            \
    EDIT::Value<DEFAULT_IF_1ST_OPTION_IS_DIGIT_OR_EMPTY(EDIT::ArgumentOf<decltype(&std::remove_reference<decltype(*pParent)>::type::setter)>::Type,##__VA_ARGS__),EDIT::overloader<__VA_ARGS__>()>          \
    {                                                                                                                                                                                                       \
        EDIT::bindSetter<EDIT::overloader<__VA_ARGS__>()>(pParent,SELECT_ACCESSOR(std::remove_reference<decltype(*pParent)>::type,##__VA_ARGS__)(&std::remove_reference<decltype(*pParent)>::type::setter)),\
        EDIT::bindGetter<EDIT::overloader<__VA_ARGS__>()>(pParent,SELECT_ACCESSOR(std::remove_reference<decltype(*pParent)>::type,##__VA_ARGS__)(&std::remove_reference<decltype(*pParent)>::type::getter)) \
    }                                                                                                                                                                                                       \

#define INITIALIZE_VARIATION(changer, getter,...)                                                                                               \
    EDIT::Variation<EDIT::ArgumentOf<decltype(&std::remove_reference<decltype(*pParent)>::type::changer)>::Type, EDIT::overloader(__VA_ARGS__)> \
    {                                                                                                                                           \
        EDIT::bindChanger<EDIT::overloader<__VA_ARGS__>()>(pParent, &std::remove_reference<decltype(*pParent)>::type::changer),                 \
        EDIT::bindGetter< EDIT::overloader<__VA_ARGS__>()>(pParent, &std::remove_reference<decltype(*pParent)>::type::getter)                   \
    }                                                                                                                                           \

#define INITIALIZE_CONTAINER(inserter, remover,...)                                                                                                 \
    EDIT::Container<EDIT::ArgumentOf<decltype(&std::remove_reference<decltype(*pParent)>::type::inserter)>::Type, EDIT::overloader<__VA_ARGS__>()>  \
    {                                                                                                                                               \
        EDIT::bindInserter<EDIT::overloader<__VA_ARGS__>()>(pParent, &std::remove_reference<decltype(*pParent)>::type::inserter),                   \
        EDIT::bindRemover< EDIT::overloader<__VA_ARGS__>()>(pParent, &std::remove_reference<decltype(*pParent)>::type::remover)                     \
    }                                                                                                                                               \

}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Attributes.inl"

#endif  // De EDIT_ATTRIBUTES_HH
