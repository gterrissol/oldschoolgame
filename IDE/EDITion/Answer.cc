/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Answer.hh"

/*! @file IDE/EDITion/Answer.cc
    @brief M�thodes (non-inline) de la classe EDIT::Answer.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 30 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT
{
//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Answer::~Answer() { }


//------------------------------------------------------------------------------
//                                 Documentation
//------------------------------------------------------------------------------

    /*! @fn Answer* Answer::clone() const
        Les impl�mentations de cette m�thode doivent retourner une copie compl�te de l'instance (appeler
        un constructeur par copie d�fini devrait suffire).
        @return Un clone de l'instance
        @note Bien que virtuelle, cette m�thode est d�clar� publique afin de profiter de la covariance
     */


    /*! @fn long Answer::instanceRTTI() const
        Les impl�mentations de cette m�thode doivent retourner un identifiant unique pour chaque type de
        classe d�riv�e.
        @return Une pseudo-rtti pour l'instance
     */
}
