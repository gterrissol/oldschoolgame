/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_CONTROLLER_HH
#define EDIT_CONTROLLER_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Controller.hh
    @brief En-t�te des classes EDIT::Controller & EDIT::GenericController.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Context.hh>

#include "OBJect/OBJect.hh"
#include "ReSourCe/ReSourCe.hh"
#include "STL/SharedPtr.hh"

class QString;

namespace EDIT
{
//------------------------------------------------------------------------------
//                                  Contr�leurs
//------------------------------------------------------------------------------

    /*! @brief Contr�leur d'objet du moteur.
        @version 0.7
     */
    class Controller
    {
    public:
        //! @name Types de pointeur
        //@{
        using Ptr           = std::shared_ptr<Controller>;                      //!< Pointeur sur contr�leur.
        using GameObjectPtr = const OBJ::GameObject*;                           //!< Pointeur sur Game Object.
        //@}
        //! @name Interface
        //@{
virtual                         ~Controller();                                  //!< Destructeur.
        inline  long            rTTI() const;                                   //!< RTTI personnalis�e.
        inline  QString         extension() const;                              //!< Extension associ�e � l'objet contr�l�.
        inline  void            takeOver();                                     //!< Reprise en main du contr�leur
        inline  void            save() const;                                   //!< Sauvegarde de l'�tat de l'objet contr�l�.
 static         Subject*        subject(Ptr& pController);                      //!< R�cup�ration du sujet d'�dition d'un contr�leur.
 static         Subject*        subject(Controller* pController);               //!< R�cup�ration du sujet d'�dition d'un contr�leur.
        //! @name Objet contr�l�
        //@{
        inline  bool            equals(Ptr pOther) const;                       //!< 
        inline  bool            manages(GameObjectPtr pObject) const;           //!< 

    protected:

virtual         bool            doesManage(GameObjectPtr pObject) const = 0;    //!< 
virtual         bool            isEqual(Ptr pOther) const = 0;                  //!< 
                bool            compare(GameObjectPtr pObject,
                                        GameObjectPtr pEdited) const;           //!< 
                bool            compare(GameObjectPtr pObject,
                                        const void*   pEdited) const;           //!< 
        //@}
                                Controller(OSGi::Context* pContext);            //!< Constructeur.
                OSGi::Context*  context() const;                                //!< Contexte d'ex�cution.

    private:
        //! @name Comportement � d�finir
        //@{
virtual         Subject*        asSubject() = 0;                                //!< R�cup�ration du sujet d'�dition.
virtual         long            instanceRTTI() const = 0;                       //!< Pseudo-rtti de la classe.
virtual         QString         getExtension() const;                           //!< Extension associ�e � l'objet contr�l�.
virtual         void            doTakeOver();                                   //!< Reprise en main du contr�leur.
virtual         void            doSave() const;                                 //!< Sauvegarde de l'�tat de l'objet contr�l�.
virtual         bool            isSubjectValid() const = 0;                     //!< Validit� du sujet contr�l�.
        //@}
        OSGi::Context*  mContext;                                               //!< Contexte d'ex�cution.
    };


    /*! @brief Contr�leur g�n�rique.
        @version 0.9
     */
    template<class TEdited, class TCtrler = Controller>
    class GenericController : public TCtrler
    {
    public:
        //! @name Types de pointeur
        //@{
        using Ptr           = typename TCtrler::Ptr;                                //!< Pointeur sur contr�leur.
        using EditedPtr     = std::weak_ptr<TEdited>;                               //!< Pointeur sur objet �dit�.
        using GameObjectPtr = typename TCtrler::GameObjectPtr;                      //!< Pointeur sur Game Object.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    GenericController(TEdited* pEdited, OSGi::Context* pContext);   //!< Constructeur.
                    GenericController(EditedPtr pEdited, OSGi::Context* pContext);  //!< Constructeur.
virtual             ~GenericController();                                           //!< Destructeur.
        //@}
        //! @name Informations
        //@{
 static long        type();                                                         //!< Discriminant.
        const void* identifier() const;                                             //!< "Identifiant".
        using TCtrler::manages;
        bool        manages(EditedPtr pObject);                                     //!< Objet �dit� ?

    protected:

        EditedPtr   edited() const;                                                 //!< Objet �dit�.
        //@}

    private:
        //! @name Informations
        //@{
        long        instanceRTTI() const override;                                  //!< RTTI personnalis�e.
        bool        isSubjectValid() const override;                                //!< Validit�.
        //@}
        //! @name Comportement � d�finir
        //@{
        bool        doesManage(const GameObjectPtr pObject) const override;         //!< 
        bool        isEqual(Controller::Ptr pOther) const override;                 //!< 
        //@}
        //! @name Attributs
        //@{
 static unsigned long   smRTTI;                                                     //!< "rtti".
        
        std::shared_ptr<TEdited>    mProxy;                                         //!< Objet �dit� (smart pointer de convenance).
        EditedPtr                   mEdited;                                        //!< Objet �dit�.
        //@}
    };


    SubjectList operator<<(SubjectList&& pList, Controller::Ptr& pController);  //!< Augmentation d'une liste de sujets.
    SubjectList operator<<(SubjectList&& pList, Controller* pController);       //!< Augmentation d'une liste de sujets.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Controller.inl"

#endif  // De EDIT_CONTROLLER_HH
