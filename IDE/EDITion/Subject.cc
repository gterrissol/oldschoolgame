/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Subject.hh"

/*! @file IDE/EDITion/Subject.cc
    @brief M�thodes (non-inline) de la classe EDIT::Subject.
    @author Vincent David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
    @todo R��crire en accord avec les r�gles de codage.
 */

#include <typeinfo>

#include <QDebug>
#include <QSet>
#include <QVector>

#include "Observer.hh"
#include "Request.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                     SubjectHelper : Assistant de Subject
//------------------------------------------------------------------------------

    /*! @brief Assistant de Subject.
        @version 0.3

        L'unique int�r�t de cette classe, est de permettre � Subject d'acc�der [indirectement] �
        quelques m�thode de SubjectManager.
     */
    class SubjectHelper
    {
    public:

                SubjectHelper(Subject* s);                  //!< Constructeur.
                SubjectHelper(const SubjectList* l);        //!< Constructeur.
        //! @name Enregistrement
        //@{
        void    checkIn();                                  //!< Inscription
        void    checkOut();                                 //!< Retrait.
        //@}
        //! @name Undo & redo
        //@{
        void    perform(const Request* request);            //!< Ex�cution d'une requ�te.
        void    registerUndo(const Request* undo) const;    //!< Enregistrement d'un undo.
        //@}

    private:

        const SubjectList*  subjects;                       //!< Liste de sujets � manipuler.
        Subject*            subject;                        //!< Sujet � manipuler.
    };


//------------------------------------------------------------------------------
//                      SubjectPrivate : P-Impl de Subject
//------------------------------------------------------------------------------

    /*! @brief P-Impl de Subject.
        @version 0.5

        Cette classe se contente de porter la liste d'observateur de son sujet.
     */
    class Subject::SubjectPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(Subject)
    public:

        SubjectPrivate(Subject* parent, QString manager);   //!< Constructeur.

        QSet<Observer*> observers;                          //!< Observateurs de ce sujet.
        SubjectManager* manager;                            //!< Gestionnaire de ce sujet.
    };


//------------------------------------------------------------------------------
//               SubjectManagerPrivate : P-Impl de SubjectManager
//------------------------------------------------------------------------------

    /*! @brief P-Impl de SubjectManager.
        @version 0.5
     */
    class SubjectManager::SubjectManagerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(SubjectManager)
    public:
        //! @name Constructeur & destructeur
        //@{
                SubjectManagerPrivate(SubjectManager* parent, QString name);    //!< Constructeur.
                ~SubjectManagerPrivate();                                       //!< Destructeur.
        //@}
        //! @name Possibilit�s
        //@{
        bool    canUndo();                                                      //!< ... d'annulation.
        bool    canRedo();                                                      //!< ... de r�tablissement.
        //@}
        //! @name Enregistrement de sujets
        //@{
        void    checkIn(Subject* s);                                            //!< Inscription.
        void    checkOut(Subject* s);                                           //!< Retrait.
        //@}
        void    perform(const SubjectList& l, const Request* r, bool first);    //!< 
        //! @name
        //@{
        void    registerRedo(const Request* request, const SubjectList& l);     //!< 
        void    registerUndo(const Request* request);                           //!< 
        void    registerDone();                                                 //!< 
        //@}
        //! @name
        //@{
        void    flushUndo();                                                    //!< 
        void    flushRedo();                                                    //!< 

        void    checkUndo();                                                    //!< 
        void    checkRedo();                                                    //!< 
        //@}
        bool    areSubjectsAlive(const SubjectList& l);                         //!< 

        /*! @brief Encapsulation d'une paire undo/redo.
            @version 0.5
         */
        struct QUndoRedo
        {
            //! @name Constructeur & destructeur
            //@{
            inline              QUndoRedo(SubjectList l,
                                          Request*    u,
                                          Request*    r);                       //!< Constructeur.
            inline              QUndoRedo(const QUndoRedo& ur);                 //!< Constructeur par copie.
            inline  QUndoRedo&  operator=(const QUndoRedo& ur);                 //!< Op�rateur d'affectation.
            inline              ~QUndoRedo();                                   //!< Destructeur.
            //@}
            void                swap(QUndoRedo& ur);                            //!< Permutation.
            //! @name Attributs
            //@{
            SubjectList sources;                                                //!< Sujet auquel sont associ�s l'undo et le redo.
            Request*    undo;                                                   //!< Undo.
            Request*    redo;                                                   //!< Redo.
            //@}
        };

        QVector<Subject*>               aliveSubjects;                          //!< Sujets actuellement actifs.
        QList<QUndoRedo>                list;                                   //!< Liste des undo/redo.
        QString                         parentName;                             //!< Nom du gestionnaire.
        int                             currentIndex;                           //!< Index sur la paire d'undo/redo actuelle.
        bool                            nesting;                                //!< Flag.
        bool                            ignore;                                 //!< Ignorer les actions ?
        bool                            enabled;                                //!< Est actif ?

 static QMap<QString,  SubjectManager*> those;                                  //!< Gestionnaires de sujets (et d'undo/redo).
    };


//------------------------------------------------------------------------------
//                         SubjectHelper : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur : acc�s pour un sujet.
     */
    SubjectHelper::SubjectHelper(Subject* s)
        : subjects{}
        , subject{s}
    {
        Q_ASSERT_X(subject != nullptr, "SubjectHelper::SubjectHelper(Subject* s)", "Subject must exists");
    }


    /*! Constructeur : acc�s pour une liste de sujets.
     */
    SubjectHelper::SubjectHelper(const SubjectList* l)
        : subjects{l}
        , subject{}
    {
        Q_ASSERT_X(subjects != nullptr, "SubjectHelper(const SubjectList& l)", "Subject list must exists");
    }


//------------------------------------------------------------------------------
//                        SubjectHelper : Enregistrement
//------------------------------------------------------------------------------

    /*!
     */
    void SubjectHelper::checkIn()
    {
        Q_ASSERT_X(subject != nullptr, "void SubjectHelper::checkIn()", "Subject must exists");
        subject->manager()->pthis->checkIn(subject);
    }


    /*!
     */
    void SubjectHelper::checkOut()
    {
        Q_ASSERT_X(subject != nullptr, "void SubjectHelper::checkOut()", "Subject must exists");
        subject->manager()->pthis->checkOut(subject);
    }


//------------------------------------------------------------------------------
//                          SubjectHelper : Undo & Redo
//------------------------------------------------------------------------------

    /*!
     */
    void SubjectHelper::perform(const Request* request)
    {
        Q_ASSERT_X(subjects != nullptr, "void SubjectHelper::perform(const Request* request)", "Subject list must exists");
        subjects->manager()->pthis->perform(*subjects, request, true);
    }


    /*!
     */
    void SubjectHelper::registerUndo(const Request* undo) const
    {
        subject->manager()->pthis->registerUndo(undo);
    }


//------------------------------------------------------------------------------
//                         SubjectPrivate : Constructeur
//------------------------------------------------------------------------------

    /*! @param parent  Sujet pour lequel porter la liste d'observateurs.
        @param manager Nom du gestionnaire du sujet.
     */
    Subject::SubjectPrivate::SubjectPrivate(Subject* parent, QString manager)
        : q{parent}
        , observers{}
        , manager{SubjectManager::instance(manager)}
    {
        Q_ASSERT_X(manager != nullptr, "Subject::SubjectPrivate::SubjectPrivate(Subject* parent, QString manager)", "Unknown manager");
    }


//------------------------------------------------------------------------------
//                                  Subject
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Subject::Subject(QString manager)
        : pthis{this, manager}
    {
        // Le sujet s'enregistre aupr�s du gestionnaire.
        SubjectHelper{this}.checkIn();
    }


    /*! Destructeur.
     */
    Subject::~Subject()
    {
        Q_D(Subject);

        // Le sujet se retire de la liste du gestionnaire.
        SubjectHelper{this}.checkOut();
        // Et se d�senregistre de tous ses observateurs.
        foreach(Observer* o, d->observers)
        {
            o->watch(nullptr);
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    SubjectManager* Subject::manager() const
    {
        Q_D(const Subject);

        return d->manager;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
    // listener may not be NULL, and may not be associated to this object yet.
     */
    void Subject::addObserver(Observer* o)
    {
        Q_D(Subject);

        Q_ASSERT_X(o != nullptr, "void Subject::addObserver(Observer* o)", "Null observer");

        if (o == nullptr)
        {
            return;
        }

        Q_ASSERT_X(!d->observers.contains(o), "void Subject::addObserver(Observer* o)", "The observer already watches the subject");

        d->observers.insert(o);
    }


    /*!
     */
    void Subject::removeObserver(Observer* o)
    {
        Q_D(Subject);

        Q_ASSERT_X(d->observers.contains(o), "void Subject::removeObserver(Observer* o)", "Unknown observer");

        d->observers.remove(o);
    }


    /*!
     */
    bool Subject::isWatched() const
    {
        Q_D(const Subject);

        return (0 < d->observers.size());
    }


    /*! Les observateurs sont inform�s d'un modification du sujet (c'est sa r�ponse � cette modification
        qui leur est envoy�e).
        @param answer R�ponse � faire suivre
        @param undo   Requ�te � ex�cuter pour annuler la modification
     */
    void Subject::notify(const Answer* answer, const Request* undo)
    {
        Q_D(Subject);

        foreach(Observer* o, d->observers)
        {
            o->update(answer);
        }

        SubjectHelper{this}.registerUndo(undo);
    }


//------------------------------------------------------------------------------
//                                 Subject List
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    SubjectList::SubjectList()
        : QList<Subject*>{}
    { }


    /*! Constructeur pour une liste � un �l�ment.
        @param s Sujet � inclure dans la liste
     */
    SubjectList::SubjectList(Subject* s)
        : QList<Subject*>{}
    {
        (*this) << s;
    }


    /*!
     */
    SubjectList& SubjectList::operator<<(Subject* s)
    {
        if (!isEmpty())
        {
            Q_ASSERT_X(front()->manager() == s->manager(), "SubjectList& SubjectList::operator<<(Subject* s)", "Managers mismatch");
        }
        append(s);

        return *this;
    }


    /*!
     */
    SubjectManager* SubjectList::manager() const
    {
        if (!isEmpty())
        {
            return front()->manager();
        }
        else
        {
            return {};
        }
    }


    /*!
     */
    void SubjectList::perform(const Request* r)
    {
        // 1 - Nettoyage : supprimer tous les sujets nuls.
        removeAll(nullptr);

        // 2 - Accomplissement (pompeux, non ?) : appelle la requ�te sur tous les sujets valides.
        SubjectHelper{this}.perform(r);
    }


//------------------------------------------------------------------------------
//        SubjectManagerPrivate::QUndoRedo : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param l
        @param u
        @param r
     */
    inline SubjectManager::SubjectManagerPrivate::QUndoRedo::QUndoRedo(SubjectList l, Request* u, Request* r)
        : sources{l}
        , undo{u}
        , redo{r}
    { }


    /*! Constructeur par copie.
     */
    inline SubjectManager::SubjectManagerPrivate::QUndoRedo::QUndoRedo(const QUndoRedo& ur)
        : sources{ur.sources}
        , undo{}
        , redo{}
    {
        if (ur.undo != nullptr)
        {
            undo = ur.undo->clone();
        }
        if (ur.redo != nullptr)
        {
            undo = ur.redo->clone();
        }
    }


    /*! Op�rateur d'affectation.
     */
    inline SubjectManager::SubjectManagerPrivate::QUndoRedo& SubjectManager::SubjectManagerPrivate::QUndoRedo::operator=(const QUndoRedo& ur)
    {
        QUndoRedo   tmp{ur};
        swap(tmp);

        return *this;
    }


    /*! Destructeur.
     */
    inline SubjectManager::SubjectManagerPrivate::QUndoRedo::~QUndoRedo()
    {
        delete undo;
        delete redo;
    }


//------------------------------------------------------------------------------
//        SubjectManagerPrivate::QUndoRedo : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Permutation.
     */
    void SubjectManager::SubjectManagerPrivate::QUndoRedo::swap(QUndoRedo& ur)
    {
        qSwap(sources,  ur.sources);
        qSwap(undo,     ur.undo);
        qSwap(redo,     ur.redo);
    }


//------------------------------------------------------------------------------
//              SubjectManagerPrivate : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param parent Gestionnaire pour lequel porter des informations et fournir du comportement
     */
    SubjectManager::SubjectManagerPrivate::SubjectManagerPrivate(SubjectManager* parent, QString name)
        : q{parent}
        , parentName(name)
        , currentIndex{}
        , nesting{false}
        , ignore{false}
        , enabled{false}
    {
        those[name] = q;
    }


    /*! Destructeur.
     */
    SubjectManager::SubjectManagerPrivate::~SubjectManagerPrivate()
    {
        list.clear();
        those.remove(parentName);
    }


//------------------------------------------------------------------------------
//     SubjectManagerPrivate : Possibilit�s d'Annulation & de R�tablissement
//------------------------------------------------------------------------------

    /*! @return Vrai s'il est possible d'annuler une action
     */
    bool SubjectManager::SubjectManagerPrivate::canUndo()
    {
        return (0 < currentIndex);
    }


    /*! @return Vrai s'il est possible "d'annuler une annulation"
     */
    bool SubjectManager::SubjectManagerPrivate::canRedo()
    {
        return (currentIndex < list.size());
    }


//------------------------------------------------------------------------------
//               SubjectManagerPrivate : Enregistrement de sujets
//------------------------------------------------------------------------------

    /*!
     */
    void SubjectManager::SubjectManagerPrivate::checkIn(Subject* s)
    {
        aliveSubjects.push_back(s);
    }


    /*!
     */
    void SubjectManager::SubjectManagerPrivate::checkOut(Subject* s)
    {
        Q_Q(SubjectManager);

        aliveSubjects.remove(aliveSubjects.indexOf(s));
        // Les actions (undo/redo) associ�s � ce sujet sont supprim�s.
        checkUndo();
        checkRedo();
qDebug() << QStringLiteral("%1 undo(s) & %2 redo(s)").arg(currentIndex).arg(list.size() - currentIndex);
        // currentIndex a pu changer.
        emit q->countChanged(currentIndex, list.size() - currentIndex);
    }


//------------------------------------------------------------------------------
//                SubjectManagerPrivate : Ex�cution d'une Requ�te
//------------------------------------------------------------------------------

    /*! @param l
        @param r
        @param first Vrai si la requ�te n'a pas encore �t� ex�cut�e, Faux s'il s'agit d'un undo ou d'un
        redo
     */
    void SubjectManager::SubjectManagerPrivate::perform(const SubjectList& l, const Request* r, bool first)
    {
        // Pas d'appel � perform() imbriqu�s !
        Q_ASSERT_X(!nesting, "void SubjectManagerPrivate::perform(const Request* request)", "A request is proceeding already");
        nesting = true;

        if (first)
        {
            registerRedo(r, l);
        }
        else
        {
            registerRedo(nullptr, l);
        }
        r->actOn(l);
        registerDone();

        // On peut � nouveau ex�cuter une requ�te.
        nesting = false;
    }


//------------------------------------------------------------------------------
//               SubjectManagerPrivate : 
//------------------------------------------------------------------------------

    /*! Register an action and its associated cancel.
     * @param request The event that occurred. If nul, this means we must ignore following undos.
       @param l
     * @note The events must provide clone() methods in order to be undo'able.
     */
    void SubjectManager::SubjectManagerPrivate::registerRedo(const Request* request, const SubjectList& l)
    {
        Q_Q(SubjectManager);

        Q_ASSERT_X(nesting, "void SubjectManagerPrivate::registerRedo(const Request* request)", "Trying to register a redo but no request is being performed");

        if ((request != nullptr) && !l.empty())
        {
            // In any case, the Redo part is reset
            flushRedo();

            // Insert the redo/undo pair.
            if ((list.empty()) || (!list.back().redo->merge(request)))
            {
                list.push_back(QUndoRedo{l, nullptr, nullptr});
                list.back().redo = request->clone();
                Q_ASSERT_X(typeid(*list.back().redo) == typeid(*request), "void SubjectManagerPrivate::registerRedo(const Request* request, SubjectList l)", "clone() method must be reimplemented");
                ++currentIndex;
                emit q->countChanged(currentIndex, list.size() - currentIndex);
            }
            else
            {
                // Ajoute le sujet � la liste de ceux en cours de traitement.
                SubjectList&   subjectList = list.back().sources;
                foreach(Subject* s, l)
                {
                    if (!subjectList.contains(s))
                    {
                        subjectList.push_back(s);
                    }
                }
            }

            Q_ASSERT_X(currentIndex == list.size(), "void SubjectManagerPrivate::registerRedo(const Request* request, SubjectList l)", "Registering an undo/redo pair at a wrong location");
        }
        else
        {
            ignore = true;
        }
    }


    /*! @param request
     */
    void SubjectManager::SubjectManagerPrivate::registerUndo(const Request* request)
    {
        Q_ASSERT_X(nesting, "void SubjectManagerPrivate::registerUndo(const Request* request)", "Trying to register an undo but no request is being performed");

        if (!ignore)
        {
            if(request != nullptr)
            {
                // Fusion de requ�te multi-sujet ?
                if (list.back().undo != nullptr)
                {
                    if (!list.back().undo->merge(request))
                    {
                        Q_ASSERT_X(false, "void SubjectManagerPrivate::registerUndo(const Request* request)", "Merge undo failed");
                    }
                }
                else
                {
                    list.back().undo = request->clone();
                }
            }
        }
    }


    /*!
     */
    void SubjectManager::SubjectManagerPrivate::registerDone()
    {
        Q_ASSERT_X(nesting, "void SubjectManagerPrivate::registerDone()", "Trying to end a redo registering but no request is being performed");

        if (ignore)
        {
            ignore = false;
        }
        else
        {
            if(list.back().undo == nullptr)
            {
                // Eliminate this Undo/Redo because if Undo is nul, we assume the Redo did nothing.
                // The easiest way to eliminate it is decrementing the Undo/Redo current index and flushing the Redos.
                Q_ASSERT_X(currentIndex == list.size(), "void SubjectManagerPrivate::registerDone()", "Trying to close a null redo from a wrong location");
                --currentIndex;
                flushRedo();
            }
        }
    }


//------------------------------------------------------------------------------
//               SubjectManagerPrivate : 
//------------------------------------------------------------------------------

    /*! Flush every action before/after the current action.
     */
    void SubjectManager::SubjectManagerPrivate::flushUndo()
    {
        while(0 < currentIndex)
        {
            // Remove the element from the list
            list.pop_front();
            --currentIndex;
        }
    }


    /*!
     */
    void SubjectManager::SubjectManagerPrivate::flushRedo()
    {
        while(currentIndex != list.size())
        {
            // Remove the element from the list
            list.pop_back();
        }
    }


    /*! Check the availability of one action before/after the current one, and flushes it along with its successors, if needed.
     */
    void SubjectManager::SubjectManagerPrivate::checkUndo()
    {
        if (canUndo() && (0 < currentIndex) && !areSubjectsAlive(list[currentIndex - 1].sources))
        {
            flushUndo();
        }
    }


    /*!
     */
    void SubjectManager::SubjectManagerPrivate::checkRedo()
    {
        if (canRedo() && (currentIndex < list.size()) && !areSubjectsAlive(list[currentIndex].sources))
        {
            flushRedo();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return VRAI si au moins un des sujets dans la liste <i>l</i> est valide, FAUX sinon
     */
    bool SubjectManager::SubjectManagerPrivate::areSubjectsAlive(const SubjectList& l)
    {
        bool    oneAlive    = false;

        foreach(Subject* s, l)
        {
            if (aliveSubjects.indexOf(s) != -1)
            {
                oneAlive = true;
            }
        }

        return oneAlive;
    }


//------------------------------------------------------------------------------
//                  SubjectManager : Constructeur & destructeur
//------------------------------------------------------------------------------

    /*!
     */
    SubjectManager::SubjectManager(QString name, QObject* parent)
        : QObject{parent}
        , pthis{this, name}
    { }


    /*!
     */
    SubjectManager::~SubjectManager() { }


//------------------------------------------------------------------------------
//                            SubjectManager : Ecoute
//------------------------------------------------------------------------------

    /*! Functions to be called by the MainEditor
     * @sa perform()
     */
    void SubjectManager::redo()
    {
        Q_D(SubjectManager);

        if (d->canRedo())
        {
            // Get the corresponding action on the corresponding object
            Request*            request = d->list[d->currentIndex].redo;
            const SubjectList&  sources = d->list[d->currentIndex].sources;

            Q_ASSERT_X(d->areSubjectsAlive(sources), "void SubjectManager::redo()", "Redo try for a no longer alive subject");

            // Perform fast-registration
            ++d->currentIndex;

            // Check immediately if actions after the current one are valid.
            d->checkRedo();

            d->perform(sources, request, false);

            emit countChanged(d->currentIndex, d->list.size() - d->currentIndex);
            emit redoDone();
        }
    }


    /*!
     */
    void SubjectManager::undo()
    {
        Q_D(SubjectManager);

        if (d->canUndo())
        {
            // Get the corresponding action on the corresponding object
            Request*            request = d->list[d->currentIndex - 1].undo;
            const SubjectList&  sources = d->list[d->currentIndex - 1].sources;

            Q_ASSERT_X(d->areSubjectsAlive(sources), "void SubjectManager::undo()", "Undo try for a no longer alive subject");

            // Perform fast-registration
            --d->currentIndex;

            // Check immediately if actions before the current one are valid.
            d->checkUndo();

            d->perform(sources, request, false);

            emit countChanged(d->currentIndex, d->list.size() - d->currentIndex);
            emit undoDone();
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    SubjectManager* SubjectManager::instance(const QString& name)
    {
        auto    lMgrIt = SubjectManagerPrivate::those.find(name);
        if (lMgrIt != SubjectManagerPrivate::those.end())
        {
            return lMgrIt.value();
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//                          SubjectManager : Activation
//------------------------------------------------------------------------------

    /*! @return L'�tat d'activation du gestionnaire
     */
    bool SubjectManager::isEnabled() const
    {
        Q_D(const SubjectManager);

        return d->enabled;
    }


    /*! @param enabled Nouvel �tat d'activation
     */
    void SubjectManager::setEnabled(bool enabled)
    {
        Q_D(SubjectManager);

        d->enabled = enabled;
    }


//------------------------------------------------------------------------------
//                     SubjectManagerPrivate : Singleton(s)
//------------------------------------------------------------------------------

    QMap<QString,  SubjectManager*> SubjectManager::SubjectManagerPrivate::those{};
}
