/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/EDITion/Attributes.inl
    @brief M�thodes inline des classes EDIT::Value, EDIT::Variation & EDIT::Container.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 16 Mai 2016
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/V4.hh"
#include "ERRor/Assert.hh"
#include "MATH/Q.hh"

#include "Answer.hh"
#include "ErrMsg.hh"
#include "Requests.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                    D�claration Pr�alable de Sp�cialisation
//------------------------------------------------------------------------------

#ifndef NOT_FOR_DOXYGEN

    template<>
    inline MATH::Q minus(MATH::Q pL, MATH::Q pR);
    template<>
    inline MATH::Q plus(MATH::Q pL, MATH::Q pR);

#endif  // NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                      Value : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pSet "Fonction" d'acc�s en �criture pour l'attribut � �diter
        @param pGet "Fonction" d'acc�s en lecture pour l'attribut � �diter
     */
    template<class TData, int TEnum>
    Value<TData, TEnum>::Value(Setter pSet, Getter pGet)
        : mSet{pSet}
        , mGet{pGet}
        , mVoidGet{}
    { }


    /*! Constructeur.
        @param pSet "Fonction" d'acc�s en �criture pour l'attribut � �diter
        @param pGet "Fonction" d'acc�s en lecture pour l'attribut � �diter
     */
    template<class TData, int TEnum>
    Value<TData, TEnum>::Value(Setter pSet, GetterVoid pGet)
        : mSet{pSet}
        , mGet{}
        , mVoidGet{pGet}
    { }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    Value<TData, TEnum>::~Value() { }


//------------------------------------------------------------------------------
//                              Value : Op�rations
//------------------------------------------------------------------------------

    /*! Assigne une nouvelle valeur � l'attribut � modifier.
        @param pValue Nouvelle valeur
     */
    template<class TData, int TEnum>
    inline void Value<TData, TEnum>::set(TData pValue)
    {
        if (isGetVoid())
        {
            // Valeur pr�c�dente (la r�cup�ration par param�tre permet une initialisation particuli�re).
            TDataNoRef  lOldValue    = pValue;
            mVoidGet(lOldValue);

            mSet(pValue);

            // Nouvelle valeur ?
            TDataNoRef  lNewValue   = pValue;
            mVoidGet(lNewValue);

            // R�ponse.
            GenericAnswer<TData, TEnum> lAnswer{lNewValue, EditRequest<TData, TEnum>::type()};
            EditRequest<TData, TEnum>   lUndo{lOldValue, true};

            sendAnswer(&lAnswer, &lUndo);
        }
        else
        {
            // Valeur pr�c�dente.
            TDataNoRef  lOldValue   = mGet();

            mSet(pValue);

            // Nouvelle valeur ?
            TDataNoRef  lNewValue   = mGet();

            // R�ponse.
            GenericAnswer<TData, TEnum> lAnswer{lNewValue, EditRequest<TData, TEnum>::type()};
            EditRequest<TData, TEnum>   lUndo{lOldValue, true};

            sendAnswer(&lAnswer, &lUndo);
        }
    }


    /*! @return La valeur de l'attribut � r�cup�rer
     */
    template<class TData, int TEnum>
    inline TData Value<TData, TEnum>::get() const
    {
        ASSERT(!isGetVoid(), kInvalidCall)

        return mGet();
    }


    /*! @param pValue R�f�rence dans laquelle sera plac�e la valeur de l'attribut � r�cup�rer
     */
    template<class TData, int TEnum>
    inline void Value<TData, TEnum>::get(TDataNoRef& pValue) const
    {
        ASSERT(isGetVoid(), kInvalidCall)

        mVoidGet(pValue);
    }


    /*! @return VRAI s'il faut utiliser get(TDataNoRef&), FAUX pour get()
     */
    template<class TData, int TEnum>
    inline bool Value<TData, TEnum>::isGetVoid() const
    {
        return bool(mVoidGet);
    }


//------------------------------------------------------------------------------
//                    Variation : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pChange "Fonction" d'acc�s en �criture pour l'attribut � moduler
        @param pGet    "Fonction" d'acc�s en lecture pour l'attribut � moduler
     */
    template<class TData, int TEnum>
    Variation<TData, TEnum>::Variation(Changer pChange, Getter pGet)
        : mChange{pChange}
        , mGet{pGet}
    { }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    Variation<TData, TEnum>::~Variation() { }


//------------------------------------------------------------------------------
//                            Variation : Op�rations
//------------------------------------------------------------------------------

    /*! Change la valeur de l'attribut � modifier par une variation donn�e.
        @param pValue Variation � apporter � l'attribut
     */
    template<class TData, int TEnum>
    inline void Variation<TData, TEnum>::change(TData pValue)
    {
        // Valeur pr�c�dente.
        TDataNoRef  lOldValue   = mGet();

        mChange(pValue);

        // Nouvelle valeur ?
        TDataNoRef  lNewValue   = mGet();

        // R�ponse.
        GenericAnswer<TData, TEnum>     lAnswer{lNewValue, ModulateRequest<TData, TEnum>::type()};
        ModulateRequest<TData, TEnum>   lUndo{minus(lOldValue, lNewValue), true};

        sendAnswer(&lAnswer, &lUndo);
    }


    /*! @return La valeur de l'attribut � r�cup�rer
     */
    template<class TData, int TEnum>
    inline TData Variation<TData, TEnum>::get() const
    {
        return mGet();
    }


//------------------------------------------------------------------------------
//                            Variation : Op�rateurs
//------------------------------------------------------------------------------

    /*!
     */
    template<class TData>
    inline TData minus(TData pL, TData pR)
    {
        return (- pR + pL);
    }


    /*!
     */
    template<class TData>
    inline TData plus(TData pL, TData pR)
    {
        return pL + pR;
    }


    /* Sp�cialisation pour les quaterions.
     */
    template<>
    inline MATH::Q minus(MATH::Q pL, MATH::Q pR)
    {
        return (pR.conjugated() * pL).normalized();
    }


    /* Sp�cialisation pour les quaterions.
     */
    template<>
    inline MATH::Q plus(MATH::Q pL, MATH::Q pR)
    {
        return (pL * pR).normalized();
    }


//------------------------------------------------------------------------------
//                    Container : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pInsert "Fonction" d'insertion pour le type d'�l�ment � ajouter
        @param pRemove "Fonction" de retrait pour le type d'�l�ment � supprimer
     */
    template<class TData, int TEnum>
    Container<TData, TEnum>::Container(Inserter pInsert, Remover pRemove)
        : mInsert{pInsert}
        , mRemove{pRemove}
    { }


    /*! Destructeur.
     */
    template<class TData, int TEnum>
    Container<TData, TEnum>::~Container() { }


//------------------------------------------------------------------------------
//                            Container : Op�rations
//------------------------------------------------------------------------------

    /*! Ins�re un �lement du type � ajouter.
        @param pElement El�ment � ins�rer
     */
    template<class TData, int TEnum>
    inline void Container<TData, TEnum>::insert(TData pElement)
    {
        // Insertion.
        if (mInsert(pElement))
        {
            // L'insertion a r�ussi : envoie une r�ponse.
            GenericAnswer<TData, TEnum> lAnswer{pElement, InsertRequest<TData, TEnum>::type()};
            RemoveRequest<TData, TEnum> lUndo{pElement, true};

            sendAnswer(&lAnswer, &lUndo);
        }
        else
        {
            // Envoie une r�ponse "vide".
            sendAnswer(nullptr, nullptr);
        }
    }


    /*! Retire un �lement du type � supprimer.
        @param pElement El�ment � retirer
     */
    template<class TData, int TEnum>
    inline void Container<TData, TEnum>::remove(TData pElement)
    {
        // Retrait.
        if (mRemove(pElement))
        {
            // Le retrait a r�ussi : envoie une r�ponse.
            GenericAnswer<TData, TEnum> lAnswer{pElement, RemoveRequest<TData, TEnum>::type()};
            InsertRequest<TData, TEnum> lUndo{pElement, true};

            sendAnswer(&lAnswer, &lUndo);
        }
        else
        {
            // Envoie une r�ponse "vide".
            sendAnswer(nullptr, nullptr);
        }
    }


//------------------------------------------------------------------------------
//                             Fonctions Assistantes
//------------------------------------------------------------------------------

    /*! @name Fonctions internes pour l'initialisation des sujets
        @internal
     */
    // @{
    //! @brief Classe pour d�duction de param�tre de m�thode.
    template<class S>
    struct ArgumentOf;

    //! @brief Sp�cialisation.
    template<class TCtrl, class R, class TData>
    struct ArgumentOf<R (TCtrl::*)(TData)>
    {
        using Type = TData;
    };

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type du param�tre du "setter"
        @param pController Contr�leur dont lier un "setter"
        @param pSetter     M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Value<TData, TEnum>::Setter bindSetter(TCtrl* pController, void (TCtrl::*pSetter)(TData))
    {
        return std::bind(static_cast<void (TCtrl::*)(TData)>(pSetter), pController, _1);
    }

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type du param�tre du "getter"
        @param pController Contr�leur dont lier un "getter" (valeur en param�tre de sortie)
        @param pGetter     M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Value<TData, TEnum>::GetterVoid bindGetter(TCtrl* pController, void (TCtrl::*pGetter)(TData&) const)
    {
        return std::bind(static_cast<void (TCtrl::*)(typename EDIT::Value<TData>::TDataNoRef&) const>(pGetter), pController, _1);
    }

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type de retour du "getter"
        @param pController Contr�leur dont lier un "getter" (valeur retourn�e)
        @param pGetter     M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Value<TData, TEnum>::Getter bindGetter(TCtrl* pController, TData (TCtrl::*pGetter)() const)
    {
        return std::bind(static_cast<TData (TCtrl::*)() const>(pGetter), pController);
    }

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type du param�tre du "changer"
        @param pController Contr�leur dont lier un "changer"
        @param pChanger    M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Variation<TData, TEnum>::Changer bindChanger(TCtrl* pController, void (TCtrl::*pChanger)(TData))
    {
        return std::bind(static_cast<void (TCtrl::*)(TData)>(pChanger), pController, _1);
    }

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type du param�tre de l'"inserter"
        @param pController Contr�leur dont lier un "inserter"
        @param pInserter   M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Container<TData, TEnum>::Inserter bindInserter(TCtrl* pController, bool (TCtrl::*pInserter)(TData))
    {
        return std::bind(static_cast<bool (TCtrl::*)(TData)>(pInserter), pController, _1);
    }

    /*! @tparam TEnum      Discriminant pour la surcharge. Premier param�tre template afin de forcer la surcharge.
        @tparam TCtrl      Classe contr�leur
        @tparam TData      Type du param�tre du "remover"
        @param pController Contr�leur dont lier un "remover"
        @param pRemover    M�thode � lier
     */
    template<int TEnum, class TCtrl, class TData>
    inline typename EDIT::Container<TData, TEnum>::Remover bindRemover(TCtrl* pController, bool (TCtrl::*pRemover)(TData))
    {
        return std::bind(static_cast<bool (TCtrl::*)(TData)>(pRemover), pController, _1);
    }

    /*! Discrimination d'un setter : void (@p TCtrl::*)(@p TData) .
        @tparam TCtrl Classe contr�leur
        @tparam TData Type de la donn�e manipul�e par l'accesseur
        @tparam TEnum Non utilis�
        @param  pFunc Setter de @p TCtrl
     */
    template<class TCtrl, class TData, int TEnum = -1>
    auto select(void (TCtrl::*pFunc)(TData)) -> void (TCtrl::*)(TData)
    {
        return pFunc;
    }

    /*! Discrimination d'un getter : @p TData (@p TCtrl::*)() const .
        @tparam TCtrl Classe contr�leur
        @tparam TData Type de la donn�e manipul�e par l'accesseur
        @tparam TEnum Non utilis�
        @param  pFunc Getter de @p TCtrl
     */
    template<class TCtrl, class TData, int TEnum = -1>
    auto select(typename std::remove_const<typename std::remove_reference<TData>::type>::type (TCtrl::*pFunc)() const)
        -> typename std::remove_const<typename std::remove_reference<TData>::type>::type (TCtrl::*)() const
    {
        return pFunc;
    }

    /*! Discrimination d'un getter : void (@p TCtrl::*)(@p TData&) const .
        @tparam TCtrl Classe contr�leur
        @tparam TData Type de la donn�e manipul�e par l'accesseur
        @tparam TEnum Non utilis�
        @param  pFunc Getter de @p TCtrl
     */
    template<class TCtrl, class TData, int TEnum = -1>
    auto select(void (TCtrl::*pFunc)(typename std::remove_const<typename std::remove_reference<TData>::type>::type&) const)
        -> void (TCtrl::*)(typename std::remove_const<typename std::remove_reference<TData>::type>::type&) const { return pFunc; }

    /*! Fonction assistante (et sp�cialisation) pour la r�solution de surcharge avec __VA_ARGS__.@n
        Les macros d'initialisation (INITIALIZE_VALUE, INITIALIZE_VARIATION & INITIALIZE_CONTAINER) acceptent maintenant un � deux
        param�tres suppl�mentaires, put�t que d'�tre dupliqu�es en une version _OVERLOAD. Afin d'instancier correctemenr les classes
        Value, Variation et Container, il fallait notamment une valeur constante, m�me en cas de non indication de valeur pour les
        surcharges.@n
        Cette fonction permet de retourner syst�matiquement une valeur.
        @sa EDIT_Attributes_Page
     */
    template<int TEnum = -1>
    constexpr int overloader()
    {
        return TEnum;
    }

    template<class TData, int TEnum = -1>
    constexpr int overloader()
    {
        return TEnum;
    }
    //@}

//------------------------------------------------------------------------------
//                                 Autres Macros
//------------------------------------------------------------------------------

#define FIRST(...) FIRST_(__VA_ARGS__,throwaway)
#define FIRST_(first,...) first

#define SECOND(...) SECOND_(__VA_ARGS__,,throwaway)
#define SECOND_(first, second,...) second

#define THIRD(...) THIRD_(__VA_ARGS__,,,throwaway)
#define THIRD_(first, second,third,...) third

#define FIFTH(...) FIFTH_(__VA_ARGS__,,,,throwaway)
#define FIFTH_(first,second,third,fourth,fifth...) fifth

#define DEFAULT_IF_EMPTY(default,...) SECOND(,##__VA_ARGS__,default)

#define ADD_ARGUMENT_IF_FIRST_IS_DIGIT(...) ADD_ARGUMENT_IF_FIRST_IS_DIGIT_(__VA_ARGS__,throwaway)
#define ADD_ARGUMENT_IF_FIRST_IS_DIGIT_(first,...) N##first

#define DEFAULT_IF_1ST_OPTION_IS_DIGIT_OR_EMPTY(default,...) THIRD(ADD_ARGUMENT_IF_FIRST_IS_DIGIT(__VA_ARGS__), default, DEFAULT_IF_EMPTY(default,##__VA_ARGS__))
#define N0 ,0
#define N1 ,1
#define N2 ,2
#define N3 ,3
#define N4 ,4
#define N5 ,5
#define N6 ,6
#define N7 ,7
#define N8 ,8
#define N9 ,9


#define SELECT_ACCESSOR(parent,...) THIRD(ADD_ARGUMENT_IF_FIRST_IS_DIGIT(__VA_ARGS__),,GET_ACCESSOR_IF_NOT_EMPTY(parent,##__VA_ARGS__))
#define GET_ACCESSOR_IF_NOT_EMPTY(parent,...) FIFTH(,##__VA_ARGS__,,SPECIFIC_ACCESSOR(parent,FIRST(__VA_ARGS__)),SPECIFIC_ACCESSOR(parent,FIRST(__VA_ARGS__)))
#define SPECIFIC_ACCESSOR(parent,data) (EDIT::select<parent,data>)
}
