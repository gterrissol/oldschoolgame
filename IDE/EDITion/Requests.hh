/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_REQUESTS_HH
#define EDIT_REQUESTS_HH

#include "EDITion.hh"

/*! @file IDE/EDITion/Requests.hh
    @brief En-t�te des classes EDIT::EditRequest, EDIT::ModulateRequest, EDIT::InsertRequest &
    EDIT::RemoveRequest.
    @author @ref Guillaume_Terrissol
    @date 5 Mai 2008 - 13 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QList>

#include "Request.hh"
#include "STL/Function.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                                   Requ�tes
//------------------------------------------------------------------------------

    /*! @brief Requ�te d'�dition d'un attribut.
        @version 1.1

        Pour �diter un attribut, il suffit d'instancier cette classe en passant comme param�tre au
        constructeur la nouvelle valeur souhait�e pour l'attribut en question. Si, pour le type
        <i>TData</i>, il existe plusieurs attributs distincts, utilisez le param�tre template
        <i>TEnum</i> pour les diff�rencier. Sinon, profitez de la valeur par d�faut pour ne pas avoir �
        g�rer ce param�tre.
     */
    template<class TData, int TEnum = -1>
    class EditRequest : public Request
    {
    public:
        //! @name Type
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                                  //!< Donn�e par valeur.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                        EditRequest(TData pValue, bool pUndo = false);              //!< Constructeur.
virtual EditRequest*    clone() const override;                                     //!< Constructeur virtuel.
virtual                 ~EditRequest();                                             //!< Destructeur.
        //@}
static inline  long     type();                                                     //!< Pseudo-rtti de la classe.

    private:
        //! @name Action
        //@{
virtual void            actOnSubjects(const SubjectList& pTargets) const override;  //!< Application de de la requ�te sur un sujet.
virtual bool            mergeRequest(const Request* pOther) override;               //!< Fusion de requ�tes.
        //@}
        TDataNoRef  mValue;                                                         //!< "Valeur" de la requ�te.
        bool        mUndo;                                                          //!< Requ�te d'annulation ?
    };


    /*! @brief Requ�te de modulation d'un attribut.
        @version 1.1

        Pour moduler la valeur d'un attribut, il suffit d'instancier cette classe en passant comme
        param�tre au constructeur la valeur par laquelle "moduler" l'attribut en question. Si, pour le
        type <i>TData</i>, il existe plusieurs attributs distincts, utilisez le param�tre template
        <i>TEnum</i> pour les diff�rencier. Sinon, profitez de la valeur par d�faut pour ne pas avoir �
        g�rer ce param�tre.
     */
    template<class TData, int TEnum = -1>
    class ModulateRequest : public Request
    {
    public:
        //! @name Type
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                                      //!< Donn�e par valeur.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                            ModulateRequest(TData pValue, bool pUndo = false);          //!< Constructeur.
virtual ModulateRequest*    clone() const override;                                     //!< Constructeur virtuel.
virtual                     ~ModulateRequest();                                         //!< Destructeur.
        //@}
static inline  long         type();                                                     //!< Pseudo-rtti de la classe.

    private:
        //! @name Action
        //@{
virtual void                actOnSubjects(const SubjectList& pTargets) const override;  //!< Application de de la requ�te sur un sujet.
virtual bool                mergeRequest(const Request* pOther) override;               //!< Fusion de requ�tes.
        //@}
        TDataNoRef  mValue;                                                             //!< "Valeur" de la requ�te.
        bool        mUndo;                                                              //!< Requ�te d'annulation ?
    };


    /*! @brief Requ�te d'insertion d'un �lement.
        @version 1.1

        Pour ins�rer un �lement dans un objet, il suffit d'instancier cette classe en passant comme
        param�tre au constructeur l'�l�ment � ins�rer, tout en utilisant le param�tre template
        <i>TEnum</i> pour diff�rencier les diff�rentes cat�gories d'�lement possibles. Pour un �lement
        d'un type unique, profitez de la valeur par d�faut de <i>TEnum</i> pour ne pas avoir � g�rer ce
        param�tre.
     */
    template<class TData, int TEnum = -1>
    class InsertRequest : public Request
    {
    public:
        //! @name Type
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                                  //!< Donn�e par valeur.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                        InsertRequest(TData pElement, bool pUndo = false);          //!< Constructeur.
virtual InsertRequest*  clone() const override;                                     //!< Constructeur virtuel.
virtual                 ~InsertRequest();                                           //!< Destructeur.
        //@}
static inline  long     type();                                                     //!< Pseudo-rtti de la classe.

    private:
        //! @name Action
        //@{
virtual void            actOnSubjects(const SubjectList& pTargets) const override;  //!< Application de de la requ�te sur un sujet.
virtual bool            mergeRequest(const Request* pOther) override;               //!< Fusion de requ�tes.
        //@}
        QList<TDataNoRef>   mElementList;                                           //!< "Valeur" de la requ�te.
        bool                mUndo;                                                  //!< Requ�te d'annulation ?
    };


    /*! @brief Requ�te de retrait d'un �lement (d'un type multiple).
        @version 1.1

        Pour retirer un �lement d'un type multiple (c'est-�-dire que l'objet "porteur" permet le retrait de plusieurs cat�gorie d'�l�ments de
        ce type), il suffit d'instancier cette classe en passant comme param�tre au constructeur l'�l�ment � retirer, tout en utilisant le
        param�tre template <i>TEnum</i> pour diff�rencier les diff�rentes cat�gories d'�lement. Pour un �lement d'un type unique, profitez
        de la valeur par d�faut de <i>TEnum</i> pour ne pas avoir � g�rer ce param�tre.
     */
    template<class TData, int TEnum = -1>
    class RemoveRequest : public Request
    {
    public:
        //! @name Type
        //@{
        using TDataNoRef =  typename std::remove_const<
                                typename std::remove_reference<TData>
                                    ::type>::type;                                  //!< Donn�e par valeur.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                        RemoveRequest(TData pValue, bool pUndo = false);            //!< Constructeur.
virtual RemoveRequest*  clone() const override;                                     //!< Constructeur virtuel.
virtual                 ~RemoveRequest();                                           //!< Destructeur.
        //@}
static inline  long     type();                                                     //!< Pseudo-rtti de la classe.

    private:
        //! @name Action
        //@{
virtual void            actOnSubjects(const SubjectList& pTargets) const override;  //!< Application de de la requ�te sur un sujet.
virtual bool            mergeRequest(const Request* pOther) override;               //!< Fusion de requ�tes.
        //@}
        QList<TDataNoRef>   mElementList;                                           //!< "Valeur" de la requ�te.
        bool                mUndo;                                                  //!< Requ�te d'annulation ?
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Requests.inl"

#endif  // De EDIT_REQUESTS_HH
