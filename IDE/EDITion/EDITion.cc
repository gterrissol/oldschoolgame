/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "EDITion.hh"

/*! @file IDE/EDITion/EDITion.cc
    @brief D�finitions diverses du module EDIT.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 2 Mai 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDebug>

#include <OSGi/Context.hh>

#include "ERRor/ErrMsg.hh"
#include "EXTernals/Define.hh"

#include "Request.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidCall,   "Appel invalide.",  "Invalid call.")
    REGISTER_ERR_MSG(kInvalidObject, "Objet invalide.",  "Invalid object.")


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page EDIT_Attributes_Page Edition des objets du moteur
        @section EDIT_Attributes_Page_Assumption Principe
        A venir

        @section EDIT_Attributes_Page_Components Composants
        A venir

        @section EDIT_Attributes_Page_Example Exemple concret
        A venir
     */
}

MINIMAL_ACTIVATOR(EDIT)

#if 0
#include "Subject.hh"
#include "ReSourCe/BaseResource.tcc"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/SharedPtr.hh"

#include "Attributes.hh"
#include "Controller.hh"
#include "Requests.hh"

namespace N
{
    class T { public: T() { } T(const T&) = default; };
    class G { public: G() { } G(const G&) = default; };
    class I { public: I() { } I(const I&) = default; };

    class Controller : public EDIT::GenericController<T>
    {
    public:

        Controller(EditedPtr pTile);

virtual ~Controller()
        {
        }

    private:

        const I& getI() const
        {
            qDebug() << "const I& getI() const";
            static I i;
            return i;
            
        }
        void setI(const I&)
        {
            qDebug() << "void setI(const I&)";
        }
        const I& getI2() const
        {
            qDebug() << "const I& getI2() const";
            static I i;
            return i;
        }
        void setI2(const I&)
        {
            qDebug() << "void setI2(const I&)";
        }
        void getG(G&) const
        {
            qDebug() << "void getG(G&) const";
        }
        void setG(const G&)
        {
            qDebug() << "void setG(const G&)";
        }
        bool insert(U32*)
        {
            qDebug() << "bool insert(U32*)";
            return true;
        }
        bool remove(U32*)
        {
            qDebug() << "bool remove(U32*)";
            return true;
        }

        void changeF(F32)
        {
            qDebug() << "void changeF(F32)";
        }
        F32  getF() const
        {
            qDebug() << "F32  getF() const";
            return k0F;
        }

virtual EDIT::Subject* asSubject();

        Q_CUSTOM_DECLARE_PRIVATE(Controller)
    };


    /*!
     */
    class Controller::ControllerPrivate
        : public EDIT::Subject
        , public EDIT::Value<const I&>
        , public EDIT::Value<const I&, 2>
        , public EDIT::Value<const G&>
        , public EDIT::Variation<F32>
        , public EDIT::Container<U32*>
    {
            Q_CUSTOM_DECLARE_PUBLIC(Controller)
    public:
        ControllerPrivate(Controller* pParent)
            : EDIT::Subject{"N"}
            , INITIALIZE_VALUE(    setI,    getI)
            , INITIALIZE_VALUE(    setI2,   getI2, 2)
            , INITIALIZE_VALUE(    setG,    getG)
            , INITIALIZE_VARIATION(changeF, getF)
            , INITIALIZE_CONTAINER(insert,  remove)
            , q{pParent}
        { }
    virtual ~ControllerPrivate()
        {

        }
    virtual void sendAnswer(EDIT::Answer* pAnswer, EDIT::Request* pUndo)
            {
                notify(pAnswer, pUndo);
            }
    };


    Controller::Controller(EditedPtr pTile)
        : EDIT::GenericController<T>(pTile)
        , pthis{this}
    { }

    EDIT::Subject* Controller::asSubject()
    {
        Q_D(Controller);

        return d;
    }
}

namespace EDIT
{
    SubjectManager  lMgr{"N", nullptr};

    void foo()
    {
        auto                                lTilePtr    = std::make_shared<N::T>();
        std::shared_ptr<EDIT::Controller>   lController = std::make_shared<N::Controller>(lTilePtr);

        qDebug() << "- 0 ****************************";
        {
            N::I                           lI;
            EDIT::EditRequest<const N::I&> lRequest1{lI};
            SubjectList                lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest1);
        }
        qDebug() << "- 0' ***************************";
        {
            N::I                           lI;
            EDIT::EditRequest<const N::I&, 2> lRequest1{lI};
            SubjectList                lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest1);
        }
        qDebug() << "- 1 ****************************";
        {
            N::G                           lGroup;
            EDIT::EditRequest<const N::G&> lRequest1{lGroup};
            SubjectList                lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest1);
        }
        qDebug() << "- 2 ****************************";
        {
            U32                         lE  = k0UL;
            EDIT::InsertRequest<U32*>   lRequest2{&lE};
            SubjectList            lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest2);
            EDIT::RemoveRequest<U32*>   lRequest2_{&lE};
            lList.perform(&lRequest2_);
        }
        qDebug() << "- 3 ****************************";
        {
            F32                         lF  = k0F;
            EDIT::ModulateRequest<F32>  lRequest3{lF};
            SubjectList            lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest3);
        }
        qDebug() << "- 4 ****************************";
        lTilePtr.reset();
        qDebug() << QString{"Controller == 0x%1"}.arg(reinterpret_cast<int>(lController.get()), 8, 16);
        qDebug() << "- 5 ****************************";
        {
            N::G                           lGroup;
            EDIT::EditRequest<const N::G&> lRequest4{lGroup};
            SubjectList                lList;
            lList << N::Controller::subject(lController);
            lList.perform(&lRequest4);
        }
        qDebug() << "- 6 ****************************";
        qDebug() << QString{"Controller == 0x%1"}.arg(reinterpret_cast<int>(lController.get()), 8, 16);
        qDebug() << "- 7 ****************************";
    }

}
#endif
