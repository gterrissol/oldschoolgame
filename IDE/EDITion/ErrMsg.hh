/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_ERRMSG_HH
#define EDIT_ERRMSG_HH

/*! @file IDE/EDITion/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module DAT.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 8 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace EDIT
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kInvalidCall;
    extern  ERR::String kInvalidObject;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De EDIT_ERRMSG_HH
