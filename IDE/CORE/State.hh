/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_STATE_HH
#define CORE_STATE_HH

#include "CORE.hh"

/*! @file IDE/CORE/State.hh
    @brief En-t�te de la classe CORE::State.
    @author @ref Guillaume_Terrissol
    @date 9 Septembre 2005 - 7 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QObject>

#include "Private.hh"

class QBitArray;
template<class T1, class T2>   struct QPair;

namespace CORE
{
    /*! @brief Etat de l'application.
        @version 0.6

        Cette classe n'est utilis�e que par l'impl�mentation de StateMachine. Si Qt savait moccer les
        classes internes, elle aurait fait partie de l'interface priv�e de StateMachine.@n
        Cette classe correspond � un �tat complexe (combinaison de plusieurs �tats �l�mentaires) de
        l'application.
     */
    class State : public QObject
    {
        Q_OBJECT
    public:

        using Configuration = QPair<QBitArray, QBitArray>;                  //!< Configuration compl�te.
        //! @name Constructeur & destructeur
        //@{
                State(Configuration pActiveConfig, StateMachine* pParent);  //!< Constructeur.
virtual         ~State();                                                   //!< Destructeur.
        //@}
        void    update(Configuration pCurrentConfig);                       //!< Mise � jour.

    signals:
        //! @name Signal
        //@{
        void    changes(bool pEnabled);                                     //!< Activation de l'�tat.
        //@}
    private:

        Q_CUSTOM_DECLARE_PRIVATE(State)
    };
}

#endif  // CORE_STATE_HH
