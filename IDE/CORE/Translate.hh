/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_TRANSLATE_HH
#define CORE_TRANSLATE_HH

#include "CORE.hh"

/*! @file IDE/CORE/Translate.hh
    @brief En-t�te des classes CORE::Translated & CORE::Translate.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <functional>

#include <QString>

class QObject;

namespace CORE
{
    /*! @brief Objet traduit.
        @version 0.5
     */
    class Translated
    {
    public:

        using Updater = std::function<void ()>;     //!< 

                Translated();                       //!< 
virtual         ~Translated();                      //!< 

        Updater translater();                       //!< 

    protected:

        void    from     (QObject* pObject);        //!< 
        QString fromName () const;                  //!< 
        QString translate(QString pText) const;     //!< 
        QString translate(const char* pText) const; //!< 

    private:
    
virtual Updater buildTranslater() = 0;              //!< Construction d'un "traducteur" d'IHM.

        QString     mFrom;
        const char* mContext;
    };


    /*! @brief Objet traduit g�n�rique.
        @version 0.4
     */
    template<class TSelf>
    class Translate : public Translated
    {
    public:

                Translate()
                    : Translated()
                { }
virtual         ~Translate() {}
        TSelf&  from(QObject* pObject)
        {
            Translated::from(pObject);
            return dynamic_cast<TSelf&>(*this);
        }
    };
}

#endif  // CORE_TRANSLATE_HH
