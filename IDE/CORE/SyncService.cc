/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "SyncService.hh"

/*! @file IDE/CORE/SyncService.cc
    @brief M�thodes (non-inline) de la classe CORE::SyncService.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 6 D�cembre 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Synchronize.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    SyncService::SyncService()
        : OSGi::Service()
        , mSyncMachine{nullptr}
        , mFirstActions{}
    { }


    /*! Defaulted.
     */
    SyncService::~SyncService() = default;


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*! @note Par d�faut, l'�tat est inactif. Utilisez startWith() pour activer un �tat au lancement
     */
    void SyncService::declare(const Synchronizer& pConfiguration)
    {
        mSyncMachine.declare(pConfiguration.state(), pConfiguration.enter(), pConfiguration.leave());
        startWith(Enabler{pConfiguration.leave()});
        mLeaves.append(pConfiguration.leave());
    }


    /*!
     */
    void SyncService::bind(QObject* pObject, QString pState)
    {
        mSyncMachine.synchronize(pObject, pState);
    }


    /*!
     */
    void SyncService::perform(QString pAction)
    {
        mSyncMachine.perform(pAction);
    }


    /*!
     */
    void SyncService::registerType(QString pKey)
    {
        mSyncMachine.registerType(pKey);
    }


    /*!
     */
    void SyncService::open(QString pDoc)
    {
        mSyncMachine.open(pDoc);
    }

    /*!
     */
    void SyncService::close(QString pDoc)
    {
        mSyncMachine.close(pDoc);
    }

    /*!
     */
    void SyncService::closeAll()
    {
        mSyncMachine.closeAll();
    }

    /*!
     */
    void SyncService::startWith(const Enabler& pAction)
    {
        mFirstActions << pAction.name();
    }


    /*!
     */
    void SyncService::onStart()
    {
        for(auto a : mFirstActions)
        {
            perform(a);
        }
    }


    /*!
     */
    void SyncService::onStop()
    {
        for(auto a : mLeaves)
        {
            perform(a);
        }
    }


//------------------------------------------------------------------------------
//                                Impl�mentation
//------------------------------------------------------------------------------

    /*!
     */
    bool SyncService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string SyncService::typeName() const
    {
        return typeid(*this).name();
    }
}
