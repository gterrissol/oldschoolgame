/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MenuService.hh"

/*! @file IDE/CORE/MenuService.cc
    @brief M�thodes (non-inline) de la classe CORE::MenuService.
    @author @ref Guillaume_Terrissol
    @date 21 Juin 2013 - 8 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Menu.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    MenuService::MenuService() = default;


    /*! Defaulted.
     */
    MenuService::~MenuService() = default;


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*!
     */
    Menu MenuService::menu(QString pName)
    {
        return Menu(pName);
    }


    /*!
     */
    MenuItem MenuService::item(QString pMenu, QString pName)
    {
        return MenuItem(pMenu, pName).handler(this);
    }


    /*!
     */
    MenuItem MenuService::separator(QString pMenu)
    {
        return MenuItem(pMenu).handler(this);
    }


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*! 
     */
    void MenuService::connect(QString pSignal, Slot pSlot)
    {
        mSlots[pSignal].push_back(pSlot);
    }


    /*! 
     */
    void MenuService::connect(QString pSignal, CheckedSlot pSlot)
    {
        mCheckedSlots[pSignal].push_back(pSlot);
    }


    /*! Permet de connecter un signal � un autre pour lequel des actions sont d�j� enregistr�es.
        @param pSignal        Signal d�clencheur
        @param pForwardSignal Signal � activer sur d�clenchement de @p pSignal
        @retval true Si le cha�nage a r�ussi
        @retval false Sinon (i.e. aucune action n'a encore �t� connect�e � @p pForwardSignal)
     */
    bool MenuService::connect(QString pSignal, QString pForwardSignal)
    {
        if      (mSlots.contains(pForwardSignal))
        {
            mSlots[pSignal].push_back([=] () { trigger(pForwardSignal); });
            return true;
        }
        else if (mCheckedSlots.contains(pForwardSignal))
        {
            mCheckedSlots[pSignal].push_back([=] (bool pIsChecked) { trigger(pForwardSignal, pIsChecked); });
            return true;
        }
        else
        {
            return false;
        }
    }


    /*! 
     */
    void MenuService::trigger(QString pSignal)
    {
        for(auto lSlot : mSlots[pSignal])
        {
            lSlot();
        }
    }


    /*! 
     */
    void MenuService::trigger(QString pSignal, bool pIsChecked)
    {
        for(auto lSlot : mCheckedSlots[pSignal])
        {
            lSlot(pIsChecked);
        }
    }


//------------------------------------------------------------------------------
//                                Implementation
//------------------------------------------------------------------------------

    /*!
     */
    bool MenuService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string MenuService::typeName() const
    {
        return typeid(*this).name();
    }
}
