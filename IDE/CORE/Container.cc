/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/CORE/Container.cc
    @brief M�thodes (non-inline) de la classe CORE::Container.
    @author @ref Guillaume_Terrissol
    @date 9 D�cembre 2012 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QCoreApplication>
#include <QDebug>
#include <QMenuBar>

#include "ERRor/Assert.hh"

#include "MenuService.hh"
#include "SyncService.hh"
#include "TrService.hh"
#include "UIService.hh"
#include "StateMachine.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                     Container : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Container::Container(QString pCategory, QString pCaption, QString pIcon, OSGi::Context* pContext)
        : QMainWindow(nullptr)
        , mContext(pContext)
        , mMenuData()
        , mMenuItemData()
        , mCategory(pCategory)
        , mCaption(pCaption)
        , mIcon(pIcon)
        , mUndoRedo{}
        , mUndoCount{}
        , mRedoCount{}
    {
        setObjectName(pCategory);
        menuBar()->setObjectName(QString("%1.menuBar").arg(pCategory));
        statusBar();

        setUndoRedoManager();

        auto lService = pContext->services()->findByTypeAndName<MenuService>("fr.osg.ide.menu");

        add(lService->menu("file").from(this).text(tr("&File")));
        add(lService->item("file", "new").from(this).
            text(tr("&New")).
            icon(   ":/ide/core/icons/new file").
            key (tr("Ctrl+N")).
            sync(   "enabled").
            tip (tr("Creates a new file.")).
            what(tr("Select this entry to create a new file.")));
        add(lService->item("file", "open").from(this).
            text(tr("&Open")).
            icon(   ":/ide/core/icons/open file").
            key (tr("Ctrl+O")).
            sync(   "enabled").
            tip (tr("Opens a file.")).
            what(tr("Select this entry to open an existing file.")));
        add(lService->item("file", "save").from(this).
            text(tr("&Save")).
            key (tr("Ctrl+S")).
            sync(   "enabled + file + " + pCategory + ".undo").
            tip (tr("Saves changes.")).
            what(tr("Select this entry to force modifications recording.")));
        add(lService->item("file", "close").from(this).
            text(tr("&Close")).
            icon(   ":/ide/core/icons/close file").
            key (tr("Ctrl+W")).
            sync(   "enabled + file").
            tip (tr("Closes the opened file.")).
            what(tr("Select this entry to end the opened file edition.")));
        add(lService->separator("file"));
        add(lService->item("file", "quit").from(this).
            text(tr("&Quit")).
            icon(   ":/ide/core/icons/quit").
            key (tr("Ctrl+Q")).
            sync(   "enabled").
            tip (tr("Quits the application.")).
            what(tr("Select this entry to quit the application.")));

        add(lService->menu("edit").from(this).text(tr("&Edit")));
        add(lService->item("edit", "undo").from(this).
            text(tr("&Undo")).
            icon(   ":/ide/core/icons/undo").
            key (tr("Ctrl+Z")).
            sync(   "enabled + " + pCategory + ".undo + edition").
            tip (tr("Undoes the last action.")).
            what(tr("Select this entry to undo the last action.")));
        add(lService->item("edit", "redo").from(this).
            text(tr("&Redo")).
            icon(   ":/ide/core/icons/redo").
            key(tr("Shift+Ctrl+Z")).
            sync(   "enabled + " + pCategory + ".redo + edition").
            tip (tr("Redoes the last action.")).
            what(tr("Select this entry to redo the last undone action.")));
        add(lService->item("edit", "edit").from(this).
            text(tr("&Edition")).
            key (tr("Ctrl+E")).
            sync(   "enabled").
            chk (true).
            tip (tr("Activates the edition mode.")).
            what(tr("Select this entry to alternate data edition (checked) and visualization (unchecked).")));
        add(lService->separator("edit"));
        add(lService->item("edit", "pref").from(this).
            text(tr("&Preferences")).
            key (tr("Ctrl+P")).
            sync(   "enabled").
            tip (tr("Sets the preferences.")).
            what(tr("Select this entry to edit the application preferences.")));

        add(lService->menu("help").from(this).text(tr("&Help")));
        add(lService->item("help", "man").from(this).
            text(tr("&Manual")).
            icon(   ":/ide/core/icons/manual").
            key(tr("F1")).
            tip (tr("Opens the online help.")). 
            what(tr("Select this entry to open the application manual.")));
        add(lService->item("help", "what").from(this).
            text(tr("What's this ?")).
            whatsThis().
            tip (tr("Activates the \"What's this ?\" mode.")).
            what(tr("Select this entry to display pieces of information about the interface components.")));
        add(lService->item("help", "about").from(this).
            text(tr("&About")).
            icon(":/ide/core/icons/about").
            tip (tr("Displays the copyright.")).
            what(tr("Select this entry to display the copyright information.")));
    }


    /*! Destructeur.
     */
    Container::~Container() { }


//------------------------------------------------------------------------------
//                              Container : M�thodes
//------------------------------------------------------------------------------

    /*!
     */
    void Container::setup(const QStringList& pWidgetNames)
    {
        for(auto lName : menuNames()) buildMenu(lName);

        // Main frame.
        auto    lUIService   = mContext->services()->findByTypeAndName<UIService>("fr.osg.ide.ui");
        auto    lSyncService = mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync");

        for(auto lName : sortWidgets(pWidgetNames))
        {
            if (auto lWidget = lUIService->build(lName))
            {
                if (auto lTranslated = dynamic_cast<Translated*>(lWidget.get()))
                {
                    add(lTranslated->translater());
                }
                if (auto lSynchronized = dynamic_cast<Synchronized*>(lWidget.get()))
                {
                    lSyncService->bind(lWidget.get(), lSynchronized->config());
                }
                addWidget(std::move(lWidget));
            }
        }

        // Si une fen�tre de log est ajout�e, la taille de toutes (une par cat�gorie) doit �tre gard�e en coh�rence.
        onLanguageChange();
    }


    /*!
     */
    QString Container::category() const
    {
        return mCategory;
    }


    /*!
     */
    QString Container::caption() const
    {
        return QCoreApplication::translate(metaObject()->className(), qPrintable(mCaption));
    }


    /*!
     */
    QString Container::icon() const
    {
        return mIcon;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pData Attributs pour le menu
     */
    void Container::add(const Menu& pData)
    {
        mMenuData.append(pData);
        mMenuItemData[pData.name()];    // Cela cr�e le vecteur d'items de menu.
    }


    /*! @param pData Attributs pour l'item de menu
     */
    void Container::add(const MenuItem& pData)
    {
        mMenuItemData[pData.menu()].push_back(pData);
        auto    lService = mContext->services()->findByTypeAndName<CORE::MenuService>("fr.osg.ide.menu");
        if (pData.isCheckable())
        {
            lService->connect(pData.signal(), MenuService::CheckedSlot([=](bool b) { calledMenu(pData.name(), b); }));
        }
        else
        {
            lService->connect(pData.signal(), MenuService::Slot([=]() { calledMenu(pData.name()); }));
        }
    }


    /*!
     */
    void Container::add(Translated::Updater pTranslater)
    {
        mTranslaters.append(pTranslater);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::buildMenu(QString pName)
    {
        auto    lSyncService = mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync");
        auto    lMenuData    = std::find_if(mMenuData.begin(), mMenuData.end(), [pName](decltype(*mMenuData.begin()) pMenu) -> bool
        {
            return pName == pMenu.name();
        });
        if (lMenuData != mMenuData.end())
        {
            QMenuBar*   lMenus  = menuBar();
            QMenu*      lMenu   = lMenuData->addTo(lMenus);
            add(lMenuData->translater());

            for(auto& lItem : mMenuItemData[pName])
            {
                auto    lAction = lItem.addTo(lMenu);   // TODO : Enregistrer les actions (pour les r�cup�rer par nom).
                add(lItem.translater());
                if (!lItem.sync().isEmpty())
                {
                    lSyncService->bind(lAction, lItem.sync());
                }
            }
        }
    }


    /*!
     */
    void Container::addWidget(Widget) { }


    /*!
     */
    void Container::changeEvent(QEvent* pEvent)
    {
        if (pEvent->type() == QEvent::LanguageChange)
        {
            onLanguageChange();
        }
        else
        {
            QMainWindow::changeEvent(pEvent);
        }
    }


    /*!
     */
    OSGi::Context* Container::context() const
    {
        return mContext;
    }


    /*! 
     */
    void Container::setUndoRedoManager()
    {
        mUndoRedo = new EDIT::SubjectManager{mCategory, this};

        auto    lSyncService = context()->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
        lSyncService->bind(mUndoRedo, "edition");

        connect(mUndoRedo, SIGNAL(countChanged(int, int)), SLOT(undoRedoDone(int, int)));

        auto lMenuService = context()->services()->findByTypeAndName<MenuService>("fr.osg.ide.menu");
        lMenuService->connect(QStringLiteral("%1.edit.undo").arg(category()), MenuService::Slot{std::bind(&EDIT::SubjectManager::undo, mUndoRedo)});
        lMenuService->connect(QStringLiteral("%1.edit.redo").arg(category()), MenuService::Slot{std::bind(&EDIT::SubjectManager::redo, mUndoRedo)});

    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Container::calledMenu(QString pItem)
    {
        qDebug() << QString("Received signal for item %1 for %2").arg(pItem).arg(category());

        //auto    lSyncService = mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync");

        if      (pItem == "new")
        {
            //lSyncService->perform("open file");
        }
        else if (pItem == "open")
        {
            //lSyncService->perform("open file");
        }
        else if (pItem == "close")
        {
            //lSyncService->perform("close file");
        }
        else if (pItem == "quit")
        {
            qApp->quit();
        }
        else if (pItem == "undo")
        {
        }
        else if (pItem == "redo")
        {
        }
        else if (pItem == "pref")
        {
            static QString  sLocale = "en";
            sLocale = (sLocale == "en") ? "fr" : "en";
            mContext->services()->findByTypeAndName<CORE::TrService>("fr.osg.ide.tr")->set(sLocale);
        }
        else if (pItem == "man")
        {
        }
        else if (pItem == "about")
        {
        }
        else
        {
            qDebug() << QString("Received signal for unknown action %1").arg(pItem);
        }
    }


    /*!
     */
    void Container::calledMenu(QString pItem, bool pIsChecked)
    {
        qDebug() << QString("Received signal for item %1 : [%2]").arg(pItem).arg(pIsChecked ? 'X' : ' ');
        if (pItem == "edit")
        {
        }
        else
        {
            qDebug() << QString("Received signal for unknown action %1 : [%2]").arg(pItem).arg(pIsChecked ? 'X' : ' ');
        }
    }


    /*!
     */
    void Container::onLanguageChange()
    {
        // Call all setText() callbacks.
        for(auto lTranslater : mTranslaters)
        {
            lTranslater();
        }
    }


    /*!
     */
    void Container::undoRedoDone(int pUndo, int pRedo)
    {
        auto    lSyncService = mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync");

        bool    lOldUndoActive = (0 < mUndoCount);
        bool    lOldRedoActive = (0 < mRedoCount);

        bool    lNewUndoActive = (0 < pUndo);
        bool    lNewRedoActive = (0 < pRedo);

        if (lOldUndoActive != lNewUndoActive)
        {
            mUndoCount = pUndo;

            if (lNewUndoActive)
            {
                lSyncService->perform(QStringLiteral("positive %1.undo").arg(mCategory));
            }
            else
            {
                lSyncService->perform(QStringLiteral("null %1.undo").arg(mCategory));
            }
        }
        if (lOldRedoActive != lNewRedoActive)
        {
            mRedoCount = pRedo;

            if (lNewRedoActive)
            {
                lSyncService->perform(QStringLiteral("positive %1.redo").arg(mCategory));
            }
            else
            {
                lSyncService->perform(QStringLiteral("null %1.redo").arg(mCategory));
            }
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QStringList Container::menuNames()
    {
        QStringList lMenuNames;

        lMenuNames << "file" << "edit";
        for(int i = 3; i < mMenuData.count(); ++i)
            lMenuNames << mMenuData[i].name();
        lMenuNames << "help";

        return lMenuNames;
    }


    /*!
     */
    QStringList Container::sortWidgets(const QStringList& pNames) const
    {
        // Par d�faut, pas de tri.
        return pNames;
    }
}
