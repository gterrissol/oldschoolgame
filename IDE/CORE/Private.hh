/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_PRIVATE_HH
#define CORE_PRIVATE_HH

/*! @file IDE/CORE/Private.hh
    @brief En-t�te de la classe CORE::QPrivate.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 13 Janvier 2008 - 7 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QtGlobal>

namespace CORE
{
    /*! @brief Smart pointer d�di� au p-impl des classes Qt.
        @version 0.5

        Dans une classe @b C d�riv�e d'une classe de Qt, utiliser la macro @ref Q_CUSTOM_DECLARE_PRIVATE(@b C), dans
        sa section priv�e.@n
        D�finir la classe priv�e associ�e, @b C::CPrivate, et utiliser la macro @ref Q_CUSTOM_DECLARE_PUBLIC(@b C),
        �galement dans sa section priv�e.@n
        Tous les constructeurs de @b C::Private devront avoir un premier
        param�tre de type @b C*, le propri�taire du @e p-impl, � utiliser pour initialiser l'attribut @e @b q .@n
        Dans le(s) constructeur(s) de @b C, initialiser le @e p-impl via @e @b pthis (@b this, \<arguments du constructeur
        de la classe priv�e\>).@n
        @e Exemple:
        @code
    class C
    {
        Q_CUSTOM_DECLARE_PRIVATE(C)
    public:
        C();
        ...
    };

    class C::CPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(C)
    public:
        CPrivate(C* pThat, QString pName);
        ...
    };

    C::C()
        : pthis(this, QStringLiteral("text"))
    { ... }

    C::CPrivate::CPrivate(C* pThat, QString pName)
        : q(pThat)
    { ... }
        @endcode
        @tparam TQ    Type de la classe pour laquelle d�finir un p-impl (@b C dans l'exemple ci-dessus)
        @tparam TPriv Type du p-impl de TQ (@b CPrivate dans l'exemple ci-dessus)

        Ces param�tres dont �tablis par la macro Q_CUSTOM_DECLARE_PRIVATE()
     */
    template<class TQ, class TPriv>
    class Private
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    template<typename... TArgs>
        explicit    Private(TQ* pThat, TArgs&&... pArgs);   //!< Constructeur.
                    ~Private() noexcept;                    //!< Destructeur.
        //@}
        //! @name Acc�s au pointeur
        TPriv*      operator->() const noexcept;            //!< Op�rateur "fl�che".
        TPriv*      get() const noexcept;                   //!< Acc�s non-s�curis�.
        //@}
    private:

        TPriv*  mPointer;                                   //!< Pointeur encapsul�.
        Q_DISABLE_COPY(Private)
    };


//------------------------------------------------------------------------------
//                               Macros (a la Qt)
//------------------------------------------------------------------------------

/*! Cette macro permet de d�clarer un p-impl (a la Qt) dans la d�claration de la classe <i>QClass</i>.
    @sa Q_CUSTOM_DECLARE_PUBLIC(QClass)
 */
#define Q_CUSTOM_DECLARE_PRIVATE(QClass) \
    Q_DISABLE_COPY(QClass) \
    private: \
    class QClass##Private; \
    inline QClass##Private* d_func() { return pthis.get(); } \
    inline const QClass##Private* d_func() const { return pthis.get(); } \
    CORE::Private<QClass, QClass##Private> pthis;

/*! Cette macro permet de d�clarer l'acc�s � la classe <i>QClass</i> dans la d�claration de son p-impl (a la Qt).
    @sa Q_CUSTOM_DECLARE_PRIVATE(QClass)
 */
#define Q_CUSTOM_DECLARE_PUBLIC(QClass) \
    private: \
    QClass* q; \
    inline QClass* q_func() { return q; } \
    inline const QClass* q_func() const { return q; } \
    friend class QClass;
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Private.inl"

#endif  // De CORE_PRIVATE_HH
