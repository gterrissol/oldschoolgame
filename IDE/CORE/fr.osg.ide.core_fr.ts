<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CORE::Container</name>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <source>Creates a new file.</source>
        <translation>Crée un nouveau fichier.</translation>
    </message>
    <message>
        <source>Select this entry to create a new file.</source>
        <translation>Sélectionnez cette entrée pour créer un nouveau fichier.</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>Opens a file.</source>
        <translation>Ouvre un fichier.</translation>
    </message>
    <message>
        <source>Select this entry to open an existing file.</source>
        <translation>Sélectionnez cette entrée pour ouvrir un fichier existant.</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>Saves changes.</source>
        <translation>Enregistre les modifications actuelles.</translation>
    </message>
    <message>
        <source>Select this entry to force modifications recording.</source>
        <translation>Sélectionnez cette entrée pour forcer l&apos;enregistrement des modifications.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>Closes the opened file.</source>
        <translation>Ferme le fichier ouvert.</translation>
    </message>
    <message>
        <source>Select this entry to end the opened file edition.</source>
        <translation>Sélectionnez cette entrée pour terminer l&apos;édition du fichier ouvert.</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <source>Quits the application.</source>
        <translation>Quitte l&apos;application.</translation>
    </message>
    <message>
        <source>Select this entry to quit the application.</source>
        <translation>Sélectionnez cette entrée pour quitter l&apos;application.</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>Ann&amp;uler</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <source>Undoes the last action.</source>
        <translation>Annule la dernière action.</translation>
    </message>
    <message>
        <source>Select this entry to undo the last action.</source>
        <translation>Sélectionnez cette entrée pour annuler la dernière action.</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <source>Shift+Ctrl+Z</source>
        <translation>Shift+Ctrl+Z</translation>
    </message>
    <message>
        <source>Redoes the last action.</source>
        <translation>Réeffectue la dernière action.</translation>
    </message>
    <message>
        <source>Select this entry to redo the last undone action.</source>
        <translation>Sélectionnez cette entrée pour refaire la dernière action annulée.</translation>
    </message>
    <message>
        <source>&amp;Edition</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <source>Activates the edition mode.</source>
        <translation>Active le mode édition.</translation>
    </message>
    <message>
        <source>Select this entry to alternate data edition (checked) and visualization (unchecked).</source>
        <translation>Sélectionnez cette entré pour basculer de l&apos;édition (cochée) à la simple visualisation (décochée) des données.</translation>
    </message>
    <message>
        <source>&amp;Preferences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <source>Sets the preferences.</source>
        <translation></translation>
    </message>
    <message>
        <source>Select this entry to edit the application preferences.</source>
        <translation>Sélectionnez cette entrée pour éditer les préférences de l&apos;application.</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;Manual</source>
        <translation>&amp;Manuel</translation>
    </message>
    <message>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <source>Opens the online help.</source>
        <translation>Ouvre l&apos;aide en ligne.</translation>
    </message>
    <message>
        <source>Select this entry to open the application manual.</source>
        <translation>Sélectionnez cette entrée pour ouvrir le manuel de l&apos;application.</translation>
    </message>
    <message>
        <source>What&apos;s this ?</source>
        <translation>Qu&apos;est-ce que c&apos;est ?</translation>
    </message>
    <message>
        <source>Activates the &quot;What&apos;s this ?&quot; mode.</source>
        <translation>Active le mode &quot;Qu&apos;est-ce que c&apos;est ?&quot;.</translation>
    </message>
    <message>
        <source>Select this entry to display pieces of information about the interface components.</source>
        <translation>Sélectionnez cette entrée pour afficher des informations sur les composants de l&apos;interface.</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <source>Displays the copyright.</source>
        <translation>Affiche le copyright.</translation>
    </message>
    <message>
        <source>Select this entry to display the copyright information.</source>
        <translation>Sélectionnez cette entrée pour afficher les informations de copyright.</translation>
    </message>
    <message>
        <source>Please wait...</source>
        <translation>Veuillez patienter...</translation>
    </message>
</context>
</TS>
