/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Display.hh"

/*! @file IDE/CORE/Display.cc
    @brief M�thodes (non-inline) de la classe CORE::Display.
    @author @ref Guillaume_Terrissol
    @date 27 Octobre 2013 - 27 Octobre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QFile>
#include <QGridLayout>

namespace CORE
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param pParent Widget parent
     */
    Display::Display(QWidget* pParent)
        : QWidget(pParent)
    {
        auto    lLayout = new QGridLayout{this};
        lLayout->setContentsMargins(0, 0, 0, 0);
    }


    /*! Par d�faut.
     */
    Display::~Display() = default;


//------------------------------------------------------------------------------
//                                     Rendu
//------------------------------------------------------------------------------

    /*! @param pFilename Fichier de donn�es dont afficher le contenu sous forme d'imagte
     */
    void Display::load(QString pFilename)
    {
        if (QFile::exists(pFilename))
        {
            render(pFilename);
        }
        else
        {
            clear();
        }
    }


    /*! Nettoyage du rendu.
     */
    void Display::clear()
    {
        reset();
    }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @fn void Display::render(QString pFilename)
        @param pFilename Fichier dont afficher le contenu dans ce widget. La taille optimale est celle du widget : size()
        @note En cas d'erreur de chargement, un fond par d�faut peut �tre affich� (e.g. via reset())
     */


    /*! @fn void Display::reset()
        Affiche un fond par d�faut.
     */
}
