/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file CORE/CORE.cc
    @brief Enregistrement de l'activateur du module CORE.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2012 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QCoreApplication>
#include <QDebug>

#include <OSGi/Activator.hh>
#include <OSGi/Context.hh>

#include "ERRor/Assert.hh"

#include "Activator.hh"
#include "GameService.hh"
#include "MenuService.hh"
#include "ProgressService.hh"
#include "Synchronize.hh"
#include "SyncService.hh"
#include "TrService.hh"
#include "UIService.hh"

namespace CORE
{
    /*!
     */
    class CoreActivator : public IDEActivator
    {
    public:
                CoreActivator() : IDEActivator(BUNDLE_NAME) { }
virtual         ~CoreActivator() { }
    private:
virtual void    checkInServices(OSGi::Context* pContext) override final
                {
                    // Enregistrement des services.
                    pContext->services()->checkIn("fr.osg.ide.menu",     std::make_shared<CORE::MenuService>());
                    pContext->services()->checkIn("fr.osg.ide.sync",     std::make_shared<CORE::SyncService>());
                    pContext->services()->checkIn("fr.osg.ide.game",     std::make_shared<CORE::GameService>(pContext));
                    pContext->services()->checkIn("fr.osg.ide.tr",       std::make_shared<CORE::TrService>());
                    pContext->services()->checkIn("fr.osg.ide.ui",       std::make_shared<CORE::UIService>());
                    pContext->services()->checkIn("fr.osg.ide.progress", std::make_shared<CORE::ProgressService>());
                }
virtual void    checkOutServices(OSGi::Context* pContext) override
                {
                    pContext->services()->checkOut("fr.osg.ide.progress");
                    pContext->services()->checkOut("fr.osg.ide.ui");
                    pContext->services()->checkOut("fr.osg.ide.tr");
                    pContext->services()->checkOut("fr.osg.ide.game");
                    pContext->services()->checkOut("fr.osg.ide.sync");
                    pContext->services()->checkOut("fr.osg.ide.menu");
                }
virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lTrService = pContext->services()->findByTypeAndName<TrService>("fr.osg.ide.tr");
                    ASSERT_EX(lTrService, "Translation service should have been registered.", return;)

                    QString lQmFiles = QString::fromStdString(pContext->properties("com.digia.qt")->get("qm"));
                    for(auto lQm : lQmFiles.split(",", QString::SkipEmptyParts))
                    {
                        lTrService->checkIn(QString("%1/%2/translations/%3").
                            arg(pContext->temporaryPath().c_str()).
                            arg("com.digia.qt").
                            arg(lQm));
                    }

                    auto    lSyncService = pContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync");

                    lSyncService->declare(Synchronizer{"file"}.    enter("open file").leave("close file"));
                    lSyncService->declare(Synchronizer{"enabled"}. enter("enable").   leave("disable"));
                    lSyncService->declare(Synchronizer{"hardware"}.enter("boot").     leave("shutdown"));
                    lSyncService->declare(Synchronizer{"edition"}. enter("edit").     leave("look"));
                    lSyncService->startWith(Enabler{"edit"});
                    lSyncService->startWith(Enabler{"enable"});

                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->execOnUIEvent([=]() { lSyncService->onStart(); }, UIService::eOnUILaunch);
                    lUIService->execOnUIEvent([=]() { lSyncService->onStop();  }, UIService::eOnUIStop);
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(CORE::CoreActivator)
OSGI_END_REGISTER_ACTIVATORS()
