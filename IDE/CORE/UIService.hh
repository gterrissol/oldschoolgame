/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_UISERVICE_HH
#define CORE_UISERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/UIService.hh
    @brief En-t�te de la classe CORE::UIService.
    @author @ref Guillaume_Terrissol
    @date 2 D�cembre 2012 - 12 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <QString>
#include <QStringList>
class QWidget;

#include <OSGi/Service.hh>

#include "Private.hh"

namespace CORE
{
    /*! @brief Service : cr�ation de l'interface utilisateur.
        @version 0.3

        Ce service est enregistr� sous le nom @b fr.osg.ide.ui .
     */
    class UIService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr           = std::shared_ptr<UIService>;                   //!< Pointeur sur service.
        using MainWindow    = std::unique_ptr<QWidget>;                     //!< Pointeur sur fen�tre principale
        using Widget        = std::unique_ptr<QWidget>;                     //!< Pointeur sur widget.
        using Builder       = std::function<Widget ()>;                     //!< Cr�ateur de widget.
        using Callback      = std::function<void ()>;                       //!< Callback sur d�marrage/arr�t.
        //@}
        //! @name Constructeur & destructeur    
        //@{    
                    UIService();                                            //!< Constructeur par d�faut.
virtual             ~UIService();                                           //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        void        checkIn(QString pFullName, Builder pBuilder);           //!< Enregistrement d'un cr�ateur de widget.
        void        checkOut(QString pFullName);                            //!< Suppression d'un cr�ateur de widget.
        Widget      build(QString pFullName) const;                         //!< Construction d'un widget.

        MainWindow  buildMainWindow(QStringList pTabOrder = QStringList{}); //!< Construction de la fen�tre principale.

        void    connect(QString pSender,   QString pSignal,
                        QString pReceiver, QString pSlot);                  //!< Connection diff�r�e.
        //@}
        //! @name Actions sur �v�nement
        //!< Ev�nements g�r�s
        enum EEvent
        {
            eBeforeUILaunch = 0,                                            //!< Avant la cr�ation de l'IHM principale.
            eOnUILaunch,                                                    //!< Une fois que l'IHM principale a �t� cr��e.
            eOnUIStop,                                                      //!< Au moment de fermer l'IHM principale.
            eAfterUIStop,                                                   //!< Apr�s fermeture de l'IHM principale.
            eEventCount                                                     //!< Nombre d'�v�nements.
        };
        void        execOnUIEvent(Callback pSlot, EEvent pEvent);           //!< Enregistrement d'actions sur �v�nement.
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
        Q_CUSTOM_DECLARE_PRIVATE(UIService)
    };
}

#endif  // De CORE_UISERVICE_HH
