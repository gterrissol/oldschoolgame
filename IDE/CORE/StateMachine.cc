/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "StateMachine.hh"

/*! @file IDE/CORE/StateMachine.cc
    @brief M�thodes (non-inline) des classes CORE::StateMachine::StateMachinePrivate & CORE::StateMachine.
    @author @ref Guillaume_Terrissol
    @date 17 Septembre 2005 - 17 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QBitArray>
#include <QList>
#include <QPair>
#include <QRegExp>
#include <QStringList>
#include <QVariant>
#include <QVector>

#include "State.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                StateMachinePrivate : P-Impl de StateMachine
//------------------------------------------------------------------------------

    /*! @brief P-Impl de StateMachine.
        @version 0.5
     */
    class StateMachine::StateMachinePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(StateMachine)
    public:
        using Configuration = State::Configuration;
        //! @name Constructeur
        //@{
                        StateMachinePrivate(StateMachine* pParent); //!< Constructeur.
        //@}
        //! @name Gestion des �tats
        //@{
 static void            formatStateName(QString& pState);           //!< 

        Configuration   configure(QString pState);                  //!< 
        int             stateIndex(QString pState);                 //!< 

        void            updateForState(int pIndex);                 //!< 
        void            updateForDocType(int pIndex);               //!< 
        
        void            reset();                                    //!< 
        //@}

        QList<QList<State*>>    mStateForUpdateList;                //!< Etats (li�s � des �tats �l�mentaires) regroup�s pour mise � jour.
        QStringList             mStateList;                         //!< Etats �l�mentaires.
        QStringList             mActionList;                        //!< Actions [d�s]activant les �tats �l�mentaires.

        QList<QList<State*>>    mDocTypeForUpdateList;              //!< Etats (li�s � des types de documents) regroup�s pour mise � jour.
        QStringList             mDocTypeList;                       //!< Types de documents.

        QList<Configuration>    mConfigsForRegister;                //!< Liste des configurations compl�tes d�j� d�finies.
        QList<State*>           mStatesForRegister;                 //!< Listes des �tats (complexes) d�j� d�finis.

        QBitArray               mCurrentStates;                     //!< Configuration actuelle : �tats �l�mentaires activ�s.
        QBitArray               mCurrentDocType;                    //!< Configuration actuelle : type de document ouvert.
        QVector<quint32>        mCurrentDocTypeCounters;            //!< Configuration actuelle : nombre de documents ouverts pour chaque type.
    };



//------------------------------------------------------------------------------
//                      StateMachinePrivate : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Machine � �tat pour laquelle porter des informations et fournir du comportement
     */
    StateMachine::StateMachinePrivate::StateMachinePrivate(StateMachine* pParent)
        : q(pParent)
        , mStateForUpdateList()
        , mStateList()
        , mActionList()
        , mDocTypeForUpdateList()
        , mDocTypeList()
        , mConfigsForRegister()
        , mStatesForRegister()
        , mCurrentStates()
        , mCurrentDocType()
        , mCurrentDocTypeCounters()
    { }


//------------------------------------------------------------------------------
//                   StateMachinePrivate : Gestion des Etats
//------------------------------------------------------------------------------

    /*!
     */
    StateMachine::StateMachinePrivate::Configuration StateMachine::StateMachinePrivate::configure(QString pState)
    {
        // Assertion sur l'existence des �tats.
        QStringList lStates = pState.split("+", QString::SkipEmptyParts);

        QBitArray   lConfigStates(mCurrentStates.size(), false);
        QBitArray   lConfigTypes(mCurrentDocType.size(), false);

        foreach(QString s, lStates)
        {
            // La fin des d�clarations de type pour une synchronisation doit �tre suivie d'un point-virgule.
            if (s.contains(";"))
            {
                QStringList lTypes = s.split(";", QString::SkipEmptyParts);
                foreach(QString lType, lTypes)
                {
                    int lIndex = mDocTypeList.indexOf(lType);
                    Q_ASSERT_X(lIndex != -1, "", "");
                    lConfigTypes.setBit(lIndex, true);
                }
            }
            else
            {
                bool    lPositive = true;
                if (s[0] == '!')
                {
                    lPositive = false;
                    s = s.mid(1);
                }

                int lIndex = mStateList.indexOf(s);
                Q_ASSERT_X(lIndex != -1, "", "");

                int lState1Index = 2 * lIndex;
                int lState2Index = 2 * lIndex + 1;

                lConfigStates.setBit(lState1Index, lPositive);
                lConfigStates.setBit(lState2Index, !lPositive);
            }
        }

        return qMakePair(lConfigStates, lConfigTypes);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void StateMachine::StateMachinePrivate::formatStateName(QString& pState)
    {
        pState = pState.replace(QRegExp(R"(\s)"), "").toLower();
    }


    /*! Retrouve l'index d'un �tat, dans mStatesForRegister, d'apr�s son nom. Si l'�tat n'�tait pas encore
        d�fini...
     */
    int StateMachine::StateMachinePrivate::stateIndex(QString pState)
    {
        Q_Q(StateMachine);

        formatStateName(pState);

        // Configuration compl�te associ�e � l'�tat.
        Configuration   lConfigs = configure(pState);
        int             lIndex   = mConfigsForRegister.indexOf(lConfigs);

        // La configuration est-elle d�j� r�pertori�e ?
        if (lIndex == -1)
        {
            // Non : il faut s'en occuper.

            // On ne veut plus que les noms des �tats.
            pState.replace("!", ""); 
            lIndex = mConfigsForRegister.size();

            // M�morise la configuration.
            mConfigsForRegister.push_back(lConfigs);
            // Cr�e et m�morise un nouvel �tat, activ� par cette configuration.
            State* lNewState = new State(lConfigs, q);
            mStatesForRegister.push_back(lNewState);

            // Recense les sous-�tats de la machine pour lequel mettre � jour le nouvel �tat.
            QStringList lStates = pState.split("+", QString::SkipEmptyParts);
            foreach(QString pState, lStates)
            {
                if (pState.contains(";"))
                {
                    // Documents : un sous-�tat de la machine pour chacun.
                    QStringList lDocTypes = pState.split(";", QString::SkipEmptyParts);
                    foreach(QString lDocType, lDocTypes)
                    {
                        int lId = mDocTypeList.indexOf(lDocType);
                        Q_ASSERT_X(0 <= lId, "int StateMachine::StateMachinePrivate::stateIndex(QString pState)", "Invalid document type");
                        mDocTypeForUpdateList[lId].push_back(lNewState);
                    }
                }
                else
                {
                    int lId = mStateList.indexOf(pState);
                    Q_ASSERT_X(0 <= lId, "int StateMachine::StateMachinePrivate::stateIndex(QString pState)", "Invalid state");
                    mStateForUpdateList[lId].push_back(lNewState);
                }
            }
        }

        return lIndex;
    }


    /*!
     */
    void StateMachine::StateMachinePrivate::updateForState(int pIndex)
    {
        QList<State*>& statesToUpdate = mStateForUpdateList[pIndex];

        foreach(State* s, statesToUpdate)
        {
            s->update(qMakePair(mCurrentStates, mCurrentDocType));
        }
    }


    /*!
     */
    void StateMachine::StateMachinePrivate::updateForDocType(int pIndex)
    {
        QList<State*>& statesToUpdate = mDocTypeForUpdateList[pIndex];

        foreach(State* s, statesToUpdate)
        {
            s->update(qMakePair(mCurrentStates, mCurrentDocType));
        }
    }


    /*! 
     */
    void StateMachine::StateMachinePrivate::reset()
    {
        mStateForUpdateList.clear();
        mStateList.clear();
        mActionList.clear();

        mDocTypeForUpdateList.clear();
        mDocTypeList.clear();

        mConfigsForRegister.clear();
        // Les �tats sont enfants de la machine, mais ils faut bien les supprimer manuellement en cas de r�initialisation.
        for(auto s : mStatesForRegister)
        {
            delete s;
        }
        mStatesForRegister.clear();

        mCurrentStates.clear();
        mCurrentDocType.clear();
        mCurrentDocTypeCounters.clear();
    }


//------------------------------------------------------------------------------
//                      StateMachine : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    StateMachine::StateMachine(QObject* pParent)
        : QObject(pParent)
        , pthis(this)
    { }


    /*! Destructeur.
     */
    StateMachine::~StateMachine() { }


//------------------------------------------------------------------------------
//                    StateMachine : Associations aux �tats
//------------------------------------------------------------------------------

    /*! @param pState Nom identifiant l'�tat; c'est lui qu'il faut utiliser lors de l'appel �
        synchronize(QObject*, QString)
        @param pEnter Nom de l'action permettant d'activer l'�tat <i>pState</i>
        @param pLeave Nom de l'action permettant d'inactiver l'�tat <i>pState</i>
     */
    void StateMachine::declare(QString pState, QString pEnter, QString pLeave)
    {
        Q_D(StateMachine);

        // Les d�clarations peuvent avoir lieu tant qu'aucune synchronisation n'a �t� effectu�e.
        Q_ASSERT_X(d->mCurrentStates.isNull(), "", "");

        d->formatStateName(pState);

        Q_ASSERT_X(!d->mStateList.contains(pState), "", "");
        d->mStateList.push_back(pState);
        d->mStateForUpdateList.push_back(QList<State*>());

        Q_ASSERT_X(d->mActionList.indexOf(pEnter) == -1, "", "");
        Q_ASSERT_X(d->mActionList.indexOf(pLeave) == -1, "", "");
        d->mActionList << pEnter << pLeave;
    }


    /*!
     */
    void StateMachine::registerType(QString pDocType)
    {
        Q_D(StateMachine);

        Q_ASSERT_X(d->mCurrentDocType.isNull(), "", "");

        Q_ASSERT_X((1 <= pDocType.length()) && (pDocType.length() <= 3), "void StateMachine::registerType(QString pDocType)", "Parameter is not a document type");

        Q_ASSERT_X(d->mDocTypeList.indexOf(pDocType) == -1, "", "");
        d->mDocTypeList << pDocType;

        d->mDocTypeForUpdateList.push_back(QList<State*>());
    }


    /*! @param pObject Instance � synchroniser avec l'�tat d�crit par <i>pState</i>
        @param pState  D�crit un �tat avec lequel rester coh�rent (active <i>o</i> si <i>pState</i> est
        VRAI). <i>pState</i> se d�finit par une ou plusieurs entr�es (s�par�es par des +). La
        synchronisation avec des types de document ouvert se sp�cifie avec la liste des extensions (d�j�
        enregistr�es) s�par�es par des ; (cette liste, si elle existe, doit constituer la derni�re entr�e
        des �tats - s�par�s par +).

        @note <b>Important</b> : Les objets � synchroniser sont d�sactiv�s par d�faut; une s�rie de
        perform() initiaux se chargera d'activer ceux qui doivent l'�tre au d�marrage de l'application
        (qui correspond � un �tat donn�)
     */
    void StateMachine::synchronize(QObject* pObject, QString pState)
    {
        Q_ASSERT_X(pObject->metaObject()->indexOfSlot("setEnabled(bool)") != -1, "void StateMachine::synchronize(QObject* pObject, QString pState)", "Object can't be enabled");

        // NB: On ne synchronise que les objets qui doivent l'�tre.
        if (!pState.isEmpty())
        {
            Q_D(StateMachine);

            if (d->mCurrentStates.isNull())
            {
                d->mCurrentStates.resize(2 * d->mStateList.size());
                d->mCurrentDocType.resize(d->mDocTypeList.size());
                d->mCurrentDocTypeCounters.resize(d->mCurrentDocType.size());
            }

            int     lIndex              = d->stateIndex(pState);
            State*  lSynchronizedState  = d->mStatesForRegister[lIndex];

            connect(lSynchronizedState, SIGNAL(changes(bool)), pObject, SLOT(setEnabled(bool)));
            pObject->setProperty("enabled", false);
        }
    }


    /*! R�initialise la machine � �tat.@n
        @note Les objets synchronis�s sont d�sactiv�s
     */
    void StateMachine::reset()
    {
        Q_D(StateMachine);
        // D�sactivation des objets synchronis�s.
        for(int i = 1; i < d->mActionList.count(); i += 2)
        {
            // Les actions de sortie sont les paires.
            perform(d->mActionList[i]);
        }
        // Nettoyage.
        d->reset();
    }


//------------------------------------------------------------------------------
//                       StateMachine : Changement d'Etat
//------------------------------------------------------------------------------

    /*! Actions autoris�es :
     */
    void StateMachine::perform(QString action)
    {
        Q_D(StateMachine);

        int lActionIndex = d->mActionList.indexOf(action);
        Q_ASSERT_X(lActionIndex != -1, "void StateMachine::perform(QString action)", "");

        // Les actions marchent par paire. En divisant par deux,
        // on retouve l'index de l'�tat proprement dit.
        int lStateIndex = lActionIndex / 2;

        int lAct1Index  = 2 * lStateIndex;
        int lAct2Index  = 2 * lStateIndex + 1;

        d->mCurrentStates.setBit(lAct1Index, lActionIndex == lAct1Index);
        d->mCurrentStates.setBit(lAct2Index, lActionIndex == lAct2Index);

        d->updateForState(lStateIndex);
    }


    /*! Ouvre un document.
     */
    void StateMachine::open(QString pDocType)
    {
        Q_D(StateMachine);

        int lDocTypeIndex   = d->mDocTypeList.indexOf(pDocType);

        if (lDocTypeIndex != -1)
        {
            ++d->mCurrentDocTypeCounters[lDocTypeIndex];

            d->mCurrentDocType.setBit(lDocTypeIndex, true);

            d->updateForDocType(lDocTypeIndex);
        }
    }


    /*! Ferme un document actuellement ouvert.
     */
    void StateMachine::close(QString pDocType)
    {
        Q_D(StateMachine);

        int lDocTypeIndex   = d->mDocTypeList.indexOf(pDocType);

        if (lDocTypeIndex != -1)
        {
            quint32&    lCounter = d->mCurrentDocTypeCounters[lDocTypeIndex];
            Q_ASSERT_X(0 < lCounter, "void StateMachine::close(QString pDocType)", "No more opened document of this type");

            if (--lCounter == 0)
            {
                d->mCurrentDocType.setBit(lDocTypeIndex, false);
                d->updateForDocType(lDocTypeIndex);
            }
        }
    }


    /*! Ferme tous les documents actuellement ouverts.
     */
    void StateMachine::closeAll()
    {
        Q_D(StateMachine);

        d->mCurrentDocType.fill(false);

        for(int lDocType = 0; lDocType < d->mCurrentDocType.size(); ++lDocType)
        {
            quint32&    lCounter = d->mCurrentDocTypeCounters[lDocType];

            if (0 < lCounter)
            {
                lCounter = 0;

                d->updateForDocType(lDocType);
            }
        }
    }
}
