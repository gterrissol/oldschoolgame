/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "UIService.hh"

/*! @file IDE/CORE/UIService.cc
    @brief M�thodes (non-inline) de la classe CORE::UIService.
    @author @ref Guillaume_Terrissol
    @date 9 D�cembre 2012 - 14 Avril 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QApplication>
#include <QBoxLayout>
#include <QDebug>
#include <QDesktopWidget>

#include "Container.hh"
#include "SyncService.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                              UIService : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de CORE::UIService.
        @version 0.5
     */
    class UIService::UIServicePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(UIService)
    public:

        /*! @brief Ev�nement pour nettoyage post-UI.
            @version 0.5
            @internal
         */
        class CleanUpEvent : public QEvent
        {
        public:
            //! @name Constructeur & destructeur
            //@{
            CleanUpEvent(UIServicePrivate* pParent);            //!< Constructeur.
    virtual ~CleanUpEvent();                                    //!< Destructeur.
            //@}
        private:

            UIService::UIServicePrivate*    mParent;            //!< P-impl du service fr.osg.ide.ui
        };
        //! @name Interface
        //@{
                UIServicePrivate(UIService* pThat);             //!< Constructeur.

        void    exec(EEvent pOn);                               //!< Ex�cution d'actions sur �v�nement.
        //@}
        //! @name Attributs
        //@{
        QMap<QString, Builder>      mBuilders;                  //!< Cr�ateurs de widget enregistr�s.
        QStringList                 mTabNames;                  //!< Noms des onglets.
        QMap<QString, QStringList>  mWidgetNames;               //!< Noms des widgets constructibles.
        QList<Callback>             mCallbacks[eEventCount];    //!< Actions sur �v�nement IHM.
        QVector<QStringList>        mDelayedConnections;        //!< Connexion futures.
        //@}
    };


    /*! @param pThat Service fr.osg.ide.ui
     */
    UIService::UIServicePrivate::UIServicePrivate(UIService* pThat)
        : q{pThat}
        , mBuilders{}
        , mTabNames{}
        , mWidgetNames{}
        , mCallbacks{}
        , mDelayedConnections{}
    { }


    /*! @param pOn Ev�nement d�clencheur des actions � ex�cuter
     */
    void UIService::UIServicePrivate::exec(EEvent pOn)
    {
        for(const auto& lSlot : mCallbacks[pOn])
        {
            lSlot();
        }
    }


    /*! @param pParent P-impl du service fr.osg.ide.ui
     */
    UIService::UIServicePrivate::CleanUpEvent::CleanUpEvent(UIService::UIServicePrivate* pParent)
        : QEvent{QEvent::None}
        , mParent(pParent)
    { }


    //! Ex�cute les actions enregistr�es pour UIService::eAfterUIStop.@n
    UIService::UIServicePrivate::CleanUpEvent::~CleanUpEvent()
    {
        mParent->exec(eAfterUIStop);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    UIService::UIService()
        : OSGi::Service()
        , pthis{this}
    { }


    //!
    UIService::~UIService() { }


//------------------------------------------------------------------------------
//  Interface
//------------------------------------------------------------------------------

    /*! @todo Ajouter un petit contr�le...
     */
    void UIService::checkIn(QString pFullName, Builder pBuilder)
    {
        QRegExp lLocaleRE(R"((\w+)(\.\w+)+)");

        QString lCategory = lLocaleRE.exactMatch(pFullName) ? lLocaleRE.cap(1) : "";
        QString lName     = lLocaleRE.exactMatch(pFullName) ? lLocaleRE.cap(2) : "";

        pthis->mBuilders[pFullName] = pBuilder;

        if (lName == ".container")
        {
            pthis->mTabNames << lCategory;
        }
        else
        {
            pthis->mWidgetNames[lCategory] << pFullName;
        }
    }


    /*!
     */
    void UIService::checkOut(QString pFullName)
    {
        QRegExp lLocaleRE(R"((\w+)(\.\w+)+)");

        QString lCategory = lLocaleRE.exactMatch(pFullName) ? lLocaleRE.cap(1) : "";
        QString lName     = lLocaleRE.exactMatch(pFullName) ? lLocaleRE.cap(2) : "";

        pthis->mBuilders.remove(pFullName);

        if (lName == ".container")
        {
            pthis->mTabNames.removeAll(lCategory);
        }
        else
        {
            pthis->mWidgetNames[lCategory].removeAll(pFullName);
        }
    }


    /*!
     */
    UIService::Widget UIService::build(QString pFullName) const
    {
        auto lBuilder = pthis->mBuilders.find(pFullName);
        if (lBuilder != pthis->mBuilders.end())
        {
            auto lW = lBuilder.value()();
            if (lW->objectName().isEmpty())
            {
                lW->setObjectName(pFullName);
            }
            return lW;
        }
        else
        {
            return nullptr;
        }
    }


    /*! @param pTabOrder Enum�re tout ou partie des onglets, dans l'ordre dans lequel il doivent appara�tre
     */
    UIService::MainWindow UIService::buildMainWindow(QStringList pTabOrder)
    {
        pthis->exec(eBeforeUILaunch);

        MainWindow  lMainWindow{new QWidget(nullptr, Qt::Window | Qt::FramelessWindowHint)};
        QTabWidget* lTabs = new QTabWidget(lMainWindow.get());
        lTabs->setObjectName("mainTabs");
        QBoxLayout* lLayout = new QBoxLayout(QBoxLayout::LeftToRight, lMainWindow.get());
        lLayout->insertWidget(-1, lTabs);
        lLayout->setMargin(0);
        lLayout->setSpacing(0);
        lMainWindow->setGeometry(QRect(0, 0, 1280, 720));//lMainWindow->setGeometry(QApplication::desktop()->screenGeometry());

        // Trie les onglets en tenant compte de l'ordre impos�.
        pthis->mTabNames = pTabOrder + pthis->mTabNames;
        pthis->mTabNames.removeDuplicates();

        for(auto lCategory : pthis->mTabNames)
        {
            if (auto lWidget = build(QStringLiteral("%1.container").arg(lCategory)))
            {
                if (auto lContainer = qobject_cast<Container*>(lWidget.get()))
                {
                    int  lIndex     = lTabs->addTab(lContainer, QIcon(lContainer->icon()), QStringLiteral(""));
                    lContainer->add([=]() { lTabs->setTabText(lIndex, QStringLiteral("  %1  ").arg(lContainer->caption())); }); // Traduction du titre de l'onglet.
                    lContainer->setup(pthis->mWidgetNames[lCategory]);
                    lWidget.release();  // Le conteneur a �t� ajout� avec succ�s juste au dessus...
                }
            }
            /*else
            {
                std::cerr << "Category " << lCategory->first << " : no container available" << std::endl;
            }*/
        }
        QObject::connect(lMainWindow.get(), &QWidget::destroyed, [&](QObject*)
        {
            pthis->exec(eOnUIStop);
            qApp->postEvent(qApp, new UIServicePrivate::CleanUpEvent{pthis.get()});
        });
        pthis->exec(eOnUILaunch);

        //1 onglet = un titre + 1 menu (avec custom) + widget enrichi (tabwidget pour Db, 3Widgets pour 3D, ...)

        for(const auto & conn : pthis->mDelayedConnections)
        {
            auto* lSender   = lMainWindow->findChild<QWidget*>(conn[0]);
            auto* lReceiver = lMainWindow->findChild<QWidget*>(conn[2]);
            if ((lSender != nullptr) && (lReceiver != nullptr))
            {
                if (QObject::connect(lSender, conn[1].toStdString().c_str(), lReceiver, conn[3].toStdString().c_str()))
                {
                    continue;
                }
            }
            qDebug() << "Failed to connect(" << conn[0] << ", SIGNAL(" << conn[1] << "), " << conn[2] << ", SLOT(" << conn[3] << "))";
        }

        return lMainWindow;

    }


    /*! Cr�er une connection, une fois la fen�tre principale cr��e (tous les widgets devraient �tre pr�sents).
        @param pSender   Nom de l'object �metteur.
        @param pSignal   Signal attendu.
        @param pReceiver Nom de l'bojet r�cepteur.
        @param pSlot     Slot vis�.
     */
    void UIService::connect(QString pSender, QString pSignal, QString pReceiver, QString pSlot)
    {
        pthis->mDelayedConnections.append({ pSender, pSignal, pReceiver, pSlot});
    }


    /*! @return The widget nomm� @p pFullName s'ils existe, @b nullptr sinon.
     * /
    QWidget* UIService::findWidget(QString pFullName)
    {
        if (pthis->mRoot != nullptr)
        {
            return pthis->mRoot->findChild<QWidget*>(pFullName);
        }
        return nullptr;
    }*/


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @param pSlot  Callback � ex�cuter lors de @p pEvent
        @param pEvent Ev�nement sur lequel �x�cuter @p pSlot
     */
    void UIService::execOnUIEvent(Callback pSlot, EEvent pEvent)
    {
        pthis->mCallbacks[pEvent].append(pSlot);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool UIService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string UIService::typeName() const
    {
        return typeid(*this).name();
    }
}
