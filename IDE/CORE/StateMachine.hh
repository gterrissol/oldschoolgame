/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_STATEMACHINE_HH
#define CORE_STATEMACHINE_HH

#include "CORE.hh"

/*! @file IDE/CORE/StateMachine.hh
    @brief En-t�te de la classe CORE::StateMachine.
    @author @ref Guillaume_Terrissol
    @date 17 Septembre 2005 - 7 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QObject>

#include "Private.hh"

namespace CORE
{
    /*! @brief Etats de l'application
        @version 0.75

        La gestion "manuelle" de l'activation de composants d'une interface graphique (entr�es de menu,
        widgets, etc) en fonction d'�tats particuliers de l'application (document ouvert, actions
        r�alis�es, etc) est une intarissable source d'erreurs (incoh�rences, oubli, etc). J'ai donc pens�
        introduire une gestion la plus automatis�e possible.@n
        Le fonctionnement g�n�ral est le suivant :
        - d�clarer une s�rie d'�tats �l�mentaires pour l'application (pas forc�ment incompatibles, il
        suffit qu'ils correspondent � un �tat pour lequel un composant doit s'activer ou se d�sactiver)
        (declare(QString, QString, QString)), @b et, si besoin est, une liste de type de donn�es
        (extensions) dont l'ouverture modifie l'�tat d'activation de certains composants
        (registerType(QString)),
        - @b apr�s cette phase de d�claration, il faut associer les composants aux combinaisons d'�tats pour
        lesquels ils sont actifs (synchronize(QObject*, QString)),
        - enfin, le chagement effectif d'�tat s'op�re gr�ce aux m�thodes perform(QString), open(QString),
        close(QString) et closeAll() : les objets associ�s aux �tats s'activeront ou se d�sactiveront selon
        les changements de configuration.@n
        Une fois cette phase d'initialisation achev�e, l'application peut �tre configur�e (i.e.
        l'application va activer une s�rie d'�tats �l�mentaires; les objets associ�es � ces combinaisons
        (configuration) d'�tats verront leur �tat d'activation changer).

        @note Il faut bien distinguer les �tats �l�mentaires, et les �tats [complexes] qui sont une
        combinaison d'�tats �lementaires
     */
    class StateMachine : public QObject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                StateMachine(QObject* pParent);                             //!< Constructeur.
virtual         ~StateMachine();                                            //!< Destructeur.
        //@}
        //! @name Associations aux �tats
        //@{
        void    declare(QString pState, QString pEnter, QString pLeave);    //!< D�claration d'un �tat �l�mentaire.
        void    registerType(QString pDocType);                             //!< Enregistrement d'un type de document.
        void    synchronize(QObject* pObject, QString pState);              //!< Synchronisation avec un �tat complexe.
        void    reset();                                                    //!< R�initialisation.
        //@}
        //!  @name Changement d'�tat
        //@{
        void    perform(QString pAction);                                   //!< Ex�cution d'une action [d�s]activant un �tat �l�mentaire.
        void    open(QString pDocType);                                     //!< Notification de l'ouverture d'un document.
        void    close(QString pDocType);                                    //!< Notification de la fermeture d'un document.
        void    closeAll();                                                 //!< Notification de la fermeture de tous les documents.
        //@}
    private:

        Q_CUSTOM_DECLARE_PRIVATE(StateMachine)
    };
}

#endif  // CORE_STATEMACHINE_HH
