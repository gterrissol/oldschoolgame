/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_TRSERVICE_HH
#define CORE_TRSERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/TrService.hh
    @brief En-t�te de la classe CORE::TrService.
    @author @ref Guillaume_Terrissol
    @date 2 D�cembre 2012 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <QString>
#include <QStringList>

#include <OSGi/Service.hh>

class QTranslator;

namespace CORE
{
    /*!
     */
    class TrService : public OSGi::Service
    {
    public:
        //! @name Pointeur
        //@{
        typedef std::shared_ptr<TrService>  Ptr;                                        //!< Pointeur sur service.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    TrService();                                                        //!< Constructeur par d�faut.
virtual             ~TrService();                                                       //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        void        checkIn(QString pQm);       //!< Enregistrement d'un fichier de traductions pour chargement diff�r�.
        void        set(QString pLocale);   //!< Chargement des fichiers de traductions.
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
        void        load(QString pQm, QString pLocale);       //!< Chargement imm�diat d'un fichier de traduction.

        QMap<QString, QStringList>  mQmFiles;       //!< Traductions, class�es par locale.
        QMap<QString, QTranslator*> mTranslators;   //!< Traducteurs, class�es par [radical de] fichier de traduction.
    };
}

#endif  // De CORE_TRSERVICE_HH
