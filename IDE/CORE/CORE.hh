/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_CORE_HH
#define CORE_CORE_HH

/*! @file IDE/CORE/CORE.hh
    @brief Pr�-d�clarations du module @ref CORE.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2012 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

namespace CORE
{
    // Composants d'IHM.
    class Container;
    class Display;
    class Menu;
    class MenuItem;

    // Services.
    class GameService;
    class MenuService;
    class TrService;
    class UIService;

    // Autres.
    class Enabler;
    class Synchronizer;
    class StateMachine;
}

#endif  // De CORE_CORE_HH
