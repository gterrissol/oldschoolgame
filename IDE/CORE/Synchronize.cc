/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Synchronize.hh"

/*! @file IDE/CORE/Synchronize.cc
    @brief M�thodes (non-inline) des classes CORE::Synchronize & CORE::Enabler.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 6 Juillet 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CORE
{
//------------------------------------------------------------------------------
//                                 Synchronizer
//------------------------------------------------------------------------------

    /*!
     */
    Synchronizer::Synchronizer(QString pName)
        : mState(pName)
        , mEnter()
        , mLeave()
    { }


    /*!
     */
    Synchronizer& Synchronizer::enter(QString pEnter)
    {
        mEnter = pEnter;
        return *this;
    }


    /*!
     */
    Synchronizer& Synchronizer::leave(QString pLeave)
    {
        mLeave = pLeave;
        return *this;
    }


    /*!
     */
    QString Synchronizer::state() const
    {
        return mState;
    }


    /*!
     */
    QString Synchronizer::enter() const
    {
        return mEnter;
    }


    /*!
     */
    QString Synchronizer::leave() const
    {
        return mLeave;
    }


//------------------------------------------------------------------------------
//                                    Enabler
//------------------------------------------------------------------------------

    /*!
     */
    Enabler::Enabler(QString pName)
        : mName(pName)
    { }


    /*!
     */
    QString Enabler::name() const
    {
        return mName;
    }


//------------------------------------------------------------------------------
//                                 Synchronized
//------------------------------------------------------------------------------

    /*! @param pConfiguration Configuration de synchronisation (voir StateMachine::synchronize())
     */
    Synchronized::Synchronized(QString pConfiguration)
        : mConfig(pConfiguration)
    { }


    /*! Le seul int�r�t de ce destructeur est de fournir un RTTI � cette classe.
     */
    Synchronized::~Synchronized() = default;


    /*! @return La configration de synchronisation
     */
    QString Synchronized::config() const
    {
        return mConfig;
    }
}
