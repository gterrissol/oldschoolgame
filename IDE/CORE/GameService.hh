/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_GAMESERVICE_HH
#define CORE_GAMESERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/GameService.hh
    @brief En-t�te de la classe CORE::GameService.
    @author @ref Guillaume_Terrissol
    @date 7 D�cembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <QSize>
#include <QString>
#include <QVector>

#include <OSGi/Context.hh>
#include <OSGi/Service.hh>

#include "PROGram/GameSteps.hh"
#include "SHaDers/Library.hh"
#include "UNIverse/GameUniverse.hh"

#include "Private.hh"

namespace CORE
{
    /*! @brief Service : gestion du moteur de jeu.
        @version 0.5

        Ce service est enregistr� sous le nom @b fr.osg.ide.game .
     */
    class GameService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr   = std::shared_ptr<GameService>;                                     //!< Pointeur sur service.
        //@}    
        //! @name Constructeur & destructeur
        //@{
                                GameService(OSGi::Context* pContext);                   //!< Constructeur par d�faut.
virtual                         ~GameService();                                         //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        PROG::Game::Ptr         boot(void* pWinId);                                     //!< @copybrief PROG::Hardware::boot(void*)
        void                    halt();                                                 //!< @copybrief PROG::Hardware::halt()
        UNI::Universe::Ptr      universe();                                             //!< Univers de jeu.
        GEO::GeometryMgr::Ptr   geometries();                                           //!< G�om�tries.
        SHD::Library::Ptr       shaders();                                              //!< Biblioth�que de shaders.
        void                    resizeViewport(const QSize& pSize);                     //!< Redimensionnement du viewport.
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool                    isType(const std::string& pTypeName) const override;    //!< Est de type ... ?

    private:

virtual std::string             typeName() const override;                              //!< Nom du type (r�el).

        Q_CUSTOM_DECLARE_PRIVATE(GameService)
        //@}
    };
}

#endif  // De CORE_GAMESERVICE_HH
