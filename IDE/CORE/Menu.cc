/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Menu.hh"

/*! @file IDE/CORE/Menu.cc
    @brief M�thodes (non-inline) des classes CORE::Menu & CORE::MenuItem.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <cassert>

#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QWhatsThis>

#include "ERRor/Assert.hh"

#include "MenuService.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Menu::Menu() = default;


    /*!
     */
    Menu::Menu(QString pName)
        : mMenu(nullptr)
        , mName(pName)
        , mText()
        , mSync()
    { }


    /*!
     */
    Menu::~Menu() = default;


    /*!
     */
    Menu& Menu::text(QString pCaption)
    {
        mText = pCaption;
        return *this;
    }


    /*!
     */
    Menu& Menu::sync(QString pStates)
    {
        mSync = pStates;
        return *this;
    }


    /*!
     */
    QString Menu::name() const
    {
        return mName;
    }


    /*!
     */
    QString Menu::sync() const
    {
        return mSync;
    }


    /*!
     */
    QMenu* Menu::addTo(QMenuBar* pParent)
    {
        assert((pParent != nullptr) && "Invalid parent menubar.");

        if (mMenu == nullptr)
        {
            mMenu = pParent->addMenu(mText);
            mMenu->setObjectName(QString("%1.%2").arg(pParent->objectName()).arg(mName));
        }

        return mMenu;
    }


    /*!
     */
    Translated::Updater Menu::buildTranslater()
    {
        assert((mMenu != nullptr) && "Invalid menu.");

        return [=]()
        {
            if (!mText.isEmpty()) mMenu->setTitle(translate(mText));
        };
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    MenuItem::MenuItem()
        : MenuItem(QString::null, QString::null)
    { }


    /*!
     */
    MenuItem::MenuItem(QString pMenu)
        : MenuItem(pMenu, QStringLiteral(""))
    { }


    /*!
     */
    MenuItem::MenuItem(QString pMenu, QString pName)
        : mAction(nullptr)
        , mMenu(pMenu)
        , mName(pName)
        , mCaption()
        , mIcon()
        , mKey()
        , mCheck(eUncheckable)
        , mHandler(nullptr)
        , mTip()
        , mWhat()
        , mIsWhatsThis(false)
    { }


    /*!
     */
    MenuItem::~MenuItem() = default;


    /*!
     */
    MenuItem& MenuItem::text(QString pCaption)
    {
        mCaption = pCaption;
        return *this; 
    }


    /*!
     */
    MenuItem& MenuItem::icon(QString pFilename)
    {
        mIcon = pFilename;
        return *this;
    }


    /*!
     */
    MenuItem& MenuItem::key(QString pShortcut)
    {
        mKey = pShortcut;
        return *this;
    }


    /*!
     */
    MenuItem& MenuItem::sync(QString pStates)
    {
        mSync = pStates;
        return *this;
    }



    /*!
     */
    MenuItem& MenuItem::chk(bool pIsChecked)
    {
        mCheck = pIsChecked ? eChecked : eUnchecked;
        return *this;
    }


    /*!
     */
    MenuItem& MenuItem::tip(QString pStatusTip)
    {
        mTip = pStatusTip;
        return *this;
    }


    /*!
     */
    MenuItem& MenuItem::what(QString pWhatsThis)
    {
        mWhat = pWhatsThis;
        return *this;
    }


    /*!
     */
    MenuItem& MenuItem::whatsThis()
    {
        mIsWhatsThis = true;
        return *this;
    }


    /*!
     */
    QString MenuItem::menu() const
    {
        return mMenu;
    }


    /*!
     */
    QString MenuItem::name() const
    {
        return mName;
    }


    /*!
     */
    QString MenuItem::sync() const
    {
        return mSync;
    }


    /*!
     */
    QString MenuItem::signal() const
    {
        return QStringLiteral("%1.%2.%3").arg(fromName()).arg(menu()).arg(name());
    }


    /*!
     */
    bool MenuItem::isCheckable() const
    {
        return (mCheck != eUncheckable);
    }


    /*!
     */
    QAction* MenuItem::addTo(QMenu* pParent)  // La synchro doit �tre faite par l'appelant.
    {
        assert((pParent != nullptr) && "Invalid parent menu.");
        if (mAction == nullptr)
        {
            if (mIsWhatsThis)
            {
                mAction = QWhatsThis::createAction(pParent);
            }
            else
            {
                mAction = new QAction(pParent);
            }
            pParent->addAction(mAction);

            if (!mCaption.isEmpty() && !mName.isEmpty())
            {
                mAction->setText(mCaption);
                mAction->setObjectName(QString("%1->%2").arg(pParent->objectName()).arg(mName));

                if (!mIcon.isEmpty())   mAction->setIcon(QIcon(mIcon));
                if (!mKey.isEmpty())    mAction->setShortcut(mKey);
                if (mCheck == eUncheckable)
                {
                    if (mHandler != nullptr)    // Il y a le cas des What's this ?
                    {
                        QObject::connect(mAction, &QAction::triggered, [=]() { mHandler->trigger(signal()); });
                    }
                }
                else
                {
                    assert((mHandler != nullptr) && "Incompatible callback");
                    mAction->setCheckable(true);
                    mAction->setChecked(mCheck == eChecked);
                    QObject::connect(mAction, &QAction::triggered, [=](bool b) { mHandler->trigger(signal(), b); });
                }
                if (!mTip.isEmpty())    mAction->setStatusTip(mTip);
                if (!mWhat.isEmpty())   mAction->setWhatsThis(mWhat);
            }
            else
            {
                mAction->setSeparator(true);
            }
        }

        return mAction;
    }


    /*!
     */
    MenuItem& MenuItem::handler(MenuService* pHandler)
    {
        mHandler = pHandler;
        return *this;
    }


    /*!
     */
    Translated::Updater MenuItem::buildTranslater()
    {
        assert((mAction != nullptr) && "Invalid menu.");

        return [=]() {
            if (!mCaption.isEmpty()) mAction->setText(translate(mCaption));
            if (!mTip.isEmpty())     mAction->setStatusTip(translate(mTip));
            if (!mWhat.isEmpty())    mAction->setWhatsThis(translate(mWhat));
        };
    }
}
