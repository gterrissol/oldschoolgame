/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_MENU_HH
#define CORE_MENU_HH

#include "CORE.hh"

/*! @file IDE/CORE/Menu.hh
    @brief En-t�te des classes CORE::Menu & CORE::MenuItem.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 27 Octobre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <functional>

#include <QKeySequence>
#include <QString>

#include "Translate.hh"

class QAction;
class QMenu;
class QMenuBar;

namespace CORE
{
    /*! @brief D�finition de menu.
        @version 0.2
     */
    class Menu : public Translate<Menu>
    {
        //! @name Classe amie
        //@{
        friend class MenuService;                           //!< Pour l'acc�s au constructeur priv�.
        //@}
    public:
        //! @name Constructeurs & destructeur
        //@{
                Menu();
virtual         ~Menu();

        Menu&   text(QString pCaption);
        Menu&   sync(QString pStates);
        //@}    
        //! @name Informations
        //@{
        QString name() const;
        QString sync() const;
        //@}
        QMenu*  addTo(QMenuBar* pParent);


    private:
        //! @name Constructeur
        //@{
                Menu(QString pName);
        //@}
virtual Updater buildTranslater() override;

        QMenu*      mMenu;
        QString     mName;
        QString     mText;
        QString     mSync;
    };


    /*! @brief D�finition d'item de menu.
     */
    class MenuItem : public Translate<MenuItem>
    {
        //! @name Classe amie
        //@{
        friend class MenuService;                           //!< Pour l'acc�s au constructeur priv�.
        //@}
    public:
        //! @name Constructeurs & destructeur
        //@{
                    MenuItem();
virtual             ~MenuItem();

        MenuItem&   text(QString pCaption);
        MenuItem&   icon(QString pFilename);
        MenuItem&   key (QString pShortcut);
        MenuItem&   sync(QString pStates);
        MenuItem&   chk (bool pIsChecked);
        MenuItem&   tip (QString pStatusTip);
        MenuItem&   what(QString pWhatsThis);
        MenuItem&   whatsThis();
        //@}
        //! @name Informations
        //@{
        QString     menu() const;
        QString     name() const;
        QString     sync() const;
        QString     signal() const;
        bool        isCheckable() const;
        //@}
        QAction*    addTo(QMenu* pParent);


    private:
        //! @name Constructeurs
        //@{
                    MenuItem(QString pMenu);                //!< ... d'un s�parateur d'items.
                    MenuItem(QString pMenu, QString pName); //!< ... d'un item de menu.
        MenuItem&   handler(MenuService* pHandler);         //!< Liaison avec le service.
        //@}
virtual Updater     buildTranslater() override;

        enum ECheckStatus
        {
            eUncheckable,
            eUnchecked,
            eChecked
        };
        QAction*        mAction;
        QString         mMenu;
        QString         mName;
        QString         mCaption;
        QString         mIcon;
        QKeySequence    mKey;
        QString         mSync;
        ECheckStatus    mCheck;
        MenuService*    mHandler;
        QString         mTip;
        QString         mWhat;
        bool            mIsWhatsThis;
    };
}

#endif  // CORE_MENU_HH
