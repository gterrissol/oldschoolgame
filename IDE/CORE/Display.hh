/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_DISPLAY_HH
#define CORE_DISPLAY_HH

#include "CORE.hh"

/*! @file IDE/CORE/Display.hh
    @brief En-t�te de la classe CORE::Display.
    @author @ref Guillaume_Terrissol
    @date 27 Octobre 2013 - 27 Octobre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QString>
#include <QWidget>

namespace CORE
{
    /*! @brief Classe de base pour l'affichage du contenu de fichiers [externes] de donn�es.
        @version 0.5

        Afin d'afficher sous forme dans l'�diteur le contenu de fichiers de donn�es, cette classe peut �tre d�riv�e, pour chaque type
        de donn�es.
     */
    class Display : public QWidget
    {
    public:
        //! @name Pointeur
        //@{
        using Ptr = std::unique_ptr<Display>;           //!< ... sur afficheur.
        //@}
        //! @name Constructeur & destructeur
        //@{
                Display(QWidget* pParent = nullptr);    //!< Constructeur.
virtual         ~Display();                             //!< Destructeur.
        //@}
        //! @name Rendu
        //@{
        void    load(QString pFilename);                //!< Chargement du fichier et affichage de la donn�e.
        void    clear();                                //!< Nettoyage de l'affichage.


    private:

virtual void    render(QString pFilename) = 0;          //!< Rendu de la donn�e.
virtual void    reset() = 0;                            //!< Nettoyage de l'image affich�e.
        //@}
    };
}

#endif  // CORE_DISPLAY_HH
