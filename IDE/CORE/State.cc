/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "State.hh"

/*! @file IDE/CORE/State.cc
    @brief M�thodes (non-inline) des classes CORE::State::StatePrivate, CORE::State & CORE::Synchronized.
    @author @ref Guillaume_Terrissol
    @date 9 Septembre 2005 - 4 Juillet 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QBitArray>
#include <QPair>

#include "StateMachine.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                        StatePrivate : P-Impl de State
//------------------------------------------------------------------------------

    /*! @brief P-Impl de State.
        @version 0.3

     */
    class State::StatePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(State)
    public:
        using Configuration = State::Configuration;
        //! @name "Constructeurs"
        //@{
                StatePrivate(State* pParent, Configuration pConfig);  //!< Constructeur.
        void    init();                                                 //!< Initialisation.
        //@}
        void    update(Configuration pCurrent);                         //!< Mise � jour de l'activation.


    private:

        Configuration   mActiveConfiguration;                           //!< Configuration pour laquelle activer l'instance.
        bool            mActive;                                        //!< Etat d'activation actuel.
        bool            mNoDocument;                                    //!< Activation d'apr�s des types de documents ?
    };


//------------------------------------------------------------------------------
//                        StatePrivate : "Constructeurs"
//------------------------------------------------------------------------------

    /*! @param pParent Etat pour lequel porter des informations et fournir du comportement
        @param pConfig Configuration pour laquelle activer l'instance
     */
    State::StatePrivate::StatePrivate(State* pParent, Configuration pConfig)
        : q(pParent)
        , mActiveConfiguration(pConfig)
        , mActive(false)
        , mNoDocument(false)
    { }


    /*! Cette m�thode est appel�e une fois que l'�tat et "son impl�mentation" ont �t� li�s.
     */
    void State::StatePrivate::init()
    {
        const QBitArray&    lActiveDocTypes = mActiveConfiguration.second;
        mNoDocument = (lActiveDocTypes == QBitArray(lActiveDocTypes.size(), false));
    }


//------------------------------------------------------------------------------
//                          StatePrivate : Mise � jour
//------------------------------------------------------------------------------

    /*! Met � jour l'�tat suite � un changement de configuration de la machie � �tats.
        @param pCurrent Nouvelle configuration
     */
    void State::StatePrivate::update(Configuration pCurrent)
    {
        Q_Q(State);

        // Correspondance niveau �tats, si tous les �tats requis pour
        // l'activation sont pr�sents dans la configuration actuelle.
        QBitArray&  lCurrentStates  = pCurrent.first;
        QBitArray&  lActiveStates   = mActiveConfiguration.first;
        bool        lMatchingStates = ((lCurrentStates & lActiveStates) == lActiveStates);

        if (mNoDocument)
        {
            if (mActive != lMatchingStates)
            {
                mActive = lMatchingStates;
                emit q->changes(mActive);
            }
        }
        else
        {
            // Correspondance niveau type de document, si un des types requis
            // pour l'activation est pr�sent dans la configuration actuelle.
            QBitArray&  lCurrentDocType  = pCurrent.second;   // Il n'y a qu'un seul type de document actif.
            QBitArray&  lActiveDocTypes  = mActiveConfiguration.second;
            bool        lMatchingDocType = ((lCurrentDocType & lActiveDocTypes) != QBitArray(lActiveDocTypes.size(), false));

            bool        lNewActive = lMatchingStates & lMatchingDocType;

            //if (active != newActive)
            // Tant pis pour l'optimisation : probl�me de synchronisation � cause de cette �valuation
            // paresseuse (�tat d�j� faux avant une autre demande de d�sactivation; les objets
            // synchronis�s n'�taient donc pas d�sactiv�s comme ils auraient d� l'�tre).
            {
                mActive = lNewActive;
                emit q->changes(mActive);
            }
        }
    }


//------------------------------------------------------------------------------
//                      State : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pActiveConfig Configuration pour laquelle activer l'instance
        @param pParent       Machine � �tats "portant" cette instance
        @note Un �tat est d�sactiv� par d�faut
     */
    State::State(Configuration pActiveConfig, StateMachine* pParent)
        : QObject(pParent)
        , pthis(this, pActiveConfig)
    {
        Q_D(State);

        Q_ASSERT_X(pParent != nullptr,
                   "State::State(Configuration pActiveConfig, StateMachine* pParent)",
                   "State instances must have a StateMachine parent");

        d->init();
    }


    /*! Destructeur.
     */
    State::~State() { }


//------------------------------------------------------------------------------
//                             State : Autre m�thode
//------------------------------------------------------------------------------

    /*! Met � jour l'�tat d'activation de l'instance. Si celui-ci change, le signal changes() sera �mis;
        son param�tre sera le nouvel �tat d'activation
        @param pCurrentConfig Nouvelle configuration de la machine � �tats
     */
    void State::update(Configuration pCurrentConfig)
    {
        Q_D(State);

        d->update(pCurrentConfig);
    }


//------------------------------------------------------------------------------
//                                State : Signaux
//------------------------------------------------------------------------------

    /*! @fn void State::changes(bool pEnabled)
        Signal �mis lorsque l'�tat change.
        @param pEnabled Vrai si l'�tat devient actif, Faux sinon
     */
}
