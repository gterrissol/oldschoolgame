/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Translate.hh"

/*! @file IDE/CORE/Translate.cc
    @brief M�thodes (non-inline) de la classe CORE::Translated.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 21 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Assert.hh"

#include <QCoreApplication>
#include <QObject>
#include <QString>

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Translated::Translated()
        : mFrom()
        , mContext(nullptr)
    { }


    /*!
     */
    Translated::~Translated() { }


    /*!
     */
    Translated::Updater Translated::translater()
    {
        if (mContext != nullptr)
            return buildTranslater();
        else
            return [](){};
    }


    /*!
     */
    void Translated::from(QObject* pObject)
    {
        mFrom    = pObject->objectName();
        mContext = pObject->metaObject()->className();
    }


    /*!
     */
    QString Translated::fromName() const
    {
        return mFrom;
    }


    /*!
     */
    QString Translated::translate(QString pText) const
    {
        return translate(qPrintable(pText));
    }


    /*!
     */
    QString Translated::translate(const char* pText) const
    {
        return QCoreApplication::translate(mContext, pText);
    }
}
