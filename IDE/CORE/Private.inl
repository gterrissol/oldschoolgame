/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/CORE/Private.inl
    @brief M�thodes inline de la classe CORE::QPrivate.
    @author @ref Guillaume_Terrissol
    @date 13 Janvier 2008 - 6 F�vrier 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QtGlobal>

namespace CORE
{
//------------------------------------------------------------------------------
//                          Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @param pThat Pointeur sur instance propri�taire
        @param pArgs Arguments � passer au constructeur de TPriv
        @tparam TArgs Type(s) automatique(s) (vive les templates variadiques)
     */
    template<class TQ, class TPriv>
    template<typename... TArgs>
    Private<TQ, TPriv>::Private(TQ* pThat, TArgs&&... pArgs)
        : mPointer(new TPriv{pThat, std::forward<TArgs>(pArgs)...})
    { }


    /*! 
     */
    template<class TQ, class TPriv>
    Private<TQ, TPriv>::~Private() noexcept
    {
        delete mPointer;
    }


//------------------------------------------------------------------------------
//                               Acc�s au pointeur
//------------------------------------------------------------------------------

    /*! @return Le pointeur encapsul�
     */
    template<class TQ, class TPriv>
    TPriv* Private<TQ, TPriv>::operator->() const noexcept
    {
        Q_ASSERT_X(mPointer != nullptr, "T& Private<TQ, TPriv>::operator*() const noexcept", "Invalid mPointer");

        return mPointer;
    }


    /*! @return Le pointeur encapsul�
     */
    template<class TQ, class TPriv>
    TPriv* Private<TQ, TPriv>::get() const noexcept
    {
        return mPointer;
    }
}
