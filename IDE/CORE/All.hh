/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_ALL_HH
#define CORE_ALL_HH

/*! @file IDE/CORE/All.hh
    @brief Interface publique du module @ref CORE.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2012 - 8 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CORE
{
    /*! @namespace CORE
        @version 0.2

        Composants de base de l'IDE.
     */

    /*! @defgroup CORE CORE : Coeur de l'IDE.
        <b>namespace</b> CORE.
     */
}

#include "Category.hh"
#include "Container.hh"
#include "Display.hh"

#endif  // De CORE_ALL_HH
