/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "TrService.hh"

/*! @file IDE/CORE/TrService.cc
    @brief M�thodes (non-inline) de la classe CORE::TrService.
    @author @ref Guillaume_Terrissol
    @date 9 D�cembre 2012 - 20 Juin 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QApplication>
#include <QTranslator>

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    TrService::TrService()
        : OSGi::Service()
        , mQmFiles()
        , mTranslators()
    { }


    /*!
     */
    TrService::~TrService() { }


//------------------------------------------------------------------------------
//  Interface
//------------------------------------------------------------------------------

    /*!
     */
    void TrService::checkIn(QString pQm)
    {
        QRegExp lLocaleRE(R"(.*_([a-z]{2}(_([A-Z]{2}))?)\.qm)");
        QRegExp lLocaleQm(R"((.*)_([a-z]{2})(_([A-Z]{2}))?\.qm)");

        QString lLocale = lLocaleRE.exactMatch(pQm) ? lLocaleRE.cap(1) : "";
        QString lQm     = lLocaleQm.exactMatch(pQm) ? lLocaleQm.cap(1) : "";

        if (!lLocale.isEmpty() && !lQm.isEmpty())
        {
            mQmFiles[lLocale].append(lQm);
        }
    }


    /*!
     */
    void TrService::set(QString pLocale)
    {
        // Supprime les traducteurs actuels.
        for(auto lTr : mTranslators)
        {
            qApp->removeTranslator(lTr);
        }
        // Les r�installe (si des traductions existent pour la locale, sinon ce sont les textes par d�faut qui seront affich�s).
        for(auto lQm : mQmFiles[pLocale])
        {
            load(lQm, pLocale);
        }
    }


//------------------------------------------------------------------------------
//                                Impl�mentation
//------------------------------------------------------------------------------

    /*!
     */
    bool TrService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    /*!
     */
    std::string TrService::typeName() const
    {
        return typeid(*this).name();
    }


    /*!
     */
    void TrService::load(QString pQm, QString pLocale)
    {
        QTranslator*    lTranslator = mTranslators[pQm];
        if (lTranslator == nullptr)
        {
            mTranslators[pQm] = lTranslator = new QTranslator(qApp);
        }
        qApp->installTranslator(lTranslator);
        lTranslator->load(QString("%1_%2.qm").arg(pQm).arg(pLocale));
    }
}
