/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GameService.hh"

/*! @file IDE/CORE/GameService.cc
    @brief M�thodes (non-inline) de la classe CORE::GameService.
    @author @ref Guillaume_Terrissol
    @date 7 D�cembre 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "PROGram/Hardware.hh"

#include "SyncService.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                             GameService : P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de CORE::GameService.
        @version 0.5
     */
    class GameService::GameServicePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(GameService)
    public:
        GameServicePrivate(GameService* pThat, OSGi::Context* pContext);    //!< Constructeur.

        OSGi::Context*  mContext;                                           //!< Contexte d'ex�cution.
        PROG::Hardware  mHardware;                                          //!< "Composants hardware".
        PROG::Game::Ptr mGameEngine;                                        //!< Moteur de jeu.
    };


    /*! @param pThat    Service dont l'instance est le p-impl.
        @param pContext Contexte d'ex�cution.
     */
    GameService::GameServicePrivate::GameServicePrivate(GameService* pThat, OSGi::Context* pContext)
        : q{pThat}
        , mContext{pContext}
        , mHardware{}
        , mGameEngine{}
    { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    //!
    GameService::GameService(OSGi::Context* pContext)
        : pthis{this, pContext}
    { }


    //!
    GameService::~GameService()
    {
        halt();
    }


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*! Initialise le moteur de jeu.
        @param pWinId ID de la fenere � utiliser pour l'affichage
        @return Le "moteur de jeu"
     */
    PROG::Game::Ptr GameService::boot(void* pWinId)
    {
        if (pthis->mGameEngine.expired() && (pWinId != nullptr))
        {
            pthis->mGameEngine = pthis->mHardware.boot(pWinId);
            pthis->mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync")->perform("boot");
        }
        return pthis->mGameEngine;
    }


    /*! Arr�te le moteur de jeu et ses "composants hardware".
     */
    void GameService::halt()
    {
        pthis->mContext->services()->findByTypeAndName<SyncService>("fr.osg.ide.sync")->perform("shutdown");
        pthis->mHardware.halt();
    }


    /*! @return L'univers de jeu du @b pak file ouvert
     */
    UNI::Universe::Ptr GameService::universe()
    {
        auto    lGameEngine = pthis->mGameEngine.lock();
        if (lGameEngine)
        {
            return lGameEngine->gameUniverse();
        }
        else
        {
            return {};
        }
    }


    /*! @return Le gestionnaire de g�om�tries du @b pak file ouvert.
     */
    GEO::GeometryMgr::Ptr GameService::geometries()
    {
        auto    lGameEngine = pthis->mGameEngine.lock();
        if (lGameEngine)
        {
            return lGameEngine->geometries();
        }
        else
        {
            return {};
        }
    }


    /*!
     */
    SHD::Library::Ptr GameService::shaders()
    {
        auto    lGameEngine = pthis->mGameEngine.lock();
        if (lGameEngine)
        {
            return lGameEngine->shaders();
        }
        else
        {
            return {};
        }

    }

    /*!
     */
    void GameService::resizeViewport(const QSize& pSize)
    {
        pthis->mHardware.rcntl(PROG::Hardware::eResizeView, { I32{pSize.width()}, I32{pSize.height()} });
    }


//------------------------------------------------------------------------------
//                                Implementation
//------------------------------------------------------------------------------

    //!
    bool GameService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }


    //!
    std::string GameService::typeName() const
    {
        return typeid(*this).name();
    }
}
