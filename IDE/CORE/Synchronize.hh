/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_SYNCHRONIZE_HH
#define CORE_SYNCHRONIZE_HH

#include "CORE.hh"

/*! @file IDE/CORE/Synchronize.hh
    @brief En-t�te des classes CORE::Synchronize & CORE::Enabler.
    @author @ref Guillaume_Terrissol
    @date 20 Juin 2013 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QString>

namespace CORE
{
    /*! @brief D�finition d'un �tat de synchronisation.
        @version 0.5
     */
    class Synchronizer
    {
    public:
        //! @name Constructeurs
        //@{
                        Synchronizer() = default;
                        Synchronizer(QString pName);
        Synchronizer&   enter(QString pEnter);
        Synchronizer&   leave(QString pLeave);
        //@}
        QString state() const;
        QString enter() const;
        QString leave() const;

    private:

        QString mState;
        QString mEnter;
        QString mLeave;
    };


    /*! @brief Action d'activation initiale.
        @version 0.5
     */
    class Enabler
    {
    public:
        //! @name Constructeurs
        //@{
                Enabler() = default;
                Enabler(QString pName);
        //@}
        QString name() const;

    private:

        QString mName;
    };


    /*! @brief El�ment � synchroniser.
        @version 0.5

        Une instance de cette classe fournit sa configuration d'activation pour SyncService.
     */
    class Synchronized
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Synchronized(QString pConfiguration);   //!< Constructeur.
virtual         ~Synchronized();                        //!< Destructeur.
        //@}
        //! @name Configuration
        //@{
        QString config() const;                         //!< R�cup�ration.

    private:

        QString mConfig;                                //!< Valeur.
        //@}
    };
}

#endif  // CORE_SYNCHRONIZE_HH
