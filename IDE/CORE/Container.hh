/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_CONTAINER_HH
#define CORE_CONTAINER_HH

#include "CORE.hh"

/*! @file IDE/CORE/Container.hh
    @brief En-t�te de la classe CORE::Container.
    @author @ref Guillaume_Terrissol
    @date 9 D�cembre 2012 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <functional>

#include <QMainWindow>
#include <QMap>
#include <QVector>

#include <OSGi/Context.hh>

#include "EDITion/Subject.hh"

#include "Menu.hh"
#include "Synchronize.hh"
#include "Translate.hh"

namespace CORE
{
    /*! @brief Conteneur.
        @version 0.2

        Fen�tre principale pour une cat�gorie de donn�es.
     */
    class Container : public QMainWindow
    {
        Q_OBJECT
    public:

        using Widget = std::unique_ptr<QWidget>;

        //! @name Constructeur & destructeur
        //@{
                        Container(QString pCategory, QString pCaption, QString pIcon, OSGi::Context* pContext); //!< Constructeur.
virtual                 ~Container();                                                                           //!< Destructeur.
        //@}
        void            setup(const QStringList& pWidgetNames);

        QString         category() const;
        QString         caption() const;
        QString         icon() const;
        //! @name Ajout d'�l�ments
        //@{
        void            add(const Menu&     pData);
        void            add(const MenuItem& pData);
        void            add(Translated::Updater pTranslater);
        //@}


    protected:

        void            buildMenu(QString pName);
virtual void            addWidget(Widget pWidget);
        void            changeEvent(QEvent* pEvent);

        OSGi::Context*  context() const;

        void            setUndoRedoManager();


    protected slots:
        //! @name Slots
        //@{
        void            onLanguageChange();                                     //!< Changement de langue.

    private slots:

        void            undoRedoDone(int pUndo, int pRedo);                     //!< Mise � jour apr�s un undo/redo.
        //@}

    private:

        void            calledMenu(QString pItem);
        void            calledMenu(QString pItem, bool pIsChecked);

        QStringList     menuNames();
virtual QStringList     sortWidgets(const QStringList& pNames) const;   //!< Tri des widgets.

        OSGi::Context*                      mContext;

        QVector<Menu>                       mMenuData;      // Liste des menus : nom / titre.
        QMap<QString, QVector<MenuItem>>    mMenuItemData;  // Menu[items];

        QList<Translated::Updater>          mTranslaters;
        QString                             mCategory;
        QString                             mCaption;
        QString                             mIcon;
        EDIT::SubjectManager*               mUndoRedo;
        int                                 mUndoCount;
        int                                 mRedoCount;
    };
}

#endif  // CORE_CONTAINER_HH
