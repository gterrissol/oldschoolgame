/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Activator.hh"

/*! @file IDE/CORE/Activator.cc
    @brief M�thodes (non-inline) de la classe CORE::Activator.
    @author @ref Guillaume_Terrissol
    @date 17 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QResource>

#include <OSGi/Context.hh>

#include "ERRor/Assert.hh"

#include "TrService.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
    /*!
     */
    IDEActivator::IDEActivator(std::string pBundleName)
        : mBundleName(pBundleName)
        , mBundlePath()
    { }


    /*!
     */
    IDEActivator::~IDEActivator() { }


    /*!
     */
    QString IDEActivator::bundleName() const
    {
        return QString::fromStdString(mBundleName);
    }


    /*!
     */
    QString IDEActivator::bundlePath() const
    {
        return mBundlePath;
    }


    /*!
     */
    void IDEActivator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting " << mBundleName << '\n';
        mBundlePath = QString::fromStdString(pContext->temporaryPath() + "/" + mBundleName + "/");

        checkInServices(pContext);
        checkInComponents(pContext);

        checkInQtFiles(pContext);
    }


    /*!
     */
    void IDEActivator::doStop(OSGi::Context* pContext)
    {
        pContext->out() << "Stopping " << mBundleName << '\n';
        checkOutQtFiles(pContext);

        checkOutComponents(pContext);
        checkOutServices(pContext);

        mBundlePath.clear();
    }


    /*!
     */
    void IDEActivator::checkInServices(OSGi::Context*) { }


    /*!
     */
    void IDEActivator::checkOutServices(OSGi::Context*) { }


    /*!
     */
    void IDEActivator::checkInComponents(OSGi::Context*) { }


    /*!
     */
    void IDEActivator::checkOutComponents(OSGi::Context*) { }


    /*!
     */
    void IDEActivator::checkInQtFiles(OSGi::Context* pContext)
    {
        // Fichiers de traduction.
        auto lTrService = pContext->services()->findByTypeAndName<CORE::TrService>("fr.osg.ide.tr");
        ASSERT_EX(lTrService, "Translation service should have been registered.", return;)
        if (lTrService)
        {
            std::string lQmFiles = pContext->properties(mBundleName)->get("qm", "");
            if (!lQmFiles.empty())
            {
                for(auto lQm : QString::fromStdString(lQmFiles).split(",", QString::SkipEmptyParts))
                {
                    lTrService->checkIn(bundlePath() + lQm);
                }
            }
        }

        // Ressources graphiques.
        std::string lRcc    = pContext->properties(mBundleName)->get("rcc", "");
        if (!lRcc.empty())
        {
            QResource::registerResource(bundlePath() + lRcc.c_str());
        }
    }


    /*!
     */
    void IDEActivator::checkOutQtFiles(OSGi::Context* pContext)
    {
        std::string lRcc    = pContext->properties(mBundleName)->get("rcc", "");
        if (!lRcc.empty())
        {
            QResource::unregisterResource(bundlePath() + lRcc.c_str());
        }
    }
}
