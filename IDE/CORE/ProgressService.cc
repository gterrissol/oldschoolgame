/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "ProgressService.hh"

/*! @file IDE/CORE/ProgressService.cc
    @brief M�thodes (non-inline) de la classe CORE::ProgressService.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 214 - 10 Ao�t 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QApplication>
#include <QProgressDialog>
#include <QSet>
#include <QTime>

#include "Container.hh"
#include "STL/Function.hh"

namespace CORE
{
//------------------------------------------------------------------------------
//                       Barre : Classes d'Impl�mentation
//------------------------------------------------------------------------------

    /*! @brief Impl�mentation d'une barre de progression.
        @version 0.8
     */
    class BarImpl : public ProgressService::Bar
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                BarImpl(QString pName, int pBegin, int pEnd);   //!< Constructeur.
virtual         ~BarImpl();                                     //!< Destructeur.
        //@}
        //! @name Impl�mentation de la barre
        //@{
        void    advance(int pValue) override;                   //!< Avancement.
        //@}
    private:

        QProgressDialog*    mBar;                               //!< Composant IHM.
    };


//------------------------------------------------------------------------------
//                             Impl�mentation : Bar
//------------------------------------------------------------------------------

    //! Par d�faut.
    ProgressService::Bar::~Bar() = default;

    //! Par d�faut.
    ProgressService::Bar::Bar() = default;


//------------------------------------------------------------------------------
//                           Impl�mentation : BarImpl
//------------------------------------------------------------------------------

    /*! @param pTitle   Titre du widget barre de progression.
        @param pBegin   Premi�re valeur de l'intervale de progression.
        @param pEnd     Derni�re valeur de l'intervale de progression.
     */
    BarImpl::BarImpl(QString pTitle, int pBegin, int pEnd)
        : Bar{}
        , mBar{}
    {
        mBar = new QProgressDialog{pTitle, QString::null, pBegin, pEnd, qApp->activeWindow(), Qt::FramelessWindowHint};
        mBar->setMinimumDuration(2000);
        mBar->setLabelText(pTitle);
        mBar->setCancelButton(nullptr);
        mBar->setWindowModality(Qt::WindowModal);
    }

    /*! Un peu de m�nage.
     */
    BarImpl::~BarImpl()
    {
        delete mBar;
    }

    /*! @param pValue Nouvelle valeur d'avancement.
     */
    void BarImpl::advance(int pValue)
    {
        mBar->setValue(pValue);
    }


//------------------------------------------------------------------------------
//                 ProgressService : Constructeur & Destructeur
//------------------------------------------------------------------------------

    //! Par d�faut.
    ProgressService::ProgressService() = default;

    //! Par d�faut.
    ProgressService::~ProgressService() = default;


//------------------------------------------------------------------------------
//                          ProgressService : Interface
//------------------------------------------------------------------------------

    /*! @param pBegin Premi�re valeur de l'intervale de progression.
        @param pEnd   Derni�re valeur de l'intervale de progression.
        @return La barre de progression cr��e d'apr�s les param�tres.
     */
    ProgressService::Bar::Ptr ProgressService::create(int pBegin, int pEnd)
    {
        return create(CORE::Container::tr("Please wait..."), pBegin, pEnd);
    }

    /*! @param pTitle Titre de la barre de progression � cr�er.
        @copydoc CORE::ProgressService::create(int pBegin, int pEnd)
     */
    ProgressService::Bar::Ptr ProgressService::create(QString pTitle, int pBegin, int pEnd)
    {
        return std::make_unique<BarImpl>(pTitle, pBegin, pEnd);
    }

//------------------------------------------------------------------------------
//                       ProgressService : Implementation
//------------------------------------------------------------------------------

    //!
    bool ProgressService::isType(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName) || Service::isType(pTypeName);
    }

    //!
    std::string ProgressService::typeName() const
    {
        return typeid(*this).name();
    }
}
