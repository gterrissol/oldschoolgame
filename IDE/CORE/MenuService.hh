/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_MENUSERVICE_HH
#define CORE_MENUSERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/MenuService.hh
    @brief En-t�te de la classe CORE::MenuService.
    @author @ref Guillaume_Terrissol
    @date 21 Juin 2013 - 8 D�cembre 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMap>
#include <QString>
#include <QVector>

#include <OSGi/Service.hh>

namespace CORE
{
    /*! @brief Service : cr�ation de menus.
        @version 0.4

        Ce service est enregistr� sous le nom @b fr.osg.ide.menu .
     */
    class MenuService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr           = std::shared_ptr<MenuService>;                 //!< Pointeur sur service.
        using Slot          = std::function<void ()>;                       //!< Callback.
        using CheckedSlot   = std::function<void (bool)>;                   //!< Callback (avec statut).
        //@}    
        //! @name Constructeur & destructeur
        //@{    
                    MenuService();                                          //!< Constructeur par d�faut.
virtual             ~MenuService();                                         //!< Destructeur.
        //@}
        //! @name Cr�ation
        //@{
        Menu        menu(QString pName);                                    //!< ... d'un menu.
        MenuItem    item(QString pMenu, QString pName);                     //!< ... d'un item de menu.
        MenuItem    separator(QString pMenu);                               //!< ... d'un s�parateur d'items.
        //@}
        //! @name Gestion des �v�nements
        //@{
        void        connect(QString pSignal, Slot pSlot);                   //!< Connexion � un signal.
        void        connect(QString pSignal, CheckedSlot pSlot);            //!< Connexion � un signal (avec statut).
        bool        connect(QString pSignal, QString pForwardSignal);       //!< Cha�nage de signaux.
        void        trigger(QString pSignal);                               //!< Envoi d'un signal
        void        trigger(QString pSignal, bool pIsChecked);              //!< Envoi d'un signal (avec statut).
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
        QMap<QString, QVector<Slot>>        mSlots;                         //!< Callbacks.
        QMap<QString, QVector<CheckedSlot>> mCheckedSlots;                  //!< Callbacks (avec statut).
    };
}

#endif  // De CORE_MENUSERVICE_HH
