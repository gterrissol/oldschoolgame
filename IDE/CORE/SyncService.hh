/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_SYNCSERVICE_HH
#define CORE_SYNCSERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/SyncService.hh
    @brief En-t�te de la classe CORE::SyncService.
    @author @ref Guillaume_Terrissol
    @date 23 Juin 2013 - 17 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QStringList>

#include <OSGi/Service.hh>

#include "StateMachine.hh"

namespace CORE
{
    /*! @brief Service : synchronisation de composants d'IHM.
        @version 0.6

        Ce service est enregistr� sous le nom @b fr.osg.ide.sync .
     */
    class SyncService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr           = std::shared_ptr<SyncService>;                 //!< Pointeur sur service.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    SyncService();                                          //!< Constructeur par d�faut.
virtual             ~SyncService();                                         //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        void    declare(const Synchronizer& pConfiguration);                //!< D�claration d'un �tat de synchronisation.
        void    bind(QObject* pObject, QString pState);                     //!< Synchronisation avec un �tat complexe.
        void    perform(QString pAction);                                   //!< Ex�cution d'une action.

        void    registerType(QString pKey);                                 //!< Enregistrement d'un type de document.
        void    open(QString pDoc);                                         //!< Ouverture d'un document.
        void    close(QString pDoc);                                        //!< Fermeture d'un document.
        void    closeAll();                                                 //!< Fermeture de tous les documents.

        void    startWith(const Enabler& pAction);                          //!< D�claration d'une action � l'initialisation.
        void    onStart();                                                  //!< Activation des actions initiales.
        void    onStop();                                                   //!< D�sactive tout.
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?


    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
        StateMachine    mSyncMachine;
        QStringList     mFirstActions;
        QStringList     mLeaves;
    };
}

#endif  // De CORE_SYNCSERVICE_HH
