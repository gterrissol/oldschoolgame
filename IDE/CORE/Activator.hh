/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_ACTIVATOR_HH
#define CORE_ACTIVATOR_HH

#include "CORE.hh"

/*! @file IDE/CORE/Activator.hh
    @brief En-t�te de la classe CORE::Container.
    @author @ref Guillaume_Terrissol
    @date 17 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Activator.hh>

#include <QString>

namespace CORE
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class IDEActivator : public OSGi::Activator
    {
    public:
                IDEActivator(std::string pBundleName);
virtual         ~IDEActivator();

        QString bundleName() const;
        QString bundlePath() const;
        
    private:

virtual void    doStart(OSGi::Context* pContext) override final;
virtual void    doStop(OSGi::Context* pContext) override final;

virtual void    checkInServices(OSGi::Context* pContext);
virtual void    checkOutServices(OSGi::Context* pContext);
virtual void    checkInComponents(OSGi::Context* pContext);
virtual void    checkOutComponents(OSGi::Context* pContext);

        void    checkInQtFiles(OSGi::Context* pContext);
        void    checkOutQtFiles(OSGi::Context* pContext);


        std::string mBundleName;
        QString     mBundlePath;
    };
}

#endif  // CORE_ACTIVATOR_HH
