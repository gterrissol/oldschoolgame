/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CORE_PROGRESSSERVICE_HH
#define CORE_PROGRESSSERVICE_HH

#include "CORE.hh"

/*! @file IDE/CORE/ProgressService.hh
    @brief En-t�te de la classe CORE::ProgressService.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 2014 - 10 Ao�t 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <OSGi/Service.hh>

#include "Private.hh"

namespace CORE
{
    /*! @brief Service : gestion des statuts de progression.
        @version 0.8

        Ce service est enregistr� sous le nom @b fr.osg.ide.progress .
     */
    class ProgressService : public OSGi::Service
    {
    public:
        //! @name Types
        //@{
        using Ptr   = std::shared_ptr<ProgressService>;                     //!< Pointeur sur service.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    ProgressService();                                      //!< Constructeur par d�faut.
virtual             ~ProgressService();                                     //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        /*! @brief Barre de progression (class d'interface).
            @version 0.5
         */
        class Bar
        {
        public:
            //! @name Type
            //@{
            using Ptr = std::unique_ptr<Bar>;                               //!< Pointeur sur barre de progression.
            //@}
    virtual         ~Bar();                                                 //!< Destructeur.
    virtual void    advance(int pValue) = 0;                                //!< Avancement.

        protected:

                    Bar();                                                  //!< Constructeur.
        };

        Bar::Ptr    create(int pBegin, int pEnd);                           //!< Lancement d'une barre de progression sans titre.
        Bar::Ptr    create(QString pTitle, int pBegin, int pEnd);           //!< Lancement d'une barre de progression.
        //@}
        //! @name Impl�mentation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< Est de type ... ?

    private:

virtual std::string typeName() const override;                              //!< Nom du type (r�el).
        //@}
    };
}

#endif  // De CORE_PROGRESSSERVICE_HH
