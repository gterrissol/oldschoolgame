/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_SYSTEM_HH
#define EDIT_SYSTEM_HH

/*! @file IDE/EDITor/System.hh
    @brief En-t�te de la classe EDIT::System.
    @author @ref Guillaume_Terrissol
    @date 18 Mai 2007 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/MemoryMgr.hh"
#include "QPaNeL/Editor.hh"
#include "QT/Observer.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                              Editeur d'Univers
//------------------------------------------------------------------------------

    /*! @brief Interface affichant les ressources syst�me.
        @version 0.5
     */
    class System : public QPNL::QGenericEditor<MEM::MemoryMgr>
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    System();                                  //!< Constructeur par d�faut.
static  QTAB::QTab* builder();                                  //!< Construction d'une instance sous forme d'onglet.
virtual             ~System();                                 //!< Destructeur.
        //@}

    protected:
        //! @name Changement de langue
        //@{
virtual void        onLanguageChange();                         //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private slots:

        void        refresh();                                  //!< Mise � jour.


    private:
        //! @name Pertinence du sujet
        //@{
virtual bool        canManage(ControllerPtr controller) const; //!< Possibilit� de gestion d'un sujet.
virtual void        manage(ControllerPtr controller);          //!< Gestion effective d'un sujet.
        //@}
        Q_DISABLE_COPY(System)
        Q_CUSTOM_DECLARE_PRIVATE(System)
    };
}

#endif  // De EDIT_SYSTEM_HH
