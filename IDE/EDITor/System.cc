/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "System.hh"

/*! @file IDE/EDITor/System.cc
    @brief M�thodes (non-inline) des classes EDIT::System.
    @author @ref Guillaume_Terrissol
    @date 18 Mai 2007 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QLayout>
#include <QPainter>
#include <QTimer>

#include "QT/MainWindow.hh"
#include "UI/ResourceViewer.hh"

SPECIALIZE_EDITOR(MEM::MemoryMgr, QT_TR_NOOP("Ressources syst�me"), QT_TR_NOOP("Utilisation des ressources syst�me"), ":/icons/memory")

namespace
{
    //! @name Pixmaps pour les brosses Qt
    //@{
    const char* const gLevelOnXPM[] =
    {
        "1 3 2 1",
        " 	c #000000",
        "+	c #40FF40",
        "+",
        " ",
        " "
    };  //!< Niveau de m�moire "allum�".

    const char* const gLevelOffXPM[] =
    {
        "1 3 2 1",
        " 	c #000000",
        "-	c #006000",
        "-",
        " ",
        " "
    };  //!< Niveau de m�moire "�teint".

    const int kCurveBackgroundAnimationStep = 5;  //!< Nombre d'�tapes d'animation pour les courbes.

    const char* const gBrushGridXPM[kCurveBackgroundAnimationStep][13] =
    {
        {
            "10 10 2 1",
            " 	c #000000",
            "+	c #006000",
            "++++++++++",
            "+         ",
            "+         ",
            "+         ",
            "+         ",
            "+         ",
            "+         ",
            "+         ",
            "+         ",
            "+         "
        },
        {
            "10 10 2 1",
            " 	c #000000",
            "+	c #006000",
            "++++++++++",
            "        + ",
            "        + ",
            "        + ",
            "        + ",
            "        + ",
            "        + ",
            "        + ",
            "        + ",
            "        + "
        },
        {
            "10 10 2 1",
            " 	c #000000",
            "+	c #006000",
            "++++++++++",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   ",
            "      +   "
        },
        {
            "10 10 2 1",
            " 	c #000000",
            "+	c #006000",
            "++++++++++",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     ",
            "    +     "
        },
        {
            "10 10 2 1",
            " 	c #000000",
            "+	c #006000",
            "++++++++++",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       ",
            "  +       "
        }
    };  //!< Fond "anim�" pour le rendu des courbes.
    //@}
}

namespace EDIT
{
//------------------------------------------------------------------------------
//                      Syst�me de Courbes pour Historique
//------------------------------------------------------------------------------

    /*! @brief Affichage de courbes (t, f<i><small>n</small></i>(t)).
        @version 0.5
     */
    class Curves : public QWidget
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                Curves(QWidget* pParent);                      //!< Constructeur.
virtual         ~Curves();                                     //!< Destructeur.
        //@}
virtual bool    eventFilter(QObject* pWatched, QEvent* pEvent); //!< Filtre d'�v�nements.
        //! @name Gestion des courbes
        //@{
        int     addCurve(QString pZName, QColor poColor);       //!< Ajout d'une courbe � afficher.
        void    setMaximum(int pId, int pMax);                  //!< Maximum th�orique des valeurs d'une courbe.
        void    setCurveData(int pId, int pValue);              //!< D�finition d'un point d'une courbe.
        //@}

    protected:

virtual void    resizeEvent(QResizeEvent* pEvent);              //!< Traitement d'un �v�nement de redimensionnement.


    private:
        //! @name Gestion des courbes
        //@{
        void    setVisibleSampleCount(int pCount);              //!< Nombre de points des courbes � afficher.
        void    drawCurves();                                   //!< Affichage des courbes.
        //@}
        //! @name Composants de l'interface
        //@{
        QLabel*                 mCurveNamesLabel;               //!< Etiquette pr�sentant les noms des courbes.
        QWidget*                mCurves;                        //!< Widget affichant les courbes.
        //@}
        //! @name donn�es
        //@{
        QStringList             mCurveNames;                    //!< Noms des courbes.
        QVector<QColor>         mCurveColors;                   //!< Couleurs des courbes.
        QVector<QList<int> >    mCurveData;                     //!< Donn�es des courbes.
        QVector<int>            mMaxima;                        //!< Amplitudes des courbes.
        int                     mCount;                         //!< Nombre d'�chantillons � afficher.
        //@}
    };


//------------------------------------------------------------------------------
//                        Affichage du Niveau de M�moire
//------------------------------------------------------------------------------

    /*! @brief Affichage du niveau de m�moire.
        @version 0.5
     */
    class MemoryLeveling : public QWidget
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                MemoryLeveling(QWidget* pParent);  //!< Constructeur.
virtual         ~MemoryLeveling();                 //!< Destructeur.
        //@}
        //! @name Gestion du niveau
        //@{
        void    setMaximum(int pMax);               //!< Niveau maximum.
        void    setLevel(int pCurrent);             //!< D�finition du niveau actuel.
        //@}

    protected:

virtual void    paintEvent(QPaintEvent* pEvent);    //!< Rafra�chissement.


    private:

        float   mMaximum;                           //!< Niveau maximum.
        float   mPercent;                           //!< Niveau actuel (pourcentage).
    };


//------------------------------------------------------------------------------
//                      Curves : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Widget parent
     */
    Curves::Curves(QWidget* pParent)
        : QWidget(pParent)
        , mCurveNamesLabel(nullptr)
        , mCurves(nullptr)
        , mCurveNames()
        , mCurveColors()
        , mCurveData()
        , mMaxima()
        , mCount(0)
    {
        setAutoFillBackground(true);
        setPalette(QPalette(Qt::black));

        mCurveNamesLabel    = new QLabel(this);
        mCurveNamesLabel->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
        mCurves             = new QWidget(this);
        mCurves->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

        QHBoxLayout*    lLayout = new QHBoxLayout(this);
        lLayout->setMargin(0);
        lLayout->setSpacing(0);
        lLayout->addWidget(mCurveNamesLabel);
        lLayout->addWidget(mCurves, 1);

        mCurves->installEventFilter(this);

        setVisibleSampleCount(width() / 2);
    }


    /*! Destructeur.
     */
    Curves::~Curves() { }


//------------------------------------------------------------------------------
//                        Curves : Filtrage d'Ev�nements
//------------------------------------------------------------------------------

    /*! Filtre les �v�nements de widgets "surveill�s.<br>
        Plus pr�cis�ment, s'occupe d'intercepter les �v�nements de rafra�chissment de mCurves pour
        s'occuper directement de l'affichage de courbes.
        @param pWatched Objet surveill�
        @param pEvent   Ev�nement � traiter
        @return VRAI si <i>pEvent</i> �tait une demande de rafra�chissement de mCurves, la valeur de
        retour QWidget::eventFilter sinon
     */
    bool Curves::eventFilter(QObject* pWatched, QEvent* pEvent)
    {
        if ((pWatched == mCurves) && (pEvent->type() == QEvent::Paint))
        {
            if (mCurves->isEnabled())
            {
                drawCurves();
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return QWidget::eventFilter(pWatched, pEvent);
        }
    }


//------------------------------------------------------------------------------
//                         Curves : Gestion des Courbes
//------------------------------------------------------------------------------

    /*! D�finit une courbe nouvelle courbe � afficher.
        @param pName  Nom de la courbe
        @param pColor Couleur de la courbe
        @return L'identifiant de la nouvelle courbe
     */
    int Curves::addCurve(QString pName, QColor pColor)
    {
        // Ajout des informations dans tous les tableaux.
        mCurveNames.push_back(pName);
        mCurveColors.push_back(pColor);
        mCurveData.push_back(QList<int>());
        mMaxima.push_back(100);

        // Recompose le texte de l'�tiquette pr�sentant les courbes.
        QString lCurves = "<qt>";
        for(int lCurve = 0; lCurve < mCurveNames.size(); ++lCurve)
        {
            lCurves += QString("<font color=%1>%2</font>").arg(mCurveColors[lCurve].name()).arg(mCurveNames[lCurve]);
            if (lCurve < mCurveNames.size() - 1)
            {
                lCurves += "<br>";
            }
        }
        lCurves += "</qt>";
        mCurveNamesLabel->setText(lCurves);
        mCurveNamesLabel->setMinimumSize(QSize(1, mCurveNames.size() * mCurveNamesLabel->fontMetrics().height()));

        return mCurveNames.size() - 1;
    }


    /*! D�finit le maximum th�orique pour les donn�es d'une courbe.
        @param pId  Identifiant de la courbe
        @param pMax Valeur maximale
     */
    void Curves::setMaximum(int pId, int pMax)
    {
        mMaxima[pId] = pMax;
    }


    /*! Ajoute une valeur � une courbe.
        @param pId    Identifiant de la courbe
        @param pValue Nouvelle valeur
     */
    void Curves::setCurveData(int pId, int pValue)
    {
        if (mCurveData[pId].size() == mCount)
        {
            mCurveData[pId].pop_front();
        }
        mCurveData[pId].push_back(pValue);

        mCurves->update();
    }


//------------------------------------------------------------------------------
//                       Curves : Traitement d'Ev�nements
//------------------------------------------------------------------------------

    /*! Red�finit le nombre d'�chantillons utilis�s pour tracer les courbes.
        @param pEvent Ev�nement de redimensionnement
     */
    void Curves::resizeEvent(QResizeEvent* pEvent)
    {
        QWidget::resizeEvent(pEvent);

        setVisibleSampleCount(width() / 2);
    }


//------------------------------------------------------------------------------
//                         Curves : Gestion des Courbes
//------------------------------------------------------------------------------

    /*! D�finit combien d'�chantillons sont conserv�s, au maximum, pour chaque courbe et donc, de combien
        de points seront compos�es les courbes.
        @param pCount Nombre de points maximum des courbes
     */
    void Curves::setVisibleSampleCount(int pCount)
    {
        mCount = pCount;
        for(int lCurve = 0; lCurve < mCurveData.size(); ++lCurve)
        {
            while(mCount < mCurveData[lCurve].size())
            {
                mCurveData[lCurve].pop_front();
            }
        }
    }


    /*! Affiche les courbes, sur un fond quadrill� (et anim� par un d�placement lat�ral).
     */
    void Curves::drawCurves()
    {
        QPainter    lPainter(mCurves);

        // Fond quadrill�.
        static int sBackground = 0;
        lPainter.setPen(QPen(Qt::SolidLine));
        lPainter.fillRect(mCurves->rect(), QBrush(QPixmap(gBrushGridXPM[sBackground++ % kCurveBackgroundAnimationStep])));

        // Courbes.
        float   lHeight = mCurves->height();
        float   lWidth  = mCurves->width();
        float   lHStep  = lWidth / mCount;
        for(int lCurve = 0; lCurve < mCurveData.size(); ++lCurve)
        {
            const QList<int>&   lList = mCurveData[lCurve];
            int                 lCount  = lList.size() - 1;

            if (1 <= lCount)
            {
                lPainter.setPen(QPen(mCurveColors[lCurve]));

                float   lVStep  = lHeight / mMaxima[lCurve];
                float   lStart  = (mCount - lCount) * lHStep;
                QPointF lPrev   = QPointF(lStart, lHeight - lList[0] * lVStep);

                for(int lStep = 1; lStep <= lCount; ++lStep)
                {
                    int lData   = lList[lStep];
                    QPointF lCurrent = QPointF(lStart + lStep * lHStep, lHeight - lData * lVStep);
                    if (lData != -1)
                    {
                        lPainter.drawLine(lPrev, lCurrent);
                    }
                    lPrev = lCurrent;
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                 Memory Leveling : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Widget parent
     */
    MemoryLeveling::MemoryLeveling(QWidget* pParent) : QWidget(pParent), mPercent(0.0F)
    {
        setAutoFillBackground(true);
        setPalette(QPalette(Qt::black));
    }


    /*! Destructeur.
     */
    MemoryLeveling::~MemoryLeveling() { }


//------------------------------------------------------------------------------
//                      Memory Leveling : Gestion du Niveau
//------------------------------------------------------------------------------

    /*! @param pMax Niveau maximum th�orique
     */
    void MemoryLeveling::setMaximum(int pMax)
    {
        mMaximum = pMax;
    }


    /*! D�finition du niveau � repr�senter.
        @param pCurrent Niveau actuel
     */
    void MemoryLeveling::setLevel(int pCurrent)
    {
        mPercent = pCurrent / mMaximum;

        if      (mPercent < 0.0F)
        {
            mPercent = 0.0F;
        }
        else if (1.0F < mPercent)
        {
            mPercent = 1.0F;
        }

        update();
    }


//------------------------------------------------------------------------------
//                        Memory Leveling : Autre M�thode
//------------------------------------------------------------------------------

    /*! Rafra�chit le widget.
     */
    void MemoryLeveling::paintEvent(QPaintEvent* /*pEvent*/)
    {
        const int   lMargin = 3;
        QPoint      lTL(x() + lMargin,         y() + lMargin);
        QPoint      lMM(width() / 2,           int((1.0F - mPercent) * (height() - 2 * lMargin)));
        QPoint      lBR(width() - lMargin - 1, height() - lMargin);

        QPainter    lPainter(this);

        lPainter.setPen(QPen(Qt::SolidLine));
        lPainter.fillRect(QRect(QPoint(lTL.x(),               lTL.y()), QPoint(lMM.x(), lMM.y())), QBrush(QPixmap(gLevelOffXPM)));
        lPainter.fillRect(QRect(QPoint(lMM.x() + lMargin - 1, lTL.y()), QPoint(lBR.x(), lMM.y())), QBrush(QPixmap(gLevelOffXPM)));

        lPainter.fillRect(QRect(QPoint(lTL.x(),               lMM.y()), QPoint(lMM.x(), lBR.y())), QBrush(QPixmap(gLevelOnXPM)));
        lPainter.fillRect(QRect(QPoint(lMM.x() + lMargin - 1, lMM.y()), QPoint(lBR.x(), lBR.y())), QBrush(QPixmap(gLevelOnXPM)));
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de EDIT::System.
        @version 0.5
     */
    class System::SystemPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(System)
    public:
        //! @name Constrcuteur & initialisation
        //@{
                        SystemPrivate(System* pParent);   //!< Constructeur.
 static QTAB::QPage*    create();                           //!< Cr�ation d'un onglet.
        void            init();                             //!< Initialisation.
        //@}
        //! @name Composants d'interface
        //@{
        Ui::ResourceViewer  mUI;                            //!< Interface cr��e par Designer.
        MemoryLeveling*    mLeveling;                      //!< Niveau de m�moire.
        Curves*            mMemoryCurves;                  //!< Historique de la m�moire.
        //@}
        QTimer*             mTimer;                         //!< Timer pour le rafra�chissement.
    };


//------------------------------------------------------------------------------
//                System Private : Constrcuteur & Initialisation
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Objet dont cette instance est l'impl�mentation priv�e
     */
    System::SystemPrivate::SystemPrivate(System* pParent)
        : q_custom_ptr(pParent)
        , mUI()
        , mLeveling(nullptr)
        , mMemoryCurves(nullptr)
        , mTimer(nullptr)
    { }


    /*!
     */
    QTAB::QPage* System::SystemPrivate::create()
    {
        return new System();
    }


    /*! Construit l'interface.
     */
    void System::SystemPrivate::init()
    {
        Q_Q(System);

        // Construit l'interface.
        mUI.setupUi(q);

        // Niveau de m�moire.
        QFrame*         lMemoryInfoFrame    = q->findChild<QFrame*>("memoryInfoFrame");
        mLeveling = new MemoryLeveling(lMemoryInfoFrame);
#ifdef MEMORY_CHECK
        mLeveling->setMaximum(MEM::MemoryMgr::get()->totalMemorySize());
#endif  // De MEMORY_CHECK
        QVBoxLayout*    lInfoLayout         = dynamic_cast<QVBoxLayout*>(lMemoryInfoFrame->layout());
        lInfoLayout->insertWidget(0, mLeveling, 1);
        
        // Historique de l'utilisation de la m�moire.
        QFrame*         lMemoryReportFrame  = q->findChild<QFrame*>("memoryReportFrame");
        mMemoryCurves = new Curves(lMemoryReportFrame);
        QHBoxLayout*    lReportLayout       = new QHBoxLayout(lMemoryReportFrame);
        lReportLayout->setMargin(0);
        lReportLayout->setSpacing(0);
        lReportLayout->insertWidget(0, mMemoryCurves, 1);

        mMemoryCurves->addCurve("", QColor("#00FF00"));
#ifdef MEMORY_CHECK
        mMemoryCurves->setMaximum(0, MEM::MemoryMgr::get()->totalMemorySize());
#endif  // De MEMORY_CHECK

        // Arme le rafra�chissement.
        mTimer = new QTimer(q);
        q->connect(mTimer, SIGNAL(timeout()), SLOT(refresh()));
        mTimer->start(1000);

        QT::QMainWindow::synchronize(q, "file");
    }


//------------------------------------------------------------------------------
//                      System : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur (par d�faut).
     */
    System::System()
        : QPNL::QGenericEditor<MEM::MemoryMgr>()
        , d_custom_ptr(new SystemPrivate(this))
    {
        Q_D(System);

        d->init();
    }


    /*!
     */
    QTAB::QTab* System::builder()
    {
        return new QTAB::QTab(&System::name, &System::title, QIcon(":/icons/memory"), &SystemPrivate::create);
    }


    /*! Destructeur
     */
    System::~System() { }


//------------------------------------------------------------------------------
//                         System : Changement de Langue
//------------------------------------------------------------------------------

    /*! Met � jour le widget suite � un changement de langue.
     */
    void System::onLanguageChange()
    {
        Q_D(System);

        d->mUI.retranslateUi(this);
    }


//------------------------------------------------------------------------------
//                             System : Mise � jour
//------------------------------------------------------------------------------

    /*! Rafra�chit l'interface avec les derni�res informations disponibles sur les ressources syst�me.
     */
    void System::refresh()
    {
#ifdef MEMORY_CHECK
        Q_D(System);

        // M�moire.
        unsigned long   lCurrent    = MEM::MemoryMgr::get()->usedMemorySize();
        unsigned long   lSpare      = MEM::MemoryMgr::get()->spareMemorySize();

        d->mLeveling->setLevel(lCurrent);
        d->mUI.usedMemorySizeLabel->setText(tr("<qt>%1 Mo <font color=\"#FF8000\">(%2 ko)</font></qt>").
            arg((lCurrent + 1048575)/ 1048576).
            arg((lSpare + 1023)/ 1024));

        d->mMemoryCurves->setCurveData(0, lCurrent);
#endif  // De MEMORY_CHECK
    }


//------------------------------------------------------------------------------
//                          Pertinence du Sujet
//------------------------------------------------------------------------------

    /*! Cette classe ne g�re pas de sujet.
        @treturn Toujours FAUX
     */
    bool System::canManage(ControllerPtr) const
    {
        return false;
    }


    /*! Cette classe ne g�re pas de sujet. Par cons�quent, cette m�thode ne fait rien.
     */
    void System::manage(ControllerPtr) { }
}
