/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "EDITor.hh"

/*! @file IDE/EDITor/EDITor.cc
    @brief D�finitions diverses du module EDIT.
    @author @ref Guillaume_Terrissol
    @date 12 Juin 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAGameViewInstanceWasExpected,  "Une instance de EDIT::cl_GameView �tait attendue.",    "A EDIT::cl_GameView instance was expected.")
    REGISTER_ERR_MSG(kBadIdentifier,                 "Identifiant erron�.",                                  "Bad identifier.")
    REGISTER_ERR_MSG(kInvalidPakFileName,            "Nom de pak file invalide.",                            "Invalid pak file name.")
    REGISTER_ERR_MSG(kInvalidWidget,                 "Widget invalide",                                      "Invalid widget.")
    REGISTER_ERR_MSG(kUndefinedSoftwareComponents,   "Composants logiciels non-d�finis.",                    "Undefined software components.")
}
