/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_MAINEDITOR_HH
#define EDIT_MAINEDITOR_HH

#include "EDITor.hh"

/*! @file IDE/EDITor/MainEditor.hh
    @brief En-t�te de la classe EDIT::MainEditor.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 D�cembre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "QT/MainWindow.hh"

#include "GameView.hh"

namespace EDIT
{
    /*! @brief Editeur principal.
        @version 0.5

        Cette classe impl�mente la fen�tre principale de l'�diteur : elle contient tous les autres �diteurs.
     */
    class MainEditor : public QT::QMainWindow
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                        MainEditor(QWidget* pParent = 0);  //!< Constructeur.
virtual                 ~MainEditor();                     //!< Destructeur.
        //@}

    protected:

virtual void        closeEvent(QCloseEvent* pEvent);        //!< R�action � la fermeture de la fen�tre.
virtual void        onLanguageChange();                     //!< Changement de langue.

    private:
        //! @name Gestion du projet
        //@{
virtual QString     createFile();                           //!< 
virtual bool        openFile(QString file);                 //!< 
virtual void        save();                                 //!< Enregistrement du projet.
virtual void        closeFile();                            //!< 
        //@}
        //! @name
        //@{
virtual void        declareStates();                        //!< 
virtual void        synchronizeComponents();                //!< 
virtual void        performOnStartup();                     //!< 
virtual void        customMenu(int actionId);               //!< 
        //@}
virtual void        setupAboutBox(QDialog* pDialog);        //!< D�finition du dialogue "A propos...".

        Q_DISABLE_COPY(MainEditor)
        Q_CUSTOM_DECLARE_PRIVATE(MainEditor)

      typedef GameView::ControllerPtr ControllerPtr;

    private slots:

        void        onEdit(ControllerPtr pController);     //!< 
        void        onLeave(ControllerPtr pController);    //!< 
        void        goOneFrame();                            //!< 
    };
}

#endif  // De EDIT_MAINEDITOR_HH
