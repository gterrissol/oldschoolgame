/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef EDIT_EDITOR_HH
#define EDIT_EDITOR_HH

/*! @file IDE/EDITor/EDITor.hh
    @brief En-t�te du module EDItor.
    @author @ref Vincent_David & @ref Guillaume_Terrissol
    @date 15 Ao�t 2002 - 11 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace EDIT  //! Editeurs.
{
    /*! @namespace EDIT
        @version 0.65

     */
}

#include "System.hh"
#include "GameView.hh"
#include "MainEditor.hh"

#endif  // De EDIT_EDITOR_HH
