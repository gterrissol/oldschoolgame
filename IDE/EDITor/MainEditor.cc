/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MainEditor.hh"

/*! @file IDE/EDITor/MainEditor.cc
    @brief M�thodes (non-inline) de la classe EDIT::MainEditor::MainEditorPrivate & EDIT::MainEditor.
    @author @ref Guillaume_Terrissol & @ref Vincent_David
    @date 15 D�cembre 2002 - 17 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QCloseEvent>
#include <QDockWidget>

#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QSettings>
#include <QVBoxLayout>
#include <QTimer>

#include "ERRor/Assert.hh"
#include "ERRor/ErrMsg.hh"
#include "PAcKage/Enum.hh"
#include "QPAcKage/FileDB.hh"
#include "QPaNeL/Editor.hh"
#include "QT/Application.hh"
#include "QT/WidgetSerializer.hh"
#include "QT3D/Widget.hh"
#include "QOBJect/Controller.hh"
#include "QOBJect/Editor.hh"
#include "QOBJect/ModelLibrary.hh"
#include "QOUTside/Controller.hh"
#include "QOUTside/Editor.hh"
#include "QTILe/Controller.hh"
#include "QTILe/Editor.hh"
#include "QTILe/FragmentMgr.hh"
#include "QWORld/Brushes.hh"
//#include "QUNDerworld/Controller.hh"
#include "QUNIverse/Editor.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "UI/AboutBox.hh"
#include "UNIverse/GameUniverse.hh"

#   include "PROGram/MBR.hh"
#   include "PROGram/GameSteps.hh"
#   include "DATum/Controller.tcc"

#include "ErrMsg.hh"
#include "GameView.hh"
#include "System.hh"

namespace EDIT
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    enum
    {
        eRunEngine,
        eRestartEngine,
        eStopEngine,
        eTool,
        eEditor
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief P-Impl de MainEditor.
        @version 0.4
     */
    class MainEditor::MainEditorPrivate : public QT::QWidgetPrivateSerializer<MainEditor::MainEditorPrivate>
    {
        Q_CUSTOM_DECLARE_PUBLIC(MainEditor)
    public:
        //! @name "Constructeurs"
        //@{
                    MainEditorPrivate(MainEditor* pParent);   //!< Constructeur.
                    ~MainEditorPrivate();
        void        init();                                     //!< Initialisation.
        //@}
        //! @name M�thodes associ�es aux actions (menu jeu)
        //@{
        void        startGame();                                //!< Lancement du jeu.
        void        pauseGame();                                //!< Mise en pause du jeu.
        void        resetGame();                                //!< Rem�rrage du jeu.
        void        stopGame();                                 //!< Arr�t du jeu.
        //@}
        //! @name M�thodes associ�es aux actions (menu outils)
        //@{
        void        displayTools();                             //!< 
        void        displayEditors();                           //!< 
        void        closePanels();                              //!< 
        //@}
        //! @name Menu "Moteur"
        //@{
        QMenu*                          mEngineMenu;            //!< 
        QAction*                        mRunAct;                //!< 
        QAction*                        mRestartAct;            //!< 
        QAction*                        mStopAct;               //!< 
        //@}
        //! @name Menu "Outils"
        //@{
        QMenu*                          mToolsMenu;             //!< 
        QAction*                        mToolAct;               //!< 
        QAction*                        mEditorAct;             //!< 
        //@}
        //! @name
        //@{
        std::unique_ptr<QPAK::FileDB>   mFiles;                 //!< 
        std::shared_ptr<GameView>       mGameView;              //!< 
        std::shared_ptr<QT3D::Mode>     mUniverseMode;          //!< 
        QTimer                          mTimer;                 //!< 
        bool                            mIsGamePaused;          //!< Jeu en pause ?
        //@}

    private:
        //! @name Settings de l'�diteur
        //@{
virtual void        writeCustom(QSettings& settings);           //!< Ecriture des param�tres suppl�menaires.
virtual void        readCustom(QSettings& settings);            //!< Lecture des param�tres suppl�menaires.
        //@}
virtual bool        isInherited() const;                        //!< "D�rivation" d'un autre s�rialiseur ?
 friend class QT::QWidgetPrivateSerializer<MainEditor::MainEditorPrivate>;    //!< 
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent 
     */
    MainEditor::MainEditorPrivate::MainEditorPrivate(MainEditor* pParent)
        : q_custom_ptr(pParent)
        , mEngineMenu(nullptr)
        , mRunAct(nullptr)
        , mRestartAct(nullptr)
        , mStopAct(nullptr)
        , mToolsMenu(nullptr)
        , mToolAct(nullptr)
        , mEditorAct(nullptr)
        , mFiles()
        , mGameView()
        , mUniverseMode()
        , mTimer()
        , mIsGamePaused(false)
    { }


    /*!
     */
    MainEditor::MainEditorPrivate::~MainEditorPrivate() { }


    /*! Cette m�thode est appel�e une fois que la barre et "son impl�mentation" ont �t� li�es.
     */
    void MainEditor::MainEditorPrivate::init()
    {
        Q_Q(MainEditor);

        //---------
        // Actions
        //---------
        mRunAct     = new QAction(QIcon(":/icons/play"), "", q);
        q->addActionToMenuGroup(mRunAct, eRunEngine);
        mRestartAct = new QAction(q);
        q->addActionToMenuGroup(mRestartAct, eRestartEngine);
        mStopAct    = new QAction(QIcon(":/icons/stop"), "", q);
        q->addActionToMenuGroup(mStopAct, eStopEngine);

        mToolAct    = new QAction(q);
        q->addActionToMenuGroup(mToolAct, eTool);
        mToolAct->setCheckable(true);

        mEditorAct  = new QAction(q);
        q->addActionToMenuGroup(mEditorAct, eEditor);
        mEditorAct->setCheckable(true);

        //------
        // Menu
        //------
        mEngineMenu = new QMenu(q->menuBar());
        q->insertMenu(mEngineMenu);

        mEngineMenu->addAction(mRunAct);
        mEngineMenu->addAction(mRestartAct);
        mEngineMenu->addAction(mStopAct);

        mToolsMenu  = new QMenu(q->menuBar());
        q->insertMenu(mToolsMenu);

        mToolsMenu->addAction(mToolAct);
        mToolsMenu->addAction(mEditorAct);

        q->onLanguageChange();

        q->setCorner(Qt::TopLeftCorner,     Qt::LeftDockWidgetArea);
        q->setCorner(Qt::BottomLeftCorner,  Qt::LeftDockWidgetArea);
        q->setCorner(Qt::TopRightCorner,    Qt::RightDockWidgetArea);
        q->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

        mFiles.reset(new QPAK::FileDB());

        mGameView = std::make_shared<GameView>(q);
        q->connect(mGameView.get(), SIGNAL(edit(ControllerPtr)), SLOT(onEdit(ControllerPtr)));
        q->connect(mGameView.get(), SIGNAL(leave(ControllerPtr)), SLOT(onLeave(ControllerPtr)));
        q->setMainWidget(mGameView.get());

        int iWindID = int(mGameView->winId());
#if   defined(LINUX)
        setenv("SDL_WINDOWID", QString::number(iWindID).toAscii().constData(), 1);
#elif defined(MINGW)
        _putenv(QString("SDL_WINDOWID=%1").arg(iWindID).toAscii().constData());
        _putenv(QString("SDL_VIDEODRIVER=%1").arg("windib").toAscii().constData());
#else
#   error "Can't set environment variable"
#endif  // HAVE_SETENV

        // Ic�ne et titre de la fen�tre.
        q->setWindowIcon(QIcon(":/types/resource")); // En attendant une meilleure ic�ne.
        q->setWindowTitle("IDE");

        // Le nom du widget a d�j� �t� �tabli par QT::QMainWindowPrivate.
        readSettings();

        DAT::GameEntityMgr::checkIn<UNI::GameUniverse>(UNI::Controller::make);
        DAT::GameEntityMgr::checkIn<OUT::World>(OUT::Controller::make);
        DAT::GameEntityMgr::checkIn<TILE::Tile>(TILE::Controller::make);
        DAT::GameEntityMgr::checkIn<TILE::TileRsc>(TILE::Controller::make);
        DAT::GameEntityMgr::checkIn<OBJ::GameObject>(OBJ::Controller::make);
        //DAT::GameEntityMgr::checkIn<UND::World>(UND::Controller::Make);
        mUniverseMode = mGameView->buildMode<UNI::Mode>(DAT::GenericController<UNI::Universe>::type());

        mTimer.setInterval(20);
        q->connect(&mTimer, SIGNAL(timeout()), SLOT(goOneFrame()));
    }


//------------------------------------------------------------------------------
//                   M�thodes Associ�es aux Actions (Menu Jeu)
//------------------------------------------------------------------------------

    /*! Lancement du jeu.
     */
    void MainEditor::MainEditorPrivate::startGame()
    {
        Q_Q(MainEditor);

        q->performAction("disable");
        q->performAction("run engine");

        mIsGamePaused = false;
        mGameView->start();
        mTimer.start();
    }


    /*! Mise en pause du jeu.<br>
        L'aspect "toggle" de cette entr�e de menu est g�r�e manuellement, car Qt ne semble pas faire son travail correctement.
     */
    void MainEditor::MainEditorPrivate::pauseGame()
    {
        if (mIsGamePaused)
        {
            mGameView->pause();
            mTimer.stop();
            mIsGamePaused = false;
        }
        else
        {
            mGameView->resume();
            mTimer.start();
            mIsGamePaused = false;
        }
    }


    /*! Mise en pause du jeu.
     */
    void MainEditor::MainEditorPrivate::resetGame()
    {
//        mGameView->reset();
    }


    /*! Arr�te le jeu.
     */
    void MainEditor::MainEditorPrivate::stopGame()
    {
        Q_Q(MainEditor);

        mTimer.stop();
        mGameView->stop();

        q->performAction("stop engine");
        q->performAction("enable");
    }


//------------------------------------------------------------------------------
//                 M�thodes Associ�es aux Actions (Menu Outils)
//------------------------------------------------------------------------------

    /*!
     */
    void MainEditor::MainEditorPrivate::displayTools()
    {
        Q_Q(MainEditor);

        QPNL::QEditorPanel* lToolPanel  = q->findChild<QPNL::QEditorPanel*>("ToolPanel");

        if (lToolPanel == nullptr)
        {
            lToolPanel    = new QPNL::QEditorPanel(MainEditor::tr("Outils"), "ToolPanel", q);
            q->addDockWidget(Qt::RightDockWidgetArea, lToolPanel);

            q->connect(lToolPanel, SIGNAL(available(bool)),    mToolAct,   SLOT(setChecked(bool)));
            q->connect(mGameView.get(), SIGNAL(edit(ControllerPtr)), lToolPanel, SLOT(controllerActivated(ControllerPtr)));

            // Enregistre et affiche tous les outils.
            lToolPanel->registerPage<QWOR::Brushes>();
//            lToolPanel->displayTab(QWOR::Brushes::name());
            lToolPanel->registerPage<QTIL::FragmentMgr>();
            lToolPanel->displayTab(QTIL::FragmentMgr::name());
            lToolPanel->registerPage<System>();
            lToolPanel->displayTab(System::name());
            lToolPanel->registerPage<QOBJ::ModelLibrary>();
            lToolPanel->displayTab(QOBJ::ModelLibrary::name());

lToolPanel->displayTab(QWOR::Brushes::name());

            q->connect(lToolPanel->findChild<QTIL::FragmentMgr*>(),    SIGNAL(familyAdded()),
                       lToolPanel->findChild<QWOR::Brushes*>(),        SLOT(updateEnvironmentBrushes()));
            q->connect(lToolPanel->findChild<QOBJ::ModelLibrary*>(),   SIGNAL(modelSelected(AppearancePtr)),
                       lToolPanel->findChild<QWOR::Brushes*>(),        SLOT(setDropModel(AppearancePtr)));
        }

        lToolPanel->setVisible(mToolAct->isChecked());
    }


    /*!
     */
    void MainEditor::MainEditorPrivate::displayEditors()
    {
        Q_Q(MainEditor);

        QPNL::QEditorPanel* lEditorPanel  = q->findChild<QPNL::QEditorPanel*>("EditorPanel");

        if (lEditorPanel == nullptr)
        {
            lEditorPanel  = new QPNL::QEditorPanel(MainEditor::tr("Editeurs"), "EditorPanel", q);
            q->addDockWidget(Qt::LeftDockWidgetArea, lEditorPanel);

            q->connect(lEditorPanel, SIGNAL(available(bool)),     mEditorAct,   SLOT(setChecked(bool)));
            q->connect(mGameView.get(),    SIGNAL(edit(ControllerPtr)), lEditorPanel, SLOT(controllerActivated(ControllerPtr)));

            // Enregistre et affiche tous les visualiseurs/outils.
            lEditorPanel->registerPage<QUNI::Editor>();
            lEditorPanel->displayTab(QUNI::Editor::name());
            lEditorPanel->registerPage<QOUT::Editor>();
//            lEditorPanel->displayTab(QOUT::Editor::name());
            lEditorPanel->registerPage<QTIL::Editor>();
            lEditorPanel->displayTab(QTIL::Editor::name());
            lEditorPanel->registerPage<QOBJ::Editor>();
            lEditorPanel->displayTab(QOBJ::Editor::name());

lEditorPanel->displayTab(QOUT::Editor::name());
        }

        lEditorPanel->setVisible(mEditorAct->isChecked());
    }


    /*! Cette m�thode ne doit �tre appel�e que lors de la fermeture d'un <b>pak</b> file; en effet, la fen�tre conserve l'�tat d'ouverture de
        ces panneaux gr�ce aux actions associ�es (case coch�e ou pas), il ne faut donc pas que la fermeture implicite des panneaux interf�re
        avec le m�canisme de s�rialisation. Cette m�thode se charge donc de "faire le m�nage" correctement.
     */
    void MainEditor::MainEditorPrivate::closePanels()
    {
        Q_Q(MainEditor);

        if (QPNL::QEditorPanel* lToolPanel = q->findChild<QPNL::QEditorPanel*>("ToolPanel"))
        {
            q->disconnect(lToolPanel, SIGNAL(available(bool)), mToolAct, SLOT(setChecked(bool)));
            q->removeDockWidget(lToolPanel);
            delete lToolPanel;
        }
        if (QPNL::QEditorPanel* lEditorPanel  = q->findChild<QPNL::QEditorPanel*>("EditorPanel"))
        {
            q->disconnect(lEditorPanel, SIGNAL(available(bool)), mEditorAct, SLOT(setChecked(bool)));
            q->removeDockWidget(lEditorPanel);
            delete lEditorPanel;
        }
    }


//------------------------------------------------------------------------------
//                             Settings de l'Editeur
//------------------------------------------------------------------------------

    /*!
     */
    void MainEditor::MainEditorPrivate::writeCustom(QSettings& settings)
    {
        settings.beginGroup("Panels");
        settings.setValue("tool", mToolAct->isChecked());
        settings.setValue("viewer", mEditorAct->isChecked());
        settings.endGroup();
    }


    /*!
     */
    void MainEditor::MainEditorPrivate::readCustom(QSettings& settings)
    {
        settings.beginGroup("Panels");
        mToolAct->setChecked(settings.value("tool", true).toBool());
        mEditorAct->setChecked(settings.value("viewer", false).toBool());
        settings.endGroup();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool MainEditor::MainEditorPrivate::isInherited() const
    {
        return true;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pParent Widget parent (nul, en g�n�ral)
     */
    MainEditor::MainEditor(QWidget* pParent)
        : QT::QMainWindow(pParent)
        , d_custom_ptr(new MainEditorPrivate(this))
    {
        Q_D(MainEditor);

        d->init();
    }


    /*! Destructeur.
     */
    MainEditor::~MainEditor() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     * /
    Components* MainEditor::createComponents()
    {
        Q_D(MainEditor);

        return new Components(d->mGameView, 0);
    }*/


    /*!
     * /
    void MainEditor::debug(PROG::Hardware* pMachine)
    {
        Q_D(MainEditor);

        d->mGameView->bindOS(pMachine->vM());
    }*/


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! La fermeture de cette fen�tre met fin au programme : il faut terminer l'ex�cution de la machine virtuelle.
        @param pEvent Ev�nement Qt de fermeture, d�clench� par l'utilisateur
     */
    void MainEditor::closeEvent(QCloseEvent* pEvent)
    {
        Q_D(MainEditor);

//d->mGameView->unregisterMode(d->mUniverseMode);
d->mUniverseMode = nullptr;

        // Important : d�sactive l'affichage.
        d->mGameView->update();
        d->mGameView->setEnabled(false);
//?d->mGameView->save();
        QT::QMainWindow::closeEvent(pEvent);

        d->mGameView->cleanUp();
        d->mGameView = nullptr;
    }


    /*! Met � jour l'interface suite � un changement de langue.
     */
    void MainEditor::onLanguageChange()
    {
        Q_D(MainEditor);

        QT::QMainWindow::onLanguageChange();

        //---------
        // Actions
        //---------
        d->mRunAct->setText(tr("&D�marrer"));
        d->mRunAct->setShortcut(QKeySequence(tr("F5", "Moteur | D�marrer")));
        d->mRunAct->setStatusTip(tr("Lance le moteur"));

        d->mStopAct->setText(tr("&Arr�ter"));
        d->mStopAct->setShortcut(QKeySequence(tr("F5", "Moteur | Arr�ter")));
        d->mStopAct->setStatusTip(tr("Stoppe le moteur"));

        d->mRestartAct->setText(tr("&Red�marrer"));
        d->mRestartAct->setShortcut(QKeySequence(tr("F7", "Moteur | Red�marrer")));
        d->mRestartAct->setStatusTip(tr("Arr�te puis relance le moteur"));

        d->mToolAct->setText(tr("Bo�te � ou&tils"));
        d->mToolAct->setShortcut(QKeySequence(tr("Ctrl+T", "Outils | Bo�te � outils")));
        d->mToolAct->setStatusTip(tr("Ouvre la bo�te � outils"));

        d->mEditorAct->setText(tr("Edite&ur"));
        d->mEditorAct->setShortcut(QKeySequence(tr("Ctrl+U", "Outils | Editeurs")));
        d->mEditorAct->setStatusTip(tr("Ouvre l'�diteur de donn�es"));

        //-------
        // Menus
        //-------
        d->mEngineMenu->setTitle(tr("&Moteur"));
        d->mToolsMenu->setTitle(tr("&Outils"));

        //--------
        // Moteur
        //--------
        QString ZLocale = qApp->language();
        if      (ZLocale == "fr")
        {
            SET_ERR_MSG_LANGUAGE(eFr);
        }
        else if (ZLocale == "en")
        {
            SET_ERR_MSG_LANGUAGE(eEn);
        }
    }


//------------------------------------------------------------------------------
//                               Gestion du Projet
//------------------------------------------------------------------------------

    /*!
     */
    QString MainEditor::createFile()
    {
        Q_D(MainEditor);

        return d->mFiles->make();
/*        PakFileBuilder   oPakFileBuilder;

        if (oPakFileBuilder.exec() == QDialog::Accepted)
        {
            return oPakFileBuilder.windowTitle();
        }
        else
        {
            return "";
        }*/
        //return QString::null;
    }


    /*!
     */
    bool MainEditor::openFile(QString file)
    {
        Q_D(MainEditor);

//        ASSERT_EX(QRegExp(QString(".+%1").arg(PAK::kaz_PakFileExtension)).exactMatch(file), kaz_InvalidPakFileName, return false;)

        try
        {
            d->mFiles->open(file);
            //d->mGameView->load(file);
        }
        catch(std::exception& pE)
        {
            qDebug(pE.what());

            QMessageBox::critical(nullptr,
                                  MainEditor::tr("Ouverture d'un pak file"), tr("L'ouverture du fichier %1 a �chou�").arg(file),
                                  QMessageBox::Ok, QMessageBox::NoButton);
            return false;
        }
        catch(...)
        {
            QMessageBox::critical(nullptr,
                                  MainEditor::tr("Ouverture d'un pak file"), tr("L'ouverture du fichier %1 a �chou�").arg(file),
                                  QMessageBox::Ok, QMessageBox::NoButton);
            return false;
        }

        d->mGameView->boot();
        centralWidget()->setEnabled(true);
        d->displayTools();
        d->displayEditors();
        d->mGameView->loaded();

        return true;
    }


    /*!
     */
    void MainEditor::save()
    {
        Q_D(MainEditor);

        d->mGameView->save();
    }


    /*!
     */
    void MainEditor::closeFile()
    {
        Q_D(MainEditor);

        save();

        centralWidget()->setEnabled(false);
        d->closePanels();

        TileMgr->save();
        ImageMgr->save();
        GeometryMgr->save();

        d->mGameView->cleanUp();
        d->mFiles->close();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void MainEditor::declareStates()
    {
        declare("engine", "run engine", "stop engine");
        registerType("lua");
        registerType("uni");
        registerType("out");
        registerType("til");
    }


    /*!
     */
    void MainEditor::synchronizeComponents()
    {
        Q_D(MainEditor);

        //---------
        // Actions
        //---------
        synchronize(d->mRunAct,     "file + !engine");
        synchronize(d->mStopAct,    "file + engine");
        synchronize(d->mRestartAct, "file + engine");

        synchronize(d->mToolAct,    "enabled + file");
        synchronize(d->mEditorAct,  "enabled + file");

        //-------
        // Menus
        //-------
        synchronize(d->mEngineMenu, "file");
        synchronize(d->mToolsMenu,  "enabled + file");
    }


    /*!
     */
    void MainEditor::performOnStartup()
    {
        performAction("stop engine");
    }


    /*!
     */
    void MainEditor::customMenu(int actionId)
    {
        Q_D(MainEditor);

        switch(actionId)
        {
            case eRunEngine:
                d->startGame();
                break;
            case eRestartEngine:
                d->resetGame();
                break;
            case eStopEngine:
                d->stopGame();
                break;
            case eTool:
                d->displayTools();
                break;
            case eEditor:
                d->displayEditors();
                break;
            default:
                ASSERT(false, kBadIdentifier)
                break;
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Construit le dialogue "A propos...".
        @param pDialog Le dialogue "A propos..." sera construit dans ce widget
     */
    void MainEditor::setupAboutBox(QDialog* pDialog)
    {
        ASSERT_EX(pDialog != nullptr, kInvalidWidget, return;)
        Ui::AboutBox    ui;
        ui.setupUi(pDialog);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void MainEditor::onEdit(ControllerPtr pController)
    {
        QString lExtension = pController->extension();

        openDocument(lExtension);
    }


    /*!
     */
    void MainEditor::onLeave(ControllerPtr pController)
    {
        QString lExtension = pController->extension();

        closeDocument(lExtension);
    }


    /*!
     */
    void MainEditor::goOneFrame()
    {
        Q_D(MainEditor);

        if (!d->mGameView->step())
        {
            d->stopGame();
        }
    }
}
