/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "PreferencesBox.hh"

/*! @file IDE/QT/PreferencesBox.cc
    @brief M�thodes (non-inline) de la classe QT::QPreferencesBox.
    @author @ref Guillaume_Terrissol
    @date 11 Septembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QSettings>
#include <QStyleFactory>

#include "UI/PreferencesBox.hh"

#include "Application.hh"

namespace
{
#if defined(EDIT_MODE_BY_DEFAULT) && (EDIT_MODE_BY_DEFAULT == true)

    bool        editModeByDefault   = true;     //!< Mode �dition par d�faut : oui.

#else

    bool        editModeByDefault   = false;    //!< Mode �dition par d�faut : faux.

#endif

    const char* editMode            = "edit";   //!< Nom du mode �dition
    const char* lookMode            = "look";   //!< Nom du mode visualisation.
}

namespace QT
{
//------------------------------------------------------------------------------
//              QPreferencesBoxPrivate : P-Impl de QPreferencesBox
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPreferencesBox.
        @version 0.3

     */
    class QPreferencesBox::QPreferencesBoxPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QPreferencesBox)
    public:
        //! @name "Constructeurs"
        //@{
                    QPreferencesBoxPrivate(QPreferencesBox* parent);    //!< Constructeur.
        void        init();                                             //!< Initialisation.
        //@}
        //! @name Langues
        //@{
        QStringList availableLanguages() const;                         //!< Noms des langues disponibles.
        QStringList availableLocales() const;                           //!< Codes des langues disponibles.
        //@}
        //! @name Permanence des pr�f�rences
        //@{
        void        writeSettings() const;                              //!< Ecriture.
        void        readSettings();                                     //!< Lecture.
        //@}
        Ui::PreferencesBox  ui;                                         //!< Interface cr��e par Designer.
        QString             style;                                      //!< Nom du style actuel.
        QString             locale;                                     //!< Code de la langue actuelle.
        bool                editEnabledByDefault;                       //!< Mode par d�faut.
    };


//------------------------------------------------------------------------------
//                   QPreferencesBoxPrivate : "Constructeurs"
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QPreferencesBox::QPreferencesBoxPrivate::QPreferencesBoxPrivate(QPreferencesBox* parent)
        : q_custom_ptr(parent)
        , ui()
        , style(QApplication::style()->objectName())
        , locale("fr")
        , editEnabledByDefault(editModeByDefault)
    { }


    /*!
     */
    void QPreferencesBox::QPreferencesBoxPrivate::init()
    {
        Q_Q(QPreferencesBox);

        ui.setupUi(q);

        readSettings();

        // Style.
        QStringList styles      = QStyleFactory::keys();
        int         styleIndex  = -1;
        for(int s = 0; s < styles.count(); ++s)
        {
            ui.styleComboBox->addItem(styles[s]);

            if (ui.styleComboBox->itemText(s).toUpper() == style.toUpper())
            {
                styleIndex = s;
            }
        }
        Q_ASSERT_X(styleIndex != -1, "void QPreferencesBox::readSettings()", "Default style not found");
        ui.styleComboBox->setCurrentIndex(styleIndex);

        // Language.
        QStringList languages       = availableLanguages();
        QStringList locales         = availableLocales();
        int         languageIndex   = -1;
        for(int l = 0; l < languages.count(); ++l)
        {
            ui.languageComboBox->addItem(languages[l]);

            if (locales[l].toUpper() == locale.toUpper())
            {
                languageIndex = l;
            }
        }
        Q_ASSERT_X(languageIndex != -1, "void QPreferencesBox::readSettings()", "Default language not found");
        ui.languageComboBox->setCurrentIndex(languageIndex);

        // Mode par d�faut.
        ui.defaultModeComboBox->setCurrentIndex(editEnabledByDefault ? 0 : 1);
    }


//------------------------------------------------------------------------------
//                       QPreferencesBoxPrivate : Langues
//------------------------------------------------------------------------------

    /*!
     */
    QStringList QPreferencesBox::QPreferencesBoxPrivate::availableLanguages() const
    {
        static QStringList  languages;
        
        if (languages.empty())
        {
            languages << "Fran�ais" << "English";
        }

        return languages;
    }


    /*!
     */
    QStringList QPreferencesBox::QPreferencesBoxPrivate::availableLocales() const
    {
        static QStringList  locales;
        
        if (locales.empty())
        {
            locales << "fr" << "en";
        }

        return locales;
    }


//------------------------------------------------------------------------------
//              PreferencesBoxPrivate : Permanence des Pr�f�rences
//------------------------------------------------------------------------------

    /*!
     */
    void QPreferencesBox::QPreferencesBoxPrivate::writeSettings() const
    {
        QSettings   settings;
        settings.beginGroup("Application");

        settings.setValue("style",     style);
        settings.setValue("locale",    locale);
        settings.setValue("default mode", QString(editEnabledByDefault ? editMode : lookMode));

        settings.endGroup();
    }


    /*!
     */
    void QPreferencesBox::QPreferencesBoxPrivate::readSettings()
    {
        QSettings   settings;
        settings.beginGroup("Application");

        style                = settings.value("style",  style).toString();
        locale               = settings.value("locale", locale).toString();
        editEnabledByDefault = settings.value("default mode", editModeByDefault).toString() == editMode;

        settings.endGroup();
    }


//------------------------------------------------------------------------------
//                  PreferencesBox : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QPreferencesBox::QPreferencesBox()
        : QDialog(nullptr)
        , d_custom_ptr(new QPreferencesBoxPrivate(this))
    {
        Q_D(QPreferencesBox);

        d->init();
    }


    /*! Destructeur.
     */
    QPreferencesBox::~QPreferencesBox() { }


//------------------------------------------------------------------------------
//                  PreferencesBox : Choix
//------------------------------------------------------------------------------

    /*!
     */
    void QPreferencesBox::accept()
    {
        Q_D(QPreferencesBox);

        QString style             = d->ui.styleComboBox->currentText();
        QString locale            = d->availableLocales()[d->availableLanguages().indexOf(d->ui.languageComboBox->currentText())];
        bool    defaultModeIsEdit = d->ui.defaultModeComboBox->currentIndex() == 0;

        if (style != d->style || locale != d->locale || defaultModeIsEdit != d->editEnabledByDefault)
        {
            if (style != d->style)
            {
                QT::QApplication::setStyle(style);
                d->style = style;
            }
            if (locale != d->locale)
            {
                qApp->setLanguage(locale);
                d->locale = locale;
            }
            if (defaultModeIsEdit != d->editEnabledByDefault)
            {
                qApp->enableEditByDefault(defaultModeIsEdit);
                d->editEnabledByDefault = defaultModeIsEdit;
            }

            d->writeSettings();
        }

        QDialog::accept();
    }
}
