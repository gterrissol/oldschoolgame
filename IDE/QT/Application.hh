/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_APPLICATION_HH
#define QT_APPLICATION_HH

#include "QT.hh"

/*! @file IDE/QT/Application.hh
    @brief En-t�te de la classe QT::QApplication.
    @author @ref Guillaume_Terrissol
    @date 8 Septembre 2005 - 17 Mars 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QApplication>

#include "Private.hh"

namespace QT
{
    /*! @brief Application.
        @version 0.5

        Application Qt �tendue.
     */
    class QApplication : public ::QApplication
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                QApplication(int& argc, char** argv);       //!< Constructeur.
virtual         ~QApplication();                            //!< Destructeur.
        //@}
virtual bool    notify(QObject* pReceiver, QEvent* pEvent); //!< Notification d'�v�nements.
        //! @name Mode �dition
        //@{
        void    enableEditByDefault(bool enable);           //!< [d�s]Activation du mode �dition par d�faut.
        bool    isEditEnabledByDefault() const;             //!< Mode �dition par d�faut ?
        bool    isEditEnabled() const;                      //!< Mode �dition ?
        //@}
        //! @name Choix de la langue
        //@{
        void    setLanguage(QString locale);                //!< Choix de la langue.
        QString language() const;                           //!< Langue actuelle.
        //@}

    public slots:
        //! @name Progression de t�ches
        //@{
        void    progressTo(int percent);                    //!< Affichage de la progression d'une t�che.
        //@}

    protected:

virtual void    childEvent(QChildEvent * e);                //!< Traitement d'�v�nements li�s aux enfants.


    private:

        Q_DISABLE_COPY(QApplication)
        Q_CUSTOM_DECLARE_PRIVATE(QApplication)
    };
}

#ifdef qApp
#undef qApp
#endif  // De qApp

/*! @brief Singleton application.
    La macro qApp de Qt4 donne acc�s � l'instance unique de QApplication. Afin d'�viter les casts
    explicites, cette macro est red�finie, et donne maintenant acc�s � une instance (toujours unique)
    de QT::QApplication.
 */
#define qApp (static_cast<QT::QApplication *>(QCoreApplication::instance()))

#endif  // QT_APPLICATION_HH
