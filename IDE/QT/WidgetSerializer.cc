/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "WidgetSerializer.hh"

/*! @file IDE/QT/WidgetSerializer.cc
    @brief M�thodes (non-inline) de la classe QT::QAbstractWidgetSerializer.
    @author @ref Guillaume_Terrissol
    @date 20 Octobre 2005 - 11 Juin 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QSet>
#include <QSettings>

namespace QT
{
    /*!
     */
    class QWidgetAbstractSerializer::QWidgetAbstractSerializerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QWidgetAbstractSerializer)
    public:
        //! @name "Constructeurs"
        //@{
                QWidgetAbstractSerializerPrivate(QWidgetAbstractSerializer* parent);    //!< Constructeur.
        void    init();                                                                 //!< Initialisation.
        //@}
        QString                                      key;                               //!< La clef est "cach�e".

 static QSet<QWidgetAbstractSerializer*>             instances;                         //!< Instances existantes.
 static QMap<QString, QWidgetAbstractSerializer*>    instancesByName;                   //!< Instances existantes, accessibles par nom.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QWidgetAbstractSerializer::QWidgetAbstractSerializerPrivate::QWidgetAbstractSerializerPrivate(QWidgetAbstractSerializer* parent)
        : q_custom_ptr(parent)
        , key("")
    { }


    /*!
     */
    void QWidgetAbstractSerializer::QWidgetAbstractSerializerPrivate::init()
    {
        Q_Q(QWidgetAbstractSerializer);

        Q_ASSERT_X(!instances.contains(q), "void QWidgetAbstractSerializerPrivate::init()", "Instance already registered");

        instances.insert(q);
    }


//------------------------------------------------------------------------------
//                                  Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    QWidgetAbstractSerializer::QWidgetAbstractSerializer()
        : d_custom_ptr(new QWidgetAbstractSerializerPrivate(this))
    {
        Q_D(QWidgetAbstractSerializer);

        d->init();
    }


    /*! Destructeur.
     */
    QWidgetAbstractSerializer::~QWidgetAbstractSerializer()
    {
        Q_D(QWidgetAbstractSerializer);

        d->instances.remove(this);
        if (!d->key.isEmpty())
        {
            d->instancesByName.remove(d->key);
        }
    }


    /*!
     */
    void QWidgetAbstractSerializer::serialize()
    {
        foreach(QWidgetAbstractSerializer* s, QWidgetAbstractSerializerPrivate::instances)
        {
            s->writeSettings();
        }
    }


    /*!
     */
    void QWidgetAbstractSerializer::writeSettings()
    {
        QSettings   settings;

        settings.beginGroup(key());
        if (!isInherited())
        {
            settings.setValue("size", getSize());
            settings.setValue("pos",  getPos());
        }

        writeCustom(settings);

        settings.endGroup();
    }


    /*!
     */
    void QWidgetAbstractSerializer::readSettings()
    {
        QSettings   settings;

        settings.beginGroup(key());
        if (!isInherited())
        {
            setSize(settings.value("size", defaultSize()).toSize());
            setPos(settings.value("pos", defaultPos()).toPoint());
        }

        readCustom(settings);

        settings.endGroup();

        // Afin de maintenir les settings qui seraient cr��s lors de cet appel (les valeurs par d�faut),
        // une sauvegarde intervient maintenant.
        //writeSettings();
    }


    /*!
     */
    void QWidgetAbstractSerializer::writeCustom(QSettings&)
    {
        // Par d�faut, aucun param�tre suppl�mentaire.
    }


    /*!
     */
    void QWidgetAbstractSerializer::readCustom(QSettings&)
    {
        // Par d�faut, aucun param�tre suppl�mentaire.
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QString QWidgetAbstractSerializer::key()
    {
        Q_D(QWidgetAbstractSerializer);

        
        if (d->key.isEmpty())
        {
            d->key = widgetName();
            Q_ASSERT_X(!d->key.isEmpty(), "QString QWidgetAbstractSerializer::key() const", "At this point, the QWidget should be named");
            Q_ASSERT_X(isInherited() || !d->instancesByName.contains(d->key), "QString QWidgetAbstractSerializer::key() const", "A widget with the same name exists already");

            d->instancesByName[d->key] = this;
        }

        return d->key;
    }


//------------------------------------------------------------------------------
//              Configuration par d�faut
//------------------------------------------------------------------------------

    /*! @return La position par d�faut de l'instance
        @note Cette m�thode peut �tre r�impl�ment�e pour donner une position initiale pertinente au
        widget
     */
    QPoint QWidgetAbstractSerializer::defaultPos() const
    {
        return QPoint();
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool QWidgetAbstractSerializer::isInherited() const
    {
        return false;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    QSet<QWidgetAbstractSerializer*>            QWidgetAbstractSerializer::QWidgetAbstractSerializerPrivate::instances;
    QMap<QString, QWidgetAbstractSerializer*>   QWidgetAbstractSerializer::QWidgetAbstractSerializerPrivate::instancesByName;


//------------------------------------------------------------------------------
//                                 Documentation
//------------------------------------------------------------------------------

    /*! @fn QString QWidgetAbstractSerializer::widgetName() const
        @return Le nom du widget
        @note Cette m�thode, d�finie par QWidgetSerializer et QWidgetPrivateSerializer, ne devrait pas
        �tre r�impl�ment�e
     */

    /*! @fn QSize QWidgetAbstractSerializer::defaultSize() const
        @return La taille par d�faut de l'instance
        @note Cette m�thode est d�finie par QWidgetSerializer et QWidgetPrivateSerializer; elle ne
        devrait �tre r�impl�ment�e que pour donner une taille par d�faut pertinente au widget
     */


    /*! @fn void QWidgetAbstractSerializer::setSize(QSize sz)
        Permet de redimensionner le widget.
        @param sz Nouvelle taille du widget
        @note Cette m�thode, d�finie par QWidgetSerializer et QWidgetPrivateSerializer, ne devrait pas
        �tre r�impl�ment�e
     */


    /*! @fn QSize QWidgetAbstractSerializer::getSize() const
        @return La taille du widget
        @note Cette m�thode, d�finie par QWidgetSerializer et QWidgetPrivateSerializer, ne devrait pas
        �tre r�impl�ment�e
     */


    /*! @fn void QWidgetAbstractSerializer::setPos(QPoint pos)
        Permet de d�placer widget.
        @param pos Nouvelle position du widget
        @note Cette m�thode, d�finie par QWidgetSerializer et QWidgetPrivateSerializer, ne devrait pas
        �tre r�impl�ment�e
     */


    /*! @fn QPoint QWidgetAbstractSerializer::getPos() const
        @return La position du widget
        @note Cette m�thode, d�finie par QWidgetSerializer et QWidgetPrivateSerializer, ne devrait pas
        �tre r�impl�ment�e
     */
}
