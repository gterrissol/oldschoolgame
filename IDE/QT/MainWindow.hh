/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_MAINWINDOW_HH
#define QT_MAINWINDOW_HH

#include "QT.hh"

/*! @file IDE/QT/MainWindow.hh
    @brief En-t�te de la classe QT::QMainWindow.
    @author @ref Guillaume_Terrissol
    @date 7 Septembre 2005 - 13 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QMainWindow>

#include "Private.hh"

namespace QT
{
    /*! @brief Fen�tre principale.
        @version 0.45

        Fen�tre principale.
     */
    class QMainWindow : public ::QMainWindow
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                QMainWindow(QWidget* parent = 0, Qt::WFlags f = 0);     //!< Constructeur.
virtual         ~QMainWindow();                                         //!< Destructeur.
        //@}
        void    setMainWidget(QWidget* w);                              //!< D�finition du widget principal de la fen�tre.
        //! @name Etats de l'application
        //@{
 static void    synchronize(QObject* o, QString state);                 //!< Synchronisation avec un �tat complexe.

    protected:

        void    declare(QString state, QString enter, QString leave);   //!< D�claration d'un �tat �l�mentaire.
        void    registerType(QString docType);                          //!< Enregistrement d'un type de document.

        void    performAction(QString action);                          //!< Ex�cution d'une action [d�s]activant un �tat �l�mentaire.
        void    openDocument(QString docType);                          //!< Notification de l'ouverture d'un document.
        void    closeDocument(QString docType);                         //!< Notification de la fermeture d'un document.
        void    closeDocuments();                                       //!< Notification de la fermeture de tous les documents.
        //@}
virtual void    changeEvent(QEvent* e);                                 //!< Traitement d'un changement (dont celui de langue).
        //! @name Gestion de nouveaux menus
        //@{
        void    insertMenu(QMenu* m);                                   //!< Insertion d'un menu dans la barre idoine.
        void    addActionToMenuGroup(QAction* action, int id);          //!< Ajout d'une action � un menu
        //@}
virtual void    closeEvent(QCloseEvent* e);                             //!< Traitement d'un �v�nement de fermeture.
virtual void    showEvent(QShowEvent* e);                               //!< Traitement d'un �v�nement d'affichage.


    protected slots:
        //! @name Slots
        //@{
        void    menu(QAction* action);                                  //!< Appel d'une action d'un menu.
virtual void    onLanguageChange();                                     //!< Changement de langue.


    private slots:
    
        void    undoRedoDone(int undo, int redo);                       //!< Mise � jour apr�s un undo/redo.
        //@}

    private:
        //! @name Autres m�thodes
virtual void    declareStates() = 0;                                    //!< D�claration des �tats pour la synchronisation.
virtual void    synchronizeComponents() = 0;                            //!< Syncrhonisation des composants de l'interface.
virtual void    performOnStartup() = 0;                                 //!< Initialisation.
virtual void    customMenu(int actionId) = 0;                           //!< Appel d'un menu "non-standard".
        //@}
        //! @name Gestion du projet
        //@{
virtual QString createFile() = 0;                                       //!< Cr�ation d'un projet.
virtual bool    openFile(QString file) = 0;                             //!< Ouverture d'un projet.
virtual void    save() = 0;                                             //!< Enregistrement du projet.
virtual void    closeFile() = 0;                                        //!< Fermeture d'un projet.
        //@}
        //! @name Bo�te de dialogue "A propos"
        //@{
virtual void    setupAboutBox(QDialog* dialog) = 0;                     //!< D�finition de la bo�te de dialogue "A propos".
        //@}
        Q_DISABLE_COPY(QMainWindow)
        Q_CUSTOM_DECLARE_PRIVATE(QMainWindow)
    };
}

#endif  // QT_MAINWINDOW_HH
