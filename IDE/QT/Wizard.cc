/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Wizard.hh"

/*! @file IDE/QT/Wizard.cc
    @brief M�thodes (non-inline) de la classe QT::QMainWindow.
    @author @ref Guillaume_Terrissol
    @date 7 Septembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QEvent>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>

namespace QT
{
//------------------------------------------------------------------------------
//                QMainWindowPrivate : P-Impl de QMainWindow
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QMainWindow.
        @version 0.3

     */
    class QWizard::QWizardPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QWizard)
    public:
        //! @name "Constructeurs"
        //@{
                    QWizardPrivate(QWizard* parent);    //!< Constructeur.
        void        init();                             //!< Initialisation.
        //@}

        QList<QWidget*> history;                        //!< Historique.
        QString         title;                          //!< Titre.
        int             numPages;                       //!< Nombre de pages.
        QPushButton*    cancelButton;                   //!< Bouton "Annuler".
        QPushButton*    backButton;                     //!< Bouton "Pr�c�dent".
        QPushButton*    nextButton;                     //!< Bouton "Suivant".
        QPushButton*    finishButton;                   //!< Bouton "Terminer".
        QHBoxLayout*    buttonLayout;                   //!< Layout des boutons.
        QVBoxLayout*    mainLayout;                     //!< Layout principal.
    };


//------------------------------------------------------------------------------
//                     QMainWindowPrivate : "Constructeurs"
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QWizard::QWizardPrivate::QWizardPrivate(QWizard* parent)
        : q_custom_ptr(parent)
        , numPages(-1)
        , cancelButton(nullptr)
        , backButton(nullptr)
        , nextButton(nullptr)
        , finishButton(nullptr)
        , buttonLayout(nullptr)
        , mainLayout(nullptr)
    { }


    /*!
     */
    void QWizard::QWizardPrivate::init()
    {
        Q_Q(QWizard);

        cancelButton = new QPushButton();
        backButton = new QPushButton();
        nextButton = new QPushButton();
        finishButton = new QPushButton();

        QObject::connect(cancelButton, SIGNAL(clicked()), q, SLOT(reject()));
        QObject::connect(backButton,   SIGNAL(clicked()), q, SLOT(backButtonClicked()));
        QObject::connect(nextButton,   SIGNAL(clicked()), q, SLOT(nextButtonClicked()));
        QObject::connect(finishButton, SIGNAL(clicked()), q, SLOT(accept()));

        buttonLayout = new QHBoxLayout();
        buttonLayout->addStretch(1);
        buttonLayout->addWidget(cancelButton);
        buttonLayout->addWidget(backButton);
        buttonLayout->addWidget(nextButton);
        buttonLayout->addWidget(finishButton);

        mainLayout = new QVBoxLayout();
        mainLayout->addLayout(buttonLayout);
        q->setLayout(mainLayout);
    }


//------------------------------------------------------------------------------
//                     QWizard : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QWizard::QWizard(QWidget* parent)
        : QDialog(parent)
        , d_custom_ptr(new QWizardPrivate(this))
    {
        Q_D(QWizard);

        d->init();

        onLanguageChange();
    }


    /*! Destructeur.
     */
    QWizard::~QWizard() { }


//------------------------------------------------------------------------------
//                     QWizard : 
//------------------------------------------------------------------------------

    /*!
     */
    void QWizard::setWindowTitle(QString title)
    {
        Q_D(QWizard);

        d->title = title;
    }


    /*!
     */
    void QWizard::setButtonEnabled(bool enable)
    {
        Q_D(QWizard);

        if (d->history.size() == d->numPages)
        {
            d->finishButton->setEnabled(enable);
        }
        else
        {
            d->nextButton->setEnabled(enable);
        }
    }


//------------------------------------------------------------------------------
//                          QWizard :
//------------------------------------------------------------------------------

    /*!
     */
    void QWizard::setNumPages(int n)
    {
        Q_D(QWizard);

        d->numPages = n;
        d->history.append(createPage(0));
        switchPage(nullptr);
    }


//------------------------------------------------------------------------------
//                        QWizard : Changement de Langue
//------------------------------------------------------------------------------

    /*! Traitement d'un �v�nement de changement d'�tat.
        @param e Ev�nement � traiter
     */
    void QWizard::changeEvent(QEvent* e)
    {
        if (e->type() == QEvent::LanguageChange)
        {
            onLanguageChange();
            e->accept();
        }

        QDialog::changeEvent(e);
    }


    /*! Cette m�thode est appel�e lorsque l'application a effectu� un changement de langue.
        @sa @ref QT_Translation_Page
     */
    void QWizard::onLanguageChange()
    {
        Q_D(QWizard);

        d->title = tr("Wizard", "Titre du wizard");
        d->cancelButton->setText(tr("Annuler"));
        d->backButton->setText(tr("< &Retour"));
        d->nextButton->setText(tr("&Suivant >"));
        d->finishButton->setText(tr("&Terminer"));
    }


//------------------------------------------------------------------------------
//                          QWizard :
//------------------------------------------------------------------------------

    /*!
     */
    void QWizard::backButtonClicked()
    {
        Q_D(QWizard);

        d->nextButton->setEnabled(true);
        d->finishButton->setEnabled(true);

        QWidget*    oldPage = d->history.takeLast();
        switchPage(oldPage);
        delete oldPage;
    }


    /*!
     */
    void QWizard::nextButtonClicked()
    {
        Q_D(QWizard);

        d->nextButton->setEnabled(true);
        d->finishButton->setEnabled(d->history.size() == d->numPages - 1);

        QWidget*    oldPage = d->history.last();
        d->history.append(createPage(d->history.size()));
        switchPage(oldPage);
    }


//------------------------------------------------------------------------------
//                          QWizard :
//------------------------------------------------------------------------------

    /*!
     */
    void QWizard::switchPage(QWidget *oldPage)
    {
        Q_D(QWizard);

        if (oldPage != nullptr)
        {
            oldPage->hide();
            d->mainLayout->removeWidget(oldPage);
        }

        QWidget* newPage = d->history.last();
        d->mainLayout->insertWidget(0, newPage);
        newPage->show();
        newPage->setFocus();

        d->backButton->setEnabled(d->history.size() != 1);
        if (d->history.size() == d->numPages)
        {
            d->nextButton->setEnabled(false);
            d->finishButton->setDefault(true);
        }
        else
        {
            d->nextButton->setDefault(true);
            d->finishButton->setEnabled(false);
        }

        setWindowTitle(d->title + " - " + tr("Etape %1 sur %2").arg(d->history.size()).arg(d->numPages));
    }
}
