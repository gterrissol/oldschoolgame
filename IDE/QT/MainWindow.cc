/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "MainWindow.hh"

/*! @file IDE/QT/MainWindow.cc
    @brief M�thodes (non-inline) des classes QT::QMainWindow::QMainWindowPrivate & QT::QMainWindow.
    @author @ref Guillaume_Terrissol
    @date 7 Septembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QAction>
#include <QActionGroup>
#include <QCloseEvent>
#include <QFileDialog>
#include <QMenu>
#include <QMenuBar>
#include <QWhatsThis>

#include "Application.hh"
#include "PreferencesBox.hh"
#include "StateMachine.hh"
#include "Subject.hh"
#include "WidgetSerializer.hh"

namespace QT
{
//------------------------------------------------------------------------------
//                QMainWindowPrivate : P-Impl de QMainWindow
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QMainWindow.
        @version 0.4

     */
    class QMainWindow::QMainWindowPrivate : public QWidgetPrivateSerializer<QMainWindow::QMainWindowPrivate>
    {
        Q_CUSTOM_DECLARE_PUBLIC(QMainWindow)
    public:
        //! @name Actions standard
        enum Actions
        {
            New  = 1,
            Open,
            Save,
            Close,
            Quit,
            Undo,
            Redo,
            EnableEdit,
            Preferences,
            Manual,
            About,
            Custom
        };
        //! @name "Constructeurs" & destructeur
        //@{
                    QMainWindowPrivate(QMainWindow* parent);    //!< Constructeur.
        void        init();                                     //!< Initialisation.
virtual             ~QMainWindowPrivate();                      //!< Destructeur.
        //@}
        //! @name Initialisation
        //@{
        void    createActions();                                //!< ... des actions (des menus).
        void    fillMenus();                                    //!< ... des menus.
        void    declareStates();                                //!< ... des "�tats" de l'application.
        void    synchronizeComponents();                        //!< Synchronisation des composants de l'interface.
        //@}
        //! @name Configuration par d�faut
        //@{
virtual QSize   defaultSize() const;                            //!< Taille du widget par d�faut.
virtual QPoint  defaultPos() const;                             //!< Position du widget par d�faut.
        //@}
        bool    confirmExit() const;                            //!< Confirmation de sortie.
        //! @name M�thodes associ�es aux actions
        //@{
        void    createFile();                                   //!< Cr�ation d'un fichier.
        void    openFile();                                     //!< Ouverture d'un fichier.
        void    saveFile();                                     //!< Enregistrement d'un fichier.
        void    closeFile();                                    //!< Fermeture d'un fichier.
        void    quit();                                         //!< Fermeture de l'application
        void    undo();                                         //!< Annulation d'une action.
        void    redo();                                         //!< Reproduction d'une action.
        void    enableEdit(bool enable);                        //!< Activation de l'�dition.
        void    editPreferences();                              //!< Edition des pr�f�rences.
        void    manual();                                       //!< Appel du manuel.
        void    about();                                        //!< A propos...
        //@}
 static QMainWindowPrivate* that;                               //!< Singleton.
        //! @name Actions standard
        //@{
        QAction*            newAct;                             //!< Cr�ation d'un fichier.
        QAction*            openAct;                            //!< Ouverture d'un fichier.
        QAction*            saveAct;                            //!< Enregistrement d'un fichier.
        QAction*            closeAct;                           //!< Fermeture d'un fichier.
        QAction*            quitAct;                            //!< Fermeture de l'application

        QAction*            undoAct;                            //!< Annulation d'une action.
        QAction*            redoAct;                            //!< Reproduction d'une action.
        QAction*            enableEditAct;                      //!< Activation de l'�dition.
        QAction*            preferencesAct;                     //!< Edition des pr�f�rences.

        QAction*            manualAct;                          //!< Appel du manuel.
        QAction*            whatsThisAct;                       //!< C�koidon ?
        QAction*            aboutAct;                           //!< A propos...

        QActionGroup*       actionGroup;                        //!< Groupe d'actions.
        //@}
        //! @name Menus standard
        //@{
        QMenu*              fileMenu;                           //!< Fichier.
        QMenu*              editMenu;                           //!< Edition.
        QMenu*              helpMenu;                           //!< Aide.
        //@}
        QStateMachine*      machine;                            //!< Machine � �tats.
        QSubjectManager*    undoRedoManager;                    //!< Gestionnaire d'undo/redo.
        int                 undoCount;                          //!< Nombre d'undo possibles.
        int                 redoCount;                          //!< nombre de redo possibles.

        bool                startupDone;                        //!< D�marrage effectu� ?
        bool                fileOpened;                         //!< Pak file ouvert ?

 friend class QWidgetPrivateSerializer<QMainWindowPrivate>;
    };


    QMainWindow::QMainWindowPrivate*    QMainWindow::QMainWindowPrivate::that   = nullptr;

 
//------------------------------------------------------------------------------
//              QMainWindowPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QMainWindow::QMainWindowPrivate::QMainWindowPrivate(QMainWindow* parent)
        : q_custom_ptr(parent)
        , newAct(nullptr)
        , openAct(nullptr)
        , saveAct(nullptr)
        , closeAct(nullptr)
        , quitAct(nullptr)
        , undoAct(nullptr)
        , redoAct(nullptr)
        , enableEditAct(nullptr)
        , preferencesAct(nullptr)
        , manualAct(nullptr)
        , whatsThisAct(nullptr)
        , aboutAct(nullptr)
        , actionGroup(nullptr)
        , fileMenu(nullptr)
        , editMenu(nullptr)
        , helpMenu(nullptr)
        , machine(nullptr)
        , undoRedoManager(nullptr)
        , undoCount(0)
        , redoCount(0)
        , startupDone(false)
        , fileOpened(false)
    {
        Q_ASSERT_X(that == nullptr, "QMainWindowPrivate::QMainWindowPrivate(QMainWindow* parent)", "A QMainWindow instance already exists");

        that = this;
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::init()
    {
        Q_Q(QMainWindow);

        q->setObjectName("MainWindow");

        readSettings();

        machine         = new QStateMachine(q);
        undoRedoManager = new QSubjectManager(q);

        createActions();
        fillMenus();

        q->connect(undoRedoManager, SIGNAL(countChanged(int, int)), q, SLOT(undoRedoDone(int, int)));
    }


    /*!
     */
    QMainWindow::QMainWindowPrivate::~QMainWindowPrivate()
    {
        that = nullptr;
    }


//------------------------------------------------------------------------------
//                      QMainWindowPrivate : Initialisation
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::QMainWindowPrivate::createActions()
    {
        Q_Q(QMainWindow);

        //-------------------
        // Fichier - Nouveau
        //-------------------
        newAct          = new QAction(QIcon(":/icons/new file"), "", q);
        newAct->setData(QVariant(QMainWindowPrivate::New));

        //------------------
        // Fichier - Ouvrir
        //------------------
        openAct         = new QAction(QIcon(":/icons/open file"), "", q);
        openAct->setData(QVariant(QMainWindowPrivate::Open));

        //----------------------------
        // Fichier - Enregistrer sous
        //----------------------------
        saveAct         = new QAction(q);
        saveAct->setData(QVariant(QMainWindowPrivate::Save));

        //------------------
        // Fichier - Fermer
        //------------------
        closeAct        = new QAction(QIcon(":/icons/close file"), "", q);
        closeAct->setData(QVariant(QMainWindowPrivate::Close));

        //-------------------
        // Fichier - Quitter
        //-------------------
        quitAct         = new QAction(QIcon(":/icons/quit"), "", q);
        quitAct->setData(QVariant(QMainWindowPrivate::Quit));

        //---------
        // Edition
        //---------
        undoAct         = new QAction(QIcon(":/icons/undo"), "", q);
        undoAct->setData(QVariant(QMainWindowPrivate::Undo));

        redoAct         = new QAction(QIcon(":/icons/redo"), "", q);
        redoAct->setData(QVariant(QMainWindowPrivate::Redo));

        enableEditAct   = new QAction(QIcon(), "", q);
        enableEditAct->setData(QVariant(QMainWindowPrivate::EnableEdit));
        enableEditAct->setCheckable(true);
        enableEditAct->setChecked(qApp->isEditEnabledByDefault());

        preferencesAct  = new QAction(QIcon(":/icons/preferences"), "", q);
        preferencesAct->setData(QVariant(QMainWindowPrivate::Preferences));

        //------
        // Aide
        //------
        manualAct       = new QAction(QIcon(":/icons/manual"), "", q);
        manualAct->setData(QVariant(QMainWindowPrivate::Manual));

        whatsThisAct    = QWhatsThis::createAction();

        aboutAct        = new QAction(QIcon(":/icons/about"), "", q);
        aboutAct->setData(QVariant(QMainWindowPrivate::About));

        //-------------
        // Connections
        //-------------
        actionGroup     = new QActionGroup(q);
        actionGroup->setExclusive(false);

        newAct->setActionGroup(actionGroup);
        openAct->setActionGroup(actionGroup);
        saveAct->setActionGroup(actionGroup);
        closeAct->setActionGroup(actionGroup);
        quitAct->setActionGroup(actionGroup);
        undoAct->setActionGroup(actionGroup);
        redoAct->setActionGroup(actionGroup);
        enableEditAct->setActionGroup(actionGroup);
        preferencesAct->setActionGroup(actionGroup);
        manualAct->setActionGroup(actionGroup);
        aboutAct->setActionGroup(actionGroup);

        q->connect(actionGroup, SIGNAL(triggered(QAction*)), SLOT(menu(QAction*)));
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::fillMenus()
    {
        Q_Q(QMainWindow);

        QMenuBar*   menuBar = q->menuBar();
        //---------
        // Fichier
        //---------
        menuBar->addMenu(fileMenu = new QMenu(menuBar));

        fileMenu->addAction(newAct);
        fileMenu->addAction(openAct);
        fileMenu->addAction(saveAct);
        fileMenu->addAction(closeAct);
        fileMenu->addSeparator();
        fileMenu->addAction(quitAct);

        //---------
        // Edition
        //---------
        menuBar->addMenu(editMenu = new QMenu(menuBar));

        editMenu->addAction(undoAct);
        editMenu->addAction(redoAct);
        editMenu->addAction(enableEditAct);
        editMenu->addSeparator();
        editMenu->addAction(preferencesAct);

        //------
        // Aide
        //------
        menuBar->addMenu(helpMenu = new QMenu(menuBar));

        helpMenu->addAction(manualAct);
        helpMenu->addAction(whatsThisAct);
        helpMenu->addAction(aboutAct);
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::declareStates()
    {
        Q_Q(QMainWindow);

        q->declare("file",    "open file",     "close file");
        q->declare("enabled", "enable",        "disable");
        q->declare("edition", "edit",          "look");
        q->declare("undo",    "positive undo", "null undo");
        q->declare("redo",    "positive redo", "null redo");

        //----------------------------
        // Etats des classes d�riv�es
        //----------------------------
        q->declareStates();
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::synchronizeComponents()
    {
        Q_Q(QMainWindow);

        //---------
        // Actions
        //---------
        q->synchronize(newAct,         "enabled");
        q->synchronize(openAct,        "enabled");
        q->synchronize(saveAct,        "enabled + file + undo");
        q->synchronize(closeAct,       "enabled + file");
        q->synchronize(quitAct,        "enabled");
        q->synchronize(undoAct,        "enabled + undo + edition");
        q->synchronize(redoAct,        "enabled + redo + edition");
        q->synchronize(enableEditAct,  "enabled + file");
        q->synchronize(preferencesAct, "enabled");
        q->synchronize(manualAct,      "enabled");
        q->synchronize(whatsThisAct,   "enabled");
        q->synchronize(aboutAct,       "enabled");

        //-------
        // Menus
        //-------
        q->synchronize(fileMenu, "enabled");
        q->synchronize(editMenu, "enabled");
        q->synchronize(helpMenu, "enabled");

        //---------------------------------
        // Composants des classes d�riv�es
        //---------------------------------
        q->synchronizeComponents();
    }


//------------------------------------------------------------------------------
//                 QMainWindowPrivate : Configuration par D�faut
//------------------------------------------------------------------------------

    /*!
     */
    QSize QMainWindow::QMainWindowPrivate::defaultSize() const
    {
        return QSize(800, 600);
    }


    /*!
     */
    QPoint QMainWindow::QMainWindowPrivate::defaultPos() const
    {
        return QPoint(100, 100);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    bool QMainWindow::QMainWindowPrivate::confirmExit() const
    {
        return true;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::QMainWindowPrivate::createFile()
    {
        Q_Q(QMainWindow);

        QString newFile = q->createFile();

        if (!newFile.isEmpty())
        {
            closeFile();

            if (q->openFile(newFile))
            {
                machine->perform("open file");
                fileOpened = true;
                // Quand on cr�e un fichier, c'est pour l'�diter.
                enableEdit(true);
            }
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::openFile()
    {
        Q_Q(QMainWindow);

        QString file = QFileDialog::getOpenFileName(q,
                                                    qApp->translate("QT::QMainWindow", "Choisissez un pak file"),
                                                    QApplication::applicationDirPath() + "/pak",
                                                    "Pak Files (*.pak)");

        if (!file.isEmpty())
        {
            closeFile();

            if (q->openFile(file))
            {
                machine->perform("open file");
                fileOpened = true;
                // A l'ouverture d'un fichier, v�rifier si le mode �dition est activ� par d�faut.
                enableEdit(qApp->isEditEnabledByDefault());
            }
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::saveFile()
    {
        Q_Q(QMainWindow);

        // L'action n'est disponible que si un fichier est ouvert...
        q->save();
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::closeFile()
    {
        Q_Q(QMainWindow);

        // Sauvegarde les param�tres.
        QWidgetAbstractSerializer::serialize();

        if (fileOpened)
        {
            q->setUpdatesEnabled(false);
            q->closeDocuments();
            q->closeFile();
            machine->perform("close file");
            fileOpened = false;
            q->setUpdatesEnabled(true);
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::quit()
    {
        Q_Q(QMainWindow);

        q->close();
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::undo()
    {
        undoRedoManager->undo();

        QWidget* w = qobject_cast<QWidget*>(undoRedoManager->parent());
        if (w != nullptr)
        {
            w->update();
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::redo()
    {
        undoRedoManager->redo();

        QWidget* w = qobject_cast<QWidget*>(undoRedoManager->parent());
        if (w != nullptr)
        {
            w->update();
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::enableEdit(bool enable)
    {
        enableEditAct->setChecked(enable);

        if (enable)
        {
            machine->perform("edit");
        }
        else
        {
            machine->perform("look");
        }
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::editPreferences()
    {
        QPreferencesBox preferences;

        preferences.exec();
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::manual()
    {
        qDebug("Manuel");
    }


    /*!
     */
    void QMainWindow::QMainWindowPrivate::about()
    {
        Q_Q(QMainWindow);

        QDialog         aboutBox;
        q->setupAboutBox(&aboutBox);

        aboutBox.exec();
    }


//------------------------------------------------------------------------------
//                   QMainWindow : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QMainWindow::QMainWindow(QWidget* parent, Qt::WFlags f)
        : ::QMainWindow(parent, f)
        , d_custom_ptr(new QMainWindowPrivate(this))
    {
        Q_D(QMainWindow);

        d->init();

        onLanguageChange();

        statusBar();
    }


    /*! Destructeur.
     */
    QMainWindow::~QMainWindow() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Dommage qu'il n'y ait pas de r�ponse virtuelle � la d�finition du widget central de la Main
        Window Qt.
        @param w Widget central de la fen�tre
        @warning Cette m�thode ne devrait �tre appel�e qu'une seule fois
     */
    void QMainWindow::setMainWidget(QWidget* w)
    {
        Q_D(QMainWindow);

        setCentralWidget(w);
        d->undoRedoManager->setParent(w);
    }


//------------------------------------------------------------------------------
//                     QMainWindow : Etats de l'application
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::synchronize(QObject* o, QString state)
    {
        Q_ASSERT_X(QMainWindowPrivate::that != nullptr, "void QMainWindow::synchronize(QObject* o, QString state)" , "No QMainWindow instance has been created yet");

        QMainWindowPrivate::that->machine->synchronize(o, state);
    }


    /*!
     */
    void QMainWindow::declare(QString state, QString enter, QString leave)
    {
        Q_D(QMainWindow);

        d->machine->declare(state, enter, leave);
    }


    /*!
     */
    void QMainWindow::registerType(QString docType)
    {
        Q_D(QMainWindow);

        d->machine->registerType(docType);
    }


    /*!
     */
    void QMainWindow::performAction(QString action)
    {
        Q_D(QMainWindow);

        d->machine->perform(action);
    }


    /*!
     */
    void QMainWindow::openDocument(QString docType)
    {
        Q_D(QMainWindow);

        d->machine->open(docType);
    }


    /*!
     */
    void QMainWindow::closeDocument(QString docType)
    {
        Q_D(QMainWindow);

        d->machine->close(docType);
    }


    /*!
     */
    void QMainWindow::closeDocuments()
    {
        Q_D(QMainWindow);

        d->machine->closeAll();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Traitement d'un �v�nement de changement d'�tat.
        @param e Ev�nement � traiter
     */
    void QMainWindow::changeEvent(QEvent* e)
    {
        if (e->type() == QEvent::LanguageChange)
        {
            onLanguageChange();
            e->accept();
        }

        ::QMainWindow::changeEvent(e);
    }


//------------------------------------------------------------------------------
//                   QMainWindow : Gestion de nouveaux menus
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::insertMenu(QMenu* m)
    {
        Q_D(QMainWindow);

        menuBar()->insertMenu(d->helpMenu->menuAction(), m);
    }


    /*!
     */
    void QMainWindow::addActionToMenuGroup(QAction* action, int id)
    {
        Q_D(QMainWindow);

        Q_ASSERT_X(0 <= id, "void QMainWindow::addActionToMenuGroup(QAction* action, int id)", "");

        action->setData(QVariant(QMainWindowPrivate::Custom + id));
        d->actionGroup->addAction(action);
    }


//------------------------------------------------------------------------------
//                   QMainWindow : 
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::closeEvent(QCloseEvent* e)
    {
        Q_D(QMainWindow);

        if (d->confirmExit())
        {
            d->closeFile();

            e->accept();
        }
        else
        {
            e->ignore();
        }
    }


    /*!
     */
    void QMainWindow::showEvent(QShowEvent* e)
    {
        Q_D(QMainWindow);

        if (!d->startupDone)
        {
            d->startupDone = true;

            d->declareStates();
            d->synchronizeComponents();

            d->machine->perform("close file");
            d->machine->perform("enable");
            d->machine->perform("null undo");
            d->machine->perform("null redo");

            performOnStartup();
        }

        ::QMainWindow::showEvent(e);
    }


//------------------------------------------------------------------------------
//                   QMainWindow : Slots
//------------------------------------------------------------------------------

    /*!
     */
    void QMainWindow::menu(QAction* action)
    {
        Q_D(QMainWindow);

        int id = action->data().toInt();

        switch(id)
        {
            case QMainWindowPrivate::New:
                d->createFile();
                break;
            case QMainWindowPrivate::Open:
                d->openFile();
                break;
            case QMainWindowPrivate::Save:
                d->saveFile();
                break;
            case QMainWindowPrivate::Close:
                d->closeFile();
                break;
            case QMainWindowPrivate::Quit:
                d->quit();
                break;
            case QMainWindowPrivate::Undo:
                d->undo();
                break;
            case QMainWindowPrivate::Redo:
                d->redo();
                break;
            case QMainWindowPrivate::EnableEdit:
                d->enableEdit(action->isChecked());
                break;
            case QMainWindowPrivate::Preferences:
                d->editPreferences();
                break;
            case QMainWindowPrivate::Manual:
                d->manual();
                break;
            case QMainWindowPrivate::About:
                d->about();
                break;
            default:
                customMenu(id - QMainWindowPrivate::Custom);
                break;
        }
    }


    /*! Met � jour l'interface suite � un changement de langue.
     */
    void QMainWindow::onLanguageChange()
    {
        Q_D(QMainWindow);

        //---------
        // Actions
        //---------
        d->newAct->setText(tr("&Nouveau"));
        d->newAct->setShortcut(QKeySequence(tr("Ctrl+N", "Fichier | Nouveau")));
        d->newAct->setStatusTip(tr("Cr�e un nouveau fichier"));
        d->newAct->setWhatsThis(tr("S�lectionnez cette entr�e pour cr�er un nouveau fichier"));

        d->openAct->setText(tr("&Ouvrir..."));
        d->openAct->setShortcut(QKeySequence(tr("Ctrl+O", "Fichier | Ouvrir")));
        d->openAct->setStatusTip(tr("Ouvre un fichier"));
        d->openAct->setWhatsThis(tr("S�lectionnez cette entr�e pour ouvrir un fichier existant"));

        d->saveAct->setText(tr("Enregistrer"));
        d->saveAct->setShortcut(QKeySequence(tr("Ctrl+S", "Fichier | Enregistrer")));
        d->saveAct->setStatusTip(tr("Enregistre les modifications actuelles"));
        d->saveAct->setWhatsThis(tr("S�lectionnez cette entr�e pour forcer l'enregistrement des modifications"));

        d->closeAct->setText(tr("&Fermer"));
        d->closeAct->setShortcut(QKeySequence(tr("Ctrl+W", "Fichier | Fermer")));
        d->closeAct->setStatusTip(tr("Ferme le fichier ouvert"));
        d->closeAct->setWhatsThis(tr("S�lectionnez cette entr�e pour terminer l'�dition du fichier ouvert"));

        d->quitAct->setText(tr("&Quitter"));
        d->quitAct->setShortcut(QKeySequence(tr("Ctrl+Q", "Fichier | Quitter")));
        d->quitAct->setStatusTip(tr("Quitte l'application"));
        d->quitAct->setWhatsThis(tr("S�lectionnez cette entr�e pour quitter l'application"));

        d->undoAct->setText(tr("Annuler"));
        d->undoAct->setShortcut(QKeySequence(tr("Ctrl+Z", "Edition | Undo")));
        d->undoAct->setStatusTip(tr("Annule la derni�re action"));
        d->undoAct->setWhatsThis(tr("S�lectionnez cette entr�e pour annuler la derni�re action"));

        d->redoAct->setText(tr("R�tablir"));
        d->redoAct->setShortcut(QKeySequence(tr("Shift+Ctrl+Z", "Edition | Redo")));
        d->redoAct->setStatusTip(tr("R�effectue la derni�re action"));
        d->redoAct->setWhatsThis(tr("S�lectionnez cette entr�e pour refaire la derni�re action annul�e"));

        d->enableEditAct->setText(tr("&Edition"));
        d->enableEditAct->setShortcut(QKeySequence(tr("Ctrl+E", "Edition | Edition")));
        d->enableEditAct->setStatusTip(tr("Active le mode �dition"));
        d->enableEditAct->setWhatsThis(tr("S�lectionnez cette entr� pour basculer de l'�dition (coch�e) � la simple visualisation (d�coch�e) des donn�es"));

        d->preferencesAct->setText(tr("&Pr�f�rences"));
        d->preferencesAct->setShortcut(QKeySequence(tr("Ctrl+P", "Edition | Pr�f�rences")));
        d->preferencesAct->setStatusTip(tr("Edite les pr�f�rences"));
        d->preferencesAct->setWhatsThis(tr("S�lectionnez cette entr�e pour �diter les pr�f�rences de l'application"));

        d->manualAct->setText(tr("&Manuel"));
        d->manualAct->setShortcut(QKeySequence(tr("F1", "Aide | Manuel")));
        d->manualAct->setStatusTip(tr("Ouvre l'aide en ligne"));
        d->manualAct->setWhatsThis(tr("S�lectionnez cette entr�e pour ouvrir le manuel de l'application"));

        d->whatsThisAct->setText(tr("&Qu'est-ce que c'est ?"));
        d->whatsThisAct->setStatusTip(tr("Active le mode \"Qu'est-ce-que c'est ?\""));
        d->whatsThisAct->setWhatsThis(tr("S�lectionnez cette entr�e pour afficher des informations sur les composants de l'interface"));

        d->aboutAct->setText(tr("&A propos"));
        d->aboutAct->setStatusTip(tr("Affiche le copyright"));
        d->aboutAct->setWhatsThis(tr("S�lectionnez cette entr�e pour afficher les informations de copyright"));

        //-------
        // Menus
        //-------
        d->fileMenu->setTitle(tr("&Fichier"));
        d->editMenu->setTitle(tr("&Edition"));
        d->helpMenu->setTitle(tr("&Aide"));
    }


    /*!
     */
    void QMainWindow::undoRedoDone(int undo, int redo)
    {
        Q_D(QMainWindow);

        bool    oldUndoActive = (0 < d->undoCount);
        bool    oldRedoActive = (0 < d->redoCount);

        bool    newUndoActive = (0 < undo);
        bool    newRedoActive = (0 < redo);

        if (oldUndoActive != newUndoActive)
        {
            d->undoCount = undo;

            if (newUndoActive)
            {
                d->machine->perform("positive undo");
            }
            else
            {
                d->machine->perform("null undo");
            }
        }
        if (oldRedoActive != newRedoActive)
        {
            d->redoCount = redo;

            if (newRedoActive)
            {
                d->machine->perform("positive redo");
            }
            else
            {
                d->machine->perform("null redo");
            }
        }
    }
}
