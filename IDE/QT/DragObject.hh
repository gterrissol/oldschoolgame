/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_DRAGOBJECT_HH
#define QT_DRAGOBJECT_HH

#include "QT.hh"

/*! @file IDE/QT/DragObject.hh
    @brief En-t�te de la classe QT::QDragObject.
    @author @ref Guillaume_Terrissol
    @date 5 Septembre 2005 - 13 Janvier 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Private.hh"

class QIcon;
class QObject;
class QPoint;
class QString;
class QWidget;

namespace QT
{
    /*! @brief Syst�me de glisser/d�poser pour les QObject.
        @version 1.0

        Cette classe a �t� introduite afin de proposer un syt�me unifi� pour le drag'n'drop de widgets
        (ou de QObject, pour �tre plus g�n�ral).<br>
        Cette classe permet, entre autres, d'afficher une ic�ne et du texte � c�t� du pointeur de la
        souris pendant la phase de glissement. Les m�thodes icon(), title(), offset(), mayDropOn(),
        dropOn() permettent de param�trer le comportement du drag'n'drop (apparence, d�p�t,...)
     */
    class QDragObject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    QDragObject(QObject* dragged);                  //!< Constructeur.
virtual             ~QDragObject();                                 //!< Destructeur.
        //@}
        //! @name Drag'n'drop
        //@{
        void        drag(QPoint p);                                 //!< D�but de glissement.
        void        move(QPoint p);                                 //!< Glissement.
        void        drop(QPoint p);                                 //!< D�p�t.


    protected:

            QObject* dragged() const;                               //!< Objet gliss� � d�poser.
        //@}

    private:
        //! @name Comportement � impl�menter
        //@{
virtual QIcon       icon() const = 0;                               //!< Ic�ne 
virtual QString     title() const = 0;                              //!< Titre
virtual QPoint      offset() const;                                 //!< Position relative du cadre d'information.
virtual bool        mayDropOn(QWidget* w, QPoint p) const = 0;      //!< Possibilit� de d�p�t.
virtual void        dropOn(QWidget* w, QPoint p) = 0;               //!< D�p�t sur un widget.
        //@}
        //!@name Surbrillance de la cible
        //@{
virtual QWidget*    buddy(QWidget* w) const;                        //!< R�cup�ration de la cible � d�corer.
        void        decorate(QWidget* w);                           //!< D�coration de la cible du d�p�t

        Q_DISABLE_COPY(QDragObject)
        Q_CUSTOM_DECLARE_PRIVATE(QDragObject)
    };
}

#endif  // QT_DRAGOBJECT_HH
