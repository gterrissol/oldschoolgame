/****************************************************************************
**
** Copyright (C) 1992-2005 Trolltech AS. All rights reserved.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY F ANY KIND, INCLUDING THE
** WARRANTY F DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QT_WIZARD_HH
#define QT_WIZARD_HH

#include "QT.hh"

/*! @file IDE/QT/Wizard.hh
    @brief En-t�te de la classe QT::QMainWindow.
    @author Code original : Trolltech
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 9 Octobre 2005 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDialog>

#include "Private.hh"

namespace QT
{
    /*! @brief Wizard.
        @version 0.4

        Wizard.
     */
    class QWizard : public QDialog
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    QWizard(QWidget* parent = 0);   //!< Constructeur.
virtual             ~QWizard();                     //!< Destructeur.
        //@}
        void        setWindowTitle(QString title);  //!< D�finition du titre.
        void        setButtonEnabled(bool enabled); //!< Activation des boutons.

    protected:
        //! @name Gestion des pages
        //@{
virtual QWidget*    createPage(int index) = 0;      //!< Cr�ation d'une page.
        void        setNumPages(int n);             //!< D�finition du nombre de pages.
        //@}
        //! @name Changement de langue
        //@{
virtual void        changeEvent(QEvent* e);         //!< Traitement d'un changement (dont celui de langue).
virtual void        onLanguageChange();             //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private slots:
        //! @name Gestion des boutons
        //@{
        void        backButtonClicked();            //!< Vers l'�tape pr�c�dente.
        void        nextButtonClicked();            //!< Vers l'�tape suivante.
        //@}

    private:

        void switchPage(QWidget* oldPage);          //!< Passage d'une page � une autre.

        Q_DISABLE_COPY(QWizard)
        Q_CUSTOM_DECLARE_PRIVATE(QWizard)
    };
}

#endif  // QT_WIZARD_HH
