/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "DragObject.hh"

/*! @file IDE/QT/DragObject.cc
    @brief M�thodes (non-inline) des classes QT::QDragObject::QDragObjectPrivate & QT::QDragObject.
    @author @ref Guillaume_Terrissol
    @date 5 Septembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QFrame>
#include <QIcon>
#include <QHBoxLayout>
#include <QLabel>
#include <QRubberBand>

#include "Application.hh"

namespace QT
{
//------------------------------------------------------------------------------
//                  QDragObjectPrivate : P-Impl de QDragObject
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QDragObject.
        @version 0.4
     */
    class QDragObject::QDragObjectPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QDragObject)
    public:
        //! @name Constructeur & destructeur
        //@{
        QDragObjectPrivate(QDragObject* parent, QObject* o);    //!< Constructeur.
        ~QDragObjectPrivate();                                  //!< Destructeur.
        //@}
        //! @name Attributs
        //@{
        QObject*                        object;                 //!< Objet � glisser et d�poser.
        std::unique_ptr<QFrame>         visual;                 //!< Apparence visuelle de l'objet gliss�.
        std::unique_ptr<QRubberBand>    highlight;              //!< Surbrillance de la cible du d�p�t.
        //@}
    };


//------------------------------------------------------------------------------
//                       QDragObjectPrivate : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param parent Object pour lequel porter les attributs de glissement
        @param o Object � glisser/d�poser
     */
    QDragObject::QDragObjectPrivate::QDragObjectPrivate(QDragObject* parent, QObject* o)
        : q_custom_ptr(parent)
        , object(o)
        , visual(nullptr)
        , highlight(nullptr)
    { }


    /*! Destructeur.
     */
    QDragObject::QDragObjectPrivate::~QDragObjectPrivate() { }


//------------------------------------------------------------------------------
//                   QDragObject : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param o Object � glisser/d�poser
     */
    QDragObject::QDragObject(QObject* o)
        : d_custom_ptr(new QDragObjectPrivate(this, o))
    { }


    /*! Destructeur.
     */
    QDragObject::~QDragObject()
    {
        decorate(0);
    }

//------------------------------------------------------------------------------
//                           QDragObject : Drag'n'Drop
//------------------------------------------------------------------------------

    /*! Commence une op�ration de drag'n'drop � une position donn�e : affiche une information visuelle
        (ic�ne et �tiquette).
        @param p Position [globale] intiale du drag'n'drop
     */
    void QDragObject::drag(QPoint p)
    {
        Q_D(QDragObject);

        d->visual.reset(new QFrame(0, Qt::ToolTip));
        d->visual->setFrameStyle(QFrame::Panel | QFrame::Raised);
        d->visual->setLineWidth(2);

        QHBoxLayout*    mainLayout  = new QHBoxLayout(d->visual.get());
        mainLayout->setMargin(2);
        mainLayout->setSpacing(2);

        QLabel*         image       = new QLabel(d->visual.get());
        image->setPixmap(icon().pixmap(24, 24));
        mainLayout->addWidget(image);

        QLabel*         text        = new QLabel(title(), d->visual.get());
        mainLayout->addWidget(text);

        d->visual->move(p + offset());
        d->visual->show();

        QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));

        QWidget*        widget      = QApplication::widgetAt(p);
        Q_ASSERT_X(widget != 0, "void QDragObject::drag(QPoint)", "No widget under cursor");

        decorate(widget);
    }


    /*! D�place l'information visuelle du drag � une position donn�e. Si une cible potentielle pour le
        drop se trouve sous la position <i>p</i>, elle est "d�cor�e".
        @param p Position [globale] actuelle du drag
     */
    void QDragObject::move(QPoint p)
    {
        Q_D(QDragObject);

        d->visual->move(p + offset());

        QWidget*    widget = QApplication::widgetAt(p);

        if (mayDropOn(widget, p))
        {
            decorate(widget);

            QApplication::changeOverrideCursor(QCursor(Qt::ArrowCursor));
        }
        else
        {
            decorate(0);

            QApplication::changeOverrideCursor(QCursor(Qt::ForbiddenCursor));
        }
    }


    /*! D�pose l'objet dragg� � la position <i>p</i> (si le widget sous cette position accepte le d�p�t).
        @param p Position [globale] du drop
     */
    void QDragObject::drop(QPoint p)
    {
        Q_D(QDragObject);

        decorate(0);

        QWidget*    widget = QApplication::widgetAt(p);

        if (mayDropOn(widget, p))
        {
            dropOn(widget, widget != 0 ? widget->mapFromGlobal(p) : p);
            d->visual.reset();
        }

        QApplication::restoreOverrideCursor();
    }


    /*! @return L'objet dragg�
     */
    QObject* QDragObject::dragged() const
    {
        Q_D(const QDragObject);

        return d->object;
    }


//------------------------------------------------------------------------------
//                    QDragObject : Surbrillance de la cible
//------------------------------------------------------------------------------

    /*! Il peut arriver que le widget sur lequel d�poser l'onglet soit l'enfant de celui sur lequel
        d�poser effectivement l'onglet (ou qu'il lui soit li� d'une quelconque autre mani�re). Cette
        m�thode permet de r�cup�rer la vraie cible du d�p�t.
        @param w Widget sur lequel le d�p�t est propos� (ou 0, en cas de drag vers l'"ext�rieur")
        @return Le vrai widget sur lequel d�poser l'onglet (ou 0, en cas de drag vers l'"ext�rieur")
     */
    QWidget* QDragObject::buddy(QWidget* w) const
    {
        return w;
    }


    /*! D�core un widget, avec un cadre color� (QRubberBand)  pour indiquer la possibilit� de d�p�t de
        l'objet dragg�.
        @param w Widget � d�corer (en cas de pointeur nul, la d�coration sera "cach�e")
     */
    void QDragObject::decorate(QWidget* w)
    {
        Q_D(QDragObject);

        if (w == 0)
        {
            d->highlight.reset();
        }
        else
        {
            w = buddy(w);

            if (d->highlight != nullptr)
            {
                if (w != d->highlight->parentWidget())
                {
                    d->highlight.reset(new QRubberBand(QRubberBand::Rectangle, w));
                    d->highlight->move(QPoint(0, 0));
                    d->highlight->resize(w->size());
                    d->highlight->show();
                }
            }
            else
            {
                d->highlight.reset(new QRubberBand(QRubberBand::Rectangle, w));
                d->highlight->move(QPoint(0, 0));
                d->highlight->resize(w->size());
                d->highlight->show();
            }
        }
    }


//------------------------------------------------------------------------------
//                 QDragObject : Comportement � (R�)Impl�menter
//------------------------------------------------------------------------------

    /*! @fn QIcon QDragObject::icon() const
        @return L'ic�ne � afficher lors du drag
     */


    /*! @fn QString QDragObject::title() const
        @return Le texte � afficher lors du drag
     */


    /*! @return Le d�calage entre la position effective du drag, et celle de l'information visuelle.
     */
    QPoint QDragObject::offset() const
    {
        return QPoint(8, 8);
    }


    /*! @fn bool QDragObject::mayDropOn(QWidget* w, QPoint p) const
        @param w Widget sur lequel tester la possibilit� de d�pot de l'objet dragg�
        @param p Position [globale] pour laquelle tester la possibilit� de d�p�t de l'objet dragg�
        @return Vrai si dragged() peut �tre d�pos� sur le widget <i>w</i>, � la position <i>p</i>
     */


    /*! @fn void QDragObject::dropOn(QWidget* w, QPoint p)
        Effectue le drop proprement dit. Cette m�thode est appel�e seulement si mayDropOn() a retourn�
        VRAI.
        @param w Widget sur lequel d�poser dragged()
        @param p Position [locale � <i>w</i> s'il existe, globale sinon] � laquelle d�poser dragged()
     */
}
