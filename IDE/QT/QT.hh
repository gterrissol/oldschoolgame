/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_QT_HH
#define QT_QT_HH

/*! @file QT/QT.hh
    @brief Pr�-d�clarations du module @ref QT.
    @author @ref Guillaume_Terrissol
    @date 13 Janvier 2008 - 18 Ao�t 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT
{
    class QAnswer;
    class QApplication;
    class QDragObject;
    class QFileIconManager;
    class QMainWindow;
    class QObserver;
    class QPreferencesBox;
    class QRequest;
    class QState;
    class QStateMachine;
    class QSubject;
    class QSubjectList;
    class QSubjectManager;
    class QSynchronized;
    class QWidgetAbstractSerializer;
    class QWizard;


    template<class T>   class QUnique;
    template<class W>   class QWidgetSerializer;
    template<class W>   class QWidgetPrivateSerializer;
}

#endif  // De QT_QT_HH
