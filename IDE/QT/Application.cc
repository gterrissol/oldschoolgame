/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Application.hh"

/*! @file IDE/QT/Application.cc
    @brief M�thodes (non-inline) des classes QT::QApplication::QApplicationPrivate & QT::QApplication.
    @author @ref Guillaume_Terrissol
    @date 8 Septembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QProgressDialog>
#include <QSettings>
#include <QStyleFactory>
#include <QTranslator>
#include <QTime>

#include "MainWindow.hh"
#include "State.hh"

/*! Charge les ressources (images et icones) utilis�es par l'interface graphique.
    @return 1
    @note La documentation de cette fonction est r�dig�e dans QT/Application.cc car elle est g�n�r�e par
    rcc (Qt4)
 */
int qInitResources();

namespace
{
    /*! @brief Mode d'�dition par d�faut de l'�diteur.
        Le mode d'�dition par d�faut est d�fini par le script configure (--enable-edit-mode-by-default ou
        --disable-edit-mode-by-default).
     */
#if defined(EDIT_MODE_BY_DEFAULT) && (EDIT_MODE_BY_DEFAULT == true)

    bool    editModeByDefault   = true;

#else

    bool    editModeByDefault   = false;

#endif
}

namespace QT
{
//------------------------------------------------------------------------------
//                 QApplicationPrivate : P-Impl de QApplication
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QApplication.
        @version 0.3

     */
    class QApplication::QApplicationPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QApplication)
    public:

        //! @name "Constructeurs" & destructeur
        //@{
                QApplicationPrivate(QApplication* parent);      //!< Constructeur.
        void    init();                                         //!< Initialisation.
                ~QApplicationPrivate();                         //!< Destructeur.
        //@}

        void    readSettings();                                 //!< Lecture des param�tres.

        void    setLanguage(QString locale);                    //!< Choix de la langue.

        void    setProgress(int percent);                       //!< Avancement de la barre de progression.

 static QApplicationPrivate*    that;                           //!< Singleton.
        QTranslator*            qtTranslator;                   //!< Traducteur Qt.
        QTranslator*            appTranslator;                  //!< Traducteur de l'application.
        QString                 language;                       //!< Langue actuelle.
        QProgressDialog*        progress;                       //!< Barre de progression.
        QSynchronized*          editMode;                       //!< Synchronisation sur le mode �dition.
        QTime                   displayProgressBarStartTime;    //!< Instant d'affichage de la progress bar.
        bool                    editEnabledByDefault;           //!< Edition active par d�faut ?
    };


    QApplication::QApplicationPrivate*  QApplication::QApplicationPrivate::that = nullptr;


//------------------------------------------------------------------------------
//              QApplicationPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QApplication::QApplicationPrivate::QApplicationPrivate(QApplication* parent)
        : q_custom_ptr(parent)
        , qtTranslator(nullptr)
        , appTranslator(nullptr)
        , language("fr")
        , progress(nullptr)
        , editMode(nullptr)
        , displayProgressBarStartTime()
        , editEnabledByDefault(editModeByDefault)
    {
        Q_ASSERT_X(that == nullptr, "QApplicationPrivate::QApplicationPrivate(QApplication* parent)", "A QApplication is already defined");

        that = this;
    }


    /*!
     */
    void QApplication::QApplicationPrivate::init()
    {
        Q_Q(QApplication);

        qInitResources();

        QFont   font = QApplication::font();
        font.setStyleStrategy(QFont::PreferAntialias);
        QApplication::setFont(font);

        editMode = new QSynchronized(q);
        editMode->setEnabled(editEnabledByDefault);

        readSettings();
    }


    /*!
     */
    QApplication::QApplicationPrivate::~QApplicationPrivate()
    {
        that = nullptr;
    }


//------------------------------------------------------------------------------
//                  QApplicationPrivate : 
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::QApplicationPrivate::readSettings()
    {
        Q_Q(QApplication);

        QSettings   settings;
        settings.beginGroup("Application");

        q->setStyle(settings.value("style", "").toString());
        q->setLanguage(settings.value("locale", language).toString());
        q->enableEditByDefault(settings.value("default mode", "edit").toString() == "edit");

        settings.endGroup();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::QApplicationPrivate::setLanguage(QString locale)
    {
        Q_Q(QApplication);

        language = locale;

        if (qtTranslator == nullptr)
        {
            qtTranslator = new QTranslator(q);
            q->installTranslator(qtTranslator);
        }
        if (appTranslator == nullptr)
        {
            appTranslator = new QTranslator(q);
            q->installTranslator(appTranslator);
        }

        // libraryPaths()[0] doit normalement �tre le r�pertoire INSTALL/plugins.
        qtTranslator->load(QString("qt_%1.qm").arg(language),
                           QString("%1/../translations/").arg(QApplication::libraryPaths()[0]));
        appTranslator->load(QString("IDE_%1.qm").arg(language), ".");
    }


    /*!
     */
    void QApplication::QApplicationPrivate::setProgress(int percent)
    {
        Q_Q(QApplication);

        Q_ASSERT_X(0 <= percent, "void QApplicationPrivate::setProgress(int percent)", "Progress must be positive");

        if (progress == 0)
        {
            if (percent < 100)
            {
                progress = new QProgressDialog(QApplication::tr("Veuillez patienter", "Progress Bar"), QString(), 0, 100, nullptr);
                progress->hide();
                displayProgressBarStartTime = QTime::currentTime().addSecs(2);
            }
            else
            {
                // D�j� � 100% (ou plus) : rien � faire.
                return;
            }
        }

        // Y a-t-il une �ch�ance � respecter ?
        if (!displayProgressBarStartTime.isNull())
        {
            // Est-elle d�pass�e ?
            if (QTime::currentTime() < displayProgressBarStartTime)
            {
                // Non : pas encore d'affichage de la progress bar.
                return;
            }
            else
            {
                // Oui : on peut afficher la progress bar.
                displayProgressBarStartTime = QTime();
                progress->show();
            }
        }

        if ((percent <= 100) && (percent != progress->value()))
        {
            progress->setValue(percent);
            q->processEvents();

            if (percent == 100)
            {
                delete progress;
                progress = nullptr;
            }
        }
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QApplication::QApplication(int& argc, char** argv)
        : ::QApplication(argc, argv)
        , d_custom_ptr(new QApplicationPrivate(this))
    {
        Q_D(QApplication);

        setOrganizationName("OldSchool");
        setOrganizationDomain("www.oldschoolgame.com");
        setApplicationName("IDE");

        d->init();
    }


    /*! Destructeur.
     */
    QApplication::~QApplication() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @note Cette m�thode n'est l� que pour la capture d'exception
     */
    bool QApplication::notify(QObject* pReceiver, QEvent* pEvent)
    {
        try
        {
            return ::QApplication::notify(pReceiver, pEvent);
        }
        /*catch(ERR::Exception& pE)
        {
            qDebug(QString("Exception osg captur�e : %1").arg(pE.what()).toAscii().constData());
        }*/
        catch(std::exception& pE)
        {
            qDebug(QString("Exception STL captur�e : %1").arg(pE.what()).toAscii().constData());
        }
        catch(...)
        {
            qDebug("Exception inconnue captur�e");
        }

        return false;
    }


//------------------------------------------------------------------------------
//                                 Mode Edition
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::enableEditByDefault(bool enable)
    {
        Q_D(QApplication);

        d->editEnabledByDefault = enable;
    }


    /*!
     */
    bool QApplication::isEditEnabledByDefault() const
    {
        Q_D(const QApplication);

        return d->editEnabledByDefault;
    }


    /*!
     */
    bool QApplication::isEditEnabled() const
    {
        Q_D(const QApplication);

        return d->editMode->isEnabled();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::setLanguage(QString locale)
    {
        Q_D(QApplication);

        d->setLanguage(locale);
    }


    /*!
     */
    QString QApplication::language() const
    {
        Q_D(const QApplication);

        return d->language;
    }


//------------------------------------------------------------------------------
//                             Progression de T�ches
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::progressTo(int percent)
    {
        Q_D(QApplication);

        d->setProgress(percent);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QApplication::childEvent(QChildEvent* e)
    {
        Q_D(QApplication);

        if (e->added())
        {
            if (QMainWindow* mainWindow = qobject_cast<QMainWindow*>(e->child()))
            {
                mainWindow->synchronize(d->editMode, "edition");
            }
        }
    }


//------------------------------------------------------------------------------
//                         Documentation Suppl�mentaire
//------------------------------------------------------------------------------

    /*! @page QT_Translation_Page Internationalisation avec Qt
        - Traduction � la vol�e...
     */
}
