/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/QT/WidgetSerializer.inl
    @brief M�thodes inline des classes QT::QWidgetSerializer & QT::QWidgetPrivateSerializer.
    @author @ref Guillaume_Terrissol
    @date 20 Octobre 2005 - 11 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QPoint>
#include <QSize>
#include <QString>

namespace QT
{
//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QWidgetSerializer<W>::QWidgetSerializer()
        : QWidgetAbstractSerializer()
    { }


    /*!
     */
    template<class W>
    QWidgetSerializer<W>::~QWidgetSerializer() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QString QWidgetSerializer<W>::widgetName() const
    {
        return dynamic_cast<const W*>(this)->objectName();
    }


//------------------------------------------------------------------------------
//  Configuration par d�faut
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QSize QWidgetSerializer<W>::defaultSize() const
    {
        return dynamic_cast<const W*>(this)->sizeHint();
    }


//------------------------------------------------------------------------------
//  Param�tres g�n�raux
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    void QWidgetSerializer<W>::setSize(QSize sz)
    {
        dynamic_cast<W*>(this)->resize(sz);
    }


    /*!
     */
    template<class W>
    QSize QWidgetSerializer<W>::getSize() const
    {
        return dynamic_cast<const W*>(this)->size();
    }


    /*!
     */
    template<class W>
    void QWidgetSerializer<W>::setPos(QPoint pos)
    {
        dynamic_cast<W*>(this)->move(pos);
    }


    /*!
     */
    template<class W>
    QPoint QWidgetSerializer<W>::getPos() const
    {
        return dynamic_cast<const W*>(this)->pos();
    }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QWidgetPrivateSerializer<W>::QWidgetPrivateSerializer()
        : QWidgetAbstractSerializer()
    { }


    /*!
     */
    template<class W>
    QWidgetPrivateSerializer<W>::~QWidgetPrivateSerializer() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QString QWidgetPrivateSerializer<W>::widgetName() const
    {
        return dynamic_cast<const W*>(this)->q_func()->objectName();
    }


//------------------------------------------------------------------------------
//  Configuration par d�faut
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    QSize QWidgetPrivateSerializer<W>::defaultSize() const
    {
        return dynamic_cast<const W*>(this)->q_func()->sizeHint();
    }


//------------------------------------------------------------------------------
//  Param�tres g�n�raux
//------------------------------------------------------------------------------

    /*!
     */
    template<class W>
    void QWidgetPrivateSerializer<W>::setSize(QSize sz)
    {
        dynamic_cast<W*>(this)->q_func()->resize(sz);
    }


    /*!
     */
    template<class W>
    QSize QWidgetPrivateSerializer<W>::getSize() const
    {
        return dynamic_cast<const W*>(this)->q_func()->size();
    }


    /*!
     */
    template<class W>
    void QWidgetPrivateSerializer<W>::setPos(QPoint pos)
    {
        dynamic_cast<W*>(this)->q_func()->move(pos);
    }


    /*!
     */
    template<class W>
    QPoint QWidgetPrivateSerializer<W>::getPos() const
    {
        return dynamic_cast<const W*>(this)->q_func()->pos();
    }
}
