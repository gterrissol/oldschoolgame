/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_WIDGETSERIALIZER_HH
#define QT_WIDGETSERIALIZER_HH

#include "QT.hh"

/*! @file IDE/QT/WidgetSerializer.hh
    @brief En-t�te des classes QT::QWidgetAbstractSerializer, QT::QWidgetSerializer &
    QT::QWidgetPrivateSerializer.
    @author @ref Guillaume_Terrissol
    @date 20 Octobre 2005 - 13 Janvier 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Private.hh"

class QPoint;
class QSettings;
class QSize;
class QString;

namespace QT
{
    /*! @brief Interface pour la "s�rialisation" de widgets.
        @version 0.75

        Pour simplifier au maximum la "permanence" de certains param�tres des widgets (taille et
        position, notamment), j'ai introduit un m�canisme bas� sur cette classe et ses d�riv�es directes.
        Pour en b�n�ficier, il suffit, pour un widget QMyWidget (resp. pour un P-Impl de widget
        MyWidgetPrivate), de d�river de QWidgetSerializer<QMyWidget> (resp.
        QWidgetPrivateSerializer<QMyWidgetPrivate>), et de doter tous les widgets de noms distincts (les
        m�mes d'une session � l'autre) et ce, avant toute utilisation des m�thodes de cette classe. A
        partir de l�, les op�rations suivantes sont disponibles :
        - serialize() : permet de sauvegarder les param�tres de toutes les instances d�finies,
        - writeSettings() : permet de sauvegarder les param�tres d'une instance donn�e � n'importe quel
        moment,
        - readSettings() : permet de charger les param�tres (et de les appliquer � l'instance),
        - writeCustom() et readCustom() permettent respectivement d'�crire et lire des param�tres
        suppl�mentaires pour un widget (par d�faut, seules la taille et la position sont g�r�es).
        @note Dans la documentation de cette classe, le terme widget fait r�f�rence � l'instance
        elle-m�me (via l'h�ritage param�tr� de QWidgetSerializer) ou au "propri�taire" du P-Impl dont
        l'instance d�rive (toujours via l'h�ritage param�tr�)
     */
    class QWidgetAbstractSerializer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                QWidgetAbstractSerializer();        //!< Constructeur par d�faut.
virtual         ~QWidgetAbstractSerializer();       //!< Destructeur.
        //@}
        //! @name "S�rialisation"
        //@{
 static void    serialize();                        //!< Sauvegarde des param�tres pour toutes les instances.


    protected:

        void    writeSettings();                    //!< Ecriture des param�tres de l'instance.
        void    readSettings();                     //!< Lecture des param�tres de l'instance.

    private:

virtual void    writeCustom(QSettings& settings);   //!< Ecriture des param�tres suppl�menaires.
virtual void    readCustom(QSettings& settings);    //!< Lecture des param�tres suppl�menaires.
        //@}
        //! @name Clef des param�tres
        //@{
        QString key();                              //!< Clef pour r�f�rencer les param�tres.
virtual QString widgetName() const = 0;             //!< R�cup�ration du nom du widget.
        //@}
        //! @name Configuration par d�faut
        //@{
virtual QSize   defaultSize() const = 0;            //!< Taille du widget par d�faut.
virtual QPoint  defaultPos() const;                 //!< Position du widget par d�faut.
        //@}
        //! @name Param�tres g�n�raux
        //@{
virtual void    setSize(QSize sz) = 0;              //!< D�finition de la taille du widget.
virtual QSize   getSize() const = 0;                //!< R�cup�ration de la taille du widget.
virtual void    setPos(QPoint pos) = 0;             //!< D�finition de la position du widget.
virtual QPoint  getPos() const = 0;                 //!< R�cup�ration de la position du widget.
        //@}
virtual bool    isInherited() const;                //!< "D�rivation" d'un autre s�rialiseur ?
        Q_DISABLE_COPY(QWidgetAbstractSerializer)
        Q_CUSTOM_DECLARE_PRIVATE(QWidgetAbstractSerializer)
    };


    /*! @brief Impl�mentation pour la "s�rialisation" de widgets.
        @version 0.7

        Cette classe doit �tre d�riv�e (par h�ritage param�tr� d'un widget) afin de b�n�ficier de la
        "s�rialisation" des param�tres de ce widget.
        @sa QWidgetPrivateSerializer
     */
    template<class W>
    class QWidgetSerializer : public QWidgetAbstractSerializer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                QWidgetSerializer();    //!< Constructeur par d�faut.
virtual         ~QWidgetSerializer();   //!< Destructeur.
        //@}

    private:
        //! @name Clef des param�tres
        //@{
virtual QString widgetName() const;     //!< R�cup�ration du nom du widget.
        //@}
        //! @name Configuration par d�faut
        //@{
virtual QSize   defaultSize() const;    //!< Taille du widget par d�faut.
        //@}
        //! @name Param�tres g�n�raux
        //@{
virtual void    setSize(QSize sz);      //!< D�finition de la taille du widget.
virtual QSize   getSize() const;        //!< R�cup�ration de la taille du widget.
virtual void    setPos(QPoint pos);     //!< D�finition de la position du widget.
virtual QPoint  getPos() const;         //!< R�cup�ration de la position du widget.
        //@}
    };


    /*! @brief Impl�mentation pour la "s�rialisation" de P-Impl de widgets.
        @version 0.7

        Cette classe doit �tre d�riv�e (par h�ritage param�tr� d'un P-Impl de widget) afin de b�n�ficier
        de la "s�rialisation" des param�tres
        de ce widget.
        @note M�me si c'est nu peu lourd, n'oubliez pas de d�clarer l'instanciation
        QWidgetPrivateSerializer<<b>P-Impl</b>> amie de P-Impl
        @sa QWidgetSerializer
     */
    template<class W>
    class QWidgetPrivateSerializer : public QWidgetAbstractSerializer
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                QWidgetPrivateSerializer();     //!< Constructeur par d�faut.
virtual         ~QWidgetPrivateSerializer();    //!< Destructeur.
        //@}

    private:
        //! @name Clef des param�tres
        //@{
virtual QString widgetName() const;             //!< R�cup�ration du nom du widget.
        //@}
        //! @name Configuration par d�faut
        //@{
virtual QSize   defaultSize() const;            //!< Taille du widget par d�faut.
        //@}
        //! @name Param�tres g�n�raux
        //@{
virtual void    setSize(QSize sz);              //!< D�finition de la taille du widget.
virtual QSize   getSize() const;                //!< R�cup�ration de la taille du widget.
virtual void    setPos(QPoint pos);             //!< D�finition de la position du widget.
virtual QPoint  getPos() const;                 //!< R�cup�ration de la position du widget.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "WidgetSerializer.inl"

#endif  // De QT_WIDGETSERIALIZER_HH
