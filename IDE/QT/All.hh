/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_ALL_HH
#define QT_ALL_HH

/*! @file IDE/QT/All.hh
    @brief Interface publique du module @ref QT.
    @author @ref Guillaume_Terrissol
    @date 5 Septembre 2005 - 13 Janvier 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QT   //! Framework bas� sur Qt 4.
{
    /*! @namespace QT
        @version 0.5

        Ce module d�finit un workspace Qt.
     */

    /*! @defgroup QT QT : Framework Qt.
        Les �l�ments du <b>namespace</b> QT consitutent une base assez compl�te pour le d�veloppement
        d'applications IHM.
     */
}


//------------------------------------------------------------------------------
//                              En-T�tes du Module                              
//------------------------------------------------------------------------------

#include "Answer.hh"
#include "Request.hh"
#include "Subject.hh"
#include "Observer.hh"
#include "State.hh"
#include "StateMachine.hh"
#include "DragObject.hh"
#include "WidgetSerializer.hh"
#include "FileIconManager.hh"
#include "Wizard.hh"
#include "PreferencesBox.hh"
#include "MainWindow.hh"
#include "Application.hh"

#endif  // De QT_ALL_HH
