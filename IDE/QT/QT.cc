/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file QT/QT.cc
    @brief Enregistrement de l'activateur du module CORE.
    @author @ref Guillaume_Terrissol
    @date 13 Mars 2013 - 13 Mars 2013
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "EXTernals/Define.hh"

MINIMAL_ACTIVATOR(QT)
