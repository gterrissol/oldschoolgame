/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_FILEICONMANAGER_HH
#define QT_FILEICONMANAGER_HH

#include "QT.hh"

/*! @file IDE/QT/FileIconManager.hh
    @brief En-t�te de la classe QT::QFileIconManager.
    @author @ref Guillaume_Terrissol
    @date 25 Octobre 2005 - 13 Janvier 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Private.hh"

class QIcon;
class QSize;
class QString;

namespace QT
{
    /*! @brief Gestion des ic�nes dans les browsers.
        @version 0.3
     */
    class QFileIconManager
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                            QFileIconManager(QString file); //!< Constructeur.
virtual                     ~QFileIconManager();            //!< Destructeur.
        //@}
 static QFileIconManager*   instance();                     //!< Gestionnaire d'ic�nes de fichier.
 static QSize               standardSize();                 //!< Taille pour les ic�nes.
        //! @name Donn�e
        //@{
        QIcon               undefined() const;              //!< Ic�ne "ind�finie".

        QIcon               load(quint32 id) const;         //!< Chargement d'une ic�ne.
        void                save(quint32 id, QIcon icon);   //!< Sauvegarde d'une ic�ne.
        //@}

    private:

        Q_DISABLE_COPY(QFileIconManager)
        Q_CUSTOM_DECLARE_PRIVATE(QFileIconManager)
    };
}

#endif  // De QT_FILEICONMANAGER_HH
