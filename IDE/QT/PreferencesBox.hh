/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QT_PREFERENCESBOX_HH
#define QT_PREFERENCESBOX_HH

#include "QT.hh"

/*! @file IDE/QT/PreferencesBox.hh
    @brief En-t�te de la classe QT::QPreferencesBox.
    @author @ref Guillaume_Terrissol
    @date 11 Septembre 2005 - 13 Janvier 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDialog>

#include "Private.hh"

namespace QT
{
    /*! @brief Choix des pr�f�rences.
        @version 0.3

        Bo�te de dialogue du choix des pr�f�rences d'application.
     */
    class QPreferencesBox : public QDialog
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                QPreferencesBox();      //!< Constructeur.
virtual         ~QPreferencesBox();     //!< Destructeur.
        //@}
        //! @name Choix
        //@{
virtual void    accept();               //!< Validation.
        //@}

        private:

        Q_DISABLE_COPY(QPreferencesBox)
        Q_CUSTOM_DECLARE_PRIVATE(QPreferencesBox)
    };
}

#endif  // QT_PREFERENCESBOX_HH
