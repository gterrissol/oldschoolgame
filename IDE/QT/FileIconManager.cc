/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "FileIconManager.hh"

/*! @file IDE/QT/FileIconManager.cc
    @brief M�thodes (non-inline) des classes QT::QFileIconManager::QFileIconManagerPrivate &
    QT::QFileIconManager.
    @author @ref Guillaume_Terrissol
    @date 31 Octobre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QBuffer>
#include <QFile>
#include <QIcon>

namespace
{
    const int   iconByteCount = 800;    //!< Taille maximale autoris�e (en octets) pour une ic�ne (compress�e en jpeg).
}

namespace QT
{
//------------------------------------------------------------------------------
//             QFileIconManagerPrivate : P-Impl de QFileIconManager
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QFileIconManager.
        @version 0.3

     */
    class QFileIconManager::QFileIconManagerPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QFileIconManager)
    public:

        //! @name "Constructeurs" & destructeur
        //@{
                QFileIconManagerPrivate(QFileIconManager* parent,
                                        QString           file);    //!< Constructeur.
        void    init();                                             //!< Initialisation.
                ~QFileIconManagerPrivate();                         //!< Destructeur.
        //@}

 static         QFileIconManagerPrivate*    that;                   //!< Singleton.
        mutable QFile                       iconFile;               //!< Fichiers contenant les ic�nes.
    };


    QFileIconManager::QFileIconManagerPrivate*  QFileIconManager::QFileIconManagerPrivate::that = nullptr;


//------------------------------------------------------------------------------
//              QFileIconManagerPrivate : "Constructeurs" & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QFileIconManager::QFileIconManagerPrivate::QFileIconManagerPrivate(QFileIconManager* parent, QString file)
        : q_custom_ptr(parent)
        , iconFile(file)
    {
        Q_ASSERT_X(that == nullptr, "QFileIconManagerPrivate::QFileIconManagerPrivate(QFileIconManager* parent)", "A QFileIconManager is already defined");

        that = this;
    }


    /*!
     */
    void QFileIconManager::QFileIconManagerPrivate::init()
    {
        bool    res = iconFile.open(QIODevice::ReadWrite);

        Q_ASSERT_X(res, "void QFileIconManagerPrivate::init()", "Icon file couldn't be opened");
    }


    /*!
     */
    QFileIconManager::QFileIconManagerPrivate::~QFileIconManagerPrivate()
    {
        iconFile.close();
        that = nullptr;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param file 
     */
    QFileIconManager::QFileIconManager(QString file)
        : d_custom_ptr(new QFileIconManagerPrivate(this, file))
    {
        Q_D(QFileIconManager);

        d->init();
    }


    /*! Destructeur.
     */
    QFileIconManager::~QFileIconManager() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Gestionnaire d'ic�nes de fichier.
     */
    QFileIconManager* QFileIconManager::instance()
    {
        if (QFileIconManagerPrivate::that != nullptr)
        {
            return QFileIconManagerPrivate::that->q_func();
        }
        else
        {
            return nullptr;
        }
    }


    /*!
     */
    QSize QFileIconManager::standardSize()
    {
        return QSize(64, 64);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    QIcon QFileIconManager::undefined() const
    {
        return QIcon(":/icons/file");
    }


    /*!
     */
    QIcon QFileIconManager::load(quint32 id) const
    {
        Q_D(const QFileIconManager);

        d->iconFile.seek(iconByteCount * id);
        QByteArray      data = d->iconFile.read(iconByteCount);
        QPixmap         pixmap;
        pixmap.loadFromData(data, "JPG");

        return QIcon(pixmap);
    }


    /*!
     */
    void QFileIconManager::save(quint32 id, QIcon icon)
    {
        Q_D(QFileIconManager);

        QByteArray  bytes;
        QBuffer     buffer(&bytes);
        QPixmap     pixmap = icon.pixmap(standardSize());
        int         size = iconByteCount + 1;

        for(int quality = 80; (iconByteCount < size) && (0 <= quality); quality -= 5)
        {
            buffer.open(QIODevice::Truncate | QIODevice::WriteOnly);
            pixmap.save(&buffer, "JPG", quality);
            size = buffer.size();
            buffer.close();
        }

        d->iconFile.seek(iconByteCount * id);
        buffer.open(QIODevice::ReadOnly);
        d->iconFile.write(buffer.data());
    }
}
