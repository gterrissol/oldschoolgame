/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Container.hh"

/*! @file IDE/Method/Container.cc
    @brief M�thodes (non-inline) de la classe Method::Container.
    @author @ref Guillaume_Terrissol
    @date 23 Janvier 2013 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Assert.hh"

#include <OSGi/Context.hh>

#include "CORE/Activator.hh"
#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"

namespace Method
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @page category_method Construction de l'interface homme-jeu.
        @todo A r�diger
     */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    Container::Container(OSGi::Context* pContext)
        : CORE::Container("method", tr("Interface"), QStringLiteral(":/ide/method/icons/Interface"), pContext)
    { }


    /*!
     */
    Container::~Container() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    class Activator : public CORE::IDEActivator
    {
    public:
                Activator()
                    : CORE::IDEActivator(BUNDLE_NAME)
                { }
virtual         ~Activator() { }

    private:

virtual void    checkInComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkIn("method.container", [=]() { return CORE::UIService::Widget{new Container(pContext)}; });

                    auto    lSyncService = pContext->services()->findByTypeAndName<CORE::SyncService>("fr.osg.ide.sync");
                    lSyncService->declare(CORE::Synchronizer("method.undo").enter("positive method.undo").leave("null method.undo"));
                    lSyncService->declare(CORE::Synchronizer("method.redo").enter("positive method.redo").leave("null method.redo"));
                }
virtual void    checkOutComponents(OSGi::Context* pContext) override final
                {
                    auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                    lUIService->checkOut("method.container");
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(Method::Activator)
OSGI_END_REGISTER_ACTIVATORS()
