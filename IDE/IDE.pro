SOURCES = EDITor/MainEditor.cc \
          EDITor/System.cc \
          QOUTside/Editor.cc \
          QPAcKage/Builder.cc \
          QPAcKage/Extension.cc \
          QPAcKage/ImportExport.cc \
          QT/Application.cc \
          QT/MainWindow.cc \
          QT/Wizard.cc \
          QT3D/Widget.cc \
          QTAB/Bar.cc \
          QTAB/Dock.cc \
          QTAB/Page.cc \
          QTILe/Editor.cc \
          QTILe/FragmentMgr.cc \
          QUNIverse/Editor.cc \
          QWORld/Brushes.cc

TRANSLATIONS = IDE_en.ts

RESOURCES = ICOns/IDE.qrc

FORMS = UI/AboutBox.ui \
        UI/Brushes.ui  \
        UI/FamilyDialog.ui \
        UI/FragmentDialog.ui \
        UI/FragmentManager.ui \
        UI/ModelLibrary.ui \
        UI/OutsideEditor.ui \
        UI/PreferencesBox.ui \
        UI/ProjectBuilder.ui \
        UI/ResourceViewer.ui \
        UI/TileEditor.ui \
        UI/UniverseEditor.ui

TEMPLATE = app
LANGUAGE = C++
