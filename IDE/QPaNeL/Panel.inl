/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/QPaNeL/Panel.inl
    @brief M�thodes inline de la classe QPNL::QGenericPanel.
    @author @ref Guillaume_Terrissol
    @date 14 Novembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "QTAB/TabWidget.hh"

namespace QPNL
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    template<class TP>
    QGenericPanel<TP>::QGenericPanel(QString title, QString name, QWidget* parent)
        : QPanel(title, name, parent)
    { }


    /*! Destructeur.
     */
    template<class TP>
    QGenericPanel<TP>::~QGenericPanel() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    template<class TP>
    template<class TPage>
    void QGenericPanel<TP>::registerPage()
    {
        // Tout �crire sur une ligne "perturbe" gcc...
        QTAB::QTabWidget*   widget = qobject_cast<QTAB::QTabWidget*>(this->widget());

        Q_ASSERT_X(widget != nullptr, "void QGenericPanel<TP>::registerPage()", "Dock widget isn't a tab widget as expected");

        widget->registerPage<TPage>();
    }
}
