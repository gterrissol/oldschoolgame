/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Panel.hh"

/*! @file IDE/QPaNeL/Panel.cc
    @brief M�thodes (non-inline) des classes QPNL::QPanel::QPanelPrivate & QPNL::QPanel.
    @author @ref Guillaume_Terrissol
    @date 14 Novembre 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QSettings>

#include "QT/MainWindow.hh"
#include "QT/WidgetSerializer.hh"
#include "QTAB/Dock.hh"

namespace QPNL
{
//------------------------------------------------------------------------------
//                       QPanelPrivate : P-Impl de QPanel
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPanel.
        @version 0.5
     */
    class QPanel::QPanelPrivate : public QT::QWidgetPrivateSerializer<QPanel::QPanelPrivate>
    {
        Q_CUSTOM_DECLARE_PUBLIC(QPanel)
    public:
        //! @name "Constructeurs"
        //@{
                QPanelPrivate(QPanel* parent);                      //!< Constructeur.
        void    init();                                             //!< Initialisation.
        //@}
    private:
        //! @name Settings de l'�diteur
        //@{
virtual void    writeCustom(QSettings& settings);                   //!< Ecriture des param�tres suppl�menaires.
virtual void    readCustom(QSettings& settings);                    //!< Lecture des param�tres suppl�menaires.
        //@}
        QTAB::QTabWidget*   tabWidget;                              //!< Onglets.
        QWidget*            applicationMainWidget;                  //!< Widget principal de l'application.
        quint32             currentSelection;                       //!< S�lection actuelle.
 friend class QT::QWidgetPrivateSerializer<QPanel::QPanelPrivate>;  //!< Pour la s�rialisation des param�tres d'interface.
    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QPanel::QPanelPrivate::QPanelPrivate(QPanel* parent)
        : q_custom_ptr(parent)
        , tabWidget(nullptr)
        , applicationMainWidget(nullptr)
        , currentSelection(0x00000000) 
    { }


    /*! 
     */
    void QPanel::QPanelPrivate::init()
    {
        Q_Q(QPanel);

        // La fermeture du panel le d�truit.
//��        q->setAttribute(Qt::WA_DeleteOnClose);

        tabWidget = new QTAB::QTabWidget();
        tabWidget->setObjectName("TabWidget");

        q->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
        q->setWidget(tabWidget);

        // Liaison avec le widget principal du parent (qui est une main window).
        QT::QMainWindow*    mainWindow = qobject_cast<QT::QMainWindow*>(q->parent());
        Q_ASSERT_X(mainWindow != nullptr, "void QPanelPrivate::init()", "Parent widget isn't a QT::QMainWindow");
        applicationMainWidget = mainWindow->centralWidget();
        Q_ASSERT_X(applicationMainWidget != nullptr, "void QPanelPrivate::init()", "No central widget is defined");

        QObject::connect(tabWidget, SIGNAL(destroyed(QObject*)), q, SLOT(close()));

        readSettings();
    }


//------------------------------------------------------------------------------
//                              Settings de l'�diteur
//------------------------------------------------------------------------------

    /*!
     */
    void QPanel::QPanelPrivate::writeCustom(QSettings& settings)
    {
        Q_Q(QPanel);

        settings.setValue("floating", q->isFloating());
    }


    /*!
     */
    void QPanel::QPanelPrivate::readCustom(QSettings& settings)
    {
        Q_Q(QPanel);

        q->setFloating(settings.value("floating", false).toBool());
    }


//------------------------------------------------------------------------------
//                            Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    QPanel::QPanel(QString title, QString name, QWidget* parent)
        : QDockWidget(title, parent)
        , d_custom_ptr(new QPanelPrivate(this))
    {
        Q_D(QPanel);

        setObjectName(name);
        d->init();
    }


    /*! Destructeur.
     */
    QPanel::~QPanel() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QPanel::displayTab(QString tabName)
    {
        Q_D(QPanel);

        d->tabWidget->dock(0)->displayTab(tabName);
    }


    /*!
     */
    QWidget* QPanel::mainWidget() const
    {
        Q_D(const QPanel);

        return d->applicationMainWidget;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void QPanel::closeEvent(QCloseEvent*)
    {
        // Informe l'action connct�e de se d�cocher.
        emit available(false);

        // Enregistre la destruction du widget.
//��        deleteLater();
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! void QPanel::available(bool still)
        Ce signal est �mis lorsque l'objet est d�truit. Les panneaux �tant connect�s � des entr�es de
        menu (via des actions), ces entr�es peuvent �tre d�coch�es gr�ce � ce signal, dont le param�tre
        est toujours Faux
        @param still Toujours faux
     */
}
