/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPNL_ALL_HH
#define QPNL_ALL_HH

/*! @file IDE/QPaNeL/QPaNeL.hh
    @brief Interface publique du module @ref QPaNeL.
    @author @ref Guillaume_Terrissol
    @date 2 Novembre 2005 - 10 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QPNL  //! Panneaux de l'IDE.
{
    /*! @namespace QPNL
        @version 0.5

     */

    /*! @defgroup QPaNeL QPaNeL : Gestion des panneaux de l'IDE
        <b>namespace</b> QPNL.
     */
}

#include "Panel.hh"
#include "Editor.hh"

#endif  // De QPNL_ALL_HH
