/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/QPaNeL/Editor.inl
    @brief M�thodes inline de la classe QPNL::QGenericEditor.
    @author @ref Guillaume_Terrissol
    @date 20 Novembre 2005 - 10 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QPNL
{
//------------------------------------------------------------------------------
//                Editeur G�n�rique : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TType>
    QGenericEditor<TType>::QGenericEditor() { }


    /*! Destructeur.
     */
    template<class TType>
    QGenericEditor<TType>::~QGenericEditor() { }
}
