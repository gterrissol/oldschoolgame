/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPNL_PANEL_HH
#define QPNL_PANEL_HH

#include "QPaNeL.hh"

/*! @file IDE/QPaNeL/Panel.hh
    @brief En-t�te des classes QPNL::QPanel & QPanel::QGenericPanel.
    @author @ref Guillaume_Terrissol
    @date 14 Novembre 2005 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QDockWidget>

#include "QT/Private.hh"

namespace QPNL
{
    /*! @brief Panneau porteur d'outils ou �diteurs/visualiseurs.
        @version 0.3
        @note Juste pour avoir le signal available(bool)
     */
    class QPanel : public QDockWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    QPanel(QString  title,
                           QString  name,
                           QWidget* parent);        //!< Constructeur.
virtual             ~QPanel();                      //!< Destructeur.
        //@}
        void        displayTab(QString tabName);    //!< Affichage d'un onglet.
        QWidget*    mainWidget() const;             //!< Widget principal.


    signals:

        void        available(bool still);          //!< Disponibilit� ?

    protected:

virtual void        closeEvent(QCloseEvent* e);     //!< Traitement de l'�v�nement de fermeture.

    private:

        Q_DISABLE_COPY(QPanel)
        Q_CUSTOM_DECLARE_PRIVATE(QPanel)
     };


    /*! @brief Panneau g�n�rique
        @version 0.2
     */
    template<class TP>
    class QGenericPanel : public QPanel
    {
    public:
        //! @name Contructeur & destructeur
        //@{
                QGenericPanel(QString title, QString name, QWidget* parent);    //!< Constructeur.
virtual         ~QGenericPanel();                                               //!< Destructeur.
        //@}
        template<class TPage>
        void    registerPage();                                                 //!< Enregistrement d'un type de page.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Panel.inl"

#endif  // De QPNL_PANEL_HH
