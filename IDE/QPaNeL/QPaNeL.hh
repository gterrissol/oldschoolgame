    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QPNL_QPANEL_HH
#define QPNL_QPANEL_HH

/*! @file IDE/QPaNeL/QPaNeL.hh
    @brief Pr�-d�clarations du module @ref QPaNeL.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2008 - 18 Ao�t 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QPNL
{
    class QEditor;
    class QEditorPanel;
    class QPanel;

    template<class TType>   class QGenericEditor;
    template<class TP>      class QGenericPanel;
}

#endif  // De QPNL_QPANEL_HH
