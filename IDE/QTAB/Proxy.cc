/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Proxy.hh"

/*! @file IDE/QTAB/Proxy.cc
    @brief M�thodes (non-inline) de la classe QTAB::QProxy.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Bar.hh"
#include "Dock.hh"
#include "Page.hh"
#include "Tab.hh"
#include "TabWidget.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! @param p Page (et son onglet) � d�placer
     */
    QProxy::QProxy(QPage* p)
        : page(p)
        , tab(nullptr)
    {
        Q_ASSERT_X(page != nullptr, "QProxy::QProxy(QPage*)", "Page is null");

        tab = p->tab();

        Q_ASSERT_X(tab != nullptr, "QProxy::QProxy(QPage*)", "Tab is null");
    }


    /*! @param t Onglet (et sa Page) � d�placer
     */
    QProxy::QProxy(QTab* t)
        : page(nullptr)
        , tab(t)
    {
        Q_ASSERT_X(tab != nullptr, "QProxy::QProxy(QTab*)", "Tab is null");

        page = qobject_cast<QPage*>(t->parent());

        Q_ASSERT_X(page != nullptr, "QProxy::QProxy(QTab*)", "Page is null");
    }


//------------------------------------------------------------------------------
//                                     Test
//------------------------------------------------------------------------------

    /*! V�rifie que l'onglet � d�placer peut �tre accept� par le tabwidget <i>w</i>
        @param w Tabwidget sur lequel l'onglet � d�placer pourrait aller
        @return Vrai si l'onglet � d�placer peut �tre �tre ins�r� dans <i>w</i>
     */
    bool QProxy::mayMoveTo(QTabWidget* w) const
    {
        bool    acceptable  = false;

        // Cherche un onglet "compatible".
        for(int i = 0; i < w->tabCount(); ++i)
        {
            if (w->tab(i)->isCompatible(tab))
            {
                acceptable = true;
                break;
            }
        }

        return acceptable;
    }


//------------------------------------------------------------------------------
//                   D�placement des Pages et de leurs Onglets
//------------------------------------------------------------------------------

    /*! D�place l'onglet vers une barre d'onglets, � une position donn�e.
        @param b     Barre vers laquelle d�placer l'onglet
        @param index Position de l'onglet sur sa nouvelle barre (-1 pour le placer � la fin)
        @note L'onglet sera retir� de sa pr�c�dente barre
     */
    void QProxy::moveTo(QBar* b, int index)
    {
        QDock*  dockFrom    = qobject_cast<QDock*>(page->dock());
        QDock*  dockTo      = qobject_cast<QDock*>(b->dock());

        Q_ASSERT_X(dockTo != nullptr, "void QProxy::moveTo(QBar*, int)", "No dock target");

        // Cas tr�s particulier, on le g�re au d�but et on n'y pense plus :
        // drag'n'drop depuis et vers la m�me barre pourvue d'un seul onglet.
        if (dockFrom == dockTo && dockFrom->count() == 1)
        {
            return;
        }

        // Retire l'onglet de sa pr�c�dente barre, s'il y en avait une.
        if (dockFrom != nullptr)
        {
            int from        = dockFrom->indexOf(page);

            if (dockFrom != dockTo || from != index)
            {
                dockFrom->removePage(from);
            }
            else
            {
                return;
            }
        }

        // Ins�re l'onglet dans sa nouvelle barre.
        QBar*   barTo           = dockTo->bar();
        index = dockTo->insertPage(page, index);
        tab->setBar(barTo);
        barTo->insertTab(tab, index);

        // S�lectionne l'onglet d�plac�.
        barTo->setCurrentIndex(index);
    }


    /*! D�place l'onglet vers un dock, identifi� par sa barre de tire.
        @param b     Barre du dock vers lequel d�placer l'onglet
        @note L'onglet sera retir� de son pr�c�dent dock
     */
    void QProxy::moveTo(QDockBar* b)
    {
        QDock*  dockFrom    = page->dock();
        QDock*  dockTo      = b->dock();

        // Cas tr�s particulier, on le g�re au d�but et on n'y pense plus :
        // drag'n'drop depuis et vers la m�me barre pourvue d'un seul onglet.
        if (dockFrom == dockTo && dockFrom->count() == 1)
        {
            return;
        }

        // Retire l'onglet de son pr�c�dent dock, s'il y en avait un.
        if (dockFrom != nullptr)
        {
            int from    = dockFrom->indexOf(page);

            dockFrom->removePage(from);
        }

        // Ajoute l'onglet � son nouveau dock.
        QBar*   barTo = dockTo->bar();
        int index = dockTo->addPage(page);
        tab->setBar(barTo);
        barTo->addTab(tab);
        // S�lectionne l'onglet d�plac�.
        barTo->setCurrentIndex(index);
    }


    /*! D�place l'onglet vers un tabwidget.
        @param w Tabwidget vers lequel d�placer l'onglet
        @param p Position [locale] de d�p�t de l'onglet (pour d�terminer � quel dock l'ajouter)
        @note L'onglet sera retir� de son pr�c�dent tabwidget
     */
    void QProxy::moveTo(QTabWidget* w, QPoint p)
    {
        // Cas tr�s particulier, on le g�re au d�but et on n'y pense plus : drag'n'drop
        // depuis et vers le m�me tabwidget, pourvu d'un seul dock, avec une seule page.
        QTabWidget* widgetFrom  = page->tabWidget();
        if (widgetFrom == w && widgetFrom->count() == 1 && widgetFrom->dock(0)->count() == 1)
        {
            return;
        }

        QDock*  newDock = w->createDock(p);

        // Enregistre les types d'onglets.
        for(int i = 0; i < w->tabCount(); ++i)
        {
            newDock->registerTab(w->tab(i)->clone());
        }

        QDock*  dockFrom = page->dock();

        // Retire l'onglet de son pr�c�dent tabwidget, s'il y en avait un.
        if (dockFrom != nullptr)
        {
            int from = dockFrom->indexOf(page);

            dockFrom->removePage(from);
        }

        // Ajoute l'onglet � son nouveau tabwidget.
        QBar*   newBar = newDock->bar();
        newDock->addPage(page);
        tab->setBar(newBar);
        newBar->addTab(tab);
    }


    /*! D�place l'onglet en dehors de tout tabwidget (un nouveau sera cr�� pour accueillir cet onglet).
     */
    void QProxy::moveToOutside()
    {
        QTabWidget* widget = moveOut();

        widget->show();
    }


    /*! D�place l'onglet en dehors de tout tabwidget (un nouveau sera cr�� pour accueillir cet onglet).
        @param p Position o� cr�er le nouveau tabwidget
     */
    void QProxy::moveToOutside(QPoint p)
    {
        QTabWidget* widget = moveOut();

        widget->move(p);
        widget->show();
    }


    /*! Cr�e un nouveau tabwidget et y place l'onglet � d�placer.
    
        @return Le tabwidget cr��
     */
    QTabWidget* QProxy::moveOut()
    {
        QDock*      dockFrom    = page->dock();
        QTabWidget* widgetFrom  = dockFrom->tabWidget();
        int         from        = dockFrom->indexOf(page);

        // 1 - Cr�e un clone vide.
        QTabWidget* newWidget   = new QTabWidget(widgetFrom->tabDefaultStyle(), widgetFrom->barDefaultStyle());
        QDock*      newDock     = newWidget->dock(0);

        // Copie, dans le 1er (et unique) dock, les types d'onglets valides.
        for(int i = 0; i < widgetFrom->tabCount(); ++i)
        {
            newWidget->registerTab(widgetFrom->tab(i)->clone());
        }

        // Retire la page (et son onglet) du widget.
        QPage*  page = dockFrom->removePage(from);
        page->setParent(nullptr);

        // Ajoute manuellement la page au nouveau widget, et l'onglet � sa barre.
        QBar*   newBar  = newDock->bar();
        newDock->addPage(page);
        page->tab()->setBar(newBar);
        newBar->addTab(page->tab());

        return newWidget;
    }
}
