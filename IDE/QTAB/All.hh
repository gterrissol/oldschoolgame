/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_ALL_hh
#define QTAB_ALL_hh

/*! @file IDE/QTAB/QTAB.hh
    @brief Interface publique du module @ref QTAB.
    @author @ref Guillaume_Terrissol
    @date 13 Ao�t 2005 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */
 
#include "QT/QT.hh"

namespace QTAB   //! D�finition d'un TabWidget am�lior�.
{
    /*! @namespace QTAB
        @version 1.0

        Ce module d�finit un ensemble de classes permettant la d�finition et l'utilisation de widgets porteurs d'onglets.<br>
        La classe QTabBar de Qt 4 propose bon nombre de fonctionnalit�s, mais il �tait impossible d'ajouter celles souhait�es pour l'IDE d'SG
        en d�rivant et r�impl�mentant quelques m�thodes. J'ai donc cr�� un ensemble de classes pour obtenir un tabwidget reprennant la plupart
        des fonctionnalit�s de ceux pr�sents dans The GIMP 2 (principale - pour ne pas dire unique - source d'inspiration pour ce module).<br>
        Les nouveaut�s par rapport � ::QTabWidget sont :
        - la possibilit� de placer dynamiquement plusieurs "tabwidgets" dans un m�me widget,
        - le drag'n'drop d'onglets entre tabwidgets,
        - diff�rents styles de barre d'onglets,
        - diff�rents styles d'onglets.

        Un menu g�n�ral permet :
        - de cr�er, d�tacher ou fermer un onglet, 
        - d'acc�der au menu d'un onglet (si d�fini),
        - d'afficher le contenu d'un onglet sous forme de liste ou de grille,
        - de changer l'�chelle du contenu d'un onglet

        @note Les �l�ments de QTAB �tant destin�s � �tre utilis�s comme des widgets de base, toutes les conventions de nommage sont celles de
        Qt
        @note Dans la documentation de ce module, les termes <b>onglet</b> et <b>page</b> sont parfois utilis�s l'un pour l'autre, m�me si,
        pour �tre pr�cis, un onglet est le fragment visuel situ� en haut d'un tabwidget permettant, lorsqu'il est s�lectionn�, d'afficher la
        page qui lui est associ�e, et est affich�e, elle, sous la barre de titre du tabwidget (QDock)
        @sa @ref QTAB_Page
     */

    /*! @defgroup QTAB QTAB : Onglets Qt
        <b>namespace</b> QTAB.
     */
}

#include "Tab.hh"
#include "Page.hh"
#include "TabWidget.hh"

#endif  // De QTAB_ALL_hh
