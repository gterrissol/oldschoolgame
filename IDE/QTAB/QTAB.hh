/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_QTAB_hh
#define QTAB_QTAB_hh

/*! @file IDE/QTAB/QTAB.hh
    @brief Pr�-d�clarations du module @ref QTAB.
    @author @ref Guillaume_Terrissol
    @date 13 Ao�t 2005 - 18 Ao�t 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */
 
namespace QTAB
{
    class QTab;
    class QBar;
    class QPage;
    class QDockBar;
    class QDock;
    class QTabWidget;
    class QProxy;
    class QDragTab;
}

#endif  // De QTAB_QTAB_hh
