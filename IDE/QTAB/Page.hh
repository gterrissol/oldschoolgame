/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_PAGE_HH
#define QTAB_PAGE_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/Page.hh
    @brief En-t�te de la classe QTAB::QPage.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

#include "QT/Private.hh"

class QMenu;

namespace QTAB
{
    /*!@brief Page accessible via onglet.
        @version 1.0

        Pour �tre ins�r� dans un tabwidget, un widget doit d�river de cette classe, afin de proposer un
        comportement standard pour toutes les pages.<br>
        Consultez la section @ref QTAB_Page pour une explication d�taill�e sur l'utilisation de cette
        classe.
     */
    class QPage : public QWidget
    {
        Q_OBJECT
    public:
        //! Taille du contenu d'un widget avec onglet.
        enum ContentsSize
        {
            Tiny,                                               //!< Minuscule.
            VerySmall,                                          //!< Tr�s petit.
            Small,                                              //!< Petit.
            Medium,                                             //!< Moyen.
            Large,                                              //!< Grand.
            VeryLarge,                                          //!< Tr�s grand.
            Huge,                                               //!< Enorme.
            Boundless,                                          //!< Immense.
            Gigantic,                                           //!< Gigantesque.
            SizeCount                                           //!< Nombre de tailles disponibles.
        };
        //! @name Constructeur & destructeur
        //@{
                        QPage(bool resizable = true,
                              bool listable  = true,
                              bool gridable  = true);           //!< Constructeur [par d�faut].
virtual                 ~QPage();                               //!< Destructeur.
        //@}
        //! @name Ascendants
        //@{
        QDock*          dock() const;                           //!< Dock.
        QTabWidget*     tabWidget() const;                      //!< Widget.
        //@}
        //! @name Propri�t�s
        //@{
        bool            isContentsResizable() const;            //!< Contenu redimensionnable ?
        bool            mayDisplayAsList() const;               //!< Contenu affichable sous forme de liste ?
        bool            mayDisplayAsGrid() const;               //!< Contenu affichable sous forme de grille ?
        QMenu*          menu() const;                           //!< Menu du widget.
        //! @name Etat actuel
        //@{
        QTab*           tab() const;                            //!< Onglet associ�.
        QIcon           status() const;                         //!< Ic�ne repr�sentant l'�tat actuel du widget.
        ContentsSize    currentSize() const;                    //!< "Echelle" du contenu du widget.
        bool            isDisplayingList() const;               //!< Contenu affich� sous forme de liste ?
        bool            isDisplayingGrid() const;               //!< Contenu affich� sous forme de grille ?
        //@}

    public slots:
        //! @name Slots
        //@{
        void            setContentsSize(QAction* sizeAction);   //!< Changement de l'"�chelle" du widget.
        void            displayAsList(bool list);               //!< Affichage du contenu sous forme de liste.
        void            displayAsGrid(bool grid);               //!< Affichage du contenu sous forme de grille.


    protected slots:

        void            statusChanged();                        //!< Changement de statut.
        //@}

    protected:
        //! @name Changement de langue
        //@{
virtual void            changeEvent(QEvent* e);                 //!< Traitement d'un changement (dont celui de langue).
virtual void            onLanguageChange();                     //!< Mise � jour de l'interface suite � un changement de langue.
        //@}

    private slots:
        //! @name Comportement � r�impl�menter
        //@{
virtual void            manageMenuEntry(QAction* entry);        //!< Gestion d'une entr�e du menu du widget.


    private:

virtual void            fillPageMenu(QMenu*& pageMenu) const;   //!< Cr�ation du menu du widget.
virtual QIcon           statusIcon() const;                     //!< Ic�ne repr�sentant l'�tat actuel du widget.
virtual void            changeContentsSize(ContentsSize sz);    //!< Changement de l'"�chelle" du widget.
virtual void            updateLayout();                         //!< Mise � jour de l'organisation (liste ou grille).
        //@}
        Q_DISABLE_COPY(QPage)
        Q_CUSTOM_DECLARE_PRIVATE(QPage)
    };
}

#endif  // QTAB_PAGE_HH
