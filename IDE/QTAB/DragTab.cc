/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "DragTab.hh"

/*! @file IDE/QTAB/DragTab.cc
    @brief M�thodes (non-inline) de la classe QTAB::QDragTab.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QIcon>

#include "Bar.hh"
#include "Dock.hh"
#include "Proxy.hh"
#include "Tab.hh"
#include "TabWidget.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param t Onglet (et sa page) � glisser/d�poser
     */
    QDragTab::QDragTab(QTab* t)
        : QDragObject(t)
    { }


    /*! Destructeur.
     */
    QDragTab::~QDragTab() { }


//------------------------------------------------------------------------------
//                        Comportement du Glisser/D�poser
//------------------------------------------------------------------------------

    /*! @return L'ic�ne de l'onglet dragg�
     */
    QIcon QDragTab::icon() const
    {
        return qobject_cast<QTab*>(dragged())->icon();
    }


    /*! @return Le titre de l'onglet dragg�
     */
    QString QDragTab::title() const
    {
        return qobject_cast<QTab*>(dragged())->title();
    }


    /*! @param w Widget sur lequel tester la possibilit� de d�pot de l'onglet dragg�
        @param p Position [globale] pour laquelle tester la possibilit� de d�p�t de l'onglet dragg�
        @return Vrai si dragged() peut �tre d�pos� sur le widget w, � la position <i>p</i>
     */
    bool QDragTab::mayDropOn(QWidget* w, QPoint) const
    {
        QProxy  proxy(qobject_cast<QTab*>(dragged()));

        if (QBar* b = qobject_cast<QBar*>(w))
        {
            QTabWidget* target = b->tabWidget();
            return proxy.mayMoveTo(target);
        }
        if (QDockBar* b = qobject_cast<QDockBar*>(w))
        {
            QTabWidget* target = b->tabWidget();
            return proxy.mayMoveTo(target);
        }
        if (QTabWidget* target = qobject_cast<QTabWidget*>(w))
        {
            return proxy.mayMoveTo(target);
        }
        if (w == nullptr)
        {
            return true;
        }

        return false;
    }


    /*! D�pose l'onglet sur une instance de QBar, QDockBar, ou QTabWidget.
        @param w Widget sur lequel d�poser l'onglet
        @param p Position [locale si <i>w</i> existe, globale sinon] � laquelle d�poser l'onglet (surtout
        utile si <i>w</i> est un tabwidget)
     */
    void QDragTab::dropOn(QWidget* w, QPoint p)
    {
        QTab*   tab = qobject_cast<QTab*>(dragged());
        QProxy  proxy(tab);

        if      (QBar* bar = qobject_cast<QBar*>(w))
        {
            proxy.moveTo(bar, bar->indexAtPos(p));
        }
        else if (QDockBar* bar = qobject_cast<QDockBar*>(w))
        {
            proxy.moveTo(bar);
        }
        else if (QTabWidget* widget = qobject_cast<QTabWidget*>(w))
        {
            proxy.moveTo(widget, p);
        }
        else
        {
            proxy.moveToOutside(p);
        }
    }


//------------------------------------------------------------------------------
//                           Surbrillance de la cible
//------------------------------------------------------------------------------

    /*! Il peut arriver que le widget sur lequel d�poser l'onglet soit l'enfant de celui sur lequel
        d�poser effectivement l'onglet (ou qu'il lui soit li� d'une quelconque autre mani�re). Cette
        m�thode permet de r�cup�rer la vraie cible du d�p�t.
        @param w Widget sur lequel le d�p�t est propos� (ou nullptr, en cas de drag vers l'"ext�rieur")
        @return Le vrai widget sur lequel d�poser l'onglet (ou nullptr, en cas de drag vers l'"ext�rieur")
     */
    QWidget* QDragTab::buddy(QWidget* w) const
    {
        // Deux type de widget peuvent �tre d�cor�s : les tabwidgets, et les docks.
        if      (QBar* b = qobject_cast<QBar*>(w))
        {
            w = b->dock();
        }
        else if (QDockBar* b = qobject_cast<QDockBar*>(w))
        {
            w = b->dock();
        }

        return w;
    }
}
