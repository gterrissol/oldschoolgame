/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Dock.hh"

/*! @file IDE/QTAB/Dock.cc
    @brief M�thodes (non-inline) des classes QTAB::QDock::QDockPrivate, QTAB::QDockBar & QTAB::QDock.
    @author @ref Guillaume_Terrissol
    @date 13 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QActionGroup>
#include <QApplication>
#include <QDesktopWidget>
#include <QFrame>
#include <QHBoxLayout>
#include <QMenu>
#include <QMouseEvent>
#include <QPushButton>
#include <QStackedWidget>
#include <QStylePainter>
#include <QStyleOptionTabWidgetFrame>

#include "Bar.hh"
#include "DragTab.hh"
#include "Page.hh"
#include "Proxy.hh"
#include "Tab.hh"
#include "TabWidget.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                        QDockPrivate : P-Impl de QDock
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QDock.
        @version 1.0

        A l'image de ce qui se fait pour QBar, cette classe fournit une grande partie de l'imp�mentation
        de QDock.
        @note Cette classe reprend quelques lignes de code de la version de QTabWidget (version Qt 4)
     */
    class QDock::QDockPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QDock)
    public:
        //! @name "Constructeurs"
        //@{
                                    QDockPrivate(QDock* parent);    //!< Constructeur.
                void                init();                         //!< Initialisation.
        //@}
        QStyleOptionTabWidgetFrame  getStyleOption() const;         //!< Options d'affichage.
        void                        onLanguageChange();             //!< Mise � jour de l'interface suite � un changement de langue.

        //! @name Widgets
        //@{
        QBar*           tabs;                                       //!< Barre d'onglets.
        QDockBar*       dockBar;                                    //!< Barre de dock.
        QStackedWidget* pages;                                      //!< Pages.
        //@}
        //! @name Autres attributs
        //@{
        typedef QMap<QString, QTab*>    TabContainer;               //!< Type du conteneur des onglets "locaux".
        TabContainer    tabData;                                    //!< Onglets "locaux" (voir QDock::registerTab(QTab*)).
        QRect           panelRect;                                  //!< G�om�trie du cadre du dock.
        //@}
    };


//------------------------------------------------------------------------------
//                        QDockPrivate : "Constructeurs"
//------------------------------------------------------------------------------

    /*! @param parent Dock pour lequel porter des informations et fournir du comportement
     */
    QDock::QDockPrivate::QDockPrivate(QDock* parent)
        : q_custom_ptr(parent)
        , tabs(nullptr)
        , dockBar(nullptr)
        , pages(nullptr)
    { }


    /*! Cette m�thode est appel�e une fois que le dock et "son impl�mentation" ont �t� li�s.
     */
    void QDock::QDockPrivate::init()
    {
        Q_Q(QDock);

        // Premi�re ligne : barre d'onglets.
        tabs                        = new QBar(q);

        // Deuxi�me ligne : titre et bouton de gestion des onglets
        // (dans une classe annexe, pour g�rer plus facilement le drag'n'drop).
        dockBar                     = new QDockBar(q);

        // Troisi�me ligne : pages associ�es aux onglets.
        pages                       = new QStackedWidget(q);

        // Connections.
        QObject::connect(tabs, SIGNAL(currentChanged(int)), q, SLOT(tabSelected(int)));

        // Param�tres g�n�raux.
        q->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        q->setFocusPolicy(Qt::TabFocus);
        q->setFocusProxy(tabs);
        q->setAttribute(Qt::WA_DeleteOnClose);
        q->setAcceptDrops(true);
    }


//------------------------------------------------------------------------------
//                        QDockPrivate : Autres M�thodes
//------------------------------------------------------------------------------

    /*! Cette m�thode permet de r�cup�rer les options de rendu du dock, en fonction de son �tat actuel
        (taille, barre d'onglets,...).
        @return Les options d'affichage pour le dock
     */
    QStyleOptionTabWidgetFrame QDock::QDockPrivate::getStyleOption() const
    {
        Q_Q(const QDock);

        QStyleOptionTabWidgetFrame option;
        option.init(q);
        // J'ai trouv� plus simple de g�rer moi-m�me la pr�sence
        // de widgets dans les coins par rapport au cadre du dock.
        option.leftCornerWidgetSize  = QSize(0, 0);
        option.rightCornerWidgetSize = QSize(0, 0);

        option.shape                 = QTabBar::Shape(tabs->shape());
        option.tabBarSize            = tabs->sizeHint();

        return option;
    }


    /*! Cette m�thode est appel�e lorsque l'application a effectu� un changement de langue.
        @sa @ref QT_Translation_Page
     */
    void QDock::QDockPrivate::onLanguageChange()
    {
        // 1 - R�cup�rer les noms traduits des onglets et
        // r�enregsitrer ces derniers avec leurs "nouveaux" noms.
        TabContainer    newTabData;
        foreach(QTab* tab, tabData)
        {
            QString newName = tab->name();
            newTabData[newName] = tab;
        }
        // Pourquoi les QMap ne proposent-elles pas la m�thode swap() ?
        tabData = newTabData;

        // 2 - Mettre � jour l'interface.
        QPage*  p     = qobject_cast<QPage*>(pages->currentWidget());
        QString title = p->tab()->title();

        dockBar->setText(title);
    }


//------------------------------------------------------------------------------
//                     QDockBar : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param parent Dock parent
     */
    QDockBar::QDockBar(QDock* parent)
        : QLabel(parent)
        , drag(nullptr)
    {
        setFrameStyle(QFrame::NoFrame);
        setMinimumSize(QSize(24, 24));

        QHBoxLayout*    mainLayout  = new QHBoxLayout(this);
        mainLayout->setMargin(3);
        mainLayout->setSpacing(0);

        mainLayout->addItem(new QSpacerItem(16, 8, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

        QPushButton*    menuButton  = new QPushButton(QIcon(":/icons/menu arrow"), "", this);
        mainLayout->addWidget(menuButton);
        menuButton->setMinimumSize(QSize(16, 16));
        menuButton->setMaximumSize(QSize(16, 16));

        QPushButton*    closeButton = new QPushButton(QIcon(":/icons/close tab"), "", this);
        mainLayout->addWidget(closeButton);
        closeButton->setMinimumSize(QSize(16, 16));
        closeButton->setMaximumSize(QSize(16, 16));

        connect(menuButton,  SIGNAL(clicked()), parent, SLOT(callMenu()));
        connect(closeButton, SIGNAL(clicked()), parent, SLOT(closePage()));
    }


    /*! Destructeur.
     */
    QDockBar::~QDockBar() { }


//------------------------------------------------------------------------------
//                             QDockBar : Ascendants
//------------------------------------------------------------------------------

    /*! Permet de conna�tre le dock (parent) auquel appartient cette barre.
        @return Le dock parent
     */
    QDock* QDockBar::dock() const
    {
        return qobject_cast<QDock*>(parent());
    }


    /*! Permet de conna�tre le tabwidget (grand-parent) auquel appartient le dock de cette barre.
        @return Le tabwidget grand-parent
     */
    QTabWidget* QDockBar::tabWidget() const
    {
        QDock*  d = dock();
        
        if (d != nullptr)
        {
            return d->tabWidget();
        }
        else
        {
            return nullptr;
        }
    }


//------------------------------------------------------------------------------
//                            QDockBar : Drag'n'Drop
//------------------------------------------------------------------------------

    /*! D�bute ou poursuit une op�ration de glisser/d�poser.
        @param e Ev�nement d�clench� par le d�placement de la souris
     */
    void QDockBar::mouseMoveEvent(QMouseEvent* e)
    {
        // Drag toujours sur bouton gauche.
        if (e->buttons() == Qt::LeftButton)
        {
            if (drag == nullptr)
            {
                if (QApplication::startDragDistance() <= (e->pos() - dragStartPosition).manhattanLength())
                {
                    // Rappel : le parent pass� au constructeur est un QDock*.
                    drag = new QDragTab(dock()->currentTab());
                    drag->drag(e->globalPos());
                }
            }
            else
            {
                drag->move(e->globalPos());
            }
        }
    }


    /*! Pr�pare une op�ration de glisser/d�poser.
        @param e Ev�nement d�clench� par un clic (pression) souris
     */
    void QDockBar::mousePressEvent(QMouseEvent* e)
    {
        // Le drag ne commencera qu'apr�s parcours d'une petite distance.
        if (e->button() == Qt::LeftButton)
        {
            dragStartPosition = e->pos();
        }
    }


    /*! Ach�ve une op�ration de glisser/d�poser.
        @param e Ev�nement d�clench� par un clic (rel�chement) souris
     */
    void QDockBar::mouseReleaseEvent(QMouseEvent* e)
    {
        if (e->button() == Qt::LeftButton)
        {
            if (drag != nullptr)
            {
                // C'est l'objet drag qui se chargera d'accepter, ou non, le d�p�t.
                drag->drop(e->globalPos());

                delete drag;
                drag = nullptr;
            }
        }
    }


//------------------------------------------------------------------------------
//                      QDock : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param parent Tabwidget auquel appartient ce dock
     */
    QDock::QDock(QTabWidget* parent)
        : QWidget(parent)
        , d_custom_ptr(new QDockPrivate(this))
    {
        Q_D(QDock);

        d->init();
    }


    /*! Destructeur.
     */
    QDock::~QDock()
    {
        Q_D(QDock);

        foreach(QTab* ptr, d->tabData)
        {
            delete ptr;
        }
    }


//------------------------------------------------------------------------------
//                                QDock : Famille
//------------------------------------------------------------------------------

    /*! Permet de conna�tre la barre d'onglets qui appartient � ce dock.
        @return La barre d'onglets de ce dock
     */
    QBar* QDock::bar() const
    {
        Q_D(const QDock);

        return d->tabs;
    }


    /*! Permet de conna�tre le tabwidget (parent) auquel appartient ce dock.
        @return Le tabwidget parent
     */
    QTabWidget* QDock::tabWidget() const
    {
        return qobject_cast<QTabWidget*>(parent());
    }


//------------------------------------------------------------------------------
//                          QDock : Gestion des Onglets
//------------------------------------------------------------------------------

    /*! Enregistre un type d'onglet (dit onglet "local") pour ce dock. Les onglets "locaux" sont destin�s
        � cr�er les onglets � placer dans une barre d'onglets (et pas � y �tre plac�s eux-m�mes : voir
        QTab::clone() const).
        @param tab Onglet � enregistrer
     */
    void QDock::registerTab(QTab* tab)
    {
        Q_D(QDock);

        QString tabText = tab->name();

        if (d->tabData.find(tabText) == d->tabData.end())
        {
            d->tabData[tabText] = tab;
        }
    }


    /*! Ajoute un onglet (d�j� enregistr�) et sa page (apr�s leur construction) au dock.
        @param name Nom de l'onglet (et de la page) � ajouter (afficher) au dock
     */
    void QDock::displayTab(QString name)
    {
        Q_D(QDock);

        Q_ASSERT_X(d->tabData.find(name) != d->tabData.end(), "void QDock::displayTab(QString)", "Unknow tab");

        const QTab* tab = d->tabData[name];
        // La cr�ation de la page ajoute un nouvel onglet � la barre.
        QPage*      p   = tab->createPage();

        QProxy  proxy(p);
        proxy.moveTo(d->dockBar);
    }


    /*! Active un onglet et sa page, d'apr�s leur indice
        @param index Indice de l'onglet � activer
     */
    void QDock::activateTab(int index)
    {
        bar()->setCurrentIndex(index);
    }


//------------------------------------------------------------------------------
//                  QDock : Etats des Pages et de leurs Onglets
//------------------------------------------------------------------------------

    /*! @return La page actuellement affich�e
     */
    QPage* QDock::currentPage() const
    {
        return page(currentIndex());
    }


    /*! @return L'onglet actuellement s�lectionn�
     */
    QTab* QDock::currentTab() const
    {
        return currentPage()->tab();
    }


    /*! @return L'indice de l'onglet et de la page actuels (c'est le m�me indice)
     */
    int QDock::currentIndex() const
    {
        return bar()->currentIndex();
    }


    /*! @return Le nombre d'onglets (ou de pages, c'est le m�me) actuellement affich�s dans le dock (m�me
        s'ils ne sont pas forc�ment visibles).
     */
    int QDock::count() const
    {
        return bar()->count();
    }


//------------------------------------------------------------------------------
//                              QDock : Dimensions
//------------------------------------------------------------------------------

    /*! @return La taille id�ale que devrait avoir le dock
     */
    QSize QDock::sizeHint() const
    {
        Q_D(const QDock);

        QStyleOption opt(0);
        opt.init(this);
        opt.state = QStyle::State_None;
        {
            QDock*  that = const_cast<QDock*>(this);
            that->setUpLayout(true);
        }
        QSize   t = d->tabs->sizeHint();
        QSize   b = d->dockBar->sizeHint();
        // La barre du dock prend un peu de place qui doit �tre rendue aux pages.
        QSize   p = d->pages->sizeHint() + QSize(0, b.height());

        if(!style()->styleHint(QStyle::SH_TabBar_PreferNoArrows, &opt, d->tabs))
        {
            t = t.boundedTo(QSize(200, 200));
        }
        else
        {
            t = t.boundedTo(QApplication::desktop()->size());
        }

        QSize   sz(qMax(t.width(), qMax(b.width(), p.width())), t.height() + b.height() + p.height());

        return style()->sizeFromContents(QStyle::CT_TabWidget, &opt, sz, this).expandedTo(QApplication::globalStrut());
    }


    /*! @return La taille minimale avec laquelle que devrait �tre affich� le dock
     */
    QSize QDock::minimumSizeHint() const
    {
        Q_D(const QDock);

        {
            QDock*  that = const_cast<QDock*>(this);
            that->setUpLayout(true);
        }
        QSize   t = d->tabs->minimumSizeHint();
        QSize   b = d->dockBar->minimumSizeHint();
        // La barre prend un peu de place qui doit �tre rendue aux pages.
        QSize   p = d->pages->minimumSizeHint() + QSize(0, b.height());
        QSize   sz(qMax(t.width(), qMax(b.width(), p.width())), t.height() + b.height() + p.height());

        QStyleOption opt(0);
        opt.rect    = rect();
        opt.palette = palette();
        opt.state   = QStyle::State_None;

        return style()->sizeFromContents(QStyle::CT_TabWidget, &opt, sz, this).expandedTo(QApplication::globalStrut());
    }


//------------------------------------------------------------------------------
//                         QDock : Changement de Langue
//------------------------------------------------------------------------------

    /*! Cette m�thode est appel�e lorsque l'application a effectu� un changement de langue.
        @sa @ref QT_Translation_Page
     */
    void QDock::onLanguageChange()
    {
        Q_D(QDock);

        d->onLanguageChange();
        bar()->updateTabs();
    }


//------------------------------------------------------------------------------
//                        QDock : Agencement des Widgets
//------------------------------------------------------------------------------

    /*! Tient compte d'un �ventuel changement de style pour r�agencer les composants du dock.
        @param e Ev�nement � traiter
     */
    void QDock::changeEvent(QEvent* e)
    {
        if      (e->type() == QEvent::StyleChange)
        {
            setUpLayout();
        }
        else if (e->type() == QEvent::LanguageChange)
        {
            onLanguageChange();
            e->accept();
        }
        QWidget::changeEvent(e);
    }


    /*! Affiche le cadre du dock.
        @param e Ev�nement commandant le dessin du dock.
     */
    void QDock::paintEvent(QPaintEvent* e)
    {
        Q_D(QDock);

        QWidget::paintEvent(e);

        QStylePainter p(this);
        QStyleOptionTabWidgetFrame opt;
        opt = d->getStyleOption();
        opt.rect = d->panelRect;
        // R�initialise la taille de la barre, afin de dessiner le cadre en entier.
        opt.tabBarSize = QSize();

        p.drawPrimitive(QStyle::PE_FrameTabWidget, opt);
    }


    /*! Traite le redimensionnement du widget en r�agen�ant les composants du dock.
        @param e Ev�nement de redimensionnement du dock
     */
    void QDock::resizeEvent(QResizeEvent* e)
    {
        QWidget::resizeEvent(e);
        setUpLayout();
    }


    /*! Provoque le r�agencement des composants du dock suite � son affichage.
        @param e Ev�nement commandant l'affichage du dock
     */
    void QDock::showEvent(QShowEvent* e)
    {
        QWidget::showEvent(e);
        setUpLayout();
    }


    /*! R�agence les composants du dock (barre d'onglet, barre de titre, conteneur de pages).
        @param onlyCheck Vrai s'il s'agit juste de v�rifier les g�om�tries des composants, Faux pour
        forcer la mise � jour de la g�om�trie du dock
     */
    void QDock::setUpLayout(bool onlyCheck)
    {
        Q_D(QDock);

        d->pages->adjustSize();

        QStyleOptionTabWidgetFrame  option = d->getStyleOption();

        QRect   tabRect         = style()->subElementRect(QStyle::SE_TabWidgetTabBar, &option, this);
        d->panelRect            = style()->subElementRect(QStyle::SE_TabWidgetTabPane, &option, this);
        QRect   contentsRect    = style()->subElementRect(QStyle::SE_TabWidgetTabContents, &option, this);

        // Barre d'onglets.
        tabRect.setWidth(d->panelRect.width());
        bar()->setGeometry(tabRect);
        bar()->updateTabs();

        // Cadre principal.
        QStyleOptionTab tabOverlap;
        tabOverlap.shape = QTabBar::Shape(bar()->shape());
        d->panelRect.setTop(tabRect.height() - style()->pixelMetric(QStyle::PM_TabBarBaseOverlap, &tabOverlap, this));

        // "Barre" du dock.
        d->dockBar->setGeometry(QRect(contentsRect.left(), contentsRect.top(), contentsRect.width(), d->dockBar->height()));

        // Pages.
        contentsRect.setTop(d->dockBar->geometry().bottom());
        d->pages->setGeometry(contentsRect);

        if (!onlyCheck)
        {
            update();
            updateGeometry();
        }
    }


//------------------------------------------------------------------------------
//                      QDock : Gestion des pages
//------------------------------------------------------------------------------

    /*! Ajoute une page au dock.
        @param p Page � ajouter
        @return L'indice de la page une fois ajout�e au dock
        @note L'onglet devra �tre ajout� s�par�ment
     */
    int QDock::addPage(QPage* p)
    {
        return insertPage(p, -1);
    }


    /*! Ins�re une page dans le conteneur de pages.
        @param p     Page � ins�rer
        @param index Position dans le conteneur o� ins�rer la page (-1 pour l'ajouter � la fin)
        @return La position r�elle de la page une fois ins�r�e dans le conteneur
        @note L'onglet devra �tre ins�r� s�par�ment
     */
    int QDock::insertPage(QPage* p, int index)
    {
        Q_D(QDock);

        index = d->pages->insertWidget(index, p);
        setUpLayout();

        return index;
    }


    /*! Permet de retrouver l'indice d'une page donn�e.
        @param p Page dont on cherche l'indice
        @return L'indice de la page <i>p</i>
     */
    int QDock::indexOf(QPage* p) const
    {
        Q_D(const QDock);

        return d->pages->indexOf(p);
    }


    /*! Permet de trouver une page � partir de son indice.
        @param index Indice de la page recherch�e
        @return La page d'indice <i>index</i>
     */
    QPage* QDock::page(int index) const
    {
        Q_D(const QDock);

        return qobject_cast<QPage*>(d->pages->widget(index));
    }


    /*! Retire une page du dock.
        @param index Indice de la page � retirer
        @return La page retir�e
        @note La page ne sera pas d�truite
        @note Le dock sera ferm� s'il ne contient plus de page
     */
    QPage* QDock::removePage(int index)
    {
        Q_D(QDock);

        QPage*  p   = page(index);

        bar()->removeTab(index);
        d->pages->removeWidget(p);
        p->setParent(nullptr);

        tabSelected(currentIndex());

        // Si plus aucun onglet n'est affich�, ferme ce widget.
        if (count() == 0)
        {
            close();
        }
        else
        {
            setUpLayout();
        }

        return p;
    }


    /*! Ferme une page donn�e.
        @param index Indice de la page � fermer
     */
    void QDock::closePage(int index)
    {
        QPage*  p   = removePage(index);
        // L'onglet associ�e � la page �tant son enfant, il sera d�truit automatiquement.
        delete p;
    }


    /*! D�tache une page donn�e.
        @param index Indice de la page � d�tacher
     */
    void QDock::deDockPage(int index)
    {
        QPage*  p   = page(index);

        QProxy  proxy(p);
        proxy.moveToOutside();
    }


//------------------------------------------------------------------------------
//                          QDock : Cr�ation des Menus
//------------------------------------------------------------------------------

    /*! @return Le menu de cr�ation de pages, construit � partir de tous les types d'onglets ("locaux")
        disponibles.
     */
    QMenu* QDock::createPageMenu() const
    {
        Q_D(const QDock);

        Q_ASSERT_X(!d->tabData.isEmpty(), "QMenu* QDock::createPageMenu() const", "No registered page");

        QMenu*  createMenu  = new QMenu(tr("&Ajouter un onglet"));

        // Remplit le menu.
        foreach(QTab* tab, d->tabData.values())
        {
            createMenu->addAction(tab->icon(), tab->title());
        }

        connect(createMenu, SIGNAL(triggered(QAction*)), SLOT(createPage(QAction*)));

        return createMenu;
    }


    /*! @return Le menu de changement d'�chelle du contenu de la page actuelle
     */
    QMenu* QDock::pageContentsSizeMenu() const
    {
        QMenu*      sizeMenu                    = new QMenu(tr("Taille de l'aper�&u"));
        QAction*    actions[QPage::SizeCount]   = { nullptr };

        actions[QPage::Tiny]        = sizeMenu->addAction(tr("Min&uscule"));
        actions[QPage::VerySmall]   = sizeMenu->addAction(tr("Tr�s pe&tit"));
        actions[QPage::Small]       = sizeMenu->addAction(tr("&Petit"));
        actions[QPage::Medium]      = sizeMenu->addAction(tr("&Moyen"));
        actions[QPage::Large]       = sizeMenu->addAction(tr("&Grand"));
        actions[QPage::VeryLarge]   = sizeMenu->addAction(tr("Tr�s gr&and"));
        actions[QPage::Huge]        = sizeMenu->addAction(tr("&Enorme"));
        actions[QPage::Boundless]   = sizeMenu->addAction(tr("&Immense"));
        actions[QPage::Gigantic]    = sizeMenu->addAction(tr("Gigantes&que"));

        QActionGroup*   group    = new QActionGroup(sizeMenu);
        for(int i = 0; i < QPage::SizeCount; ++i)
        {
            actions[i]->setData(i);
            actions[i]->setActionGroup(group);
            actions[i]->setCheckable(true);
            actions[i]->setChecked(false);
        }

        // Coche l'entr�e correspondant � l'�tat actuel de l'onglet.
        actions[currentPage()->currentSize()]->setChecked(true);

        connect(sizeMenu, SIGNAL(triggered(QAction*)), currentPage(), SLOT(setContentsSize(QAction*)));
        connect(sizeMenu, SIGNAL(triggered(QAction*)), SLOT(setUpLayout()));

        return sizeMenu;

    }


    /*! @return Le menu de changement de style de l'onglet actuel
     */
    QMenu* QDock::tabStyleMenu() const
    {
        QMenu*      styleMenu                   = new QMenu(tr("&Style d'onglet"));
        QAction*    actions[QTab::StyleCount]   = { nullptr };

        actions[QTab::Icon]             = styleMenu->addAction(tr("&Ic�ne"));
        actions[QTab::Status]           = styleMenu->addAction(tr("&Etat courant"));
        actions[QTab::Text]             = styleMenu->addAction(tr("&Texte"));
        actions[QTab::IconAndText]      = styleMenu->addAction(tr("I&c�ne et texte"));
        actions[QTab::StatusAndText]    = styleMenu->addAction(tr("St&atut et texte"));

        QActionGroup*   group   = new QActionGroup(styleMenu);
        for(int i = 0; i < QTab::StyleCount; ++i)
        {
            actions[i]->setData(i);
            actions[i]->setActionGroup(group);
            actions[i]->setCheckable(true);
            actions[i]->setChecked(false);
        }

        // Coche l'entr�e correspondant � l'�tat actuel de l'onglet.
        actions[currentTab()->style()]->setChecked(true);

        connect(styleMenu, SIGNAL(triggered(QAction*)), currentTab(), SLOT(setStyle(QAction*)));

        return styleMenu;
    }


    /*! @return Le menu de changement de style de la barre d'onglets
     */
    QMenu* QDock::barStyleMenu() const
    {
        QMenu*      styleMenu   = new QMenu(tr("Style de la &barre"));
        QAction*    actions[2]  = { nullptr };

        actions[0]  = styleMenu->addAction(tr("&Moderne"));
        actions[0]->setCheckable(true);
        actions[0]->setChecked(bar()->isModernStyleEnabled());
        connect(actions[0], SIGNAL(triggered(bool)), bar(), SLOT(setModernStyleEnabled(bool)));

        actions[1]  = styleMenu->addAction(tr("&Classique"));
        actions[1]->setCheckable(true);
        actions[1]->setChecked(!bar()->isModernStyleEnabled());
        connect(actions[1], SIGNAL(triggered(bool)), bar(), SLOT(setClassicStyleEnabled(bool)));

        return styleMenu;
    }


//------------------------------------------------------------------------------
//                      QDock : Slots Associ�s aux Boutons
//------------------------------------------------------------------------------

    /*! Affiche une page apr�s la s�lection de son onglet.
        @param tab Indice de l'onglet s�lectionn�
     */
    void QDock::tabSelected(int tab)
    {
        Q_D(QDock);

        if (tab != -1)
        {
            QPage*  p = page(tab);
            d->pages->setCurrentWidget(p);
            d->dockBar->setText(p->tab()->title());
        }
    }


    /*! Appelle le menu du dock.
     */
    void QDock::callMenu()
    {
        QPage*  page    = currentPage();

        QMenu   mainMenu;

        // Entr�es du menu (NB: les connections sont effectu�es dans les m�thodes de cr�ation des menus) :
        // 1 - Menu de la page actuelle (s'il existe).
        if (QMenu* pageMenu = page->menu())
        {
            mainMenu.addMenu(pageMenu);
        }
        // 2 - Ajouter page.
        mainMenu.addMenu(createPageMenu());
        // 3 - Fermer page.
        connect(mainMenu.addAction(tr("&Ferme l'onglet")), SIGNAL(triggered(bool)), SLOT(closePage()));
        // 4 - D�tacher la page.
        connect(mainMenu.addAction(tr("&D�tacher l'onglet")), SIGNAL(triggered(bool)), SLOT(deDockPage()));

        mainMenu.addSeparator();

        // 5 - Taille de l'aper�u (facultatif).
        if (page->isContentsResizable())
        {
            mainMenu.addMenu(pageContentsSizeMenu());
        }
        // 6 - Style d'onglet.
        mainMenu.addMenu(tabStyleMenu());
        // 7 - Afficher comme une liste (facultatif).
        if (page->mayDisplayAsList())
        {
            QAction*    act = mainMenu.addAction(tr("Voir comme une &liste"));
            connect(act, SIGNAL(triggered(bool)), page, SLOT(displayAsList(bool)));
            act->setCheckable(true);
            act->setChecked(currentPage()->isDisplayingList());
        }
        // 8 - Afficher comme une grille (facultatif).
        if (page->mayDisplayAsGrid())
        {
            QAction*    act = mainMenu.addAction(tr("Voir comme une &grille"));
            connect(act, SIGNAL(triggered(bool)), page, SLOT(displayAsGrid(bool)));
            act->setCheckable(true);
            act->setChecked(currentPage()->isDisplayingGrid());
        }

        mainMenu.addSeparator();

        // 9 - Style de la barre d'onglets.
        mainMenu.addMenu(barStyleMenu());

        mainMenu.exec(QCursor::pos());
    }


    /*! Ferme la page actuelle.
     */
    void QDock::closePage()
    {
        closePage(currentIndex());
    }


//------------------------------------------------------------------------------
//                      QDock : Slots Associ�s aux Menus
//------------------------------------------------------------------------------

    /*! Cr�e une page suite � la s�lection d'une entr�e dans le menu de cr�ation des pages.
        @param pageAction Entr�e de menu s�lectionn�
     */
    void QDock::createPage(QAction* pageAction)
    {
        Q_D(QDock);

        foreach(QTab* tab, d->tabData)
        {
            if (tab->title() == pageAction->text())
            {
                displayTab(tab->name());
                break;
            }
        }
    }


    /*! D�tache la page actuelle.
     */
    void QDock::deDockPage()
    {
        deDockPage(currentIndex());
    }
}
