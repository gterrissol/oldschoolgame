/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_DOCK_HH
#define QTAB_DOCK_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/Dock.hh
    @brief En-t�te des classes QTAB::QDockBar & QTAB::QDock.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QLabel>
#include <QWidget>

#include "QT/Private.hh"

class QMenu;

namespace QTAB
{
    /*! @brief "Barre de titre" d'un dock.
        @version 1.0

        Pour un dock, la barre de titre porte, en plus du titre de l'onglet actuel, 2 boutons :
        - appel du menu g�n�ral,
        - fermeture de l'onglet actuel.

        Cette classe � �t� introduite pour faciliter le drag'n'drop, et l'agencement des composants des
        docks.
     */
    class QDockBar : public QLabel
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    QDockBar(QDock* parent);                //!< Constructeur.
virtual             ~QDockBar();                            //!< Destructeur.
        //@}
        //! @name Ascendants
        //@{
        QDock*      dock() const;                           //!< Dock.
        QTabWidget* tabWidget() const;                      //!< Widget.
        //@}

protected:
        //! @name Drag'n'drop
        //@{
virtual void        mouseMoveEvent(QMouseEvent* e);         //!< Drag.
virtual void        mousePressEvent(QMouseEvent* e);        //!< D�but de drag.
virtual void        mouseReleaseEvent(QMouseEvent* e);      //!< Drop.
        //@}

    private:

        QDragTab*   drag;                                   //!< Instance de glisser/d�poser.
        QPoint      dragStartPosition;                      //!< Position initiale du drag.
    };


    /*! @brief Composant de tabwidget.
        @version 1.0

        Un dock est le widget �l�mentaire plac� dans un tabwidget. Il est compos� des �l�ments suivant :
        - une barre d'onglets (QBar),
        - une barre de titre (QDockBar),
        - une pile de pages (QPage).
     */
    class QDock : public QWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
                    QDock(QTabWidget* parent);              //!< Constructeur [par d�faut].
virtual             ~QDock();                               //!< Destructeur.
        //@}
        //! @name Famille
        //@{
        QBar*       bar() const;                            //!< Barre d'onglets.
        QTabWidget* tabWidget() const;                      //!< Widget.
        //@}
        //! @name Gestion des onglets
        //@{
        void        registerTab(QTab* tab);                 //!< Enregistrement d'un type d'onglet.
        void        displayTab(QString name);               //!< Ajout et affichage d'un onglet.
        void        activateTab(int index);                 //!< Activation d'un onglet (et de sa page).
        //@}
        //! @name Etats des pages et de leurs onglets
        //@{
        QPage*      currentPage() const;                    //!< Page actuelle.
        QTab*       currentTab() const;                     //!< Onglet actuel.
        int         currentIndex() const;                   //!< Indice de l'onglet (ou de la page) actuel.
        int         count() const;                          //!< Nombre d'onglets (ou de pages).
        //@}
        //! @name Dimensions
        //@{
virtual QSize       sizeHint() const;                       //!< Suggestion de taille pour le dock.
virtual QSize       minimumSizeHint() const;                //!< Suggestion pour la taille minimale du dock.
        //@}

    protected:

virtual void        onLanguageChange();                     //!< Mise � jour de l'interface suite � un changement de langue.
        //! @name Agencement des widgets
        //@{
virtual void        changeEvent(QEvent* e);                 //!< R�ponse � un changement de style.
virtual void        paintEvent(QPaintEvent* e);             //!< Dessin du dock.
virtual void        resizeEvent(QResizeEvent* e);           //!< Redimensionnement du dock.
virtual void        showEvent(QShowEvent* e);               //!< Affichage du dock.


    private slots:

        void        setUpLayout(bool onlyCheck = false);    //!< R�agencement des widgets.
        //@}

    private:
        //! @name Gestion des pages
        //@{
        int         addPage(QPage* p);                      //!< Ajout d'une page.
        int         insertPage(QPage* p, int index);        //!< Insertion d'une page.
        int         indexOf(QPage* p) const;                //!< Index d'une page.
        QPage*      page(int index) const;                  //!< Acc�s � une page.
        QPage*      removePage(int index);                  //!< Retrait d'une page.
        void        closePage(int index);                   //!< Fermeture d'une page.
        void        deDockPage(int index);                  //!< D�tachement d'une page.
        //@}
        //! @name Cr�ation des menus
        //@{
        QMenu*      createPageMenu() const;                 //!< ... de cr�ation des onglets.
        QMenu*      pageContentsSizeMenu() const;           //!< ... de changement d'"�chelle" du contenu d'un onglet.
        QMenu*      tabStyleMenu() const;                   //!< ... de choix du style d'un onglet.
        QMenu*      barStyleMenu() const;                   //!< ... de choix du style de la barrde des onglets.
        //@}

    private slots:
        //! @name Slots associ�s aux boutons
        //@{
        void        tabSelected(int tab);                   //!< S�lection d'un onglet.
        void        callMenu();                             //!< Appel du menu g�n�ral.
        void        closePage();                            //!< Fermeture de la page actuelle.
        //@}
        //! @name Slots associ�es aux menus
        //@{
        void        createPage(QAction* pageAction);        //!< Cr�ation d'une page.
        void        deDockPage();                           //!< D�tachement de la page actuelle.
        //@}

    private:

        Q_DISABLE_COPY(QDock)
        Q_CUSTOM_DECLARE_PRIVATE(QDock)

        friend class QDockBar;
        friend class QProxy;
    };
}

#endif  // De QTAB_DOCK_HH
