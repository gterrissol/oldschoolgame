/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_TAB_HH
#define QTAB_TAB_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/Tab.hh
    @brief En-t�te de la classe QTAB::Tab.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QObject>

#include "QT/Private.hh"

class QAction;
class QIcon;
class QRect;

namespace QTAB
{
    /*! @brief Onglet (pour un widget).
        @version 1.1

        Les onglets b�n�ficient de leur propre classe (contrairement � ceux de Qt 4).<br>
        Ils ont, en fait, deux emplois bien distincts :
        - faciliter l'affichage et la manipulation d'onglets dans un <i>tab widget</i> (onglets
        dynamiques)
        - regrouper les informations requises pour la cr�ation de nouveaux onglets de m�me type (onglets
        "locaux").

        @sa QTAB
     */
    class QTab : public QObject
    {
        Q_OBJECT
    public:
        //! @name Constructeurs & destructeur
        //@{
                QTab(QString (*name)(), QString (*title)(),
                     QIcon icon, QPage* (*function)()); //!< Constructeur.
        QTab*   clone() const;                          //!< Duplication (logique).
        QPage*  createPage() const;                     //!< Cr�ation d'une page.
virtual         ~QTab();                                //!< Destructeur.
        //@}

        //! Style d'onglet.
        enum Style
        {
            Icon,                                       //!< Ic�ne seulement.
            Status,                                     //!< Etat courant seulement
            Text,                                       //!< Texte seulement.
            IconAndText,                                //!< Ic�ne et texte.
            StatusAndText,                              //!< Etat courant et texte.
            StyleCount                                  //!< Nombre de styles disponibles.
        };
        //! @name Propri�t�s
        //@{
        QRect   rect() const;                           //!< G�om�trie.
        void    setRect(QRect r);                       //!< D�finition de la g�ometrie.
        Style   style() const;                          //!< Style.
        QIcon   icon() const;                           //!< Icone.
        QString name() const;                           //!< Nom.
        QString text() const;                           //!< Texte affich�.
        QString title() const;                          //!< Titre.
        QString toolTip() const;                        //!< Tooltip.
        bool    isEnabled() const;                      //!< Activ� ?
        int     shortcutId() const;                     //!< ID du raccourci-clavier associ�.
        void    setShortcutId(int id);                  //!< Association avec un raccourci-clavier.
        //@}

    public slots:
        //! @name Slot
        //@{
        void    setStyle(QAction* styleAction);         //!< Changement du "style" de l'onglet.
        //@}

    private:

                QTab(QPage* parent);                    //!< Constructeur.
        //! @name Famille
        //@{
        QPage*  page() const;                           //!< Page associ�e.
        bool    isCompatible(QTab* t) const;            //!< Test de "compatibilit�".
        //@}
        //! @name Gestion de la barre d'onglets
        //@{
        void    setBar(QBar* b);                        //!< Liaison avec une barre.
        QBar*   bar() const;                            //!< Acc�s � la barre porteuse.
        //@}
        Q_DISABLE_COPY(QTab)
        Q_CUSTOM_DECLARE_PRIVATE(QTab)
 
        friend class QProxy;
    };
}

#endif  // QTAB_TAB_HH
