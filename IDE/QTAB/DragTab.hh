/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_DRAGTAB_HH
#define QTAB_DRAGTAB_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/DragTab.hh
    @brief En-t�te de la classe QTAB::QDragTab.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "QT/DragObject.hh"

class QWidget;

namespace QTAB
{
    /*! @brief Syst�me de glisser/d�poser pour les onglets.
        @version 1.0

        Impl�m�nte le drag'n'drop pour les onglets (qui sont des QObject, pas des QWidget; c'est la raison pour laquelle le type d'objet
        dragg� est QObject et pas QWidget, le syst�me de drag ayant �t� initialement cr�� pour les onglets).
     */
    class QDragTab : public QT::QDragObject
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                QDragTab(QTab* t);                          //!< Constructeur.
virtual         ~QDragTab();                                //!< Destructeur.
        //@}

    private:
        //! @name Comportement du glisser/d�poser
        //@{
virtual QIcon   icon() const;                               //!< Ic�ne.
virtual QString title() const;                              //!< Titre.
virtual bool    mayDropOn(QWidget* w, QPoint p) const;      //!< Possibilit� de d�p�t.
virtual void    dropOn(QWidget* w, QPoint p);               //!< D�p�t sur un widget.
        //@}
        //!@name Surbrillance de la cible
        //@{
virtual QWidget*    buddy(QWidget* w) const;                //!< R�cup�ration de la cible � d�corer.
        //@}
    };
}

#endif  // QTAB_DRAGTAB_HH
