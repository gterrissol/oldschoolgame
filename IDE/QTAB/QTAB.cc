/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "QTAB.hh"

/*! @file IDE/QTAB/QTAB.cc
    @brief Documentation du module QTAB.
    @author @ref Guillaume_Terrissol
    @date 5 Septembre 2005 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */


    /*! @page QTAB_Page Utilisation de QTAB::QPage
        Un widget, pour �tre affich� dans un tabwidget, doit respecter quelques contraintes :
        - d�river de QTAB::QPage
        - d�finir une m�thode statique builder(),
        - au besoin, r�impl�menter quelques m�thodes QTAB::QPage.

        @section QTAB_QPage_inheritance H�ritage de QTAB::QPage
        Contrairement au QTabWidget de Qt, QTAB::QTabWidget ne peut contenir que des instances de QPage,
        ceci afin de proposer un comportement plus riche. Avec le nouveau style de g�n�ration du code
        d'interfaces graphiques cr��s par designer (je pense � l'approche directe :
        ui.setupUi(<i>widget</i>)), cette contrainte ne devrait pas �tre trop forte.

        @section QTAB_Builder Construction d'un onglet
        Une m�thode [statique] est � d�finir absolument dans toute classe d�riv�e de QTAB::QPage : elle
        permet de cr�er un onglet qui sera enregistr� aupr�s de tous les tabwidgets qui pourront ainsi
        afficher cette page (voir l'@ref QTAB_Detailled_Example). 

        @section QTAB_Customized_behaviour Comportement �volu�
        QTAB::QPage d�finit une version par d�faut pour quelques m�thodes virtuelles qui peuvent �tre
        r�impl�ment�es pour offrir les fonctionnalit�s suivantes :
        - affichage d'un sous-menu dans le menu principal du tabwidget
        (void QTAB::QPage::fillPageMenu(QMenu*&) const) et traitement des entr�es s�lectionn�es
        (void QTAB::QPage::manageMenuEntry(QAction*))
        - �laboration d'une ic�ne repr�sentant l'�tat actuel de la page (e.g. une page de choix de
        couleur retournera une ic�ne toute enti�re de la couleur actuellement choisie)
        (QIcon QTAB::QPage::statusIcon() const),
        - changement d'�chelle du contenu de la page (void QTAB::QPage::changeContentsSize(ContentsSize)),
        - affichage du contenu sous forme d'une liste, ou d'une grille (void QTAB::QPage::updateLayout()).

        Ces m�thodes sont �galement d�crites dans l'@ref QTAB_Detailled_Example

        @section QTAB_Detailled_Example Exemple complet
        L'exemple utilise les conventions de nommage Qt.
        @code
    class MyPage : public QTAB::QPage
    {
    public:

                myPage();
virtual         ~myPage();

static  QTab*   builder();

    private:

static  QPage*  create();

virtual void    fillPageMenu(QMenu*& menu) const;
virtual void    manageMenuEntry(QAction* menuEntry);
virtual QIcon   statusIcon() const;
virtual void    changeContentsSize(ContentsSize sz);
virtual void    updateLayout();

        // Attributs.
        ...
    };
        @endcode
        Le point important � remarquer ici, est la d�claration des m�thodes MyPage::builder() et
        MyPage::create().<br>
        Leur r�le est d�crit un peu plus loin.
        @code
        MyPage::myPage()
            : QPage(true, true, true)
            //, + Initialisation des attributs...
        {
            // Initialisations quelconques...
        }
        @endcode
        Les 3 bool�ens pass�s en param�tre au constructeur de QTAB::QPage indique, respectivement, que le
        contenu de la page est redimensionnable, affichable sous forme d'une liste, affichable sous forme
        d'une grille.
        @code
virtual MyPage::~MyPage::()
        {
            // tout travail utile.
        }
        @endcode
        Pas de remarque particuli�re sur le destucteur.
        @code
static  QTab* MyPage::builder()
        {
            return new QTab("My Tab", "This is my tab", QIcon("MyTabIcon.xpm"), &MyPage::create);
        }
        @endcode
        La m�thode builder() sera appel�e lors de l'enregistrement de cette classe aupr�s du tabwidget.
        L'onglet ainsi cr�� contiendra toutes les informations n�cessaire pour ajouter une instance de
        MyPage (et son onglet) au tabwidget.<br>
        Ici, le dernier argument est un pointeur sur une m�thode statique de la classe, mais ce pourrait
        �tre un pointeur de fonction quelconque, pourvu que la fonction en question alloue une nouvelle
        instance de MyPage (et ne prenne pas d'argument).
        @code
static  QPage* MyPage::create()
        {
            return new myPage();
        }
        @endcode
        Une nouvelle instance de MyPage est cr��e via le contructeur par d�faut. Rien n'emp�che d'appeler
        un autre constructeur, mais la "fonction" de cr�ation (ici MyPage::create()) ne peut pas avoir
        d'argument.
        @code
        void MyPage::fillPageMenu(QMenu*& menu) const
        {
            menu->addAction("&First menu entry");
            menu->addAction("&Second menu entry");
        }
        @endcode
        Exemple purement acad�mique : des entr�es sont simplement ajout�es au menu.<br>
        Rappel: par d�faut, le menu ne poss�de aucune entr�e. Si cette m�thode n'est pas red�finie, aucun
        menu ne sera disponible pour la page (la premi�re entr�e du menu g�n�ral sera la cr�ation d'une
        nouvelle page).
        @code
        void MyPage::manageMenuEntry(QAction* menuEntry)
        {
            // Traite menuEntry.
        }
        @endcode
        Action � traiter...
        @code
        QIcon MyPage::statusIcon() const
        {
            QIcon   icon;
            // ...
            return icon;
        }
        @endcode
        L'ic�ne doit �tre construite d'apr�s des informations pertinentes de la page.<br>
        Si cette m�thode n'est pas r�impl�ment�e, l'ic�ne de l'onglet sera utilis�e.
        @code
        void MyPage::changeContentsSize(ContentsSize sz)
        {
            switch(sz)
            {
                case Tiny:
                    // ...
                    break;
                case VerySmall:
                    // ...
                    break;
                case ...
                default:
                    // ...
                    break;
            }

            // ...
        }
        @endcode
        adjustSize() sera appel�e apr�s cette m�thode pour redimensionner la page (et le tabwidget) si
        besoin est.
        @code
        void MyPage::updateLayout()
        {
            if      (isDisplayingList())
            {
                // ...
            }
            else if (isDisplayingGrid)
            {
                // ...
            }
            else
            {
            }
        }
        @endcode
        Encore une fois, je ne fais que donner le squelette des r�impl�mentations des m�thodes. Le reste
        est � compl�ter en fonction des besoins.

        Une fois MyPage compl�tement d�finie, on peut l'utiliser.
        @code
        QTAB::QTabWidget*   tabWidget   = new QTAB::QTabWidget();

        // ...

        tabWidget->registerPage<MyPage>();

        // ...

        tabWidget->dock(0)->displayTab("My Tab");
        
        // ...
        @endcode
     */

