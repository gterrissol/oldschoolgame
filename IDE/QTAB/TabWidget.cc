/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "TabWidget.hh"

/*! @file IDE/QTAB/TabWidget.cc
    @brief M�thodes (non-inline) des classes QTAB::QTabWidget::QTabWidgetPrivate & QTAB::QTabWidget.
    @author @ref Guillaume_Terrissol
    @date 16 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QEvent>
#include <QMap>
#include <QVBoxLayout>

#include "Bar.hh"
#include "Dock.hh"
#include "Tab.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                   QTabWidgetPrivate : P-Impl de QTabWidget
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QTabWidget.
        @version 1.0

        A l'image de ce qui a �t� fait pour QBar, cette classe fournit une grande partie de
        l'imp�mentation de QTabWidget.
     */
    class QTabWidget::QTabWidgetPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QTabWidget)
    public:
        //! @name Constructeurs
        //@{
        QTabWidgetPrivate(QTabWidget* parent, QTab::Style ts, QBar::Style bs);  //!< Constructeur.
        //@}
        //! @name Attributs
        //@{
        typedef QMap<QString, QTab*>    TabContainer;                           //!< Type du conteneur des onglets "locaux".
        TabContainer    tabData;                                                //!< Onglets "locaux".
        QList<QDock*>   docks;                                                  //!< Liste des docks.
        QTab::Style     tabStyle;                                               //!< Style d'onglet par d�faut.
        QBar::Style     barStyle;                                               //!< Style de barre par d�faut.
        //@}
 static const QTab::Style   defaultTabStyle = QTab::Icon;                       //!< Style par d�faut pour les onglets.
 static const QBar::Style   defaultBarStyle = QBar::Modern;                     //!< Style par d�faut pour les barres.
    };


//------------------------------------------------------------------------------
//                       QTabWidgetPrivate : Constructeur
//------------------------------------------------------------------------------

    /*! @param parent TabWidget pour laquel porter des informations
        @param ts     Style pour les onglets
        @param bs     Style pour les barres
        @sa QBarPrivate::QBarPrivate(QBar* parent)
     */
    QTabWidget::QTabWidgetPrivate::QTabWidgetPrivate(QTabWidget* parent, QTab::Style ts, QBar::Style bs)
        : q_custom_ptr(parent)
        , tabStyle(ts)
        , barStyle(bs)
    { }


//------------------------------------------------------------------------------
//                   QTabwidget : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Construit un tabwidget avec les tyles d'onglet et de barre par d�faut (respectivement QTab::Icon
        et QBar::Modern).
        @param parent Widget parent
     */
    QTabWidget::QTabWidget(QWidget* parent)
        : QWidget(parent)
        , d_custom_ptr(new QTabWidgetPrivate(this, QTabWidgetPrivate::defaultTabStyle, QTabWidgetPrivate::defaultBarStyle))
    {
        init();
    }


    /*! Construit un tabwidget avec un style d'onglet particulier et le style de barre par d�faut
        (QBar::Modern).
        @param parent Widget parent
        @param ts     Style d'onglet � employer par d�faut pour tous les nouveaux onglets
     */
    QTabWidget::QTabWidget(QTab::Style ts, QWidget* parent)
        : QWidget(parent)
        , d_custom_ptr(new QTabWidgetPrivate(this, ts, QTabWidgetPrivate::defaultBarStyle))
    {
        init();
    }


    /*! Construit un tabwidget avec le style d'onglet par d�faut (QTab::Icon) et un style de barre
        particulier
        @param parent Widget parent
        @param bs     Style de barre � employer par d�faut pour toutes les nouvelles barres
     */
    QTabWidget::QTabWidget(QBar::Style bs, QWidget* parent)
        : QWidget(parent)
        , d_custom_ptr(new QTabWidgetPrivate(this, QTabWidgetPrivate::defaultTabStyle, bs))
    {
        init();
    }


    /*! Construit un tabwidget avec des styles d'onglet et de barre particuliers.
        @param parent Widget parent
        @param ts     Style d'onglet � employer par d�faut pour tous les nouveaux onglets
        @param bs     Style de barre � employer par d�faut pour toutes les nouvelles barres
     */
    QTabWidget::QTabWidget(QTab::Style ts, QBar::Style bs, QWidget* parent)
        : QWidget(parent)
        , d_custom_ptr(new QTabWidgetPrivate(this, ts, bs))
    {
        init();
    }


    /*! Destructeur.
     */
    QTabWidget::~QTabWidget()
    {
        Q_D(QTabWidget);

        foreach(QTab* ptr, d->tabData)
        {
            delete ptr;
        }
    }


//------------------------------------------------------------------------------
//                        QTabwidget : Styles par D�faut
//------------------------------------------------------------------------------

    /*! @return Le style d'onglet utilis� par d�faut par tout nouvel onglet dans ce tabwidget
     */
    QTab::Style QTabWidget::tabDefaultStyle() const
    {
        Q_D(const QTabWidget);

        return d->tabStyle;
    }


    /*! @return Le style de barre utilis� par d�faut par toute nouvelle barre dans ce tabwidget
     */
    QBar::Style QTabWidget::barDefaultStyle() const
    {
        Q_D(const QTabWidget);

        return d->barStyle;
    }


//------------------------------------------------------------------------------
//                        QTabwidget : Gestion des Docks
//------------------------------------------------------------------------------

    /*! Cr�e et positionne un dock dans le tabwidget.
        @param p Position [locale] souhait�e pour cr�er le nouveau dock, qui pourra s'ins�rer entre deux
        docks d�j� existants
        @return Un nouveau dock
        @note Les onglets "locaux" devront �tre copi�s dans le nouveau dock (ce n'est pas fait dans cette
        m�thode)
     */
    QDock* QTabWidget::createDock(QPoint p)
    {
        int index = -1;
        if (0 < count())
        {
            for(index = 0; index < count() && dock(index)->pos().y() < p.y(); ++index) { }

            if (index == count())
            {
                index = -1;
            }
        }

        QDock*  d   = new QDock(this);

        QVBoxLayout*    mainLayout = qobject_cast<QVBoxLayout*>(layout());
        mainLayout->insertWidget(index, d);
        // Dans un souci de coh�rence avec les r�gles de nommage de ce module (un indice d'onglet, de
        // page, ou de dock est en principe et respectivement t, p, d), j'ai pr�f�r� ici acc�der (une
        // seule fois, ce n'est donc pas tr�s p�nalisant) au p-impl directement via la m�thode d_func.
        d_func()->docks.insert(index, d);

        //connect(d, SIGNAL(destroyed(QObject*)), SLOT(dockClosed(QObject*)));

        return d;
    }


    /*! @param d Indice du dock � r�cup�rer
        @return Le <i>d</i>i�me dock
     */
    QDock* QTabWidget::dock(int d) const
    {
        if (0 <= d && d < count())
        {
            // Dans un souci de coh�rence avec les r�gles de nommage de ce module (un indice d'onglet, de
            // page, ou de dock est en principe et respectivement t, p, d), j'ai pr�f�r� ici acc�der (une
            // seule fois, ce n'est donc pas tr�s p�nalisant) au p-impl directement via la m�thode d_func.
            return d_func()->docks[d];
        }
        else
        {
            return nullptr;
        }
    }


    /*! @return Le nombre de docks
     */
    int QTabWidget::count() const
    {
        Q_D(const QTabWidget);

        return d->docks.count();
    }


    /*!
     */
    void QTabWidget::childEvent(QChildEvent* e)
    {
        Q_D(QTabWidget);

        if (e->removed())
        {
            QObject*    o = e->child();
            if (o != nullptr)
            {
                // Le signal destoyed() est envoy� alors que l'objet est en train d'�tre d�truit, ni
                // dynamic_cast, ni qobject_cast ne sont exploitables, d'o� le reinterpret_cast.
                int i   = d->docks.indexOf(reinterpret_cast<QDock*>(o));

                if (i != -1)
                {
                    d->docks.removeAt(i);

                    if (count() == 0)
                    {
                        close();
                    }
                }
            }
        }
    }

    /*! M�thode appel�e quand un enfant du tabwidget est sur le point d'�tre d�truit.<br>
        Si cet enfant est le dernier dock, le tabwidget se fermera.
        @param o Fils en cours de destruction
     */
    void QTabWidget::dockClosed(QObject* o)
    {
        Q_D(QTabWidget);

        if (o != nullptr)
        {
            // Le signal destoyed() est envoy� alors que l'objet est en train d'�tre d�truit, ni
            // dynamic_cast, ni qobject_cast ne sont exploitables, d'o� le reinterpret_cast.
            int i   = d->docks.indexOf(reinterpret_cast<QDock*>(o));

            if (i != -1)
            {
                /*d->docks.removeAt(i);

                if (count() == 0)
                {
                    close();
                }*/
            }
        }
    }


//------------------------------------------------------------------------------
//                          QTabwidget : Autre M�thode
//------------------------------------------------------------------------------

    /*! Il y a un bon nombre de constructeurs, dont le code �tait identique : il a �t� factoris� par
        cette m�thode.
     */
    void QTabWidget::init()
    {
        QVBoxLayout*    mainLayout  = new QVBoxLayout(this);
        mainLayout->setMargin(4);
        mainLayout->setSpacing(5);
        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        createDock(QPoint(0, 0));

        setAttribute(Qt::WA_DeleteOnClose);
    }


//------------------------------------------------------------------------------
//                         QTabwidget : Onglets "Locaux"
//------------------------------------------------------------------------------

    /*! Les onglets "locaux" sont stock�s dans chaque dock, mais aussi dans les tabwidgets. ce sont ces
        onglets qui serviront � initialiser les docks.
        @sa QDock::registerTab(QTab*)
     */
    void QTabWidget::registerTab(QTab* tab)
    {
        Q_D(QTabWidget);

        QString tabText = tab->name();

        if (d->tabData.find(tabText) == d->tabData.end())
        {
            d->tabData[tabText] = tab;
        }

        for(int i = 0; i < count(); ++i)
        {
            dock(i)->registerTab(tab->clone());
        }
    }


    /*! @return Le nombre d'onglets "locaux" enregistr�s aupr�s de ce tabwidget
     */
    int QTabWidget::tabCount() const
    {
        Q_D(const QTabWidget);

        return d->tabData.count();
    }


    /*! @param t Indice de l'onglet "local" recherch�
        @return Le <i>t</i>i�me onglet "local"
     */
    const QTab* QTabWidget::tab(int t) const
    {
        Q_D(const QTabWidget);

        Q_ASSERT_X(t < tabCount(), "const QTab* QTabWidget::tab(int) const", "Invalid tab index");

        return d->tabData.values()[t];
    }
}
