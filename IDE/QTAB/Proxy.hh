/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_PROXY_HH
#define QTAB_PROXY_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/Proxy.hh
    @brief En-t�te de la classe QTAB::QProxy.
    @author @ref Guillaume_Terrissol
    @date 19 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

class QPoint;

namespace QTAB
{
    /*! @brief M�diateur entre composants de QTAB.
        @version 1.0

        Cette classe a pour but d'effectuer les liaisons entre les diff�rents composants de ce module (QTab, QBar, QPage, QDock, QTabWidget).
        A un moment donn�, la gestion des "mouvements" de ces composants �tait devenue assez complexe, et je ne voulais pas que des accesseurs
        "sensibles" soient publics. J'ai donc introduit cette classe, et l'ai d�clar� amie de QTab, QDock et QTabWidget (comme toutes ces
        classes utilisent des p-impl, l'amiti� ne m'a pas trop g�n�).
     */
    class QProxy
    {
    public:
        //! @name Constructeurs
        //@{
                    QProxy(QPage* p);                   //!< ... pour "assister" une page.
                    QProxy(QTab* t);                    //!< ... pour "assister" un onglet.
        //@}
        bool        mayMoveTo(QTabWidget* w) const;     //!< Test de d�placement vers un tabwidget.
        //! @name D�placement des pages et de leurs onglets
        //@{
        void        moveTo(QBar* b, int index = -1);    //!< ... vers une barre d'onglet.
        void        moveTo(QDockBar* b);                //!< ... vers une barre de dock.
        void        moveTo(QTabWidget* w, QPoint p);    //!< ... vers un tabwidget.
        void        moveToOutside();                    //!< ... vers l'"ext�rieur".
        void        moveToOutside(QPoint p);            //!< ... vers une "position ext�rieure".

    private:

        QTabWidget* moveOut();                          //!< ... vers un nouveau tabwidget.
        //@}
        //! @name Composants manipul�s
        //@{
        QPage*      page;                               //!< Page.
        QTab*       tab;                                //!< Onglet.
        //@}
    };
}

#endif  // QTAB_PROXY_HH
