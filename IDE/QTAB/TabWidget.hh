/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef QTAB_TABWIDGET_HH
#define QTAB_TABWIDGET_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/TabWidget.hh
    @brief En-t�te de la classe QTAB::QTabWidget.
    @author @ref Guillaume_Terrissol
    @date 16 Ao�t 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

#include "Bar.hh"
#include "Tab.hh"

namespace QTAB
{
    /*! @brief TabWidget am�lior�.
        @version 1.0
     */
    class QTabWidget : public QWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeurs & destructeur
        //@{
                        QTabWidget(QWidget* parent = 0);    //!< Constructeur [par d�faut].
                        QTabWidget(QTab::Style tabStyle,
                                   QWidget* parent = 0);    //!< Constructeur (changement du style d'onglet par d�faut).
                        QTabWidget(QBar::Style barStyle,
                                   QWidget* parent = 0);    //!< Constructeur (changement du style de barre par d�faut).
                        QTabWidget(QTab::Style tabStyle,
                                   QBar::Style barStyle,
                                   QWidget* parent = 0);    //!< Constructeur (changement des styles par d�faut).
virtual                 ~QTabWidget();                      //!< Destructeur.
        //@}
        //! @name Gestion des onglets
        //@{
        template<class P>
        void        registerPage();                         //!< Enregistrement d'un "type de page".
        //@}
        //! @name Styles par d�faut
        //@{
        QTab::Style tabDefaultStyle() const;                //!< Style d'onglet par d�faut.
        QBar::Style barDefaultStyle() const;                //!< Style de barre par d�faut.
        //@}
        //! @name Gestion des docks
        //@{
        QDock*      dock(int d) const;                      //!< Acc�s au docks.
        int         count() const;                          //!< Nombre de docks.

    protected:

virtual void        childEvent(QChildEvent* e);             //!< Pour la fermeture d'un dock.


    private slots:
    
        void        dockClosed(QObject* o);                 //!< Fermeture d'un dock.

    private:

        QDock*      createDock(QPoint p);                   //!< Cr�ation d'un dock.
        //@}
        void        init();                                 //!< Initialisation du widget.
        //! @name Onglets "locaux"
        //@{
        void        registerTab(QTab* tab);                 //!< Enregistrement d'un "type d'onglet".
        int         tabCount() const;                       //!< Nombre d'onglets "locaux".
        const QTab* tab(int t) const;                       //!< Acc�s � un onglet "local".
        //@}
        Q_DISABLE_COPY(QTabWidget)
        Q_CUSTOM_DECLARE_PRIVATE(QTabWidget)

        friend class QProxy;
    };
}

//------------------------------------------------------------------------------
//                                M�thode inline
//------------------------------------------------------------------------------

#include "TabWidget.inl"

#endif  // De QTAB_TABWIDGET_HH
