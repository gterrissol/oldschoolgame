/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/QTAB/TabWidget.inl
    @brief M�thode inline de la classe TabWidget.
    @author @ref Guillaume_Terrissol
    @date 31 Septembre 2005 - 3 Mai 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace QTAB
{
//------------------------------------------------------------------------------
//                              Gestion des Onglets
//------------------------------------------------------------------------------

    /*! Enregistre un type de page pour l'utiliser dans le tabwidget.<br>
        Le param�tre template <i>P</i> est le type de la page � enregistrer.
     */
    template<class P>
    void QTabWidget::registerPage()
    {
        registerTab(P::builder());
    }
}
