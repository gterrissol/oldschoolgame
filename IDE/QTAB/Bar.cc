/****************************************************************************
 **
 ** Implementation of QTabBar class.
 **
 ** This file is part of the widgets module of the Qt Toolkit.
 **
 ** This file is provided AS IS with NO WARRANTY F ANY KIND, INCLUDING THE
 ** WARRANTY F DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#include "Bar.hh"

/*! @file IDE/QTAB/Bar.cc
    @brief M�thodes des classes QTAB::QBar::QBarPrivate & QTAB::QBar.
    @author Code original : Trolltech
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <memory>

#include <QApplication>
#include <QEvent>
#include <QHelpEvent>
#include <QList>
#include <QMenu>
#include <QPainter>
#include <QShortcutEvent>
#include <QStyleOptionTab>
#include <QStylePainter>
#include <QToolButton>
#include <QToolTip>

#include "Dock.hh"
#include "DragTab.hh"
#include "Tab.hh"
#include "TabWidget.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                         QBarPrivate : P-Impl de QBar
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QBar.
        @version 1.0

        A l'image de ce qui se fait pour QTabBar, cette classe fournit une grande partie de
        l'impl�mentation de QBar. La majeure partie du code de cette classe provient d'ailleurs de
        Qt-4.0.0, d'o� le copyright.
     */
    class QBar::QBarPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QBar)
    public:
        //! @name "Constructeurs"
        //@{
                                QBarPrivate(QBar* parent);          //!< Constructeur.
                void            init();                             //!< Initialisation.
        //@}
        //! @name Informations
                int             extraWidth() const;                 //!< ... g�n�rale.
                int             arrowHeight() const;                //!< ... sur les fl�ches de d�filement.
                QStyleOptionTab getStyleOption(int tab) const;      //!< ... par onglet.
        //@}
        //! @name Dimension et position
        //@{
                int             width() const;                      //!< Largeur de la barre.
                int             left() const;                       //!< Position gauche de la barre.
        //@}
        //! @name Acc�s aux onglets
        //@{
                QTab*           at(int index);                      //!< N-i�me onglet.
                const QTab*     at(int index) const;                //!< N-i�me onglet constant.
                int             indexAtPos(const QPoint &p) const;  //!< Onglet sous une position donn�e.
        inline bool             validIndex(int index) const;        //!< Index valide ?
        //@}
        //! @name Gestion de l'affichage
        //@{
        void                    scrollTabs();                       //!< D�filement des onglets.
        void                    refresh();                          //!< Rafra�chissement de la barre.
        void                    layoutTabs();                       //!< R�organisation des onglets.
        void                    makeVisible(int index);             //!< Affichage "forc�" d'un onglet.
        //@}
        //! @name "Structure" de l'interface graphique
        //@{
        QList<QTab*>                tabList;                        //!< Liste des onglets.
        QRect                       hoverRect;                      //!< Zone (un onglet) sous la souris.
        QToolButton*                modernLeftB;                    //!< Bouton gauche de d�filement des onglets (style moderne).
        QToolButton*                modernRightB;                   //!< Bouton droit de d�filement des onglets (style moderne).
        QToolButton*                classicLeftB;                   //!< Bouton gauche de d�filement des onglets (style classique).
        QToolButton*                classicRightB;                  //!< Bouton droit de d�filement des onglets (style classique).
        //@}
        //! @name Drag'n'drop
        //@{
        QPoint                      dragStartPosition;              //!< Position initiale d'une phase de glisser/d�poser.
        std::unique_ptr<QDragTab>   drag;                           //!< Instance de glisser/d�poser.
        //@}
        //! @name Etat actuel
        //@{
        int                         currentIndex;                   //!< Index de l'onglet actuel.
        int                         pressedIndex;                   //!< Index de l'onglet sous un clic souris.
        int                         scrollOffset;                   //!< Amplitude du d�filement en style classique.
        QBar::Style                 barStyle;                       //!< Style de la barre.
        bool                        layoutDirty;                    //!< Validit� de l'organisation actuelle des onglets.
        //@}
static const QTabBar::Shape         shape  = QTabBar::RoundedNorth; //!< Forme des onglet.
    };


//------------------------------------------------------------------------------
//                         QBarPrivate : "Constructeurs"
//------------------------------------------------------------------------------

    /*! QTabBarPrivate d�rivant de QWidgetPrivate, QBarPrivate aurait d� avoir la m�me classe de base.
        H�las, QWidgetPrivate est compl�tement cach�e dans Qt : pas moyen d'y acc�der sauf, peut-�tre, en
        bidouillant comme c'est pas permis. Du coup, QBarPrivate est une classe ind�pendante, et j'ai
        d�fini les macros Q_CUSTOM_DECLARE_PRIVATE() et Q_CUSTOM_DECLARE_PUBLIC() en remplacement des
        macros Qt permettant de lier une classe Widget � son P-Impl.
        @param parent Barre d'onglet pour laquelle porter des informations et fournir du comportement
     */
    QBar::QBarPrivate::QBarPrivate(QBar* parent)
        : q_custom_ptr(parent)
        , modernLeftB(nullptr)
        , modernRightB(nullptr)
        , classicLeftB(nullptr)
        , classicRightB(nullptr)
        , drag(nullptr)
        , currentIndex(-1)
        , pressedIndex(-1)
        , scrollOffset(0)
        , barStyle(QBar::Modern)
        , layoutDirty(false)
    { }


    /*! Cette m�thode est appel�e une fois que la barre et "son impl�mentation" ont �t� li�es.
     */
    void QBar::QBarPrivate::init()
    {
        Q_Q(QBar);

        // Fl�ches de d�filement (modernes).
        modernLeftB     = new QToolButton(q);
        modernLeftB->setArrowType(Qt::LeftArrow);
        modernLeftB->resize(extraWidth() / 2, 0);
        QObject::connect(modernLeftB, SIGNAL(clicked()), q, SLOT(scrollTabs()));

        modernRightB    = new QToolButton(q);
        modernRightB->setArrowType(Qt::RightArrow);
        modernRightB->resize(extraWidth() / 2, 0);
        QObject::connect(modernRightB, SIGNAL(clicked()), q, SLOT(scrollTabs()));

        // Fl�ches de d�filement (classiques).
        classicLeftB    = new QToolButton(q);
        QObject::connect(classicLeftB, SIGNAL(clicked()), q, SLOT(scrollTabs()));
        classicLeftB->hide();

        classicRightB   = new QToolButton(q);
        QObject::connect(classicRightB, SIGNAL(clicked()), q, SLOT(scrollTabs()));
        classicRightB->hide();

        // Param�tres g�n�raux.
        q->setFocusPolicy(Qt::TabFocus);
        q->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        q->setAcceptDrops(true);
        q->setModernStyleEnabled(q->tabWidget()->barDefaultStyle() == QBar::Modern);
    }


//------------------------------------------------------------------------------
//                          QBarPrivate : Informations
//------------------------------------------------------------------------------

    /*! Lorsque le style est classique, les fl�ches de d�filement peuvent �tre affich�es au besoin, dans
        le coin droit de la barre. L'espace occup� par ces fl�ches de d�filement ne pourra pas �tre
        utilis� pour l'affichage des onglets, il faut donc en tenir compte dans les calculs effectu�s
        pour estim� l'roganisation des onglets.
        @return L'espace (en pixels) utilis� par les fl�ches de d�filement du style classique (et m�me
        moderne), ou 0 si ces derni�res sont cach�es
     */
    int QBar::QBarPrivate::extraWidth() const
    {
        Q_Q(const QBar);

        return 2 * qMax(q->style()->pixelMetric(QStyle::PM_TabBarScrollButtonWidth, 0, q),
                        QApplication::globalStrut().width());
    }


    /*! Cette m�thode a �t� introduite afin de faciliter le calcul de la hauteur des fl�ches de
        d�filement et surtout, d'assurer leur coh�rence (selon le style, la g�om�trie des diff�rentes
        fl�ches s'�tablit � diff�rents endroits du code).
        @return La hauteur des fl�ches de d�filement
     */
    int QBar::QBarPrivate::arrowHeight() const
    {
        Q_Q(const QBar);

        QStyleOptionTab tabOverlap;
        tabOverlap.shape = shape;
        int             overlap = q->style()->pixelMetric(QStyle::PM_TabBarBaseOverlap, &tabOverlap, q);

        return q->sizeHint().height() - overlap;
    }


    /*! Les onglets sont des QObject, pas des QWidget : il ne g�rent pas leur affichage eux-m�mes, c'est
        leur barre porteuse qui s'en charge.<br>
        Cette m�thode permet de r�cup�rer les options de rendu pour chaque onglet, en fonction de leurs
        propri�t�s (ic�ne, texte, �tat d'activation, s�lection,...)
        @param tab Indice de l'onglet pour lequel r�cup�rer les options d'affichage
        @return Les options d'affichage pour le <i>tab</i>i�me onglet
     */
    QStyleOptionTab QBar::QBarPrivate::getStyleOption(int tab) const
    {
        Q_Q(const QBar);

        const QTab*     ptab        = tabList.at(tab);
        QStyleOptionTab opt;
        opt.init(q);
        opt.state   &= ~(QStyle::State_HasFocus | QStyle::State_MouseOver);
        opt.rect    = q->tabRect(tab);
        opt.row     = 0;
        opt.shape   = shape;
        opt.text    = ptab->text();
        opt.icon    = ptab->icon();

        bool            isCurrent   = tab == currentIndex;

        if (tab == pressedIndex)
        {
            opt.state |= QStyle::State_Sunken;
        }
        if (isCurrent)
        {
            opt.state |= QStyle::State_Selected;
        }
        if (isCurrent && q->hasFocus())
        {
            opt.state |= QStyle::State_HasFocus;
        }
        if (q->isEnabled() && ptab->isEnabled())
        {
            opt.state |= QStyle::State_Enabled;
        }
        else
        {
            opt.state &= ~QStyle::State_Enabled;
        }
        if (q->isActiveWindow())
        {
            opt.state |= QStyle::State_Active;
        }
        if (opt.rect == hoverRect)
        {
            opt.state |= QStyle::State_MouseOver;
        }

        int totalTabs = tabList.size();

        if (0 < tab && tab - 1 == currentIndex)
        {
            opt.selectedPosition = QStyleOptionTab::PreviousIsSelected;
        }
        else if (tab < totalTabs - 1 && tab + 1 == currentIndex)
        {
            opt.selectedPosition = QStyleOptionTab::NextIsSelected;
        }
        else
        {
            opt.selectedPosition = QStyleOptionTab::NotAdjacent;
        }

        if (tab == 0)
        {
            if (1 < totalTabs)
            {
                opt.position = q->isModernStyleEnabled() ? QStyleOptionTab::Middle : QStyleOptionTab::Beginning;
            }
            else
            {
                opt.position = q->isModernStyleEnabled() ? QStyleOptionTab::Middle : QStyleOptionTab::OnlyOneTab;
            }
        }
        else if (tab == totalTabs - 1)
        {
            opt.position = QStyleOptionTab::End;
        }
        else
        {
            if (q->isModernStyleEnabled())
            {
                opt.position = (width() - opt.rect.right() < 2) ? QStyleOptionTab::End : QStyleOptionTab::Middle;
            }
            else
            {
                opt.position = QStyleOptionTab::Middle;
            }
        }

        return opt;
    }


//------------------------------------------------------------------------------
//                      QBarPrivate : Dimension et Position
//------------------------------------------------------------------------------

    /*! Selon le style, l'espace utilisable pour l'affichage des onglets peut varier (� cause des boutons
        de d�filement). Cette m�thode permet de conna�tre cet espace disponible sans se soucier de la
        configuration actuelle.
        @return La largeur de l'espace disponible pour les onglets
     */
    int QBar::QBarPrivate::width() const
    {
        Q_Q(const QBar);

        if (q->isModernStyleEnabled())
        {
            return q->width() - (modernLeftB->width() + modernRightB->width());
        }
        else
        {
            return q->width();
        }
    }


    /*! Selon le style, la zone d'affichage des onglets pourra "se d�placer" (� cause des boutons de
        d�filement). Cette m�thode permet de conna�tre la position gauche de cette zone sans se soucier
        de la configuration actuelle.
        @return Le d�but de la zone d'affichage des onglets
     */
    int QBar::QBarPrivate::left() const
    {
        Q_Q(const QBar);

        if (q->isModernStyleEnabled())
        {
            return modernLeftB->width();
        }
        else
        {
            return 0;
        }
    }


//------------------------------------------------------------------------------
//                        QBarPrivate : Acc�s aux onglets
//------------------------------------------------------------------------------

    /*! @param index Indice de l'onglet � r�cup�rer
        @return L'<i>index</i>i�me onglet
     */
    QTab* QBar::QBarPrivate::at(int index)
    {
        return validIndex(index) ? tabList[index] : nullptr;
    }


    /*! @param index Indice de l'onglet � r�cup�rer
        @return L'<i>index</i>i�me onglet (constant)
     */
    const QTab* QBar::QBarPrivate::at(int index) const
    {
        return validIndex(index) ? tabList[index] : nullptr;
    }


    /*! Permet d'identifier l'onglet � une position donn�e (qui peut correspondre au curseur de la souris).
        @param p Position � laquelle chercher un onglet
        @return L'onglet sous la position <i>p</i>, -1 si aucun n'est trouv�
     */
    int QBar::QBarPrivate::indexAtPos(const QPoint &p) const
    {
        Q_Q(const QBar);

        if (q->tabRect(currentIndex).contains(p))
        {
            return currentIndex;
        }
        for(int i = 0; i < tabList.count(); ++i)
        {
            if (tabList.at(i)->isEnabled() && q->tabRect(i).contains(p))
            {
                return i;
            }
        }

        return -1;
    }


    /*! Permet de d�terminer s'il existe un onglet pour un indice donn�.
        @param index Indice pour lequel v�rifier s'il existe un onglet
        @return Vrai s'il la barre contient un onglet d'indice <i>index</i>, Faux sinon
     */
    inline bool QBar::QBarPrivate::validIndex(int index) const
    {
        return 0 <= index && index < tabList.count();
    }


//------------------------------------------------------------------------------
//                     QBarPrivate : Gestion de l'affichage
//------------------------------------------------------------------------------

    /*! Effectue un d�filement de la vue sur les onglets, en style classique ou moderne, en fonction de
        la fl�che cliqu�e.
        @sa QBar::scrollTabs
     */
    void QBar::QBarPrivate::scrollTabs()
    {
        Q_Q(QBar);

        const QObject*  sender  = q->sender();

        if      (sender == modernLeftB)
        {
            q->setCurrentIndex(q->currentIndex() - 1);
        }
        else if (sender == modernRightB)
        {
            q->setCurrentIndex(q->currentIndex() + 1);
        }
        else if (sender == classicLeftB)
        {
            for (int i = tabList.count() - 1; 0 <= i; --i)
            {
                if (tabList.at(i)->rect().left() - scrollOffset < 0)
                {
                    makeVisible(i);
                    return;
                }
            }
        }
        else if (sender == classicRightB)
        {
            int availableWidth = q->width() - extraWidth();

            for (int i = 0; i < tabList.count(); ++i)
            {
                if (availableWidth < tabList.at(i)->rect().right() - scrollOffset)
                {
                    makeVisible(i);
                    return;
                }
            }
        }
    }


    /*! Provoque un rafra�chissement de la barre, avec r�organisation des onglets, si la barre est
        visible. Sinon, la mise � jour aura lieu
        plus tard (d�s que ce sera n�cessaire).
     */
    void QBar::QBarPrivate::refresh()
    {
        Q_Q(QBar);

        if (!q->isVisible())
        {
            layoutDirty = true;
        }
        else
        {
            layoutTabs();
            q->update();
            q->updateGeometry();
        }
    }


    /*! R�organise l'agencement des onglets, en fonction du style de la barre, du nombre d'onglets et de
        leur style,...
     */
    void QBar::QBarPrivate::layoutTabs()
    {
        Q_Q(QBar);

        if (q->isModernStyleEnabled())
        {
            if (!validIndex(currentIndex))
            {
                return;
            }

            layoutDirty     = false;
            int visibleOffset = 0;
            for(int i = 0; i < currentIndex; ++i)
            {
                if (0 < tabList[i]->rect().width())
                {
                    visibleOffset += q->tabSizeHint(i).width();
                }
            }
            int maxHeight       = 0;
            int visibleWidth    = 0;
            int visibleCount    = 0;

            if (tabList[currentIndex]->rect().width() <= 0)
            {
                if (visibleOffset == 0)
                {
                    int x   = left();
                    // L'onglet actuel est � gauche de la partie visible.
                    for(int i = 0; i < tabList.count(); ++i)
                    {
                        QSize   sz = q->tabSizeHint(i);
                        if (currentIndex <= i && visibleWidth + sz.width() < width())
                        {
                            tabList[i]->setRect(QRect(x, 0, sz.width(), sz.height()));
                            x += sz.width();
                            visibleWidth += sz.width();
                            maxHeight = qMax(maxHeight, sz.height());
                            ++visibleCount;
                        }
                        else
                        {
                            tabList[i]->setRect(QRect());
                        }
                    }
                }
                else // 0 < visibleOffset
                {
                    int x   = width();
                    // L'onglet actuel est � droite de la partie visible.
                    for(int i = tabList.count() - 1; 0 <= i ; --i)
                    {
                        QSize   sz = q->tabSizeHint(i);
                        if (i <= currentIndex && visibleWidth + sz.width() < width())
                        {
                            x -= sz.width();
                            tabList[i]->setRect(QRect(x, 0, sz.width(), sz.height()));
                            visibleWidth += sz.width();
                            maxHeight = qMax(maxHeight, sz.height());
                            ++visibleCount;
                        }
                        else
                        {
                            tabList[i]->setRect(QRect());
                        }
                    }
                }
            }
            else
            {
                // Si le style lors du pr�c�dent appel �tait "classique", il faut s'assurer de la
                // validit� de l'"offset visible" : s'il est trop � gauche, l'onglet s�lectionn�
                // est d�plac� sur le c�t� droit de la zone de visibilit� des onglets.
                if (width() < visibleOffset + q->tabSizeHint(currentIndex).width())
                {
                    visibleOffset = width() - q->tabSizeHint(currentIndex).width() - 1;
                }
                // Cherche des widgets sur la droite (d'abord).
                int x = visibleOffset;
                for(int i = currentIndex; i < tabList.count(); ++i)
                {
                    QSize   sz = q->tabSizeHint(i);
                    if (x + sz.width() < width())
                    {
                        tabList[i]->setRect(QRect(x, 0, sz.width(), sz.height()));
                        x += sz.width();
                        visibleWidth += sz.width();
                        maxHeight = qMax(maxHeight, sz.height());
                        ++visibleCount;
                    }
                    else
                    {
                        tabList[i]->setRect(QRect());
                    }
                }
                // Puis sur la gauche.
                x = width() - visibleWidth;
                for(int i = currentIndex - 1; (0 <= i); --i)
                {
                    QSize   sz = q->tabSizeHint(i);
                    x -= sz.width();
                    if (0 <= x)
                    {
                        tabList[i]->setRect(QRect(x, 0, sz.width(), sz.height()));
                        visibleWidth += sz.width();
                        maxHeight = qMax(maxHeight, sz.height());
                        ++visibleCount;
                    }
                    else
                    {
                        tabList[i]->setRect(QRect());
                    }
                }
            }
            // Parcourt la liste et s'assure que la hauteur des onglets est uniforme,
            // tout en r�partissant les pixels restant au besoin.
            int remainingWidth  = (visibleCount < tabList.count()) ? width() - visibleWidth : 0;
            if (visibleCount == 0)
            {
                // Pour la 1�re insertion d'onglet, il peut arriver qu'il manque de la place...
                //  D'o� un petit hack.
                maxHeight = q->tabSizeHint(currentIndex).height();
                tabList[currentIndex]->setRect(QRect(0, 0, remainingWidth, maxHeight));
                visibleCount = 1;
            }
            int x   = left();
            for(int i = 0; i < tabList.count(); ++i)
            {
                QRect   r = tabList[i]->rect();
                if (0 < r.width())
                {
                    r = QRect(x, 0, r.width() + remainingWidth / visibleCount, maxHeight);
                    x = r.right();
                    tabList[i]->setRect(r);
                }
            }
            // Active les boutons au besoin.
            modernLeftB->setEnabled(0 < currentIndex);
            modernLeftB->move(0, 0);
            modernLeftB->resize(modernLeftB->width(), arrowHeight());

            modernRightB->setEnabled(currentIndex < tabList.count() - 1);
            modernRightB->move(modernLeftB->width() + width(), 0);
            modernRightB->resize(modernRightB->width(), arrowHeight());
        }
        else
        {
            // Style classique.
            scrollOffset    = 0;
            layoutDirty     = false;
            QSize   size = q->size();

            int x           = 0;
            int maxHeight   = 0;
            for(int i = 0; i < tabList.count(); ++i)
            {
                QSize   sz = q->tabSizeHint(i);
                tabList[i]->setRect(QRect(x, 0, sz.width(), sz.height()));
                maxHeight = qMax(maxHeight, sz.height());
                x += sz.width();
            }

            // Parcourt la liste et s'assure que la hauteur des onglets est uniforme.
            for(int i = 0; i < tabList.count(); ++i)
            {
                QRect   r = tabList[i]->rect();
                r.setHeight(maxHeight);
                tabList[i]->setRect(r);
            }

            int last = x;
            int available = size.width();

            if (0 < tabList.count() && available < last)
            {
                int extra   = extraWidth();

                QRect   arrows = QStyle::visualRect(Qt::LeftToRight, q->rect(), QRect(available - extra, 0, extra, arrowHeight()));
                classicLeftB->setGeometry(arrows.left(), arrows.top(), extra / 2, arrows.height());
                classicRightB->setGeometry(arrows.right() - extra / 2 + 1, arrows.top(), extra / 2, arrowHeight());
                classicLeftB->setArrowType(Qt::LeftArrow);
                classicRightB->setArrowType(Qt::RightArrow);

                classicLeftB->setEnabled(0 < scrollOffset);
                classicRightB->setEnabled(available - extra < last - scrollOffset);
                classicLeftB->show();
                classicRightB->show();
            }
            else
            {
                classicLeftB->hide();
                classicRightB->hide();
            }
        }

        q->tabLayoutChange();
    }


    /*! Rend un onglet visible, au besoin en effectuant un d�fliement des autres onglets.
        @param index Indice de l'onglet � rendre visible
        @note Cette m�thode est surtout utile pour le mode classique
     */
    void QBar::QBarPrivate::makeVisible(int index)
    {
        Q_Q(QBar);

        if (!validIndex(index) || classicLeftB->isHidden())
        {
            return;
        }

        const QRect tabRect = tabList.at(index)->rect();

        const int   oldScrollOffset = scrollOffset;
        const int   available       = q->width() - extraWidth();
        const int   start           = tabRect.left();
        const int   end             = tabRect.right();

        if      (start < scrollOffset)              // Pas assez � gauche
        {
            scrollOffset = start - (index ? 8 : 0);
        }
        else if (scrollOffset + available < end)    // Pas assez � droite.
        {
            scrollOffset = end - available + 1;
        }

        if (0 < scrollOffset && end < available)    // Y a-t-il vraiment besoin de d�filement ?
        {
            scrollOffset = 0;
        }

        classicLeftB->setEnabled(0 < scrollOffset);

        const int last = tabList.last()->rect().right();
        classicRightB->setEnabled(available < last - scrollOffset);

        if (oldScrollOffset != scrollOffset)
        {
            q->update();
        }
    }


//------------------------------------------------------------------------------
//                       QBar : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Imposer un QDock pour parent permet d'assurer la validit� des ascendants retourn�s par dock() et
        tabWidget().
        @param parent Dock parent de la barre
     */
    QBar::QBar(QDock* parent)
        : QWidget(parent, nullptr)
        , d_custom_ptr(new QBarPrivate(this))
    {
        Q_D(QBar);

        d->init();
    }


    /*! Destructeur.
     */
    QBar::~QBar() { }


//------------------------------------------------------------------------------
//                               QBar : Ascendants
//------------------------------------------------------------------------------

    /*! Permet de conna�tre le dock (parent direct) auquel appartient cette barre.
        @return Le dock parent
     */
    QDock* QBar::dock() const
    {
        return qobject_cast<QDock*>(parent());
    }


    /*! Permet de conna�tre le tabwidget (grand-parent) auquel appartient le dock de cette barre.
        @return Le tabwidget grand-parent
     */
    QTabWidget* QBar::tabWidget() const
    {
        QDock*  d = dock();

        if (d != nullptr)
        {
            return d->tabWidget();
        }
        else
        {
            return nullptr;
        }
    }


//------------------------------------------------------------------------------
//                          QBar : Gestion des Onglets
//------------------------------------------------------------------------------

    /*! Ajoute un onglet � la barre.
        @param tab Onglet � ajouter � la barre
        @return L'indice de l'onglet sur la barre
        @note Ce n'est pas la barre qui notifiera � l'onglet sa [nouvelle] barre porteuse
     */
    int QBar::addTab(QTab* tab)
    {
        return insertTab(tab, -1);
    }


    /*! Ins�re un onglet parmi ceux d�j� pr�sents sur la barre.
        @param tab   Onglet � ajouter � la barre
        @param index Position souhait�e pour l'onglet, -1 pour l'ajouter en derni�re position
        @return L'indice de l'onglet sur la barre
        @note Ce n'est pas la barre qui notifiera � l'onglet sa [nouvelle] barre porteuse
     */
    int QBar::insertTab(QTab* tab, int index)
    {
        Q_D(QBar);

        if (!d->validIndex(index))
        {
            index   = d->tabList.count();
            d->tabList.append(tab);
        }
        else
        {
            d->tabList.insert(index, tab);
        }

        if      (currentIndex() == -1)
        {
            setCurrentIndex(index);
        }
        else if (index <= currentIndex())
        {
            setCurrentIndex(currentIndex() + 1);
        }

        d->tabList[index]->setShortcutId(grabShortcut(QKeySequence::mnemonic(tab->name())));
        d->layoutTabs();
        d->refresh();
        tabInserted(index);
        return index;
    }


    /*! Retire un onglet de la barre.
        @param index Indice de l'onglet � retirer
        @note L'onglet ne sera pas d�truit
     */
    void QBar::removeTab(int index)
    {
        Q_D(QBar);

        if (d->validIndex(index))
        {
            releaseShortcut(d->tabList.at(index)->shortcutId());
            d->tabList.removeAt(index);
            if (index == d->currentIndex)
            {
                setCurrentIndex(d->validIndex(index) ? index : index - 1);
            }
            d->layoutTabs();
            d->refresh();
            tabRemoved(index);
        }
    }


    /*! Certaines modifications sur les onglets n'impliqueront pas une mise � jour implicite de leur
        aspect visuel dans la barre. Afin de forcer cette mise � jour, cette m�thode peut �tre appel�e.
     */
    void QBar::updateTabs()
    {
        Q_D(QBar);

        d->layoutTabs();
        update();
    }


//------------------------------------------------------------------------------
//                      QBar : Informations sur les Onglets
//------------------------------------------------------------------------------

    /*! Permet d'identifier l'onglet � une position donn�e (qui peut correspondre au curseur de la souris).
        @param p Position � laquelle chercher un onglet
        @return L'indice de l'onglet sous la position <i>p</i>, -1 si aucun n'est trouv�
     */
    int QBar::indexAtPos(const QPoint &p) const
    {
        Q_D(const QBar);

        return d->indexAtPos(p);
    }


    /*! Permet de conna�tre la g�om�trie (rectangulaire, n�cessairement) d'un onglet donn�.
        @param index Indice de l'onglet dont on souhaite conna�tre la g�om�trie
        @return Un rectangle correspondant � la g�om�trie de l'<i>index</i>i�me onglet
     */
    QRect QBar::tabRect(int index) const
    {
        Q_D(const QBar);

        if (const QTab* tab = d->at(index))
        {
            if (d->layoutDirty)
            {
                const_cast<QBarPrivate*>(d)->layoutTabs();
            }

            QRect   r = tab->rect();
            if (!isModernStyleEnabled())
            {
                r.translate(- d->scrollOffset, 0);
            }

            return QStyle::visualRect(Qt::LeftToRight, rect(), r);
        }

        return QRect();
    }


    /*! Permet de conna�tre l'onglet actuel.
        @return L'indice de l'onglet actuel
     */
    int QBar::currentIndex() const
    {
        Q_D(const QBar);

        if (d->validIndex(d->currentIndex))
        {
            return d->currentIndex;
        }

        return -1;
    }


    /*! Permet de savoir combien d'onglets porte la barre.
        @return Le nombre d'onglets de la barre
     */
    int QBar::count() const
    {
        Q_D(const QBar);

        return d->tabList.count();
    }


    /*! Permet de conna�tre la forme des onglets.
        @return La valeur de QTabBar::Shape identifiant la forme des onglets
     */
    int QBar::shape() const
    {
        return QBarPrivate::shape;
    }


//------------------------------------------------------------------------------
//                               QBar : Dimensions
//------------------------------------------------------------------------------

    /*! Sugg�re une taille pour la barre.
        @return La taille recommand�e pour la barre
     */
    QSize QBar::sizeHint() const
    {
        Q_D(const QBar);

        if (d->layoutDirty)
        {
            const_cast<QBarPrivate*>(d)->layoutTabs();
        }

        QRect   r;
        for(int i = 0; i < d->tabList.count(); ++i)
        {
            r = r.unite(d->tabList.at(i)->rect());
        }

        return r.size().expandedTo(QApplication::globalStrut());
    }


    /*! Sugg�re une taille minimale pour la barre (qui, normalement, ne sera pas r�duite en-de�a �
        l'affichage)
        @return La taille minimale recommand�e pour la barre
     */
    QSize QBar::minimumSizeHint() const
    {
        Q_D(const QBar);

        if (style()->styleHint(QStyle::SH_TabBar_PreferNoArrows, 0, this))
        {
            return sizeHint();
        }

        return QSize(d->classicRightB->sizeHint().width() * 2 + 75, sizeHint().height());
    }


//------------------------------------------------------------------------------
//                                 QBar : Style
//------------------------------------------------------------------------------

    /*! Permet de savoir quel est le style actuel de la barre.
        @return Vrai si le style moderne est activ�, Faux si c'est le classique
     */
    bool QBar::isModernStyleEnabled() const
    {
        Q_D(const QBar);

        return d->barStyle == Modern;
    }


//------------------------------------------------------------------------------
//                                 QBar : Slots
//------------------------------------------------------------------------------

    /*! S�lectionne un onglet.
        @param index Indice de l'onglet � �tablir comme nouvel onglet actuel
        @note Le signal currentChanged est �mis en cas de changement d'onglet actuel r�ussi
     */
    void QBar::setCurrentIndex(int index)
    {
        Q_D(QBar);

        if (d->validIndex(index))
        {
            d->currentIndex = index;

            update();
            d->makeVisible(index);

            if (isModernStyleEnabled())
            {
                d->layoutTabs();
            }

            emit currentChanged(index);
        }
    }


    /*! Permet d'activer le style moderne.
        @param enabled Vrai pour activer le style moderne, Faux pour le d�sactiver (et le remplacer par
        le style classique)
     */
    void QBar::setModernStyleEnabled(bool enabled)
    {
        Q_D(QBar);

        d->barStyle = enabled ? Modern : Classic;
        d->layoutTabs();

        if (isModernStyleEnabled())
        {
            // Cache les boutons de d�filement "classique".
            d->classicLeftB->hide();
            d->classicRightB->hide();
            // Et affiche les nouveaux.
            d->modernLeftB->show();
            d->modernRightB->show();
        }
        else
        {
            d->modernLeftB->hide();
            d->modernRightB->hide();
            d->makeVisible(currentIndex());
        }

        update();
    }


    /*! Permet d'activer le style classique.
        @param enabled Vrai pour activer le style classique, Faux pour le d�sactiver (et le remplacer par
        le style moderne)
     */
    void QBar::setClassicStyleEnabled(bool enabled)
    {
        setModernStyleEnabled(!enabled);
    }


//------------------------------------------------------------------------------
//                  QBar : Extension de la Gestion des Onglets
//------------------------------------------------------------------------------

    /*! Permet de conna�tre la taille recommand�e pour l'affichage d'un onglet donn�, en fonction de ses
        attributs, du style de la barre,...
        @param index Indice de l'onglet pour lequel on souhaite conna�tre une suggestion de taille
        @return Taille recommand�e pour l'onglet d'indice <i>index</i>
     */
    QSize QBar::tabSizeHint(int index) const
    {
        Q_D(const QBar);

        if (const QTab* tab = d->at(index))
        {
            QStyleOptionTab     opt         = d->getStyleOption(index);
            int                 iconExtent  = style()->pixelMetric(QStyle::PM_SmallIconSize, &opt, this);
            QSize               iconSize    = tab->icon().isNull() ? QSize() : QSize(iconExtent, iconExtent);
            int                 hframe      = 5;
            int                 vframe      = 5;
            const QFontMetrics  fm          = fontMetrics();

            QSize csz(fm.size(Qt::TextShowMnemonic, tab->text()).width() + iconSize.width() + hframe,
                      qMax(fm.height(), iconSize.height()) + vframe);

            return style()->sizeFromContents(QStyle::CT_TabBarTab, &opt, csz, this);
        }

        return QSize();
    }


    /*! Apr�s ajout (ou insertion) r�ussi d'un onglet dans la barre, cette m�thode est appel�e. Elle peut
        �tre r�impl�ment�e afin d'effectuer une quelconque t�che venant compl�ter l'ajout.<br>
        Par d�faut, ne fait rien.
     */
    void QBar::tabInserted(int index)
    {
        Q_UNUSED(index)
    }


    /*! Apr�s retrait r�ussi d'un onglet dans la barre, cette m�thode est appel�e. Elle peut �tre
        r�impl�ment�e afin d'effectuer une quelconque t�che venant compl�ter le retrait.<br>
        Par d�faut, ne fait rien.
     */
    void QBar::tabRemoved(int index)
    {
        Q_UNUSED(index)
    }


    /*! Apr�s r�organisation de l'agencement des onglets, cette m�thode est appel�e. Elle peut �tre
        r�impl�ment�e afin d'effectuer une quelconque t�che venant compl�ter le r�agencement des onglets.
        <br>Par d�faut, ne fait rien.
     */
    void QBar::tabLayoutChange()
    {
    }


//------------------------------------------------------------------------------
//                         QBar : Gestion des Ev�nements
//------------------------------------------------------------------------------

    /*! Traitement d'un �v�nement de changement d'�tat.
        @param e Ev�nement � traiter
     */
    void QBar::changeEvent(QEvent* e)
    {
        Q_D(QBar);

        d->refresh();
        QWidget::changeEvent(e);
    }


    /*! Traitement g�n�ral des �v�nements. La plupart des �v�nements sont trait�s dans des m�thodes plus
        sp�cialis�es. Cette m�thode s'occupe des QHelpEvent, QHoverEVent & QShortcutEvent, qui n'ont pas
        de m�thode sp�cialis�e associ�e.
        @param e Ev�nement � traiter
     */
    bool QBar::event(QEvent* e)
    {
        Q_D(QBar);

        if (e->type() == QEvent::HoverMove || e->type() == QEvent::HoverEnter)
        {
            QHoverEvent*    he = static_cast<QHoverEvent *>(e);
            if (!d->hoverRect.contains(he->pos()))
            {
                QRect   oldHoverRect = d->hoverRect;
                for(int i = 0; i < d->tabList.count(); ++i)
                {
                    QRect   area = tabRect(i);
                    if (area.contains(he->pos()))
                    {
                        d->hoverRect = area;
                        break;
                    }
                }

                if (he->oldPos() != QPoint(-1, -1))
                {
                    update(oldHoverRect);
                }
                update(d->hoverRect);
            }
        }
        else if (e->type() == QEvent::HoverLeave)
        {
            QRect   oldHoverRect = d->hoverRect;
            d->hoverRect = QRect();
            update(oldHoverRect);
        }
        else if (e->type() == QEvent::ToolTip)
        {
            if (const QTab* tab = d->at(d->indexAtPos(static_cast<QHelpEvent*>(e)->pos())))
            {
                if (!tab->toolTip().isEmpty())
                {
                    QToolTip::showText(static_cast<QHelpEvent*>(e)->globalPos(), tab->toolTip(), this);

                    return true;
                }
            }
        }
        else if (e->type() == QEvent::Shortcut)
        {
            QShortcutEvent* se = static_cast<QShortcutEvent*>(e);
            for(int i = 0; i < d->tabList.count(); ++i)
            {
                const QTab* tab = d->tabList.at(i);

                if (tab->shortcutId() == se->shortcutId())
                {
                    setCurrentIndex(i);

                    return true;
                }
            }
        }

        return QWidget::event(e);
    }


    /*! Traitement des frappes sur les touches du clavier.
        @param e Ev�nement � traiter
     */
    void QBar::keyPressEvent(QKeyEvent* e)
    {
        Q_D(QBar);

        if (e->key() != Qt::Key_Left && e->key() != Qt::Key_Right)
        {
            e->ignore();
            return;
        }

        int dx  = e->key() == (isRightToLeft() ? Qt::Key_Right : Qt::Key_Left) ? -1 : 1;

        for(int index = d->currentIndex + dx; d->validIndex(index); index += dx)
        {
            if (d->tabList.at(index)->isEnabled())
            {
                setCurrentIndex(index);
                break;
            }
        }
    }


    /*! Traitement des d�placements de la souris (avec bouton(s) enfonc�(s)).
        @param e Ev�nement � traiter
     */
    void QBar::mouseMoveEvent(QMouseEvent* e)
    {
        Q_D(QBar);

        if (e->buttons() != Qt::LeftButton)
        {
            e->ignore();
            return;
        }

        if (style()->styleHint(QStyle::SH_TabBar_SelectMouseType, 0, this) == QEvent::MouseButtonRelease)
        {
            int i   = d->indexAtPos(e->pos());

            if (i != d->pressedIndex)
            {
                int oldIndex    = d->pressedIndex;
                d->pressedIndex = -1;

                if (0 <= oldIndex)
                {
                    repaint(tabRect(oldIndex));
                }
                if (0 <= (d->pressedIndex = i))
                {
                    repaint(tabRect(i));
                }
            }
        }

        if (d->drag == nullptr)
        {
            if (QApplication::startDragDistance() <= (e->pos() - d->dragStartPosition).manhattanLength())
            {
                int i   = d->indexAtPos(e->pos());

                if (d->validIndex(i))
                {
                    d->drag.reset(new QDragTab(d->tabList[i]));
                    d->drag->drag(e->globalPos());
                }
            }
        }
        else
        {
            d->drag->move(e->globalPos());
        }
    }


    /*! Traitement des pressions sur les boutons de la souris.
        @param e Ev�nement � traiter
     */
    void QBar::mousePressEvent(QMouseEvent* e)
    {
        Q_D(QBar);

        if (e->button() != Qt::LeftButton)
        {
            e->ignore();
            return;
        }

        d->dragStartPosition = e->pos();

        d->pressedIndex = d->indexAtPos(e->pos());

        if (0 <= d->pressedIndex)
        {
            if (e->type() == style()->styleHint(QStyle::SH_TabBar_SelectMouseType, 0, this))
            {
                setCurrentIndex(d->pressedIndex);
            }
            else
            {
                repaint(tabRect(d->pressedIndex));
            }
        }
    }


    /*! Traitement des rel�chement des boutons de la souris.
        @param e Ev�nement � traiter
     */
    void QBar::mouseReleaseEvent(QMouseEvent* e)
    {
        Q_D(QBar);

        if      (e->button() == Qt::RightButton)
        {
            selectTabByMenu();
        }
        else if (e->button() != Qt::LeftButton)
        {
            e->ignore();
        }

        if (d->drag != nullptr)
        {
            d->drag->drop(e->globalPos());
            d->drag.reset();
        }

        int i   = d->indexAtPos(e->pos()) == d->pressedIndex ? d->pressedIndex : -1;
        d->pressedIndex = -1;

        if (e->type() == style()->styleHint(QStyle::SH_TabBar_SelectMouseType, 0, this))
        {
            setCurrentIndex(i);
        }
    }


    /*! Traitement des �venements demandant le dessin de la barre.
        @param e Ev�nement � traiter
     */
    void QBar::paintEvent(QPaintEvent*)
    {
        Q_D(QBar);

        // Dessine le haut du cadre des pages.
        QStyleOptionTab tabOverlap;
        tabOverlap.shape = d->shape;
        int             overlap     = style()->pixelMetric(QStyle::PM_TabBarBaseOverlap, &tabOverlap, this);
        QWidget*        theParent   = parentWidget();

        if (theParent && 0 < overlap)
        {
            QPainter::setRedirected(theParent, this, pos());
            QPaintEvent e(QRect(- d->modernLeftB->width(), height() - overlap, width(), overlap));
            QApplication::sendEvent(theParent, &e);
            QPainter::restoreRedirected(theParent);
        }

        QStylePainter   p(this);
        int             selected    = -1;
        int             cut         = -1;

        QStyleOptionTab         cutTab;
        QStyleOptionTab         selectedTab;

        // Dessine tous les onglets visibles, sauf celui s�lectionn�.
        for (int i = 0; i < d->tabList.count(); ++i)
        {
            QStyleOptionTab tab = d->getStyleOption(i);
            // Si cet onglet est partiellement cach�, on le garde dans un
            // coin pour transmettre l'information quand il faudra dessiner.
            if (tab.rect.left() < 0)
            {
                cut    = i;
                cutTab = tab;
            }

            // Pas la peine de dessiner un onglet enti�rement hors de la barre.
            if (tab.rect.right() < 0 || width() < tab.rect.left() || tab.rect.width() == 0)
            {
                continue;
            }
            if (i == d->currentIndex)
            {
                selected    = i;
                selectedTab = tab;
                continue;
            }

            p.drawControl(QStyle::CE_TabBarTabShape, tab);

            QRect   tr          = tab.rect;
            int     alignment   = Qt::AlignCenter | Qt::TextShowMnemonic;
            if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &tab, this))
            {
                alignment |= Qt::TextHideMnemonic;
            }
            if (!tab.icon.isNull())
            {
                QPixmap tabIcon = tab.icon.pixmap(style()->pixelMetric(QStyle::PM_SmallIconSize),
                                                  (tab.state & QStyle::State_Enabled) ? QIcon::Normal : QIcon::Disabled);
                p.drawPixmap(tr.left() + 2, tr.center().y() - tabIcon.height() / 2 + 2, tabIcon);
                tr.setLeft(tr.left() + tabIcon.width() + 0);
            }
            tr.setTop(tr.top() + 4);
            style()->drawItemText(&p, tr, alignment, tab.palette, tab.state & QStyle::State_Enabled, tab.text, QPalette::Foreground);
        }

        // Dessine l'onglet s�lectionn� en dernier pour l'avoir "au-dessus".
        if (0 <= selected)
        {
            QStyleOptionTab tab         = d->getStyleOption(selected);
            // Supprime tout artefact visible � l'emplacement de l'onglet.
            p.eraseRect(tab.rect);
            p.drawControl(QStyle::CE_TabBarTabShape, tab);

            QRect           tr          = tab.rect;
            tr.setBottom(tr.bottom() - style()->pixelMetric(QStyle::PM_TabBarTabShiftVertical, &tab, this));
            tr.setRight(tr.right() - style()->pixelMetric(QStyle::PM_TabBarTabShiftHorizontal, &tab, this) - 2);

            int             alignment   = Qt::AlignCenter | Qt::TextShowMnemonic;
            if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &tab, this))
            {
                alignment |= Qt::TextHideMnemonic;
            }
            if (!tab.icon.isNull())
            {
                QPixmap tabIcon = tab.icon.pixmap(style()->pixelMetric(QStyle::PM_SmallIconSize),
                                                  (tab.state & QStyle::State_Enabled) ? QIcon::Normal : QIcon::Disabled);
                p.drawPixmap(tr.left() + 2, tr.center().y() - tabIcon.height() / 2 + 1, tabIcon);
                tr.setLeft(tr.left() + tabIcon.width() + 0);
            }
            tr.setTop(tr.top() + 4);
            style()->drawItemText(&p, tr, alignment, tab.palette, tab.state & QStyle::State_Enabled, tab.text, QPalette::Foreground);
        }

        if (!isModernStyleEnabled())
        {
            // Ne dessine la bordure d'un onglet coup�, � gauche,
            // c'est utile (la plupart du temps, �a ne l'est pas).
            if (d->classicLeftB->isVisible() && 0 <= cut)
            {
                cutTab.rect = rect();
                cutTab.rect = style()->subElementRect(QStyle::SE_TabBarTearIndicator, &cutTab, this);
                p.drawPrimitive(QStyle::PE_IndicatorTabTear, cutTab);
            }
        }
    }


    /*! Traitement des �v�nement de redimensionnement.
        @param e Ev�nement � traiter
     */
    void QBar::resizeEvent(QResizeEvent*)
    {
        Q_D(QBar);

        d->layoutTabs();
        d->makeVisible(d->currentIndex);
    }


    /*! Traitement des �v�nements demandant de montrer la barre.
        @param e Ev�nement � traiter
     */
    void QBar::showEvent(QShowEvent*)
    {
        Q_D(QBar);

        if (d->layoutDirty)
        {
            d->layoutTabs();
        }

        if (!d->validIndex(d->currentIndex))
        {
            setCurrentIndex(0);
        }
    }


//------------------------------------------------------------------------------
//                            QBar : Autres M�thodes
//------------------------------------------------------------------------------

    /*! Effectue un d�filement des onglets, ou de la vue sur les onglets, selon la fl�che de d�filement
        cliqu�e (et aussi un peu le style).
     */
    void QBar::scrollTabs()
    {
        Q_D(QBar);

        d->scrollTabs();
    }


    /*! Affiche un menu (avec ic�ne et titre d'onglet) afin de s�lectionner un des onglets.
     */
    void QBar::selectTabByMenu()
    {
        Q_D(QBar);

        QMenu   selectMenu;

        for(int i = 0; i < d->tabList.count(); ++i)
        {
            const QTab* tab = d->tabList.at(i);
            QAction*    a   = selectMenu.addAction(tab->icon(), tab->title());
            a->setData(i);
        }

        QAction*    res = selectMenu.exec(QCursor::pos());

        if (res != nullptr)
        {
            QVariant    data = res->data();
            bool        ok;
            int         select = data.toInt(&ok);

            if (ok && d->validIndex(select))
            {
                setCurrentIndex(select);
            }
        }
    }
}
