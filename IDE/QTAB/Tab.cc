/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Tab.hh"

/*! @file IDE/QTAB/Tab.cc
    @brief M�thodes (non-inline) des classes QTAB::QTab::QTabPrivate & QTAB::QTab.
    @author @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QAction>
#include <QIcon>
#include <QRect>
#include <QString>
#include <QVariant>

#include "Bar.hh"
#include "Page.hh"
#include "TabWidget.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                         QTabPrivate : P-Impl de QTab
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QTab.
        @version 1.0

        A l'image de ce qui a �t� fait pour QBar, cette classe fournit une grande partie de
        l'imp�mentation de QTab.
     */
    class QTab::QTabPrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QTab)
    public:
        //! @name Constructeurs
        //@{
        QTabPrivate(QTab* parent);          //!< Constructeur "dynamique".
        QTabPrivate(QTab*     parent,
                    QString (*name)(),
                    QString (*title)(),
                    QIcon     icon,
                    QPage*  (*function)()); //!< Constructeur "local".
        //@}
        //! @name Propri�t�s
        //@{
        QRect       tabRect;                //!< G�om�trie.
        QTab::Style tabStyle;               //!< "Style" d'onglet.
        QIcon       tabIcon;                //!< Ic�ne de l'onglet.
        int         tabShortcutId;          //!< ID de raccourci-clavier.
        //@}
        QBar*       tabBar;                 //!< Barre porteuse.
        QString   (*getNameFunction)();     //!< Foncteur de r�cup�ration du nom.
        QString   (*getTitleFunction)();    //!< Foncteur de r�cup�ration du titre.
        QPage*    (*createPageFunction)();  //!< Foncteur de cr�ation de page.
    };


//------------------------------------------------------------------------------
//                          QTabPrivate : Constructeurs
//------------------------------------------------------------------------------

    /*! Ce constructeur est � appel�e pour les onglets "dynamiques" : ceux � placer dans une barre
        d'onglets.
        @param parent Onglet pour lequel porter des informations
     */
    QTab::QTabPrivate::QTabPrivate(QTab* parent)
        : q_custom_ptr(parent)
        , tabStyle(QTab::Icon)
        , tabShortcutId(0)
        , tabBar(nullptr)
        , getNameFunction(nullptr)
        , getTitleFunction(nullptr)
        , createPageFunction(nullptr)
    { }


    /*! Ce constructeur est � appeler pour les onglets destin�s � �tre des "cr�ateurs" d'onglets
        dynamiques (d'o� les informations pass�es en param�tre; voir QDock::registerTab(QTab*)).
        @param parent   Onglet pour lequel porter des informations
        @param name     Fonction de r�cup�ration du nom de l'onglet (qui sera affich� en tant que texte
        de l'onglet)
        @param title    Fonction de r�cup�ration du titre de l'onglet (affich� dans le cadre du
        <i>tabwidget</i>)
        @param icon     Ic�ne de l'onglet
        @param function Fonction de cr�ation d'une page associ�e � ce type d'onglet
     */
    QTab::QTabPrivate::QTabPrivate(QTab* parent, QString (*name)(), QString (*title)(), QIcon icon, QPage* (*function)())
        : q_custom_ptr(parent)
        , tabStyle(QTab::Icon)
        , tabIcon(icon)
        , tabShortcutId(0)
        , tabBar(nullptr)
        , getNameFunction(name)
        , getTitleFunction(title)
        , createPageFunction(function)
    { }


//------------------------------------------------------------------------------
//                       QTab : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Ce constructeur permet de stocker toutes les informations permettant de cr�er, par la suite, un
        onglet � placer dans une barre
        d'onglets. En effet, afin de pouvoir ajouter � la demande tel ou tel type d'onglet � un tabwidget,
        il faut bien conserver la liste des onglets disponibles. Toutes les informations n�cessaires sont
        donc pass�es en param�tres.
        @param name     Nom de l'onglet (qui sera affich� en tant que texte de l'onglet)
        @param title    Fonction de r�cup�ration du titre de l'onglet (affich� dans le cadre du
        <i>tabwidget</i>)
        @param icon     Ic�ne de l'onglet
        @param function Fonction de cr�ation d'une page associ�e � ce type d'onglet
     */
    QTab::QTab(QString (*name)(), QString (*title)(), QIcon icon, QPage* (*function)())
        : QObject(nullptr)
        , d_custom_ptr(new QTabPrivate(this, name, title, icon, function))
    { }


    /*! Duplique l'onglet.
        @return Une nouvelle instance regroupant les m�mes informations relatives � la construction
        d'onglets � placer dans une barre
     */
    QTab* QTab::clone() const
    {
        Q_D(const QTab);

        return new QTab(d->getNameFunction, d->getTitleFunction, d->tabIcon, d->createPageFunction);
    }


    /*! Cr�e une nouvelle page (et son onglet associ�), � partir des informations stock�es depuis l'appel
        au constructeur QTab(QString, QString, QIcon, QPage*(*)()).
        @return Une page construite par l'onglet
     */
    QPage* QTab::createPage() const
    {
        Q_D(const QTab);

        QPage*  page    = d->createPageFunction();
        QTab*   tab     = new QTab(page);

        tab->d_func()->tabStyle         = d->tabStyle;
        tab->d_func()->tabIcon          = d->tabIcon;
        tab->d_func()->getNameFunction  = d->getNameFunction;
        tab->d_func()->getTitleFunction = d->getTitleFunction;
        // Pas vraiment utile une fois dans une barre, sauf pour contr�ler le drop sur un autre widget.
        tab->d_func()->createPageFunction = d->createPageFunction;

        return page;
    }


    /*! Destructeur
     */
    QTab::~QTab() { }


//------------------------------------------------------------------------------
//                               QTab : Propri�t�s
//------------------------------------------------------------------------------

    /*! Permet de conna�tre la g�om�trie utilis�e actuellement pour afficher l'onglet.
        @return La g�om�trie de l'onglet (peut �tre vide, si l'onglet est chach�).
     */
    QRect QTab::rect() const
    {
        Q_D(const QTab);

        return d->tabRect;
    }


    /*! Etablit la g�om�trie de l'onglet pour son affichage.
        @param r Nouvelle g�om�trie de l'onglet
     */
    void QTab::setRect(QRect r)
    {
        Q_D(QTab);

        d->tabRect = r;
    }


    /*! Permet de conna�tre le style de l'onglet.
        @return Le style actuel de l'onglet
        @sa QTab::Style, setStyle(QAction*)
     */
    QTab::Style QTab::style() const
    {
        Q_D(const QTab);

        return d->tabStyle;
    }


    /*! Permet de conna�tre l'ic�ne � utiliser pour la repr�sentation visuelle de l'onglet dans sa barre.<br>
        Selon le style, il s'agira en fait de l'ic�ne d�finie � la construction, ou d'une image
        correspondant � l'�tat actuel de la page associ�e � l'onglet.
        @return L'ic�ne � afficher dans l'onglet
     */
    QIcon QTab::icon() const
    {
        Q_D(const QTab);

        switch(style())
        {
            case Icon:
                return d->tabIcon;
            case Status:
                return page()->status();
            case Text:
                return QIcon();
            case IconAndText:
                return d->tabIcon;
            case StatusAndText:
                return page()->status();
            default:
                return QIcon();
        }
    }


    /*! Pour un tabwidget donn�, tous les onglets disponibles doivent avoir des noms distincts, pass�s au
        constructeur QTab(QString, QString, QIcon, QPage*(*)()).
        @return Le nom de l'onglet
     */
    QString QTab::name() const
    {
        Q_D(const QTab);

        return d->getNameFunction();
    }


    /*! Permet de conna�tre le texte que l'onglet doit afficher sur sa repr�sentation visuelle, dans sa
        barre d'onglets (ce texte d�pend du style).
        @return Le texte associ� � l'onglet
        @sa title(), style()
     */
    QString QTab::text() const
    {
        switch(style())
        {
            case Icon:
                return QString();
            case Status:
                return QString();
            case Text:
                return name();
            case IconAndText:
                return name();
            case StatusAndText:
                return name();
            default:
                return name();
        }
    }


    /*! Permet de conna�tre le titre de l'onglet, titre qui sera affich� sur le tabwidget, sous la barre
        d'onglets, mais aussi utilis� pour le glisser/d�poser d'onglets.
        @return Le titre de l'onglet
     */
    QString QTab::title() const
    {
        Q_D(const QTab);

        return d->getTitleFunction();
    }


    /*! Permet de conna�tre le tooltip associ� � l'onglet (affich� lorsque le pointeur de la souris est
        laiss� suffisament longtemps au-dessus de l'onglet dans sa barre).
        @return Le texte du tooltip de l'onglet
     */
    QString QTab::toolTip() const
    {
        return title();
    }


    /*! Permet de savoir si l'onglet, ou plut�t sa page associ�e, est activ� ou non.
        @return Vrai si l'onglet (et sa page) est activ�, Faux sinon
     */
    bool QTab::isEnabled() const
    {
        return page()->isEnabled();
    }


    /*! A chaque onglet, lors de son insertion dans une barre, est associ� (lorsque c'est possible) un
        raccourci-clavier (d'apr�s le contenu de text()).
        @return L'ID du raccourci clavier associ� � l'onglet (-1 s'il n'y en a pas)
        @sa setShortcutId
     */
    int QTab::shortcutId() const
    {
        Q_D(const QTab);

        return d->tabShortcutId;
    }


    /*! Permet de d�finir le raccourci-clavier � associer � l'onglet.
        @param id ID du raccourci-clavier
        @sa shortcutId
     */
    void QTab::setShortcutId(int id)
    {
        Q_D(QTab);

        d->tabShortcutId = id;
    }


//------------------------------------------------------------------------------
//                                  QTab : Slot
//------------------------------------------------------------------------------

    /*! Permet de changer le style de l'onglet (informations affich�es dans sa repr�sentation visuelle
        dans la barre).
        @param styleAction Le changement de style s'effectue via un menu; l'entr�e s�lectionn�e est
        pass�e en param�tre � cette m�thode
     */
    void QTab::setStyle(QAction* styleAction)
    {
        QVariant    data   = styleAction->data();
        bool        ok     = false;
        int         newStyle  = data.toInt(&ok);

        if (ok)
        {
            Q_D(QTab);

            d->tabStyle = Style(newStyle);
            d->tabBar->updateTabs();
        }
    }


//------------------------------------------------------------------------------
//                            QTab : Autres M�thodes
//------------------------------------------------------------------------------

    /*! Ce constructeur est destin� � cr�er les onglets � placer dans une barre, contrairement �
        QTab(QString, QString, QIcon, QPage*(*)()) qui est utilis� pour les onglets destin�s � juste
        stocker les informations n�cessaires pour les "vrais" onglets.
        @param parent Page associ�e � l'onglet � cr�er (ce sera le parent de l'onglet, au sens Qt, m�me
        si ce dernier sera affich� (indirectement) dans un autre widget)
     */
    QTab::QTab(QPage* parent)
        : QObject(parent)
        , d_custom_ptr(new QTabPrivate(this))
    { }


//------------------------------------------------------------------------------
//                                QTab : Famille
//------------------------------------------------------------------------------

    /*! @return La page associ�e � cet onglet
     */
    QPage* QTab::page() const
    {
        return qobject_cast<QPage*>(parent());
    }


    /*! Lors d'une op�ration de drag'n'drop, un onglet ne peut �tre d�pos� que sur un tabwidget
        "compatible" (i.e. qui peut construire un onglet du m�me type). Cette m�thode permet de v�rifier
        si deux onglets sont effectivement compatibles, selon cette d�finition.
        @return Vrai si l'onglet et <i>t</i> sont de "m�me type"
     */
    bool QTab::isCompatible(QTab* t) const
    {
        Q_D(const QTab);

        return d->createPageFunction || t->d_func()->createPageFunction;
    }


//------------------------------------------------------------------------------
//                     QTab : Gestion de la Barre d'Onglets
//------------------------------------------------------------------------------

    /*! D�finition de la barre dans laquelle afficher l'onglet.
        @param b Barre porteuse de l'onglet
     */
    void QTab::setBar(QBar* b)
    {
        Q_D(QTab);

        // S'il s'agit du premier placement sur une barre, adopte
        // le style par d�faut, sinon conserve celui d�j� d�fini.
        if (d->tabBar == nullptr)
        {
            d->tabStyle = b->tabWidget()->tabDefaultStyle();
        }
        d->tabBar = b;
    }


    /*! Permet de conna�tre la barre portant l'onglet.
        @return La barre porteuse de l'onglet
     */
    QBar* QTab::bar() const
    {
        Q_D(const QTab);

        return d->tabBar;
    }
}
