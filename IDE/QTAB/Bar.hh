/****************************************************************************
**
** Copyright (C) 1992-2005 Trolltech AS. All rights reserved.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY F ANY KIND, INCLUDING THE
** WARRANTY F DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QTAB_BAR_HH
#define QTAB_BAR_HH

#include "QTAB.hh"

/*! @file IDE/QTAB/Bar.hh
    @brief En-t�te de la classe QTAB::QBar.
    @author Code original : Trolltech
    <br>    Adaptation    : @ref Guillaume_Terrissol
    @date 4 Ao�t 2005 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QWidget>

#include "QT/Private.hh"

namespace QTAB
{
    /*! @brief Barre d'onglets.
        @version 1.0

        Pour la barre d'onglets, il �tait pr�vu quelques fonctionnalit�s qu'il �tait impossible de
        d�finir en juste r�impl�mentant quelques m�thodes virtuelles de QTabBar. Il a donc fallu cr�er
        compl�metement un nouveau widget (qui reprend toutefois la plus grande partie du code de QTabBar
        - d'o� le copyright).
        @note A propos des styles : le style moderne, le seul disponible initialement, permet aux barre
        d'onglets de se rapprocher de ce qui a �t� fait dans The GIMP 2. Toutefois, certains craignant
        que l'ergonomie ne d�route les futurs utilisateurs, le style classique, reprenant le comportement
        propos� par Qt, a �t� ajout� par la suite
     */
    class QBar : public QWidget
    {
        Q_OBJECT
    public:
        //! @name Constructeur & destructeur
        //@{
explicit            QBar(QDock* parent);                    //!< Constructeur [par d�faut].
virtual             ~QBar();                                //!< Destructeur.
        //@}
        //! @name Ascendants
        //@{
        QDock*      dock() const;                           //!< Dock (parent).
        QTabWidget* tabWidget() const;                      //!< TabWidget (grand-parent).
        //@}
        //! @name Gestion des onglets
        //@{
        int         addTab(QTab* tab);                      //!< Ajout d'un onglet.
        int         insertTab(QTab* tab, int index);        //!< Insertion d'un onglet.
        void        removeTab(int index);                   //!< Retrait d'un onglet.
        void        updateTabs();                           //!< Mise � jour forc�e des onglets.
        //@}
        //! @name Informations sur les onglets
        //@{
        int         indexAtPos(const QPoint &p) const;      //!< Index de l'onglet sous une position donn�e.
        QRect       tabRect(int index) const;               //!< G�om�trie d'un onglet.
        int         currentIndex() const;                   //!< Index de l'onglet actuellement s�lectionn�.
        int         count() const;                          //!< Nombre d'onglets.
        int         shape() const;                          //!< Forme des onglets.
        //@}
        //! @name Dimensions
        //@{
virtual QSize       sizeHint() const;                       //!< Suggestion de taille pour la barre.
virtual QSize       minimumSizeHint() const;                //!< Suggestion pour la taille minimale de la barre.
        //@}
        //! @name Style
        //@{
        //! @name Style de barre
        enum Style
        {
            Modern,                                         //!< "Moderne".
            Classic,                                        //!< "Classique".
            StyleCount                                      //!< Nombre de styles disponibles.
        };
        bool        isModernStyleEnabled() const;           //!< Style actuel.
        //@}

    public slots:
        //! @name Slots
        //@{
        void        setCurrentIndex(int index);             //!< Changement d'onglet actuel.
        void        setModernStyleEnabled(bool enabled);    //!< Activation du style "Modern".
        void        setClassicStyleEnabled(bool enabled);   //!< Activation du style "Classic".
        //@}

    signals:

        void        currentChanged(int index);              //!< Changement de l'onglet actuel.

    protected:
        //! @name Extension de la gestion des onglets
        //@{
virtual QSize       tabSizeHint(int index) const;           //!< Suggestion pour la taille d'un onglet.
virtual void        tabInserted(int index);                 //!< Compl�ment � l'insertion d'un onglet.
virtual void        tabRemoved(int index);                  //!< Compl�ment au retrait d'un onglet.
virtual void        tabLayoutChange();                      //!< Compl�ment � la r�organisation des onglets.
        //@}
        //! @name Gestion des �v�nements
        //@{
virtual void        changeEvent(QEvent* e);                 //!< ... de changement d'�tat.
virtual bool        event(QEvent* e);                       //!< ... quelconques.
virtual void        keyPressEvent(QKeyEvent* e);            //!< ... de pression d'une touche du clavier.
virtual void        mouseMoveEvent(QMouseEvent* e);         //!< ... de d�placement de la souris.
virtual void        mousePressEvent(QMouseEvent* e);        //!< ... de pression sur un bouton de la souris.
virtual void        mouseReleaseEvent(QMouseEvent* e);      //!< ... de rel�chement d'un bouton de la souris.
virtual void        paintEvent(QPaintEvent* e);             //!< ... de rafra�chissement.
virtual void        resizeEvent(QResizeEvent* e);           //!< ... de redimensionnement.
virtual void        showEvent(QShowEvent* e);               //!< ... de demande d'affichage.
        //@}

    private slots:

        void        scrollTabs();                           //!< D�filement des onglets.


    private:

        void        selectTabByMenu();                      //!< S�lection d'un onglet par menu.
        Q_DISABLE_COPY(QBar)
        Q_CUSTOM_DECLARE_PRIVATE(QBar)
    };
}

#endif  // QTAB_BAR_HH
