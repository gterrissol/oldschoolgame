/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Page.hh"

/*! @file IDE/QTAB/Page.cc
    @brief M�thodes (non-inline) des classes QTAB::QPage::QPagePrivate & QTAB::QPage.
    @author @ref Guillaume_Terrissol
    @date 13 Ao�t 2005 - 18 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <QEvent>
#include <QMenu>

#include "Bar.hh"
#include "Dock.hh"
#include "Tab.hh"

namespace QTAB
{
//------------------------------------------------------------------------------
//                        QPagePrivate : P-Impl de QPage
//------------------------------------------------------------------------------

    /*! @brief P-Impl de QPage.
        @version 1.0

        A l'image de ce qui a �t� fait pour QBar, cette classe fournit une grande partie de
        l'imp�mentation de QPage.
     */
    class QPage::QPagePrivate
    {
        Q_CUSTOM_DECLARE_PUBLIC(QPage)
    public:
        //! @name Constructeur
        //@{
        QPagePrivate(QPage* parent,
                     bool resizable = true,
                     bool listable  = true,
                     bool gridable  = true);    //!< Constructeur.
        //@}
        //! @name Attributs : propri�t�s
        //@{
        mutable QTab*       pageTab;            //!< Onglet associ� au widget.
        bool                resizableContents;  //!< Contenu redimensionnable ?
        bool                displayableAsList;  //!< Contenu affichable sous forme de liste ?
        bool                displayableAsGrid;  //!< Contenu affichable sous forme de grille ?
        //@}
        //! @name Attributs : �tat actuel
        //@{
        QPage::ContentsSize contentsSize;       //!< "Echelle" du contenu du widget.
        bool                displayedAsList;    //!< Contenu affich� sous forme de liste ?
        bool                displayedAsGrid;    //!< Contenu affich� sous forme de grille ?
        //@}
    };


//------------------------------------------------------------------------------
//                          QPagePrivate : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param parent    Page pour laquelle porter des informations
        @param resizable Vrai si le contenu de la page peut changer d'�chelle
        @param listable  Vrai si le contenu de la page peut �tre affich� sous forme de liste
        @param gridable  Vrai si le contenu de la page peut �tre affich� sous forme de grille
        @sa QBarPrivate::QBarPrivate(QBar* parent)
     */
    QPage::QPagePrivate::QPagePrivate(QPage* parent, bool resizable, bool listable, bool gridable)
        : q_custom_ptr(parent)
        , pageTab(nullptr)
        , resizableContents(resizable)
        , displayableAsList(listable)
        , displayableAsGrid(gridable)
        , contentsSize(QPage::Medium)
        , displayedAsList(displayableAsList)
        , displayedAsGrid(!displayableAsList && displayableAsGrid)
    { }


//------------------------------------------------------------------------------
//                      QPage : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Sont transmises � ce contructeur les seules informations indispensables.
        @param resizable Vrai si le contenu de la page peut changer d'�chelle
        @param listable  Vrai si le contenu de la page peut �tre affich� sous forme de liste
        @param gridable  Vrai si le contenu de la page peut �tre affich� sous forme de grille
     */
    QPage::QPage(bool resizable, bool listable, bool gridable)
        : QWidget(nullptr)
        , d_custom_ptr(new QPagePrivate(this, resizable, listable, gridable))
    { }


    /*! Destructeur.
     */
    QPage::~QPage() { }


//------------------------------------------------------------------------------
//                              QPage : Ascendants
//------------------------------------------------------------------------------

    /*! Permet de conna�tre le dock (grand parent) auquel appartient cette page (le parent direct �tant
        un QStackedWidget).
        @return Le dock "parent"
     */
    QDock* QPage::dock() const
    {
        QWidget* stack = qobject_cast<QWidget*>(parent());

        if (stack != nullptr)
        {
            return qobject_cast<QDock*>(stack->parent());
        }
        else
        {
            return nullptr;
        }
    }


    /*! Permet de conna�tre le tabwidget (arri�re grand-parent) auquel appartient cette page.
        @return Le tabwidget "grand-parent"
     */
    QTabWidget* QPage::tabWidget() const
    {
        QDock*  d = dock();

        if (d != nullptr)
        {
            return d->tabWidget();
        }
        else
        {
            return nullptr;
        }
    }


//------------------------------------------------------------------------------
//                              QPage : Propri�t�s
//------------------------------------------------------------------------------

    /*! @return Vrai si le contenu de la page peut �tre affich� � diff�rentes �chelles (voir
        QPage::ContentsSize). Cette propri�t� est �tablie via le constructeur
     */
    bool QPage::isContentsResizable() const
    {
        Q_D(const QPage);

        return d->resizableContents;
    }


    /*! @return Vrai si le contenu de la page peut �tre affich� sous la forme d'une liste. Cette
        propri�t� est �tablie via le constructeur
     */
    bool QPage::mayDisplayAsList() const
    {
        Q_D(const QPage);

        return d->displayableAsList;
    }


    /*! @return Vrai si le contenu de la page peut �tre affich� sous la forme d'une grille. Cette
        propri�t� est �tablie via le constructeur
     */
    bool QPage::mayDisplayAsGrid() const
    {
        Q_D(const QPage);

        return d->displayableAsGrid;
    }


    /*! @return Le menu associ� � la page (s'il existe, sinon retourne nullptr).
        @note R�impl�mentez fillPageMenu(QMenu*&) const pour remplir le menu
     */
    QMenu* QPage::menu() const
    {
        QMenu*  pageMenu = new QMenu(tr("Menu de %1").arg(tab()->title()));
        fillPageMenu(pageMenu);

        if (pageMenu != nullptr)
        {
            connect(pageMenu, SIGNAL(triggered(QAction*)), SLOT(manageMenuEntry(QAction*)));
        }

        return pageMenu;
    }


//------------------------------------------------------------------------------
//                              QPage : Etat Actuel
//------------------------------------------------------------------------------

    /*! Permet de conna�tre l'onglet associ� � cette page.
        @return L'onglet de la apge
     */
    QTab* QPage::tab() const
    {
        Q_D(const QPage);

        if (d->pageTab == nullptr)
        {
            d->pageTab = findChild<QTab*>();
            Q_ASSERT_X(d->pageTab != nullptr, "QTab* QPage::tab() const", "Qt Kernel Panic");
        }

        return d->pageTab;
    }


    /*! @return Une ic�ne repr�sentant l'�tat actuel de la page (e.g. une page de choix de couleur
        retournera une ic�ne toute enti�re de la couleur actuellement choisie)
        @sa statusIcon()
     */
    QIcon QPage::status() const
    {
        return statusIcon();
    }


    /*! @return L'�chelle actuelle � laquelle est affich�e la page
     */
    QPage::ContentsSize QPage::currentSize() const
    {
        Q_D(const QPage);

        return d->contentsSize;
    }


    /*! @return Vrai si le contenu de la page est actuellement affich� sous forme de liste
     */
    bool QPage::isDisplayingList() const
    {
        Q_D(const QPage);

        return d->displayedAsList;
    }


    /*! @return Vrai si le contenu de la page est actuellement affich� sous forme de grille
     */
    bool QPage::isDisplayingGrid() const
    {
        Q_D(const QPage);

        return d->displayedAsGrid;
    }


//------------------------------------------------------------------------------
//                                 QPage : Slots
//------------------------------------------------------------------------------

    /*! Permet de modifier l'�chelle du contenu de la page, apr�s appel � un menu.
        @param sizeAction Entr�e de menu s�lectionn�e correspondant au choix de la nouvelle �chelle
     */
    void QPage::setContentsSize(QAction* sizeAction)
    {
        QVariant    data    = sizeAction->data();
        bool        ok      = true;
        int         size    = data.toInt(&ok);

        if (ok)
        {
            Q_D(QPage);

            d->contentsSize = ContentsSize(size);
            changeContentsSize(d->contentsSize);
            adjustSize();
        }
    }


    /*! Permet d'afficher le contenu de la page sous forme de liste, ou de grille.
        @param list Vrai pour afficher le contenu de la page sous forme de liste
     */
    void QPage::displayAsList(bool list)
    {
        Q_D(QPage);

        if (list)
        {
            d->displayedAsList = true;
            d->displayedAsGrid = false;

            updateLayout();
        }
        else
        {
            d->displayedAsList = false;
        }
    }


    /*! Permet d'afficher le contenu de la page sous forme de grille, ou de liste.
        @param grid Vrai pour afficher le contenu de la page sous forme de grille
     */
    void QPage::displayAsGrid(bool grid)
    {
        Q_D(QPage);

        if (grid)
        {
            d->displayedAsGrid = true;
            d->displayedAsList = false;

            updateLayout();
        }
        else
        {
            d->displayedAsGrid = false;
        }
    }


    /*! Rafra�chit l'onglet associ� suite � un changement de statut (pour r�cup�rer la nouvelle ic�ne).
     */
    void QPage::statusChanged()
    {
        dock()->bar()->updateTabs();
    }


//------------------------------------------------------------------------------
//                         QPage : Changement de Langue
//------------------------------------------------------------------------------

    /*! Traitement d'un �v�nement de changement d'�tat.
        @param e Ev�nement � traiter
     */
    void QPage::changeEvent(QEvent* e)
    {
        if      (e->type() == QEvent::LanguageChange)
        {
            onLanguageChange();
            e->accept();
        }
        else if (e->type() == QEvent::EnabledChange)
        {
            // L'onglet est dessin� par sa barre, c'est donc � elle qu'il faut demander de se rafra�chir.
            dock()->bar()->repaint();
        }

        QWidget::changeEvent(e);
    }


    /*! Cette m�thode est appel�e lorsque l'application a effectu� un changement de langue. La version
        par d�faut ne fait rien, mais ses r�impl�mentations (qui doivent toujours appeler les versions
        des classes parentes) doivent �tre l'unique endroit o� �tablir les textes � afficher dans les
        interfaces graphiques (sauf ceux, cas plus rares, qui sont plac�s dans des widgets cr��s � la
        demande).
        @sa @ref QT_Translation_Page
     */
    void QPage::onLanguageChange() { }


//------------------------------------------------------------------------------
//                     QPage : Comportement � R�impl�menter
//------------------------------------------------------------------------------

    /*! Traite l'entr�e s�lectionn�e du menu retourn� par menu()
        @param entry Entr�e du menu � traiter
        @note Par d�faut, n'effectue aucun traitement
     */
    void QPage::manageMenuEntry(QAction*)
    {
        // Par d�faut, ne fait rien.
    }


    /*! Remplit un menu avec des actions que cette page peut effectuer.
        @note Par d�faut, pas d'entr�e dans le menu
     */
    void QPage::fillPageMenu(QMenu*& pageMenu) const
    {
        // Par d�faut, pas d'entr�e dans le menu.
        delete pageMenu;
        pageMenu = nullptr;
    }


    /*! @return Par d�faut, une ic�ne vide. Toute r�impl�mentation devrait renvoyer une ic�ne pertinente
        (voir QPage::status())
     */
    QIcon QPage::statusIcon() const
    {
        return QIcon();
    }


    /*! Met � jour le contenu de la page suite � un changement d'�chelle d'icelui.
        @param sz Nouvelle �chelle
        @note Par d�faut, ne fait rien
     */
    void QPage::changeContentsSize(ContentsSize)
    {
        // Par d�faut, ne fait rien.
    }


    /*! R�organise le contenu de la page, suite � un changement d'organisation (de liste en grille, ou vice-versa).
        @note Par d�faut, ne fait rien
     */
    void QPage::updateLayout()
    {
        // Par d�faut, ne fait rien.
    }
}
