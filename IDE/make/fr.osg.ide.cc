/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file IDE/make/fr.osg.ide.cc
    @brief Programme principal (avec interface graphique).
    @author @ref Guillaume_Terrissol
    @date 1er Avril 2002 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CORE/SyncService.hh"
#include "CORE/UIService.hh"
#include "ERRor/Exception.hh"
#include "PROGram/Hardware.hh"

#include <memory>
#include <iostream>
#include <typeinfo>

#include <QSplashScreen>
#include <QApplication>

#include <OSGi/Activator.hh>
#include <OSGi/ApplicationService.hh>
#include <OSGi/Context.hh>

namespace
{
    class QtArgs
    {
    public:
        QtArgs(const OSGi::ApplicationService::Args& pArgs)
            : mData()
            , mArgs()
            , mCount(pArgs.size() + 1)
        {
            mData.reserve(mCount);
            mArgs.reserve(mCount);
            for(auto lArg : pArgs)
            {
                add(lArg);
            }
            add("-platformpluginpath cache");
        }
        int&    count()
        {
            return mCount;
        }
        char**  values()
        {
            return &mArgs[0];
        }

    private:

        void add(const std::string& pStr)
        {
            mData.emplace_back(new char[pStr.length() + 1]);
            strcpy(mData.back().get(), pStr.c_str());
            mArgs.push_back(mData.back().get());
        }
        std::vector<std::unique_ptr<char[]>>    mData;
        std::vector<char*>                      mArgs;
        int                                     mCount;
    };
}

    class IDEService : public OSGi::ApplicationService
    {
    public:

        typedef std::shared_ptr<IDEService> Ptr;

        IDEService()
            : OSGi::ApplicationService()
        { }
        virtual ~IDEService() { }

    private:
        /*! Fonction principale du programme <b>IDE</b>, lanc�e par main().
            @param pArgC Nombre d'arguments pass�s sur la ligne de commande
            @param pArgV Liste des arguments pass�s sur la ligne de commande
            @return 0 si l'application s'est correctement termin�e, un code d'erreur sinon
         */
virtual int process(const Args& pArgs, OSGi::Context* pContext) override final
        {
            try
            {
#   if   defined(FRANCAIS)
                SET_ERR_MSG_LANGUAGE(eFr)
#   elif defined(ENGLISH)
                SET_ERR_MSG_LANGUAGE(eEn)
#   endif
                QtArgs          lArgs{pArgs};
                QApplication    lApplication{lArgs.count(), lArgs.values()};
                lApplication.setOverrideCursor(Qt::WaitCursor);

                // Affichage du splash screen.
                QSplashScreen  lSplash(QPixmap(":/ide/core/images/splash"), Qt::WindowStaysOnTopHint);
                lSplash.show();
                lApplication.processEvents();

                // Cr�ation des composants.
                auto    lUIService = pContext->services()->findByTypeAndName<CORE::UIService>("fr.osg.ide.ui");
                if (lUIService)
                {
                    auto            lOrderedTabs    = pContext->properties(BUNDLE_NAME)->get("tabs", "");
                    auto            lMainWindow     = lUIService->buildMainWindow(QString::fromStdString(lOrderedTabs).split(","));

                    lMainWindow->show();
                    lSplash.finish(lMainWindow.get());
                    lApplication.restoreOverrideCursor();

                    // C'est parti.
                    return lApplication.exec();
                }
                else
                {
                    throw ERR::Exception("Invalid UI service");
                }
            }
            catch(PROG::HaltException&)
            {
                std::cerr << "Exits program because an assertion failed" << std::endl;

                return 1;
            }
            catch(ERR::Exception& pE)
            {
                std::cerr << "Caught exception  : " << pE.what() << std::endl;

                return 2;
            }
            catch(std::exception& pE)
            {
                std::cerr << "Standard exception (" << typeid(pE).name() << ") : " << pE.what() << std::endl;

                return 3;
            }
            catch(...)
            {
                std::cerr << "Unknown exception." << std::endl;

                throw;
            }
        }
    };

namespace IDE
{
    class Activator : public OSGi::Activator
    {
    public:
                Activator() : OSGi::Activator() { }
virtual         ~Activator() { }
    private:
virtual void    doStart(OSGi::Context* pContext) override final
                {
                    IDEService::Ptr   lApp{new IDEService()};
                    pContext->services()->checkIn(BUNDLE_NAME, lApp);
                }
virtual void    doStop(OSGi::Context* pContext) override final
                {
                    pContext->services()->checkOut(BUNDLE_NAME);
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(IDE::Activator)
OSGI_END_REGISTER_ACTIVATORS()
