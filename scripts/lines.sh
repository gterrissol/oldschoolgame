#! /bin/bash

SCRIPT_DIR=`dirname $0`

source "$SCRIPT_DIR/utils.sh"

wc -l `files` | tail -1 | tr -c -d [:digit:]
