#! /bin/bash 

SUFFIXES="inl hh cc tcc"

files () {
  opt=
  for suf in $SUFFIXES
  do
    if [ ! -z "$opt" ] 
    then
      opt="$opt -o"
    fi
    opt="$opt -name \"*.$suf\""
  done 

  eval find . $opt | grep -v "release/\|debug/"
}
