#! /bin/bash

SCRIPT_DIR=`dirname $0`

source "$SCRIPT_DIR/utils.sh"

files | wc -l | tr -c -d [:digit:]
