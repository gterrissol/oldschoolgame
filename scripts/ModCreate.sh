#!/bin/bash

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License,or (at
#  your option) any later version.
#  For more details, see the GNU General Public License (www.fsf.org or
#  the COPYING file somewhere in the package)


# ! @file scripts/ModCreate.sh
#   @brief Utilitaire de cr�ation de module automatis�
#   @author @ref Jean_Marc_Challier (addendum : @ref Guillaume_Terrissol)
#   @date 1er D�cembre 2002 - 28 Novembre 2008
#   @version 1.2.1
#   @note Ce fichier est diffus� sous licence GPL.
#   Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.


SHORT_NAME=
LONG_NAME=
UPPER_LONG_NAME=
DIR=
AUTHOR=
CLASSES=
CLASS=
UPPER_CLASS=
FORWARDS=
MONTH=`date +%B`
DATE=`date +%d`" "`echo ${MONTH:0:1} | tr a-z A-Z`${MONTH:1}" "`date +%Y`
SRC_DIR=scripts/etc

function display_menu() {
  clear
  echo "                         OSG Module Generation Utility"
  echo "                         ============================="
  echo ""
  echo ""
  echo "Created by the OSG team. Please read the LICENCE file"
  echo ""
  echo ""
  echo ""
  echo "  1 - Module short name : $SHORT_NAME"
  echo "  2 - Module long name : $LONG_NAME"
  echo "  3 - Author name : $AUTHOR"
  echo "  4 - Module location : $DIR"
  echo "  5 - Other module classes : $CLASSES"
  echo "  6 - Generate (empty) module"
  echo "  7 - Destroy module (Use carfully !)"
  echo "  8 - Quit"
  echo ""
  echo -n "       Enter your choice : "
}

function translate() {
  echo -n "Building file '$2'..."
  cat "$SRC_DIR/$1" | sed -e "s/%SHORT_NAME%/$SHORT_NAME/g"             \
                          -e "s/%LONG_NAME%/$LONG_NAME/g"               \
                          -e "s/%UPPER_LONG_NAME%/$UPPER_LONG_NAME/g"   \
                          -e "s/%CC_FILES%/$CC_FILES/g"                 \
                          -e "s/%HH_FILES%/$HH_FILES/g"                 \
                          -e "s/%INL_FILES%/$INL_FILES/g"               \
                          -e "s/%DATE%/$DATE/g"                         \
                          -e "s/%AUTHOR%/$AUTHOR/g"                     \
                          -e "s/%CLASS%/$CLASS/g"                       \
                          -e "s/%UPPER_CLASS%/$UPPER_CLASS/g"           \
                          -e "s/%FORWARDS%/$FORWARDS/g"                 \
                          -e "s/%INCLUDES%/$INCLUDES/g"                 \
                          -e "s/%DIR%/$DIR/g"                           > "$2"
  echo "done."
}

while true
do
  display_menu
  read ans
  case $ans in
    1)
      ok=0
      while [ $ok -ne 1 ]
      do 
        echo -n "Please enter module short name (2-4 upcase letters, 3 recommended) [$SHORT_NAME] : "
        read sn
        sn=`echo $sn | tr [:lower:] [:upper:]`
        if [ -z $sn ]
	then
          sn=$SHORT_NAME
        fi
        sz=`echo $sn | wc -c`
        if [ \( $sz -ge 3 \) -a \( $sz -le 5 \) ]
        then
	  SHORT_NAME=$sn
          ok=1
          echo "Module short name validated."
          break
        fi
      done
    ;;
    2)
      echo -n "Please enter module long name [$LONG_NAME] : "
      read ln
      if [ -z "$ln" ]
      then
        ln="$LONG_NAME"
      fi
      LONG_NAME="$ln"
      UPPER_LONG_NAME=`echo $LONG_NAME | tr [:lower:] [:upper:]`
      echo "Module long name validated."
    ;;
    3)
      echo -n "Enter the author's name [$AUTHOR] : "
      read auth
      if [ -z "$auth" ]
      then
	auth="$AUTHOR"
      fi
      AUTHOR="$auth"
      echo "Author name validated."
    ;;
    4) 
      echo -n "Enter the location of the module [$DIR] : "
      read md
      if [ -z "$md" ]
      then 
        md="$DIR"
      fi
      while [ ! -d "$md" ]
      do
        echo -n "Enter the name of an *existing* directory to put the module into [$DIR] : "
        read md
        if [ -z "$md" ]
        then 
          md="$DIR"
        fi
      done
      DIR="$md"
      echo "Module location validated."
    ;;
    5)
      echo ""
      echo "  a. Enter list of classes (space separated)"
      echo "  b. Add a class to the list"
      echo "  c. Remove a class from the list"
      echo -n "     => your choice : "
      read l
      ok=1
      case $l in 
        a)
          echo -n "Enter new class list [$CLASSES] : "
          read cl
          if [ -z "$cl" ]
          then
            cl="$CLASSES"
          fi
          CLASSES="$cl"
          ;;
        b)
          echo -n "Enter the class to add to the list : "
          read nc
          CLASSES="$CLASSES $nc"
          ;;
        c)
          echo -n "Enter the class to remove from the list : "
          read oc
          CLASSES=`echo "$CLASSES" | sed -e "s/\<$oc\>//"`
          ;;
        *)
          ok=0
        ;;
      esac
      CLASSES=`echo "$CLASSES" | tr " " "\n" | grep -v ^$ | sort | uniq | tr "\n" " " | sed -e 's/ $//'`
      if [ $ok -ne 0 ]
      then
        echo "Class list updated."
      else
	echo "Unknown choice - No action taken."
      fi
    ;;
    6)
      echo -n "Creating module directory... "
      mkdir "$DIR/$LONG_NAME"
      echo "done."
      echo -n "Compiling list of source files..."
      CC_FILES=""
      HH_FILES=""
      INL_FILES=""
      FORWARD=""
      INCLUDES=""
      for src in $CLASSES
      do
        CC_FILES="$CC_FILES $src.cc"
        HH_FILES="$HH_FILES $src.hh"
        INL_FILES="$INL_FILES $src.inl"
        FORWARDS="$FORWARDS    class O$src;\n"
        INCLUDES="$INCLUDES\#include \"$src.hh\"\n"
      done
      echo "done."
      for f in Makefile.am Enum.hh ErrMsg.hh
      do
        translate "$f" "$DIR/$LONG_NAME/$f"
      done
      for CLASS in $CLASSES
      do
        UPPER_CLASS=`echo $CLASS | tr [:lower:] [:upper:]`
        translate Class.hh  "$DIR/$LONG_NAME/$CLASS.hh"
        translate Class.inl "$DIR/$LONG_NAME/$CLASS.inl"
        translate Class.cc  "$DIR/$LONG_NAME/$CLASS.cc"
      done
      translate "Module.hh" "$DIR/$LONG_NAME/$LONG_NAME.hh"
      translate "Module.cc" "$DIR/$LONG_NAME/$LONG_NAME.cc"
      translate "All.hh"    "$DIR/$LONG_NAME/All.hh"
      echo "Module creation terminated"
    ;;
    7)
      echo -n "Are you *SURE* you want to destroy the '$DIR/$LONG_NAME' module (Y/N) ? "
      read conf
      if [ "$conf" = "Y" ]
      then 
        echo -n "Destroying module..."
        rm -rf "$DIR/$LONG_NAME"
        echo "done."
      else
        echo -n "Aborted..."
      fi
    ;;
    8)
      exit
    ;;
    *)
      echo -n "Unknown choice !"
    ;;
    esac
  sleep 2
done
