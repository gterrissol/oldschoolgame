#! /bin/bash

# please insert GPL blah blah

usage() {
  echo "Usage : $0 <suffixe originel> <suffixe final>"
}

if [ $# -ne 2 ]
then
  usage
fi

src=$1
dst=$2

find . -name "*.$src" -print | while read fname
do
  mv -i "$fname" `echo $fname | sed -e "s/.$src$/.$dst/"`
done
