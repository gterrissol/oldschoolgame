#! /bin/bash

SCRIPT_DIR=`dirname $0`

source "$SCRIPT_DIR/utils.sh"

if [ $# -lt 1 ]
then
    echo "Usage : $0 <pattern>"
else
    files | xargs grep -n --color "$*"
fi
