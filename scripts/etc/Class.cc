/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "%CLASS%.hh"

/*! @file %DIR%/%LONG_NAME%/%CLASS%.cc
    @brief M�thodes (non-inline) de la classe %SHORT_NAME%::%CLASS%.
    @author @ref %AUTHOR%
    @date %DATE% - %DATE%
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <...>

#include ".../..."

#include "..."

namespace %SHORT_NAME%
{
//------------------------------------------------------------------------------
//                     %CLASS% : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    %CLASS%::%CLASS%()
    { 
        <...>
    }



    /*! Destructeur : <Explication des actions effectuees>
     */
    %CLASS%::~%CLASS%()
    {
         <...>
    }


//------------------------------------------------------------------------------
//                              %CLASS% : M�thodes
//------------------------------------------------------------------------------

    /*! <Role de la methode>
        @param <Descriptif des parametres>
        @return <Descriptif de la valeur de retour>
        @note <Notes eventuelles>
     */
    <ReturnType> %CLASS%::Methode(<Param�tres>)
    {
        <...>
    }

//------------------------------------------------------------------------------
//                         %CLASS% : Attributs de Classe
//------------------------------------------------------------------------------

    <Type> %CLASS%::<Attribut> [= <Initialisation>];

}
