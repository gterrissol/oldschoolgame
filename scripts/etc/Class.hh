/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef %SHORT_NAME%_%UPPER_CLASS%_HH
#define %SHORT_NAME%_%UPPER_CLASS%_HH

#include "%LONG_NAME%.hh"

/*! @file %DIR%/%LONG_NAME%/%CLASS%.hh
    @brief En-t�te de la classe %SHORT_NAME%::%CLASS%.
    @author @ref %AUTHOR%
    @date %DATE% - %DATE%
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <...>

#include ".../..."

#include "..."

namespace %SHORT_NAME%
{
    /*! @brief <Descriptif de la classe>
        @version 0.2

        <Descriptif d�taill� de la classe>
     */
    class %CLASS%
    {
    public :
        //! @name Constructeur & destructeur
        //@{
        %CLASS%();   //!< Constructeur par d�faut.
virtual ~%CLASS%();  //!< Destructeur.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "%CLASS%.inl"

#endif  // De %SHORT_NAME%_%UPPER_CLASS%_HH
