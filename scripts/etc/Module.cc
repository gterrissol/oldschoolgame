    /*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "%LONG_NAME%.hh"

/*! @file %DIR%/%LONG_NAME%/%LONG_NAME%.cc
    @brief D�finitions diverses du module @ref %LONG_NAME%.
    @author @ref %AUTHOR%
    @date %DATE% - %DATE%
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <...>

#include ".../..."

#include "..."

namespace %SHORT_NAME%
{
}
