/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef %SHORT_NAME%_ALL_HH
#define %SHORT_NAME%_ALL_HH

/*! @file %DIR%/%LONG_NAME%/All.hh
    @brief Interface publique du module @ref %LONG_NAME%.
    @author @ref %AUTHOR%
    @date %DATE% - %DATE%
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace %SHORT_NAME%   //! <Descriptif du module>
{
    /*! @namespace %SHORT_NAME%
        @version 0.2

        <Descriptif d�taill� du module>
     */

    /*! @defgroup %LONG_NAME% %LONG_NAME% : <Descriptif>
        <b>namespace</b> %SHORT_NAME% et <...>.
     */

}

%INCLUDES%
#endif  // De %SHORT_NAME%_ALL_HH
