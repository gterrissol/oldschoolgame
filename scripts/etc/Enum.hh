/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef %SHORT_NAME%_ENUM_HH
#define %SHORT_NAME%_ENUM_HH

/*! @file %DIR%/%LONG_NAME%/Enum.hh
    @brief Enumérations et constantes du module %SHORT_NAME%.
    @author @ref %AUTHOR%
    @date %DATE% - %DATE%
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh" < Au besoin (pour des constantes) >

namespace %SHORT_NAME%
{
    enum EEnum
    {
        eE //!< Valeur d'énumération.
    };
}

#endif // De %SHORT_NAME%_ENUM_HH
