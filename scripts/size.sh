#! /bin/bash

SCRIPT_DIR=`dirname $0`

source "$SCRIPT_DIR/utils.sh"

wc -c `files` | tail -1 | tr -c -d [:digit:]
