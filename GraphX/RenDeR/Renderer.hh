/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_RENDERER_HH
#define RDR_RENDERER_HH

#include "RenDeR.hh"

/*! @file GraphX/RenDeR/Renderer.hh
    @brief En-t�te de classe RDR::Renderer.
    @author @ref Guillaume_Terrissol
    @date 12 F�vrier 2003 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "CAMera/CAMera.hh"
#include "MATH/MATH.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/SharedPtr.hh"

#include "Enum.hh"

namespace RDR
{
    /*! @brief Afficheur.
        @version 0.4

        R�impl�mentez dumpAppearances() pour ins�rer dans l'arbre de rendu (gr�ce � pushAppearance())
        toutes les apparences � rendre.
     */
    class Renderer : public MEM::OnHeap
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef WeakPtr<APP::Appearance const>  ConstAppearancePtr;     //!< Pointeur (constant) sur apparence.
        typedef WeakPtr<CAM::Camera const>      ConstCameraPtr;         //!< Pointeur (constant) sur cam�ra.
        typedef SharedPtr<Tree>                 TreePtr;                //!< Pointeur sur arbre de rendu.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        Renderer();                                     //!< Constructeur par d�faut.
virtual                 ~Renderer();                                    //!< Destructeur.
        //@}
        //! @name Rendu
        //@{
        void            dump(TreePtr pDumpTree);                        //!< D�ploiment d'un arbre de rendu.
        void            attach(ConstCameraPtr pCamera);                 //!< Attachement d'une cam�ra.
        void            enable(bool pEnabled);                          //!< [D�s]Activation du renderer.
        bool            isEnabled() const;                              //!< Renderer activ� ?
        ELayers         layer() const;                                  //!< Niveau de superposition.

    protected:

        ConstCameraPtr  camera() const;                                 //!< Cam�ra associ�e � l'instance.
        //@}
        void            pushAppearance(ConstAppearancePtr pAppearance,
                                       const MATH::M4&    pTransform);  //!< Recensement d'une apparence � afficher.

    private:

virtual void            dumpAppearances() = 0;                          //!< Collecte des apparences � afficher. 
virtual ELayers         getLayer() const;                               //!< Niveau de superposition.
        PIMPL()
    };


//------------------------------------------------------------------------------
//                              Op�rateur d'Egalit�
//------------------------------------------------------------------------------

    bool operator==(const WeakPtr<Renderer>& pL, const WeakPtr<Renderer>& pR);  //!< Egalit� de weak pointers de renderer.
}

#endif  // De RDR_RENDERER_HH
