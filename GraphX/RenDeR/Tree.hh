/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_TREE_HH
#define RDR_TREE_HH

#include "RenDeR.hh"

/*! @file GraphX/RenDeR/Tree.hh
    @brief En-t�te des classes RDR::Tree et d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2002 - 30 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/APPearance.hh"
#include "CAMera/CAMera.hh"
#include "CoLoR/CoLoR.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/STL.hh"
#include "UTIlity/Rect.hh"

#include "Enum.hh"
#include "Node.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                                     Tree
//------------------------------------------------------------------------------

    /*! @brief Arbre de rendu.
        @version 0.5

     */
    class Tree : public MEM::OnHeap
    {
    public:
        //! @name Types de pointeur
        //@{
        using ConstAppearancePtr = WeakPtr<APP::Appearance const>;          //!< Pointeur (constant) sur apparence.
        using ConstCameraPtr     = WeakPtr<CAM::Camera const>;              //!< Pointeur (constant) sur cam�ra.
        using ConstNodePtr       = WeakPtr<Node const>;                     //!< Pointeur (constant) sur noeud de rendu.
        using NodePtr            = WeakPtr<Node>;                           //!< Pointeur sur noeud de rendu.
        using RootNodePtr        = SharedPtr<RootNode>;                     //!< Pointeur sur noeud racine.
        using ShadersPtr         = SHD::Library::Ptr;                       //!< Pointeur sur biblioth�que de shaders.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                                Tree();                                     //!< Constructeur par d�faut.
                                Tree(ELayers pLayer);                       //!< Constructeur.
                                Tree(RootNodePtr pRoot);                    //!< Constructeur.
virtual                         ~Tree();                                    //!< Destructeur.
        //@}
        //! @name Interface
        //@{
        inline  void            insert(ConstAppearancePtr pAppearance,
                                       const MATH::M4&    pTransform);      //!< Insertion d'une apparence.
        inline  EModes          mode() const;                               //!< Type de rendu offert par l'arbre.
        inline  void            reset();                                    //!< R�initialisation de l'arbre pour pr�parer le rendu.
                void            setCamera(ConstCameraPtr pCamera);          //!< Lien avec la cam�ra "filmant" la sc�ne � rendre.
                ConstCameraPtr  camera() const;                             //!< Cam�ra pour le rendu.
                void            bind(ShadersPtr pLibrary);                  //!< D�finition des shaders pour le rendu.
                ShadersPtr      library();                                  //!< Shaders pour le rendu.
                void            flush();                                    //!< Rendu des apparences enregistr�es.
        //@}
        //! @name Parcours des noeuds
        //@{
                NodeIterator    begin() const;                              //!< It�rateur : r�f�rence le premier noeud.
                NodeIterator    end() const;                                //!< It�rateur : r�f�rence le dernier noeud + 1.
        //@}

    protected:
        //! @name Noeud racine
        //@{
                void            setRootNode(RootNodePtr pRoot);             //!< D�finition du noeud racine.
                ConstNodePtr    rootNode() const;                           //!< R�cup�ration du noeud racine.
                NodePtr         rootNode();                                 //!< R�cup�ration du noeud racine.
        //@}
        //! @name Gestion du rendu
        //@{
virtual         void            preFlush();                                 //!< Travail pr�-rendu.
virtual         void            postFlush();                                //!< Travail post-rendu.
        //@}
        //! @name Gestion interne des noeuds
        //@{
virtual void                    cleanTree();                                //!< R�initialisation de l'arbre.

        template<class TTree>
        class NodeChecker;
        template<class TNode>
        class NodeList;


    private:

                template<class TN>
                SharedPtr<TN>   insertNode(SharedPtr<TN> pNode,
                                           NodePtr       pParent);          //!< Insertion d'un noeud.
        //@}
        //! @name Autres m�thodes
        //@{
virtual         void            insertApp(ConstAppearancePtr pAppearance,
                                          const MATH::M4&    pTransform);   //!< Insertion d'une apparence.
virtual         EModes          getMode() const;                            //!< Type de rendu propos� par l'arbre.
        //@}
        FORBID_COPY(Tree)
        PIMPL()
    };


//------------------------------------------------------------------------------
//                            Autres Arbres de Rendu
//------------------------------------------------------------------------------

    /*! @brief Arbre de rendu pour le picking.
        @version 0.4
     */
    class PickingTree : public Tree
    {
        //! @name Classe amie
        //@{
        template<class TTree>
 friend class Tree::NodeChecker;                                        //!< Pour l'acc�s � pt_InsertNode.
        //@}
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<PickingNode>  PickingNodePtr;                 //!< Pointeur sur noeud de picking.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            PickingTree(SHD::Program::Id pPicking);     //!< Constructeur par d�faut.
                            PickingTree(ELayers          pLayer,
                                        SHD::Program::Id pPicking);     //!< Constructeur.
virtual                     ~PickingTree();                             //!< Destructeur.
        //@}
        APP::Pickable::Ptr  picked(const UTI::Point& pPosition) const;  //!< Donn�e "pick�e".

    protected:
        //! @name Gestion du rendu
        //@{
virtual void                preFlush();                                 //!< Travail pr�-rendu.
virtual void                postFlush();                                //!< Travail post-rendu.
        //@}
        //! @name Gestion interne des noeuds
        //@{
virtual void                cleanTree();                                //!< R�initialisation de l'arbre.


    private:

        PickingNodePtr      insertNode(PickingNodePtr pNode,
                                       NodePtr        pParent);         //!< Insertion d'un noeud.
        //@}
        //! @name Autres m�thodes
        //@{
virtual void                insertApp(ConstAppearancePtr pAppearance,
                                      const MATH::M4&    pTransform);   //!< Insertion d'une apparence.
virtual EModes              getMode() const;                            //!< Type de rendu propos� par l'arbre.
        //@}
        FORBID_COPY(PickingTree)
        PIMPL()
    };


    /*! @brief Arbre de rendu pour la surbrillance.
        @version 0.4
     */
    class HighlightTree : public Tree
    {
        //! @name Classe amie
        //@{
        template<class TTree>
 friend class Tree::NodeChecker;                                        //!< Pour l'acc�s � pt_InsertNode.
        //@}
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<HighlightNode>    HighlightNodePtr;           //!< Pointeur sur noeud de surbrillance.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            HighlightTree(const CLR::RGB&  pColor,
                                          SHD::Program::Id pHighlight); //!< Constructeur.
virtual                     ~HighlightTree();                           //!< Destructeur.
        //@}

    protected:
        //! @name Gestion interne des noeuds
        //@{
virtual void                cleanTree();                                //!< R�initialisation de l'arbre.


    private:

        HighlightNodePtr    insertNode(HighlightNodePtr pNode,
                                       NodePtr          pParent);       //!< Insertion d'un noeud.
        //@}
                            HighlightTree();                            //!< Constructeur par d�faut (d�sactiv�).
        //! @name Autres m�thodes
        //@{
virtual void                insertApp(ConstAppearancePtr pAppearance,
                                      const MATH::M4&    pTransform);   //!< Insertion d'une apparence.
virtual EModes              getMode() const;                            //!< Type de rendu propos� par l'arbre.
        //@}
        FORBID_COPY(HighlightTree)
        PIMPL()
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Tree.inl"

#endif  // De RDR_TREE_HH
