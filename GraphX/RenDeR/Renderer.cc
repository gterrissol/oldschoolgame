/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Renderer.hh"

/*! @file GraphX/RenDeR/Renderer.cc
    @brief M�thodes (non-inline) de la classe RDR::Renderer.
    @author @ref Guillaume_Terrissol
    @date 22 Juillet 2003 - 20 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>

#include "APPearance/BaseAppearance.hh"
#include "CAMera/BaseCamera.hh"
#include "STL/SharedPtr.hh"

#include "Tree.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RDR::Renderer.
        @version 1.0

        Comme pour nombre de classes lors de la refonte, tous les attributs ainsi que les m�thodes
        priv�es ont �t� d�plac�es dans P-Impl.
     */
    class Renderer::Private : public MEM::OnHeap
    {
    public:

        Private();                                             //!< Constructeur par d�faut.
        //! @name Attributs
        //@{
        ConstCameraPtr                         mCamera;        //!< Cam�ra.
        WeakPtr<TreePtr::element_type>   mDumpTree;      //!< Arbre portant les apparences � rendre.
        bool                                    mIsEnabled;     //!< Rendu activ� ?
    };



//------------------------------------------------------------------------------
//                             P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Renderer::Private::Private()
        : mCamera()
        , mDumpTree()
        , mIsEnabled(true)
    { }


//------------------------------------------------------------------------------
//                     Renderer : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Renderer::Renderer()
        : pthis()
    { }


    /*! Destructeur.
     */
    Renderer::~Renderer() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Remplit un arbre de rendu avec les apparences port�es par le renderer. Ainsi, on peut obtenir un
        rendu diff�rent � partir des m�mes informations.
        @param pDumpTree Arbre � remplir avec les apparences de l'objet associ� � cette instance
        @warning <i>pDumpTree</i> n'est pas r�initialis�, afin de permettre la "construction" d'un arbre
        de rendu � partir de plusieurs renderers
     */
    void Renderer::dump(TreePtr pDumpTree)
    {
        if (!isEnabled())
        {
            return;
        }

        pthis->mDumpTree = pDumpTree;

        dumpAppearances();
    }


//------------------------------------------------------------------------------
//                               Renderer : Rendu
//------------------------------------------------------------------------------

    /*!
     */
    void Renderer::attach(ConstCameraPtr pCamera)
    {
        pthis->mCamera = pCamera;
    }


    /*!
     */
    void Renderer::enable(bool pEnabled)
    {
        pthis->mIsEnabled = pEnabled;
    }


    /*!
     */
    bool Renderer::isEnabled() const
    {
        return pthis->mIsEnabled;
    }


    /*! Niveau de superposition.
     */
    ELayers Renderer::layer() const
    {
        return getLayer();
    }


    /*! Un renderer n'a pas � modifier sa cam�ra : ceux qui y acc�dent ne peuvent donc pas non plus la modifier.
     */
    Renderer::ConstCameraPtr Renderer::camera() const
    {
        return pthis->mCamera;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Enregistre une apparence pour le prochain rendu. Les r�impl�mentation de dumpAppearances() sont
        charg�es d'appeller cette m�thode.
        @param pAppearance Apparence � rendre
        @param pTransform  Matrice de transformation pour le rendu de <i>pAppearance</i>
        @note Rien n'est fait si <i>pAppearance</i> n'est pas valide
     */
    void Renderer::pushAppearance(ConstAppearancePtr pAppearance, const MATH::M4& pTransform)
    {
        if (!pAppearance.expired())
        {
            if (auto lTree = pthis->mDumpTree.lock())
            {
                lTree->insert(pAppearance, pTransform);
            }
        }
    }


    /*! @fn Renderer::dumpAppearances()
        Cette m�thode doit �tre r�impl�ment�e afin de collecter (via pushAppearance()) les apparences �
        envoyer au rendu.
     */


    /*! Niveau de superposition.
     */
    ELayers Renderer::getLayer() const
    {
        return eMainLayer;
    }


//------------------------------------------------------------------------------
//                              Op�rateur d'Egalit�
//------------------------------------------------------------------------------

    /*! Compare deux weak pointers.
        @param pL Premier pointeur � comparer
        @param pR Second pointeur � comparer
        @return VRAI si <i>pL</i> et <i>pR</i> sont �quivalents (i.e. partagent un m�me pointeur
        r�f�rence ou sont tous deux vides)
        @note Koenig lookup : l'o�rateur devait �tre d�fini dans l'espace de noms de d�finitions des
        pointeurs...
     */
    bool operator==(const WeakPtr<Renderer>& pL, const WeakPtr<Renderer>& pR)
    {
        return !std::owner_less<WeakPtr<Renderer>>()(pL, pR) && !std::owner_less<WeakPtr<Renderer>>()(pR, pL);
    }
}
