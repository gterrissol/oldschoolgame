/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Node.hh"

/*! @file GraphX/RenDeR/Node.cc
    @brief M�thodes (non-inline) des classes RDR::Node et d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2002 - 7 F�vrier 2018
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>

#include "CoLoR/RGB.hh"
#include "ERRor/Assert.hh"
#include "GEOmetry/Fields.hh"
#include "GEOmetry/List.hh"
#include "MATerial/Texture.hh"
#include "MATH/Const.hh"
#include "SHaDers/Const.hh"

#include "Enum.hh"
#include "ErrMsg.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                                     Node
//------------------------------------------------------------------------------

    /*! Constructeur.<br>
        Les classes d�riv�es de RDR::Node sont les noeuds de l'arbre de rendu. Cette classe de
        base g�re les liaisons entre noeuds.<br>
        Les classe d�riv�es impl�mentent les changement d'�tats de rendu.
        @param pParent Noeud p�re
     */
    Node::Node(ConstNodePtr pParent)
        : mChildren{}
        , mParentNode{pParent}
        , mNextNode{}
        , mShaders{}
    { }


    /*! Destructeur.
     */
    Node::~Node() { }


    /*! Acc�s au noeud p�re.
        @return Le noeud p�re du noeud instance
     */
    Node::ConstNodePtr Node::parent() const
    {
        return mParentNode;
    }


    /*! Noeud suivant (fr�re).
        @return Le noeud suivant dans l'arbre, s'il y en a un au m�me niveau, de m�me p�re, nullptr sinon
     */
    Node::ConstNodePtr Node::next() const
    {
        return mNextNode;
    }


    /*! Acc�s aux fils d'un noeud.
        @param pN Identifie le fils du noeud instance � r�cup�rer
        @return Le <i>pN</i>-i�me noeud fils de l'instance
     */
    Node::ConstChildNodePtr Node::child(U32 pN) const
    {
        ASSERT_EX(pN < childCount(), kRangeCheckError, return nullptr;)

        return mChildren[pN];
    }


    /*! Nombre de noeuds fils.
        @return Le nombre de noeuds fils de l'instance
     */
    U32 Node::childCount() const
    {
        return U32(mChildren.size());
    }


    /*! Ins�re un noeud fils.
        @param pChild Nouveau noeud fils de l'instance
     */
    void Node::insertChild(ConstChildNodePtr pChild)
    {
        if (0 < childCount())
        {
            std::const_pointer_cast<ChildNodePtr::element_type>(mChildren.back())->mNextNode = pChild;
        }
        mChildren.push_back(pChild);
    }


    /*! Supprime toutes les r�f�rences vers les noeuds fils.
     */
    void Node::clear()
    {
        mChildren.clear();
    }


    /*!
     */
    void Node::bind(WeakPtr<SHD::Library::Ptr::element_type> pShaders)
    {
        mShaders = pShaders;
    }


    /*!
     */
    Node::ShadersPtr Node::shaders() const
    {
        return mShaders;
    }


//------------------------------------------------------------------------------
//                                   Root Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    RootNode::RootNode(ELayers pLayer, SHD::Program::Id pDefault)
        : Node{ConstNodePtr{}}
        , mLayer{pLayer}
        , mProgram{pDefault}
    { }


    /*! Destructeur.
     */
    RootNode::~RootNode() { }


    /*! M�thode de rendu (vide).
     */
    void RootNode::perform() const
    {
        glRenderMode(GL_RENDER);
        switch(mLayer)
        {
            case eFrontOverlay:
            case eMainOverlay:
                glStencilFunc(GL_ALWAYS, mLayer & eLayerMask, eLayerMask);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                break;
            case eMainLayer:
            case eHighlightLayer:
                glStencilFunc(GL_EQUAL, eMainLayer, eLayerMask);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                break;
            default:
                break;
        }
        if (mProgram != SHD::Program::kNullId)
        {
            if (auto lLibrary = shaders().lock())
            {
                glUseProgram(lLibrary->program(mProgram));
            }
            else
            {
                glUseProgram(SHD::Program::kNullHdl);
            }
        }
        else
        {
            glUseProgram(SHD::Program::kNullHdl);
        }
    }


//------------------------------------------------------------------------------
//                               Picking Root Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    PickingRootNode::PickingRootNode(ELayers pLayer, SHD::Program::Id pPicking)
        : RootNode{pLayer, pPicking}
    { }


    /*! Destructeur.
     */
    PickingRootNode::~PickingRootNode() { }


    /*! Pr�pare le picking.
        @note Certaines op�rations devraient d�j� avoir �t� effectu�es au niveau du renderer
     */
    void PickingRootNode::perform() const
    {
        RootNode::perform();
    }


//------------------------------------------------------------------------------
//                              Highlight Root Node
//------------------------------------------------------------------------------

    /*! @param pWireColor Couleur.
        @param pHighlight Id du programme de rendu.
     */
    HighlightRootNode::HighlightRootNode(const CLR::RGB& pWireColor, SHD::Program::Id pHighlight)
        : RootNode{eHighlightLayer, pHighlight}
        , mColor{pWireColor}
    { }


    /*! Destructeur.
     */
    HighlightRootNode::~HighlightRootNode() { }


    /*! Pr�pare le rendu (surbrillance).
     */
    void HighlightRootNode::perform() const
    {
        // Couleur de surbrillance.
        RootNode::perform();
        glColor4fv(addressof(mColor[0]));
    }


//------------------------------------------------------------------------------
//                                  Alpha Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    AlphaNode::AlphaNode()
        : Node(ConstNodePtr())
        , mIsTranslucent(false)
    { }


    /*! Destructeur.
     */
    AlphaNode::~AlphaNode() { }


    /*! Vrai constructeur : d�finition de la transparence.
        @param pAppearance Apparence dont les textures seront activ�es
        @param pParentNode Noeud parent (dans l'arbre de rendu)
     */
    AlphaNode::AlphaNode(ConstAppearancePtr pAppearance, ConstNodePtr pParentNode)
        : Node(pParentNode)
        , mIsTranslucent((pAppearance.lock()->transparencyCoef() != k0F) || pAppearance.lock()->geometry()->testFlags(GEO::eRGBA))
    { }


    /*! M�thode de rendu : gestion de la transparence.
     */
    void AlphaNode::perform() const
    {
        if (mIsTranslucent)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);  // Voir http://stackoverflow.com/questions/17224879/opengl-alpha-blending-issue-blending-ignored-maybe
        }
        else
        {
            glDisable(GL_BLEND);
        }
    }


//------------------------------------------------------------------------------
//                                 Texture Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    TextureNode::TextureNode()
        : Node(ConstNodePtr())
        , mTexture1()
        , mTexture2()
    { }


    /*! Vrai constructeur : d�finition des textures � "binder".
        @param pAppearance Apparence dont les textures seront activ�es
        @param pParentNode Noeud parent (dans l'arbre de rendu)
     */
    TextureNode::TextureNode(ConstAppearancePtr, ConstNodePtr pParentNode)
        : Node(pParentNode)
        , mTexture1()
        , mTexture2()
    { }


    /*! Destructeur.
     */
    TextureNode::~TextureNode() { }


    /*! M�thode de rendu : gestion des textures.
        @todo G�rer correctement la deuxi�me texture
     */
    void TextureNode::perform() const
    {
        if (!mTexture1.expired())
        {
            glEnable(GL_TEXTURE_2D);

            mTexture1.lock()->activate(U32{0});

            if (!mTexture2.expired())
            {
                mTexture2.lock()->activate(U32{1});
            }
        }
        else
        {
            glDisable(GL_TEXTURE_2D);
        }
    }


//------------------------------------------------------------------------------
//                                  Light Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    LightNode::LightNode()
        : Node(ConstNodePtr())
        , mLights()
    { }


    /*! Vrai constructeur : d�finition des lumi�res � activer.
        @param pAppearance Les lumi�res �clairant cet objet seront activ�es
        @param pParentNode Noeud parent (dans l'arbre de rendu)
     */
    LightNode::LightNode(ConstAppearancePtr pAppearance, ConstNodePtr pParentNode)
        : Node(pParentNode)
        , mLights(pAppearance.lock()->litingLights())
    { }


    /*! Destructeur.
     */
    LightNode::~LightNode() { }


    /*! M�thode de rendu : gestion des lumi�res.
     */
    void LightNode::perform() const
    {
        APP::Appearance::ConstLightListPtr    lLightList = mLights.lock();

        // Active les lumi�res utilis�es.
        for(U32 lL = k0UL; lL < lLightList->count(); ++lL)
        {
            lLightList->at(lL).lock()->prepareForRendering(lL);
        }
        // Eteint les autres.
        for(U32 lL = lLightList->count(); lL < /*GraphicDevice->maxLightCount()*/8; ++lL)
        {
            glDisable(GL_LIGHT0 + lL);
        }

        if (0 < lLightList->count())
        {
            glEnable(GL_LIGHTING);
        }
        else
        {
            glDisable(GL_LIGHTING);
        }
    }


//------------------------------------------------------------------------------
//                                 Material Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    MaterialNode::MaterialNode()
        : Node(ConstNodePtr())
        , mMaterial()
    { }


    /*! Vrai constructeur : d�finition du mat�riau � appliquer.
        @param pAppearance Objet dont il faut activer les param�tres de mat�riau
        @param pParentNode Noeud parent (dans l'arbre de rendu)
        @note Il est vrai que la texture fait partie du mat�riau; toutefois, une m�me texture pouvant
        �tre partag�e par plusieurs mat�riaux, d�finir un noeud de texture et un noeud de mat�riau n'est
        pas erron�. En outre, changer de texture est bien plus co�teux que de modifier les param�tres de
        mat�riau (ce qui explique l'ordre de ces 2 noeuds dans la "hi�rarchie" de noeuds de rendu).
     */
    MaterialNode::MaterialNode(ConstAppearancePtr pAppearance, ConstNodePtr pParentNode)
        : Node(pParentNode)
        , mMaterial(pAppearance.lock()->material())
    { }


    /*! Destructeur.
     */
    MaterialNode::~MaterialNode() { }


    /*! M�thode de rendu : gestion des mat�riaux.
     */
    void MaterialNode::perform() const
    {
        if (!mMaterial.expired())
        {
            APP::Appearance::ConstMaterialPtr lMaterial = mMaterial.lock();

            glMaterialfv(GL_FRONT, GL_AMBIENT,  addressof(lMaterial->ambient()[0]));
            glMaterialfv(GL_FRONT, GL_DIFFUSE,  addressof(lMaterial->diffuse()[0]));
            glMaterialfv(GL_FRONT, GL_SPECULAR, addressof(lMaterial->specular()[0]));
            glMaterialfv(GL_FRONT, GL_EMISSION, addressof(lMaterial->emissive()[0]));

            glEnable(GL_COLOR_MATERIAL);
        }
        else
        {
            glDisable(GL_COLOR_MATERIAL);
        }
    }


//------------------------------------------------------------------------------
//                                  Shader Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    ShaderNode::ShaderNode()
        : Node{ConstNodePtr{}}
        , mProj{}
        , mView{}
        , mTransform{}
        , mAppearance{}
    { }


    /*! Vrai constructeur : configuration du shader � ex�cuter.
        @param pAppearance Objet � rendre.
        @param pProj       Matrice de projection de la cam�ra.
        @param pView       Matrice de vue de la cam�ra.
        @param pTransform  Matrice de transformation de l'apparence.
        @param pParentNode Noeud parent (dans l'arbre de rendu)
     */
    ShaderNode::ShaderNode(ConstAppearancePtr pAppearance, const MATH::M4& pProj, const MATH::M4& pView, const MATH::M4& pTransform, ConstNodePtr pParentNode)
        : Node(pParentNode)
        , mProj{pProj}
        , mView{pView}
        , mTransform{pTransform}
        , mAppearance{pAppearance}
    { }


    /*! Destructeur.
     */
    ShaderNode::~ShaderNode() = default;

    /*! M�thode de rendu par shaders.
     */
    void ShaderNode::perform() const
    {
        if(auto lAppearance = mAppearance.lock())
        {
            auto    lMaterial   = lAppearance->material();
            auto    lProgram    = lMaterial->program();
            if (lProgram != SHD::Program::kNullId)
            {
                if (auto lLibrary = shaders().lock())
                {
                    auto    lHdl = lLibrary->program(lProgram);
                    glUseProgram(lHdl);

                    auto    lPVM = mProj * mView * mTransform;
                    glUniformMatrix4fv(glGetUniformLocation(lHdl, "uPVM"), 1, false, addressof(lPVM[0]));
                    glUniformMatrix4fv(glGetUniformLocation(lHdl, "uModel"), 1, false, addressof(mTransform[0]));
                    auto    lInv = mTransform;  lInv.invert();
                    glUniformMatrix4fv(glGetUniformLocation(lHdl, "uInvModel"), 1, false, addressof(lInv[0]));
                    glUniform4fv(glGetUniformLocation(lHdl, "uDiffuse"), 1, addressof(lMaterial->diffuse()[0]));

                    // Lights.
                    if (auto lLightList = lAppearance->litingLights())
                    {
                        MATH::V4    lDirection{k0F, k0F, - k1F, k0F};
                        if (0 < lLightList->count())
                        {
                            if (auto lLight = lLightList->at(0).lock())
                            {
                                if (auto lDirLight = std::dynamic_pointer_cast<const LIT::DirectionalLight>(lLight))
                                {
                                    lDirection = lDirLight->direction();
                                    // Lumi�re blanche pour l'instant, la couleur arrivera plus tard.
                                }
                                glUniform4fv(glGetUniformLocation(lHdl, "uAmbient"), 1, addressof(lLight->ambient()[0]));
                            }
                            else
                            {
                                glUniform4fv(glGetUniformLocation(lHdl, "uAmbient"), 1, addressof(MATH::kNullV4[0]));
                            }
                            glUniform1i(glGetUniformLocation(lHdl, "uIsLit"), 1);
                        }
                        else
                        {
                            glUniform1i(glGetUniformLocation(lHdl, "uIsLit"), 0);
                        }
                        glUniform4fv(glGetUniformLocation(lHdl, "uLightDir"), 1, addressof(lDirection[0]));
                    }
                    else
                    {
                        glUniform1i(glGetUniformLocation(lHdl, "uIsLit"), 0);
                        glUniform4fv(glGetUniformLocation(lHdl, "uAmbient"), 1, addressof(MATH::kNullV4[0]));
                    }

                    for(U32 lT = k0UL; lT < lMaterial->textureCount(); ++lT)
                    {
                        auto    lTexture = lMaterial->namedTexture(lT);
                        auto    lTextureLoc = glGetUniformLocation(lHdl, std::get<MAT::Material::eTextureName>(lTexture).c_str());
                        std::get<MAT::Material::eTexture>(lTexture)->activate(lT);
                        glUniform1i(lTextureLoc, lT);
                    }

                    return;
                }
            }
        }

        glUseProgram(0);
    }


//------------------------------------------------------------------------------
//                                 Geometry Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    GeometryNode::GeometryNode()
        : Node(ConstNodePtr())
        , mTransform{}
        , mAppearance{}
    { }


    /*! Vrai constructeur : tous les attributs sont d�finis.
        @param pAppearance Objet � afficher
        @param pTransform  Matrice de transformation de <i>pAppearance</i>
        @param pParentNode Noeud parent
     */
    GeometryNode::GeometryNode(ConstAppearancePtr pAppearance,
        const MATH::M4& pTransform,
        ConstNodePtr    pParentNode)
        : Node(pParentNode)
        , mTransform(pTransform)
        , mAppearance{pAppearance}
    { }


    /*! Destructeur.
     */
    GeometryNode::~GeometryNode() { }


    /*! M�thode de rendu : affichage de la g�om�trie (tous les �tat de rendu ont d�j� �t� fix�s).
        @todo Essayer d'optimiser : d�finir les buffers dans Geometry::bufferObjects() pour le moteur, et ici, pour chaque rendu dans l'IDE seulement.
     */
    void GeometryNode::perform() const
    {
        if (auto lAppearance = mAppearance.lock())
        {
            APP::Appearance::ConstGeometryPtr    lGeometry = lAppearance->geometry();

            if (auto lLibrary = shaders().lock())
            {
                auto    lProgram = lAppearance->material()->program();
                auto    lProgHdl = lLibrary->program(lProgram);

                for(const auto& lField : lGeometry->fields())
                {
                    auto lVBO    = std::get<GEO::Geometry::EField::eVBO>(lField);
                    auto lAttrib = glGetAttribLocation(lProgHdl, std::get<GEO::Geometry::EField::eName>(lField).c_str());

                    if (lAttrib == -1)
                    {
                        continue;   // Il est possible que des attributs d�finis dans la g�om�trie, mais non-utilis�s
                                    // par le shader, aient �t� supprim�s � la compilation ("optimized-out").
                    }
                    glBindBuffer(GL_ARRAY_BUFFER, lVBO);

                    auto lByteCount = std::get<GEO::Geometry::EField::eByteCount>(lField);
                    auto lData      = std::get<GEO::Geometry::EField::eData>(lField);
                    if (lByteCount != 0 && lData != nullptr)
                    {
                        auto lSize  = std::get<GEO::Geometry::EField::eSize>(lField);
                        auto lType  = std::get<GEO::Geometry::EField::eType>(lField);

                        glBufferData(GL_ARRAY_BUFFER, lByteCount, lData, GL_DYNAMIC_DRAW);
                        if      (lType == GL_FLOAT)
                        {
                            glVertexAttribPointer(lAttrib, lSize, lType, GL_FALSE, 0, 0);
                        }
                        else if (lType == GL_INT)
                        {
                            glVertexAttribIPointer(lAttrib, lSize, lType, 0, 0);
                        }
                        else
                        {
                            continue;   // Cela ne devrait pas arriver. On passe au champ suivant sans activer celui-ci.
                        }
                        glEnableVertexAttribArray(lAttrib);
                    }
                    else
                    {
                        glDisableVertexAttribArray(lAttrib);
                    }
                }
                // Pour l'instant, les coordonnn�es de skining et de brouillard sont d�sactiv�es. On verra plus tard...

                if      (lGeometry->testFlags(GEO::eTriangles))
                {
                    // Liste de triangles.
                    const Vector<GEO::Triangle>&    lTriangleList   = lGeometry->triangleList(k1F);
                    U32                             lTriangleCount  = U32(lTriangleList.size());

                    glDrawElements(GL_TRIANGLES, 3 * lTriangleCount, GL_UNSIGNED_SHORT, &lTriangleList[0][0]);
                }
                else if (lGeometry->testFlags(GEO::eLines))
                {
                    // Liste de segments.
                    const Vector<GEO::Segment>&     lSegmentList    = lGeometry->segmentList(k1F);
                    U32                             lSegmentCount   = U32(lSegmentList.size());

                    glDrawElements(GL_LINES, 2 * lSegmentCount, GL_UNSIGNED_SHORT, &lSegmentList[0][0]);
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                                 Picking Node
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    PickingNode::PickingNode()
        : Node(ConstNodePtr())
        , mGeometry()
        , mName(eNoHit)
    { }


    /*! Vrai constructeur : tous les attributs sont d�finis.
        @param pAppearance Objet � afficher.
        @param pTransform  Matrice de transformation de <i>pAppearance</i>.
        @param pParentNode Noeud parent.
        @param pProgram    Handle du programme de rendu.
        @param pName       "Nom" pour le picking.
     */
    PickingNode::PickingNode(
        ConstAppearancePtr pAppearance,
        const MATH::M4&    pTransform,
        ConstNodePtr       pParentNode,
        SHD::Program::Hdl  pProgram,
        U32                pName)
        : Node(pParentNode)
        , mTransform(pTransform)
        , mGeometry(pAppearance.lock()->geometry())
        , mProgram{pProgram}
        , mName(pName)
    { }


    /*! Destructeur.
     */
    PickingNode::~PickingNode() { }


    /*! M�thode de rendu : affichage de la g�om�trie pour picking.
        @todo Plut�t passer la location que le handle du programme !
     */
    void PickingNode::perform() const
    {
        APP::Appearance::ConstGeometryPtr lGeometry = mGeometry.lock();

        glUniformMatrix4fv(glGetUniformLocation(mProgram, "uModel"), 1, false, addressof(mTransform[0]));
        CLR::RGB    lColor{mName};
        glUniform4f(glGetUniformLocation(mProgram, "uPickingColor"), lColor.r(), lColor.g(), lColor.b(), 1.0F);

        // En fonction des flags, les tableaux ad�quats sont sp�cifi�s...
        for(const auto& lField : lGeometry->fields())
        {
            auto    lAttrib = glGetAttribLocation(mProgram, std::get<GEO::Geometry::EField::eName>(lField).c_str());
            glBindBuffer(GL_ARRAY_BUFFER, std::get<GEO::Geometry::EField::eVBO>(lField));

            if (std::get<GEO::Geometry::EField::eName>(lField) == SHD_ATTRIBUTE_VERTEX)
            {
                auto lByteCount = std::get<GEO::Geometry::EField::eByteCount>(lField);
                auto lData      = std::get<GEO::Geometry::EField::eData>(lField);

                if (lByteCount != 0 && lData != nullptr)
                {
                    auto lSize  = std::get<GEO::Geometry::EField::eSize>(lField);
                    auto lType  = std::get<GEO::Geometry::EField::eType>(lField);

                    glBufferData(GL_ARRAY_BUFFER, lByteCount, lData, GL_DYNAMIC_DRAW);
                    glVertexAttribPointer(lAttrib, lSize, lType, GL_FALSE, 0, 0);
                    glEnableVertexAttribArray(lAttrib);
                }
            }
            else
            {
                glDisableVertexAttribArray(lAttrib);
            }
        }

        if      (lGeometry->testFlags(GEO::eTriangles))
        {
            // Liste de triangles.
            const Vector<GEO::Triangle>&    lTriangleList   = lGeometry->triangleList(k1F);
            U32                             lTriangleCount  = U32(lTriangleList.size());

            glDrawElements(GL_TRIANGLES, 3 * lTriangleCount, GL_UNSIGNED_SHORT, &lTriangleList[0][0]);
        }
        else if (lGeometry->testFlags(GEO::eLines))
        {
            // Liste de segments.
            const Vector<GEO::Segment>&     lSegmentList    = lGeometry->segmentList(k1F);
            U32                             lSegmentCount   = U32(lSegmentList.size());

            glDrawElements(GL_LINES, 2 * lSegmentCount, GL_UNSIGNED_SHORT, &lSegmentList[0][0]);
        }
    }


//------------------------------------------------------------------------------
//                                Highlight Node
//------------------------------------------------------------------------------

    /*!  Constructeur par d�faut.
     */
    HighlightNode::HighlightNode()
        : Node(ConstNodePtr())
        , mGeometry()
    { }


    /*!
     */
    HighlightNode::HighlightNode(ConstAppearancePtr pAppearance,
         const MATH::M4&    pTransform,
         ConstNodePtr pParentNode)
         : Node(pParentNode)
         , mTransform(pTransform)
         , mGeometry(pAppearance.lock()->geometry())
    { }


    /*!
     */
    HighlightNode::~HighlightNode() { }


    /*! M�thode de rendu : affichage de la g�om�trie seule (d'une couleur unie).
     */
    void HighlightNode::perform() const
    {
        APP::Appearance::ConstGeometryPtr lGeometry = mGeometry.lock();

        // Affichage en fil de fer.
        glPushAttrib(GL_POLYGON_BIT);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        // Etablit la matrice de projection.
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glMultMatrixf(addressof(mTransform[0]));

        // En fonction des flags, les tableaux ad�quats sont sp�cifi�s...
        // Vertex.
        if      (lGeometry->testFlags(GEO::eVertex3))
        {
            glVertexPointer(3, GL_FLOAT, 0, &(lGeometry->vertices3()[0]));
            glEnableClientState(GL_VERTEX_ARRAY);
        }
        else if (lGeometry->testFlags(GEO::eVertex4))
        {
            glVertexPointer(4, GL_FLOAT, 0, &(lGeometry->vertices4()[0]));
            glEnableClientState(GL_VERTEX_ARRAY);
        }
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        if      (lGeometry->testFlags(GEO::eTriangles))
        {
            // Liste de triangles.
            const Vector<GEO::Triangle>&    lTriangleList   = lGeometry->triangleList(k1F);
            U32                             lTriangleCount  = U32(lTriangleList.size());

            glDrawElements(GL_TRIANGLES, 3 * lTriangleCount, GL_UNSIGNED_SHORT, &lTriangleList[0][0]);
        }
        else if (lGeometry->testFlags(GEO::eLines))
        {
            // Liste de segments.
            const Vector<GEO::Segment>&     lSegmentList    = lGeometry->segmentList(k1F);
            U32                             lSegmentCount   = U32(lSegmentList.size());

            glDrawElements(GL_LINES, 2 * lSegmentCount, GL_UNSIGNED_SHORT, &lSegmentList[0][0]);
        }

        // R�tablit la matrice de transformation.
        glPopMatrix();

        // R�tablit le mode d'affichage des polygones.
        glPopAttrib();
    }
}
