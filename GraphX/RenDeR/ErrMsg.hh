/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_ERRMSG_HH
#define RDR_ERRMSG_HH

/*! @file GraphX/RenDeR/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module RDR.
    @author @ref Guillaume_Terrissol
    @date 14 Mars 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kNoParentNode;
    extern  ERR::String kRangeCheckError;
    extern  ERR::String kUnexistingNode;
    extern  ERR::String kInvalidModeValue;
    extern  ERR::String kInvalidRootNode;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De RDR_ERRMSG_HH
