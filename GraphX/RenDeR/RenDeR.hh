/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_RENDER_HH
#define RDR_RENDER_HH

/*! @file GraphX/RenDeR/RenDeR.hh
    @brief Pr�-d�clarations du module @ref RenDeR.
    @author @ref Guillaume_Terrissol
    @date 30 Mars 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR
{
    class NodeIterator;
    class HighlightTree;
    class PickingTree;
    class Renderer;
    class Tree;
}

#endif  // De RDR_RENDER_HH
