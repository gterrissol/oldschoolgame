/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/RenDeR/Node.inl
    @brief M�thodes inline des classes RDR::Node et d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 2 Juin 2002 - 27 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR
{
//------------------------------------------------------------------------------
//                             Op�rateurs d'Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup RDR_Nodes
        @return FAUX
     */
    inline bool operator==(const RootNode&, const RootNode&)
    {
        return false;
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> ont le m�me �tat de transparence (activ� ou non), FAUX sinon
     */
    inline bool operator==(const AlphaNode& pL, const AlphaNode& pR)
    {
        return pL.mIsTranslucent == pR.mIsTranslucent;
    }


    /*! @ingroup RDR_Nodes
        @return FAUX
     */
    inline bool operator==(const TextureNode& /*pL*/, const TextureNode& /*pR*/)
    {
        return false;   // Test � venir.
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques
     */
    inline bool operator==(const LightNode& pL, const LightNode& pR)
    {
        return (*pL.mLights.lock() == *pR.mLights.lock());
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques
     */
    inline bool operator==(const MaterialNode& pL, const MaterialNode& pR)
    {
        return (*pL.mMaterial.lock() == *pR.mMaterial.lock());
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques
     */
    inline bool operator==(const ShaderNode& /*pL*/, const ShaderNode& /*pR*/)
    {
        return false && // Test pour les textures. (� venir...)
               /*(pL.mProgram == pR.mProgram) &&
               (*pL.mLights.lock() == *pR.mLights.lock()) &&
               (*pL.mMaterial.lock() == *pR.mMaterial.lock()) &&
               (pL.mTransform == pR.mTransform)*/true;
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques
     */
    inline bool operator==(const GeometryNode& pL, const GeometryNode& pR)
    {
        return ((pL.mTransform == pR.mTransform) &&
                (pL.mAppearance.lock()->geometry() == pR.mAppearance.lock()->geometry()));
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si l'instance et <i>pPickingNode</i> sont indentiques
        @sa MATH::Matrix44::operator==()
     */
    inline bool operator==(const PickingNode& pL, const PickingNode& pR)
    {
        return ((pL.mTransform             == pR.mTransform) &&
                (pL.mName                  == pR.mName)      &&
                (pL.mGeometry.lock().get() == pR.mGeometry.lock().get()));
    }


    /*! @ingroup RDR_Nodes
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si <i>pL</i> et <i>pR</i> sont identiques
     */
    inline bool operator==(const HighlightNode& pL, const HighlightNode& pR)
    {
        // Seules les transformations g�om�triques sont � consid�rer (pour ne pas afficher deux
        // apparences se chevauchant : dans tous les autres cas, les g�om�tries doivent �tre rendues).
        return (pL.mTransform == pR.mTransform);
    }
}
