/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "RenDeR.hh"

/*! @file GraphX/RenDeR/RenDeR.cc
    @brief D�finitions diverses du module RDR.
    @author @ref Guillaume_Terrissol
    @date 14 Mars 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kRangeCheckError,   "Valeur d'indice incorrecte",   "Range check error.")
    REGISTER_ERR_MSG(kUnexistingNode,    "Node inexistant.",             "Unexisting node.")
    REGISTER_ERR_MSG(kNoParentNode,      "Pas de noeud p�re.",           "No parent node.")
    REGISTER_ERR_MSG(kInvalidModeValue,  "Valeur de mode incorrecte.",   "Invalid mode value.")
    REGISTER_ERR_MSG(kInvalidRootNode,   "Noeud racine invalide.",       "Invalid Root Node.")


//------------------------------------------------------------------------------
//                                   Constante
//------------------------------------------------------------------------------

    const char* kRenderingRaster = "Rendering";
}
