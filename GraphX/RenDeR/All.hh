/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_ALL_HH
#define RDR_ALL_HH

/*! @file GraphX/RenDeR/All.hh
    @brief Interface publique du module @ref RenDeR.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 9 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR   //! Gestion du rendu.
{
    /*! @namespace RDR
        @version 0.5
     */

    /*! @defgroup RenDeR RenDeR : Syst�me de rendu
        <b>namespace</b> RDR & @ref RDR_Nodes.
     */

    /*! @defgroup RDR_Nodes Noeuds de rendu
        @ingroup RenDeR
     */
}

#include "RenderTree.hh"
#include "Renderer.hh"

#endif  // De RDR_ALL_HH
