/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/RenDeR/Tree.inl
    @brief M�thodes inline des classes RDR::Tree & d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 28 Juillet 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR
{
//------------------------------------------------------------------------------
//                               Tree : Interface
//------------------------------------------------------------------------------

    /*! Insertion d'une apparence.
        @param pAppearance Objet � afficher
        @param pTransform  Matrice de transformation de <i>pAppearance</i>
     */
    inline void Tree::insert(ConstAppearancePtr pAppearance, const MATH::M4& pTransform)
    {
        insertApp(pAppearance, pTransform);
    }


    /*! @return Le mode de rendu offert par l' arbre
     */
    inline EModes Tree::mode() const
    {
        return getMode();
    }


    /*! R�initialise l'arbre : supprime tous les noeuds d�j� ins�r�s.
     */
    void Tree::reset()
    {
        cleanTree();
    }
}
