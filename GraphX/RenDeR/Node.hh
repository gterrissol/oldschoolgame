/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_NODE_HH
#define RDR_NODE_HH

#include "RenDeR.hh"

/*! @file GraphX/RenDeR/Node.hh
    @brief En-t�te des classes RDR::Node et d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2002 - 27 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/BaseAppearance.hh"
#include "CoLoR/RGBA.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "LIghT/BaseLight.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATH/M4.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "SHaDers/Program.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "Enum.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                          Noeud de Rendu : Interface
//------------------------------------------------------------------------------

    /*! @brief Noeud de rendu.
        @version 0.6
        @ingroup RenDeR

        A chaque �tat de rendu est associ� une classe d�riv�e de Node : AlphaNode, TextureNode,...<br>
        L'�tat de rendu (voire la g�om�trie) est �tabli via la m�thode Render.
        Cette hi�rarchie de classe a �t� �labor�e selon le <b>Case Pattern</b>.
     */
    class Node : public MEM::OnHeap
    {
    public:
        //! @name Types de pointeurs
        //@{
        using ConstAppearancePtr    = WeakPtr<APP::Appearance const>;           //!< Pointeur (constant) sur apparence.
        using ConstChildNodePtr     = SharedPtr<Node const>;                    //!< Pointeur (constant) sur apparence.
        using ChildNodePtr          = SharedPtr<Node>;                          //!< Pointeur sur apparence.
        using ConstNodePtr          = WeakPtr<Node const>;                      //!< Pointeur (constant) sur noeud.
        using NodePtr               = WeakPtr<Node>;                            //!< Pointeur sur noeud.
        using ShadersPtr            = WeakPtr<SHD::Library::Ptr::element_type>; //!< Pointeur sur biblioth�que de shaders.
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit            Node(ConstNodePtr pParent);                 //!< Constructeur.
virtual                     ~Node();                                    //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void                perform() const = 0;                        //!< Rend l'�tat associ� (texture / g�om�trie / ...).
        //@}
        //! @name Gestion des branches
        //@{
        ConstNodePtr        parent() const;                             //!< Noeud p�re.
        ConstNodePtr        next() const;                               //!< Noeud suivant.
        ConstChildNodePtr   child(U32 pN) const;                        //!< Acc�s aux noeuds fils.
        U32                 childCount() const;                         //!< Nombre de noeuds fils.
        void                insertChild(ConstChildNodePtr pChild);      //!< Ins�re un noeud fils.
        void                clear();                                    //!< Supprime toutes les r�f�rences vers les noeuds fils.
        //@}
        //! @name Gestion des shaders
        //@{
        void                bind(ShadersPtr pShaders);                  //!< 
        ShadersPtr          shaders() const;                            //!< 
        //@}

    private:

                            Node();                                     //!< Constructeur par d�faut.
        //! @name Gestion des branches
        //@{
        Vector<ConstChildNodePtr>   mChildren;                          //!< Noeuds fils.
        ConstNodePtr                mParentNode;                        //!< Noeud p�re.
        ConstNodePtr                mNextNode;                          //!< Noeud suivant.
        //@}
        ShadersPtr                  mShaders;                           //!< 
    };


//------------------------------------------------------------------------------
//                                 Noeuds Racine
//------------------------------------------------------------------------------

    /*! @brief Noeud : racine (standard).
        @version 0.4

        La seule utilit� de cette classe est de faire office de racine dans l'arbre de noeuds de rendu.
     */
    class RootNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const RootNode&, const RootNode&);  //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                RootNode(ELayers          pLayer,
                         SHD::Program::Id pDefault);        //!< Constructeur.
virtual         ~RootNode();                                //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                            //!< Aucun rendu effectu� par ce noeud.
        //@}

    private:

        ELayers             mLayer;                         //!< Niveau d'overlay.
        SHD::Program::Id    mProgram;                       //!< 
    };


    /*! @brief Noeud : racine pour un arbre de picking.
        @version 0.3
     */
    class PickingRootNode : public RootNode
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                PickingRootNode(ELayers          pLayer,
                                SHD::Program::Id pPicking); //!< Constructeur par d�faut.
virtual         ~PickingRootNode();                         //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                            //!< Pr�paration du picking.
        //@}
    };


    /*! @brief Noeud : racine pour un arbre de surbrillance.
        @version 0.3
     */
    class HighlightRootNode : public RootNode
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                HighlightRootNode(const CLR::RGB&  pWireColor,
                                  SHD::Program::Id pHighlight); //!< Constructeur.
virtual         ~HighlightRootNode();                           //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                //!< Pr�paration du rendu en surbrillance.
        //@}

    private:

        CLR::RGBA mColor;                                       //!< Couleur de surbrillance.
    };


//------------------------------------------------------------------------------
//                                Noeuds de Rendu
//------------------------------------------------------------------------------

    /*! @brief Noeud de rendu : gestion de la transparence.
        @version 0.2
     */
    class AlphaNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const AlphaNode& pL, const AlphaNode& pR);  //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                AlphaNode();                                        //!< Constructeur par d�faut.
virtual         ~AlphaNode();                                       //!< Destructeur.
                AlphaNode(ConstAppearancePtr pAppearance,
                          ConstNodePtr       pParentNode);          //!< Constructeur utile.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                    //!< G�re les param�tres de transparence.
        //@}

    private:
        //! @name Attribut du noeud
        //@{
        bool    mIsTranslucent;                                     //!< Etat de transparence.
        //@}
    };


    /*! @brief Noeud de rendu : gestion des textures.
        @version 0.4
     */
    class TextureNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const TextureNode&, const TextureNode&);            //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                TextureNode();                                              //!< Constructeur par d�faut.
                TextureNode(ConstAppearancePtr pAppearance,
                            ConstNodePtr       pParentNode);                //!< Constructeur utile.
virtual         ~TextureNode();                                             //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                            //!< Fixe les param�tres de texture.
        //@}

    private:
        //! @name Attributs du noeud
        //@{
        WeakPtr<MAT::Material::ConstTexturePtr::element_type>   mTexture1;  //!< 1�re texture � activer.
        WeakPtr<MAT::Material::ConstTexturePtr::element_type>   mTexture2;  //!< 2nde texture � activer.
        //@}
    };


    /*! @brief Noeud de rendu : gestion de l'�clairage.
        @version 0.4
     */
    class LightNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const LightNode& pL, const LightNode& pR);              //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                LightNode();                                                    //!< Constructeur par d�faut.
                LightNode(ConstAppearancePtr pAppearance,
                          ConstNodePtr       pParentNode);                      //!< Constructeur utile.

virtual         ~LightNode();                                                   //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                                //!< Pr�pare l'�clairage.
        //@}

    private:
        //! @name Attribut du noeud
        //@{
        WeakPtr<APP::Appearance::ConstLightListPtr::element_type>   mLights;    //!< Liste  des lumi�res � activer.
        //@}
    };


    /*! @brief Noeud de rendu : gestion des mat�riaux.
        @version 0.4
     */
    class MaterialNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const MaterialNode& pL, const MaterialNode& pR);        //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                MaterialNode();                                                 //!< Constructeur par d�faut.
                MaterialNode(ConstAppearancePtr pAppearance,
                             ConstNodePtr       pParentNode);                   //!< Constructeur utile.
virtual         ~MaterialNode();                                                //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                                //!< R�gle les param�tres des mat�riaux.
        //@}

    private:
        //! @name Attribut du noeud
        //@{
        WeakPtr<APP::Appearance::ConstMaterialPtr::element_type>    mMaterial;  //!< Mat�riau � appliquer.
        //@}
    };


    /*! @brief Noeud de rendu : gestion des shaders.
        @version 0.4
     */
    class ShaderNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const ShaderNode& pL, const ShaderNode& pR);    //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                ShaderNode();                                           //!< Constructeur par d�faut.
                ShaderNode(ConstAppearancePtr pAppearance,
                           const MATH::M4&    pProj,
                           const MATH::M4&    pView,
                           const MATH::M4&    pTransform,
                           ConstNodePtr       pParentNode);             //!< Constructeur utile.
virtual         ~ShaderNode();                                          //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                        //!< R�gle les param�tres des mat�riaux.
        //@}

    private:
        //! @name Attribut du noeud
        //@{
        MATH::M4            mProj;
        MATH::M4            mView;
        MATH::M4            mTransform;                                 //!< Regroupe une position, une orientation et une �chelle.
        ConstAppearancePtr  mAppearance;                                //!< Apparence � rendre.
        //@}
    };


    /*! @brief Noeud de rendu : g�om�trie.
        @version 0.6
     */
    class GeometryNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const GeometryNode& pL, const GeometryNode& pR);        //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                GeometryNode();                                                 //!< Constructeur par d�faut.
                GeometryNode(ConstAppearancePtr pAppearance,
                             const MATH::M4&    pTransform,
                             ConstNodePtr       pParentNode);                   //!< Constructeur utile.
virtual         ~GeometryNode();                                                //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                                //!< Affiche une g�om�trie.
        //@}

    private:
        //! @name Attributs du noeud
        //@{
        MATH::M4            mTransform; //!< Regroupe une position, une orientation et une �chelle.
        ConstAppearancePtr  mAppearance;    //!< Apparence � rendre.
        //@}
    };


    /*! @brief Noeud de rendu : picking.
        @version 0.4
     */
    class PickingNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const PickingNode& pL, const PickingNode& pR);          //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                PickingNode();                                                  //!< Constructeur par d�faut.
                PickingNode(ConstAppearancePtr pAppearance,
                            const MATH::M4&    pTransform,
                            ConstNodePtr       pParentNode,
                            SHD::Program::Hdl  pProgram,
                            U32                pName);                          //!< Constructeur utile.
virtual         ~PickingNode();                                                 //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                                //!< Affichage d'une g�om�trie pour picking.
        //@}

    private:
        //! @name Attributs du noeud
        //@{
        MATH::M4                                                    mTransform; //!< Regroupe une position, une orientation et une �chelle.
        WeakPtr<APP::Appearance::ConstGeometryPtr::element_type>    mGeometry;  //!< G�om�trie � afficher.
        SHD::Program::Hdl                                           mProgram;   //!< Programme GLSL pour le picking.
        U32                                                         mName;      //!< Nom (pour le picking) de l'apparence � rendre.
        //@}
    };


    /*! @brief Noeud de rendu : subrillance.
        @version 0.4

        Le rendu en surbrillance consiste � dessiner un objet en fil de fer color�
     */
    class HighlightNode : public Node
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const HighlightNode& pL, const HighlightNode& pR);      //!< Op�rateur d'�galit�.
        //@}
        //! @name Constructeur & destructeur
        //@{
                HighlightNode();                                                //!< Constructeur par d�faut.
                HighlightNode(ConstAppearancePtr pAppearance,
                              const MATH::M4&    pTransform,
                              ConstNodePtr       pParentNode);                  //!< Constructeur utile.
virtual         ~HighlightNode();                                               //!< Destructeur.
        //@}
        //! @name M�thode virtuelle de rendu
        //@{
virtual void    perform() const;                                                //!< Affichage d'une g�om�trie en surbrillance.
        //@}

    private:
        //! @name Attributs du noeud
        //@{
        MATH::M4                                                    mTransform; //!< Regroupe une position, une orientation et une �chelle.
        WeakPtr<APP::Appearance::ConstGeometryPtr::element_type>    mGeometry;  //!< G�om�trie � afficher.
        //@}
    };


    //! @name Op�rateurs d'�galit� pour des noeuds
    //@{
    inline  bool    operator==(const RootNode&,         const RootNode&);           //!< ... racine.
    inline  bool    operator==(const AlphaNode&     pL, const AlphaNode&     pR);   //!< ... alpha.
    inline  bool    operator==(const TextureNode&,      const TextureNode&);        //!< ... de texture.
    inline  bool    operator==(const LightNode&     pL, const LightNode&     pR);   //!< ... de lumi�re.
    inline  bool    operator==(const MaterialNode&  pL, const MaterialNode&  pR);   //!< ... de mat�riau.
    inline  bool    operator==(const ShaderNode&    pL, const ShaderNode&    pR);   //!< ... de shader.
    inline  bool    operator==(const GeometryNode&  pL, const GeometryNode&  pR);   //!< ... de g�om�trie.
    inline  bool    operator==(const PickingNode&   pL, const PickingNode&   pR);   //!< ... de picking.
    inline  bool    operator==(const HighlightNode& pL, const HighlightNode& pR);   //!< ... de surbrillance.
    //@}
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Node.inl"

#endif  // De RDR_NODE_HH
