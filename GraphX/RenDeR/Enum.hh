/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef RDR_ENUM_HH
#define RDR_ENUM_HH

/*! @file GraphX/RenDeR/Enum.hh
    @brief Enum�rations du module RDR.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2002 - 7 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace RDR
{
    //! Modes de rendu.
    enum EModes
    {
        // Main modes (viewport).
        eRender            = 0x01,
        ePicking           = 0x02,
        // Local to renderer modes.
        eDefault           = 0x00,
        eNoAlpha           = 0x08,
        eNoTexture         = 0x10,
        eNoLight           = 0x20,
        eWireFrame         = 0x40
    };

    //! Couches de rendu.
    enum ELayers
    {
        eFrontOverlay      = 0x0,  //!< Couche de surimpression du premier plan.
        eMainOverlay       = 0x1,  //!< Couche de surimpression principale.
        eMainLayer         = 0x2,  //!< Couche principale.
        eHighlightLayer    = 0x3,  //!< Couche de surbrillance.
        eLayerMask         = 0x3,  //!< Masque binaire.
        eLayerCount        = 4     //!< Nombre de couches.
    };

    enum
    {
        eNoHit         = 0xFFFFFF,
        eMaxHitCount   = 512
    };

    extern  const void* kRenderingRaster;
}

#endif // De RDR_ENUM_HH
