/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Tree.hh"

/*! @file GraphX/RenDeR/Tree.cc
    @brief M�thodes (non-inline) des classes RDR::Tree et d�riv�es.
    @author @ref Guillaume_Terrissol
    @date 11 Mai 2002 - 27 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>

#include "CAMera/BaseCamera.hh"
#include "STL/Deque.hh"
#include "STL/Limits.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace RDR
{
//------------------------------------------------------------------------------
//                                 Node Checker
//------------------------------------------------------------------------------

    /*! @brief Contr�leur de noeud
        @version 0.4

        Le param�tre template TTree, type d'arbre, est indispensable, car la m�thode insertNode()
        (appel�e dans checkNode()) de l'arbre utilis� est template et ne peut donc pas �tre virtuelle.
     */
    template<class TTree>
    class Tree::NodeChecker : public MEM::Auto<NodeChecker<TTree>>
    {
    public:

                        NodeChecker(TTree* pTree);                          //!< Constructeur.

        template<class TN>
        SharedPtr<TN>   checkNode(SharedPtr<TN> pNode, NodePtr pParent);    //!< V�rification de la pr�sence d'un noeud.

    private:

        TTree*  mTree;                                                      //!< Arbre contr�l�.
    };


//------------------------------------------------------------------------------
//                                  Node List
//------------------------------------------------------------------------------

    /*! @brief Liste de noeuds.
        @version 0.5

        Dans le principe, NodeList -> insertion automatique == nettoyage avant chaque rendu : mTop = nullptr.<br>
        Lors de l'insertion d'un noeud, <br>
        si mTop < taille alors *mNodes[mTop] = copie du node <br>
        sinon, push_back new node(noeud � ins�rer) => la taille augmentera petit � petit.<br>
        ++mTop.
        Cette classe peut �tre consid�r�e comme une pile de noeuds, avec r�servation m�moire (taille de
        mNodes). mTop fait office d'apr�s sommet (premier emplacement libre).
        @todo R�diger proprement cette doc
     */
    template<class TNode>
    class Tree::NodeList : public MEM::Auto<NodeList<TNode>>
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<TNode>    TNodePtr;   //!< Pointeur sur noeud.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    NodeList();                 //!< Constructeur par d�faut.
                    ~NodeList();                //!< Destructeur.
        //@}
        //! @name Gestion des noeuds
        //@{
        TNodePtr    insertNode(TNodePtr pNode); //!< Insertion d'un noeud.
        void        clear();                    //!< Vidage de la liste.
        TNodePtr    node(U32 pN);               //!< Acc�s � un noeud.
        //@}

    private:

        Vector<TNodePtr>    mNodes;             //!< Ensemble de noeuds.
        U32                 mTop;               //!< Sommet de la pile de noeuds.
    };


//------------------------------------------------------------------------------
//                                 Node Iterator
//------------------------------------------------------------------------------

    /*! @brief It�rateur (simpliste) sur un arbre de rendu.
        @version 0.4

        Un tel it�rateur (uni-directionnel) permet de parcourir les noeuds d'un arbre de rendu.
     */
    class NodeIterator : public MEM::Auto<NodeIterator>
    {
        //! @name Amies
        //@{
 friend class Tree;                                                        //!< Le conteneur doit conna�tre son it�rateur.
 friend bool operator!=(const NodeIterator& pL, const NodeIterator& pR);  //!< Op�rateur d'�galit�.
        //@}
    public:
        //! @name Type de pointeur
        //@{
        typedef Tree::ConstNodePtr    ConstNodePtr;                      //!< Pointeur sur noeud de rendu.
        //@}
        //! @name Incr�mentation
        //@{
        NodeIterator&  operator++();                                       //!< Pr�-incr�mentation.
        NodeIterator   operator++(int);                                    //!< Post-incr�mentation.
        //@}
        //! @name Indirection
        //@{
        ConstNodePtr   operator*() const;                                  //!< D�r�f�rencement.
        //@}

    private:
        //! @name Constructeurs
        //@{
                        NodeIterator();                                    //!< Constructeur par d�faut (d�sactiv�).
                        NodeIterator(const Tree*  pTree,
                                      ConstNodePtr pNode);                 //!< Autre constructeur.
        //@}
        //! @name El�ments acc�d�s
        //@{
        const Tree*    mTree;                                              //!< Arbre que parcourt cet it�rateur.
        ConstNodePtr   mCurrentNode;                                       //!< Noeud de l'arbre r�f�renc� actuellement.
        //@}
    };


    //! @name Op�rateurs
    //@{
    bool    operator==(const NodeIterator& pL, const NodeIterator& pR);   //!< ... d'�galit�.
    bool    operator!=(const NodeIterator& pL, const NodeIterator& pR);   //!< ... d'in�galit�.
    //@}


//------------------------------------------------------------------------------
//                                  Tree P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RDR::Tree.
        @version 0.2
     */
    class Tree::Private : public MEM::OnHeap
    {
    public:
    
        Private(RootNodePtr pRoot);             //!< Constructeur.
        //! @name Listes de noeuds
        //@{
        NodeList<AlphaNode>     mAlphaNodes;    //!< Noeuds de transparence.
        NodeList<ShaderNode>    mShaderNodes;   //!< Noeuds de programmes GLSL.
        NodeList<GeometryNode>  mGeometryNodes; //!< Noeuds de g�om�trie (transformation incluse).
        //@}
        //! @name Autres atributs
        //@{
        RootNodePtr             mRootNode;      //!< Noeud racine.
        ConstCameraPtr          mCamera;        //!< Cam�ra utilis�e pour rendre la sc�ne.
        ShadersPtr              mShaders;       //!< Shaders GLSL.
        //@}
    };


//------------------------------------------------------------------------------
//                              Picking Tree P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RDR::PickingTree.
        @version 0.3
     */
    class PickingTree::Private : public MEM::OnHeap
    {
    public:

        Private(SHD::Program::Id pProgram);         //!< Constructeur.

        NodeList<PickingNode>       mPickingNodes;  //!< Noeuds � rendre.
        Deque<APP::Pickable::Ptr>   mPickables;     //!< Objet s�lectionnables.
        GLuint                      mPickedName;    //!< Nom pick�.
        SHD::Program::Id            mProgramId;     //!< Id du programme GLSL pour le picking.
        SHD::Program::Hdl           mProgramHdl;    //!< Handle du programme GLSL pour le picking.
    };


//------------------------------------------------------------------------------
//                             Highlight Tree P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de RDR::HighlightTree.
        @version 0.2
     */
    class HighlightTree::Private : public MEM::OnHeap
    {
    public:

        NodeList<HighlightNode>   mHightlightNodes;   //!< Noeuds � rendre.
    };


//------------------------------------------------------------------------------
//                            Node Checker : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    template<class TTree>
    Tree::NodeChecker<TTree>::NodeChecker(TTree* pTree)
        : mTree(pTree)
    { }


    /*! V�rifie la pr�sence d'un noeud donn� (pour un noeud p�re donn�) dans l'arbre de rendu et l'ins�re
        dans l'arbre s'il en �tait absent.<br>
        <b>Principe</b> :<br>
        Afin de minimiser le nombre de changements d'�tats de rendu (textures, lumi�res,...), les
        RDR::Appearance sont tri�es.<br>
        A chaque �tat de rendu est associ� un type de noeud (d�riv� de RDR::Node). Lors de l'insertion
        d'une apparence, pour chaque �tat de rendu, une instance du type de noeud correspondant est cr�� :
        on v�rifie sa pr�sence dans l'arbre :<dl>
        <dt><b>s'il est d�j� pr�sent</b> (pour le m�me noeud p�re)</dt><dd> on ne l'ins�re pas � nouveau :
        la branche continuera par ce noeud, qui devient donc le noeud p�re du noeud suivant,
        <dt><b>s'il est absent de l'arbre</b></dt><dd> ce nouveau noeud est ins�r� (en tant que fils du
        noeud pr�c�dent).
        </dl>
        Ce m�canisme est it�r� jusqu'� ce que tous les noeuds soient ins�r�s pour une apparence donn�e.
        @param pNode   Noeud dont on veut v�rifier la pr�sence dans l'arbre
        @param pParent Noeud p�re de <i>pNode</i>
        @return Un noeud identique � <i>pNode</i> (et de m�me noeud p�re) d�j� ins�r� dans l'arbre, s'il
        y en a un, une copie de <i>pNode</i>, tout juste ins�r�e dans l'arbre, sinon
     */
    template<class TTree> template<class TN>
    SharedPtr<TN> Tree::NodeChecker<TTree>::checkNode(SharedPtr<TN> pNode, NodePtr pParent)
    {
        if (!pParent.expired())
        {
            SharedPtr<NodePtr::element_type>    lParentNode = pParent.lock();

            for(U32 lI = k0UL; lI < lParentNode->childCount(); ++lI)
            {
                SharedPtr<TN const> lNNode = std::dynamic_pointer_cast<const TN>(lParentNode->child(lI));

                // Si le noeud est d�j� pr�sent dans la liste, il est tout de suite retourn�.
                if ((lNNode != nullptr) && (*lNNode == *pNode))
                {
                    return std::const_pointer_cast<TN>(lNNode);
                }
            }
        }

        return mTree->insertNode(pNode, pParent);
    }


//------------------------------------------------------------------------------
//                             Node List : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TNode>
    Tree::NodeList<TNode>::NodeList()
        : mTop(0)
    {
        mNodes.reserve(1);
        mNodes.push_back(STL::makeShared<TNode>());
    }


    /*! Destructeur.
     */
    template<class TNode>
    Tree::NodeList<TNode>::~NodeList() { }


    /*! Ins�re un noeud dans la liste.
        @return Le noeud ins�r�
     */
    template<class TNode>
    typename Tree::NodeList<TNode>::TNodePtr Tree::NodeList<TNode>::insertNode(TNodePtr pNode)
    {
        if (mTop < mNodes.size())
        {
            mNodes[mTop] = pNode;
        }
        else
        {
            mNodes.push_back(pNode);
        }

        return mNodes[mTop++];   // Correct mais risqu�.
    }


    /*! Supprime les noeuds de la liste.
        @note Avant le passage aux smart pointers, les pointeurs stock�s dans la liste pointaient vers
        des objets
     */
    template<class TNode>
    void Tree::NodeList<TNode>::clear()
    {
        mNodes.resize(1);   // Pour revenir dans l'�tat initial.
        mNodes.front().reset();
        mTop = k0UL;
    }


    /*! @param pN Indice du noeud � r�cup�rer
        @return Le <i>pN</i>i�me noeud de la liste
     */
    template<class TNode>
    typename Tree::NodeList<TNode>::TNodePtr Tree::NodeList<TNode>::node(U32 pN)
    {
        ASSERT_EX(pN <= mTop, kUnexistingNode, return nullptr;)

        return mNodes[pN];
    }


//------------------------------------------------------------------------------
//                          Tree P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pRoot Noeud racine pour l'arbre
     */
    Tree::Private::Private(RootNodePtr pRoot)
        : mAlphaNodes{}
        , mShaderNodes{}
        , mGeometryNodes{}
        , mRootNode{pRoot}
        , mCamera{}
        , mShaders{}
    { }


//------------------------------------------------------------------------------
//                       Tree : Constructeur & Destructeur
//------------------------------------------------------------------------------


    /*! Constructeur par d�faut.
     */
    Tree::Tree()
        : pthis(nullptr)
    {
        setRootNode(STL::makeShared<RootNode>(eMainLayer, SHD::Program::kNullId));
    }


    /*! Constructeur.
        @param pRoot Noeud racine pour l'arbre
        @note C'est l'arbre qui se chargera de d�truire <i>pRoot</i>
     */
    Tree::Tree(RootNodePtr pRoot)
        : pthis(pRoot)
    {
        if (rootNode().expired())
        {
            setRootNode(STL::makeShared<RootNode>(eMainLayer, SHD::Program::kNullId));
        }
    }


    /*! Constructeur.
        @param pLayer Couche d'affichage
     */
    Tree::Tree(ELayers pLayer)
        : pthis{STL::makeShared<RootNode>(pLayer, SHD::Program::kNullId)}
    { }


    /*! Destructeur.
     */
    Tree::~Tree() { }


//------------------------------------------------------------------------------
//                               Tree : Interface
//------------------------------------------------------------------------------

    /*! D�finit la cam�ra utilis�e pour "voir" les apparences qui seront ins�r�es dans l'arbre.
        @param pCamera Cam�ra utilis�e pour "filmer" la sc�ne � rendre
     */
    void Tree::setCamera(ConstCameraPtr pCamera)
    {
        pthis->mCamera = pCamera;
    }


    /*!
     */
    Tree::ConstCameraPtr Tree::camera() const
    {
        return pthis->mCamera;
    }

    /*!
     */
    void Tree::bind(ShadersPtr pLibrary)
    {
        pthis->mShaders = pLibrary;
        if (auto lNode = rootNode().lock())
        {
            lNode->bind(pthis->mShaders);
        }
    }


    /*!
     */
    Tree::ShadersPtr Tree::library()
    {
        return pthis->mShaders;
    }


    /*! Proc�de au rendu des apparences.
     */
    void Tree::flush()
    {
        // Certains attributs seront sans doute modifi�s.
        glPushAttrib(GL_ALL_ATTRIB_BITS);

        preFlush();

        for(auto lNodeI = begin(); lNodeI != end(); ++lNodeI)
        {
            (*lNodeI).lock()->perform();
        }

        postFlush();

        glPopAttrib();
    }


//------------------------------------------------------------------------------
//                          Tree : Parcours des Noeuds
//------------------------------------------------------------------------------

    /*! @return Un it�rateur sur le premier noeud de rendu
     */
    NodeIterator Tree::begin() const
    {
        NodeIterator   lIter(this, rootNode());

        return lIter;
    }


    /*! @return Un it�rateur invalide
     */
    NodeIterator Tree::end() const
    {
        NodeIterator   lIter(this, ConstNodePtr());

        return lIter;
    }


//------------------------------------------------------------------------------
//                              Tree : Noeud racine
//------------------------------------------------------------------------------

    /*! Etablit le noeud racine de l'arbre. Les classes d�riv�es peuvent ainsi d�finir un noeud racine
        particulier.
        @param pRoot Noeud racine � utiliser pour le rendu de l'arbre
     */
    void Tree::setRootNode(RootNodePtr pRoot)
    {
        ASSERT(pRoot != nullptr, kInvalidRootNode);

        if (pthis->mRootNode != pRoot)
        {
            pthis->mRootNode = pRoot;
            pthis->mRootNode->bind(pthis->mShaders);
        }
    }


    /*! R�cup�ration du noeud racine.
        @return Le oeud racine de l'arbre
     */
    Tree::ConstNodePtr Tree::rootNode() const
    {
        return pthis->mRootNode;
    }


    /*! R�cup�ration du noeud racine.
        @return Le oeud racine de l'arbre
     */
    Tree::NodePtr Tree::rootNode()
    {
        return pthis->mRootNode;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Par d�faut, ne fait rien.
     */
    void Tree::preFlush() { }


    /*! Par d�faut, ne fait rien.
     */
    void Tree::postFlush() { }
    

//------------------------------------------------------------------------------
//                       Tree : Gestion Interne des Noeuds
//------------------------------------------------------------------------------

    /*! R�initialise les listes de noeuds pour pr�parer les insertions des noeuds d'un nouveau rendu.
     */
    void Tree::cleanTree()
    {
        rootNode().lock()->clear();
        pthis->mAlphaNodes.clear();
        pthis->mShaderNodes.clear();
        pthis->mGeometryNodes.clear();
    }


    /*! @fn template<class TN> SharedPtr<TN> Tree::insertNode(SharedPtr<TN> pNode, NodePtr pParent)
        Ins�re un nouveau noeud de type TN.
        @param pNode   Noeud � ins�rer
        @param pParent Noeud p�re de <i>pNode</i>
        @return Une copie de <i>pNode</i> (qui a �t� ins�r�e dans l'arbre)
     */

#ifndef NOT_FOR_DOXYGEN
    template<>
    SharedPtr<AlphaNode> Tree::insertNode(SharedPtr<AlphaNode> pNode, NodePtr pParent)
    {
        ASSERT_EX(!pParent.expired(), kNoParentNode, return SharedPtr<AlphaNode>();)

        // Un nouveau noeud est ins�r�.
        SharedPtr<AlphaNode>    lInsertedNode = pthis->mAlphaNodes.insertNode(pNode);
        pParent.lock()->insertChild(lInsertedNode);

        return lInsertedNode;
    }


    template<>
    SharedPtr<ShaderNode> Tree::insertNode(SharedPtr<ShaderNode> pNode, NodePtr pParent)
    {
        ASSERT_EX(!pParent.expired(), kNoParentNode, return SharedPtr<ShaderNode>();)

        // Un nouveau noeud est ins�r�.
        SharedPtr<ShaderNode> lInsertedNode = pthis->mShaderNodes.insertNode(pNode);
        lInsertedNode->bind(pthis->mShaders);
        pParent.lock()->insertChild(lInsertedNode);

        return lInsertedNode;
    }


    template<>
    SharedPtr<GeometryNode> Tree::insertNode(SharedPtr<GeometryNode> pNode, NodePtr pParent)
    {
        ASSERT_EX(!pParent.expired(), kNoParentNode, return SharedPtr<GeometryNode>();)

        // Un nouveau noeud est ins�r�.
        SharedPtr<GeometryNode> lInsertedNode = pthis->mGeometryNodes.insertNode(pNode);
        lInsertedNode->bind(pthis->mShaders);
        pParent.lock()->insertChild(lInsertedNode);

        return lInsertedNode;
    }
#endif  // NOT_FOR_DOXYGEN

//------------------------------------------------------------------------------
//                            Tree : Autres M�thodes
//------------------------------------------------------------------------------

    /*! 
     */
    void Tree::insertApp(ConstAppearancePtr pAppearance, const MATH::M4& pTransform)
    {
        NodeChecker<Tree>       lChecker(this);
        // Cr�e le noeud de transparence.
        SharedPtr<AlphaNode>    lAlphaNode = STL::makeShared<AlphaNode>(pAppearance, rootNode());
        lAlphaNode      = lChecker.checkNode(lAlphaNode, rootNode());

        // Cr�e le noeud shader.
        SharedPtr<ShaderNode>   lShaderNode = STL::makeShared<ShaderNode>(pAppearance,
                                                                          pthis->mCamera.lock()->projection(),
                                                                          pthis->mCamera.lock()->modelView(),
                                                                          pTransform,
                                                                          lAlphaNode);
        lShaderNode     = lChecker.checkNode(lShaderNode, lAlphaNode);

        // Cr�e le noeud g�om�trie (feuille).
        SharedPtr<GeometryNode> lGeometryNode = STL::makeShared<GeometryNode>(pAppearance, pTransform, lShaderNode);
                          lChecker.checkNode(lGeometryNode, lShaderNode);
    }


    /*! @return Le mode de rendu offert par l'arbre (eRender)
     */
    EModes Tree::getMode() const
    {
        return eRender;
    }


//------------------------------------------------------------------------------
//                          Node Iterator : (In)Egalit�
//------------------------------------------------------------------------------

    /*! @param pL 1er op�rande de la comparaison
        @param pR 2nd op�rande de la comparaison
        @return VRAI si les 2 it�rateurs r�f�rencent les m�mes �l�ments, FAUX sinon
     */
    bool operator==(const NodeIterator& pL, const NodeIterator& pR)
    {
        // Le seul op�rateur d'�galit� � utiliser est celui d'in�galit� :
        // c'est donc sur ce dernier qu'est bas� le r�sultat des comparaisons.
        return !(pL != pR);
    }


    /*! @param pL 1er op�rande de la comparaison
        @param pR 2nd op�rande de la comparaison
        @return VRAI si les it�rateurs r�f�rencent des �l�ments diff�rents (arbres ou noeuds), FAUX sinon
     */
    bool operator!=(const NodeIterator& pL, const NodeIterator& pR)
    {
        return ((pL.mTree != pR.mTree)                                                            ||
                std::owner_less<NodeIterator::ConstNodePtr>()(pL.mCurrentNode, pR.mCurrentNode) ||
                std::owner_less<NodeIterator::ConstNodePtr>()(pR.mCurrentNode, pL.mCurrentNode));
    }


//------------------------------------------------------------------------------
//                        Node Iterator : Incr�mentation
//------------------------------------------------------------------------------

    /*! Pr�-incr�mente l'it�rateur.
        @return L'it�rateur incr�ment�
     */
    NodeIterator& NodeIterator::operator++()
    {
        // Cherche le noeud suivant (d'abord un enfant, puis un fr�re).
        ConstNodePtr   lNextNode;

        if (mCurrentNode.lock()->childCount() != 0)
        {
            lNextNode = mCurrentNode.lock()->child(k0UL);
        }
        else
        {
            lNextNode = mCurrentNode.lock()->next();

            while((lNextNode.expired()) && (!mCurrentNode.lock()->parent().expired()))
            {
                mCurrentNode = mCurrentNode.lock()->parent();
                lNextNode = mCurrentNode.lock()->next();
            }
        }

        mCurrentNode = lNextNode;

        return *this;
    }


    /*! Post-incr�mente l'it�rateur.
        @return L'it�rateur avant son incr�mentation
     */
    NodeIterator NodeIterator::operator++(int)
    {
        NodeIterator lTmp = *this;
        ++(*this);

        return lTmp;
    }


//------------------------------------------------------------------------------
//                        Node Iterator : D�r�f�rencement
//------------------------------------------------------------------------------

    /*! D�r�f�rence l'it�rateur.
        @return Le noeud de rendu actuellement "point�" par l'it�rateur
     */
    Tree::ConstNodePtr NodeIterator::operator*() const
    {
        return mCurrentNode;
    }


//------------------------------------------------------------------------------
//                         Node Iterator : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur utile.
        @param pTree Arbre de rendu � parcourir avec cet it�rateur
        @param pNode Premier noeud du parcours
     */
    NodeIterator::NodeIterator(const Tree* pTree, ConstNodePtr pNode)
        : mTree(pTree)
        , mCurrentNode(pNode)
    { }


//------------------------------------------------------------------------------
//                      Picking Tree P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    PickingTree::Private::Private(SHD::Program::Id pProgram)
        : mPickingNodes()
        , mPickables{}
        , mPickedName(eNoHit)
        , mProgramId{pProgram}
        , mProgramHdl{SHD::Program::kNullHdl}
    { }


//------------------------------------------------------------------------------
//                   Picking Tree : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    PickingTree::PickingTree(SHD::Program::Id pPicking)
        : PickingTree(eMainLayer, pPicking)
    { }


    /*! Constructeur.
     */
    PickingTree::PickingTree(ELayers pLayer, SHD::Program::Id pPicking)
        : Tree{STL::makeShared<PickingRootNode>(pLayer, pPicking)}
        , pthis{pPicking}
    { }


    /*! Destructeur.
     */
    PickingTree::~PickingTree() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @sa https://www.opengl.org/wiki/Common_Mistakes#Selection_and_Picking_and_Feedback_Mode
     */
    APP::Pickable::Ptr PickingTree::picked(const UTI::Point& pPosition) const
    {
        GLint viewport[4]; 
        glGetIntegerv(GL_VIEWPORT, viewport);

        unsigned char   lPicked[4] = { };
        glReadPixels(pPosition.x, viewport[3] - pPosition.y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &lPicked);
        pthis->mPickedName = (GLuint{lPicked[0]} <<  0)
                           + (GLuint{lPicked[1]} <<  8)
                           + (GLuint{lPicked[2]} << 16);

        if (pthis->mPickedName != eNoHit)
        {
            return pthis->mPickables[pthis->mPickedName];
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Pr�pare la s�lection OpenGL.
     */
    void PickingTree::preFlush()
    {
        pthis->mPickedName = eNoHit;

        pthis->mProgramHdl = library()->program(pthis->mProgramId);
        glUseProgram(pthis->mProgramHdl);

        auto    lPV = camera().lock()->projection() * camera().lock()->modelView();
        glUniformMatrix4fv(glGetUniformLocation(pthis->mProgramHdl, "uPV"), 1, false, addressof(lPV[0]));

        // La couleur par d�faut sera restaur�e par glPopAttrib().
        glClearColor(1.0F, 1.0F, 1.0F, 1.0F);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }


    /*! R�cup�re le nom "s�lectionn�".
     */
    void PickingTree::postFlush()
    {
        glFlush();
        glFinish();
    }


//------------------------------------------------------------------------------
//                   Picking Tree : Gestion Interne des Noeuds
//------------------------------------------------------------------------------

    /*! R�initialise les listes de noeuds pour pr�parer les insertions des noeuds d'un nouveau rendu.
     */
    void PickingTree::cleanTree()
    {
        Tree::cleanTree();

        pthis->mPickingNodes.clear();
        pthis->mPickables.clear();
    }


    /*! Ins�re un nouveau noeud de type RDR::PickingNode.
        @param pNode   Noeud � ins�rer
        @param pParent Noeud p�re de <i>pNode</i>
        @return Une copie de <i>pNode</i> (qui a �t� ins�r�e dans l'arbre)
     */
    PickingTree::PickingNodePtr PickingTree::insertNode(PickingNodePtr pNode, NodePtr pParent)
    {
        ASSERT_EX(!pParent.expired(), kNoParentNode, return nullptr;)

        // Un nouveau noeud est ins�r�.
        PickingNodePtr lInsertedNode = pthis->mPickingNodes.insertNode(pNode);
        pParent.lock()->insertChild(lInsertedNode);

        return lInsertedNode;
    }


//------------------------------------------------------------------------------
//                        Picking Tree : Autres M�thodes
//------------------------------------------------------------------------------

    /*! Insertion d'une apparence.
     */
    void PickingTree::insertApp(ConstAppearancePtr pAppearance, const MATH::M4& pTransform)
    {
        U32                lName{static_cast<U32::TType>(pthis->mPickables.size())};
        pthis->mPickables.push_back(pAppearance.lock()->boundPickable());

        NodeChecker<PickingTree>    lChecker(this);
        // L'abre de picking n'a que des feuilles (un seul niveau d'arborescence).
        PickingNodePtr lPickingNode = STL::makeShared<PickingNode>(pAppearance, pTransform, rootNode(), pthis->mProgramHdl, lName);
        lChecker.checkNode(lPickingNode, rootNode());
    }


    /*! @return Le mode de rendu offert par l'arbre (ePicking)
     */
    EModes PickingTree::getMode() const
    {
        return ePicking;
    }


//------------------------------------------------------------------------------
//                  Highlight Tree : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param pColor     Couleur de surbrillance.
        @param pHighlight Id du programme de rendu.
     */
    HighlightTree::HighlightTree(const CLR::RGB& pColor, SHD::Program::Id pHighlight)
        : Tree{STL::makeShared<HighlightRootNode>(pColor, pHighlight)}
        , pthis{}
    { }


    /*! Destructeur.
     */
    HighlightTree::~HighlightTree() { }


//------------------------------------------------------------------------------
//                  Highlight Tree : Gestion Interne des Noeuds
//------------------------------------------------------------------------------

    /*! R�initialise les listes de noeuds pour pr�parer les insertions des noeuds d'un nouveau rendu.
     */
    void HighlightTree::cleanTree()
    {
        Tree::cleanTree();

        pthis->mHightlightNodes.clear();
    }


    /*! Ins�re un nouveau noeud de type RDR::HighlightNode.
        @param pNode   Noeud � ins�rer
        @param pParent Noeud p�re de <i>pNode</i>
        @return Une copie de <i>pNode</i> (qui a �t� ins�r�e dans l'arbre)
        @note Bien que ne retournant pas un param�tre template, le pr�fixe <i>t</i> est utilis� afin que
        le nom de la m�thode surcharg�e corresponde aux m�thodes template disponibles dans la classe de
        base
     */
    HighlightTree::HighlightNodePtr HighlightTree::insertNode(HighlightNodePtr pNode, NodePtr pParent)
    {
        ASSERT_EX(!pParent.expired(), kNoParentNode, return nullptr;)

        // Un nouveau noeud est ins�r�.
        HighlightNodePtr   lInsertedNode = pthis->mHightlightNodes.insertNode(pNode);
        pParent.lock()->insertChild(lInsertedNode);

        return lInsertedNode;
    }


//------------------------------------------------------------------------------
//                       Highlight Tree : Autres M�thodes
//------------------------------------------------------------------------------

    /*! Insertion d'une apparence.
     */
    void HighlightTree::insertApp(ConstAppearancePtr pAppearance, const MATH::M4& pTransform)
    {
        NodeChecker<HighlightTree>    lChecker(this);
        // L'abre de picking n'a que des feuilles (un seul niveau d'arborescence).
        HighlightNodePtr   lHighlightNode = STL::makeShared<HighlightNode>(pAppearance, pTransform, rootNode());
        lChecker.checkNode(lHighlightNode, rootNode());
    }


    /*! @return Le mode de rendu offert par l'arbre (eWireFrame)
     */
    EModes HighlightTree::getMode() const
    {
        return eWireFrame;
    }
}
