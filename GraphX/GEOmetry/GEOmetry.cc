/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GEOmetry.hh"

/*! @file GraphX/GEOmetry/GEOmetry.cc
    @brief D�finitions diverses du module GEO.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace GEO
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kAlreadyDefinedField,                "Champ d�j� d�fini.",                                       "Already defined field.")
    REGISTER_ERR_MSG(kAlreadyDefinedVBO,                  "VBO d�j� d�fini.",                                         "Already defined VBO.")
    REGISTER_ERR_MSG(kAutomaticFlagActivated,             "Flag automatique activ�.",                                 "Automatic flag activated.")
    REGISTER_ERR_MSG(kGeometryMustExist,                  "La g�om�trie doit exister.",                               "The geometry must exist.")
    REGISTER_ERR_MSG(kInvalidGeometry,                    "G�om�trie invalide.",                                      "InvalidGeometry.")
    REGISTER_ERR_MSG(kInvalidGeometrySize,                "Invalid g�om�trie size.",                                  "Invalid g�om�trie size.")
    REGISTER_ERR_MSG(kRangeCheckError,                    "Valeur d'indice incorrecte.",                              "Range check error.")
    REGISTER_ERR_MSG(kSegmentListNotOptimizedAsExpected,  "La liste de segments n'est pas optimis�e comme attendu.",  "The segment list isn't optimized as expected.")
    REGISTER_ERR_MSG(kTooManyElements,                    "Trop d'�l�ments.",                                         "Too many elements.")
    REGISTER_ERR_MSG(kTriangleAndSegmentListsBothEmpty,   "Listes de triangles et de segments toutes deux vides.",    "Triangle and segment lists both empty.")
    REGISTER_ERR_MSG(kTriangleListNotOptimizedAsExpected, "La liste de triangles n'est pas optimis�e comme attendu.", "The triangle list isn't optimized as expected.")
    REGISTER_ERR_MSG(kUndefinedField,                     "Champ non-d�fini.",                                        "Undefined field.")
    REGISTER_ERR_MSG(kUndefinedSegmentList,               "Liste de segments non-d�finie",                            "Undefined segment list")
    REGISTER_ERR_MSG(kUndefinedTriangleList,              "Liste de triangles non-d�finie",                           "Undefined triangle list")
}
