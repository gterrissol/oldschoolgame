/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_LIST_HH
#define GEO_LIST_HH

#include "GEOmetry.hh"

/*! @file GraphX/GEOmetry/List.hh
    @brief En-t�te des classes GEO::List & GEO::OptimizedList.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2005 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace GEO
{
//------------------------------------------------------------------------------
//                          Liste d'El�ments G�n�riques
//------------------------------------------------------------------------------

    /*! @brief Liste d'�l�ments (triangles ou segments).
        @version 0.5
        @ingroup GEOmetry

        Lorsque j'ai voulu introduire la gestion des segments, j'ai vraiment eu l'impression de faire du
        copier/coller de TriangleList. J'ai donc d�cider de fusionner les deux classes en une seule,
        <b>template</b>. Il existe donc deux param�tres avec lesquels instancier cette classe : Triangle
        (pour un maillage) et Segment (pour une courbe - au sens large, ce peut �tre une droite).<br>
        Les indices r�f�rencent les vertex d'une instance de GEO::Geometry.
     */
    template<class TE>
    class List : public MEM::OnHeap
    {
        //! @name Classe amie
        //@{
        BEFRIEND_MEM_ALLOCATOR()                                            //!< Pour l'acc�s au constructeur par d�faut.
        //@}
    public:
        typedef SharedPtr<List<TE>> ListPtr;
        //! @name Constructeurs & destructeur
        //@{
                                    List();                                 //!< Constructeur par d�faut.
                                    List(U16 pElementCount);                //!< Constructeur avec r�servation m�moire.
        inline  ListPtr             clone() const;                          //!< Constructeur virtuel.
virtual                             ~List();                                //!< Destructeur.
        //@}
        //! @name Gestion des �l�ments
        //@{
        inline  const Vector<TE>&   get(F32 pResolution = F32(1.0F)) const; //!< Liste d'�l�ments.
                void                push(const TE& pElement);               //!< Empilement d'un �l�ment.
        //@}
        //! @name Permutation des donn�es
        //@{
                void                swap(List<TE>& pElements);              //!< Liste des �l�ments.
        //@}

    protected:
        //! @name Autres constructeurs
        //@{
                                    List(const List& pList);                //!< Constructeur par copie.
    private:

                List<TE>&           operator=(const List<TE>& pList);       //!< Op�rateur d'affectation.
        //@}
        //! @name Comportement � sp�cialiser
        //@{
virtual         ListPtr             cloneInstance() const;                  //!< Constructeur virtuel.
virtual         const Vector<TE>&   computeList(F32 pResolution) const;     //!< Liste d'�l�ments.
        //@}
        PIMPL()
    };


    /*! @brief Liste d'�l�ments optimis�e
        @version 0.4
        @ingroup GEOmetry

        Cette classe impl�mente (pour le rendu) une liste d'�l�ments tri�s selon l'algorithme Totally
        Ordered Mesh (ou une variante, pour les segments).
        @sa TOM
     */
    template<class TE>
    class OptimizedList : public List<TE>
    {
        //! @name Classe amie
        //@{
        template<class TObj>
 friend class MEM::Allocator;                                                   //!< Pour l'acc�s au constructeur par d�faut.
        //@}
    public:
        typedef typename List<TE>::ListPtr    ListPtr;
        //! @name Constructeurs & destructeur
        //@{
                                    OptimizedList(U16 pElementCount,
                                                  U16 pVertexCount);            //!< Constructeur avec r�servation m�moire.
virtual                             ~OptimizedList();                           //!< Destructeur.
        //@}
        //! @name Gestion des �l�ments & des vertex
        //@{
        using   List<TE>::push;                                                 // Pour ne pas "cacher" List<TE>::Push(const TE&).
        inline  void                push(U16 pIndex);                           //!< Empilement d'un substitutant.
        inline  U16                 substitute(U16 pIndex) const;               //!< Substituant.
        //@}

    protected:
        //! @name Autres constructeurs
        //@{
                                    OptimizedList(const OptimizedList& pList);  //!< Constructeur par copie.
    private:

                OptimizedList<TE>&  operator=(const OptimizedList&);            //!< Op�rateur d'affectation.
        //@}
        //! @name Comportement sp�cialis�
        //@{
virtual         ListPtr             cloneInstance() const;                      //!< Constructeur virtuel.
virtual         const Vector<TE>&   computeList(F32 pResolution) const;         //!< Liste d'�l�ments.
        //@}
        PIMPL()
    };
}

//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "List.inl"

#endif  // De GEO_LIST_HH
