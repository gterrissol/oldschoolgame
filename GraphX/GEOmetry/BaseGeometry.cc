/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseGeometry.hh"

/*! @file GraphX/GEOmetry/BaseGeometry.cc
    @brief M�thodes (non-inline) de la classe GEO::Geometry.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 28 Mars 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>

#include "CoLoR/RGB.hh"
#include "CoLoR/RGBA.hh"
#include "PAcKage/LoaderSaver.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "SHaDers/Const.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "Fields.hh"
#include "List.tcc"

namespace GEO
{
    const U32 kInvalidObject = U32{0xFFFFFFFF};


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GEO::Geometry.
        @version 1.0

        Comme pour nombre de classes lors de la refonte, tous les attributs ainsi que les m�thodes
        priv�es ont �t� d�plac�es dans P-Impl.
     */
    class Geometry::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                        Private();                                                      //!< Constructeur par d�faut.
                        Private(PAK::Loader& pLoader);                                  //!< Constructeur (chargement imm�diat).
                        ~Private();                                                     //!< Destructeur.
        //@}
        //! @name Gestion des flags
        //@{
 static         void    checkFlags(I32& pFlags);                                        //!< Contr�le de la validit� des flags.
        inline  I32     flags() const;                                                  //!< Acc�s aux flags.
        inline  bool    testFlags(EFlags pFlags) const;                                 //!< Test d'activation d'un (ou de) flag(s).
        inline  void    setFlags(I32 pFlags);                                           //!< Etablissements des flags.

 static         void    checkAutoFlags(I32& pAutoFlags);                                //!< Contr�les des "flags automatiques".
        inline  I32     autoFlags() const;                                              //!< "Flags automatiques".
        inline  bool    testAutoFlags(EFlags pAutoFlags) const;                         //!< Test des "flags automatiques".
        template<class TF>
                void    setAutoFlag(bool pAutoFlag);                                    //!< D�finition d'un "flag automatique".
                void    setAutoFlags(I32 pAutoFlags);                                   //!< D�finition des "flags automatiques".
        //@}
        //! @name Ajout d'�l�ments
        //@{
        inline  void    pushTriangle(const Triangle& pTriangle);                        //!< Empilement d'un triangle.
        inline  void    pushSegment(const Segment& pSegment);                           //!< Empilement d'un segment.
        //@}
        //! @name Gestion des champs
        //@{
                void    checkFields();                                                  //!< Contr�le des champs.
                U32     makeVBO();                                                      //!< Allocation d'un VBO.
                void    addField(String pName, int pSize, int pType);                   //!< Ajout d'un champ standard.
                void    addExternalField(Field pData);                                  //!< Ajout dun champ.
                void    updateField(String pName, U32 pByteCount, const void* pData);   //!< Mise � jour des donn�es d'un champ.
                bool    hasField(const String& pName) const;                            //!< Test de pr�sence d'un champ.
        //@}
        //! @name Attributs : g�om�trie
        //@{
        SharedPtr<Vector<Vertex3>>      mVertices3;                                     //!< Coordonn�es spatiales.
        SharedPtr<Vector<Normal>>       mNormals;                                       //!< Normales.
        SharedPtr<Vector<RGBA>>         mColors4;                                       //!< Couleurs (RGBA).
        SharedPtr<Vector<TexCoord0>>    mTexCoords0;                                    //!< Coordonn�es pour la 1�re texture.
        SharedPtr<Vector<SkinCoord>>    mSkinCoords;                                    //!< Coordonn�es de m�lange.

        SharedPtr<Vector<Vertex4>>      mVertices4;                                     //!< Coordonn�es spatiales homog�nes.
        SharedPtr<Vector<RGB>>          mColors3;                                       //!< Couleurs (RGB).
        SharedPtr<Vector<TexCoord1>>    mTexCoords1;                                    //!< Coordonn�es pour la 2e texture.
        SharedPtr<Vector<FogCoord>>     mFogCoords;                                     //!< Coordonn�es de brouillard.

        Vector<Field>                   mFields;                                        //!< Champs de donn�es.

        Geometry::TriangleListPtr       mTriangleList;                                  //!< Liste de triangles.
        Geometry::SegmentListPtr        mSegmentList;                                   //!< Liste de segments.
        //@}
        //! @name Autres attributs
        //@{
        I32                             mFlags;                                         //!< Flags associ�s � la pr�sences des donn�es.
        I32                             mAutoFlags;                                     //!< Liaisons des champs cr��s par l'instance.
        U32                             mVAO;                                           //!< Handle de Vertex Array Object ("conteneur" de VBOs).
        U16                             mVertexCount;                                   //!< Nombre de vertex.
        U16                             mTriangleCount;                                 //!< Nombre de triangles.
        U16                             mSegmentCount;                                  //!< Nombre de segments.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Geometry::Private::Private()
        : mVertices3()
        , mNormals()
        , mColors4()
        , mTexCoords0()
        , mSkinCoords()
        , mVertices4()
        , mColors3()
        , mTexCoords1()
        , mFogCoords()
        , mFields{}
        , mTriangleList()
        , mSegmentList()
        , mFlags(0)
        , mAutoFlags(0)
        , mVAO{kInvalidObject}
        , mVertexCount(0)
        , mTriangleCount(0)
        , mSegmentCount(0)
    { }


    /*! Constructeur (chargement imm�diat).
     */
    Geometry::Private::Private(PAK::Loader& pLoader)
        : mVertices3()
        , mNormals()
        , mColors4()
        , mTexCoords0()
        , mSkinCoords()
        , mVertices4()
        , mColors3()
        , mTexCoords1()
        , mFogCoords()
        , mFields{}
        , mTriangleList()
        , mSegmentList()
        , mFlags(0)
        , mAutoFlags(0)
        , mVAO{kInvalidObject}
        , mVertexCount(0)
        , mTriangleCount(0)
        , mSegmentCount(0)
    {
        // Flags & "dimensions" de la g�om�trie.
        I32 lMainFlags(0), lAutoFlags(0);
        pLoader >> lMainFlags;
        pLoader >> lAutoFlags;

        // Les "dimensions" doivent �tre initialis�es avant les flags.
        pLoader >> mVertexCount;
        pLoader >> mTriangleCount;
        pLoader >> mSegmentCount;

        // Une instance cr��e � partir d'un fichier porte elle-m�me les champs qu'elle utilise.
        setFlags(lMainFlags);
        setAutoFlags(lAutoFlags);

        // Chargement des donn�es des vertex (d�clarer les instances
        // une seule fois �vite de perdre du temps en initialisation).
        Vertex3     lVertex3;
        Vertex4     lVertex4;
        Normal      lNormal;
        RGBA        lRGBA;
        RGB         lRGB;
        TexCoord0   lTexCoord0;
        TexCoord1   lTexCoord1;
        SkinCoord   lSkinCoord;
        FogCoord    lFogCoord;

        // G�om�trie proprement dite.
        for(U16 lV = k0UW; lV < mVertexCount; ++lV)
        {
            // Vertex.
            if      (testAutoFlags(eVertex3))
            {
                pLoader >> lVertex3;
                mVertices3->push_back(lVertex3);
            }
            else if (testAutoFlags(eVertex4))
            {
                pLoader >> lVertex4;
                mVertices4->push_back(lVertex4);
            }
            // Normales.
            if      (testAutoFlags(eNormal))
            {
                pLoader >> lNormal;
                mNormals->push_back(lNormal);
            }
            // Couleurs.
            if      (testAutoFlags(eRGBA))
            {
                pLoader >> lRGBA;
                mColors4->push_back(lRGBA);
            }
            else if (testAutoFlags(eRGB))
            {
                pLoader >> lRGB;
                mColors3->push_back(lRGB);
            }
            // Coordonn�es de texture.
            if      (testAutoFlags(eTexCoord0))
            {
                pLoader >> lTexCoord0;
                mTexCoords0->push_back(lTexCoord0);
            }
            if      (testAutoFlags(eTexCoord1))
            {
                pLoader >> lTexCoord1;
                mTexCoords1->push_back(lTexCoord1);
            }
            // Poids de m�lange pour le skinning.
            if      (testAutoFlags(eSkinCoord))
            {
                pLoader >> lSkinCoord;
                mSkinCoords->push_back(lSkinCoord);
            }
            // Coordonn�es de brouillard.
            if      (testAutoFlags(eFogCoord))
            {
                pLoader >> lFogCoord;
                mFogCoords->push_back(lFogCoord);
            }
        }

        // Triangles.
        if (testAutoFlags(eTriangles))
        {
            for(U16 lT = k0UW; lT < mTriangleCount; ++lT)
            {
                Triangle    lTriangle;
                pLoader >> lTriangle;
                pushTriangle(lTriangle);
            }

            if (testAutoFlags(eTOM))
            {
                // Si la liste de triangles est un TOM "automatique",
                // il faut aussi charger les substituants.
                OptimizedTriangleList*  lTom = dynamic_cast<OptimizedTriangleList*>(mTriangleList.get());
                ASSERT_EX(lTom != nullptr, kTriangleListNotOptimizedAsExpected, return;)

                for(U16 lV = k0UW, lSubstitute = k0UW; lV < mVertexCount; ++lV)
                {
                    pLoader >> lSubstitute;
                    lTom->push(lSubstitute);
                }
            }
        }
        else if (testAutoFlags(eLines))
        {
            for(U16 lS = k0UW; lS < mSegmentCount; ++lS)
            {
                Segment lSegment;
                pLoader >> lSegment;
                pushSegment(lSegment);
            }

            if (testAutoFlags(eTOC))
            {
                // Si la liste de segments est un TOL "automatique",
                // il faut aussi charger les substituants.
                OptimizedSegmentList*   lTol = dynamic_cast<OptimizedSegmentList*>(mSegmentList.get());
                ASSERT_EX(lTol != nullptr, kSegmentListNotOptimizedAsExpected, return;)

                for(U16 lV = k0UW, lSubstitute = k0UW; lV < mVertexCount; ++lV)
                {
                    pLoader >> lSubstitute;
                    lTol->push(lSubstitute);
                }
            }
        }
        else
        {
            WARNING(kTriangleAndSegmentListsBothEmpty)
        }
    }

    //! @todo V�rifier si le glBindVertexArray() est vraiment requis.
    Geometry::Private::~Private()
    {
        if (!mFields.empty())
        {
            glBindVertexArray(mVAO);
            for(const auto& f : mFields)
            {
                glDeleteBuffers(1, addressof(std::get<eVBO>(f)));
            }
            glDeleteVertexArrays(1, addressof(mVAO));
        }
    }

//------------------------------------------------------------------------------
//                          P-Impl : Gestion des Flags
//------------------------------------------------------------------------------

    /*! Certaines contraintes doivent s'appliquer sur les flags (ex : il y a forc�ment un champ de vertex
        et un seul).<br>
        Cette m�thode permet de contr�ler un ensemble de flags et de les compl�ter (ou les r�duire) pour
        obtenir des flags valides.
        @param pFlags Ensemble de flags (valeurs de GEO::EFlags combin�es par des <b>ou</b> binaires) �
        valider
        @sa CheckAutoFlags
     */
    void Geometry::Private::checkFlags(I32& pFlags)
    {
        // Contraintes communes aux deux types de flags.
        checkAutoFlags(pFlags);

        // eTexCoord1 implique eTexCoord0
        if (Geometry::testFlags(eTexCoord1, pFlags))
        {
            pFlags |= I32(eTexCoord0);
        }

        // Il faut toujours que des triangles ou des segments soient d�finis (avec une priorit�
        // pour les triangles : ce sont eux qui sont d�finis en cas d'absence des autres).
        if      (!Geometry::testFlags(eTriangles, pFlags) && !Geometry::testFlags(eLines, pFlags))
        {
            pFlags |= I32(eTriangles);
        }
        else if (Geometry::testFlags(EFlags(eTriangles | eLines), pFlags))
        {
            // S'il y a � la fois des triangles et des segments, seuls les triangles sont conserv�s.
            pFlags &= I32(~eTOC);
        }
    }


    /*! @return Les flags associ�s � la pr�sence de certains attributs (vertex, normales, couleurs,...)
     */     
    I32 Geometry::Private::flags() const
    {
        return mFlags;
    }


    /*! Teste la pr�sence d'un (ensemble de) flag(s).
        @param pFlags flag(s) dont on cherche la pr�sence dans les flags de l'instance
        @return VRAI si le(s) flag(s) <i>pFlags</i> a (ont) �t� d�fini(s) pour l'instance, FAUX sinon
     */
    inline bool Geometry::Private::testFlags(EFlags pFlags) const
    {
        return Geometry::testFlags(pFlags, flags());
    }


    /*! Etablit les nouveaux flags de l'instance.
        @param pFlags Nouveaux flags
     */
    inline void Geometry::Private::setFlags(I32 pFlags)
    {
        checkFlags(pFlags);
        mFlags = pFlags;
        
        if (!hasField(SHD_ATTRIBUTE_VERTEX))
        {
            if      (Geometry::testFlags(eVertex3, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_VERTEX},  3, GL_FLOAT);
            }
            else if (Geometry::testFlags(eVertex4, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_VERTEX},  4, GL_FLOAT);
            }
        }
        if (!hasField(SHD_ATTRIBUTE_NORMAL))
        {
            if (Geometry::testFlags(eNormal, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_NORMAL},  3, GL_FLOAT);
            }
        }
        if (!hasField(SHD_ATTRIBUTE_COLOR))
        {
            if      (Geometry::testFlags(eRGBA, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_COLOR},   4, GL_FLOAT);
            }
            else if (Geometry::testFlags(eRGB, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_COLOR},   3, GL_FLOAT);
            }
        }
        if (!hasField(SHD_ATTRIBUTE_TEX "0"))
        {
            if (Geometry::testFlags(eTexCoord0, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_TEX "0"}, 2, GL_FLOAT);
            }
        }
        if (!hasField(SHD_ATTRIBUTE_TEX "1"))
        {
            if (Geometry::testFlags(eTexCoord1, mFlags))
            {
                addField(String{SHD_ATTRIBUTE_TEX "1"}, 2, GL_FLOAT);
            }
        }
    }


    /*! Certaines contraintes doivent s'appliquer sur les flags automatiques - qui correspondent aux
        donn�es cr��es par et pour l'instance, par opposition aux donn�es partag�es (ex : il y a
        forc�ment un champ de vertex et un seul).<br>
        Cette m�thode permet de contr�ler un ensemble de flags et de les compl�ter (ou les r�duire) pour
        obtenir des flags valides.
        @param pAutoFlags Ensemble de flags (valeurs de EFlags combin�es par des <b>ou</b> binaires) �
        valider
        @sa CheckFlags
     */
    void Geometry::Private::checkAutoFlags(I32& pAutoFlags)
    {
        // eVertex3 & eVertex4 sont incompatibles.
        if      (Geometry::testFlags(eVertex3, pAutoFlags))
        {
            // Supprime le flag eVertex4 au cas o� il aurait �t� fix� (eVertex3 est prioritaire).
            pAutoFlags &= I32(~eVertex4);
        }
        else if (Geometry::testFlags(eVertex4, pAutoFlags))
        {
            // Supprime le flag eVertex3 au cas o� il aurait �t� fix�.
            pAutoFlags &= I32(~eVertex3);
        }
        // Une g�om�trie ne doit pas n�cessairement poss�der des vertex (ils peuvent �tre li�s).

        // eRGBA et eRGB sont trait�s diff�remment.
        if (Geometry::testFlags(eRGBA, pAutoFlags))
        {
            pAutoFlags &= I32(~eRGB);
        }
    }


    /*! @return La liste des flags automatiques
     */
    I32 Geometry::Private::autoFlags() const
    {
        return mAutoFlags;
    }


    /*! Teste la pr�sence d'un (ensemble de) flag(s) de liaison.
        @param pAutoFlags flag(s) � tester
        @return VRAI si le(s) flag(s) <i>pFlags</i> est (sont) automatiques, FAUX sinon
     */
    bool Geometry::Private::testAutoFlags(EFlags pAutoFlags) const
    {
        return Geometry::testFlags(pAutoFlags, autoFlags());
    }


    /*! @fn template<class TF> void Geometry::Private::setAutoFlag<TF>(bool pAutoFlag)
        Traite le champ de type <i>TF</i><small>ield</small> pour correspondre aux flags automatiques.
        @param pAutoFlag VRAI si le flag associ� au champ de type <i>TF</i> doit �tre automatique, FAUX
        s'il est inexistant ou doit �tre li�
     */

#ifndef NOT_FOR_DOXYGEN
    template<>
    void Geometry::Private::setAutoFlag<Vertex3>(bool pAutoFlag)
    {
        // eVertex3 doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eVertex3))
            {
                // Non : il le devient.
                mVertices3 = STL::makeShared<Vector<Vertex3>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mVertices3->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_VERTEX, U32(mVertices3->capacity() * sizeof(Vertex3)), mVertices3->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eVertex3))
            {
                // Oui : il est supprim�.
                mVertices3.reset();
                updateField(SHD_ATTRIBUTE_VERTEX, k0UL, nullptr);
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<Normal>(bool pAutoFlag)
    {
        // eNormal doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eNormal))
            {
                // Non : il le devient.
                mNormals = STL::makeShared<Vector<Normal>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mNormals->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_NORMAL, U32(mNormals->capacity() * sizeof(Normal)), mNormals->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eNormal))
            {
                // Oui : il est supprim�.
                mNormals.reset();
                updateField(SHD_ATTRIBUTE_NORMAL, k0UL, nullptr);
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<RGBA>(bool pAutoFlag)
    {
        // eRGBA doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eRGBA))
            {
                // Non : il le devient.
                mColors4 = STL::makeShared<Vector<RGBA>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mColors4->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_COLOR, U32(mColors4->capacity() * sizeof(RGBA)), mColors4->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eRGBA))
            {
                // Oui : il est supprim�.
                mColors4.reset();
                updateField(SHD_ATTRIBUTE_COLOR, k0UL, nullptr);
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<TexCoord0>(bool pAutoFlag)
    {
        // eTexCoord0 doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eTexCoord0))
            {
                // Non : il le devient.
                mTexCoords0 = STL::makeShared<Vector<TexCoord0>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mTexCoords0->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_TEX "0", U32(mTexCoords0->capacity() * sizeof(TexCoord0)), mTexCoords0->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eTexCoord0))
            {
                // Oui : il est supprim�.
                mTexCoords0.reset();
                updateField(SHD_ATTRIBUTE_TEX "0", k0UL, nullptr);
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<SkinCoord>(bool pAutoFlag)
    {
        // eSkinCoord doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eSkinCoord))
            {
                // Non : il le devient.
                mSkinCoords = STL::makeShared<Vector<SkinCoord>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            mSkinCoords->reserve(mVertexCount);
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eSkinCoord))
            {
                // Oui : il est supprim�.
                mSkinCoords.reset();
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<Vertex4>(bool pAutoFlag)
    {
        // eVertex4 doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eVertex4))
            {
                // Non : il le devient.
                mVertices4 = STL::makeShared<Vector<Vertex4>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mVertices4->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_VERTEX, U32(mVertices4->capacity() * sizeof(Vertex4)), mVertices4->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eVertex4))
            {
                // Oui : il est supprim�.
                mVertices4.reset();
                updateField(SHD_ATTRIBUTE_VERTEX, k0UL, nullptr);
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<RGB>(bool pAutoFlag)
    {
        // eRGB doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eRGB))
            {
                // Non : il le devient.
                mColors3 = STL::makeShared<Vector<RGB>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mColors3->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_COLOR, U32(mColors3->capacity() * sizeof(RGB)), mColors3->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eRGB))
            {
                // Oui : il est supprim�.
                mColors3.reset();
                updateField(SHD_ATTRIBUTE_COLOR, k0UL, nullptr);

            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<TexCoord1>(bool pAutoFlag)
    {
        // eTexCoord1 doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eTexCoord1))
            {
                // Non : il le devient.
                mTexCoords1 = STL::makeShared<Vector<TexCoord1>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            // Mise � jour des champs syst�matique.
            mTexCoords1->reserve(mVertexCount);
            updateField(SHD_ATTRIBUTE_TEX "1", U32(mTexCoords1->capacity() * sizeof(TexCoord1)), mTexCoords1->data());
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eTexCoord1))
            {
                // Oui : il est supprim�.
                mTexCoords1.reset();
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
            updateField(SHD_ATTRIBUTE_TEX "1", k0UL, nullptr);
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<FogCoord>(bool pAutoFlag)
    {
        // eFogCoord doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eFogCoord))
            {
                // Non : il le devient.
                mFogCoords = STL::makeShared<Vector<FogCoord>>();
            }
            // else / Il l'�tait d�j� : rien � faire.
            mFogCoords->reserve(mVertexCount);
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eFogCoord))
            {
                // Oui : il est supprim�.
                mFogCoords.reset();
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<Triangle>(bool pAutoFlag)
    {
        // eTriangles doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eTriangles))
            {
                // Non : il le devient.
                if (testFlags(eTOM))
                {
                    mTriangleList = STL::makeShared<OptimizedTriangleList>(mTriangleCount, mVertexCount);
                }
                else
                {
                    if (mTriangleCount != 0)
                    {
                        mTriangleList = STL::makeShared<TriangleList>(mTriangleCount);

                    }
                    else
                    {
                        mTriangleList = STL::makeShared<TriangleList>();
                    }
                }
            }
            // else / Il l'�tait d�j� : rien � faire.
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eTriangles))
            {
                // Oui : il est supprim�.
                mTriangleList.reset();
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }


    template<>
    void Geometry::Private::setAutoFlag<Segment>(bool pAutoFlag)
    {
        // eLines doit-il �tre automatique ?
        if (pAutoFlag)
        {
            // Oui. L'�tait-il d�j� ?
            if (!testAutoFlags(eLines))
            {
                // Non : il le devient.
                if (testFlags(eTOC))
                {
                    mSegmentList = STL::makeShared<OptimizedSegmentList>(mSegmentCount, mVertexCount);
                }
                else
                {
                    if (mSegmentCount != 0)
                    {
                        mSegmentList = STL::makeShared<SegmentList>(mSegmentCount);

                    }
                    else
                    {
                        mSegmentList = STL::makeShared<SegmentList>();
                    }
                }
            }
            // else / Il l'�tait d�j� : rien � faire.
        }
        else
        {
            // Non, il sera li�. Etait-il automatique ?
            if (testAutoFlags(eLines))
            {
                // Oui : il est supprim�.
                mSegmentList.reset();
            }
            // else / Non : rien � faire (�tant d�j� li� ou inexistant, il
            // pourra au besoin �tre li� � un champ d'une autre instance).
        }
    }

#endif  // De NOT_FOR_DOXYGEN


    /*! Les flags "auto" indiquent que tel ou tel champ doit �tre g�r� directement par l'instance, plut�t
        que li� au champ d'un autre.
        @param pAutoFlags Nouveaux flags
        @warning Cette m�thode r�serve de la m�moire pour les champs automatiques, par cons�quent,
        mVertexCount, mTriangleCount et mSegmentCount doivent avoir �t� initialis�s
     */
    inline void Geometry::Private::setAutoFlags(I32 pAutoFlags)
    {
        // Avant toute chose, contr�le les flags.
        checkAutoFlags(pAutoFlags);

        // Puis �tablit les flags pour chacun des champs.
        setAutoFlag<Vertex3>(Geometry::testFlags(eVertex3, pAutoFlags));
        setAutoFlag<Normal>(Geometry::testFlags(eNormal, pAutoFlags));
        setAutoFlag<RGBA>(Geometry::testFlags(eRGBA, pAutoFlags));
        setAutoFlag<TexCoord0>(Geometry::testFlags(eTexCoord0, pAutoFlags));
        setAutoFlag<SkinCoord>(Geometry::testFlags(eSkinCoord, pAutoFlags));
        setAutoFlag<Vertex4>(Geometry::testFlags(eVertex4, pAutoFlags));
        setAutoFlag<RGB>(Geometry::testFlags(eRGB, pAutoFlags));
        setAutoFlag<TexCoord1>(Geometry::testFlags(eTexCoord1, pAutoFlags));
        setAutoFlag<FogCoord>(Geometry::testFlags(eFogCoord, pAutoFlags));
        setAutoFlag<Triangle>(Geometry::testFlags(eTriangles, pAutoFlags));
        setAutoFlag<Segment>(Geometry::testFlags(eLines, pAutoFlags));

        // Et valide.
        mAutoFlags = pAutoFlags;
    }


//------------------------------------------------------------------------------
//                           P-Impl : Ajout d'El�ments
//------------------------------------------------------------------------------

    /*! Empilement d'un triangle.
        @param pTriangle Triangle � empiler
     */
    void Geometry::Private::pushTriangle(const Triangle& pTriangle)
    {
        ASSERT(mTriangleList != nullptr, kUndefinedTriangleList)

        mTriangleList->push(pTriangle);
    }


    /*! Empilement d'un segment.
        @param pSegment Segment � empiler
     */
    void Geometry::Private::pushSegment(const Segment& pSegment)
    {
        ASSERT(mSegmentList != nullptr, kUndefinedSegmentList)

        mSegmentList->push(pSegment);
    }


//------------------------------------------------------------------------------
//                        Gestion des Attributs (Champs)
//------------------------------------------------------------------------------

    /*! L'introduction tardive des shaders a commenc� � poser des probl�mes � cause de l'ordre des diff�rentes initialisations.@n
        Certaines g�om�tries peuvent �tre construites @b avant qu'OpenGL soit enti�rement initialis�; la cr�ation de VBO peut donc
        �chouer dans certains cas, c'est pourquoi cette cr�ation est retard� autant que possible : lors du rendu (voir pile d'appels).
     */
    void Geometry::Private::checkFields()
    {
        for(auto& f : mFields)
        {
            auto&   lVBO = std::get<eVBO>(f);
            if (lVBO == kInvalidObject)
            {
                lVBO = makeVBO();
            }
        }
    }

    /*! G�n�re un nouveau VBO (et aussi un Vertex Array si aucun n'a encore �t� d�fini).
        @return Le handle d'un nouveau VBO.
     */
    U32 Geometry::Private::makeVBO()
    {
        if (mVAO == kInvalidObject)
        {
            glGenVertexArrays(1, addressof(mVAO));
        }
        U32 lVBO = kInvalidObject;
        if (mVAO != kInvalidObject)
        {
            glBindVertexArray(mVAO);
            glGenBuffers(1, addressof(lVBO));
        }
        return lVBO;
    }

    /*! @param pName Nom de l'attribut,
        @param pSize Nombre d'�l�ments dans l'attribut (e.g. iNormal : 3),
        @param pType Type OpenGL des �l�ments (e.g. GL_FLOAT).
     */
    void Geometry::Private::addField(String pName, int pSize, int pType)
    {
        mFields.emplace_back(pName, I32(pSize), I32(pType), k0UL, nullptr, kInvalidObject);
    }

    //! @param pData Donn�es d'un attribut g�r� en dehors de l'instance g�om�trie.
    void Geometry::Private::addExternalField(Field pData)
    {
        ASSERT_EX(!hasField(std::get<eName>(pData)), kAlreadyDefinedField, return;)
        ASSERT_EX(std::get<eVBO>(pData) == k0UL, kAlreadyDefinedVBO, return;)

        std::get<eVBO>(pData) = kInvalidObject;

        mFields.push_back(pData);
    }

    /*! Pr�pare l'association des donn�es d'un attribut en m�moire centrale avec le VBO d�di�.
        @param pName      Nom de l'attribut,
        @param pByteCount Taille des donn�es (en octets),
        @param pData      Pointeur sur les donn�es.
     */
    void Geometry::Private::updateField(String pName, U32 pByteCount, const void* pData)
    {
        auto lField = std::find_if(mFields.begin(), mFields.end(), [&pName](const Field& pF)
        {
            return (std::get<EField::eName>(pF) == pName);
        });

        if (lField != mFields.end())
        {
            std::get<EField::eByteCount>(*lField) = pByteCount;
            std::get<EField::eData>(*lField)      = pData;
        }
    }

    /*! @retval true  Si un attribut nomm� @p pName a �t� d�fini,
        @retval false Sinon.
     */
    bool Geometry::Private::hasField(const String& pName) const
    {
        auto lField = std::find_if(mFields.begin(), mFields.end(), [&pName](const Field& pF)
        {
            return (std::get<EField::eName>(pF) == pName);
        });
        return (lField != mFields.end());
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Construit une instance � partir des donn�es d'un fichier.
        @param pLoader "Fichier" contenant les donn�es de l'instance � construire
     */
    Geometry::Geometry(PAK::Loader& pLoader)
        : RSC::Object(pLoader)
        , pthis(pLoader)
    { }


    /*! Destructeur.<br>
        Les diff�rents champs ne sont supprim�s que s'ils ont �t� allou�es par l'instance
     */
    Geometry::~Geometry() { }


//------------------------------------------------------------------------------
//                           R�cup�ration des Donn�es
//------------------------------------------------------------------------------

    /*! @return Le nombre de vertex de la g�om�trie
     */
    U16 Geometry::vertexCount() const
    {
        return pthis->mVertexCount;
    }


    /*! @return Le nombre (total) de triangles
     */
    U16 Geometry::triangleCount() const
    {
        return pthis->mTriangleCount;
    }


    /*! @return Le nombre (total) de segments
     */
    U16 Geometry::segmentCount() const
    {
        return pthis->mSegmentCount;
    }


    /*! @return Un vecteur sur les coordonn�es 3D de l'instance
     */
    const Vector<Vertex3>& Geometry::vertices3() const
    {
        ASSERT(pthis->mVertices3 != nullptr, kUndefinedField)

        return *pthis->mVertices3.get();
    }


    /*! @return Un vecteur sur les normales de l'instance
     */
    const Vector<Normal>& Geometry::normals() const
    {
        ASSERT(pthis->mNormals != nullptr, kUndefinedField)

        return *pthis->mNormals.get();
    }


    /*! @return Un vecteur sur les couleurs RGBA de l'instance
     */
    const Vector<RGBA>& Geometry::colors4() const
    {
        ASSERT(pthis->mColors4 != nullptr, kUndefinedField)

        return *pthis->mColors4.get();
    }


    /*! @return Un vecteur sur les 1�res coordonn�es de texture de l'instance
     */
    const Vector<TexCoord0>& Geometry::texCoords0() const
    {
        ASSERT(pthis->mTexCoords0 != nullptr, kUndefinedField)

        return *pthis->mTexCoords0.get();
    }


    /*! @return Un vecteur sur les coordonn�es de skinning de l'instance
     */
    const Vector<SkinCoord>& Geometry::skinCoords() const
    {
        ASSERT(pthis->mSkinCoords != nullptr, kUndefinedField)

        return *pthis->mSkinCoords.get();
    }


    /*! @return Un vecteur sur les coordonn�es 4D de l'instance
     */
    const Vector<Vertex4>& Geometry::vertices4() const
    {
        ASSERT(pthis->mVertices4 != nullptr, kUndefinedField)

        return *pthis->mVertices4.get();
    }


    /*! @return Un vecteur sur les couleurs RGB de l'instance
     */
    const Vector<RGB>& Geometry::colors3() const
    {
        ASSERT(pthis->mColors3 != nullptr, kUndefinedField)

        return *pthis->mColors3.get();
    }


    /*! @return Un vecteur sur les 2�mes coordonn�es de texture de l'instance
     */
    const Vector<TexCoord1>& Geometry::texCoords1() const
    {
        ASSERT(pthis->mTexCoords1 != nullptr, kUndefinedField)

        return *pthis->mTexCoords1.get();
    }


    /*! @return Un vecteur sur les coordonn�es de brouillard de l'instance
     */
    const Vector<FogCoord>& Geometry::fogCoords() const
    {
        ASSERT(pthis->mFogCoords != nullptr, kUndefinedField)

        return *pthis->mFogCoords.get();
    }

    /*! @param pResolution R�solution souhait�e pour la g�om�trie
        @return La liste de triangles de la g�om�trie avec une "pr�cision" de <i>pResolution</i>%
     */
    const Vector<Triangle>& Geometry::triangleList(F32 pResolution) const
    {
        ASSERT(pthis->mTriangleList != nullptr, kUndefinedTriangleList)

        return pthis->mTriangleList->get(pResolution);
    }


    /*! @param pResolution R�solution souhait�e pour la g�om�trie
        @return La liste de segments de la g�om�trie avec une "pr�cision" de <i>pResolution</i>%
     */
    const Vector<Segment>& Geometry::segmentList(F32 pResolution) const
    {
        ASSERT(pthis->mSegmentList != nullptr, kUndefinedSegmentList)

        return pthis->mSegmentList->get(pResolution);
    }


//------------------------------------------------------------------------------
//                              Donn�es des Sommets
//------------------------------------------------------------------------------

    //! @return Les attributs actuellement d�finis.
    const Vector<Geometry::Field>& Geometry::fields() const
    {
        pthis->checkFields();

        return pthis->mFields;
    }


//------------------------------------------------------------------------------
//                                Ajout de Champs
//------------------------------------------------------------------------------

    /*! @fn Geometry::glType<TT>()
        @return L'�num�ration OpenGL correspondant au type @p TT.
     */

    //! Instantiation explicite pour F32 : GL_FLOAT.
    template<>
    I32 Geometry::glType<F32>()
    {
        return I32(GL_FLOAT);
    }

    //! Instantiation explicite pour I32 : GL_INT.
    template<>
    I32 Geometry::glType<I32>() { return I32(GL_INT); }


    /*! @param pData Donn�es d'un attribut g�r� en dehors de l'instance.
     */
    void Geometry::addField(Field pData)
    {
        pthis->addExternalField(pData);
    }

//------------------------------------------------------------------------------
//                                   Liaisons
//------------------------------------------------------------------------------

    /*! @fn template<class TF> void Geometry::linkField<TF>(ConstGeometryPtr pRefGeo)
        Lie le champ de type <i>TField</i> de l'instance � celui d'une autre g�om�trie.
        @param pRefGeo G�om�trie dont le champ de type <i>TF</i><small>ield</small> sera utilis� par
        l'instance
        @note Le flag automatique associ� au champ de type <i>TF</i> doit �tre inhib�
     */

#ifndef NOT_FOR_DOXYGEN
    template<>
    void Geometry::linkField<Vertex3>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eVertex3), kAutomaticFlagActivated)

        pthis->mVertices3 = pRefGeo->pthis->mVertices3;
        pthis->updateField(SHD_ATTRIBUTE_VERTEX, U32(pthis->mVertices3->capacity() * sizeof(Vertex3)), pthis->mVertices3->data());
    }


    template<>
    void Geometry::linkField<Normal>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eNormal), kAutomaticFlagActivated)

        pthis->mNormals = pRefGeo->pthis->mNormals;
        pthis->updateField(SHD_ATTRIBUTE_NORMAL, U32(pthis->mNormals->capacity() * sizeof(Normal)), pthis->mNormals->data());
    }


    template<>
    void Geometry::linkField<RGBA>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eRGBA), kAutomaticFlagActivated)

        pthis->mColors4 = pRefGeo->pthis->mColors4;
        pthis->updateField(SHD_ATTRIBUTE_COLOR, U32(pthis->mColors4->capacity() * sizeof(RGBA)), pthis->mColors4->data());
    }


    template<>
    void Geometry::linkField<TexCoord0>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eTexCoord0), kAutomaticFlagActivated)

        pthis->mTexCoords0 = pRefGeo->pthis->mTexCoords0;
        pthis->updateField(SHD_ATTRIBUTE_TEX "0", U32(pthis->mTexCoords0->capacity() * sizeof(TexCoord0)), pthis->mTexCoords0->data());
    }


    template<>
    void Geometry::linkField<SkinCoord>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eSkinCoord), kAutomaticFlagActivated)

        pthis->mSkinCoords = pRefGeo->pthis->mSkinCoords;
    }


    template<>
    void Geometry::linkField<Vertex4>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eVertex4), kAutomaticFlagActivated)

        pthis->mVertices4 = pRefGeo->pthis->mVertices4;
        pthis->updateField(SHD_ATTRIBUTE_VERTEX, U32(pthis->mVertices4->capacity() * sizeof(Vertex4)), pthis->mVertices4->data());
    }


    template<>
    void Geometry::linkField<RGB>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eRGB), kAutomaticFlagActivated)

        pthis->mColors3 = pRefGeo->pthis->mColors3;
        pthis->updateField(SHD_ATTRIBUTE_COLOR, U32(pthis->mColors3->capacity() * sizeof(RGB)), pthis->mColors3->data());
    }


    template<>
    void Geometry::linkField<TexCoord1>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eTexCoord1), kAutomaticFlagActivated)

        pthis->mTexCoords1 = pRefGeo->pthis->mTexCoords1;
        pthis->updateField(SHD_ATTRIBUTE_TEX "1", U32(pthis->mTexCoords1->capacity() * sizeof(TexCoord1)), pthis->mTexCoords1->data());
    }


    template<>
    void Geometry::linkField<FogCoord>(ConstGeometryPtr pRefGeo)
    {
        ASSERT(!testAutoFlags(eFogCoord), kAutomaticFlagActivated)

        pthis->mFogCoords = pRefGeo->pthis->mFogCoords;
    }

#endif  // De NOT_FOR_DOXYGEN

    /*! Lie la liste de triangles de l'instance � celle d'une autre g�om�trie.
        @param pRefGeo G�om�trie dont la liste de triangles sera utilis� par l'instance
     */
    void Geometry::linkTriangleList(GeometryPtr pRefGeo)
    {
        linkTriangleList(pRefGeo->pthis->mTriangleList);
    }


    /*! Demande l'utilisation d'une liste de triangles externe.
        @param pList Liste de triangles que cette g�om�trie devra utiliser
     */
    void Geometry::linkTriangleList(TriangleListPtr pList)
    {
        ASSERT(!testAutoFlags(eTriangles), kAutomaticFlagActivated)

        pthis->mTriangleList = pList;
    }


    /*! Duplique la liste de triangles d'une autre g�om�trie.
        @param pRefGeo L'instance va porter une copie de la liste de triangles de cette g�om�trie
        @note Les flags (standard et automatiques) associ�s aux listes de triangles sont �tablis
        (indirectement) par cette m�thode
        @sa cloneTriangleList(const TriangleList* pList)
     */
    void Geometry::cloneTriangleList(ConstGeometryPtr pRefGeo)
    {
        cloneTriangleList(pRefGeo->pthis->mTriangleList);
    }

    /*! Duplique une liste de triangles externe.
        @param pList L'instance va porter une copie de cette liste de triangles
        @note Les flags (standard et automatiques) associ�s aux listes de triangles sont �tablis par
        cette m�thode
     */
    void Geometry::cloneTriangleList(ConstTriangleListPtr pList)
    {
        // Que la pr�c�dente liste de triangles ait �t� port�e en propre ou partag�e
        // importe peu, elle est simplement remplac�e (et supprim� au besoin).
        pthis->mTriangleList = pList->clone();

        // Le "champ" liste de triangles �tant assez particulier, il n'est pas forc�ment
        // aberrant de manipuler les flags dans cette m�thode (mais attention !).
        pthis->mAutoFlags |= I32(eTriangles);
        if (std::dynamic_pointer_cast<const OptimizedTriangleList>(pList) != nullptr)
        {
            pthis->mFlags      |= I32(eTOM);
            pthis->mAutoFlags  |= I32(eTOM);
        }
    }


    /*! Lie la liste de segments de l'instance � celle d'une autre g�om�trie.
        @param pRefGeo G�om�trie dont la liste de segments sera utilis� par l'instance
     */
    void Geometry::linkSegmentList(GeometryPtr pRefGeo)
    {
        linkSegmentList(pRefGeo->pthis->mSegmentList);
    }


    /*! Demande l'utilisation d'une liste de segments externe.
        @param pList Liste de segments que cette g�om�trie devra utiliser
     */
    void Geometry::linkSegmentList(SegmentListPtr pList)
    {
        ASSERT(!testAutoFlags(eLines), kAutomaticFlagActivated)

        pthis->mSegmentList = pList;
    }


    /*! Duplique la liste de segments d'une autre g�om�trie.
        @param pRefGeo L'instance va porter une copie de la liste de segments de cette g�om�trie
        @note Les flags (standard et automatiques) associ�s aux listes de segments sont �tablis
        (indirectement) par cette m�thode
        @sa cloneSegmentList(const SegmentList* pList)
     */
    void Geometry::cloneSegmentList(ConstGeometryPtr pRefGeo)
    {
        cloneSegmentList(pRefGeo->pthis->mSegmentList);
    }

    /*! Duplique une liste de segments externe.
        @param pList L'instance va porter une copie de cette liste de segments
        @note Les flags (standard et automatiques) associ�s aux listes de segments sont �tablis par cette
         m�thode
     */
    void Geometry::cloneSegmentList(ConstSegmentListPtr pList)
    {
        // Que la pr�c�dente liste de segments ait �t� port�e en propre ou partag�e
        // importe peu, elle est simplement remplac�e (et supprim� au besoin).
        pthis->mSegmentList = pList->clone();

        // Le "champ" liste de segments �tant assez particulier, il n'est pas forc�ment
        // aberrant de manipuler les flags dans cette m�thode (mais attention !).
        pthis->mAutoFlags |= I32(eLines);
        if (std::dynamic_pointer_cast<const OptimizedSegmentList>(pList) != nullptr)
        {
            pthis->mFlags      |= I32(eTOC);
            pthis->mAutoFlags  |= I32(eTOC);
        }
    }


//------------------------------------------------------------------------------
//                               Gestion des Flags
//------------------------------------------------------------------------------

    /*! @return Les flags associ�s � la pr�sence de certains attributs (vertex, normales, couleurs,...)
     */     
    I32 Geometry::flags() const
    {
        return pthis->flags();
    }


    /*! Teste la pr�sence d'un (ensemble de) flag(s).
        @param pFlags Flag(s) dont on cherche la pr�sence dans les flags de l'instance
        @return VRAI si le(s) flag(s) <i>pFlags</i> a (ont) �t� d�fini(s) pour l'instance, FAUX sinon
     */
    bool Geometry::testFlags(EFlags pFlags) const
    {
        return testFlags(pFlags, flags());
    }


    /*! Teste la pr�sence d'un flag parmi un ensemble donn�.
        @param pFlags    Flags � rechercher
        @param pRefFlags Flags de r�f�rence
        @return VRAI si le(s) flag(s) <i>pFlags</i> fait (font) partie des flags <i>pRefFlags</i>, FAUX
        sinon
     */
    bool Geometry::testFlags(EFlags pFlags, I32 pRefFlags)
    {
        return (pRefFlags & pFlags) == pFlags;
    }


    /*! Etablit les nouveaux flags de l'instance.
        @param pFlags Nouveaux flags
     */
    void Geometry::setFlags(I32 pFlags)
    {
        pthis->setFlags(pFlags);
    }


    /*! @return La liste des flags automatiques
     */
    I32 Geometry::autoFlags() const
    {
        return pthis->autoFlags();
    }


    /*! Teste la pr�sence d'un (ensemble de) flag(s) de liaison.
        @param pAutoFlags flag(s) � tester
        @return VRAI si le(s) flag(s) <i>pFlags</i> est (sont) automatiques, FAUX sinon
     */
    bool Geometry::testAutoFlags(EFlags pAutoFlags) const
    {
        return testFlags(pAutoFlags, autoFlags());
    }


    /*! Les flags "auto" indiquent que tel ou tel champ doit �tre g�r� directement par l'instance, plut�t
        que li� au champ d'un autre.
        @param pAutoFlags Nouveaux flags
        @warning Cette m�thode r�serve de la m�moire pour les champs automatiques, par cons�quent,
        mVertexCount, mTriangleCount et mSegmentCount doivent avoir �t� initialis�s
     */
    void Geometry::setAutoFlags(I32 pAutoFlags)
    {
        pthis->setAutoFlags(pAutoFlags);
    }


//------------------------------------------------------------------------------
//                             Autres Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut (g�om�trie temporaire).<br>
        Par d�faut, une g�om�trie est un ensemble de points vide.
     */
    Geometry::Geometry()
        : RSC::Object()
        , pthis()
    {
        setFlags(I32(eTriangles));
        setAutoFlags(I32(eTriangles));
    }


    /*! Constructeur (chargement diff�r�).
        @param pFileHdl Handle du fichier contenant les donn�es de l'instance
     */
    Geometry::Geometry(PAK::FileHdl pFileHdl)
        : RSC::Object(pFileHdl)
        , pthis()
    {
        setFlags(I32(eTriangles));
        setAutoFlags(I32(eTriangles));
    }


//------------------------------------------------------------------------------
//                              Gestion des Donn�es
//------------------------------------------------------------------------------

    /*!
     */
    void Geometry::swap(Geometry& pLoadedGeometry)
    {
        pthis.swap(pLoadedGeometry.pthis);
    }


    /*! Sauvegarde des donn�es.
        @param pSaver Fichier dans lequel sauvegarder les donn�es
     */
    void Geometry::store(PAK::Saver& pSaver) const
    {
        RSC::Object::store(pSaver);

        // Flags & "dimensions" de la g�om�trie.
        pSaver << flags();
        pSaver << autoFlags();
        pSaver << vertexCount();
        pSaver << triangleCount();
        pSaver << segmentCount();

        // Sauve les informations des vertex.
        for(U16 lV = k0UW; lV < vertexCount(); ++lV)
        {
            // Vertex.
            if      (testAutoFlags(eVertex3))
            {
                pSaver << vertices3()[lV];
            }
            else if (testAutoFlags(eVertex4))
            {
                pSaver << vertices4()[lV];
            }
            // Normales.
            if      (testAutoFlags(eNormal))
            {
                pSaver << normals()[lV];
            }
            // Couleurs.
            if      (testAutoFlags(eRGBA))
            {
                pSaver << colors4()[lV];
            }
            else if (testAutoFlags(eRGB))
            {
                pSaver << colors3()[lV];
            }
            // Coordonn�es de texture.
            if      (testAutoFlags(eTexCoord0))
            {
                pSaver << texCoords0()[lV];
            }
            if      (testAutoFlags(eTexCoord1))
            {
                pSaver << texCoords1()[lV];
            }
            // Poids de m�lange pour le skinning.
            if      (testAutoFlags(eSkinCoord))
            {
                pSaver << skinCoords()[lV];
            }
            // Coordonn�es de brouillard.
            if      (testAutoFlags(eFogCoord))
            {
                pSaver << fogCoords()[lV];
            }
        }

        // Ne sauve la liste de triangles que si cette derni�re appartient effectivement � l'instance.
        if (testAutoFlags(eTriangles))
        {
            // Il faut sauver la liste de triangles compl�te (1.0F == 100%).
            const Vector<Triangle>& lTrianglesToStore = triangleList(F32(1.0F));

            for(U32 lT = k0UL; lT < lTrianglesToStore.size(); ++lT)
            {
                pSaver << lTrianglesToStore[lT];
            }

            if (testAutoFlags(eTOM))
            {
                OptimizedTriangleList*  lTom = dynamic_cast<OptimizedTriangleList*>(pthis->mTriangleList.get());
                ASSERT(lTom != nullptr, kTriangleListNotOptimizedAsExpected)

                // Pour un TOM, il faut �galement sauver les substituants.
                for(U16 lV = k0UW; lV < vertexCount(); ++lV)
                {
                    pSaver << lTom->substitute(lV);
                }
            }
        }
        else if (testAutoFlags(eLines))
        {
            // Il faut sauver la liste de segments compl�te (1.0F == 100%).
            const Vector<Segment>&  lSegmentsToStore = segmentList(F32(1.0F));

            for(U32 lS = k0UL; lS < lSegmentsToStore.size(); ++lS)
            {
                pSaver << lSegmentsToStore[lS];
            }

            if (testAutoFlags(eTOC))
            {
                OptimizedSegmentList*  lTol = dynamic_cast<OptimizedSegmentList*>(pthis->mSegmentList.get());
                ASSERT(lTol != nullptr, kSegmentListNotOptimizedAsExpected)

                // Pour un TOL, il faut �galement sauver les substituants.
                for(U16 lV = k0UW; lV < vertexCount(); ++lV)
                {
                    pSaver << lTol->substitute(lV);
                }
            }
        }
    }


//------------------------------------------------------------------------------
//                            D�finition des donn�es
//------------------------------------------------------------------------------

    /*! @return Un vecteur sur les coordonn�es 3D de l'instance
     */
    Vector<Vertex3>& Geometry::vertices3()
    {
        ASSERT(pthis->mVertices3 != nullptr, kUndefinedField)

        return *pthis->mVertices3.get();
    }


    /*! @return Un vecteur sur les normales de l'instance
     */
    Vector<Normal>& Geometry::normals()
    {
        ASSERT(pthis->mNormals != nullptr, kUndefinedField)

        return *pthis->mNormals.get();
    }


    /*! @return Un vecteur sur les couleurs RGBA de l'instance
     */
    Vector<RGBA>& Geometry::colors4()
    {
        ASSERT(pthis->mColors4 != nullptr, kUndefinedField)

        return *pthis->mColors4.get();
    }


    /*! @return Un vecteur sur les 1�res coordonn�es de texture de l'instance
     */
    Vector<TexCoord0>& Geometry::texCoords0()
    {
        ASSERT(pthis->mTexCoords0 != nullptr, kUndefinedField)

        return *pthis->mTexCoords0.get();
    }


    /*! @return Un vecteur sur les coordonn�es de skinning de l'instance
     */
    Vector<SkinCoord>& Geometry::skinCoords()
    {
        ASSERT(pthis->mSkinCoords != nullptr, kUndefinedField)

        return *pthis->mSkinCoords.get();
    }


    /*! @return Un vecteur sur les coordonn�es 4D de l'instance
     */
    Vector<Vertex4>& Geometry::vertices4()
    {
        ASSERT(pthis->mVertices4 != nullptr, kUndefinedField)

        return *pthis->mVertices4.get();
    }


    /*! @return Un vecteur sur les couleurs RGB de l'instance
     */
    Vector<RGB>& Geometry::colors3()
    {
        ASSERT(pthis->mColors3 != nullptr, kUndefinedField)

        return *pthis->mColors3.get();
    }


    /*! @return Un vecteur sur les 2�mes coordonn�es de texture de l'instance
     */
    Vector<TexCoord1>& Geometry::texCoords1()
    {
        ASSERT(pthis->mTexCoords1 != nullptr, kUndefinedField)

        return *pthis->mTexCoords1.get();
    }


    /*! @return Un vecteur sur les coordonn�es de brouillard de l'instance
     */
    Vector<FogCoord>& Geometry::fogCoords()
    {
        ASSERT(pthis->mFogCoords != nullptr, kUndefinedField)

        return *pthis->mFogCoords.get();
    }


    /*! Empilement d'un triangle.
        @param pTriangle Triangle � empiler
     */
    void Geometry::pushTriangle(const Triangle& pTriangle)
    {
        ASSERT(pthis->mTriangleList != nullptr, kUndefinedTriangleList)

        pthis->pushTriangle(pTriangle);
    }


    /*! Empilement d'un segment.
        @param pSegment Segment � empiler
     */
    void Geometry::pushSegment(const Segment& pSegment)
    {
        pthis->pushSegment(pSegment);
    }

    /*! D�finit le nombre maximum de vertex potentiels.
        @param pVertexCount Nombre de vertex
     */
    void Geometry::setVertexCount(U16 pVertexCount)
    {
        pthis->mVertexCount = pVertexCount;
        // R�assigne les drapeaux, pour les mises � jour des champs.
        setAutoFlags(autoFlags());
    }


    /*! D�finition du nombre total de triangles.
        @note M�me si cette information fait doublon avec la taille de mTriangleList, elle est n�anmoins requise pour pouvoir construire
        les listes de triangles dans SetAutoFlag<Triangle>()
     */
    void Geometry::setTriangleCount(U16 pTriangleCount)
    {
        pthis->mTriangleCount = pTriangleCount;
    }


    /*! D�finition du nombre total de segments.
        @note M�me si cette information fait doublon avec la taille de mSegmentList, elle est n�anmoins
        requise pour pouvoir construire les listes de segments dans SetAutoFlag<Segment>()
     */
    void Geometry::setSegmentCount(U16 pSegmentCount)
    {
        pthis->mSegmentCount = pSegmentCount;
    }


//------------------------------------------------------------------------------
//                          Documentation des Attributs
//------------------------------------------------------------------------------

    /* SkinCoords :
        Ces donn�es permettent de r�aliser le skinning pour le rendu des objets. Les "bones" sont stock�s
        dans le tableau de la base Group de l'objet. Les indices de mBlendCoords r�f�rencent des
        �l�ments de ce tableau. L'animation des "bones" est calcul� dans la biblioth�que ANImation, celle
        des vertex est r�alis�e par le moteur.
     */


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  RSC::ResourceMgr<GeometryRsc>*  MEM::Singleton<RSC::ResourceMgr<GeometryRsc>>::smThat   = nullptr;
#endif
}

#ifndef NOT_FOR_DOXYGEN
namespace RSC
{
    template class ResourceMgr<GEO::GeometryRsc>;   // G�n�re le code pour GeometryMgr.
}
#endif  // De NOT_FOR_DOXYGEN
