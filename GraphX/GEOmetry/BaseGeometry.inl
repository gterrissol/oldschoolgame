/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/GEOmetry/BaseGeometry.inl
    @brief M�thodes inline de la classe GEO::Geometry.
    @author @ref Guillaume_Terrissol
    @date 13 Novembre 2015 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO
{
//------------------------------------------------------------------------------
//                                Ajout de Champs
//------------------------------------------------------------------------------

    /*! @param pName Nom du champ.
        @param pData Donn�es.
        @tparam TT Type de donn�e des tableaux.
        @tparam TN Taille des tableaux.
     */
    template<class TT, std::size_t TN>
    void Geometry::addField(String pName, const Vector<std::array<TT, TN>>& pData)
    {
        addField(std::make_tuple(pName, I32(TN), glType<TT>(), U32(pData.size() * TN * sizeof(TT)), static_cast<const void*>(pData.data()), k0UL));
    }

    /*! @param pName Nom du champ.
        @param pData Donn�es.
        @tparam TT Type de donn�e du champ.
     */
    template<class TT>
    void Geometry::addField(String pName, const Vector<TT>& pData)
    {
        addField(std::make_tuple(pName, I32(1), glType<TT>(), U32(pData.size() * sizeof(TT)), static_cast<const void*>(pData.data()), k0UL));
    }
}
