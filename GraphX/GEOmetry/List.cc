/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "List.hh"

/*! @file GraphX/GEOmetry/List.cc
    @brief M�thodes (non-inline) de la classe GEO::OptimizedList.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2005 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>

#include "Fields.hh"
#include "List.tcc"

namespace GEO
{
//------------------------------------------------------------------------------
//      Liste d'El�ments Optimis�e : Gestion des Elements (Sp�cialisations)
//------------------------------------------------------------------------------

    /*!
     */
    template<>
    const Vector<Triangle>& OptimizedList<Triangle>::computeList(F32 pResolution) const
    {
        if (pResolution != pthis->mPreviousResolution)
        {
            const Vector<Triangle>& lRefTrList          = List<Triangle>::get();
            U16                     lLastTriangleIndex  = U16(U16::TType(pResolution * lRefTrList.size()) - 1);
            const Triangle&         lLastTriangle       = lRefTrList[lLastTriangleIndex];
            U16                     lLastVertexIndex    = *std::max_element(&lLastTriangle[0], &lLastTriangle[3]);

            pthis->mCurrentIndices.resize(lLastTriangleIndex + 1);
            for(U16 lTr = k0UW; lTr <= lLastTriangleIndex; ++lTr)
            {
                Triangle    lCurrentTriangle    = lRefTrList[lTr];

                while(lLastVertexIndex < lCurrentTriangle[0])
                {
                    lCurrentTriangle[0] = pthis->mSubstitutes[lCurrentTriangle[0]];
                }
                while(lLastVertexIndex < lCurrentTriangle[1])
                {
                    lCurrentTriangle[1] = pthis->mSubstitutes[lCurrentTriangle[1]];
                }
                while(lLastVertexIndex < lCurrentTriangle[2])
                {
                    lCurrentTriangle[2] = pthis->mSubstitutes[lCurrentTriangle[2]];
                }

                pthis->mCurrentIndices[lTr] = lCurrentTriangle;
                if (lCurrentTriangle != lRefTrList[lTr])
                {
                    // M�moriser le plus bas indice de triangle modifi�, pour chaque pas de simplication (== nombre de vertex).
                }
            }

            pthis->mPreviousResolution = pResolution;
        }

        return pthis->mCurrentIndices;
    }


    /*!
     */
    template<>
    const Vector<Segment>& OptimizedList<Segment>::computeList(F32 pResolution) const
    {
        if (pResolution != pthis->mPreviousResolution)
        {
            const Vector<Segment>&  lRefSegList         = List<Segment>::get();
            U16                     lLastSegmentIndex   = U16(U16::TType(pResolution * lRefSegList.size()) - 1);
            const Segment&          lLastSegment        = lRefSegList[lLastSegmentIndex];
            U16                     lLastVertexIndex    = std::max(lLastSegment[0], lLastSegment[1]);

            pthis->mCurrentIndices.resize(lLastSegmentIndex + 1);
            for(U16 lSeg = k0UW; lSeg <= lLastSegmentIndex; ++lSeg)
            {
                Segment lCurrentSegment = lRefSegList[lSeg];

                while(lLastVertexIndex < lCurrentSegment[0])
                {
                    lCurrentSegment[0] = pthis->mSubstitutes[lCurrentSegment[0]];
                }
                while(lLastVertexIndex < lCurrentSegment[1])
                {
                    lCurrentSegment[1] = pthis->mSubstitutes[lCurrentSegment[1]];
                }

                pthis->mCurrentIndices[lSeg] = lCurrentSegment;
                if (lCurrentSegment != lRefSegList[lSeg])
                {
                    // M�moriser le plus bas indice de segment modifi�, pour chaque pas de simplication (== nombre de vertex).
                }
            }

            pthis->mPreviousResolution = pResolution;
        }

        return pthis->mCurrentIndices;
    }
}
