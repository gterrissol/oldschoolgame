/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_GEOMETRY_HH
#define GEO_GEOMETRY_HH

/*! @file GraphX/GEOmetry/GEOmetry.hh
    @brief Pr�-d�clarations du module @ref GEOmetry.
    @author @ref Guillaume_Terrissol
    @date 19 Mars 2008 - 14 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/CoLoR.hh"

#include "Enum.hh"

namespace GEO
{
    class Geometry;
    class Segment;
    class SkinCoord;
    class Triangle;

    template<int TC, int TS>    class Field;
    template<class TE>          class List;
    template<class TE>          class OptimizedList;

    //! @name Champs d'une g�om�trie
    //@{
    typedef class Field<3, eVertex3>    Vertex3;    //!< Vertex (3D).
    typedef class Field<4, eVertex4>    Vertex4;    //!< Vertex (4D : + w).
    typedef class Field<3, eNormal>     Normal;     //!< Vecteur normal.
    typedef class Field<2, eTexCoord0>  TexCoord0;  //!< Coordonn�es de texture (1�re).
    typedef class Field<2, eTexCoord1>  TexCoord1;  //!< Coordonn�es de texture (2nde).
    typedef class Field<1, eFogCoord>   FogCoord;   //!< Coordonn�es de brouillard.
    // Les couleurs aussi sont des champs...
    typedef CLR::RGBA                   RGBA;       //!< Couleur (R G B A).
    typedef CLR::RGB                    RGB;        //!< Couleur (R G B).
    //@}

    //! @name Types de liste
    //@{
    typedef List<Triangle>              TriangleList;           //!< Liste de triangles.
    typedef OptimizedList<Triangle>     OptimizedTriangleList;  //!< Liste de triangles.

    typedef List<Segment>               SegmentList;            //!< Liste de segments.
    typedef OptimizedList<Segment>      OptimizedSegmentList;   //!< Liste de segments.
    //@}
}

#endif  // De GEO_GEOMETRY_HH
