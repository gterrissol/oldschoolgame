/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_BASEGEOMETRY_HH
#define GEO_BASEGEOMETRY_HH

#include "GEOmetry.hh"

/*! @file GraphX/GEOmetry/BaseGeometry.hh
    @brief En-t�te de la classe GEO::Geometry.
    @author @ref Guillaume_Terrissol
    @date 1er Avril 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GraphicDeVice/GraphicDeVice.hh"
#include "MEMory/BuiltIn.hh"
#include "ReSourCe/BaseResource.hh"
#include "ReSourCe/Object.hh"
#include "STL/String.hh"
#include "STL/Tuple.hh"
#include "STL/Vector.hh"

#include "Enum.hh"

namespace GEO
{
    /*! @brief Objet g�om�trie.
        @version 0.7
        @ingroup GEOmetry

        Classe de base pour manipuler la g�om�trie (volume) d'un objet.<br>
        Les instances de cette classe peuvent �tre utilis�es en tant que g�om�tries temporaires pour l'affichage (en tant qu'attributs
        d'apparences). Dans la plupart des autres cas, ce sont les classes d�riv�es qui seront utilis�es.
        @sa MATerial
     */
    class Geometry : public RSC::Object
    {
        //! @name Classes amies
        //@{
        BEFRIEND_MEM_ALLOCATOR()                                                        //!< Pour l'acc�s au constructeur par d�faut.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        typedef SharedPtr<Geometry> Ptr;
        //@}
        //! @name Constructeur & destructeur
        //@{
        explicit                    Geometry(PAK::Loader& pLoader);                     //!< Constructeur (chargement imm�diat).
virtual                             ~Geometry();                                        //!< Destructeur.
        //@}
        //! @name R�cup�ration des donn�es
        //@{
        U16                         vertexCount() const;                                //!< Nombre (maximum) de vertex.
        U16                         triangleCount() const;                              //!< Nombre (total) de triangles.
        U16                         segmentCount() const;                               //!< Nombre (total) de segments.

        const Vector<Vertex3>&      vertices3() const;                                  //!< Coordonn�es spatiales.
        const Vector<Normal>&       normals() const;                                    //!< Normales.
        const Vector<RGBA>&         colors4() const;                                    //!< Couleurs (RGBA).
        const Vector<TexCoord0>&    texCoords0() const;                                 //!< Coordonn�es pour la 1�re texture.
        const Vector<SkinCoord>&    skinCoords() const;                                 //!< Coordonn�es de m�lange.

        const Vector<Vertex4>&      vertices4() const;                                  //!< Coordonn�es spatiales homog�nes.
        const Vector<RGB>&          colors3() const;                                    //!< Couleurs (RGB).
        const Vector<TexCoord1>&    texCoords1() const;                                 //!< Coordonn�es pour la 2e texture.
        const Vector<FogCoord>&     fogCoords() const;                                  //!< Coordonn�es de brouillard.

        const Vector<Triangle>&     triangleList(F32 pResolution) const;                //!< Liste de triangles.
        const Vector<Segment>&      segmentList(F32 pResolution) const;                 //!< Liste de segments.
        //@}
        //! @name Acc�s aux donn�es des sommets (Field = attribut de vertex)
        //@{
        //! Indexation des �lements de Field.
        enum EField
        {
            eName,          //!< Nom de l'attribut.
            eSize,          //!< Nombre d'�l�ments dans l'attribut (e.g. iNormal : 3).
            eType,          //!< Type OpengGL des �l�ments de l'attribut.
            eByteCount,     //!< Taille totale des donn�es (en octets).
            eData,          //!< Pointeur sur les donn�es en m�moire centrale.
            eVBO            //!< Handle de VBO.
        };
        using Field = std::tuple<String, I32, I32, U32, const void*, U32>;              //!< Un groupe de donn�es pour un sommet.

        const Vector<Field>&        fields() const;                                     //!< Liste des champs d�finis.
        //@}
        //! @name Ajout de champs
        //@{
        void                        addField(Field pData);                              //!< Ajout d'un champ (d�j� d�fini).
        template<class TT, std::size_t TN>
        void                        addField(String pName,
                                             const Vector<std::array<TT, TN>>& pData);  //!< Ajout d'un champ (vecteur de tableaux).
        template<class TT>
        void                        addField(String pName, const Vector<TT>& pData);    //!< Ajout d'un champ (vecteur "simple").
        //@}
        //! @name Liaisons
        //@{
        typedef SharedPtr<Geometry const>       ConstGeometryPtr;                       //!< Pointeur (constant) sur liste de triangle partag�e.
        typedef SharedPtr<Geometry>             GeometryPtr;                            //!< Pointeur sur liste de triangle partag�e.
        typedef SharedPtr<TriangleList const>   ConstTriangleListPtr;                   //!< Pointeur (constant) sur liste de triangle partag�e.
        typedef SharedPtr<TriangleList>         TriangleListPtr;                        //!< Pointeur sur liste de triangle partag�e.
        typedef SharedPtr<SegmentList const>    ConstSegmentListPtr;                    //!< Pointeur (constant) sur liste de segments partag�e.
        typedef SharedPtr<SegmentList>          SegmentListPtr;                         //!< Pointeur sur liste de segments partag�e.

        template<class TField>
        void                        linkField(ConstGeometryPtr pRefGeo);                //!< ... avec un champ externe.
        void                        linkTriangleList(GeometryPtr pRefGeo);              //!< ... avec la liste de triangles d'une autre g�om�trie.
        void                        linkTriangleList(TriangleListPtr pList);            //!< ... avec une liste de triangles externe.
        void                        cloneTriangleList(ConstGeometryPtr pRefGeo);        //!< Clonage de la liste de triangles d'une autre g�om�trie..
        void                        cloneTriangleList(ConstTriangleListPtr pList);      //!< Clonage d'une liste de triangles externe.
        void                        linkSegmentList(GeometryPtr pRefGeo);               //!< ... avec la liste de segments d'une autre g�om�trie.
        void                        linkSegmentList(SegmentListPtr pList);              //!< ... avec une liste de segments externe.
        void                        cloneSegmentList(ConstGeometryPtr pRefGeo);         //!< Clonage de la liste de segments d'une autre g�om�trie..
        void                        cloneSegmentList(ConstSegmentListPtr pList);        //!< Clonage d'une liste de segments externe.
        //@}
        //! @name Gestion des flags
        //@{
        I32                         flags() const;                                      //!< Acc�s aux flags.
        bool                        testFlags(EFlags pFlags) const;                     //!< Test de pr�sence d'un (ou de) flag(s).

    protected:

 static bool                        testFlags(EFlags pFlags, I32 pRefFlags);            //!< Test de pr�sence d'un (ou de) flag(s).
        void                        setFlags(I32 pFlags);                               //!< Etablissements des flags.
        I32                         autoFlags() const;                                  //!< "Flags automatiques".
        bool                        testAutoFlags(EFlags pAutoFlags) const;             //!< Tests sur les flags "automatiques".
        void                        setAutoFlags(I32 pAutoFlags);                       //!< D�finition des "flags automatiques".
        //@}
        //! @name Autres constructeurs
        //@{
        explicit                    Geometry();                                         //!< Constructeur par d�faut.
        explicit                    Geometry(PAK::FileHdl pFileHdl);                    //!< Constructeur (chargement diff�r�).
        //@}
        //! @name Gestion des donn�es
        //@{
        void                        swap(Geometry& pLoadedGeometry);                    //!< Permutation des donn�es.
virtual void                        store(PAK::Saver& pSaver) const;                    //!< Sauvegarde des donn�es.
        //@}
        //! @name D�finition des donn�es
        //@{
        Vector<Vertex3>&            vertices3();                                        //!< Coordonn�es spatiales.
        Vector<Normal>&             normals();                                          //!< Normales.
        Vector<RGBA>&               colors4();                                          //!< Couleurs (RGBA).
        Vector<TexCoord0>&          texCoords0();                                       //!< Coordonn�es pour la 1�re texture.
        Vector<SkinCoord>&          skinCoords();                                       //!< Coordonn�es de m�lange.

        Vector<Vertex4>&            vertices4();                                        //!< Coordonn�es spatiales homog�nes.
        Vector<RGB>&                colors3();                                          //!< Couleurs (RGB).
        Vector<TexCoord1>&          texCoords1();                                       //!< Coordonn�es pour la 2e texture.
        Vector<FogCoord>&           fogCoords();                                        //!< Coordonn�es de brouillard.

        void                        pushTriangle(const Triangle& pTriangle);            //!< Empilement d'un triangle.
        void                        pushSegment(const Segment& pSegment);               //!< Empilement d'un segment.

        void                        setVertexCount(U16 pVertexCount);                   //!< D�finition du nombre (maximum) de vertex.
        void                        setTriangleCount(U16 pTriangleCount);               //!< D�finition du nombre (total) de triangles.
        void                        setSegmentCount(U16 pSegmentCount);                 //!< D�finition du nombre (total) de segment.
        //@}

    private:

        template<class TT>
 static I32                         glType();                                           //!< Enum�ration d'un type OpenGL.

        FORBID_COPY(Geometry)
        PIMPL()
    };

    using   GeometryRsc = RSC::Resource<Geometry>;          //!< Ressource g�om�trie.
    using   GeometryMgr = RSC::ResourceMgr<GeometryRsc>;    //!< Gestionnaire de g�om�tries.
    using   Key         = GeometryMgr::TKey;                //!< Clef pour une GEO::Geometry.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "BaseGeometry.inl"

#endif  // De GEO_BASEGEOMETRY_HH
