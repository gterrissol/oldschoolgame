/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_AABBOX_HH
#define GEO_AABBOX_HH

#include "GEOmetry.hh"

/*! @file GraphX/GEOmetry/AABBox.hh
    @brief En-t�te de la classe GEO::Geometry.
    @author @ref Guillaume_Terrissol
    @date 7 Mai 2009 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/PImpl.hh"
#include "MATH/MATH.hh"
#include "STL/SharedPtr.hh"

namespace GEO
{
    /*! @brief Bo�te englobante align�es aux axes.
        @version 0.3
        @ingroup GEOmetry

     */
    class AABBox : public MEM::Auto<AABBox>
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef WeakPtr<Geometry const> GeometryPtr;    //!< Pointeur sur g�om�trie.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    AABBox(GeometryPtr pGeometry);      //!< Constructeur.
                    ~AABBox();                          //!< Destructeur.
        //@}
        //! @name Informations
        //@{
        MATH::V3    center() const;                     //!< Centre.
        MATH::V3    min() const;                        //!< Coordonn�es minimales.
        MATH::V3    max() const;                        //!< Coordonn�es maximales.
        MATH::V3    dimensions() const;                 //!< Dimensions.
        //@}

    private:

        PIMPL()
    };
}

#endif  // De GEO_AABBOX_HH
