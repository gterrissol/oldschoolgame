/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_ERRMSG_HH
#define GEO_ERRMSG_HH

/*! @file GraphX/GEOmetry/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module GEO.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace GEO
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kAlreadyDefinedField;
    extern  ERR::String kAlreadyDefinedVBO;
    extern  ERR::String kAutomaticFlagActivated;
    extern  ERR::String kGeometryMustExist;
    extern  ERR::String kInvalidGeometry;
    extern  ERR::String kInvalidGeometrySize;
    extern  ERR::String kRangeCheckError;
    extern  ERR::String kSegmentListNotOptimizedAsExpected;
    extern  ERR::String kTooManyElements;
    extern  ERR::String kTriangleAndSegmentListsBothEmpty;
    extern  ERR::String kTriangleListNotOptimizedAsExpected;
    extern  ERR::String kUndefinedField;
    extern  ERR::String kUndefinedSegmentList;
    extern  ERR::String kUndefinedTriangleList;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De GEO_ERRMSG_HH
