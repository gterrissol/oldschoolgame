/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_LIST_TCC
#define GEO_LIST_TCC

#include "List.hh"

/*! @file GraphX/GEOmetry/List.tcc
    @brief M�thodes inline des classes GEO::List & GEO::OptimizedList.
    @author @ref Guillaume_Terrissol
    @date 22 Mars 2008 - 20 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace GEO
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GEO::List<TE>.
        @version 1.0
     */
    template<class TE>
    class List<TE>::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
        Private();                  //!< Constructeur par d�faut.
        Private(U16 pElementCount); //!< Constructeur avec r�servation m�moire
        //@}
        //! @name Donn�es
        //@{
        Vector<TE>  mIndices;       //!< Liste des �l�ments [de r�f�rence].
        //@}
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    template<class TE>
    List<TE>::Private::Private() : mIndices() { }


    /*! Constructeur &vec r�servation m�moire.
        @param pElementCount Nombre d'�l�ments � pr�voir pour la liste
     */
    template<class TE>
    List<TE>::Private::Private(U16 pElementCount) : mIndices()
    {
        mIndices.reserve(pElementCount);
    }


//------------------------------------------------------------------------------
//                 Liste d'El�ments : Constructeur & Destructeur
//------------------------------------------------------------------------------


    /*! Constructeur par d�faut.
     */
    template<class TE>
    List<TE>::List()
        : pthis()
    { }


    /*! Construit une liste (vide) d'�l�ments.
        @param pElementCount Nombre d'�l�ments total que contiendra la liste, une fois remplie
     */
    template<class TE>
    List<TE>::List(U16 pElementCount)
        : pthis(pElementCount)
    { }


    /*! Destructeur.
     */
    template<class TE>
    List<TE>::~List() { }


//------------------------------------------------------------------------------
//                    Liste d'El�ments : Gestion des El�ments
//------------------------------------------------------------------------------

    /*! Empile un �l�ment dans la liste.
        @param pElement N-uplet d'indices � empiler
        @note Les �l�ments doivent �tre empil�s dans l'odre souhait� d'affichage
        @note Il faudrait que le nombre total d'�l�ments soit convenablement d�fini � la cr�ation d'une
        instance (voir List(U16))
     */
    template<class TE>
    inline void List<TE>::push(const TE& pElement)
    {
        WARNING_IF(pthis->mIndices.capacity() <= pthis->mIndices.size(), kTooManyElements)

        pthis->mIndices.push_back(pElement);
    }


//------------------------------------------------------------------------------
//                  Liste d'El�ments : Permutation des Donn�es
//------------------------------------------------------------------------------

    /*! Permute sa liste d'�l�ments avec celle d'une autre instance.
        @param pElements Instance avec laquelle permuter les donn�es
     */
    template<class TE>
    inline void List<TE>::swap(List<TE>& pElements)
    {
        pthis->mIndices.swap(pElements.pthis->mIndices);
    }


//------------------------------------------------------------------------------
//                 Liste d'El�ments : Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par copie
     */
    template<class TE>
    List<TE>::List(const List& pList)
        : pthis(U16(pList.pthis->mIndices.size()))
    {
        pthis->mIndices = pList.pthis->mIndices;
    }


//------------------------------------------------------------------------------
//                 Liste d'El�ments : Comportement � sp�cialiser
//------------------------------------------------------------------------------

    /*! Constructeur virtuel.
        @return Une nouvelle liste d'�lements, copie conforme de l'instance
        @sa clone()
     */
    template<class TE>
    typename List<TE>::ListPtr List<TE>::cloneInstance() const
    {
        return STL::makeShared<List<TE>>(*this);
    }


    /*! @param pResolution Pourcentage d'�l�ments � afficher
        @return Un tableau contenant les �l�ments � afficher (n-uplets d'indices de vertex)
        @sa get(F32)
     */
    template<class TE>
    const Vector<TE>& List<TE>::computeList(F32) const
    {
        return pthis->mIndices;
    }


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GEO::OptimizedList<TE>.
        @version 1.0
     */
    template<class TE>
    class OptimizedList<TE>::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
        Private(U16 pVertexCount);                 //!< Constructeur.
        //@}
        //! @name Multi-r�solution
        //@{
        mutable Vector<TE>  mCurrentIndices;        //!< Segments � afficher.
        Vector<U16>         mSubstitutes;           //!< Indices de substitution.
        mutable F32         mPreviousResolution;    //!< R�solution pr�c�dente.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pVertexCount  
     */
    template<class TE>
    OptimizedList<TE>::Private::Private(U16 pVertexCount)
        : mCurrentIndices()
        , mSubstitutes()
        , mPreviousResolution(F32(1.0F))
    {
        mSubstitutes.reserve(pVertexCount);
    }


//------------------------------------------------------------------------------
//            Liste d'El�ments Optimis�e : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pElementCount 
        @param pVertexCount  
     */
    template<class TE>
    OptimizedList<TE>::OptimizedList(U16 pElementCount, U16 pVertexCount)
        : List<TE>(pElementCount)
        , pthis(pVertexCount)
    { }


    /*! Destructeur.
     */
    template<class TE>
    OptimizedList<TE>::~OptimizedList() { }


//------------------------------------------------------------------------------
//        Liste d'El�ments Optimis�e : Gestion des Elements & des Vertex
//------------------------------------------------------------------------------

    /*! Empile un substituant dans la liste.
        @param pIndex 
     */
    template<class TE>
    inline void OptimizedList<TE>::push(U16 pIndex)
    {
        pthis->mSubstitutes.push_back(pIndex);
    }


    /*! @param pIndex Indice du vertex dont on cherche le subsitutant
        @return L'indice du vertex substituant le <i>pIndex</i>i�me
        @note Aucun contr�le n'est effectu� sur <i>pIndex</i>
     */
    template<class TE>
    inline U16 OptimizedList<TE>::substitute(U16 pIndex) const
    {
        return pthis->mSubstitutes[pIndex];
    }


//------------------------------------------------------------------------------
//                Liste d'El�ments Optimis�e : Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par copie
     */
    template<class TE>
    OptimizedList<TE>::OptimizedList(const OptimizedList& pList)
        : List<TE>(pList)
        , pthis(k0UW)
    {
        pthis->mCurrentIndices      = pList.pthis->mCurrentIndices;
        pthis->mSubstitutes         = pList.pthis->mSubstitutes;
        pthis->mPreviousResolution  = pList.pthis->mPreviousResolution;
    }


//------------------------------------------------------------------------------
//             Liste d'El�ments Optimis�e : Comportement Sp�cialis�
//------------------------------------------------------------------------------

    /*! Constructeur virtuel.
        @return Une nouvelle liste d'�l�ments optimis�e, copie conforme de l'instance
     */
    template<class TE>
    typename OptimizedList<TE>::ListPtr OptimizedList<TE>::cloneInstance() const
    {
        return STL::makeShared<OptimizedList<TE>>(*this);
    }
}

#endif  // De GEO_LIST_TCC
