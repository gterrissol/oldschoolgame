/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_ALL_HH
#define GEO_ALL_HH

/*! @file GraphX/GEOmetry/All.hh
    @brief Interface publique du module @ref GEOmetry.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO   //! G�om�trie.
{
    /*! @namespace GEO
        @version 0.5

        ...
        @todo Revoir toute la doc du module
     */

    /*! @defgroup GEOmetry GEOmetry : G�om�tries
        <b>namespace</b> GEO & @ref GEO_Fields.
     */

    /*! @defgroup GEO_Fields Types des donn�es des g�om�tries
        Triangles, segments et coordonn�es diverses.
     */
}

#include "Fields.hh"
#include "List.hh"
#include "List.tcc"
#include "BaseGeometry.hh"

#endif  // De GEO_ALL_HH
