/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_FIELDS_HH
#define GEO_FIELDS_HH

#include "GEOmetry.hh"

/*! @file GraphX/GEOmetry/Fields.hh
    @brief Structures du module GEO.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 8 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/CoLoR.hh"
#include "MEMory/Array.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

#include "Enum.hh"

namespace GEO
{
    /*! @brief Triangle (triplet d'indices de vertex).
        @version 1.01
        @ingroup GEO_Fields

        Les indices d'un triangle r�f�rencent les vertex d'une instance de GEO::Geometry (� charge du
        programmeur de ne pas faire n'importe quoi : le triangle et la g�om�trie doivent appartenir � la
        m�me instance d' APP::Appearance).
     */
    class Triangle : public MEM::Auto<Triangle>
    {
    public:
        //! @name Constructeurs
        //@{
                            Triangle();                         //!< ... par d�faut.
                            Triangle(U16 pA, U16 pB, U16 pC);   //!< ... par des indices.
                            Triangle(const U16 pABC[3]);        //!< ... par un tableau d'indices.
        //@}
        //! @name Acc�s direct aux indices
        //@{
        inline  const U16&  operator[](size_t pN) const;        //!< ... constant.
        inline  U16&        operator[](size_t pN);              //!< ... par r�f�rence.
        //@}

    private:

        MEM::Array<U16, 3>  mIndices;                           //!< Indices.
    };


    //! @name Operateurs
    //@{
            bool    operator==(const Triangle& pL, const Triangle& pR); //!< ... d'�galit�.
    inline  bool    operator!=(const Triangle& pL, const Triangle& pR); //!< ... d'in�galit�.
    //@}


    /*! @brief Segment (duplet d'indices de vertex).
        @version 1.0
        @ingroup GEO_Fields

        Les indices d'un segment r�f�rencent les vertex d'une instance de GEO::Geometry (� charge du
        programmeur de ne pas faire n'importe quoi : le segment et la g�ometrie doivent appartenir � la
        m�me instance d' APP::Appearance).
     */
    class Segment : public MEM::Auto<Segment>
    {
    public:
        //! @name Constructeurs
        //@{
                            Segment();                      //!< ... par d�faut.
                            Segment(U16 pA, U16 pB);        //!< ... par des indices.
                            Segment(const U16 pAB[2]);      //!< ... par un tableau d'indices.
        //@}
        //! @name Acc�s direct aux indices
        //@{
                const U16&  operator[](size_t pN) const;    //!< ... constant.
        inline  U16&        operator[](size_t pN);          //!< ... par r�f�rence.
        //@}

    private:

        MEM::Array<U16, 2>  mIndices;                       //!< Indices.
    };


    //! @name Operateurs
    //@{
            bool    operator==(const Segment& pL, const Segment& pR);   //!< ... d'�galit�.
    inline  bool    operator!=(const Segment& pL, const Segment& pR);   //!< ... d'in�galit�.
    //@}


    /*! @brief Objet champ g�n�rique.
        @version 1.02
        @ingroup GEO_Fields

        Cette classe permet de d�finir simplement les diff�rents champs qui composent une g�om�trie
        (vertex, normale,...), en se basant sur des templates (<i>TC</i><small>ount</small> == nombre de
        coordonn�es).
        @note <i>TS</i><small>pare</small> permet juste de d�clarer, via des <b>typedef</b>s, des types
        diff�rents pour des Fields ayant le m�me nombre de champs, mais des r�les diff�rents (voir
        Vertex3 & Normal,...)
     */
    template<int TC, int TS = 0>
    class Field : public MEM::Auto<Field<TC, TS>>
    {
    public:
        //! @name Constructeurs
        //@{
        inline              Field();                        //!< ... par d�faut.
        inline              Field(const F32 pCoords[TC]);   //!< ... par un tableau de coordonn�es.
        //@}
        //! @name Acc�s direct aux �l�ments
        //@{
        inline  const F32&  operator[](size_t pN) const;    //!< ... constant.
        inline  F32&        operator[](size_t pN);          //!< ... par r�f�rence.
        //@}

    private:

        typename MEM::Array<F32, TC>    mCoords;            //!< Tableau des coordonn�es.
    };


    //! @name Operateurs
    //@{
    template<int TC, int TS>
            bool    operator==(const Field<TC, TS>& pL, Field<TC, TS>& pR); //!< ... d'�galit�.
    template<int TC, int TS>
    inline  bool    operator!=(const Field<TC, TS>& pL, Field<TC, TS>& pR); //!< ... d'in�galit�.
    //@}


    /*! @brief Poids de m�lange pour le skinning.
        @version 0.18
        @ingroup GEO_Fields

        L'impl�mentation du skinning requiert que tout vertex d'un objet animable poss�de des "poids de
        blending" (au plus 3).<br>
        A chacun de ces poids de blending est associ� un indice, qui r�f�rence l'os (BK::Bone) sur
        lequel s'applique ce poids.
     */
    class SkinCoord : public MEM::Auto<SkinCoord>
    {
    public:

            SkinCoord();
        U8  count() const;                  //!< Nombre de poids.

    private:

        U8                  mWeightCount;   //!< Nombre de poids.
        MEM::Array<U8, 3>   mIndices;       //!< Indices des vertex � m�langer.
        MEM::Array<F32, 3>  mWeights;       //!< Poids de m�lange.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Fields.inl"

#endif  // De GEO_FIELDS_HH
