/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Fields.hh"

/*! @file GraphX/GEOmetry/Fields.cc
    @brief M�thodes (non-inline) de la classe GEO::OptimizedList.
    @author @ref Guillaume_Terrissol
    @date 20 Mars 2008 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO
{
//------------------------------------------------------------------------------
//                           Triangle : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : tous les indices sont initialis�s � 0 (le triangle est d�g�n�r�, mais valide).
     */
    Triangle::Triangle()
        : mIndices(k0UW, k0UW, k0UW)
    { }


    /*! Constructeur � partir d'indices.
        @param pA Indice du premier sommet
        @param pB Indice du deuxi�me sommet
        @param pC Indice du troisi�me sommet
        @note Le triangle doit �tre orient� <b>CCW</b>
     */
    Triangle::Triangle(U16 pA, U16 pB, U16 pC)
        : mIndices(pA, pB, pC)
    { }


    /*! Constructeur � partir d'un tableau d'indices.
        @param pABC Indices des 3 sommets du triangle (dans le bon ordre; le triangle doit �tre orient�
        <b>CCW</b>)
     */
    Triangle::Triangle(const U16 pABC[3])
        : mIndices(pABC[0], pABC[1], pABC[2])
    { }


//------------------------------------------------------------------------------
//                        Triangle : Op�rateurs d'Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup GEO_Fields
        @param pL Premier triangle de la comparaison
        @param pR Second triangle de la comparaison
        @return VRAI si les triangles sont identiques (m�mes indices, �ventuellement ordonn�s
        diff�remment, mais avec respect de l'orientation), FAUX sinon
     */
    bool operator==(const Triangle& pL, const Triangle& pR)
    {
        U16 l0 = U16(0), l1 = U16(1), l2 = U16(2);

        return ((pL[l0] == pR[l0]) && (pL[l1] == pR[l1]) && (pL[l2] == pR[l2])) ||
               ((pL[l0] == pR[l1]) && (pL[l1] == pR[l2]) && (pL[l2] == pR[l0])) ||
               ((pL[l0] == pR[l2]) && (pL[l1] == pR[l0]) && (pL[l2] == pR[l1]));
    }


//------------------------------------------------------------------------------
//                            Segment : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : tous les indices sont initialis�s � 0 (le segment est nul, mais valide).
     */
    Segment::Segment()
        : mIndices(k0UW, k0UW)
    { }


    /*! Constructeur � partir d'indices.
        @param pA Indice du premier sommet
        @param pB Indice du second sommet
     */
    Segment::Segment(U16 pA, U16 pB)
        : mIndices(pA, pB)
    { }


    /*! Constructeur � partir d'un tableau d'indices.
        @param pAB Indices des 2 sommets du segment
     */
    Segment::Segment(const U16 pAB[2])
        : mIndices(pAB[0], pAB[1])
    { }


//------------------------------------------------------------------------------
//                        Segment : Op�rateurs d'Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup GEO_Fields
        @param pL Premier segment de la comparaison
        @param pR Second segment de la comparaison
        @return VRAI si les segments sont identiques (m�mes indices, �ventuellement ordonn�s
        diff�rement), FAUX sinon
     */
    bool operator==(const Segment& pL, const Segment& pR)
    {
        U16 l0 = U16(0), l1 = U16(1);

        return ((pL[l0] == pR[l0]) && (pL[l1] == pR[l1])) ||
               ((pL[l0] == pR[l1]) && (pL[l1] == pR[l0]));
    }


//------------------------------------------------------------------------------
//                                  Skin Coord
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    SkinCoord::SkinCoord()
        : mWeightCount(0)
    { }


    /*! @return Le ombre de poids de blend
     */
    U8 SkinCoord::count() const
    {
        return mWeightCount;
    }
}
