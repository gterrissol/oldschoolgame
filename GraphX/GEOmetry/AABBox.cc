/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "AABBox.hh"

/*! @file GraphX/GEOmetry/AABBox.cc
    @brief M�thodes (non-inline) de la classe GEO::Geometry.
    @author @ref Guillaume_Terrissol
    @date 7 Mai 2009 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/V3.hh"
#include "STL/Limits.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "BaseGeometry.hh"
#include "Fields.hh"

namespace GEO
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GEO::AABBox.
        @version 0.4

     */
    class AABBox::Private : public MEM::OnHeap
    {
    public:

                Private(GeometryPtr pGeometry);

        void    compute();

        GeometryPtr mGeometry;
        MATH::V3    mMin;
        MATH::V3    mMax;
        bool        mInitialized;
    };
    

//------------------------------------------------------------------------------
//                               P-Impl : M�thodes
//------------------------------------------------------------------------------

    /*!
     */
    AABBox::Private::Private(GeometryPtr pGeometry)
        : mGeometry(pGeometry)
        , mMin(std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max())
        , mMax(- mMin)
        , mInitialized(false)
    { }


    /*!
     */
    void AABBox::Private::compute()
    {
        const auto  lGeometry = mGeometry.lock();

        if ((lGeometry != nullptr) && (0 < lGeometry->vertexCount()))
        {
            const Vector<Vertex3>&  lVertices   = lGeometry->vertices3();

            for(auto lVertexI = lVertices.begin(); lVertexI != lVertices.end(); ++lVertexI)
            {
                const Vertex3&  lV  = *lVertexI;
                for(size_t i = 0; i < 3; ++i)
                {
                    if      (lV[i] < mMin[i])
                    {
                        mMin[i] = lV[i];
                    }
                    else if (mMax[i] < lV[i])
                    {
                        mMax[i] = lV[i];
                    }
                }
            }
        }
        else
        {
            mMin = mMax = MATH::kNullV3;
        }

        mInitialized = true;
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pGeometry G�om�trie dont d�terminer la bo�te englobante
     */
    AABBox::AABBox(GeometryPtr pGeometry)
        : pthis(pGeometry)
    { }


    /*! Destructeur.
     */
    AABBox::~AABBox() { }


//------------------------------------------------------------------------------
//                           Informations
//------------------------------------------------------------------------------

    /*!
     */
    MATH::V3 AABBox::center() const
    {
        if (!pthis->mInitialized)
        {
            pthis->compute();
        }

        return (pthis->mMax + pthis->mMin) * F32(0.5F);
    }


    /*!
     */
    MATH::V3 AABBox::min() const
    {
        if (!pthis->mInitialized)
        {
            pthis->compute();
        }

        return pthis->mMin;
    }


    /*!
     */
    MATH::V3 AABBox::max() const
    {
        if (!pthis->mInitialized)
        {
            pthis->compute();
        }

        return pthis->mMax;
    }


    /*!
     */
    MATH::V3 AABBox::dimensions() const
    {
        if (!pthis->mInitialized)
        {
            pthis->compute();
        }

        return (pthis->mMax - pthis->mMin);
    }
}
