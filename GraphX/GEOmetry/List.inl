/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/GEOmetry/List.inl
    @brief M�thodes inline des classes GEO::List & GEO::OptimizedList.
    @author @ref Guillaume_Terrissol
    @date 23 Novembre 2005 - 7 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO
{
//------------------------------------------------------------------------------
//                       Liste d'El�ments : "Constructeur"
//------------------------------------------------------------------------------

    /*! Constructeur virtuel.
        @return Une nouvelle liste d'�l�ments, copie conforme de l'instance
        @note R�impl�mentez pt_CloneInstance() en cas de d�rivation
     */
    template<class TE>
    inline typename List<TE>::ListPtr List<TE>::clone() const
    {
        return cloneInstance();
    }


//------------------------------------------------------------------------------
//                    Liste d'El�ments : Gestion des El�ments
//------------------------------------------------------------------------------

    /*! @param pResolution Pourcentage d'�l�ments � afficher
        @return Un tableau contenant les �l�ments � afficher (n-uplets d'indices de vertex)
        @note R�impl�mentez computeList(F32) pour effectuer la simplication
     */
    template<class TE>
    inline const Vector<TE>& List<TE>::get(F32 pResolution) const
    {
        return computeList(pResolution);
    }
}
