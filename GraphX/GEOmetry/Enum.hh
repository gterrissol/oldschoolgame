/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GEO_ENUM_HH
#define GEO_ENUM_HH

/*! @file GraphX/GEOmetry/Enum.hh
    @brief Enum�rations du module GEO.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 22 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO
{
    /*! @brief Flags pour la pr�sences des champs dans une instance de cl_Geometry.
        @note Les premi�res valeurs de l'�num�ration correspondent aux cas les plus courants (a priori)
     */
    enum EFlags
    {
        eNoFlag     = 0x00000000,   //!< Pas de champ.
        // Champs par d�faut.
        eVertex3    = 0x00000001,   //!< Vertex 3D.
        eNormal     = 0x00001000,   //!< Normale.
        eRGBA       = 0x00000020,   //!< Couleur RGBA.
        eTexCoord0  = 0x00000100,   //!< Coordonn�es de la [premi�re] texture.
        eSkinCoord  = 0x00010000,   //!< Poids de <i>blending</i>.
        // "Alternatives".
        eVertex4    = 0x00000002,   //!< Vertex 4D.
        eRGB        = 0x00000010,   //!< Couleur RGB.
        eTexCoord1  = 0x00000300,   //!< == E_TexCoord2 "seul" + E_TexCoord1.
        eFogCoord   = 0x00100000,   //!< Coordonn�e de brouillard.
        // Liste de triangles ou de segments.
        eTriangles  = 0x01000000,   //!< Liste de triangles.
        eTOM        = 0x03000000,   //!< Liste de triangles optimis�e (Totally Ordered Mesh).
        eLines      = 0x04000000,   //!< Liste de segments.
        eTOC        = 0x0C000000    //!< Liste de segments optimis�e (Totally Ordered Curve).
    };
}

#endif  // De GEO_ENUM_HH
