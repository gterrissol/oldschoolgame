/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/GEOmetry/Fields.inl
    @brief M�thodes inline des classes <b>champs</b> de <b>GEO::Geometry</b>.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GEO
{
//------------------------------------------------------------------------------
//                      Triangle : Acc�s Direct aux Indices
//------------------------------------------------------------------------------

    /*! @param pN L'index de l'indice � r�cup�rer
        @return Le <i>pN</i>i�me indice de l'instance
     */
    inline const U16& Triangle::operator[](size_t pN) const
    {
        return mIndices[pN];
    }


    /*! @param pN L'index de l'indice � r�cup�rer
        @return Le <i>pN</i>i�me indice de l'instance
     */
    inline U16& Triangle::operator[](size_t pN)
    {
        return mIndices[pN];
    }

    
//------------------------------------------------------------------------------
//                       Triangle : Op�rateurs d'In�galit�
//------------------------------------------------------------------------------

    /*! @ingroup GEO_Fields
        @param pL Premier triangle de la comparaison
        @param pR Second triangle de la comparaison
        @return FAUX si les triangles sont les m�mes (m�mes indices, �ventuellement ordonn�s diff�remment), VRAI sinon
     */
    inline bool operator!=(const Triangle& pL, const Triangle& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                      Segment : Acc�s Direct aux Indices
//------------------------------------------------------------------------------

    /*! @param pN L'index de l'indice � r�cup�rer
        @return Le <i>pN</i>i�me indice de l'instance
     */
    inline const U16& Segment::operator[](size_t pN) const
    {
        return mIndices[pN];
    }


    /*! @param pN L'index de l'indice � r�cup�rer
        @return Le <i>pN</i>i�me indice de l'instance
     */
    inline U16& Segment::operator[](size_t pN)
    {
        return mIndices[pN];
    }

    
//------------------------------------------------------------------------------
//                       Segment : Op�rateurs d'In�galit�
//------------------------------------------------------------------------------

    /*! @ingroup GEO_Fields
        @param pL Premier segment de la comparaison
        @param pR Second segment de la comparaison
        @return FAUX si les segments sont les m�mes (m�mes indices, �ventuellement ordonn�s
        diff�remment), VRAI sinon
     */
    inline bool operator!=(const Segment& pL, const Segment& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                             Field : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : toutes les coordonn�es sont initialis�es � 0.0F.
     */
    template<int TC, int TS>
    inline Field<TC, TS>::Field()
        : mCoords()
    { }


    /*! Constructeur � partir d'un tableau de coordonn�es.
        @param pCoords Coordonn�es du champ (dans le bon ordre).
     */
    template<int TC, int TS>
    inline Field<TC, TS>::Field(const F32 pCoords[TC])
        : mCoords(pCoords)
    { }


//------------------------------------------------------------------------------
//                       Field : Acc�s Direct aux El�ments
//------------------------------------------------------------------------------

    /*! @param pN Indice de la coordonn�e � r�cup�rer
        @return La <i>pN</i>i�me coordonn�e de l'instance
     */
    template<int TC, int TS>
    inline const F32& Field<TC, TS>::operator[](size_t pN) const
    {
        return mCoords[pN];
    }


    /*! @param pN Indice de la coordonn�e � r�cup�rer
        @return La <i>pN</i>i�me coordonn�e de l'instance
     */
    template<int TC, int TS>
    inline F32& Field<TC, TS>::operator[](size_t pN)
    {
        return mCoords[pN];
    }

//------------------------------------------------------------------------------
//                       Field : Op�rateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup GEO_Fields
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return VRAI si les 2 champs sont identiques, FAUX sinon
     */
    template<int TC, int TS>
    bool    operator==(const Field<TC, TS>& pL, Field<TC, TS>& pR)
    {
        return (pL.mCoords == pR[0].mCoords);
    }

    /*! @ingroup GEO_Fields
        @param pL 1er op�rande
        @param pR 2nd op�rande
        @return FAUX si les 2 champs sont identiques, VRAI sinon
     */
    template<int TC, int TS>
    inline bool operator!=(const Field<TC, TS>& pL, Field<TC, TS>& pR)
    {
        return !(pL == pR);
    }
}
