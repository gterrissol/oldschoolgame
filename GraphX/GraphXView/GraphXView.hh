/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GXV_GRAPHXVIEW_HH
#define GXV_GRAPHXVIEW_HH

/*! @file GraphX/GraphXView/GraphXView.hh
    @brief En-t�te du module de GXV.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GXV
{
    class EngineViewport;
    class Viewport;
}

#endif  // De GXV_GRAPHXVIEW_HH
