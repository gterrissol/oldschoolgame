/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GXV_ERRMSG_HH
#define GXV_ERRMSG_HH

/*! @file GraphX/GraphXView/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module GXV.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GXV
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kInvalidLayerCount;
    extern  ERR::String kInvalidLayerLevel;
    extern  ERR::String kInvalidRenderer;
    extern  ERR::String kInvalidViewport;
    extern  ERR::String kUninitializedDimensions;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De GXV_ERRMSG_HH
