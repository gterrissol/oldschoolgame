/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Viewport.hh"

/*! @file GraphX/GraphXView/Viewport.cc
    @brief M�thodes (non-inline) de la classe GXV::Viewport.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 26 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <GL/gl.h>

#include "CAMera/BaseCamera.hh"
#include "CoLoR/Colors.hh"
#include "RenDeR/Tree.hh"
#include "RenDeR/Renderer.hh"
#include "SHaDers/Const.hh"
#include "SHaDers/Library.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace GXV
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de GXV::Viewport.
        @version 1.0
     */
    class Viewport::Private : public MEM::OnHeap
    {
    public:

        typedef SharedPtr<RDR::Tree>        RenderTreePtr;  //!< Pointeur sur arbre de rendu.
        typedef SharedPtr<RDR::PickingTree> PickingTreePtr; //!< Pointeur sur arbre de picking.

                            Private();                      //!< Constructeur.
        void                paint();                        //!< Affichage.
        APP::Pickable::Ptr  pick(const UTI::Point& pPoint); //!< Picking.
        //! @name Syst�me de rendu
        //@{
        typedef Vector<RendererPtr>::iterator   Iterator;   //!< It�rateur sur les renderers.

        Vector<RendererPtr>     mRenderers;                 //!< Objets de rendu.
        CameraPtr               mCamera;                    //!< Cam�ra associ�e.
        Vector<RenderTreePtr>   mRenderTrees;               //!< Arbres de rendu.
        Vector<PickingTreePtr>  mPickingTrees;              //!< Arbres de picking.
        SHD::Library::Ptr       mShaders;                   //:< Biblioth�que de shaders.
        I32                     mWidth;                     //!< Largeur du viewport.
        I32                     mHeight;                    //!< Hauteur du viewport.
        bool                    mDefaultEnabledRenderers;   //!< Activation par d�faut des renderers.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Viewport::Private::Private()
        : mRenderers{}
        , mCamera{STL::makeShared<CAM::Camera>()}
        , mRenderTrees{}
        , mPickingTrees{}
        , mShaders{}
        , mWidth{-1}
        , mHeight{-1}
        , mDefaultEnabledRenderers{true}
    {
        auto    lDefault    = SHD::kDefaultProgram;
        auto    lPicking    = SHD::kPickingProgram;
        mRenderTrees.push_back(STL::makeShared<RDR::Tree>(RDR::eFrontOverlay));
        mRenderTrees.push_back(STL::makeShared<RDR::Tree>(RDR::eMainOverlay));
        mRenderTrees.push_back(STL::makeShared<RDR::Tree>(RDR::eMainLayer));
        mRenderTrees.push_back(STL::makeShared<RDR::HighlightTree>(CLR::kYellow, lDefault));
        ASSERT(mRenderTrees.size() == RDR::eLayerCount, kInvalidLayerCount)

        mPickingTrees.push_back(STL::makeShared<RDR::PickingTree>(RDR::eFrontOverlay, lPicking));
        mPickingTrees.push_back(STL::makeShared<RDR::PickingTree>(RDR::eMainOverlay, lPicking));
        mPickingTrees.push_back(STL::makeShared<RDR::PickingTree>(RDR::eMainLayer, lPicking));
        // NB: Les mod�les de surbrillance (en fil de fer) ne sont pas pickables.

        ASSERT(mPickingTrees.size() == RDR::eLayerCount - 1, kInvalidLayerCount)

        for(auto& lTree : mRenderTrees)  { lTree->setCamera(mCamera); }
        for(auto& lTree : mPickingTrees) { lTree->setCamera(mCamera); }
    }


//------------------------------------------------------------------------------
//                           P-Impl : Autres M�thodes
//------------------------------------------------------------------------------

    /*!
     */
    void Viewport::Private::paint()
    {
        for(U32 lT = k0UL; lT < RDR::eLayerCount; ++lT)
        {
            mRenderTrees[lT]->reset();
        }

        // Y a-t-il quelque chose � afficher ?
        for(U32 lR = k0UL; lR < mRenderers.size(); ++lR)
        {
            SharedPtr<RendererPtr::element_type>    lRenderer = mRenderers[lR].lock();

            if (lRenderer != nullptr)
            {
                RDR::ELayers    lLevel = lRenderer->layer();
                ASSERT(lLevel < RDR::eLayerCount, kInvalidLayerLevel)

                lRenderer->dump(mRenderTrees[lLevel]);
            }
        }
        mRenderers.erase(std::remove_if(mRenderers.begin(),
                                        mRenderers.end(),
                                        std::mem_fun_ref(&RendererPtr::expired)),
                         mRenderers.end());

        for(U32 lT = k0UL; lT < RDR::eLayerCount; ++lT)
        {
            mRenderTrees[lT]->flush();
        }
    }


    /*!
     */
    APP::Pickable::Ptr Viewport::Private::pick(const UTI::Point& pPoint)
    {
        for(U32 lT = k0UL; lT < RDR::eLayerCount - 1; ++lT)
        {
            mPickingTrees[lT]->reset();
        }

        for(U32 lR = k0UL; lR < mRenderers.size(); ++lR)
        {
            SharedPtr<RendererPtr::element_type>    lRenderer = mRenderers[lR].lock();

            if (lRenderer != nullptr)
            {
                RDR::ELayers    lLevel = lRenderer->layer();
                if (lLevel < RDR::eLayerCount - 1) // Rappel : la couche de surbrillance ne participe pas au picking.
                {
                    lRenderer->dump(mPickingTrees[lLevel]);
                }
            }
        }
        mRenderers.erase(std::remove_if(mRenderers.begin(),
                                        mRenderers.end(),
                                        std::mem_fun_ref(&RendererPtr::expired)),
                         mRenderers.end());

        APP::Pickable::Ptr  lPicked{};
        for(U32 lT = k0UL; (lT < RDR::eLayerCount - 1) && lPicked.expired(); ++lT)
        {
            mPickingTrees[lT]->flush();
            lPicked = mPickingTrees[lT]->picked(pPoint);
        }

        return lPicked;
    }


    /*! @return Par d�faut, un pointeur nul.
     */
    INP::Controller::Ptr Viewport::makeController() const
    {
        return {};
    }


//------------------------------------------------------------------------------
//                          Constructeur et Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Viewport::Viewport()
        : VIEW::Viewport{}
        , pthis{}
    { }


    /*! Destructeur.
     */
    Viewport::~Viewport() { }


//------------------------------------------------------------------------------
//                       Liaisons avec le Syt�me de Rendu
//------------------------------------------------------------------------------

    /*! Lie un objet de rendu au viewport.
        @param pRenderer Objet encapsulant la donn�e � rendre
     */
    void Viewport::attach(RendererPtr pRenderer)
    {
        ASSERT_EX(!pRenderer.expired(), kInvalidRenderer, return;)

        Private::Iterator lIter = std::find(pthis->mRenderers.begin(), pthis->mRenderers.end(), pRenderer);

        if (lIter == pthis->mRenderers.end())
        {
            // Le renderer n'�tait pas encore attach� � ce viewport : il l'est.
            pRenderer.lock()->enable(pthis->mDefaultEnabledRenderers);
            pthis->mRenderers.push_back(pRenderer);
            pRenderer.lock()->attach(camera());
        }
        // Sinon, rien n'est fait.
    }


    /*! D�lie un objet de rendu du viewport.
        @param pRenderer Objet encapsulant la donn�e � rendre
     */
    void Viewport::detach(RendererPtr pRenderer)
    {
        ASSERT_EX(!pRenderer.expired(), kInvalidRenderer, return;)

        Private::Iterator lIter = std::find(pthis->mRenderers.begin(), pthis->mRenderers.end(), pRenderer);

        if (lIter != pthis->mRenderers.end())
        {
            // Le renderer �tait bien attach� � ce viewport : il est d�li�.
            std::swap(*lIter, pthis->mRenderers.back());
            pthis->mRenderers.pop_back();
            // Plus de cam�ra attach�e.
            pRenderer.lock()->attach(CameraPtr());

            // Pas de rafra�chissement forc�.
        }
    }


    /*!
     */
    Viewport::CameraPtr Viewport::camera() const
    {
        return pthis->mCamera;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Viewport::initShaders(PAK::FileHdl pHdl)
    {
        PAK::Loader lLoader{pHdl};
        pthis->mShaders = STL::makeShared<SHD::Library>(lLoader);

        for(auto& lTree : pthis->mRenderTrees)  { lTree->bind(pthis->mShaders); lTree->setCamera(pthis->mCamera); }
        for(auto& lTree : pthis->mPickingTrees) { lTree->bind(pthis->mShaders); lTree->setCamera(pthis->mCamera); }
    }

    /*!
     */
    void Viewport::freeShaders()
    {
        pthis->mShaders.reset();
    }

    /*!
     */
    SHD::Library::Ptr Viewport::shaders()
    {
        return pthis->mShaders;
    }

    /*! Picke sur une position donn�e.
        @param pPoint Position sur l'�cran sur laquelle tenter une s�lection OpenGL.
     */
    APP::Pickable::Ptr Viewport::pick(const UTI::Point& pPoint)
    {
        return pthis->pick(pPoint);
    }


//------------------------------------------------------------------------------
//                              Liste des renderers
//------------------------------------------------------------------------------

    /*! 
     */
    Viewport::RendererPtr Viewport::getRenderer(U32 pN)
    {
        return pthis->mRenderers[pN];
    }


    /*! @return Le nombre de renderers attach�s � ce viewport
     */
    U32 Viewport::rendererCount() const
    {
        return U32(pthis->mRenderers.size());
    }


    /*! @param pEnabled Si VRAI, active tous les renderers; si FAUX, les d�sactive tous
     */
    void Viewport::enableRenderers(bool pEnabled)
    {
        for(auto lRendererI = pthis->mRenderers.begin(); lRendererI != pthis->mRenderers.end(); ++lRendererI)
        {
            lRendererI->lock()->enable(pEnabled);
        }
    }


    /*!
     */
    void Viewport::defaultEnabledRenderers(bool pEnabled)
    {
        pthis->mDefaultEnabledRenderers = pEnabled;
    }


//------------------------------------------------------------------------------
//                                   Affichage
//------------------------------------------------------------------------------

    /*! Efface le pr�c�dent contenu de l'�cran.
     */
    void Viewport::doClearing()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    }


    /*! Pr�pare le rendu.
     */
    void Viewport::preparePainting()
    {
        ASSERT_EX((pthis->mWidth != -1) && (pthis->mHeight != -1), kUninitializedDimensions, return;)

        camera()->setForRender(F32(pthis->mWidth) / F32(pthis->mHeight));
    }


    /*! Rend la sc�ne.
     */
    void Viewport::doPainting()
    {
        pthis->paint();
    }


//------------------------------------------------------------------------------
//                                Gestion avanc�e
//------------------------------------------------------------------------------

    /*! Etablit les param�tres initiaux du viewport via le Graphic Device.
     */
    void Viewport::doInitialize()
    {
        // Couleurs.
        glShadeModel(GL_SMOOTH);
        const GLfloat kFogColor[] = { 0.5F, 0.703F, 0.828F, 1.0F };
        glClearColor(kFogColor[0], kFogColor[1], kFogColor[2], kFogColor[3]);

        // Z-Buffer & Culling.
        glDepthFunc(GL_LEQUAL);
        glFrontFace(GL_CCW);
        glCullFace(GL_BACK);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // Stencil buffer.
        glClearStencil(RDR::eMainLayer);

        // Activations.
        glEnable(GL_LEQUAL);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_NORMALIZE);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    }


    /*! Redimensionne le viewport (typiquement : apr�s un redimensionnement).
     */
    void Viewport::doResize(I32 pWidth, I32 pHeight)
    {
        pthis->mWidth  = pWidth;
        pthis->mHeight = pHeight;

        glViewport(0, 0, pthis->mWidth, pthis->mHeight);
    }


    /*! @return Le ratio d'affichage
     */
    F32 Viewport::getRatio() const
    {
        return F32(pthis->mWidth) / F32(pthis->mHeight);
    }
}
