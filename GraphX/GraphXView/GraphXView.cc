/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GraphXView.hh"

/*! @file GraphX/GraphXView/GraphXView.cc
    @brief D�finitions diverses du module GXV.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace GXV
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidLayerCount,         "Nombre de couches invalide.",  "Invalid layer count.")
    REGISTER_ERR_MSG(kInvalidLayerLevel,         "Niveau de couche invalide.",   "Invalid layer level.")
    REGISTER_ERR_MSG(kInvalidRenderer,           "Renderer invalide.",           "Invalid renderer.")
    REGISTER_ERR_MSG(kInvalidViewport,           "Viewport invalide.",           "Invalid viewport.")
    REGISTER_ERR_MSG(kUninitializedDimensions,   "Dimensions non-initialis�es.", "Uninitialized dimensions.")
}
