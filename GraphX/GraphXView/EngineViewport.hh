/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GXV_ENGINEVIEWPORT_HH
#define GXV_ENGINEVIEWPORT_HH

#include "GraphXView.hh"

/*! @file GraphX/GraphXView/EngineViewport.hh
    @brief En-t�te de la classe GXV::EngineViewport.
    @author @ref Guillaume_Terrissol
    @date 7 F�vrier 2003 - 8 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "INPut/InputMgr.hh"

#include "Viewport.hh"

namespace GXV
{
    /*! @brief Viewport moteur.
        @version 0.66

        Cette classe fournit le viewport (singleton) utilis� en mode moteur pur pour le rendu du monde.
     */
    class EngineViewport
        : public Viewport
    {
    public :
        //! @name Constructeur & destructeur
        //@{
                EngineViewport(INP::InputMgr::Ptr pInput, bool pRendererEnabled);   //!< Constructeur.
virtual         ~EngineViewport();                                                  //!< Destructeur.
        //@}

        auto    makeController() const -> INP::Controller::Ptr override;            //!< Cr�ation d'un contr�leur.

    private:

        WeakPtr<INP::InputMgr::Ptr::element_type>   mInputMgr;                      //!< Gestionnaire de p�riph�riques d'entr�es.
    };
}

#endif  // De GXV_ENGINEVIEWPORT_HH
