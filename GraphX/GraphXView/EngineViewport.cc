/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "EngineViewport.hh"

/*! @file GraphX/GraphXView/EngineViewport.cc
    @brief M�thodes (non-inline) de la classe GXV::EngineViewport.
    @author @ref Guillaume_Terrissol
    @date 7 F�vrier 2003 - 8 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GXV
{
//------------------------------------------------------------------------------
//                          Constructeur et Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pInput           Gestionnaire de p�riph�riques d'entr�es.
        @param pRendererEnabled Activation par d�faut des renderers.
     */
    EngineViewport::EngineViewport(INP::InputMgr::Ptr pInput, bool pRendererEnabled)
        : Viewport{}
        , mInputMgr{pInput}
    {
        defaultEnabledRenderers(pRendererEnabled);
    }


    /*! Destructeur.
     */
    EngineViewport::~EngineViewport() { }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return Un nouveau contr�leur.
     */
    INP::Controller::Ptr EngineViewport::makeController() const
    {
        return STL::makeShared<INP::Controller>(mInputMgr.lock());
    }
}
