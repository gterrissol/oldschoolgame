/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GXV_ALL_HH
#define GXV_ALL_HH

/*! @file GraphX/GraphXView/All.hh
    @brief Interface publique du module @ref GraphXView.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 2 Avril 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GXV   //! Gestion de l'affichage.
{
    /*! @namespace GXV
        @version 0.75
     */

    /*! @defgroup GraphXView GraphXView : Vues graphiques
        <b>namespace</b> GXV.
     */
}

#include "Viewport.hh"
#include "EngineViewport.hh"

#endif  // De GXV_ALL_HH
