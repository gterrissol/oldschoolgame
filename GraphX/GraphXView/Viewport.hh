/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GXV_VIEWPORT_HH
#define GXV_VIEWPORT_HH

#include "GraphXView.hh"

/*! @file GraphX/GraphXView/Viewport.hh
    @brief En-t�te de la classe GXV::Viewport.
    @author @ref Guillaume_Terrissol
    @date 2 F�vrier 2003 - 13 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "APPearance/Pickable.hh"
#include "CAMera/CAMera.hh"
#include "INPut/Controller.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/Handle.hh"
#include "RenDeR/RenDeR.hh"
#include "SHaDers/Library.hh"
#include "STL/STL.hh"
#include "UTIlity/Rect.hh"
#include "VIEWport/BaseViewport.hh"

namespace GXV
{
    /*! @brief Vue pour le rendu 3D.
        @version 0.8

        Cette classe impl�mente les appels OpenGL et pr�pare la liaison du viewport avec le moteur.
     */
    class Viewport : public VIEW::Viewport
    {
    public :
        //! @name Types de pointeur
        //@{
        using   Ptr         = SharedPtr<Viewport>;                      //!< Pointeur sur viewport.
        using   CameraPtr   = SharedPtr<CAM::Camera>;                   //!< Pointeur sur cam�ra.
        using   RendererPtr = WeakPtr<RDR::Renderer>;                   //!< Pointeur sur Renderer.
        //@}
        //! @name Constructeur & destructeur
        //@{
                                Viewport();                             //!< Constructeur.
virtual                         ~Viewport();                            //!< Destructeur.
        //@}
        //! @name Liaisons avec le syt�me de rendu
        //@{
        void                    attach(RendererPtr pRenderer);          //!< Attachement d'un objet de rendu.
        void                    detach(RendererPtr pRenderer);          //!< D�tachement d'un objet de rendu.
        CameraPtr               camera() const;                         //!< Acc�s � la cam�ra.
        //@}
        APP::Pickable::Ptr      pick(const UTI::Point& pPoint);         //!< Picking.
virtual INP::Controller::Ptr    makeController() const;                 //!< Cr�ation d'un contr�leur.
        //! @name Shaders
        void                    initShaders(PAK::FileHdl pHdl);         //!< D�finition des shaders.
        void                    freeShaders();                          //!< Suppression des shaders.
        SHD::Library::Ptr       shaders();                              //!< Biblioth�que de shaders.
        //@}
        //! @name Gestion des renderers
        //@{
        RendererPtr             getRenderer(U32 pN);                    //!< Acc�s � un renderer.
        U32                     rendererCount() const;                  //!< Nombre de renderers.
        void                    enableRenderers(bool pEnabled);         //!< [D�s]activation des renderers.


    protected:

        void                    defaultEnabledRenderers(bool pEnabled); //!< Etat d'activation par d�faut des renderers.
        //@}

    private:
        //! @name Affichage
        //@{
virtual void                    doClearing();                           //!< "Nettoyage" du viewport.
virtual void                    preparePainting();                      //!< Pr�paration du rendu.
virtual void                    doPainting();                           //!< Impl�mentation du rendu.
        //@}
        //! @name Gestion avanc�e
        //@{
virtual void                    doInitialize();                         //!< Initialisation du viewport.
virtual void                    doResize(I32 pWidth, I32 pHeight);      //!< Redimensionnement du viewport.
virtual F32                     getRatio() const;                       //!< Ratio d'affichage.
        //@}
        FORBID_COPY(Viewport)
        PIMPL()
    };
}

#endif  // De GXV_VIEWPORT_HH
