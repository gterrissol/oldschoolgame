/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LIT_BASELIGHT_HH
#define LIT_BASELIGHT_HH

/*! @file GraphX/LIghT/BaseLight.hh
    @brief En-t�te des classes LIT::Light, LIT::DirectionalLight, LIT::PointLight & LIT::LightList.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/RGBA.hh"
#include "MATH/V4.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/SharedPtr.hh"

#include "Enum.hh"

namespace LIT
{
//------------------------------------------------------------------------------
//                              Lumi�re El�mentaire
//------------------------------------------------------------------------------

    /*! @brief Classe lumi�re.
        @version 0.32

        @todo R�diger une petite pr�sentation des lumi�res
     */
    class Light : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<Light>    Ptr;                                        //!< Pointeur sur lumi�re.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Light();                                        //!< Constructeur.
virtual             ~Light();                                       //!< Destructeur.
        //@}
        //! @name Spectres
        //@{
 static void        setGlobalAmbient(const CLR::RGBA& pAmbient);    //!< D�finition du spectre ambiant global.
 static CLR::RGBA   globalAmbient();                                //!< Spectre ambiant global.
        void        setAmbient(const CLR::RGBA& pAmbient);          //!< D�finition du spectre ambiant.
        CLR::RGBA   ambient() const;                                //!< Spectre ambiant.
        void        setDiffuse(const CLR::RGBA& pDiffuse);          //!< D�finition du spectre diffus.
        CLR::RGBA   diffuse() const;                                //!< Spectre diffus.
        void        setSpecular(const CLR::RGBA& pSpecular);        //!< D�finition du spectre sp�culaire.
        CLR::RGBA   specular() const;                               //!< Spectre sp�culaire.
        //@}
        //! @name Fonctions �volu�es
        //@{
        // Priorit�s : � revoir.
        void        setShadowCoeff(F32 pCoeff);                     //!< D�finition du coefficient d'ombrage.
        F32         shadowCoeff() const;                            //!< Coefficient d'ombrage.
        //@}
        //! @name M�thodes sp�cifiques pour chaque type de lumi�re
        //@{
virtual EType       type() const = 0;                               //!< Type de lumi�re.
virtual void        prepareForRendering(U32 pLightId) const;        //!< Etablissement des param�tres de rendu.
        //@}

    private:

        //! @name Membre de classe
        //@{
 static CLR::RGBA  smGlobalAmbient;                                //!< Spectre ambiant global.
        //@}
        //! @name Spectres
        //@{
        CLR::RGBA  mAmbient;                                       //!< ... ambiant.
        CLR::RGBA  mDiffuse;                                       //!< ... diffus.
        CLR::RGBA  mSpecular;                                      //!< ... sp�culaire.
        //@}
        //! @name Autres attributs
        //@{
        F32         mShadowCoeff;                                   //!< Coefficient d'ombrage.
        // Priorit�s � revoir...
        //@}
    };


//------------------------------------------------------------------------------
//                            Lumi�res Sp�cialis�es.
//------------------------------------------------------------------------------

    /*! @brief Lumi�re directionnelle.
        @version 0.3

        @todo R�diger une petite pr�sentation des lumi�res directionnelles
     */
    class DirectionalLight : public Light
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    DirectionalLight();                         //!< Constructeur.
virtual             ~DirectionalLight();                        //!< Destructeur.
        //@}
        //! @name Fonctions propres aux lumi�res directionnelles
        //@{
        void        setDirection(const MATH::V4& pDirection);   //!< D�finition de la direction.
        MATH::V4    direction() const;                          //!< Direction de la lumi�re.
        //@}
        //! @name M�thodes sp�cifiques pour chaque type de lumi�re
        //@{
virtual EType       type() const;                               //!< Type de lumi�re.
virtual void        prepareForRendering(U32 pLightId) const;    //!< Etablissement des param�tres de rendu.
        //@}

    private:
        //! @name Attribut propre aux lumi�res directionnelles
        //@{
        MATH::V4    mDirection;                                 //!< Direction de la lumi�re.
        //@}
    };


    /*! @brief Lumi�re ponctuelle.
        @version 0.3

        @todo R�diger une petite pr�sentation des lumi�res directionnelles
     */
    class PointLight : public Light
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                    PointLight();                               //!< Constructeur.
virtual             ~PointLight();                              //!< Destructeur.
        //@}
        //! @name Fonctions propres aux lumi�res ponctueles
        //@{
        void        setPosition(const MATH::V4& pPosition);     //!< D�finition de la position.
        MATH::V4    position() const;                           //!< Position de la lumi�re.
        void        setRange(F32 pRange);                       //!< D�finition du rayon d'�clairage.
        F32         range() const;                              //!< Rayon d'action de la lumi�re.
        // Att�nuation : discussion.
        //@}
        //! @name M�thodes sp�cifiques pour chaque type de lumi�re
        //@{
virtual EType       type() const;                               //!< Type de lumi�re.
virtual void        prepareForRendering(U32 pLightId) const;    //!< Etablissement des param�tres de rendu.
        //@}

    private:
        //! @name Attribut propre aux lumi�res ponctuelles
        //@{
        MATH::V4    mPosition;                                  //!< Position de la lumi�re.
        F32         mRange;                                     //!< Rayon d'�clairage.
        //@}
    };


//------------------------------------------------------------------------------
//                               Liste de Lumi�res
//------------------------------------------------------------------------------

    /*! @brief Liste de lumi�res
        @version 0.5

        Afin de faciliter la gestion des lumi�res �clairant un objet (son apparence), j'ai introduit
        cette classe qui n'est rien d'autre qu'un conteneur un peu sp�cialis�.
     */
    class LightList : public MEM::OnHeap
    {
    public:
        //! @name Fonction amie
        //@{
 friend bool operator==(const LightList& pL, const LightList& pR);  //!< Egalit�.
        //@}
        //! @name Types de pointeur
        //@{
        typedef WeakPtr<Light const>    ConstLightPtr;              //!< Pointeur (constant) sur lumi�re.
        typedef WeakPtr<Light>          LightPtr;                   //!< Pointeur sur lumi�re.
        //@}
        //! @name Constructeur & destructeur
        //@{
                        LightList();                                //!< Constructeur par d�faut.
                        ~LightList();                               //!< Destructeur.
        //@}
        //! @name Manipulation des lumi�res
        //@{
        U32             count() const;                              //!< Nombre courant de lumi�res.
        ConstLightPtr   at(size_t pN) const;                        //!< R�cup�ration d'une lumi�re (constante).
        LightPtr        at(size_t pN);                              //!< R�cup�ration d'une lumi�re.
        void            insert(LightPtr pLight);                    //!< Insertion d'une lumi�re.
        void            remove(LightPtr pLight);                    //!< Retrait d'une lumi�re.
        //@}

    private:

        I32             findLight(ConstLightPtr pLight) const;      //!< Recherche d'une lumi�re.
        FORBID_COPY(LightList)
        PIMPL()
    };

    //! @name Operateurs d'(in)�galit�
    bool    operator==(const LightList& pL, const LightList& pR);   //!< Egalit�.
    bool    operator!=(const LightList& pL, const LightList& pR);   //!< In�galit�.
    //@}
}

#endif  // De LIT_BASELIGHT_HH
