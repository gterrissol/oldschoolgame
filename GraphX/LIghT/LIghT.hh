/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LIT_LIGHT_HH
#define LIT_LIGHT_HH

/*! @file GraphX/LIghT/LIghT.hh
    @brief Pr�-d�clarations du module @ref LIghT.
    @author @ref Guillaume_Terrissol
    @date 24 Mars 2008 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */


namespace LIT
{
    /*! @namespace LIT
        @version 0.15

        Gestion des lumi�res.
     */

    class ODirectionalLight;
    class OLight;
    class OLightList;
    class OPointLight;
}

#include "BaseLight.hh"

#endif  // De LIT_LIGHT_HH
