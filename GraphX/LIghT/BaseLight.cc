/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "LIghT.hh"

/*! @file GraphX/LIghT/BaseLight.cc
    @brief M�thodes (non-inline) de la classe LIT::Light.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 19 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <algorithm>
#include <GL/gl.h>

#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace LIT
{
//------------------------------------------------------------------------------
//                     Lumi�re : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Light::Light()
        : mAmbient()
        , mDiffuse()
        , mSpecular()
        , mShadowCoeff(k0F)
    { }


    /*! Destructeur.
     */
    Light::~Light() { }


//------------------------------------------------------------------------------
//                              Lumi�re : Spectres
//------------------------------------------------------------------------------

    /*! D�finition du spectre ambiant global.
     */
    void Light::setGlobalAmbient(const CLR::RGBA& pAmbient)
    {
        smGlobalAmbient = pAmbient;
    }


    /*! Spectre ambiant.
     */
    CLR::RGBA Light::globalAmbient()
    {
        return smGlobalAmbient;
    }


    /*! D�finition du spectre ambiant.
     */
    void Light::setAmbient(const CLR::RGBA& pAmbient)
    {
        mAmbient = pAmbient;
    }


    /*! Spectre ambiant.
     */
    CLR::RGBA Light::ambient() const
    {
        return mAmbient;
    }


    /*! D�finition du spectre diffus.
     */
    void Light::setDiffuse(const CLR::RGBA& pDiffuse)
    {
        mDiffuse = pDiffuse;
    }


    /*! Spectre diffus.
     */
    CLR::RGBA Light::diffuse() const
    {
        return mDiffuse;
    }


    /*! D�finition du spectre sp�culaire.
     */
    void Light::setSpecular(const CLR::RGBA& pSpecular)
    {
        mSpecular = pSpecular;
    }


    /*! Spectre sp�culaire.
     */
    CLR::RGBA Light::specular() const
    {
        return mSpecular;
    }


//------------------------------------------------------------------------------
//                         Lumi�re : Fonctions Evolu�es
//------------------------------------------------------------------------------

    /*! D�finition du coefficient d'ombrage.
     */
    void Light::setShadowCoeff(F32 pCoeff)
    {
        mShadowCoeff = pCoeff;
    }


    /*! Coefficient d'ombrage.
     */
    F32 Light::shadowCoeff() const
    {
        return mShadowCoeff;
    }


//------------------------------------------------------------------------------
//                   M�thode Commune � chaque Type de Lumi�re
//------------------------------------------------------------------------------

    /*! Etablit les param�tres de rendu (utilise le graphic device) communs � toutes les lumi�res. Les parties sp�cifiques sont r�alis�es
        dans les r�impl�mentations de cette m�thode (n'oubliez pas de l'appeler � partir de ces r�impl�mentations).
        @param pLightId Indique quelle lumi�re du <b>graphic device</b> va recevoir les attributs de l'instance
     */
    void Light::prepareForRendering(U32 pLightId) const
    {
#if   (GXD_IMPL == 0)
        // Fixe les couleurs.
        glLightfv(GL_LIGHT0 + pLightId, GL_AMBIENT,  addressof(ambient()[0]));
        glLightfv(GL_LIGHT0 + pLightId, GL_DIFFUSE,  addressof(diffuse()[0]));
        glLightfv(GL_LIGHT0 + pLightId, GL_SPECULAR, addressof(specular()[0]));

        // Et active la lumi�re.
        glEnable(GL_LIGHT0 + pLightId);
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL
    }


//------------------------------------------------------------------------------
//              Lumi�re Directionnelle : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    DirectionalLight::DirectionalLight() { }


    /*! Destructeur.
     */
    DirectionalLight::~DirectionalLight() { }


//------------------------------------------------------------------------------
//                  Lumi�re Directionnelle : Fonctions Propres
//------------------------------------------------------------------------------

    /*! D�finition de la direction.
     */
    void DirectionalLight::setDirection(const MATH::V4& pDirection)
    {
        mDirection = pDirection;
        // Contraint w.
        mDirection.w = k0F;
    }


    /*! Direction de la lumi�re.
     */
    MATH::V4 DirectionalLight::direction() const
    {
        return mDirection;
    }


//------------------------------------------------------------------------------
//               M�thodes Sp�cifiques pour chaque Type de Lumi�re
//------------------------------------------------------------------------------

    /*!
     */
    EType DirectionalLight::type() const
    {
        return eDirectional;
    }


    /*! Etablit les param�tres de rendu (utilise le graphic device).
     */
    void DirectionalLight::prepareForRendering(U32 pLightId) const
    {
#if   (GXD_IMPL == 0)
        // Propri�t� sp�cifique : direction de la lumi�re.
        glLightfv(GL_LIGHT0 + pLightId, GL_POSITION, addressof(direction()[0]));
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL

        Light::prepareForRendering(pLightId);
    }


//------------------------------------------------------------------------------
//                Lumi�re Ponctuelle : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    PointLight::PointLight()
        : mRange(k1F)
    { }


    /*! Destructeur.
     */
    PointLight::~PointLight() { }


//------------------------------------------------------------------------------
//                    Lumi�re Ponctuelle : Fonctions Propres
//------------------------------------------------------------------------------

    /*! D�finition de la position.
     */
    void PointLight::setPosition(const MATH::V4& pPosition)
    {
        mPosition = pPosition;
        // Contraint w.
        mPosition.w = k1F;
    }


    /*! Position de la lumi�re.
     */
    MATH::V4 PointLight::position() const
    {
        return mPosition;
    }


    /*! D�finition du rayon d'�clairage.
     */
    void PointLight::setRange(F32 pRange)
    {
        mRange = pRange;
    }


    /*! Rayon d'action de la lumi�re.
     */
    F32 PointLight::range() const
    {
        return mRange;
    }


//------------------------------------------------------------------------------
//               M�thodes Sp�cifiques pour chaque Type de Lumi�re
//------------------------------------------------------------------------------

    /*!
     */
    EType PointLight::type() const
    {
        return ePoint;
    }


    /*! Etablit les param�tres de rendu (utilise le graphic device).
     */
    void PointLight::prepareForRendering(U32 pLightId) const
    {
#if   (GXD_IMPL == 0)
        // Propri�t� sp�cifique : position de la lumi�re.
        glLightfv(GL_LIGHT0 + pLightId, GL_POSITION, addressof(position()[0]));
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL

        Light::prepareForRendering(pLightId);
    }


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de LIT::LightList.
        @version 0.2
     */
    class LightList::Private : public MEM::OnHeap
    {
    public:
        Private();
        //! @name Attribut
        //@{
        Vector<LightPtr>   mElements;
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    LightList::Private::Private()
        : mElements()
    { }


//------------------------------------------------------------------------------
//                              Op�rateur d'Egalit�
//------------------------------------------------------------------------------

    /*! Compare deux weak pointers.
        @param pA Premier pointeur � comparer
        @param pB Second pointeur � comparer
        @return VRAI si les pA et pB sont �quivalents (i.e. partagent un m�me pointeur r�f�rence ou
        sont tous deux vides)
     */
    bool operator==(const LightList::ConstLightPtr& pA, const LightList::ConstLightPtr& pB)
    {
        return !std::owner_less<LightList::ConstLightPtr>()(pA, pB) && !std::owner_less<LightList::ConstLightPtr>()(pA, pB);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    LightList::LightList()
        : pthis()
    { }


    /*! Destructeur.
     */
    LightList::~LightList() { }


//------------------------------------------------------------------------------
//                 Liste de Lumi�res : Manipulation des Lumi�res
//------------------------------------------------------------------------------

    /*! Nombre courant de lumi�res.
     */
    U32 LightList::count() const
    {
        return U32(pthis->mElements.size());
    }


    /*! R�cup�ration d'une lumi�re (constante).
     */
    LightList::ConstLightPtr LightList::at(size_t pN) const
    {
        return pthis->mElements[pN];
    }


    /*! R�cup�ration d'une lumi�re.
     */
    LightList::LightPtr LightList::at(size_t pN)
    {
        return pthis->mElements[pN];
    }


    /*! Insertion d'une lumi�re.
        @param pLight Lumi�re � ins�rer de la liste
        @note Rien n'est fait si :
        - la lumi�re �tait d�j� pr�sente dans la liste
        - <i>pLight</i> est nul
     */
    void LightList::insert(LightPtr pLight)
    {
        if (!pLight.expired())
        {
            // Il faut v�rifier que la lumi�re n'est pas d�j� pr�sente dans la liste.
            if (findLight(pLight) == -1)
            {
                // La lumi�re peut donc �tre ajout�e � la liste.
                pthis->mElements.push_back(pLight);
            }
        }
    }


    /*! Retrait d'une lumi�re.
        @param pLight Lumi�re � retirer de la liste
        @note Rien n'est fait si la lumi�re n'�tait pas pr�sente dans la liste
     */
    void LightList::remove(LightPtr pLight)
    {
        // Recherche de la lumi�re.
        I32 lLightIndex = findLight(pLight);

        if (lLightIndex != -1)
        {
            // Si la lumi�re est d�j� la derni�re de la liste, la permutation est inutile.
            if (lLightIndex != I32(count() - 1))
            {
                std::swap(pthis->mElements[lLightIndex], pthis->mElements.back());
            }

            // Retrait de la lumi�re.
            pthis->mElements.pop_back();
        }
    }


//------------------------------------------------------------------------------
//                       Liste de Lumi�res : Autre M�thode
//------------------------------------------------------------------------------

    /*! Recherche d'une lumi�re.
        @param pLight Lumi�re � chercher
        @return L'indice de la lumi�re dans le tableau si celle-ci a �t� trouv�e, -1 sinon
     */
    I32 LightList::findLight(ConstLightPtr pLight) const
    {
        auto lIter = std::find(pthis->mElements.begin(), pthis->mElements.end(), pLight);

        if (lIter != pthis->mElements.end())
        {
            return I32(lIter - pthis->mElements.begin());
        }
        else
        {
            return I32(-1);
        }
    }


//------------------------------------------------------------------------------
//                  Liste de Lumi�res : Operateur d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! Teste si 2 listes de lumi�res sont identiques.
        @param pL 1er op�rande de la comparaison
        @param pR 2nd op�rande de la comparaison
        @return VRAI si les listes r�f�rencent exactement les m�mes lumi�res (peu importe l'ordre), FAUX
        sinon
     */
    bool operator==(const LightList& pL, const LightList& pR)
    {
        // Si les nombres de lumi�res dans chacune des listes ne sont pas �gaux, l'in�galit� des listes est �vidente.
        if (pL.count() != pR.count())
        {
            return false;
        }

        // Chaque lumi�re de la liste...
        for(U32 lL = k0UL; lL < pL.count(); ++lL)
        {
            // Est recherch�e dans la seconde liste.
            if (pR.findLight(pL.at(lL)) == -1)
            {
                return false;
            }
        }

        // Les lumi�res des deux listes sont les m�mes.
        return true;
    }


    /*! Teste si 2 listes de lumi�res sont diff�rentes.
        @param pL 1er op�rande de la comparaison
        @param pR 2nd op�rande de la comparaison
        @return FAUX si les listes r�f�rencent exactement les m�mes lumi�res, VRAI sinon
     */
    bool operator!=(const LightList& pL, const LightList& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                          Lumi�re : Membre de Classe
//------------------------------------------------------------------------------

    CLR::RGBA   Light::smGlobalAmbient(k0F, k0F, k0F, k0F);
}
