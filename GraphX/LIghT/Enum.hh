/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LIT_ENUM_HH
#define LIT_ENUM_HH

/*! @file GraphX/LIghT/Enum.hh
    @brief Enum�rations du module LIT.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace LIT
{
    //! @name Types de lumi�re
    enum EType
    {
        eDirectional = 0,
        ePoint
    };
}

#endif // De LIT_ENUM_HH
