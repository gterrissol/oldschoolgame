/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef LIT_ALL_HH
#define LIT_ALL_HH

/*! @file GraphX/LIghT/All.hh
    @brief Interface publique du module @ref LIghT.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 24 Mars 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/CoLoR.hh"
#include "STL/STL.hh"

//------------------------------------------------------------------------------
//                                 Enum�rations
//------------------------------------------------------------------------------

#include "Enum.hh"


namespace LIT   //! Lumi�res.
{
    /*! @namespace LIT
        @version 0.15

        Gestion des lumi�res.
     */

    /*! @defgroup LIghT LIghT : Lumi�res et �clairage
        <b>namespace</b> LIT.
     */

}

#include "BaseLight.hh"

#endif  // De LIT_LIGHT_HH
