/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CAM_BASECAMERA_HH
#define CAM_BASECAMERA_HH

#include "CAMera.hh"

/*! @file GraphX/CAMera/BaseCamera.hh
    @brief En-t�te de la classe CAM::Camera.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
    @note Encore une fois, les besoins de portabilit� et l'incapacit� de certains syst�mes � tenir compte de la casse des noms de fichiers
    m'a oblig� � nommer ce fichier diff�remment de <i>Camera.hh</i>
 */

#include "MATH/MATH.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "STL/SharedPtr.hh"
#include "UTIlity/UTIlity.hh"

namespace CAM
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Classe cam�ra.
        @version 0.6

        Cette classe fournit des m�thodes "brutes" pour la vision du l'univers de jeu.<br>
        Une gestion plus intelligente des cam�ras devra �tre r�alis�e via les utilisateurs de la cam�ra
        (scripts, �diteurs,...)
    */
    class Camera : public MEM::OnHeap
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<Camera>   Ptr;                    //!< Pointeur sur cam�ra.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Camera();                               //!< Constructeur par d�faut.
                    ~Camera();                              //!< Destructeur.
        //@}
        void        moveTo(const MATH::V3& pPosition);      //!< D�placement absolu.
        MATH::V3    position() const;                       //!< Position actuelle.
        //! @name Rotations
        //@{
        void        rotate(F32 pAlpha);                     //!< Rotation de la cam�ra.
        F32         viewAngle() const;                      //!< Angle de rotation de la cam�ra.
        F32         pitchAngle() const;                     //!< Angle d'inclinaison de la cam�ra.
        //@}
        //! @name Zoom
        //@{
        void        zoom(F32 pCoeff);                       //!< Zoom.
        void        setZoom(F32 pNewZoom);                  //!< D�finition du zoom.
        F32         zoom() const;                           //!< Niveau actuel de zoom
        //@}
        //! @name Position
        //@{
        void        move(const MATH::V3& pOffsets);         //!< D�placement incr�mental. 
        //@}
        void        setForRender(F32 pRatio);               //!< Activation de la cam�ra.
        void        setForPicking(const UTI::Rect& pZone);  //!< Activation de la cam�ra.
        void        reset();                                //!< R�initialisation des param�tres.
        //! @name Picking
        //@{
        void        setDepthScale(F32 pScale);              //!< Facteur pour le calcul des profondeurs.
        void        postRender();                           //!< Traitement post-rendu.
        MATH::V3    pickPoint(U16 pX, U16 pY) const;        //!< Picking.
        MATH::V3    viewed() const;                         //!< Position de la "cible" de la cam�ra.
        UTI::Rect   visibleArea() const;                    //!< Zone visible.
        //@}
        //! @name Matrices
        //@{
        MATH::M4    modelView() const;                      //!< ... de vue.
        MATH::M4    projection() const;                     //!< ... de projection
        //@}

    private:

        FORBID_COPY(Camera)
        PIMPL()
    };
}

#endif  // De CAM_BASECAMERA_HH
