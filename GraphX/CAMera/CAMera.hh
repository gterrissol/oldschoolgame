/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CAM_CAMERA_HH
#define CAM_CAMERA_HH

/*! @file GraphX/CAMera/CAMera.hh
    @brief Pr�-d�clarations du module @ref CAMera.
    @author @ref Guillaume_Terrissol
    @date 30 Mars 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CAM
{
    class Camera;
}

#endif  // De CAM_CAMERA_HH
