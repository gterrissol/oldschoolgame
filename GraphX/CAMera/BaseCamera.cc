/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseCamera.hh"

/*! @file GraphX/CAMera/BaseCamera.cc
    @brief M�thodes (non-inline) de la classe CAM::Camera.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 9 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>

#include <algorithm>
#include <cmath>
#include <limits>

#include "MATH/Const.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/Q.hh"
#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "UTIlity/Rect.hh"

namespace
{
    MATH::V3        kVerticalUnit       = MATH::V3(k0F, k0F, k1F);              //!< Vecteur vertical unitaire.
    const F32       kPitchAngle         = F32(60.0F);                           //!< Angle de tangage.

    const F32       kDefaultAltitude    = F32(320.0F);                          //!< Altitude par d�faut.
    const F32       kDefaultZoomValue   = F32(640.0F);                          //!< Zoom par d�faut.

    const F32       kDepth              = F32(10.0F);                           //!< Profondeur du frustum

    /*! Impl�mentation (initiale) de gluPickMatrix r�cup�r�e dans les sources de Mesa (merci M'sieur Paul).
     */
    MATH::M4 pickMatrix(GLdouble x, GLdouble y, GLdouble width, GLdouble height, GLint viewport[4])
    {
        if (width <= 0 || height <= 0)
        { 
            return MATH::kIdM4;
        }

        return MATH::M4{MATH::kIdM3, MATH::V3{F32((viewport[2] - 2 * (x - viewport[0])) / width),
                                              F32((viewport[3] - 2 * (y - viewport[1])) / height),
                                              k0F}} *
               MATH::M4{MATH::kIdM3, MATH::kNullV3, MATH::V3{F32(viewport[2] / width),
                                                             F32(viewport[3] / height),
                                                             k1F}};
    }

    /*! @param pZoom  Niveau de zoom actuel.
        @param pRatio Ratio du viewport.
        @return La matrice de projection bas�e sur @p pZoom et @p pRatio.
     */
    MATH::M4 orthoMatrix(F32 pZoom, F32 pRatio)
    {
        return MATH::M4{ { k1F / (pZoom * pRatio), k0F,         k0F,                    k0F },
                         { k0F,                    k1F / pZoom, k0F,                    k0F },
                         { k0F,                    k0F,       - k1F / (kDepth * pZoom), k0F },
                         { k0F,                    k0F,         k0F,                    k1F } };
    }

    /*! @param pPosition Position de la cam�ra.
        @param pYaw      Angle de lacet.
        @return La matrice de vue bas�e sur @p pPosition et @p pYaw.
     */
    MATH::M4 viewMatrix(const MATH::V3& pPosition, F32 pYaw)
    {
        MATH::M3    lYaw{};
        lYaw.createRotationOnZ(pYaw);
        MATH::M3    lPitch{};
        lPitch.createRotationOnX(- kPitchAngle * kDegToRad);

        return MATH::M4{lPitch * lYaw} * MATH::M4{MATH::kIdM3, MATH::V3{- pPosition.x, - pPosition.y, k0F}};
    }


//------------------------------------------------------------------------------
//                              Fonction Auxiliaire
//------------------------------------------------------------------------------

    /*! Transformation d'un point �cran en point objet.
        @param pWinPos     Point �cran � "d�projeter" dans l'espace (exprim� en coordonn�es normalis�es - [-1, 1])
        @param pModelview  Matrice de vue utilis�e pour le rendu
        @param pProjection Matrice de projection pour le rendu
        @return La position 3D correspondant au point �cran <i>pWinPos</i> (ou un vecteur invalide en cas d'�chec)
        @note Ce code est une adaptation de l'impl�mentation de gluUnProject propos�e par Mesa
     */
    MATH::V3 unProject(const MATH::V3& pWinPos,
                        const MATH::M4& pModelview,
                        const MATH::M4& pProjection)
    {
        // Calcul de la transformation inverse.
        MATH::M4   lA      = pProjection * pModelview;
        lA.invert();

        // D'ou les coordonnees objets :
        MATH::V4   lOut    = lA * MATH::V4(pWinPos);

        // Echec ?
        if (lOut.w == k0F)
        {
            return MATH::V3(F32(std::numeric_limits<float>::quiet_NaN()),
                             F32(std::numeric_limits<float>::quiet_NaN()),
                             F32(std::numeric_limits<float>::quiet_NaN()));
        }

        // Non : succ�s.
        return (k1F / lOut.w) * lOut;
    }
}


namespace CAM
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de CAM::Camera.
        @version 0.2

     */
    class Camera::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeur
        //@{
        Private();                              //!< Constructeur.
        //@}
        //! @name Attributs
        //@{
        mutable MATH::M4    mModelview;         //!< Matrice d�finissant la vue de la cam�ra.
        mutable MATH::M4    mProjection;        //!< Matrice de projection (utile ici ?).
        MATH::V3            mPosition;          //!< Position du point de vue.
        MATH::V3            mViewed;            //!< "Cible" de la cam�ra.
        MATH::Q             mRotation;          //!< Rotation (sous forme de quaterion).
        F32                 mYaw;               //!< Angle de rotation selon Z (radians).
        F32                 mZoom;              //!< Niveau de zoom.
        F32                 mDepthScale;        //!< 
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Camera::Private::Private()
        : mModelview()
        , mProjection()
        , mPosition(k0F, k0F, kDefaultAltitude)
        , mViewed(MATH::kNullV3)
        , mRotation(MATH::kNullQ)
        , mYaw(k0F)
        , mZoom(kDefaultZoomValue)
        , mDepthScale(k1F / 4294967296.0F)
    { }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Camera::Camera()
        : MEM::OnHeap()
        , pthis()
    { }


    /*! Destructeur.
     */
    Camera::~Camera() = default;


//------------------------------------------------------------------------------
//                                    Gestion
//------------------------------------------------------------------------------

    /*! Rotation de la cam�ra.
        @param pAlpha Angle de rotation de la cam�ra, en radians
     */
    void Camera::rotate(F32 pAlpha)
    {
        pthis->mYaw += pAlpha;
        pthis->mRotation = MATH::Q(kVerticalUnit, - pthis->mYaw);
    }


    /*! Angle de rotation de la cam�ra (lacet), en radians.
     */
    F32 Camera::viewAngle() const
    {
        return pthis->mYaw;
    }


    /*! Angle d'inclinaison de la cam�ra (tangage), en radians.
     */
    F32 Camera::pitchAngle() const
    {
        return kPitchAngle * kDegToRad;
    }


    /*! Zoom (changement d'�l�vation).
     */
    void Camera::zoom(F32 pCoeff)
    {
        pthis->mZoom += F32(pCoeff * (k1F + pthis->mZoom / 50.0F));
        pthis->mPosition.z = F32(0.5F * pthis->mZoom);
    }


    /*! Zoom (changement d'�l�vation).
     */
    void Camera::setZoom(F32 pNewZoom)
    {
        pthis->mZoom = pNewZoom;
        pthis->mPosition.z = F32(0.5F * pthis->mZoom);
    }


    /*! D�placement incr�mental.
     */
    void Camera::move(const MATH::V3& pOffsets)
    {
        MATH::V3   lOffsets(pOffsets.x, pOffsets.y, k0F);
        lOffsets.rotate(pthis->mRotation);

        pthis->mPosition += MATH::V3(lOffsets.x, lOffsets.y, pOffsets.z);
    }


    /*! D�placement absolu.
     */
    void Camera::moveTo(const MATH::V3& pPosition)
    {
        MATH::V3   lPosition(pPosition[0], pPosition[1], k0F);
        lPosition.rotate(pthis->mRotation);

        pthis->mPosition.x = lPosition.x;
        pthis->mPosition.y = lPosition.y;
    }


    /*! @return Le niveau de zoom actuel
     */
    F32 Camera::zoom() const
    {
        return pthis->mZoom;
    }


    /*! @return La position actuelle
     */
    MATH::V3 Camera::position() const
    {
        return pthis->mPosition;
    }


    /*! Etablit les param�tres de la cam�ra au niveau du graphic device.
     */
    void Camera::setForRender(F32 pRatio)
    {
        pthis->mProjection = orthoMatrix(F32(0.5F * zoom()), pRatio);
        pthis->mModelview = viewMatrix(pthis->mPosition, pthis->mYaw);
    }


    /*! Etablit les param�tres de la cam�ra au niveau du graphic device.
        @param pZone  Zone de picking (coordonn�es viewport : (0, 0) - (width, height)).
     */
    void Camera::setForPicking(const UTI::Rect& pZone)
    {
        GLint       lViewport[4];
        glGetIntegerv(GL_VIEWPORT, lViewport);
        UTI::Pointf lCenter = pZone.center();
        pthis->mProjection  = pickMatrix(lCenter.x, lViewport[3] - lCenter.y, pZone.width(), pZone.height(), lViewport) *
                              orthoMatrix(F32(0.5F * zoom()), pZone.width() / pZone.height());

        pthis->mModelview = viewMatrix(pthis->mPosition, pthis->mYaw);
    }


    /*! R�initialise les attributs de la cam�ra.
     */
    void Camera::reset()
    {
        Camera lCamera;
        pthis.swap(lCamera.pthis);
    }


//------------------------------------------------------------------------------
//                                    Picking
//------------------------------------------------------------------------------

    /*!
     */
    void Camera::setDepthScale(F32 pScale)
    {
        pthis->mDepthScale = pScale;
    }


    /*!
     */
    void Camera::postRender()
    {
        GLint   lViewport[4];
        glGetIntegerv(GL_VIEWPORT, lViewport);
// � remplacer avec un vrai lancer de rayon sur les tuiles (ou, de mani�re plus g�n�rale, sur les objets "sol").
        pthis->mViewed = pickPoint(U16(lViewport[2] / 2), U16(lViewport[3] / 2));
    }


    /*!
     */
    MATH::V3 Camera::pickPoint(U16 pX, U16 pY) const
    {
        GLint   lViewport[4];
        glGetIntegerv(GL_VIEWPORT, lViewport);

        glFlush();
        U32     lIntDepth   = k0UL;
        // NB : La profondeur est r�cup�r�e sous forme enti�re, plut�t que flottante, � cause de probl�mes (de drivers ?) rencontr�s.
        glReadPixels(pX, pY, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, &lIntDepth);

        F32     lDepth = F32(lIntDepth * pthis->mDepthScale);
        //if (lDepth < k1F)
        {
            UTI::Rect  lRect(k0F, k0F, F32(lViewport[2]), F32(- lViewport[3]));
            MATH::V3   lPos(F32((pX - lRect.left()) * 2.0F / lRect.width()  - k1F),
                            F32((pY - lRect.top())  * 2.0F / lRect.height() - k1F),
                            F32(2.0F * lDepth  - k1F));

            return unProject(lPos, modelView(), projection());
        }
        /*else
        {
            return MATH::kNullV3;
        }*/
    }


    /*! @return La position du point "vu" par la cam�ra (i.e. la d�projection du centre de l'�cran)
     */
    MATH::V3 Camera::viewed() const
    {
        return pthis->mViewed;
    }


    /*! Permet de conna�tre la partie du monde affich�e dans le viewport (avec une limite : le rectangle (x0y)
        est align� aux axes).
        @return Un rectangle contenant la partie du monde actuellement affich�e
     */
    UTI::Rect Camera::visibleArea() const
    {
        GLint   lViewport[4];
        glGetIntegerv(GL_VIEWPORT, lViewport);

        MATH::M4   lProj   = projection();
        MATH::M4   lModl   = modelView();

        // D�projette sur le monde les coins de l'�cran (Far en haut, Near en bas),
        // et retourne la zone couverte.
        F32 lTan    = F32(tanf(pitchAngle()));
        MATH::V3   lTL = unProject(MATH::V3(-k1F,  k1F, (k1F + lTan) / kDepth), lModl, lProj);
        MATH::V3   lTR = unProject(MATH::V3( k1F,  k1F, (k1F + lTan) / kDepth), lModl, lProj);
        MATH::V3   lBL = unProject(MATH::V3(-k1F, -k1F, (k1F - lTan) / kDepth), lModl, lProj);
        MATH::V3   lBR = unProject(MATH::V3( k1F, -k1F, (k1F - lTan) / kDepth), lModl, lProj);

        return UTI::Rect(std::min(std::min(lTL.x, lTR.x), std::min(lBL.x, lBR.x)),
                         std::max(std::max(lTL.y, lTR.y), std::max(lBL.y, lBR.y)),
                         std::max(std::max(lTL.x, lTR.x), std::max(lBL.x, lBR.x)),
                         std::min(std::min(lTL.y, lTR.y), std::min(lBL.y, lBR.y)));
    }


//------------------------------------------------------------------------------
//                                   Matrices
//------------------------------------------------------------------------------

    /*! Matrice de vue.
     */
    MATH::M4 Camera::modelView() const
    {
        return pthis->mModelview;
    }


    /*! Matrice de projection.
     */
    MATH::M4 Camera::projection() const
    {
        return pthis->mProjection;
    }
}
