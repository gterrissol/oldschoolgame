/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CAM_ALL_HH
#define CAM_ALL_HH

/*! @file GraphX/CAMera/All.hh
    @brief Interface publique du module @ref CAMera.
    @author @ref Guillaume_Terrissol
    @date 21 Ao�t 2002 - 30 Mars 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CAM   //! Cam�ra.
{
    /*! @namespace CAM
        @version 0.15

        ...
     */


    /*! @defgroup CAMera CAMera : Cam�ras
        <b>namespace</b> CAM.
     */
}

#include "BaseCamera.hh"

#endif  // De CAM_ALL_HH
