/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GDV_ERRMSG_HH
#define GDV_ERRMSG_HH

/*! @file GraphX/GraphicDeVice/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module GDV.
    @author @ref Guillaume_Terrissol
    @date 14 mars 2003 - 29 Juillet 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace GDV
{
#ifdef ASSERTIONS

    // ! @name Messages d'assertion
    // @{
    extern  ERR::String kInvalidDevice;
    extern  ERR::String kInvalidWindowId;
    extern  ERR::String kNoDeviceDefined;
    // @}

#endif  // De ASSERTIONS
}

#endif  // De GDV_ERRMSG_HH
