/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GraphicDeVice.hh"

/*! @file GraphX/GraphicDeVice/GraphicDeVice.cc
    @brief D�finitions diverses du module GDV.
    @author @ref Guillaume_Terrissol
    @date 14 Mars 2003 - 29 Juillet 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace GDV
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidDevice,    "Le device est invalide.",  "Invalid device.")
    REGISTER_ERR_MSG(kInvalidWindowId,  "Id de fen�tre invalide.",  "Invalid window Id.")
    REGISTER_ERR_MSG(kNoDeviceDefined,  "Aucun device d�fini.",     "No device defined.")
}
