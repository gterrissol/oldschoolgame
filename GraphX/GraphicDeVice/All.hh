/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GDV_ALL_HH
#define GDV_ALL_HH

/*! @file GraphX/GraphicDeVice/All.hh
    @brief Interface publique du module @ref GraphicDeVice.
    @author @ref Guillaume_Terrissol
    @date 14 Mars 2003 - 22 Avril 2007
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GDV   //! Rendu bas niveau.
{
    /*! @namespace GDV
        @version 0.1
     */

    /*! @defgroup GraphicDeVice GraphicDeVice : Gestion 3D bas niveau
        <b>namespace</b> GDV.
     */
}

#include "Device.hh"

#endif  // De GDV_ALL_HH
