/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Device.hh"

/*! @file GraphX/GraphicDeVice/Device.cc
    @brief M�thodes (non-inline) de la classe GDV::Device.
    @author @ref Guillaume_Terrissol
    @date 14 mars 2003 - 18 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>

namespace GDV
{
/*
glPushAttrib
glPopAttrib
glFlush
glRenderMode
glSelectBuffer
glInitNames
glPushName
glColor4fv
glEnable
glBlendFunc
glDisable
glMaterialfv
glMatrixMode
glPushMatrix
glMultMatrixf
glVertexPointer
glEnableClientState
glDisableClientState
glNormalPointer
glColorPointer
glTexCoordPointer
glDrawElements
glPopMatrix
glLoadName
glShadeModel
glClearColor
glDepthFunc
glFrontFace
glCullFace
glPolygonMode
glPixelStorei
glClear
glTranslatef
glScalef
glOrtho
glLoadIdentity
glRotatef
glGetIntegerv
glGetFloatv
glTexParameterf
glTexEnvi
glDeleteTextures
glGenTextures
glBindTexture
glTexImage2D
glTexParameteri
glNewList
glVertex2f
glTexCoord2d
glBegin
glEnd
glEndList
glListBase
glCallLists
glGetFloatv
glGenLists
glDeleteLists
glLightfv
glGetString
glReadPixels

RenDeR/RenderTree.cc
RenDeR/RenderNode.cc
GraphXView/Viewport.cc
CAMera/BaseCamera.cc
MATerial/Texture.cc
MATerial/Image.cc
FoNT/Data.cc
LIghT/BaseLight.cc
QT3D/GLWidget.cc
 */
//------------------------------------------------------------------------------
//                     Device : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Device::Device() { }



    /*! Destructeur.
     */
    Device::~Device() { }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  Device* MEM::Singleton<Device>::smThat  = nullptr;
#endif
}
