/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GDV_ENUM_HH
#define GDV_ENUM_HH

/*! @file GraphX/GraphicDeVice/Enum.hh
    @brief Enum�rations du module GDV.
    @author @ref Guillaume_Terrissol
    @date 14 mars 2003 - 29 Juillet 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace GDV
{
}

#endif // De GDV_ENUM_HH
