/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef GDV_DEVICE_HH
#define GDV_DEVICE_HH

#include "GraphicDeVice.hh"

/*! @file GraphX/GraphicDeVice/Device.hh
    @brief En-t�te de la classe GDV::Device.
    @author @ref Guillaume_Terrissol
    @date 14 mars 2003 - 18 F�vrier 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/Singleton.hh"

#include "Enum.hh"

namespace GDV
{
    /*! @brief Encapsulation d'OpenGL.
        @version 0.2

        Cette classe est une abstraction d'OpenGL : elle fournit un acc�s centralis� � toutes les
        fonctions OpenGL.
        @todo A compl�ter
     */
    class Device : public MEM::Singleton<Device>, public MEM::OnHeap
    {
    public :
        //! @name Constructeur & destructeur
        //@{
                Device();          //!< Constructeur par d�faut.
                ~Device();         //!< Destructeur.
        //@}
        //! @name Activation
        //@{
        void    enable(I32 pFlags); //!< 
        //@}
        //! @name Lumi�res
        //@{
//        void    setLightProperty(I32 pLightId, ELighting pProperty, const F32* pProperties);
//        void    enableLight(I32 pLightId, bool pActive);
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "Device.inl"

#endif  // De GDV_DEVICE_HH
