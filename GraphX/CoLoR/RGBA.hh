/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CLR_RGBA_HH
#define CLR_RGBA_HH

#include "CoLoR.hh"

/*! @file GraphX/CoLoR/RGBA.hh
    @brief En-t�te de la classe CLR::RGBA.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 8 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"
#include "MEMory/Array.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Spectre RGBA.
        @version 0.8
        @ingroup CoLoR

        Cette classe permet de manipuler des spectres RGBA. Une classe est utilils�e plut�t qu'une
        structure afin de g�rer les constructeurs, bien que sa gestion se rapproche plus de celle d'un
        POD.
        @sa CLR::RGB
     */
    class RGBA : public MEM::Auto<RGBA>
    {
    public:
        //! @name Constructeurs
        //@{
                            RGBA();                                 //!< ... par d�faut.
                            RGBA(F32 pR, F32 pG, F32 pB, F32 pA);   //!< ... � partir d'un quadruplet de couleurs.
                explicit    RGBA(const F32 pRGBA[4]);               //!< ... � partir d'un tableau de 4 couleurs.
                explicit    RGBA(const RGB& pRGB);                  //!< ... � partir d'une instance de CLR::RGB.
                explicit    RGBA(U32 pRGB);                         //!< ... � partir d'un entier.
        //@}
        //! @name M�thodes d'acc�s
        //@{
        inline  const F32&  operator[](size_t pChannel) const;      //!< ... par indice (constante).
        inline  F32&        operator[](size_t pChannel);            //!< ... par indice.
        inline  F32         r() const;                              //!< ... � la couleur rouge.
        inline  F32         g() const;                              //!< ... � la couleur verte.
        inline  F32         b() const;                              //!< ... � la couleur bleue.
        inline  F32         a() const;                              //!< ... � la valeur de transparence.
        //@}
        //! @name Transformations par canal
        //@{
                const RGBA  operator<<(U32 pShift) const;           //!< Multiplication, par canal.
                const RGBA  operator>>(U32 pShift) const;           //!< Division, par canal.
        //@}
        //! @name Conversion
        //@{
                            operator U32 () const;                  //!< Conversion en entier.
        //@}

    private:

        MEM::Array<F32, 4>  mChannels;                              //!< Couleurs.
    };


    //! @name Operateurs
    //@{
    bool    operator==(const RGBA& pL, const RGBA& pR);           //!< ... d'�galit�.
    bool    operator!=(const RGBA& pL, const RGBA& pR);           //!< ... d'in�galit�.
    //@}


//------------------------------------------------------------------------------
//                           Affichage d'une Couleur 
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const RGBA& pToPrint);   //!< Ecriture d'une couleur.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "RGBA.inl"

#endif  // De CLR_RGBA_HH
