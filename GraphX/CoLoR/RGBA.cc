/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "RGBA.hh"

/*! @file GraphX/CoLoR/RGBA.cc
    @brief M�thodes (non-inline) de la classe CLR::RGBA.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 8 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Log.hh"
#include "UTIlity/Template.hh"

#include "RGB.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : couleur noire et opaque.
     */
    RGBA::RGBA()
        : mChannels(k0F, k0F, k0F, F32(1.0F))
    { }


    /*! Constructeur � partir d'un quadruplet de couleurs.
        @param pR Rouge
        @param pG Vert
        @param pB Bleu
        @param pA Transparence (pas vraiment une couleur, mais bon...)
     */
    RGBA::RGBA(F32 pR, F32 pG, F32 pB, F32 pA)
        : mChannels(pR, pG, pB, pA)
    { }


    /*! Constructeur � partir d'un tableau de couleurs.
        @param pRGBA Tableau comportant [au moins] 4 �l�ments
     */
    RGBA::RGBA(const F32 pRGBA[4])
        : mChannels(pRGBA[eR], pRGBA[eG], pRGBA[eB], pRGBA[eA])
    { }


    /*! Constructeur � partir d'une instance de CLR::RGB.
        @param pRGB Couleur sans alpha (qui sera initialis� � 1.0F)
     */
    RGBA::RGBA(const RGB& pRGB)
        : mChannels(pRGB.r(), pRGB.g(), pRGB.b(), F32(1.0F))
    { }


    /*! @param pRGB Couleur sous forme d'entier 32 bits (0xAABBGGRR).
     */
    RGBA::RGBA(U32 pRGB)
        : mChannels(F32{((pRGB & 0x000000FF) >>  0) / 255.0F},
                    F32{((pRGB & 0x0000FF00) >>  8) / 255.0F},
                    F32{((pRGB & 0x00FF00FF) >> 16) / 255.0F},
                    F32{((pRGB & 0xFF000000) >> 24) / 255.0F})
    { }


//------------------------------------------------------------------------------
//                           Transformations par Canal
//------------------------------------------------------------------------------

    /*! Multiplie chaque canal par une valeur.
        @param pShift Le multiplicateur appliqu� sur chaque canal vaut 2 ^ <i>pShift</i>
     */
    const RGBA RGBA::operator<<(U32 pShift) const
    {
        // Un d�calage nul ne modifie pas les valeurs des canaux.
        if (0 < pShift)
        {
            F32 lMul = F32(0x00000001 << (pShift - 1));

            return RGBA(r() * lMul, g() * lMul, b() * lMul, a() * lMul);
        }
        else
        {
            return *this;
        }
    }


    /*! Divise chaque cancl par une valeur.
        @param pShift Le diviseur appliqu� sur chaque canal vaut 2 ^ <i>pShift</i>
     */
    const RGBA RGBA::operator>>(U32 pShift) const
    {
        if (0 < pShift)
        {
            F32 lDiv = F32(0x00000001 << (pShift - 1));

            return RGBA(r() / lDiv, g() / lDiv, b() / lDiv, a() / lDiv);
        }
        else
        {
            return *this;
        }
    }


//------------------------------------------------------------------------------
//                                  Conversion
//------------------------------------------------------------------------------

    /*! Convertit la valeur de l'instance en entier 32 bits (R : bits poids faible, A : bits poids fort).
        @return La valeur de *<b>this</b> sous forme d'entier 32 bits (0xAABBGGRR).
     */
    RGBA::operator U32() const
    {
        U32 lRGBA =                   U32(U32::TType(a() * 255.0F));
        lRGBA     = U32((lRGBA << 0x08) + U32::TType(b() * 255.0F));
        lRGBA     = U32((lRGBA << 0x08) + U32::TType(g() * 255.0F));
        lRGBA     = U32((lRGBA << 0x08) + U32::TType(r() * 255.0F));

        return lRGBA;
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup CoLoR
        @param pL Premier op�rande de la comparaison
        @param pR Second  op�rande de la comparaison
        @return VRAI si les couleurs sont identiques, FAUX sinon
     */
    bool operator==(const RGBA& pL, const RGBA& pR)
    {
        return UTI::cmpN<4>(&pL[0], &pR[0]);
    }


    /*! @ingroup CoLoR
        @param pL Premier op�rande de la comparaison
        @param pR Second  op�rande de la comparaison
        @return FAUX si les couleurs sont identiques, VRAI sinon
     */
    bool operator!=(const RGBA& pL, const RGBA& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                           Affichage d'une Couleur 
//------------------------------------------------------------------------------

    /*! Affichage d'une couleur sous la forme :<br>
        { R; G; B; A }
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Couleur � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const RGBA& pToPrint)
    {
        pLog << "{ " << pToPrint.r() << "; " << pToPrint.g() << "; " << pToPrint.b() << "; " << pToPrint.a() << " }";

        return pLog;
    }
}
