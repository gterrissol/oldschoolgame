/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CLR_RGB_HH
#define CLR_RGB_HH

#include "CoLoR.hh"

/*! @file GraphX/CoLoR/RGB.hh
    @brief En-t�te de la class CLR::RGB.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 8 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"
#include "MEMory/Array.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Spectre RGB.
        @version 0.8
        @ingroup CoLoR

        Cette classe permet de manipuler des spectres RGB. Une classe est utilils�e plut�t qu'une
        structure afin de g�rer les constructeurs, bien que sa gestion se rapproche plus de celle d'un
        POD.
        @sa CLR::RGBA
     */
    class RGB : public MEM::Auto<RGB>
    {
    public:
        //! @name Constructeurs
        //@{
                            RGB();                              //!< ... par d�faut.
                            RGB(F32 pR, F32 pG, F32 pB);        //!< ... � partir d'un triplet de couleurs.
                explicit    RGB(const F32 pRGB[3]);             //!< ... � partir d'un tableau de 3 couleurs.
                explicit    RGB(U32 pRGB);                      //!< ... � partir d'un entier.
        //@}
        //! @name M�thodes d'acc�s
        //@{
        inline  const F32&  operator[](size_t pChannel) const;  //!< ... par indice (constante).
        inline  F32&        operator[](size_t pChannel);        //!< ... par indice.
        inline  F32         r() const;                          //!< ... � la couleur rouge.
        inline  F32         g() const;                          //!< ... � la couleur verte.
        inline  F32         b() const;                          //!< ... � la couleur bleue.
        //@}
        //! @name Transformations par canal
        //@{
                const RGB   operator<<(U32 pShift) const;       //!< Multiplication, par canal.
                const RGB   operator>>(U32 pShift) const;       //!< Division, par canal.
        //@}
        //! @name Conversion
        //@{
                            operator U32() const;               //!< Conversion en entier.
        //@}

    private:

        MEM::Array<F32, 3>  mChannels;                          //!< Couleurs.
    };


    //! @name Operateurs
    //@{
    bool    operator==(const RGB& pL, const RGB& pR);         //!< ... d'�galit�.
    bool    operator!=(const RGB& pL, const RGB& pR);         //!< ... d'in�galit�.
    //@}


//------------------------------------------------------------------------------
//                           Affichage d'une Couleur 
//------------------------------------------------------------------------------

    LOG::Log&   operator<<(LOG::Log& pLog, const RGB& pToPrint);    //!< Ecriture d'une couleur.
}


//------------------------------------------------------------------------------
//                                M�thodes inline
//------------------------------------------------------------------------------

#include "RGB.inl"

#endif  // De CLR_RGB_HH
