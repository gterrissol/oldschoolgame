/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "RGB.hh"

/*! @file GraphX/CoLoR/RGB.cc
    @brief M�thodes (non-inline) de la classe CLR::RGB.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 8 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/Log.hh"
#include "UTIlity/Template.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//                                 Constructeurs
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut : couleur noire.
     */
    RGB::RGB()
        : mChannels(k0F, k0F, k0F)
    { }


    /*! Constructeur � partir d'un triplet de couleurs.
        @param pR Rouge
        @param pG Vert
        @param pB Bleu
     */
    RGB::RGB(F32 pR, F32 pG, F32 pB)
        : mChannels(pR, pG, pB)
    { }


    /*! Constructeur � partir d'un tableau de couleurs.
        @param pRGB Tableau comportant [au moins] 3 �l�ments
     */
    RGB::RGB(const F32 pRGB[3])
        : mChannels(pRGB[eR], pRGB[eG], pRGB[eB])
    { }


    /*! @param pRGB Couleur sous forme d'entier 32 bits (0xXXBBGGRR).
     */
    RGB::RGB(U32 pRGB)
        : mChannels(F32{((pRGB & 0x000000FF) >>  0) / 255.0F},
                    F32{((pRGB & 0x0000FF00) >>  8) / 255.0F},
                    F32{((pRGB & 0x00FF0000) >> 16) / 255.0F})
    { }


//------------------------------------------------------------------------------
//                           Transformations par Canal
//------------------------------------------------------------------------------

    /*! Multiplie chaque canal par une valeur.
        @param pShift Le multiplicateur appliqu� sur chaque canal vaut 2 ^ <i>pShift</i>
     */
    const RGB RGB::operator<<(U32 pShift) const
    {
        // Un d�calage nul ne modifie pas les valeurs des canaux.
        if (0 < pShift)
        {
            F32 lMul = F32(0x00000001 << (pShift - 1));

            return RGB(r() * lMul, g() * lMul, b() * lMul);
        }
        else
        {
            return *this;
        }
    }


    /*! Divise chaque cancl par une valeur.
        @param pShift Le diviseur appliqu� sur chaque canal vaut 2 ^ <i>pShift</i>
     */
    const RGB RGB::operator>>(U32 pShift) const
    {
        if (0 < pShift)
        {
            F32 lDiv = F32(0x00000001 << (pShift - 1));

            return RGB(r() / lDiv, g() / lDiv, b() / lDiv);
        }
        else
        {
            return *this;
        }
    }


//------------------------------------------------------------------------------
//                                  Conversion
//------------------------------------------------------------------------------

    /*! Convertit la valeur de l'instance en entier 32 bits (R : bits poids faible, A (forc� � 255) : bits poids fort).
        @return La valeur de *<b>this</b> sous forme d'entier 32 bits (0xAABBGGRR).
     */
    RGB::operator U32() const
    {
        U32 lRGB =                                   U32(255);  // Valeur alpha en RGB : 1.0F.
        lRGB     = U32((lRGB << 0x08) + U32::TType(b() * 255.0F));
        lRGB     = U32((lRGB << 0x08) + U32::TType(g() * 255.0F));
        lRGB     = U32((lRGB << 0x08) + U32::TType(r() * 255.0F));

        return lRGB;
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @ingroup CoLoR
        @param pL Premier op�rande de la comparaison
        @param pR Second  op�rande de la comparaison
        @return VRAI si les couleurs sont identiques, FAUX sinon
     */
    bool operator==(const RGB& pL, const RGB& pR)
    {
        return UTI::cmpN<3>(&pL[0], &pR[0]);
    }


    /*! @ingroup CoLoR
        @param pL Premier op�rande de la comparaison
        @param pR Second  op�rande de la comparaison
        @return FAUX si les couleurs sont identiques, VRAI sinon
     */
    bool operator!=(const RGB& pL, const RGB& pR)
    {
        return !(pL == pR);
    }


//------------------------------------------------------------------------------
//                           Affichage d'une Couleur 
//------------------------------------------------------------------------------

    /*! Affichage d'une couleur sous la forme :<br>
        { R; G; B }
        @param pLog     Log sur lequel afficher <i>pToPrint</i>
        @param pToPrint Couleur � afficher
        @return <i>pLog</i>
     */
    LOG::Log& operator<<(LOG::Log& pLog, const RGB& pToPrint)
    {
        pLog << "{ " << pToPrint.r() << "; " << pToPrint.g() << "; " << pToPrint.b() << " }";

        return pLog;
    }
}
