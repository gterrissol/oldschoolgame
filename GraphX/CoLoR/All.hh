/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CLR_ALL_HH
#define CLR_ALL_HH

/*! @file GraphX/CoLoR/All.hh
    @brief Interface publique du module @ref CoLoR.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 12 Octobre 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CLR   //! Gestion des couleurs.
{
    /*! @namespace CLR
        @version 0.6

        Cet espace de noms permet de manipuler les spectres (RGB, RGBA,...).
     */

    /*! @defgroup CoLoR CoLor : Couleurs
        <b>namespace</b> CLR.
     */

    /*! @name defgroup CLR_Colors Couleurs standard
        @ingroup CoLoR
     */
}

#include "RGB.hh"
#include "RGBA.hh"
#include "Colors.hh"

#endif  // De CLR_ALL_HH
