/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/CoLoR/RGBA.inl
    @brief M�thodes inline de la classe CLR::RGBA.
    @author @ref Guillaume_Terrissol
    @date 1 Avril 2002 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Enum.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//                               M�thodes d'Acc�s
//------------------------------------------------------------------------------

    /*! @param pChannel Indice du canal souhait�
        @return La valeur du canal <i>pIndex</i>
     */
    inline const F32& RGBA::operator[](size_t pChannel) const
    {
        return mChannels[pChannel];
    }


    /*! @param pChannel Indice du canal souhait�
        @return La valeur du canal <i>pIndex</i>
     */
    inline F32& RGBA::operator[](size_t pChannel)
    {
        return mChannels[pChannel];
    }


    /*! @return La valeur du canal rouge
     */
    inline F32 RGBA::r() const
    {
        return mChannels[eR];
    }


    /*! @return La valeur du canal vert
     */
    inline F32 RGBA::g() const
    {
        return mChannels[eG];
    }


    /*! @return La valeur du canal bleu
     */
    inline F32 RGBA::b() const
    {
        return mChannels[eB];
    }


    /*! @return La valeur du canal alpha
     */
    inline F32 RGBA::a() const
    {
        return mChannels[eA];
    }
}
