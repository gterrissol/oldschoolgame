/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CLR_ENUM_HH
#define CLR_ENUM_HH

/*! @file GraphX/CoLoR/Enum.hh
    @brief Enumérations du module RSC.
    @author @ref Guillaume_Terrissol
    @date 19 Mars 2008 - 14 Septembre 2011
    @note Ce fichier est diffusé sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CLR
{
//------------------------------------------------------------------------------
//                                 Enumérations
//------------------------------------------------------------------------------

    //! Couleurs
    enum EColors
    {
        eR = 0,
        eG,
        eB,
        eA
    };
}

#endif // De CLR_ENUM_HH
