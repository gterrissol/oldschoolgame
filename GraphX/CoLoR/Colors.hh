/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CLR_COLORS_HH
#define CLR_COLORS_HH

#include "CoLoR.hh"

/*! @file GraphX/CoLoR/Colors.hh
    @brief Couleurs standard.
    @author @ref Guillaume_Terrissol
    @date 12 Octobre 2008 - 14 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "RGB.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//                               Couleurs Standard
//------------------------------------------------------------------------------

    extern  const RGB   kBlack;     //!< Noir.
    extern  const RGB   kBlue;      //!< Bleu.
    extern  const RGB   kCyan;      //!< Cyan.
    extern  const RGB   kDarkGrey;  //!< Gris sombre.
    extern  const RGB   kGreen;     //!< Vert.
    extern  const RGB   kGrey;      //!< Gris.
    extern  const RGB   kLightGrey; //!< Gris clair.
    extern  const RGB   kMagenta;   //!< Magenta.
    extern  const RGB   kRed;       //!< Rouge.
    extern  const RGB   kWhite;     //!< Blanc.
    extern  const RGB   kYellow;    //!< Jaune.
}

#endif  // De CLR_COLORS_HH
