/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "CoLoR.hh"

/*! @file GraphX/CoLoR/CoLoR.cc
    @brief D�finitions diverses du module CLR.
    @author @ref Guillaume_Terrissol
    @date 23 Ao�t 2002 - 19 Juin 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"
#include "MATH/Const.hh"

#include "Colors.hh"
#include "RGB.hh"

namespace CLR
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Message d'assertion.
    REGISTER_ERR_MSG(kRangeCheckError, "Valeur d'indice incorrecte.", "Range check error.")


//------------------------------------------------------------------------------
//                               Couleurs Standard
//------------------------------------------------------------------------------

    namespace
    {
        const F32   k0_25F  = F32(0.25F);
        const F32   k0_5F   = F32(0.5F);
        const F32   k0_75F  = F32(0.75F);
    }


    /*! @ingroup CLR_Colors
        Couleur noire.
     */
    const RGB   kBlack      = RGB(k0F, k0F, k0F);


    /*! @ingroup CLR_Colors
        Couleur bleue.
     */
    const RGB   kBlue       = RGB(k0F, k0F, k1F);


    /*! @ingroup CLR_Colors
        Couleur cyan.
     */
    const RGB   kCyan       = RGB(k0F, k1F, k1F);


    /*! @ingroup CLR_Colors
        Couleur gris sombre.
     */
    const RGB   kDarkGrey   = RGB(k0_25F, k0_25F, k0_25F);


    /*! @ingroup CLR_Colors
        Couleur verte.
     */
    const RGB   kGreen      = RGB(k0F, k1F, k0F);


    /*! @ingroup CLR_Colors
        Couleur grise.
     */
    const RGB   kGrey       = RGB(k0_5F, k0_5F, k0_5F);


    /*! @ingroup CLR_Colors
        Couleur gris clair.
     */
    const RGB   kLightGrey  = RGB(k0_75F, k0_75F, k0_75F);


    /*! @ingroup CLR_Colors
        Couleur magenta.
     */
    const RGB   kMagenta    = RGB(k1F, k0F, k1F);


    /*! @ingroup CLR_Colors
        Couleur rouge.
     */
    const RGB   kRed        = RGB(k1F, k0F, k0F);


    /*! @ingroup CLR_Colors
        Couleur blanche.
     */
    const RGB   kWhite      = RGB(k1F, k1F, k1F);


    /*! @ingroup CLR_Colors
        Couleur jaune.
     */
    const RGB   kYellow     = RGB(k1F, k1F, k0F);
}
