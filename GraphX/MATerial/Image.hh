/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_IMAGE_HH
#define MAT_IMAGE_HH

#include "MATerial.hh"

/*! @file GraphX/MATerial/Image.hh
    @brief En-t�te de la classe MAT::Image & MAT::ImageRsc.
    @author @ref Guillaume_Terrissol & @ref Vincent_David
    @date 25 Septembre 2002 - 28 Mars 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/PAcKage.hh"
#include "ReSourCe/Object.hh"
#include "ReSourCe/ResourceMgr.hh"

#include "Enum.hh"

namespace MAT
{
    /*! @brief Classe image.
        @version 0.35
        @ingroup MATerial

        Cette classe permet de manipuler les images 2D dans le moteur.
        @sa MAT::Texture
     */
    class Image : public RSC::Object
    {
        //! @name Classes amies
        //@{
        template<class TObj>
 friend class RSC::Local;                                   //!< Pour l'acc�s au constructeur par d�faut.
 friend class ImageController;                              //!< Pour les m�thodes d'�dition.
        //@}
    public:

        using   Ptr = SharedPtr<Image>;                     //!< Pointeur sur image.
        //! @name Constructeurs & destructeur
        //@{
                    Image();                                //!< Constructeur par d�faut.
                    Image(PAK::Loader& pLoader);            //!< Constructeur (chargement imm�diat).
virtual             ~Image();                               //!< Destructeur.
        //@}
        //! @name Propri�t�s
        //@{
        EFormat     format() const;                         //!< Format (nombre de bits par pixel, entre autres).
        U16         width() const;                          //!< Largeur.
        U16         height() const;                         //!< Hauteur.
        U16         level() const;                          //!< Niveau de r�solution.
        U32         size() const;                           //!< Taille des donn�es (en octets).
        const void* data() const;                           //!< Donn�es.
        //@}
        //! @name Gestion haut-niveau
        //@{
        void        activate(U32 pUnit) const;              //!< Activation de l'image en tant que texture.
        void        bind() const;                           //!< Cr�ation de la texture OpenGL.
        //@}

    protected:
        //! @name Autre constructeur
        //@{
                    Image(PAK::FileHdl pFileHdl);           //!< Constructeur (chargement diff�r�).
        //@}
        //! @name D�finitions
        //@{
        void        setFormat(EFormat pPixelFormat);        //!< D�finition du format.
        void        setWidth(U16 pWidth);                   //!< Redimensionnement (largeur).
        void        setHeight(U16 pHeight);                 //!< Redimensionnement (hauteur).
        void        setLevel(U16 pNewLevel);                //!< Niveau de texture (mip-map).
        void        setData(const void* pData);             //!< D�finition des donn�es de l'image.
        void        setLine(U16 pLine, const void* pData);  //!< D�finition des donn�es d'une ligne de l'image.
        //@}
        //! @name Gestion des donn�es
        //@{
        void        swap(Image& pImage);                    //!< Permutation des donn�es.
virtual void        store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        void        keepDataAfterBind(bool pKeep);          //!< N�cessit� des donn�es post-"bind".
        //@}

    private:

        FORBID_COPY(Image)
        PIMPL()
    };

    using   ImageRsc    = RSC::Resource<Image>;             //!< Ressource image.
    using   ImageMgr    = RSC::ResourceMgr<ImageRsc>;       //!< Gestionnaire d'images.
    using   Key         = ImageMgr::TKey;                   //!< Clef pour une MAT::Image.
}

#endif  // De MAT_IMAGE_HH
