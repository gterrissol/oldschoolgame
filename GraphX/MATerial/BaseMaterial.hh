/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_BASEMATERIAL_HH
#define MAT_BASEMATERIAL_HH

#include "MATerial.hh"

/*! @file GraphX/MATerial/BaseMaterial.hh
    @brief En-t�te de la classe MAT::Material.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 16 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/CoLoR.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "PAcKage/PAcKage.hh"
#include "SHaDers/Library.hh"
#include "STL/STL.hh"

namespace MAT
{
//------------------------------------------------------------------------------
//                                   Mat�riau
//------------------------------------------------------------------------------

    /*! @brief Classe mat�riau.
        @version 0.25
        @ingroup MATerial

     */
    class Material : public MEM::OnHeap
    {
        //! @name Classes amies
        //@{
 friend class MaterialController;                                   //!< Pour les m�thodes d'�dition.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        using Ptr               = SharedPtr<Material>;              //!< Pointeur sur mat�riau.
        using ConstTexturePtr   = SharedPtr<Texture const>;         //!< Pointeur (constant) sur texture.
        using TexturePtr        = SharedPtr<Texture>;               //!< Pointeur sur texture.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                            Material();                             //!< Constructeur par d�faut.
                            Material(const Material& pMaterial);    //!< Constructeur par copie.
            Material&       operator=(const Material& pMaterial);   //!< Op�rateur d'affectation.
                            Material(PAK::Loader& pLoader);         //!< Constructeur (chargement imm�diat).
                            ~Material();                            //!< Destructeur.
        //@}
        //! @name "Permanence" des donn�es
        //@{
        void                store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //@}
        //! @name Propri�t�s
        //@{
        const CLR::RGBA&    ambient() const;                        //!< 
        const CLR::RGBA&    diffuse() const;                        //!< 
        const CLR::RGBA&    specular() const;                       //!< 
        const CLR::RGBA&    emissive() const;                       //!< 
        F32                 alpha() const;                          //!< 
        SHD::Program::Id    program() const;                        //!< 
        //@}
        //! @name Textures
        //@{
        //! Enum�ration pour les textures nomm�es.
        enum
        {
            eTexture,                                               //!< Texture.
            eTextureName                                            //!< Nom de la texture.
        };
        using NamedTexture = std::tuple<TexturePtr, String>;
        U32                 textureCount() const;                   //!< 
        ConstTexturePtr     texture(U32 pN) const;                  //!< 

        NamedTexture        namedTexture(U32 pN) const; //!< 
        //@}

    protected:
        //! @name M�thodes d'�dition
        //@{
        void                setAmbient(const CLR::RGBA& pRGBA);     //!< 
        void                setDiffuse(const CLR::RGBA& pRGBA);     //!< 
        void                setSpecular(const CLR::RGBA& pRGBA);    //!< 
        void                setEmissive(const CLR::RGBA& pRGBA);    //!< 
        void                setProgram(SHD::Program::Id pProgram);  //!< 
        TexturePtr          texture(I16 pN);                        //!< 
        void                addTexture(TexturePtr pTexture,
                                       String     pName);           //!< 
        //@}

    private:

        PIMPL()
    };


    //! @name Operateurs
    //@{
    bool    operator==(const Material& pL, const Material& pR); //!< ... d'�galit�.
    bool    operator!=(const Material& pL, const Material& pR); //!< ... d'in�galit�.
    //@}
}

#endif  // De MAT_BASEMATERIAL_HH
