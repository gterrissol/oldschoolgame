/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_ALL_HH
#define MAT_ALL_HH

/*! @file GraphX/MATerial/All.hh
    @brief Interface publique du module @ref MATerial.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MAT   //! Mat�riaux.
{
    /*! @namespace MAT
        @version 0.3

        Gestion des mat�riaux.
     */

    /*! @defgroup MATerial MATerial : Mat�riaux
        <b>namespace</b> MAT.
     */
}

#include "Image.hh"
#include "Texture.hh"
#include "BaseMaterial.hh"

#endif  // De MAT_ALL_HH
