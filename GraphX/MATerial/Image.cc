/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Image.hh"

/*! @file GraphX/MATerial/Image.cc
    @brief M�thodes (non-inline) de la classe MAT::Image.
    @author @ref Guillaume_Terrissol & @ref Vincent_David
    @date 25 Septembre 2002 - 28 Mars 2020
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>

#include "PAcKage/LoaderSaver.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace MAT
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de MAT::Image.
        @version 0.2
     */
    class Image::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
                        Private();                      //!< Constructeur par d�faut.
                        Private(PAK::FileHdl pFileHdl); //!< Constructeur (chargement diff�r�).
                        Private(PAK::Loader& pLoader);  //!< Constructeur (chargement imm�diat).
                        ~Private();
        //@}
        //! @name Taille des donn�es
        //@{
        inline  U32     size() const;                   //!< Taille de l'image, en octets.
        inline  void    resizeData();                   //!< Redimensionnement du tableau de donn�es.
        //@}
        //! @name Attributs
        //@{
        Vector<U32> mData;                              //!< Donn�es.
        U32         mTextureId;                         //!< Identifiant de texture OpenGL.
        EFormat     mFormat;                            //!< "Format".
        U16         mWidth;                             //!< Largeur.
        U16         mHeight;                            //!< Hauteur.
        U16         mLevel;                             //!< Niveau de r�solution.
        bool        mKeepData;                          //!< Conservation des donn�es post-bind ?
        //@}
    };


//------------------------------------------------------------------------------
//                     P-Impl : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @copydoc MAT::Image::Image()
     */
    Image::Private::Private()
        : mData()
        , mTextureId(0)
        , mFormat(EFormat(e32Bits | eRGBA))
        , mWidth(1)
        , mHeight(1)
        , mLevel(0)
        , mKeepData(false)
    {
        // Blanc.
        mData.resize(1, U32(0xFFFFFFFF));
    }


    /*! @copydoc MAT::Image::Image(PAK::FileHdl)
     */
    Image::Private::Private(PAK::FileHdl)
        : mData()
        , mTextureId(0)
        , mFormat(eNone)
        , mWidth(0)
        , mHeight(0)
        , mLevel(0)
        , mKeepData(false)
    { }


    /*! @copydoc MAT::Image::Image(PAK::Loader&)
     */
    Image::Private::Private(PAK::Loader& pLoader)
        : mData()
        , mTextureId(0)
        , mFormat(eNone)
        , mWidth(0)
        , mHeight(0)
        , mLevel(0)
        , mKeepData(false)
    {
        // Format des donn�es.
        pLoader >> mFormat;

        // Dimensions & niveau de r�solution.
        pLoader >> mWidth;
        pLoader >> mHeight;
//        pLoader >> mLevel;

        // Redimensionnement.
        resizeData();

        // Lecture des pixels.
        U32  lSizeIn4ByteBlocks = U32((size() - 1) / 4 + 1);

        for(U32 lLong = k0UL; lLong < lSizeIn4ByteBlocks; ++lLong)
        {
            pLoader >> mData[lLong];
        }
    }


    /*! Destructeur.
     */
    Image::Private::~Private()
    {
        if (mTextureId != 0)
        {
#if   (GXD_IMPL == 0)
            glDeleteTextures(1, reinterpret_cast<GLuint*>(addressof(mTextureId)));
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL
        }
    }


//------------------------------------------------------------------------------
//                          P-Impl : Taille des donn�es
//------------------------------------------------------------------------------

    /*! @return La taille de l'image, en octets
     */
    inline U32 Image::Private::size() const
    {
        return U32(U32(mFormat & eByteCount) * mWidth * mHeight);
    }


    /*! Redimensionnement du tableau de donn�es.
     */
    inline void Image::Private::resizeData()
    {
        U32  lSizeIn4ByteBlocks = U32((size() - 1) / 4 + 1);
        // NB: en cas de rapetissement du tableau, la capacit� restant telle
        // quelle, les donn�es ne sont pas perdues (pour les undos).
        mData.resize(lSizeIn4ByteBlocks, k0UL);
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.<br>
        Par d�faut, une image est un bloc de 1 pixel blanc (cod� sur 32 bits).
     */
    Image::Image()
        : pthis()
    { }


    /*! Constructeur (chargement imm�diat).
        @param pLoader Fichier dans lequel lire les donn�es de l'image
     */
    Image::Image(PAK::Loader& pLoader)
        : pthis(pLoader)
    { }


    /*! Destructeur.
     */
    Image::~Image() { }


//------------------------------------------------------------------------------
//                                  Propri�t�s
//------------------------------------------------------------------------------

    /*! @return Le format de l'image (qui correspond au nombre de bits par pixel : 8, 16, 24 ou 32).
        @sa e_ImageFormat
     */
    EFormat Image::format() const
    {
        return pthis->mFormat;
    }


    /*!@return La largeur de l'image
     */
    U16 Image::width() const
    {
        return pthis->mWidth;
    }


    /*! @return La hauteur de l'image
     */
    U16 Image::height() const
    {
        return pthis->mHeight;
    }


    /*! @return La niveau de r�solution de l'image
     */
    U16 Image::level() const
    {
        return pthis->mLevel;
    }


    /*! @return La taille de l'image, en octets
     */
    U32 Image::size() const
    {
        return pthis->size();
    }


    /*! @return Un pointeur sur les donn�es de l'image
     */
    const void* Image::data() const
    {
        return &(pthis->mData[0]);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Activation de l'image en tant que texture.
        @param pUnit Unit� de texture � activer pour la texture.
     */
    void Image::activate(U32 pUnit) const
    {
        if (pthis->mTextureId == 0)
        {
            bind();
        }
        glActiveTexture(GL_TEXTURE0 + pUnit);
        glBindTexture(GL_TEXTURE_2D, pthis->mTextureId);
    }


    /*! Cr�ation de la texture OpenGL.
     */
    void Image::bind() const
    {
        // 0 est utilis� par OpenGL pour la texture par d�faut, et n'est
        // donc pas un nom de texture retourn� par glGenTextures.
        if (pthis->mTextureId == 0)
        {
            // Nom g�n�r� au besoin.
            glGenTextures(1, reinterpret_cast<GLuint*>(addressof(pthis->mTextureId)));
        }

        // Binding OpenGL.
        activate(k0UL);

        GLint   lLevel      = level();
        GLint   lComponents = [](int pByteCount)
                              {
                                  switch(pByteCount)
                                  {
                                      case e8Bits:  return GL_R8;
                                      case e16Bits: return GL_RG8;
                                      case e24Bits: return GL_RGB8;
                                      case e32Bits: return GL_RGBA8;
                                      default:      ASSERT_EX(false, kInvalidFormat, return 0;)
                                  }
                              }(format() & eByteCount);
        GLenum  lFormat     = [](int pChannels)
                              {
                                  switch(pChannels)
                                  {
                                      case eR:    return GL_LUMINANCE;
                                      case eRA:   return GL_LUMINANCE_ALPHA;
                                      case eRGB:  return GL_RGB;
                                      case eRGBA: return GL_RGBA;
                                      default:    ASSERT_EX(false, kInvalidFormat, return 0;)
                                  }
                              }(format() & eChannels);

        glTexImage2D(GL_TEXTURE_2D, lLevel, lComponents, width(), height(), 0, lFormat, GL_UNSIGNED_BYTE, data());

        // Encore besoin des donn�es ?
        if (!pthis->mKeepData)
        {
            const_cast<Image*>(this)->setData(nullptr);
        }
    }


//------------------------------------------------------------------------------
//                              Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur (chargement diff�r�).
        @param pFileHdl Handle du fichier contenant les donn�es de l'instance
     */
    Image::Image(PAK::FileHdl pFileHdl)
        : RSC::Object(pFileHdl)
        , pthis()
    { }


//------------------------------------------------------------------------------
//                                  D�finitions
//------------------------------------------------------------------------------

    /*! D�finition du format.
     */
    void Image::setFormat(EFormat pPixelFormat)
    {
        pthis->mFormat = pPixelFormat;

        // Redimensionnement.
        pthis->resizeData();
    }


    /*! Redimensionnement (largeur).
     */
    void Image::setWidth(U16 pWidth)
    {
        pthis->mWidth = pWidth;

        // Redimensionnement.
        pthis->resizeData();
    }


    /*! Redimensionnement (hauteur).
     */
    void Image::setHeight(U16 pHeight)
    {
        pthis->mHeight = pHeight;

        // Redimensionnement.
        pthis->resizeData();
    }


    /*! Niveau de texture (mip-map).
     */
    void Image::setLevel(U16 pNewLevel)
    {
        pthis->mLevel = pNewLevel;
    }


    /*! D�finition des donn�es de l'image.
        @warning <i>Data</i> doit pointer sur un bloc de donn�es de taille au moins �gale � size() octets
        @note N'oubliez pas d'appeler bind() pour tenir compte des modifications
     */
    void Image::setData(const void* pData)
    {
        if (pData != nullptr)
        {
            const char* lBegin    = static_cast<const char*>(pData);
            const char* lEnd      = lBegin + size();
            char*       lDst      = reinterpret_cast<char*>(&pthis->mData[0]);

            std::copy(lBegin, lEnd, lDst);
        }
        else
        {
            // Vide compl�tement la m�moire associ�e aux donn�es de l'image.
            decltype(pthis->mData)().swap(pthis->mData);
        }
    }


    /*! D�finition des donn�es d'une ligne de l'image.
        @note N'oubliez pas d'appeler bind() pour tenir compte des modifications
     */
    void Image::setLine(U16 pLine, const void* pData)
    {
        U32 lPixelSize    = U32(format() & eByteCount);
        U32 lWidthSize    = U32(width() * lPixelSize);
        
        const char* lBegin  = static_cast<const char*>(pData);
        const char* lEnd    = lBegin + lWidthSize;
        char*       lDst    = reinterpret_cast<char*>(&pthis->mData[0]) + pLine * lWidthSize;

        std::copy(lBegin, lEnd, lDst);
    }


//------------------------------------------------------------------------------
//                                Gestion des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void Image::store(PAK::Saver& pSaver) const
    {
        RSC::Object::store(pSaver);

        // Format des donn�es (sauv� sous forme d'entier 32 bits, pour �viter les probl�mes de taille des valeurs d'�num�ration).
        pSaver << format();

        // Dimensions.
        pSaver << width();
        pSaver << height();
//        pSaver << level();

        // Ecriture des pixels.
        const U32*  lBeginData = static_cast<const U32*>(data());
        const U32*  lEndData   = lBeginData + ((size() - 1) / 4 + 1);

        for(const U32* lData = lBeginData; lData != lEndData; ++lData)
        {
            pSaver << *lData;
        }
    }


    /*! Permutation des donn�es.
     */
    void Image::swap(Image& pImage)
    {
        pthis.swap(pImage.pthis);
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Dans la plupart des cas, il n'est pas n�cessaire de conserver les donn�es de l'image une fois
        qu'elles ont �t� "bind�es" dans une texture OpenGL (� l'exception des images �ditables, pour
        lesquelles il faudra appeler cette m�thode, avec <i>pKeep</i> � VRAI).
        @param pKeep VRAI pour conserver les donn�es de l'image apr�s le bind OpenGL, FAUX si elles
       	peuvent �tre d�truites
     */
    void Image::keepDataAfterBind(bool pKeep)
    {
        pthis->mKeepData = pKeep;
    }

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#if defined(_WIN32)
template<>  RSC::ResourceMgr<ImageRsc>* MEM::Singleton<RSC::ResourceMgr<ImageRsc>>::smThat  = nullptr;
#endif
}
