/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_ENUM_HH
#define MAT_ENUM_HH

/*! @file GraphX/MATerial/Enum.hh
    @brief Enum�rations du module MAT.
    @author @ref Guillaume_Terrissol & @ref Vincent_David
    @date 14 Novembre 2002 - 17 Juillet 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace MAT
{
    /*! Format d'image.
        @note Certaines combinaisons sont interdites, comme par exemple RGBA en 24 bits.
     */
    enum EFormat : U32::TType
    {
        eNone       = 0x00000000,   //!< Format ind�fini.

        e8Bits      = 0x00000001,   //!< Chaque texel contient sur 1 octet.
        e16Bits     = 0x00000002,   //!< Chaque texel contient sur 2 octets.
        e24Bits     = 0x00000003,   //!< Chaque texel contient sur 3 octets.
        e32Bits     = 0x00000004,   //!< Chaque texel contient sur 4 octets.
        eByteCount  = 0x0000000F,   //!< Masque pour les tailles de texel.

        eR          = 0x00000010,   //!< Un canal pour les niveaux de gris, l'alpha est toujours 1.
        eRA         = 0x00000020,   //!< Un canal pour les niveaux de gris, un canal pour l'alpha.
        eRGB        = 0x00000040,   //!< Un canal pour Rouge, un pour Vert, un pour Bleu, l'alpha est toujours 1.
        eRGBA       = 0x00000080,   //!< Un canal pour Rouge, un pour Vert, un pour Bleu, un pour Alpha.
        eChannels   = 0x000000F0,   //!< Masque pour les valeurs des canaux.

        eCompressed = 0x00000100    //!< La texture sera compress�e par la carte graphique, si possible.
    };


    /*! Filtrage de la texture.
        Le premier param�tre concerne le filtrage d'un niveau de texture et
        le second param�tre filtre entre plusieurs niveaux de texture (mipmaps).
     */
    enum EMipMap
    {
        eNearNone,
        eNearNear,
        eNearLinear,
        eLinearNone,
        eLinearNear,
        eLinearLinear
    };


    /*! Syst�me de placage de coordonn�es de texture.
     */
    enum EClamp
    {
        eRepeat,
        eClamp
    };


    /*! Syst�me de placage de coordonn�es de texture.
     */
    enum ECoordGen
    {
        eSpherical,     //!< Placage en longitude/latitude, l'axe vertical �tant Z.
        ePlanar,        //!< Placage sur le plan horizontal, l'axe vertical �tant Z.
        eCylindrical,   //!< Placage autour d'un cylindre vertical infini, l'axe vertical �tant Z.
        eOrtho,         //!< Placage de sorte que, dans le triangle, les coordonn�es de textures soient orthonormales.
        eManual         //!< Coordonn�es d�finies par l'utilisateur dans la g�om�trie.
    };


    /*! Propri�t�s de m�lange d'une texture.
        @note Plusieurs textures peuvent �tre m�lang�es sur un seul et m�me objet.
        @warning Certains effets ne sont pas compatibles sur un m�me objet.
     */
    enum EBlendFunc
    {
        eDecal,         //!< Le canal alpha (de la texture) module le placage des couleurs de la texture.
        eBlend,         //!< La couleur de la texture module le placage de la couleur de base avec la couleur de r�f�rence de la texture.
        eModulate       //!< Multiplie la couleur de la texture avec la couleur de base.
//        eAdd,           //!< Ajoute la couleur texture � la couleur de base, ce qui consiste � "�clairer" la couleur de base.
//        eBump,          //!< Perturbe la normale pour que l'objet �clair� paraisse bossel�.
//        eFur            //!< Etoffe le mod�le 3D pour qu'il paraisse porter de la fourrure.
    };


    /*! Quelques couleurs remarquables (opaques, 32 bits).
     */
    enum
    {
        eBlack      = 0xFFFFFFFF,   //!< Noir.
        eRed        = 0xFF0000FF,   //!< Rouge.
        eGreen      = 0xFF00FF00,   //!< Vert.
        eBlue       = 0xFFFF0000,   //!< Blue.
        eCyan       = 0xFFFFFF00,   //!< Cyan.
        eMagenta    = 0xFFFF00FF,   //!< Magenta.
        eYellow     = 0xFF00FFFF,   //!< Jaune.
        eWhite      = 0xFFFFFFFF    //!< Blanc.
    };
}

#endif // De MAT_ENUM_HH
