/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_TEXTURE_HH
#define MAT_TEXTURE_HH

#include "MATerial.hh"

/*! @file GraphX/MATerial/Texture.hh
    @brief En-t�te de la classe MAT::Texture.
    @author @ref Guillaume_Terrissol
    @date 25 Septembre 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/STL.hh"
#include "ReSourCe/Object.hh"

#include "Enum.hh"
#include "Image.hh"

namespace MAT
{

    /*! @brief Classe texture.
        @version 0.25
        @ingroup MATerial

     */
    class Texture : public MEM::OnHeap
    {
        //! @name Classes amies
        //@{
        template<class TObj>
 friend class RSC::Local;                                       //!< Pour l'acc�s au constructeur par d�faut.
 friend class TextureController;                                //!< Pour les m�thodes d'�dition.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        typedef SharedPtr<ImageRsc>     ImageRscPtr;            //!< Pointeur sur image.
        typedef SharedPtr<Image const>  ConstImagePtr;          //!< Pointeur (constant) sur image.
        typedef SharedPtr<Image>        ImagePtr;               //!< Pointeur sur image.
        //@}
        //! @name Constructeur et destructeur
        //@{
                        Texture(PAK::Loader& pLoader);          //!< Constructeur (chargement imm�diat).
virtual                 ~Texture();                             //!< Destructeur.
        //@}
        //! @name Propri�t�s
        //@{
        I16             imageCount() const;                     //!< Nombre d'images de la texture.
        EFormat         format() const;                         //!< Format de la texture.
        EMipMap         mipMapParams() const;                   //!< Param�tres de mip-mapping.
        EClamp          clamping() const;                       //!< Param�tres de clamping.
        ECoordGen       coordGen() const;                       //!< G�n�ration des coordonn�es.
        EBlendFunc      blendFunc() const;                      //!< Fonction de m�lange de la texture.
        //@}
        //! @name Utilisation
        //@{
        void            activate(U32 pUnit) const;              //!< Activation de la texture.
        //@}

    protected:
        //! @name Autre constructeur
        //@{
                        Texture();                             //!< Constructeur par d�faut.
        //@}
        //! @name Gestion des images
        //@{
        ConstImagePtr   image(I16 pwN) const;                   //!< Acc�s � l'(aux) image(s) de la texture.
        ConstImagePtr   currentImage() const;                   //!< Acc�s � l'image de la texture � utiliser.
        void            insertImage(ImageRscPtr pNewImage,
                                    I16 pN = I16(-1));          //!< Insertion d'une image.
        void            removeImage(I16 pN);                    //!< Retrait d'une image.
        void            setManualImage(ImagePtr pImage);        //!< D�finition manuelle de l'image.
        //@}
        //! @name D�finitions
        //@{
        void            setFormat(EFormat pNewFormat);          //!< ... du format de la texture.
        void            setMipMapParams(EMipMap pNewParams);    //!< ... des param�tres de mip-mapping.
        void            setClamping(EClamp pNewClamping);       //!< ... des param�tres de r�p�tition.
        void            setCoordGen(ECoordGen pNewCoordGen);    //!< ... de la g�n�ration des coordonn�es.
        void            setBlendFunc(EBlendFunc pNewBlend);     //!< ... du m�lange de la texture.
        //@}
        //! @name Gestion des donn�es
        //@{
virtual void            store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //@}

    private:

        FORBID_COPY(Texture)
        PIMPL()
    };
}

#endif  // De MAT_TEXTURE_HH
