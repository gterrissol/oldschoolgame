/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef MAT_ERRMSG_HH
#define MAT_ERRMSG_HH

/*! @file GraphX/MATerial/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module MAT.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace MAT
{
#ifdef ASSERTIONS

    //! @name Messages d'assertion
    //@{
    extern  ERR::String kInvalidEnumerationValue;
    extern  ERR::String kInvalidFormat;
    extern  ERR::String kInvalidTextureIndex;
    extern  ERR::String kUnknownImageCantBeManipulated;
    extern  ERR::String kUnknownTextureCantBeManipulated;
    //@}

#endif  // De ASSERTIONS
}

#endif  // De MAT_ERRMSG_HH
