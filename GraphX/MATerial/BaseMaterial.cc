/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseMaterial.hh"

/*! @file GraphX/MATerial/BaseMaterial.cc
    @brief M�thodes (non-inline) de la classe MAT::Material.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 16 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/RGBA.hh"
#include "MATH/Const.hh"
#include "PAcKage/LoaderSaver.hh"
#include "SHaDers/Const.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"
#include "Texture.hh"

namespace MAT
{
    namespace
    {
        CLR::RGBA   kDefaultAmbient  = CLR::RGBA(F32{0.2F}, F32{0.2F}, F32{0.2F}, k1F);
        CLR::RGBA   kDefaultDiffuse  = CLR::RGBA(F32{0.8F}, F32{0.8F}, F32{0.8F}, k1F);
        CLR::RGBA   kDefaultSpecular = CLR::RGBA(k0F, k0F, k0F, k1F);
        CLR::RGBA   kDefaultEmissive = CLR::RGBA(k0F, k0F, k0F, k1F);
    }


//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de MAT::Material.
        @version 0.2
     */
    class Material::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs & destructeur
        //@{
        Private();                          //!< Constructeur par d�faut.
        Private(PAK::Loader& pLoader);      //!< Constructeur (chargement imm�diat).
        //@}
        //! @name Attributs
        //@{
        Vector<NamedTexture>    mTextures;  //!< Textures.
        CLR::RGBA               mAmbient;   //!< Couleur ambiante.
        CLR::RGBA               mDiffuse;   //!< Couleur diffuse.
        CLR::RGBA               mSpecular;  //!< Couleur sp�culaire.
        CLR::RGBA               mEmissive;  //!< Lumi�re �mise.
        F32                     mAlpha;     //!< Coefficient de transparence.
        SHD::Program::Id        mProgram;   //!< Shader associ� au mat�riau.
        //@}
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! @copydoc MAT::Material::Material()
     */
    Material::Private::Private()
        : mTextures()
        , mAmbient(kDefaultAmbient)
        , mDiffuse(kDefaultDiffuse)
        , mSpecular(kDefaultSpecular)
        , mEmissive(kDefaultEmissive)
        , mAlpha(k1F)
        , mProgram{SHD::kDefaultProgram}
    { }


    /*! @copydoc MAT::Material::Material(PAK::Loader&)
     */
    Material::Private::Private(PAK::Loader& pLoader)
        : mTextures()
        , mAmbient(kDefaultAmbient)
        , mDiffuse(kDefaultDiffuse)
        , mSpecular(kDefaultSpecular)
        , mEmissive(kDefaultEmissive)
        , mAlpha(k1F)
        , mProgram{SHD::kDefaultProgram}
    {
        // Couleurs.
        pLoader >> mAmbient;
        pLoader >> mDiffuse;
        pLoader >> mSpecular;
        pLoader >> mEmissive;
        // Coefficient de transparence.
        pLoader >> mAlpha;

        // Textures.
        // ...
    }


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Material::Material()
        : pthis()
    { }


    /*! Construit une instance � partir des donn�es d'un fichier.
     */
    Material::Material(PAK::Loader& pLoader)
        : pthis(pLoader)
    { }


    /*! Constructeur par copie.
     */
    Material::Material(const Material& pMaterial)
        : pthis(pMaterial.pthis)
    {
    }


    /*! Op�rateur d'affectation.
     */
    Material& Material::operator=(const Material& pMaterial)
    {
        if (&pMaterial != this)
        {
            pthis = pMaterial.pthis;
        }

        return *this;
    }


    /*! Destructeur.
     */
    Material::~Material() { }


//------------------------------------------------------------------------------
//                           "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void Material::store(PAK::Saver& pSaver) const
    {
        // Couleurs.
        pSaver << pthis->mAmbient;
        pSaver << pthis->mDiffuse;
        pSaver << pthis->mSpecular;
        pSaver << pthis->mEmissive;
        // Coefficient de transparence.
        pSaver << pthis->mAlpha;
    }


//------------------------------------------------------------------------------
//                                  Propri�t�s
//------------------------------------------------------------------------------

    /*!
     */
    const CLR::RGBA& Material::ambient() const
    {
        return pthis->mAmbient;
    }

    /*!
     */
    const CLR::RGBA& Material::diffuse() const
    {
        return pthis->mDiffuse;
    }

    /*!
     */
    const CLR::RGBA& Material::specular() const
    {
        return pthis->mSpecular;
    }

    /*!
     */
    const CLR::RGBA& Material::emissive() const
    {
        return pthis->mEmissive;
    }

    /*!
     */
    F32 Material::alpha() const
    {
        return pthis->mAlpha;
    }

    /*!
     */
    SHD::Program::Id Material::program() const
    {
        return pthis->mProgram;
    }


//------------------------------------------------------------------------------
//                                   Textures
//------------------------------------------------------------------------------

    /*!
     */
    U32 Material::textureCount() const
    {
        return U32(pthis->mTextures.size());
    }

    /*! @return La <i>pN</i>i�me texture, si elle a �t� d�finie, un pointeur nul sinon
     */
    Material::ConstTexturePtr Material::texture(U32 pN) const
    {
        if (pN < U32(pthis->mTextures.size()))
        {
            return std::get<eTexture>(pthis->mTextures[pN]);
        }
        else
        {
            return nullptr;
        }
    }

    //!
    std::tuple<Material::TexturePtr, String> Material::namedTexture(U32 pN) const
    {
        if (pN < U32(pthis->mTextures.size()))
        {
            return pthis->mTextures[pN];
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//                              M�thodes d'Edition
//------------------------------------------------------------------------------

    /*!
     */
    void Material::setAmbient(const CLR::RGBA& pRGBA)
    {
        pthis->mAmbient = pRGBA;
    }

    /*!
     */
    void Material::setDiffuse(const CLR::RGBA& pRGBA)
    {
        pthis->mDiffuse = pRGBA;
    }

    /*!
     */
    void Material::setSpecular(const CLR::RGBA& pRGBA)
    {
        pthis->mSpecular = pRGBA;
    }

    /*!
     */
    void Material::setEmissive(const CLR::RGBA& pRGBA)
    {
        pthis->mEmissive = pRGBA;
    }

    /*!
     */
    void Material::setProgram(SHD::Program::Id pProgram)
    {
        pthis->mProgram = pProgram;
    }

    /*!
     */
    Material::TexturePtr Material::texture(I16 pN)
    {
        if (pN < I16(pthis->mTextures.size()))
        {
            return std::get<eTexture>(pthis->mTextures[pN]);
        }
        else
        {
            return nullptr;
        }
    }

    /*!
     */
    void Material::addTexture(TexturePtr pNewTexture, String pName)
    {
        pthis->mTextures.push_back(std::make_tuple(pNewTexture, pName));
    }


//------------------------------------------------------------------------------
//                           Operateurs d'(In)Egalit�
//------------------------------------------------------------------------------

    /*! @return VRAI si les propri�t�s de mat�riau (pas les textures) sont identiques, FAUX sinon
        @ingroup MATerial
     */
    bool operator==(const Material& pL, const Material& pR)
    {
         return ((pL.ambient()  == pR.ambient())  &&
                 (pL.diffuse()  == pR.diffuse())  &&
                 (pL.specular() == pR.specular()) &&
                 (pL.emissive() == pR.emissive()) &&
                 (pL.alpha()    == pR.alpha()));
    }


    /*! @return FAUX si les propri�t�s de mat�riau (pas les textures) sont identiques, VRAI sinon
        @ingroup MATerial
     */
    bool operator!=(const Material& pL, const Material& pR)
    {
        return !(pL == pR);
    }
}
