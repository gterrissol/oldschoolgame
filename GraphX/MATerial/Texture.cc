/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Texture.hh"

/*! @file GraphX/MATerial/Texture.cc
    @brief M�thodes (non-inline) de la classe MAT::Texture.
    @author @ref Guillaume_Terrissol
    @date 14 Novembre 2002 - 14 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <GL/gl.h>

#include "PAcKage/LoaderSaver.hh"
#include "ReSourCe/ResourceMgr.tcc"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "ErrMsg.hh"

namespace MAT
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de MAT::Texture.
        @version 0.2
     */
    class Texture::Private : public MEM::OnHeap
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef Texture::ImageRscPtr    ImageRscPtr;    //!< Pointeur sur image.
        typedef Texture::ConstImagePtr  ConstImagePtr;  //!< Pointeur (constant) sur image.
        typedef Texture::ImagePtr       ImagePtr;       //!< Pointeur sur image.
        //@}
        //! @name Constructeurs
        //@{
        Private();                                      //!< Constructeur par d�faut.
        Private(PAK::Loader& pLoader);                  //!< Constructeur (chargement imm�diat).
        //@}
        //! @name Attributs
        //@{
        Vector<SharedPtr<ImageRsc>> mImages;            //!< image(s) de la texture.
        ImagePtr                    mManualImage;       //!< Image g�r�e manuellement.
        ConstImagePtr               mCurrentImage;      //!< Image � utiliser.
        EFormat                     mFormat;            //!< Format.
        EMipMap                     mMipMap;            //!< Param�tres de mip-mapping.
        EClamp                      mClamp;             //!< Param�tres de r�p�tition.
        ECoordGen                   mCoordGen;          //!< Gestion des coordonn�es.
        EBlendFunc                  mBlendFunc;         //!< Fonction de m�lange.
        //@}
    };


//------------------------------------------------------------------------------
//                            P-Impl : Constructeurs
//------------------------------------------------------------------------------

    /*! @copydoc MAT::Texture::Texture()
     */
    Texture::Private::Private()
        : mImages()
        , mManualImage()
        , mCurrentImage()
        , mFormat(e32Bits)
        , mMipMap(eNearNone)
        , mClamp(eClamp)
        , mCoordGen(ePlanar)
        , mBlendFunc(eDecal)
    { }


    /*! @copydoc MAT::Texture::Texture(PAK::Loader&)
     */
    Texture::Private::Private(PAK::Loader& pLoader)
        : mImages()
        , mManualImage()
        , mCurrentImage()
        , mFormat(e32Bits)
        , mMipMap(eNearNone)
        , mClamp(eClamp)
        , mCoordGen(ePlanar)
        , mBlendFunc(eDecal)
    {
        pLoader >> mFormat;
        pLoader >> mMipMap;
        pLoader >> mClamp;
        pLoader >> mCoordGen;
        pLoader >> mBlendFunc;

        U32 lImageCount = k0UL;
        pLoader >> lImageCount;
        mImages.resize(lImageCount);

        for(U32 lI = k0UL; lI < lImageCount; ++lI)
        {
            MAT::Key    lImgKey;
            pLoader >> lImgKey;

            mImages[lI] = ImageMgr::get()->retrieve(lImgKey);
        }
    }


//------------------------------------------------------------------------------
//                     Texture : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    Texture::Texture(PAK::Loader& pLoader)
        : pthis(pLoader)
    { }


    /*! Destructeur.
     */
    Texture::~Texture() { }


    /*! Constructeur par d�faut.<br>
        Par d�faut, une texture est ... (� compl�ter).
     */
    Texture::Texture()
        : pthis()
    { }


//------------------------------------------------------------------------------
//                             Texture : Propri�t�s
//------------------------------------------------------------------------------

    /*! Nombre d'images de la texture.
     */
    I16 Texture::imageCount() const
    {
        return I16(pthis->mImages.size());
    }

    /*! Format de la texture.
     */
    EFormat Texture::format() const
    {
        return pthis->mFormat;
    }


    /*! Param�tres de mip-mapping.
     */
    EMipMap Texture::mipMapParams() const
    {
        return pthis->mMipMap;
    }


    /*! Param�tres de clamping.
     */
    EClamp Texture::clamping() const
    {
        return pthis->mClamp;
    }


    /*! G�n�ration des coordonn�es.
     */
    ECoordGen Texture::coordGen() const
    {
        return pthis->mCoordGen;
    }


    /*! Fonction de m�lange de la texture.
     */
    EBlendFunc Texture::blendFunc() const
    {
        return pthis->mBlendFunc;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Activation de la texture.
        @param pUnit Unit� de texture � utiliser.
     */
    void Texture::activate(U32 pUnit) const
    {
        // Image � activer.
        pthis->mManualImage->activate(pUnit);

        // Param�tres de placage.
#if   (GXD_IMPL == 0)

        switch(mipMapParams())
        {
            case eNearNone:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                break;
            case eNearNear:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                break;
            case eNearLinear:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                break;
            case eLinearNone:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                break;
            case eLinearNear:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                break;
            case eLinearLinear:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                break;
            default:
                ASSERT(false, kInvalidEnumerationValue)
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                break;
        }

        switch(clamping())
        {
            case eRepeat:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                break;
            case eClamp:
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
                break;
            default:
                ASSERT(false, kInvalidEnumerationValue)
                break;
        }

        switch(coordGen())
        {
            case eSpherical:
                break;
            case ePlanar:
                break;
            case eCylindrical:
                break;
            case eOrtho:
                break;
            case eManual:
                break;
            default:
                ASSERT(false, kInvalidEnumerationValue)
                break;
        }

        switch(blendFunc())
        {
            case eDecal:
                glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_DECAL);
                break;
            case eBlend:
                glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_BLEND);
                break;
            case eModulate:
                glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                break;
            default:
                ASSERT(false, kInvalidEnumerationValue)
                break;
        }
#elif (GXD_IMPL == 1)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 2)
#   error "Aucune impl�mentation d�finie."
#elif (GXD_IMPL == 3)
#   error "Aucune impl�mentation d�finie."
#endif  // GXD_IMPL
    }


//------------------------------------------------------------------------------
//                              Gestion des Images
//------------------------------------------------------------------------------

    /*! Acc�s � l' (aux) image(s) de la texture.
     */
    Texture::ConstImagePtr Texture::image(I16 pN) const
    {
        return pthis->mImages[pN];
    }


    /*! Acc�s � l'image de la texture � utiliser.
     */
    Texture::ConstImagePtr Texture::currentImage() const
    {
        return pthis->mCurrentImage;
    }


    /*! Insertion d'une image.
        @note Les images pouvant �tre ins�r�es plusieurs fois (pour une animation), aucun test n'est
        effectu� sur la pr�sence pr�alable de l'image
     */
    void Texture::insertImage(ImageRscPtr pNewImage, I16 pN)
    {
        ASSERT(pNewImage != nullptr, kUnknownImageCantBeManipulated)

        MAT::Key    lImgKey = pNewImage->key();

        if (!lImgKey.isNull())
        {
            // 2 cas � traiter.
            if ((0 <= pN) && (pN < imageCount()))
            {
                // Cas 1 : insertion de l'image dans le tableau.
                pthis->mImages.insert(pthis->mImages.begin() + pN, ImageMgr::get()->retrieve(lImgKey));
            }
            else
            {
                // Cas 2 : empilement de l'image.
                pthis->mImages.push_back(ImageMgr::get()->retrieve(lImgKey));
            }
        }
    }


    /*! Retrait d'une image.
     */
    void Texture::removeImage(I16 pN)
    {
        if ((0 <= pN) && (pN < imageCount()))
        {
            pthis->mImages.erase(pthis->mImages.begin() + pN);
        }
    }


    /*! Il peut arriver (surtout au niveau de l'�diteur) d'avoir besoin d'une texture cr��e � partir
        d'une image d�finie � la vol�e (pas lue depuis un fichier). Il faut bien pouvoir exploiter une
        telle texture. Cette m�thode permet d'y arriver.
        @param pImage Image que la texture doit utiliser pour le rendu (les images ressources
        �ventuellement d�finies ne seront plus utilis�es)
        @note Pour retrouver le comportement par d�faut (liste d'images ressources), il suffit d'appeler
        cette m�thode avec 0 comme param�tre
     */
    void Texture::setManualImage(ImagePtr pImage)
    {
        pthis->mManualImage = pImage;
    }


//------------------------------------------------------------------------------
//                                  D�finitions
//------------------------------------------------------------------------------

    /*!
     */
    void Texture::setFormat(EFormat pNewFormat)
    {
        pthis->mFormat = pNewFormat;
    }


    /*!
     */
    void Texture::setMipMapParams(EMipMap pNewParams)
    {
        pthis->mMipMap = pNewParams;
    }


    /*!
     */
    void Texture::setClamping(EClamp pNewClamping)
    {
        pthis->mClamp = pNewClamping;
    }


    /*!
     */
    void Texture::setCoordGen(ECoordGen pNewCoordGen)
    {
        pthis->mCoordGen = pNewCoordGen;
    }


    /*! @param pNewBlend Nouveau param�tre de m�lange de texture
     */
    void Texture::setBlendFunc(EBlendFunc pNewBlend)
    {
        pthis->mBlendFunc = pNewBlend;
    }


//------------------------------------------------------------------------------
//                              Gestion des donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void Texture::store(PAK::Saver& pSaver) const
    {
        pSaver << U32(format());
        pSaver << U32(mipMapParams());
        pSaver << U32(clamping());
        pSaver << U32(coordGen());
        pSaver << U32(blendFunc());

        U32 lImageCount = U32(pthis->mImages.size());
        pSaver << lImageCount;

        for(U32 lI = k0UL; lI < lImageCount; ++lI)
        {
            pSaver << pthis->mImages[lI]->key();
        }
    }
}
