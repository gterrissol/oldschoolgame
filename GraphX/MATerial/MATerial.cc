/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file GraphX/MATerial/MATerial.cc
    @brief D�finitions diverses du module MAT.
    @author @ref Guillaume_Terrissol
    @date 6 Ao�t 2006 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

namespace MAT
{
//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    // Messages d'assertion.
    REGISTER_ERR_MSG(kInvalidEnumerationValue,         "Valeur d'�num�ration invalide.",                  "Invalid enumeration value.")
    REGISTER_ERR_MSG(kInvalidFormat,                   "Format invalide.",                                "Invalid format.")
    REGISTER_ERR_MSG(kInvalidTextureIndex,             "Indice de texture invalide.",                     "Invalid texture index.")
    REGISTER_ERR_MSG(kUnknownImageCantBeManipulated,   "Une image inexistante ne peut �tre manipul�e.",   "Unknown image can't be manipulated.")
    REGISTER_ERR_MSG(kUnknownTextureCantBeManipulated, "Une texture inexistante ne peut �tre manipul�e.", "Unknown texture can't be manipulated.")
}
