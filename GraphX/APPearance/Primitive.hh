/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef APP_PRIMITIVE_HH
#define APP_PRIMITIVE_HH

#include "APPearance.hh"

/*! @file GraphX/APPearance/Primitive.hh
    @brief En-t�te de la classe APP::Primitive.
    @author @ref Guillaume_Terrissol
    @date 12 Octobre 2008 - 25 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/Colors.hh"

#include "BaseAppearance.hh"

namespace APP
{
    enum EPrimitive
    {
        eLine,
        eDart,
        eCircle,
        eTorus,
        eCube,
        eSphere,
        eArrow
    };

    /*! @brief Primitives.
        @version 0.2
        @ingroup APPearances

        
     */
    class Primitive
    {
    public:
        //! @name Types
        //@{
        typedef SharedPtr<Appearance>   PrimitivePtr;   //!< Pointeur sur primitive.
        typedef const CLR::RGB&         RGB;            //!< Couleur.
        //@}
        //! @name Cr�ation
        //@{
 static PrimitivePtr    create(EPrimitive pWhich, F32 pSize, RGB pColor);
        //@}
    private:

                        Primitive();
    };
}

#endif  // De APP_PRIMITIVE_HH
