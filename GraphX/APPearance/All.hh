/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef APP_ALL_HH
#define APP_ALL_HH

/*! @file GraphX/APPearance/All.hh
    @brief Interface publique du module @ref APPearance.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 25 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace APP   //! Apparence d'un objet 3D.
{
    /*! @namespace APP
        @version 0.3

        ...
     */

    /*! @defgroup APPearance APPearance : Apparences
        <b>namespace</b> APP.
     */


}

#include "Pickable.hh"
#include "BaseAppearance.hh"
#include "CustomAppearance.hh"
#include "Primitive.hh"

#endif  // De APP_ALL_HH
