/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef APP_BASEAPPEARANCE_HH
#define APP_BASEAPPEARANCE_HH

#include "APPearance.hh"

/*! @file GraphX/APPearance/BaseAppearance.hh
    @brief En-t�te de la classe APP::Appearance.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GEOmetry/GEOmetry.hh"
#include "LIghT/LIghT.hh"
#include "MATerial/MATerial.hh"
#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "PAcKage/PAcKage.hh"
#include "STL/Shared.hh"
#include "STL/STL.hh"

#include "Pickable.hh"

namespace APP
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Objet apparence.
        @version 0.4

        Tout objet 3D poss�de une apparence, compos� d'un g�om�trie (volume) et d'un mat�riau (surface).
        Dans la grande famille des ressources, l'apparence tient une place � part, puisqu'elle peut �tre instanci�e directement (et
        construite � partir de clef de g�om�trie et de mat�riau).
     */
    class Appearance : public MEM::OnHeap
    {
        //! @name Classe amie
        //@{
        BEFRIEND_MEM_ALLOCATOR()                                    //!< Pour l'acc�s au constructeur par d�faut.
        //@}
    public:
        //! @name Types de pointeur
        //@{
        typedef SharedPtr<Appearance>           Ptr;                //!< Pointeur sur apparance.
        typedef SharedPtr<GEO::Geometry const>  ConstGeometryPtr;   //!< Pointeur (constant) sur g�om�trie.
        typedef SharedPtr<GEO::Geometry>        GeometryPtr;        //!< Pointeur sur g�om�trie.
        typedef SharedPtr<MAT::Material const>  ConstMaterialPtr;   //!< Pointeur (constant) sur mat�riau.
        typedef SharedPtr<MAT::Material>        MaterialPtr;        //!< Pointeur sur mat�riau.
        typedef SharedPtr<LIT::LightList const> ConstLightListPtr;  //!< Pointeur (constant) sur liste de lumi�res.
        typedef SharedPtr<LIT::LightList>       LightListPtr;       //!< Pointeur sur liste de lumi�res.
        typedef SharedPtr<LIT::Light>           LightPtr;           //!< Pointeur sur lumi�re.
        typedef SharedPtr<F32>                  F32Ptr;             //!< Pointeur sur lumi�re.
        //@}
        //! @name Constructeurs & destructeur
        //@{
                            Appearance(PAK::Loader& pLoader);       //!< Constructeur (chargement imm�diat).
                            Appearance(PAK::Loader&& pLoader);      //!< Constructeur (chargement imm�diat).
                            Appearance(GeometryPtr pGeometry,
                                       MaterialPtr pMaterial);      //!< Constructeur (donn�es "manuelles").
virtual                     ~Appearance();                          //!< Destructeur.
        //@}
        void                store(PAK::Saver& pSaver) const;        //!< Sauvegarde des donn�es.
        //! @name Eclairage
        //@{
        void                getLit(LightPtr pLight);                //!< ...activ�.
        void                ungetLit(LightPtr pLight);              //!< ...d�sactiv�.
        //@}
        //! @name R�cup�ration des donn�es
        //@{
        MATH::V3            position() const;                       //!< Position.
        MATH::Q             orientation() const;                    //!< Orientation.
        MATH::V3            scale() const;                          //!< Echelle.
        MATH::M4            frame() const;                          //!< Matrice de transformation.
        //@}
        //! @name Etablissement des donn�es
        //@{
        void                setPosition(MATH::V3 pPosition);        //!< Position.
        void                setOrientation(MATH::Q pOrientation);   //!< Orientation.
        void                setScale(MATH::V3 pScale);              //!< Echelle.
        //@}
        //! @name Acc�s aux donn�es
        //@{
        ConstGeometryPtr    geometry() const;                       //!< G�om�trie pour la frame actuelle.
        ConstMaterialPtr    material() const;                       //!< Mat�riau pour la frame actuelle.
        ConstLightListPtr   litingLights() const;                   //!< Lumi�res �clairant l'apparence.
        F32                 shadowCoef() const;                     //!< Coefficient d'ombre.
        F32                 reflectionCoef() const;                 //!< Coefficient de r�flexion.
        F32                 transparencyCoef() const;               //!< Coefficient de transparence.
        //@}
        //! @name Gestion du picking
        //@{
        Pickable::Ptr       boundPickable() const;                  //!< Objet associ� au picking de l'instance.
        void                bind(Pickable::Ptr pObject);            //!< D�finition de l'objet associ� au picking de l'instance.
        //@}
        void                setTransparencyCoef(F32 pCoef);         //!< D�finition du coefficient de transparence.


    protected:
        //! @name Autre constructeur
        //@{
                            Appearance();                           //!< Constructeur par d�faut.
        //@}

    private:

        FORBID_COPY(Appearance)
        PIMPL()
    };
}

#endif  // De APP_BASEAPPEARANCE_HH
