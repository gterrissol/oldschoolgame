/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef APP_PICKABLE_HH
#define APP_PICKABLE_HH

#include "APPearance.hh"

/*! @file GraphX/APPearance/Pickable.hh
    @brief En-t�te de la classe APP::Pickable.
    @author @ref Guillaume_Terrissol
    @date 25 Juillet 2014 - 25 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "STL/Shared.hh"
#include "STL/STL.hh"

namespace APP
{
//------------------------------------------------------------------------------
//                              Gestion du Picking
//------------------------------------------------------------------------------

    /*! @brief Objet "pickable".
        @version 0.2
     */
    class Pickable : public STL::enable_shared_from_this<Pickable>
    {
    public:
        //! @name Type de pointeur
        //@{
        using Ptr = WeakPtr<Pickable>;  //!< Pointeur sur instance.
        //@}
        // !@name Interface
        //@{
virtual     ~Pickable();                //!< Destructeur.
        U32 type() const;               //!< Type d'objet "pickable".
        //@}

    private:

virtual U32 getType() const = 0;        //!< Type d'objet "pickable".
    };
}

#endif  // De APP_PICKABLE_HH
