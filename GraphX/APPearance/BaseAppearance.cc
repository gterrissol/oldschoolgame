/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseAppearance.hh"

/*! @file GraphX/APPearance/BaseAppearance.cc
    @brief M�thodes (non-inline) de la classe APP::Appearance.
    @author @ref Guillaume_Terrissol
    @date 10 Mai 2002 - 13 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "CoLoR/RGBA.hh"
#include "GEOmetry/BaseGeometry.hh"
#include "GEOmetry/Fields.hh"
#include "MATH/Const.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "MATerial/BaseMaterial.hh"
#include "MATerial/Texture.hh"
#include "MEMory/Allocator.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/String.hh"

namespace APP
{
//------------------------------------------------------------------------------
//                             P-Impl de Appearance
//------------------------------------------------------------------------------

    /*! @brief P-Impl de APP::Appearance.
        @version 0.35
     */
    class Appearance::Private : public MEM::OnHeap
    {
    public:
        //! @name Constructeurs
        //@{
                Private();                                                  //!< Constructeur par d�faut.
                Private(PAK::Loader& pLoader);                              //!< Constructeur.
                Private(GeometryPtr pGeo, MaterialPtr pMat);
        //@}
        //! @name G�om�tries
        //@{
        GEO::GeometryRsc::Ptr   mGeometry;                                  //!< G�om�trie.
        //@}
        //! @name Mat�riaux
        //@{
        MAT::Material::Ptr      mMaterial;                                  //!< Mat�riau.
        //@}
        //! @name Donn�es
        //@{
        GeometryPtr             mCustomGeometry;                            //!< G�om�trie.
        MaterialPtr             mCustomMaterial;                            //!< Mat�riau.
        //@}
        //! @name Lumi�res
        //@{
        LightListPtr            mLitingLights;                              //!< Lumi�res �clairant l'apparence.
        //@}
        //! @name Coefficients
        //@{
        F32                     mShadowCoef;                                //!< ... d'ombre de r�f�rence.
        F32                     mReflectionCoef;                            //!< ... de r�flexion de r�f�rence.
        F32                     mTransparencyCoef;                          //!< ... de transparence de r�f�rence.
        //@}
        MATH::V3                mPosition;                                  //!< Position.
        MATH::Q                 mOrientation;                               //!< Orientation.
        MATH::V3                mScale;                                     //!< Echelle.
        //! @name Pour le picking
        //@{
        Pickable::Ptr           mForPicking;                                //!< Objet associ� au picking de l'apparence.
        //@}
    };


    /*! @copydoc APP::Appearance::Appearance()
     */
    Appearance::Private::Private()
        : mGeometry()
        , mMaterial(STL::makeShared<MAT::Material>())
        , mCustomGeometry()
        , mCustomMaterial()
        , mLitingLights(STL::makeShared<LIT::LightList>())
        , mShadowCoef(k0F)
        , mReflectionCoef(k0F)
        , mTransparencyCoef(k0F)
        , mPosition(MATH::kNullV3)
        , mOrientation(MATH::kNullQ)
        , mScale(k1F, k1F, k1F)
        , mForPicking()
    { }


    /*! @copydoc APP::Appearance::Appearance(PAK::Loader&)
     */
    Appearance::Private::Private(PAK::Loader& pLoader)
        : mGeometry()
        , mMaterial()
        , mCustomGeometry()
        , mCustomMaterial()
        , mLitingLights(STL::makeShared<LIT::LightList>())
        , mShadowCoef(k0F)
        , mReflectionCoef(k0F)
        , mTransparencyCoef(k0F)
        , mPosition(MATH::kNullV3)
        , mOrientation(MATH::kNullQ)
        , mScale(k1F, k1F, k1F)
        , mForPicking()
    {
        // Charge :
        // - La g�om�trie.
        GEO::Key    lRefGeoKey;
        pLoader >> lRefGeoKey;
        mGeometry = GEO::GeometryMgr::get()->retrieve(lRefGeoKey);

        // - Le mat�riau.
        mMaterial = STL::makeShared<MAT::Material>(pLoader);

        // - Les coefficients.
        pLoader >> mShadowCoef;
        pLoader >> mReflectionCoef;
        pLoader >> mTransparencyCoef;
    }


    /*!
     */
    Appearance::Private::Private(GeometryPtr pGeo, MaterialPtr pMat)
        : mGeometry()
        , mMaterial()
        , mCustomGeometry(pGeo)
        , mCustomMaterial(pMat)
        , mLitingLights(STL::makeShared<LIT::LightList>())
        , mShadowCoef(k0F)
        , mReflectionCoef(k0F)
        , mTransparencyCoef(k0F)
        , mPosition(MATH::kNullV3)
        , mOrientation(MATH::kNullQ)
        , mScale(k1F, k1F, k1F)
        , mForPicking()
    { }


//------------------------------------------------------------------------------
//                   Appearance : Constructeurs & Destructeur
//------------------------------------------------------------------------------

    /*! Construit une instance � partir des donn�es d'un fichier.
     */
    Appearance::Appearance(PAK::Loader& pLoader)
        : pthis{pLoader}
    { }


    /*! Construit une instance � partir des donn�es d'un fichier.
     */
    Appearance::Appearance(PAK::Loader&& pLoader)
        : pthis{pLoader}
    { }


    /*!
     */
    Appearance::Appearance(GeometryPtr pGeometry, MaterialPtr pMaterial)
        : pthis{pGeometry, pMaterial}
    { }


    /*! Destructeur.
     */
    Appearance::~Appearance() { }


//------------------------------------------------------------------------------
//                     Appearance : "Permanence" des Donn�es
//------------------------------------------------------------------------------

    /*! Sauvegarde des donn�es.
     */
    void Appearance::store(PAK::Saver& pSaver) const
    {
        // Clef de la ressource g�om�trie de r�f�rence.
        const GEO::GeometryRsc* lGeoRsc = std::dynamic_pointer_cast<const GEO::GeometryRsc>(geometry()).get();

        pSaver << ((lGeoRsc != nullptr) ? lGeoRsc->key() : GEO::Key{});

        // Mat�riau.
        material()->store(pSaver);

        // Coefficients.
        pSaver << pthis->mShadowCoef;
        pSaver << pthis->mReflectionCoef;
        pSaver << pthis->mTransparencyCoef;
    }


//------------------------------------------------------------------------------
//                            Appearance : Eclairage
//------------------------------------------------------------------------------

    /*! @todo Utiliser des "procurateurs de ressources"
     */
    void Appearance::getLit(LightPtr pLight)
    {
        pthis->mLitingLights->insert(pLight);
    }


    /*! @todo Faire le m�nage dans le destructeur de LightList
     */
    void Appearance::ungetLit(LightPtr pLight)
    {
        pthis->mLitingLights->remove(pLight);
    }


//------------------------------------------------------------------------------
//                           R�cup�ration des Donn�es
//------------------------------------------------------------------------------

    /*! @return La position de l'os
     */
    MATH::V3 Appearance::position() const
    {
        return pthis->mPosition;
    }


    /*! @return L'orientation de l'os
     */
    MATH::Q Appearance::orientation() const
    {
        return pthis->mOrientation;
    }


    /*! @return L'�chelle de l'os
     */
    MATH::V3 Appearance::scale() const
    {
        return pthis->mScale;
    }


    /*! @return La matrice de transformation de l'os
     */
    MATH::M4 Appearance::frame() const
    {
        return MATH::M4(MATH::M3(orientation()), position(), scale());
    }


//------------------------------------------------------------------------------
//                           Etablissement des Donn�es
//------------------------------------------------------------------------------

    /*! @param pPosition Nouvelle position de l'os
     */
    void Appearance::setPosition(MATH::V3 pPosition)
    {
        pthis->mPosition = pPosition;
    }


    /*! @param pOrientation Nouvelle orientation de l'os
     */
    void Appearance::setOrientation(MATH::Q pOrientation)
    {
        pthis->mOrientation = pOrientation;
    }


    /*! @param pScale Nouvelle �chelle de l'os
     */
    void Appearance::setScale(MATH::V3 pScale)
    {
        pthis->mScale = pScale;
    }


//------------------------------------------------------------------------------
//                        Appearance : Acc�s aux Donn�es
//------------------------------------------------------------------------------

    /*! 
     */
    Appearance::ConstGeometryPtr Appearance::geometry() const
    {
        if (pthis->mCustomGeometry != nullptr)
        {
            return pthis->mCustomGeometry;
        }
        else
        {
            return pthis->mGeometry;
        }
    }


    /*! 
     */
    Appearance::ConstMaterialPtr Appearance::material() const
    {
        if (pthis->mCustomMaterial != nullptr)
        {
            return pthis->mCustomMaterial;
        }
        else
        {
            return pthis->mMaterial;
        }
    }


    /*!
     */
    Appearance::ConstLightListPtr Appearance::litingLights() const
    {
        return pthis->mLitingLights;
    }


    /*!
     */
    F32 Appearance::shadowCoef() const
    {
        return pthis->mShadowCoef;
    }


    /*!
     */
    F32 Appearance::reflectionCoef() const
    {
        return pthis->mReflectionCoef;
    }


    /*!
     */
    F32 Appearance::transparencyCoef() const
    {
        return pthis->mTransparencyCoef;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @return L'objet s�lectionnable "via" cette apparence
     */
    Pickable::Ptr Appearance::boundPickable() const
    {
        return pthis->mForPicking;
    }


    /*! Renomme l'instance.
        @param pObject Smart pointer sur l'objet dont cette instance est tout ou partie de l'apparence
        @note Cette m�thode ne devrait �tre appel�e que par l'arbre de rendu pour s�lection
        @sa boundPickable
     */
    void Appearance::bind(Pickable::Ptr pObject)
    {
        pthis->mForPicking = pObject;
    }


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void Appearance::setTransparencyCoef(F32 pCoef)
    {
        if (pthis->mTransparencyCoef != pCoef)
        {
            pthis->mTransparencyCoef = pCoef;
        }
    }


//------------------------------------------------------------------------------
//                        Appearance : Autre Constructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Appearance::Appearance() = default;
}
