/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Primitive.hh"

/*! @file GraphX/APPearance/Primitive.cc
    @brief M�thodes (non-inline) de la classe APP::Primitive.
    @author @ref Guillaume_Terrissol
    @date 12 Octobre 2008 - 28 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>

#include "GEOmetry/BaseGeometry.hh"
#include "GEOmetry/Fields.hh"
#include "GEOmetry/List.tcc"
#include "MATerial/BaseMaterial.hh"
#include "MATH/V3.hh"
#include "STL/SharedPtr.hh"

#include "BaseAppearance.hh"

namespace APP
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Primitive g�om�trique : ligne.
        @brief 0.4
     */
    class Line : public GEO::Geometry
    {
    public:

        Line(F32 pLength, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));

            setVertexCount(U16(2));
            setSegmentCount(U16(1));

            F32 l0[]   = { k0F,     k0F, k0F };
            F32 l1[]   = { pLength, k0F, k0F };
            vertices3().push_back(GEO::Vertex3(l0)); colors3().push_back(pColor);
            vertices3().push_back(GEO::Vertex3(l1)); colors3().push_back(pColor);

            SegmentListPtr lSgList = STL::makeShared<SegmentListPtr::element_type>(segmentCount());
            lSgList->push(GEO::Segment(U16(0), U16(1)));
            cloneSegmentList(lSgList);
        }

virtual ~Line() { }
    };


    /*! Primitive g�om�trique : ligne.
        @brief 0.4
     */
    class Dart : public GEO::Geometry
    {
    public:

        Dart(F32 pLength, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));

            setVertexCount(U16(4));
            setSegmentCount(U16(3));

            F32 lOffset = F32(0.2F * pLength);

            F32 l0[]   = { k0F,                 k0F,     k0F };
            F32 l1[]   = { pLength,             k0F,     k0F };
            F32 l2[]   = { pLength - lOffset,   lOffset, k0F };
            F32 l3[]   = { pLength - lOffset, - lOffset, k0F };

            vertices3().push_back(GEO::Vertex3(l0));
            vertices3().push_back(GEO::Vertex3(l1));
            vertices3().push_back(GEO::Vertex3(l2));
            vertices3().push_back(GEO::Vertex3(l3));

            colors3().resize(vertexCount(), pColor);

            SegmentListPtr lSgList = STL::makeShared<SegmentListPtr::element_type>(segmentCount());
            lSgList->push(GEO::Segment(U16(0), U16(1)));
            lSgList->push(GEO::Segment(U16(1), U16(2)));
            lSgList->push(GEO::Segment(U16(1), U16(3)));
            cloneSegmentList(lSgList);
        }

virtual ~Dart() { }
    };


    /*! Primitive g�om�trique : cercle.
        @brief 0.4
     */
    class Circle : public GEO::Geometry
    {
    public:

        Circle(F32 pRadius, const CLR::RGB& pColor)
        {
            setFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eRGB | GEO::eLines));

            const U16   kVxCount = U16(36);
            setVertexCount(kVxCount);
            setSegmentCount(kVxCount);

            for(U16 lV = k0UW; lV < kVxCount; ++lV)
            {
                F32 lAngle  = F32(lV * (k2Pi / kVxCount));
                F32 lVx[]   = { F32(pRadius * cos(lAngle)), F32(pRadius * sin(lAngle)), k0F };

                vertices3().push_back(GEO::Vertex3(lVx));
            }

            colors3().resize(vertexCount(), pColor);

            SegmentListPtr lSgList = STL::makeShared<SegmentListPtr::element_type>(segmentCount());
            for(U16 lS = k0UW; lS < kVxCount; ++lS)
            {
                lSgList->push(GEO::Segment(lS, U16((lS + 1) % kVxCount)));
            }
            cloneSegmentList(lSgList);
        }

virtual ~Circle() { }
    };


    /*! Primitive g�om�trique : tore.
        @brief 0.4
     */
    class Torus : public GEO::Geometry
    {
    public:

        Torus(F32 pRadius, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));

            const F32   kCylRad     = F32(0.05F * pRadius);
            const U16   kMeridians  = U16(36);
            const U16   kParallels  = U16(12);
            const U16   kVxCount    = kMeridians * kParallels;

            setVertexCount(kVxCount);
            setTriangleCount(U16(2 * kVxCount));

            for(U16 lT = k0UW; lT < kMeridians; ++lT)
            {
                F32 lTheta  = F32(lT * k2Pi / kMeridians);
                F32 lCosT   = F32(cosf(lTheta));
                F32 lSinT   = F32(sinf(lTheta));

                for(U16 lP = k0UW; lP < kParallels; ++lP)
                {
                    F32 lPhi  = F32(lP * k2Pi / kParallels);
                    F32 lCosP = F32(cosf(lPhi));
                    F32 lSinP = F32(sinf(lPhi));

                    F32         lVx[]   = { lCosT * (pRadius + lCosP * kCylRad), lSinT * (pRadius + lCosP * kCylRad), lSinP * kCylRad };
                    MATH::V3    lN      = MATH::V3(lCosT * lCosP, lSinT * lCosP, lSinT).normalized();

                    vertices3().push_back(GEO::Vertex3(lVx));
                    normals().push_back(GEO::Normal(&lN[0]));
                }
            }

            colors3().resize(vertexCount(), pColor);

            TriangleListPtr lTrList = STL::makeShared<TriangleListPtr::element_type>(triangleCount());
            for(U16 lT = k0UW; lT < kMeridians; ++lT)
            {
                for(U16 lP = k0UW; lP < kParallels; ++lP)
                {
                    U16 l0  = U16(lT * kParallels + lP + 0);
                    U16 l1  = U16((lT * kParallels + lP + 1) % kVxCount);
                    U16 l2  = U16((lT * kParallels + lP + kParallels) % kVxCount);
                    U16 l3  = U16((lT * kParallels + lP + kParallels + 1) % kVxCount);
                    lTrList->push(GEO::Triangle(l0, l1, l2));
                    lTrList->push(GEO::Triangle(l2, l1, l3));
                }
            }

            cloneTriangleList(lTrList);
        }

virtual ~Torus() { }
    };


    /*! Primitive g�om�trique : cube.
        @brief 0.4
     */
    class Cube : public GEO::Geometry
    {
    public:

        Cube(F32 pSize, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));

            setVertexCount(U16(8));
            setTriangleCount(U16(12));

            F32 lTLN[] = { - pSize,   pSize,   pSize };
            F32 lTRN[] = { - pSize, - pSize,   pSize };
            F32 lTRF[] = {   pSize,   pSize,   pSize };
            F32 lTLF[] = {   pSize, - pSize,   pSize };

            F32 lBLN[] = { - pSize,   pSize, - pSize };
            F32 lBRN[] = { - pSize, - pSize, - pSize };
            F32 lBRF[] = {   pSize,   pSize, - pSize };
            F32 lBLF[] = {   pSize, - pSize, - pSize };

            vertices3().push_back(GEO::Vertex3(lTLN));
            vertices3().push_back(GEO::Vertex3(lTRN));
            vertices3().push_back(GEO::Vertex3(lTRF));
            vertices3().push_back(GEO::Vertex3(lTLF));

            vertices3().push_back(GEO::Vertex3(lBLN));
            vertices3().push_back(GEO::Vertex3(lBRN));
            vertices3().push_back(GEO::Vertex3(lBRF));
            vertices3().push_back(GEO::Vertex3(lBLF));

            MATH::V3
            lNormal = MATH::V3(- k1F, - k1F,   k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(  k1F, - k1F,   k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(  k1F,   k1F,   k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(- k1F,   k1F,   k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(- k1F, - k1F, - k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(  k1F, - k1F, - k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(  k1F,   k1F, - k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            lNormal = MATH::V3(- k1F,   k1F, - k1F).normalized();
            normals().push_back(GEO::Normal(&lNormal[0]));
            colors3().resize(vertexCount(), pColor);

            TriangleListPtr lTrList = STL::makeShared<TriangleListPtr::element_type>(triangleCount());
            lTrList->push(GEO::Triangle(U16(0), U16(1), U16(2)));
            lTrList->push(GEO::Triangle(U16(2), U16(1), U16(3)));
            lTrList->push(GEO::Triangle(U16(3), U16(7), U16(2)));
            lTrList->push(GEO::Triangle(U16(2), U16(7), U16(6)));
            lTrList->push(GEO::Triangle(U16(6), U16(4), U16(2)));
            lTrList->push(GEO::Triangle(U16(2), U16(4), U16(0)));

            lTrList->push(GEO::Triangle(U16(1), U16(0), U16(5)));
            lTrList->push(GEO::Triangle(U16(5), U16(0), U16(4)));
            lTrList->push(GEO::Triangle(U16(4), U16(6), U16(5)));
            lTrList->push(GEO::Triangle(U16(5), U16(6), U16(7)));
            lTrList->push(GEO::Triangle(U16(7), U16(3), U16(5)));
            lTrList->push(GEO::Triangle(U16(5), U16(3), U16(1)));

            cloneTriangleList(lTrList);
        }

virtual ~Cube() { }
    };


    /*! Primitive g�om�trique : sph�re.
        @brief 0.4
     */
    class Sphere : public GEO::Geometry
    {
    public:

        Sphere(F32 pRadius, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));

            const U16   kMeridians  = U16(24);
            const U16   kParallels  = U16(kMeridians / 2 + 1);
            const U16   kVxCount    = kMeridians * kParallels;

            setVertexCount(kVxCount);
            setTriangleCount(U16(2 * kVxCount));

            for(U16 lT = k0UW; lT < kMeridians; ++lT)
            {
                F32 lTheta = F32(lT * k2Pi / kMeridians);
                F32 lCosT  = F32(cosf(lTheta));
                F32 lSinT  = F32(sinf(lTheta));

                for(U16 lP = k0UW; lP < kParallels; ++lP)
                {
                    F32 lPhi  = F32(kPiBy2 - lP * kPi / (kParallels - 1));
                    F32 lCosP = F32(cosf(lPhi));
                    F32 lSinP = F32(sinf(lPhi));

                    F32         lVx[]   = { lCosT * lCosP * pRadius, lSinT * lCosP * pRadius, lSinP * pRadius };
                    MATH::V3    lN      = MATH::V3(lCosT * lCosP, lSinT * lCosP, lSinT).normalized();

                    vertices3().push_back(GEO::Vertex3(lVx));
                    normals().push_back(GEO::Normal(&lN[0]));
                }
            }

            colors3().resize(vertexCount(), pColor);

            TriangleListPtr lTrList = STL::makeShared<TriangleListPtr::element_type>(triangleCount());
            for(U16 lT = k0UW; lT < kMeridians; ++lT)
            {
                for(U16 lP = k0UW; lP < kParallels; ++lP)
                {
                    U16 l0  = U16( lT * kParallels + lP + 0);
                    U16 l1  = U16((lT * kParallels + lP + 1) % kVxCount);
                    U16 l2  = U16((lT * kParallels + lP + kParallels) % kVxCount);
                    U16 l3  = U16((lT * kParallels + lP + kParallels + 1) % kVxCount);
                    lTrList->push(GEO::Triangle(l0, l1, l2));
                    lTrList->push(GEO::Triangle(l2, l1, l3));
                }
            }

            cloneTriangleList(lTrList);
        }

virtual ~Sphere() { }
    };


    /*! Primitive g�om�trique : fl�che.
        @brief 0.4
     */
    class Arrow : public GEO::Geometry
    {
    public:

        Arrow(F32 pLength, const CLR::RGB& pColor)
            : GEO::Geometry()
        {
            setFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));
            setAutoFlags(I32(GEO::eVertex3 | GEO::eNormal | GEO::eRGB | GEO::eTriangles));

            const U16   kVxCount    = U16(12);
            const F32   kCylLen     = F32(0.75F * pLength);
            const F32   kCylRad     = F32(0.05F * pLength);
            const F32   kConRad     = F32(0.09F * pLength);

            setVertexCount(U16(2 * kVxCount + 3 * (1 + kVxCount)));
            setTriangleCount(U16(kVxCount * 2 + 3 * kVxCount));

            // 1�re partie : cylindre.
            for(U16 lV = k0UW; lV < kVxCount; ++lV)
            {
                F32 lAngle  = F32(lV * k2Pi / kVxCount);
                F32 lCos    = F32(cosf(lAngle));
                F32 lSin    = F32(sinf(lAngle));

                F32         lVx0[]  = { k0F,     lCos * kCylRad, lSin * kCylRad };
                F32         lVx1[]  = { kCylLen, lCos * kCylRad, lSin * kCylRad };
                MATH::V3    lN      = MATH::V3(k0F, lCos, lSin).normalized();

                vertices3().push_back(GEO::Vertex3(lVx0));
                vertices3().push_back(GEO::Vertex3(lVx1));

                normals().push_back(GEO::Normal(&lN[0]));
                normals().push_back(GEO::Normal(&lN[0]));
            }

            // 2�me partie : base du cylindre.
            F32         lVxB[]  = {   k0F, k0F, k0F };
            MATH::V3    lNB     = MATH::V3(- k1F, k0F, k0F).normalized();

            vertices3().push_back(GEO::Vertex3(lVxB));
            normals().push_back(GEO::Normal(&lNB[0]));

            for(U16 lV = k0UW; lV < kVxCount; ++lV)
            {
                F32 lAngle  = F32(lV * k2Pi / kVxCount);
                F32 lCos    = F32(cosf(lAngle));
                F32 lSin    = F32(sinf(lAngle));

                F32 lVx0[]  = { k0F, lCos * kCylRad, lSin * kCylRad };

                vertices3().push_back(GEO::Vertex3(lVx0));
                normals().push_back(GEO::Normal(&lNB[0]));
            }

            // 3�me partie : base du c�ne.
            F32 lVxC[]  = { kCylLen, k0F, k0F };

            vertices3().push_back(GEO::Vertex3(lVxC));
            normals().push_back(GEO::Normal(&lNB[0]));

            for(U16 lV = k0UW; lV < kVxCount; ++lV)
            {
                F32 lAngle  = F32(lV * k2Pi / kVxCount);
                F32 lCos    = F32(cosf(lAngle));
                F32 lSin    = F32(sinf(lAngle));

                F32 lVx0[]  = { kCylLen, lCos * kConRad, lSin * kConRad };

                vertices3().push_back(GEO::Vertex3(lVx0));
                normals().push_back(GEO::Normal(&lNB[0]));
            }

            // 4�me partie : c�ne.
            F32 lVxA[]  = { pLength, k0F, k0F };
            F32 lNA[]   = { k1F,     k0F, k0F };

            vertices3().push_back(GEO::Vertex3(lVxA));
            normals().push_back(GEO::Normal(lNA));

            for(U16 lV = k0UW; lV < kVxCount; ++lV)
            {
                F32 lAngle  = F32(lV * k2Pi / kVxCount);
                F32 lCos    = F32(- cosf(lAngle));
                F32 lSin    = F32(sinf(lAngle));

                F32         lVx0[]  = { kCylLen, lCos * kConRad, lSin * kConRad };
                MATH::V3    lN      = (MATH::V3(lNA) + MATH::V3(k0F, lCos, lSin)).normalized();

                vertices3().push_back(GEO::Vertex3(lVx0));
                normals().push_back(GEO::Normal(&lN[0]));
            }

            colors3().resize(vertexCount(), pColor);

            TriangleListPtr lTrList = STL::makeShared<TriangleListPtr::element_type>(triangleCount());
            for(U16 lTr = k0UW; lTr < kVxCount; ++lTr)
            {
                lTrList->push(GEO::Triangle(U16(2 * lTr),     U16((2 * lTr + 2) % (kVxCount * 2)), U16(2 * lTr + 1)));
                lTrList->push(GEO::Triangle(U16(2 * lTr + 1), U16((2 * lTr + 2) % (kVxCount * 2)), U16((2 * lTr + 3) % (kVxCount * 2))));
            }

            for(U16 lList = k0UW, l1st = U16(2 * kVxCount); lList < 3; ++lList, l1st += U16(kVxCount + 1))
            {
                for(U16 lTr = k0UW; lTr < kVxCount; ++lTr)
                {
                    lTrList->push(GEO::Triangle(l1st + k0UW, l1st + U16(1 + (lTr + 1) % kVxCount), l1st + U16(1 + lTr)));
                }
            }
            cloneTriangleList(lTrList);
        }

virtual ~Arrow() { }
    };


//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*!
     */
    Primitive::PrimitivePtr Primitive::create(EPrimitive pWhich, F32 pSize, RGB pColor)
    {
        Appearance::GeometryPtr   lGeo;

        switch(pWhich)
        {
            case eLine:
                lGeo = STL::makeShared<Line>(pSize, pColor);
                break;
            case eDart:
                lGeo = STL::makeShared<Dart>(pSize, pColor);
                break;
            case eCircle:
                lGeo = STL::makeShared<Circle>(pSize, pColor);
                break;
            case eTorus:
                lGeo = STL::makeShared<Torus>(pSize, pColor);
                break;
            case eCube:
                lGeo = STL::makeShared<Cube>(pSize, pColor);
                break;
            case eSphere:
                lGeo = STL::makeShared<Sphere>(pSize, pColor);
                break;
            case eArrow:
                lGeo = STL::makeShared<Arrow>(pSize, pColor);
                break;
            default:
                return nullptr;
        }

        Appearance::MaterialPtr lMat = STL::makeShared<MAT::Material>();

        return STL::makeShared<Appearance>(lGeo, lMat);
    }
}
