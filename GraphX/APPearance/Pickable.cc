/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Pickable.hh"

/*! @file GraphX/APPearance/Pickable.cc
    @brief M�thodes (non-inline) de la classe APP::Pickable.
    @author @ref Guillaume_Terrissol
    @date 25 Juillet 2014 - 25 Juillet 2014
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "STL/Shared.tcc"
#include "STL/SharedPtr.hh"

namespace APP
{
//------------------------------------------------------------------------------
//                              Gestion du Picking
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Pickable::~Pickable() { }


    /*!
     */
    U32 Pickable::type() const
    {
        return getType();
    }
}
