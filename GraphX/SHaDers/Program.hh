/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_PROGRAM_HH
#define SHD_PROGRAM_HH

#include "SHaDers.hh"

/*! @file GraphX/SHaDers/Program.hh
    @brief En-t�te de la classe SHD::Program.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 23 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

#include "Shader.hh"

namespace SHD
{
    /*! @brief Programme GLSL.
        @version 0.4
     */
    class Program : public MEM::Auto<Program>
    {
    public:
        //! @name Types
        //@{
        using Id        = U16;                      //!< Id de programme.
        using Hdl       = U32;                      //!< Handle de programme compil�.
        using Modules   = Vector<Shader::Id>;       //!< Liste de shaders.
        //@}
 static const Id    kNullId;                        //!< Id nul (invalide).
 static const Hdl   kNullHdl;                       //!< Handle nul (invalide).
        //! @name Constructeurs
        //@{
                Program(const Modules& pModules);   //!< Constructeur (cr�ation).
                Program(PAK::Loader& pLoader);      //!< Constructeur (chargement).
        //@}
        //! @name Informations
        //@{
        const Modules&  modules() const;            //!< Liste des shaders du programme.
        bool            isValid() const;            //!< Est valide ?
        //@}
    private:

        Modules mModules;                           //!< Liste des shaders du programme.
    };
}

#endif  // De SHD_PROGRAM_HH
