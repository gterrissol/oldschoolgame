/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Program.hh"

/*! @file GraphX/SHaDers/Program.cc
    @brief M�thodes (non-inline) de la classe SHD::Program.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 23 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <limits>

namespace SHD
{
//------------------------------------------------------------------------------
//                              Program : Constante
//------------------------------------------------------------------------------

const Program::Id   Program::kNullId    = Program::Id{std::numeric_limits<Program::Id::TType>::max()};
const Program::Hdl  Program::kNullHdl   = Program::Hdl{0};


//------------------------------------------------------------------------------
//                            Program : Constructeurs
//------------------------------------------------------------------------------

    /*! @param pModules Liste des shaders utilis�s pour d�finir le programme.
     */
    Program::Program(const Modules& pModules)
        : mModules{pModules}
    { }

    /*! @param pLoader Donn�es du programme.
     */
    Program::Program(PAK::Loader& pLoader)
        : mModules{}
    {
        U16 lCount{};
        pLoader >> lCount;
        mModules.resize(lCount);
        for(auto& lM : mModules)
        {
            pLoader >> lM;
        }
    }


//------------------------------------------------------------------------------
//                            Program : Informations                            
//------------------------------------------------------------------------------

    /*! Les identifiants des shaders utilis�s par le programme.
     */
    const Program::Modules& Program::modules() const
    {
        return mModules;
    }

    /*! @retval true  Si le programme utilise des shaders,
        @retval false Sinon.
     */
    bool Program::isValid() const
    {
        return !modules().empty();
    }
}
