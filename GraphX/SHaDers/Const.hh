/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_CONST_HH
#define SHD_CONST_HH

/*! @file GraphX/SHaDers/Const.hh
    @brief Constantes li�es au shaders.
    @author @ref Guillaume_Terrissol
    @date 8 Octobre 2015 - 18 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "Program.hh"

namespace SHD
{
    // Shaders principaux.
    extern const Program::Id    kDefaultProgram;
    extern const Program::Id    kPickingProgram;
    extern const Program::Id    kMegaTexProgram;
}

#define SHD_ATTRIBUTE_VERTEX    "iVertex"
#define SHD_ATTRIBUTE_NORMAL    "iNormal"
#define SHD_ATTRIBUTE_COLOR     "iColor"
#define SHD_ATTRIBUTE_TEX       "iTexCoord"

#endif  // De SHD_CONST_HH
