/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_ERRMSG_HH
#define SHD_ERRMSG_HH

/*! @file GraphX/SHaDers/ErrMsg.hh
    @brief Messages (multi-langues) d'erreurs du module SHD.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 14 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"

namespace SHD
{
#ifdef ASSERTIONS

    // ! @name Messages d'assertion
    // @{
    extern  ERR::String kCouldntCompileInvalidShader;
    extern  ERR::String kCouldntDeleteInvalidShader;
    extern  ERR::String kCouldntEditInvalidShader;
    extern  ERR::String kCouldntDeleteInvalidProgram;
    extern  ERR::String kCouldntGetInvalidProgram;
    extern  ERR::String kCouldntRelinkInvalidProgram;
    extern  ERR::String kInvalidProgramIsCompiled;
    extern  ERR::String kInvalidShaderIsCompiled;
    extern  ERR::String kProgramCreationFailed;
    extern  ERR::String kShaderCreationFailed;
    // @}

#endif  // De ASSERTIONS
}

#endif  // De SHD_ERRMSG_HH
