/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_SHADERS_HH
#define SHD_SHADERS_HH

/*! @file GraphX/SHaDers/SHaDers.hh
    @brief Pr�-d�clarations du module @ref SHaDers.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 8 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace SHD
{
    class Shader;
    class Program;
    class Library;
}

#endif  // De SHD_SHADERS_HH
