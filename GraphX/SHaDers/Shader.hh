/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_SHADER_HH
#define SHD_SHADER_HH

#include "SHaDers.hh"

/*! @file GraphX/SHaDers/Shader.hh
    @brief En-t�te de la classe SHD::Shader.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 16 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/BuiltIn.hh"
#include "MEMory/Class.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/SharedPtr.hh"

namespace SHD
{
    /*! @brief Shader g�n�rique.
        @version 0.4

        Tous les types de shaders OpenGL seront g�r�s par des instances de cette classe.
     */
    class Shader : public MEM::Auto<Shader>
    {
    public :
        //! @name Types
        //@{
        enum EType : U8::TType                      //!< Types de shaders disponibles.
        {
            eNone,                                  //!< Invalide.
            eVertex,                                //!< Sommet.
            eFragment                               //!< Texture.
        };
        using Id    = U16;                          //!< Index de shader.
        using Hdl   = U32;                          //!< Handle de shader compil�.
        //@}
 static const Id    kNullId;                        //!< Id nul (invalide).
 static const Hdl   kNullHdl;                       //!< Handle nul (invalide).
        //! @name Constructeurs
        //@{
                Shader(EType pType, String pCode);  //!< Constructeur (cr�ation).
                Shader(PAK::Loader& pLoader);       //!< Constructeur (chargement).
        //@}
        //! @name Informations
        //@{
        EType   type() const;                       //!< Type du shader.
        String  code() const;                       //!< Code source.
        void    edit(String pCode);                 //!< Edition du code source.
        bool    isValid() const;                    //!< Est valide ?
        //@}
    private:

        String  mCode;                              //!< Code source.
        EType   mType;                              //!< Type du shader.
    };
}

#endif  // De SHD_SHADER_HH
