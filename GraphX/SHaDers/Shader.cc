/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Shader.hh"

/*! @file GraphX/SHaDers/Shader.cc
    @brief M�thodes (non-inline) de la classe SHD::Shader.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 23 Avril 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <limits>

namespace SHD
{
//------------------------------------------------------------------------------
//                              Shader : Constante
//------------------------------------------------------------------------------

const Shader::Id    Shader::kNullId     = Shader::Id{std::numeric_limits<Shader::Id::TType>::max()};
const Shader::Hdl   Shader::kNullHdl    = Shader::Hdl{0};


//------------------------------------------------------------------------------
//                     Shader : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @param pType Type du shader.
        @param pCode Code source du shader.
     */
    Shader::Shader(EType pType, String pCode)
        : mCode(pCode)
        , mType{pType}
    { }

    /*! @param pLoader Donn�es du shader.
     */
    Shader::Shader(PAK::Loader& pLoader)
        : mCode{}
        , mType{}
    {
        pLoader >> mType;
        pLoader >> mCode;
    }


//------------------------------------------------------------------------------
//                             Shader : Informations
//------------------------------------------------------------------------------

    /*! @return Le type du shader (vertex, fragment, etc).
     */
    Shader::EType Shader::type() const
    {
        return mType;
    }

    /*! @return Le code source du shader.
     */
    String Shader::code() const
    {
        return mCode;
    }

    /*! @param pCode Nouveau code source du shader.
     */
    void Shader::edit(String pCode)
    {
        mCode = pCode;
    }

    /*! @retval true  Si le code source est d�fini,
        @retval false Sinon.
     */
    bool Shader::isValid() const
    {
        return !code().empty();
    }
}
