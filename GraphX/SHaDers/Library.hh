/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_LIBRARY_HH
#define SHD_LIBRARY_HH

#include "SHaDers.hh"

/*! @file GraphX/SHaDers/Library.hh
    @brief En-t�te de la classe SHD::Library.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 20 Mai 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "PAcKage/LoaderSaver.hh"
#include "STL/SharedPtr.hh"
#include "STL/String.hh"

#include "Program.hh"
#include "Shader.hh"

namespace SHD
{
    /*! @brief Biblioth�que de shaders GLSL.
        @version 0.4

        Cas d'utilisation :

        1 - IDE : cr�ation de :
        - 1 vertex shader (� nommer),
        - 1 fragment shader (� nommer).
        D�finition du code, � chaque fois.
        Cr�ation d'un programme (� nommer).
        Association du programme � un mat�riau (par nom).

        On retombe sur l'ajout/suppression en deux op�rations, d�j� vu dans DATA/ListModel.cc : insertion/suppression d'un proxy.

        2 - Modification du code d'un shader, application, recompilation du shader
        => si Ok, recompilation de tous les programmes utilisant le shader,
        +> si Ok, utlisation du nouveau shader dans tous les mat�riaux l'utilisant.

        3 - Suppression d'un shader
        Si encore utilis� : �chec ou suppression d�sactiv�e,
        Sinon, supression du shader de la lib.
        Cancel => r�insertion.

        4 - Suppression d'un programme :
        Si encore li� � un mat�riau : �chec ou suppression d�sactiv�e,
        Sinon, suppression du programme de la lib.
        Cancel => r�insertion.

        Probl�me :
        Association des programmes aux mat�riaux : module.
        Mais, gestion des shaders et programmes dans database.
        Donc UndoManager diff�rents. Non: forcer Shader::UndoManager{"module"}. Mais l'action est appel�e sur l'onglet en cours...
        E.g.
        Cr�er programme (database), lier � mat�riau (module), annuler (module), annuler (database), refaire (module) => �chec.
        �a peut peut-�tre "marcher", en fait (comportement acceptable). A voir (popup d'erreur).
     */
    class Library : public MEM::OnHeap
    {
 friend class Controller;                                                               //!< Pour les m�thodes d'�dition.
    public :
        //! @name Alias
        //@{
        using Ptr = SharedPtr<Library>;                                                 //!< Pointeur sur biblioth�que.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            Library(PAK::Loader& pLoader);                              //!< Constructeur.
                            ~Library();                                                 //!< Destructeur.
        //@}
        //! @name Gestion des shaders
        //@{
        Shader::Id          addShader(Shader::EType pType, const String& pCode);        //!< Ajout.
        bool                delShader(Shader::Id pId);                                  //!< Suppression.
        void                editShader(Shader::Id pId, const String& pCode);            //!< Edition.
        //@}
        //! @name Gestion des programmes
        //@{
        Program::Id         link(const Program::Modules& pModules);                     //!< Ajout.
        void                unlink(Program::Id pId);                                    //!< Suppression.
        Program::Hdl        program(Program::Id pId);                                   //!< Acc�s.
        //@}
    protected:

        void                save() const;                                               //!< Sauvegarde.
        //! @name Edition
        //@{
        String              code(Shader::Id pId) const;                                 //!< Code d'un shader.
        Shader::EType       type(Shader::Id pId) const;                                 //!< Type d'un shader.
        void                relink(Program::Id pId, const Program::Modules& pModules);  //!< Nouveaux modules d'un programme.
        Program::Modules    modules(Program::Id pId) const;                             //!< Modules d'un programme.
        //@}
        //! @name Compilation / �dition de liens
        //@{
        String              compile(Shader::Id pId, bool* pStatus = nullptr);           //!< Compilation d'un shader.
        String              link(Program::Id, bool* pStatus = nullptr);                 //!< Edition de liens d'un programme.
        //@}
    private:

        PIMPL()
    };
}

#endif  // De SHD_LIBRARY_HH
