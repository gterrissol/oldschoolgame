/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef SHD_ALL_HH
#define SHD_ALL_HH

/*! @file GraphX/SHaDers/All.hh
    @brief Interface publique du module @ref SHaDers.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 26 Octobre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace SHD   //! Shaders GLSL
{
    /*! @namespace SHD
        @version 0.2

        Ce module permet l'utilisation des shaders GLSL.
     */

    /*! @defgroup SHaDers SHaDers : Shaders GLSL
        <b>namespace</b> SHD.
     */

}

#include "Const.hh"
#include "Shader.hh"
#include "Program.hh"
#include "Library.hh"

#endif  // De SHD_ALL_HH
