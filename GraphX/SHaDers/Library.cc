/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Library.hh"

/*! @file GraphX/SHaDers/Library.cc
    @brief M�thodes (non-inline) de la classe SHD::Library.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 28 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#define GL_GLEXT_PROTOTYPES

#include <algorithm>
#include <stdexcept>
#include <tuple>
#include <GL/gl.h>
#include <GL/glext.h>

#include "STL/SharedPtr.hh"

#include "ErrMsg.hh"

namespace
{
    /*! Conversion d'�num�ration C++ => OpenGL.
        @param pEnum Enum�ration de SHD::Shader.
        @return L'�quivalent de @p pEnum en OpenGL.
     */
    GLenum glEnumFromShaderType(SHD::Shader::EType pEnum)
    {
        switch(pEnum)
        {
            case SHD::Shader::eVertex:   return GL_VERTEX_SHADER;
            case SHD::Shader::eFragment: return GL_FRAGMENT_SHADER;
            default:                     return 0;
        }
    }
}

namespace SHD
{
//------------------------------------------------------------------------------
//                               P-Impl de Library
//------------------------------------------------------------------------------

    /*! @brief P-Impl de SHD::Library.
        @version 0.2
     */
    class Library::Private : public MEM::OnHeap
    {
    public:
        //! @name Alias
        //@{
        using ShaderDef   = std::tuple<Shader, Shader::Hdl, bool>;      //!< D�finition d'un shader.
        using ProgramDef  = std::tuple<Program, Program::Hdl, bool>;    //!< D�finition d'un programme.
        //@}
        //! @name Constructeur & destructeur
        //@{
                    Private(PAK::Loader& pLoader);                      //!< Constructeur.
                    ~Private();                                         //!< Destructeur.
        //@}
        Shader::Hdl compiledShader(Shader::Id);                         //!< R�cup�re un shader compil�.
        std::string shaderInfoLog(Shader::Hdl pHdl) const;              //!< Log de compilation d'un shader.
        std::string programInfoLog(Program::Hdl pHdl) const;            //!< Log de d'�dition de liens d'un programme.

        Vector<ShaderDef>   mShaders;                                   //!< Shaders GLSL.
        Vector<ProgramDef>  mPrograms;                                  //!< Programmes GLSL.

        PAK::FileHdl        mFileHdl;                                   //!< Handle du fichier de la bibiloth�que de shaders.
    };


    /*! Enum�ration pour l'acc�s aux �l�m�nts des tuples de Library::Private (Library::Private::ShaderDef &
        Library::Private::ProgramDef).
     */
    enum
    {
        eShader     = 0,    //!< Shader.
        eProgram    = 0,    //!< Programme.
        eHdl        = 1,    //!< Handle.
        eCompiled   = 2,    //!< Statut de compilation.
        eLinked     = 2     //!< Statut d'�dtion de liens.
    };


    /*! @param pLoader Donn�es de la biblioth�que.
     */
    Library::Private::Private(PAK::Loader& pLoader)
        : mShaders{}
        , mPrograms{}
        , mFileHdl{pLoader.fileHdl()}
    {
        U32 lCount{};

        pLoader >> lCount;
        mShaders.reserve(lCount);
        for(auto lC = k0UL; lC < lCount; ++lC)
        {
            mShaders.emplace_back(pLoader, Shader::kNullHdl, false);
        }

        pLoader >> lCount;
        mPrograms.reserve(lCount);
        for(auto lC = k0UL; lC < lCount; ++lC)
        {
            mPrograms.emplace_back(pLoader, Program::kNullHdl, false);
        }
    }


    /*! Grand nettoyage de printemps...
     */
    Library::Private::~Private()
    {
        for(const auto& lProgramDef : mPrograms)
        {
            for(const auto& lShaderId : std::get<eProgram>(lProgramDef).modules())
            {
                const auto& lShaderDef = mShaders[lShaderId];
                glDetachShader(std::get<eHdl>(lProgramDef), std::get<eHdl>(lShaderDef));
            }
            glDeleteProgram(std::get<eHdl>(lProgramDef));
        }
        for(const auto& lShaderDef : mShaders)
        {
            glDeleteShader(std::get<eHdl>(lShaderDef));
        }
    }

    /*! @param pId Identifiant du shader � r�cup�rer (et � compiler au besoin).
        @return Le hanlde OpenGL du shader @p pId compil�.
     */
    Shader::Hdl Library::Private::compiledShader(Shader::Id pId)
    {
        if (mShaders.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntCompileInvalidShader + std::to_string(pId)};
        }

        auto&   lShaderDef  = mShaders[pId];
        auto&   lHandle     = std::get<eHdl>(lShaderDef);
        if (lHandle == Shader::kNullHdl)
        {
            if ((lHandle = Shader::Hdl{glCreateShader(glEnumFromShaderType(std::get<eShader>(lShaderDef).type()))}) == Shader::kNullHdl)
            {
                throw std::runtime_error{kShaderCreationFailed + std::to_string(pId)};
            }
        }
        if (!std::get<eCompiled>(lShaderDef))
        {
            String lCode = std::get<eShader>(lShaderDef).code();
            const char* lCCode = lCode.c_str();

            glShaderSource(lHandle, 1, &lCCode, nullptr);
            glCompileShader(lHandle);

            int lStatus{};
            glGetShaderiv(lHandle, GL_COMPILE_STATUS, &lStatus);
            if (lStatus == GL_FALSE)
            {
                throw std::invalid_argument{shaderInfoLog(lHandle)};
            }
            else
            {
                std::get<eCompiled>(lShaderDef) = true;
            }
        }

        return lHandle;
    }


    /*! @param pHdl Handle du shader dont r�cup�rer le journal de compilation.
     */
    std::string Library::Private::shaderInfoLog(Shader::Hdl pHdl) const
    {
        int         lLogLen{};
        glGetShaderiv(pHdl, GL_INFO_LOG_LENGTH, &lLogLen);
        std::string lLogInfo(lLogLen, '\0');
        int         lLen{};
        glGetShaderInfoLog(pHdl, lLogLen, &lLen, &lLogInfo[0]);

        return lLogInfo;
    }

    /*! @param pHdl Handle du programme dont r�cup�rer le journal d'�dition de liens.
     */
    std::string Library::Private::programInfoLog(Program::Hdl pHdl) const
    {
        int         lLogLen{};
        glGetProgramiv(pHdl, GL_INFO_LOG_LENGTH, &lLogLen);
        std::string lLogInfo(lLogLen, '\0');
        int         lLen{};
        glGetProgramInfoLog(pHdl, lLogLen, &lLen, &lLogInfo[0]);

        return lLogInfo;
    }


//------------------------------------------------------------------------------
//                     Library : Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! @copydetails Library::Private::Private(PAK::Loader& pLoader)
     */
    Library::Library(PAK::Loader& pLoader)
        : pthis{pLoader}
    { }


    //! Defaulted.
    Library::~Library() = default;


//------------------------------------------------------------------------------
//                         Library : Gestion des shaders
//------------------------------------------------------------------------------

    /*! @param pType Type du nouveau shader.
        @param pCode Code source du nouveau shader.
        @return L'id du nouveau shader.
     */
    Shader::Id Library::addShader(Shader::EType pType, const String& pCode)
    {
        // L'algorithme n'est clairement pas optimal, mais il n'y aura pas des milliers de shaders, et puis cette m�thode ne sera appel�e que dans l'�diteur.
        auto lFreeSlot = std::find_if(pthis->mShaders.cbegin(), pthis->mShaders.cend(), [](const Private::ShaderDef& pShaderDef)
        {
            return !std::get<eShader>(pShaderDef).isValid();
        });
        if (lFreeSlot != pthis->mShaders.end())
        {
            ASSERT_EX(std::get<eHdl>(*lFreeSlot) == Shader::kNullHdl, kInvalidShaderIsCompiled, return Shader::kNullId;)
            auto    lIndex = std::distance(pthis->mShaders.cbegin(), lFreeSlot);
            std::get<eShader>(pthis->mShaders[lIndex]) = std::move(Shader{pType, pCode});
            return Shader::Id(lIndex);
        }
        else
        {
            auto    lId = pthis->mShaders.size();
            pthis->mShaders.push_back(std::make_tuple(Shader{pType, pCode}, Shader::kNullHdl, false));
            return Shader::Id(lId);
        }
    }

    /*! @param pId Identifiant du shader � supprimer.
        @retval true  Si le shader a pu �tre supprim�,
        @retval false S'il �tait encore utilis� par au moins un programme.
     */
    bool Library::delShader(Shader::Id pId)
    {
        if (pthis->mShaders.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntDeleteInvalidShader + std::to_string(pId)};
        }

        for(const auto& lProgramDef : pthis->mPrograms)
        {
            const auto& lModules = std::get<eProgram>(lProgramDef).modules();
            if (std::find(lModules.begin(), lModules.end(), pId) != lModules.end())
            {
                // Le shader est toujours utilis� : suppression interdite.
                return false;
            }
        }

        std::get<eShader>(  pthis->mShaders[pId]).edit(String{});
        std::get<eHdl>(     pthis->mShaders[pId]) = Shader::kNullHdl;
        std::get<eCompiled>(pthis->mShaders[pId]) = false;

        return true;
    }

    /*! @param pId   Identifiant du shader � �diter.
        @param pCode Nouveau code source.
     */
    void Library::editShader(Shader::Id pId, const String& pCode)
    {
        if (pthis->mShaders.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntEditInvalidShader + std::to_string(pId)};
        }

        auto&   lShaderDef  = pthis->mShaders[pId];
        std::get<eShader>(  lShaderDef).edit(pCode);
        std::get<eCompiled>(lShaderDef) = false;

        // Les programmes utilisant ce shader devront �tre relink�s.
        for(auto& lProgramDef : pthis->mPrograms)
        {
            const auto& lModules = std::get<eProgram>(lProgramDef).modules();
            if (std::find(lModules.begin(), lModules.end(), pId) != lModules.end())
            {
                glDetachShader(std::get<eHdl>(lProgramDef), std::get<eHdl>(lShaderDef));
                std::get<eLinked>(lProgramDef) = false;
            }
        }
    }


//------------------------------------------------------------------------------
//                       Library : Gestion des programmes
//------------------------------------------------------------------------------

    /*! @param pModules Identifiants des modules avec lesquels cr�er un nouveau programme.
        @return L'identifiant du nouveau programme.
     */
    Program::Id Library::link(const Program::Modules& pModules)
    {
        // Encore une fois, l'algorithme n'est pas optimal, mais...
        auto lFreeSlot = std::find_if(pthis->mPrograms.cbegin(), pthis->mPrograms.cend(), [](const Private::ProgramDef& pProgramDef)
        {
            return !std::get<eProgram>(pProgramDef).isValid();
        });
        if (lFreeSlot != pthis->mPrograms.end())
        {
            ASSERT_EX(std::get<eHdl>(*lFreeSlot) == Shader::kNullHdl, kInvalidProgramIsCompiled, return Program::kNullId;)
            auto    lIndex = std::distance(pthis->mPrograms.cbegin(), lFreeSlot);
            std::get<eProgram>(pthis->mPrograms[lIndex]) = std::move(Program{pModules});
            return Program::Id(lIndex);
        }
        else
        {
            auto    lId = pthis->mPrograms.size();
            pthis->mPrograms.emplace_back(pModules, Program::kNullHdl, false);
            return Program::Id(lId);
        }
    }

    /*! @param pId Identifiant du programme � supprimer.
     */
    void Library::unlink(Program::Id pId)
    {
        if (pthis->mPrograms.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntDeleteInvalidProgram + std::to_string(pId)};
        }

        auto&   lProgramDef = pthis->mPrograms[pId];
        if (std::get<eHdl>(lProgramDef) != Program::kNullHdl)
        {
            for(const auto& lShaderId : std::get<eProgram>(lProgramDef).modules())
            {
                const auto& lShaderDef = pthis->mShaders[lShaderId];
                glDetachShader(std::get<eHdl>(lProgramDef), std::get<eHdl>(lShaderDef));
            }
        }
        glDeleteProgram(std::get<eHdl>(lProgramDef));
        std::get<eProgram>(lProgramDef) = std::move(Program{Program::Modules{}});
    }

    /*! @param pId Identifiant du programme � r�cup�rer.
        @return Le hanlde OpenGL du programme @p pId link�.
     */
    Program::Hdl Library::program(Program::Id pId)
    {
        if (pthis->mPrograms.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntGetInvalidProgram + std::to_string(pId)};
        }

        auto&   lProgramDef = pthis->mPrograms[pId];
        auto&   lHandle     = std::get<eHdl>(lProgramDef);
        if (lHandle == Program::kNullHdl)
        {
            if ((lHandle = Program::Hdl{glCreateProgram()}) == Program::kNullHdl)
            {
                throw std::runtime_error{kProgramCreationFailed + std::to_string(pId)};
            }
        }

        if (!std::get<eLinked>(lProgramDef))
        {
            const auto& lModules = std::get<eProgram>(lProgramDef).modules();
            for(auto lShaderId : lModules)
            {
                if (!std::get<eCompiled>(pthis->mShaders[lShaderId]))
                {
                    glAttachShader(lHandle, pthis->compiledShader(lShaderId));
                }
            }

            glLinkProgram(lHandle);

            int lStatus{};
            glGetProgramiv(lHandle, GL_LINK_STATUS, &lStatus);
            if (lStatus == GL_FALSE)
            {
                throw std::invalid_argument{pthis->programInfoLog(lHandle)};
            }
            else
            {
                std::get<eLinked>(lProgramDef) = true;
            }
        }

        return lHandle;
    }


//------------------------------------------------------------------------------
//                         Library : Sauvegarde
//------------------------------------------------------------------------------

    //!
    void Library::save() const
    {
        PAK::Saver  lSaver{pthis->mFileHdl};

        lSaver << U32(pthis->mShaders.size());
        for(const auto& lShaderDef : pthis->mShaders)
        {
            const auto& lShader = std::get<eShader>(lShaderDef);
            lSaver << lShader.type();
            lSaver << lShader.code();
        }
        lSaver << U32(pthis->mPrograms.size());
        for(const auto& lProgramDef : pthis->mPrograms)
        {
            const auto& lProgram = std::get<eProgram>(lProgramDef);
            const auto& lModules = lProgram.modules();
            lSaver << U16(lModules.size());
            for(auto& lM : lModules)
            {
                lSaver << lM;
            }
        }
    }


//------------------------------------------------------------------------------
//                               Library : Edition
//------------------------------------------------------------------------------

    /*! @param pId Id du shader dont r�cup�rer le code.
        @return Le code source du shader @p pId.
     */
    String Library::code(Shader::Id pId) const
    {
        if (size_t(pId) < pthis->mShaders.size())
        {
            return std::get<eShader>(pthis->mShaders[pId]).code();
        }
        else
        {
            return {};
        }
    }

    /*! @param pId Id du shader dont r�cup�rer le type.
        @return Le type du shader @p pId.
     */
    Shader::EType Library::type(Shader::Id pId) const
    {
        if (size_t(pId) < pthis->mShaders.size())
        {
            return std::get<eShader>(pthis->mShaders[pId]).type();
        }
        else
        {
            return Shader::eNone;
        }
    }

    /*! @param pId      Programme � relier.
        @param pModules Nouveaux modules du programme @p pId.
     */
    void Library::relink(Program::Id pId, const Program::Modules& pModules)
    {
        if (pthis->mPrograms.size() <= size_t(pId))
        {
            throw std::runtime_error{kCouldntRelinkInvalidProgram + std::to_string(pId)};
        }

        std::get<eLinked>(pthis->mPrograms[pId]) = false;
        std::get<eProgram>(pthis->mPrograms[pId]) = std::move(Program{pModules});
    }

    /*! @param pId Id du programme dont r�cup�rer la liste de modules.
        @return Les modules du programme @p pId.
     */
    Program::Modules Library::modules(Program::Id pId) const
    {
        if (size_t(pId) < pthis->mPrograms.size())
        {
            return std::get<eProgram>(pthis->mPrograms[pId]).modules();
        }
        else
        {
            return {};
        }
    }


//------------------------------------------------------------------------------
//                   Library : Compilation / Edition de Liens
//------------------------------------------------------------------------------

    /*! @param pId     Id du shader � compiler.
        @param pStatus Statut de compilation (optionel).
        @return Le journal de compilation.
     */
    String Library::compile(Shader::Id pId, bool* pStatus)
    {
        try
        {
            std::get<eCompiled>(pthis->mShaders[pId]) = false;
            auto    lLog = pthis->shaderInfoLog(pthis->compiledShader(pId)).c_str();
            if (pStatus != nullptr)
            {
                *pStatus = true;
            }
            return lLog;
        }
        catch(std::exception& e)
        {
            if (pStatus != nullptr)
            {
                *pStatus = false;
            }
            return e.what();
        }
    }

    /*! @param pId     Id du programme � lier.
        @param pStatus Statut d'�dition de liens (optionel).
        @return Le journal d'�dition de liens.
     */
    String Library::link(Program::Id pId, bool* pStatus)
    {
        try
        {
            std::get<eLinked>(pthis->mPrograms[pId]) = false;
            auto    lLog = pthis->programInfoLog(program(pId)).c_str();
            if (pStatus != nullptr)
            {
                *pStatus = true;
            }
            return lLog;
        }
        catch(std::exception& e)
        {
            if (pStatus != nullptr)
            {
                *pStatus = false;
            }
            return e.what();
        }
    }
}
