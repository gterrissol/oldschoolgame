/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "SHaDers.hh"

/*! @file GraphX/SHaDers/SHaDers.cc
    @brief D�finitions diverses du module @ref SHaDers.
    @author @ref Guillaume_Terrissol
    @date 5 Avril 2015 - 18 Novembre 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ErrMsg.hh"

#include "Const.hh"

namespace SHD
{
//------------------------------------------------------------------------------
//                              Shaders Principaux
//------------------------------------------------------------------------------

    const Program::Id   kDefaultProgram{0};
    const Program::Id   kPickingProgram{1};
    const Program::Id   kMegaTexProgram{2};


//------------------------------------------------------------------------------
//                       D�finition des Messages d'Erreur
//------------------------------------------------------------------------------

    REGISTER_ERR_MSG(kCouldntCompileInvalidShader,  "N'a pu compiler le shader : #",            "Couldn't compile the shader : #")
    REGISTER_ERR_MSG(kCouldntDeleteInvalidShader,   "N'a pu supprimer le shader : #",           "Couldn't delete the shader : #")
    REGISTER_ERR_MSG(kCouldntDeleteInvalidProgram,  "N'a pu supprimer le programme : #",        "Couldn't delete the program : #")
    REGISTER_ERR_MSG(kCouldntEditInvalidShader,     "N'a pu �diter le shader : #",              "Couldn't edit the shader : #")
    REGISTER_ERR_MSG(kCouldntGetInvalidProgram,     "N'a pu obtenir le programme invalide : #", "Couldn't get invalid program : #")
    REGISTER_ERR_MSG(kCouldntRelinkInvalidProgram,  "N'a pu obtenir relier le programme : #",   "Couldn't relink invalid program : #")
    REGISTER_ERR_MSG(kInvalidProgramIsCompiled,     "Programme invalide compil�.",              "Invalid program is compiled.")
    REGISTER_ERR_MSG(kInvalidShaderIsCompiled,      "Shader invalide compil�.",                 "Invalid shader is compiled.")
    REGISTER_ERR_MSG(kProgramCreationFailed,        "Eched de cr�ation du programme : #",       "Program creation failed : #")
    REGISTER_ERR_MSG(kShaderCreationFailed,         "Echec de cr�ation du shader : #",          "Shader creation failed : #")
}
