find_package(OpenGL)

if(OPENGL_FOUND)
    set(GraphX_MODULES CoLoR SHaDers GEOmetry TOM GraphicDeVice LIghT MATerial APPearance CAMera RenDeR GraphXView)
    foreach(module ${GraphX_MODULES})
        add_subdirectory(${module})
    endforeach()

    add_bundle(fr.osg.gx SOURCES fr.osg.gx.cc VERSION 3.2.0 DEPENDS SDL fr.osg.gdk ARCHIVES ${GraphX_MODULES})

    set(GL_CXXFLAGS ${OPENGL_INCLUDE_DIR})
    set(GL_LDFLAGS  ${OPENGL_LIBRARIES})

    use_externals(fr.osg.gx GL)
else()
    message(FATAL_ERROR "OpenGL not found")
endif()
