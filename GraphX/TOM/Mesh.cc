/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Mesh.hh"

/*! @file GraphX/TOM/Mesh.cc
    @brief M�thodes (non-inline) de la classe TOM::cl_Mesh.
    @author @ref Guillaume_Terrissol
    @date 30 Octobre 2002 - 20 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "GEOmetry/BaseGeometry.hh"
#include "STL/SharedPtr.hh"
#include "STL/Vector.hh"

namespace TOM
{
//------------------------------------------------------------------------------
//                                    P-Impl
//------------------------------------------------------------------------------

    /*! @brief P-Impl de TOM::Mesh.
        @version 0.2
     */
    class Mesh::Private : public MEM::OnHeap
    {
    public:

        Private(ConstGeoPtr pGeo);  //!< Constructeur.
    };


//------------------------------------------------------------------------------
//                             P-Impl : Constructeur
//------------------------------------------------------------------------------

    /*!
     */
    Mesh::Private::Private(ConstGeoPtr /*pGeo*/) { }


//------------------------------------------------------------------------------
//                                Mesh : M�thodes
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pGeometry Maillage � optimiser
     */
    Mesh::Mesh(ConstGeoPtr pGeometry)
        : pthis(pGeometry)
    { }


    /*! Tri le maillage pass�e en param�tre au constructeur.
        @return Une liste de triangles optimis�e compatible avec la g�om�trie fournie pr�c�demment
     */
    Mesh::OptTrListPtr Mesh::sort()
    {
        return nullptr;
    }
}
