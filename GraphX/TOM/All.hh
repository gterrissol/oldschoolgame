/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TOM_ALL_HH
#define TOM_ALL_HH

/*! @file GraphX/TOM/All.hh
    @brief Interface publique du module @ref TOM.
    @author @ref Guillaume_Terrissol
    @date 30 Octobre 2002 - 24 Mars 2008
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */


namespace TOM   //! Totaly Ordered Mesh.
{
    /*! @namespace TOM
        @version 4.1

        Impl�mentation de maillages progressifs gr�ce aux TOM (5.0).
     */

    /*! @defgroup TOM TOM : Totally Ordered Meshes
        <b>namespace</b> TOM.
     */
}

#include "Mesh.hh"

#endif  // De TOM_ALL_HH
