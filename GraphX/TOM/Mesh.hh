/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef TOM_MESH_HH
#define TOM_MESH_HH

/*! @file GraphX/TOM/Mesh.hh
    @brief En-t�te de la classe TOM::Mesh.
    @author @ref Guillaume_Terrissol
    @date 30 Octobre 2002 - 19 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"
#include "MEMory/PImpl.hh"
#include "GEOmetry/GEOmetry.hh"
#include "STL/SharedPtr.hh"

namespace TOM
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Maillage progressif.
        @version 0.21

     */
    class Mesh : public MEM::Auto<Mesh>
    {
    public:
        //! @name Pointeurs
        //@{
        typedef SharedPtr<GEO::Geometry const>          ConstGeoPtr;    //!< ... sur g�om�trie constante.
        typedef SharedPtr<GEO::OptimizedTriangleList>   OptTrListPtr;   //!< ... sur liste de triangles optimis�e.
        //@}
        //! @name M�thodes
        //@{
                        Mesh(ConstGeoPtr pGeo);                         //!< Constructeur.
        OptTrListPtr    sort();                                         //!< Tri.
        //@}

    private:

        FORBID_COPY(Mesh)
        PIMPL()
    };
}

#endif  // De TOM_MESH_HH
