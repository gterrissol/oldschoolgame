/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GDK_LOG.hh"

/*! @file CHecK/GDK_LOG.cc
    @brief M�thodes (non-inline) de la classe GDK_LOG.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cmath>
#include <fstream>
#include <memory>

#include "CoLoR/RGB.hh"
#include "CoLoR/RGBA.hh"
#include "ERRor/Exception.hh"
#include "ERRor/Log.hh"
#include "MATH/Const.hh"
#include "MATH/M3.hh"
#include "MATH/M4.hh"
#include "MATH/Q.hh"
#include "MATH/V2.hh"
#include "MATH/V3.hh"
#include "MATH/V4.hh"
#include "MEMory/MemoryMgr.hh"
#include "STL/String.hh"
#include "UTIlity/Rect.hh"

#include "Utils.hh"

CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(GDK_LOG, "LOG");

//------------------------------------------------------------------------------
//                                  "Interface"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    GDK_LOG::~GDK_LOG() { }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    void GDK_LOG::testPrint()
    {
        LOG::Log*   lLogs     [2]   = { &LOG::Cout, &LOG::Cerr };
        const char* lFileNames[2]   = { "stdout", "stderr" };
        FILE*       lStreams  [2]   = { stdout, stderr };

        const std::string   lReference  =
            "Hello world\n"
            "-1 - 4294967295 == 0\n";

        for(int lI = 0; lI < 2; ++lI)
        {
            {
                RedirectStream  lRedirector(lFileNames[lI], lStreams[lI]);
                (*lLogs[lI]) << "Hello world\n";
                (*lLogs[lI]) << I32(0xFFFFFFFF) << " - " << U32(0xFFFFFFFF) << " == " << F32(k0F) << LOG::Endl;
            }
            std::string     lContent("");
            {
                std::fstream    lFile(lFileNames[lI], std::fstream::in);
                std::string     lLine("");
                while(!lFile.eof())
                {
                    getline(lFile, lLine);
                    if (!lLine.empty())
                    {
                        lContent += lLine + '\n';   // Retour chariot sauf sur derni�re ligne vide.
                    }
                }
            }
            std::string lMessage = std::string("Echec d'�criture vers ") + lFileNames[lI];
            CPPUNIT_ASSERT_MESSAGE(lMessage.c_str(), lReference == lContent);
            CPPUNIT_ASSERT(remove(lFileNames[lI]) == 0);
        }
    }


    /*!
     */
    void GDK_LOG::testPlug()
    {
        StringStream    lStringStream;
        LOG::Cout.plug(&lStringStream);

        LOG::Cout << "Hello world\n";
        LOG::Cout << 0 << '\t' << reinterpret_cast<const void*>(0x12345678) << '\t' << F32(0.5F);

        const std::string   lReference  = "Hello world\n0\t0x12345678\t0.5";
        CPPUNIT_ASSERT_MESSAGE("D�rivation de flux incorrecte", lStringStream.str() == lReference);
    }


    /*!
     */
    void GDK_LOG::testPrintBuiltIn()
    {
        StringStream    lStringStream;
        LOG::Cout.plug(&lStringStream);

        LOG::Cout << "bool : " << true << " / " << false << LOG::Endl;

        LOG::Cout << "char : " << 'a' << LOG::Endl;
        LOG::Cout << "signed char : " << I8(0xFF) << LOG::Endl;
        LOG::Cout << "unsigned char : " << U8(0xFF) << LOG::Endl;
        LOG::Cout << "signed short : " << I16(0xFFFF) << LOG::Endl;
        LOG::Cout << "unsigned short : " << U16(0xFFFF) << LOG::Endl;
        LOG::Cout << "signed long : " << I32(0xFFFFFFFF) << LOG::Endl;
        LOG::Cout << "unsigned long : " << U32(0xFFFFFFFF) << LOG::Endl;
        LOG::Cout << "signed long long : " << (I64(0xFFFFFFFF) << 32) + I64(0xFFFFFFFF) << LOG::Endl;
        LOG::Cout << "unsigned long long : " << (U64(0xFFFFFFFF) << 32) + U64(0xFFFFFFFF) << LOG::Endl;
        LOG::Cout << "signed int : " << static_cast<signed int>(0xFFFFFFFF) << LOG::Endl;
        LOG::Cout << "unsigned int : " << static_cast<unsigned int>(0xFFFFFFFF) << LOG::Endl;

        LOG::Cout << "float : " << kPi << LOG::Endl;
        LOG::Cout << "double : " << M_PI << LOG::Endl;

        LOG::Cout << "const char* : " << "Hello world" << LOG::Endl;
        LOG::Cout << "const void* : " << reinterpret_cast<const void*>(0x12345678) << LOG::Endl;

        const std::string   lReference =
            "bool : true / false\n"
            "char : a\n"
            "signed char : -1\n"
            "unsigned char : 255\n"
            "signed short : -1\n"
            "unsigned short : 65535\n"
            "signed long : -1\n"
            "unsigned long : 4294967295\n"
            "signed long long : -1\n"
            "unsigned long long : 18446744073709551615\n"
            "signed int : -1\n"
            "unsigned int : 4294967295\n"
            "float : 3.14159\n"
            "double : 3.14159\n"
            "const char* : Hello world\n"
            "const void* : 0x12345678\n";

        CPPUNIT_ASSERT_MESSAGE("Echec d'�criture des types �l�mentaires", lStringStream.str() == lReference);
    }


    /*!
     */
    void GDK_LOG::testPrintClass()
    {
        std::unique_ptr<MEM::MemoryMgr>     lMemoryMgr(new MEM::MemoryMgr());
        StringStream                        lStringStream;
        LOG::Cout.plug(&lStringStream);

        LOG::Cout << "Exception : " << ERR::Exception("This a what() message") << LOG::Endl;

        LOG::Cout << "Matrix 3x3 :\n" << MATH::M3(MATH::Q(MATH::V3(k0F, k0F, k1F), k1F)) << LOG::Endl;
        LOG::Cout << "Matrix 4 x 4 :\n" << MATH::M4(MATH::M3(MATH::Q(MATH::V3(k0F, k0F, k1F), k1F))) << LOG::Endl;
        LOG::Cout << "Quaternion : " << MATH::Q(MATH::V3(k0F, k0F, k1F), kPiBy6) << LOG::Endl;
        LOG::Cout << "Vector 2 : " << MATH::V2(k0F, k1F) << LOG::Endl;
        LOG::Cout << "Vector 3 : " << MATH::V3(k0F, k1F, kE) << LOG::Endl;
        LOG::Cout << "Vector 4 : " << MATH::V4(k0F, k1F, kE, kPi) << LOG::Endl;

        LOG::Cout << "RGB : " << CLR::RGB(k0F, k1F, k1By3) << LOG::Endl;
        LOG::Cout << "RGBA : " << CLR::RGBA(k0F, k1F, k1By3, k1By6) << LOG::Endl;

        LOG::Cout << "Rect : " << UTI::Rect(- k1F, k1F, k1F, - k1F) << LOG::Endl;

        LOG::Cout << "String : " << String("Hello world") << LOG::Endl;

        const std::string   lReference =
            "Exception : This a what() message\n"
            "Matrix 3x3 :\n"
            "[ (0.540302, 0.841471, 0) ]\n"
            "[ (-0.841471, 0.540302, 0) ]\n"
            "[ (0, -0, 1) ]\n"
            "\n"
            "Matrix 4 x 4 :\n"
            "[ (0.540302, 0.841471, 0, 0) ]\n"
            "[ (-0.841471, 0.540302, 0, 0) ]\n"
            "[ (0, -0, 1, 0) ]\n"
            "[ (0, 0, 0, 1) ]\n"
            "\n"
            "Quaternion : (0, 0, 0.258819, 0.965926)\n"
            "Vector 2 : (0, 1)\n"
            "Vector 3 : (0, 1, 2.71828)\n"
            "Vector 4 : (0, 1, 2.71828, 3.14159)\n"
            "RGB : { 0; 1; 0.333333 }\n"
            "RGBA : { 0; 1; 0.333333; 0.166667 }\n"
            "Rect : (-1, 1)-(1, -1) : (2 x 2)\n"
            "String : Hello world\n";

        CPPUNIT_ASSERT_MESSAGE("Echec d'�criture des objets du moteur", lStringStream.str() == lReference);
    }


    /*!
     */
    void GDK_LOG::testMarkup()
    {
        StringStream    lStringStream;
        LOG::Cout.plug(&lStringStream);

        // Cas nominal.
        LOG::Cout << "Header\n";
        LOG::Cout << LOG::Start("Markup1");
        LOG::Cout << "Body1\n";
        LOG::Cout << LOG::Start("Markup2") << "Body2\nData\n";
        LOG::Cout << LOG::Finish << "Body1 (sequel)\n" << LOG::Finish << "Footer" << LOG::Endl;

        const std::string   lReference1 =
            "Header\n"
            "<Markup1>Body1\n"
            "<Markup2>Body2\n"
            "Data\n"
            "</Markup2>Body1 (sequel)\n"
            "</Markup1>Footer\n";
        CPPUNIT_ASSERT_MESSAGE("Les balises de log ne fonctionnent pas comme attendu", lStringStream.str() == lReference1);

        lStringStream.str("");

        // "Cassure" du balisage.
        LOG::Cout << "Header" << LOG::Endl;
        LOG::Cout << LOG::Start("Markup1");
        LOG::Cout << "Body1" << LOG::Endl;
        LOG::Cout << LOG::Start("Markup2");
        LOG::Cout << "Body2" << LOG::Endl << "Data" << LOG::Endl;
        LOG::Cout << LOG::Finish;
        LOG::Cout << "Body1 (sequel)" << LOG::Endl;
        LOG::Cout << LOG::Finish;
        LOG::Cout << "Footer" << LOG::Endl;

        const std::string   lReference2 =
            "Header\n"
            "<Markup1>Body1</Markup1>\n"
            "<Markup2>Body2</Markup2>\n"
            "Data\n"
            "Body1 (sequel)\n"
            "Footer\n";
        CPPUNIT_ASSERT_MESSAGE("Les balises de log ne fonctionnent pas comme attendu", lStringStream.str() == lReference2);
    }
