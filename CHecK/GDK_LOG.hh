/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_GDK_LOG_HH
#define CHK_GDK_LOG_HH

#include "CHecK.hh"

/*! @file CHecK/GDK_LOG.hh
    @brief En-t�te de la classe GDK_LOG.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cppunit/extensions/HelperMacros.h>

//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Test du module LOG.
        @version 0.9
     */
    class GDK_LOG : public CppUnit::TestFixture
    {
        // Enregistrement des Test
        CPPUNIT_TEST_SUITE(GDK_LOG);
        CPPUNIT_TEST(testPrint);
        CPPUNIT_TEST(testPlug);
        CPPUNIT_TEST(testPrintBuiltIn);
        CPPUNIT_TEST(testPrintClass);
        CPPUNIT_TEST(testMarkup);
        CPPUNIT_TEST_SUITE_END();
    public:
        //! @name "Interface"
        //@{
virtual         ~GDK_LOG();         //!< Destructeur.
        //@}

    private:
        //! @name Tests
        //@{
        void    testPrint();        //!< ... standard des logs.
        void    testPlug();         //!< ... de la d�rivation des logs.
        void    testPrintBuiltIn(); //!< ... de l'affichage des types �l�mentaires.
        void    testPrintClass();   //!< ... de l'affichage des objets du moteur.
        void    testMarkup();       //!< ... des balises.
        //@}
    };

#endif  // De CHK_GDK_LOG_HH
