/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GDK_PAcKage.hh"

/*! @file CHecK/GDK_PAcKage.cc
    @brief M�thodes (non-inline) de la classe GDK_PAcKage.
    @author @ref Guillaume_Terrissol
    @date 13 F�vrier 2009 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/MemoryMgr.hh"
#include "PAcKage/PakFile.hh"
#include "STL/Vector.hh"

CPPUNIT_TEST_SUITE_REGISTRATION(GDK_PAcKage);

//------------------------------------------------------------------------------
//                                  "Interface"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    GDK_PAcKage::~GDK_PAcKage() { }

    /*!
     */
    void GDK_PAcKage::setUp()
    {
        new MEM::MemoryMgr();
    }


    /*!
     */
    void GDK_PAcKage::tearDown()
    {
        delete MEM::MemoryMgr::get();
    }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    void GDK_PAcKage::testPakFile()
    {
        Vector<U32> lData;
        Vector<U32> lRead(1024, k0UL);
        for(size_t lD = 0; lD < lRead.size(); ++lD)
        {
            lData.push_back(U32(lD));
        }

        {PAK::PakFile   lFile("test.bin", I32(20));
        lFile.seek(lFile.dataStart().value());
        lFile.write(addressof(lData[0]), U32(lData.size() * sizeof(U32)));
        //{PAK::PakFile   lFile("test.bin", PAK::eEditor);
        lFile.seek(lFile.dataStart().value());
        lFile.read(addressof(lRead[0]), U32(lRead.size() * sizeof(U32)));}

        bool    lOk = true;
        for(size_t lD = 0; lD < lRead.size(); ++lD)
        {
            if (lData[lD] != lRead[lD])
            {
//printf("%d %d %d\n", int(lD), int(lData[lD]), int(lRead[lD]));
                lOk = false;
            }
        }

        CPPUNIT_ASSERT_MESSAGE("Permanence des donn�es invalides", lOk);

        remove("test.bin");
    }
