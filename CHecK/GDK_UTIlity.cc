/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GDK_UTIlity.hh"

/*! @file CHecK/GDK_UTIlity.cc
    @brief M�thodes (non-inline) de la classe GDK_UTIlity.
    @author @ref Guillaume_Terrissol
    @date 21 Janvier 2009 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/Const.hh"
#include "MEMory/MemoryMgr.hh"
#include "UTIlity/Rect.hh"

CPPUNIT_TEST_SUITE_REGISTRATION(GDK_UTIlity);

//------------------------------------------------------------------------------
//                                  "Interface"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    GDK_UTIlity::~GDK_UTIlity() { }

    /*!
     */
    void GDK_UTIlity::setUp()
    {
        new MEM::MemoryMgr();
    }


    /*!
     */
    void GDK_UTIlity::tearDown()
    {
        delete MEM::MemoryMgr::get();
    }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    void GDK_UTIlity::testRect()
    {
        const F32   k2F(2.0F);

        CPPUNIT_ASSERT_MESSAGE("Rectangle par d�faut erron�",  UTI::Rect().isEmpty());
        CPPUNIT_ASSERT_MESSAGE("Rectangle par d�faut erron�", !UTI::Rect().isNull());
        CPPUNIT_ASSERT_MESSAGE("Rectangle par d�faut erron�", !UTI::Rect().isValid());

        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�", UTI::Rect(k0F, k0F, k0F, k0F).isEmpty());
        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�", UTI::Rect(k0F, k0F, k0F, k0F).isNull());
        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�", UTI::Rect(k0F, k0F, k0F, k0F).isValid());

        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�", !UTI::Rect(- k1F, k1F, k1F, - k1F).isEmpty());
        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�", !UTI::Rect(- k1F, k1F, k1F, - k1F).isNull());
        CPPUNIT_ASSERT_MESSAGE("Statut de rectangle erron�",  UTI::Rect(- k1F, k1F, k1F, - k1F).isValid());

        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(  k0F, k0F, k0F,  k0F), UTI::Pointf(k0F, k0F)));
        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(- k1F, k1F, k1F, -k1F), UTI::Pointf(k0F, k0F)));
        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(- k1F, k1F, k1F, -k1F), UTI::Pointf(- k1F, - k1F)));
        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(- k1F, k1F, k1F, -k1F), UTI::Pointf(- k1F,   k1F)));
        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(- k1F, k1F, k1F, -k1F), UTI::Pointf(  k1F, - k1F)));
        CPPUNIT_ASSERT_MESSAGE("Appartenance de points erron�e", contains(UTI::Rect(- k1F, k1F, k1F, -k1F), UTI::Pointf(  k1F,   k1F)));

        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(  k0F, k0F, k0F,   k0F), UTI::Rect(- k1F, k1F, k1F, -k1F)) == UTI::eInside);
        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(- k1F, k1F, k1F, - k1F), UTI::Rect(  k0F, k0F, k0F,  k0F)) == UTI::eOverlaps);
        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(- k1F, k1F, k1F, - k1F), UTI::Rect(- k1F, k1F, k1F, -k1F)) == UTI::eInside);

        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(- k1F, k1F, k1F, - k1F), UTI::Rect(- k2F, k2F, k2F, - k2F)) == UTI::eInside);
        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(- k2F, k2F, k2F, - k2F), UTI::Rect(- k1F, k1F, k1F, - k1F)) == UTI::eOverlaps);

        CPPUNIT_ASSERT_MESSAGE("Intersections erron�es", intersects(UTI::Rect(- k2F, k2F, - k1F, k1F), UTI::Rect(k1F, - k1F, k2F, - k2F)) == UTI::eOutside);

        CPPUNIT_ASSERT_MESSAGE("", UTI::Rect(k0F, k0F, k0F, k0F).width() == k0F);
        CPPUNIT_ASSERT_MESSAGE("", UTI::Rect(k0F, k0F, k0F, k0F).height() == k0F);
        CPPUNIT_ASSERT_MESSAGE("", UTI::Rect(- k1F, k1F, k1F, - k1F).width() == k2F);
        CPPUNIT_ASSERT_MESSAGE("", UTI::Rect(- k1F, k1F, k1F, - k1F).height() == k2F);
    }
