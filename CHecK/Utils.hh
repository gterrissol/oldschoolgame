/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_UTILS_HH
#define CHK_UTILS_HH

#include "CHecK.hh"

/*! @file CHecK/Utils.hh
    @brief Classes et fonctions utilitaires.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdio>
#include <string>
#include <vector>

#include "ERRor/Log.hh"

//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Redirection de flux syst�me.
        @version 0.9

        Instancier cette classe permet de rediriger (en RAII) le contenu d'un flux (stdout ou stderr) vers
        un fichier.
     */
    class RedirectStream
    {
    public:
        //! @name Constructeur & destructeur
        //@{
        RedirectStream(const char* pFileName, FILE* pStream);   //!< Constructeur.
        ~RedirectStream();                                      //!< Destructeur.
        //@}

    private:
        //! @name Descripteurs de fichier
        int mOrigin;                                            //!< ... initial
        int mDuplicate;                                         //!< ... dupliqu�.
        //@}
    };


    /*! @brief Flux de sortie d'une commande syst�me.
        @version 0.9

        Instancier cette classe permet de r�cup�rer, sous forme de cha�ne de caract�res, le r�sultat d'une
        commande syst�me.
     */
    class CommandDump
    {
    public:
        //! @name M�thodes
        //@{
                            CommandDump(const std::string& pCommand);   //!< Constructeur.
        const std::string&  output() const;                             //!< Flux r�sultant.
        //@}

    private:

        std::string mOutput;                                            //!< Flux r�sultant.
    };


    /*! @brief Flux cha�ne.
        @version 0.8

        Pour bon nombre de tests, le contenu des traces peut fournir une bonne r�f�rence pour valider le
        comportement attendu. Afin de r�cup�rer ces traces, une instance de cette classes peut �tre
        install�e en d�rivation d'un log.<br>
        Le contenu du flux sera accessible via Str().
     */
    class StringStream : public LOG::Log::OutStream
    {
    public:
        //! @name Constructeur & destructeur
        //@{
                            StringStream();                     //!< Constructeur par d�faut.
virtual                     ~StringStream();                    //!< Destructeur.
        //@}
        //! @name Edition / r�cup�ration du contenu
        //@{
        const std::string&  str() const;                        //!< R�cup�ration du contenu.
        void                str(const std::string& pStr);       //!< Edition du contenu.
        //@}

    private:
        //! @name Affichage
        //@{
        void                print(bool pBool) override;                 //!< ... d'un bool�en.
        void                print(char pChar) override;                 //!< ... d'un caract�re.
        void                print(signed char pInt) override;           //!< ... d'un entier 8 bits sign�.
        void                print(unsigned char pInt) override;         //!< ... d'un entier 8 bits non-sign�.
        void                print(signed short pInt) override;          //!< ... d'un entier 16 bits sign�.
        void                print(unsigned short pInt) override;        //!< ... d'un entier 16 bits non-sign�.
        void                print(signed long pInt) override;           //!< ... d'un entier 32 bits sign�.
        void                print(unsigned long pInt) override;         //!< ... d'un entier 32 bits non-sign�.
        void                print(signed long long pInt) override;      //!< ... d'un entier 64 bits sign�.
        void                print(unsigned long long pInt) override;    //!< ... d'un entier 64 bits non-sign�.
        void                print(signed int pInt) override;            //!< ... d'un entier sign�.
        void                print(unsigned int pInt) override;          //!< ... d'un entier non-sign�.
        void                print(float pFloat) override;               //!< ... d'un r�el simple pr�cision.
        void                print(double pFloat) override;              //!< ... d'un r�el double pr�cision.
        void                print(const char* pString) override;        //!< ... d'une cha�ne de caract�res C.
        void                print(const void* pPointer) override;       //!< ... d'un pointeur.
        void                flush() override;                           //!< Vidage du buffer.
        void                markup(const char* pName) override;         //!< D�but ou fin d'un groupe.

        std::vector<std::string>    mMarkupStack;                       //!< Liste de balises ouvertes.
        std::string                 mString;                            //!< Cha�ne r�sultante.
    };

#endif  // De CHK_UTILS_HH
