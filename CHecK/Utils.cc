/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "Utils.hh"

/*! @file CHecK/Utils.cc
    @brief M�thodes et fonctions utilitaires (non-inline).
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <unistd.h>

//------------------------------------------------------------------------------
//                                Redirect Stream
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pFileName Nom du fichier vers lequel rediriger le contenu du flux <i>pStream</i>
        @param pStream   Flux � rediriger
     */
    RedirectStream::RedirectStream(const char* pFileName, FILE* pStream)
        : mOrigin(fileno(pStream))
        , mDuplicate(dup(mOrigin))
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        freopen(pFileName, "w+", pStream);
#pragma GCC diagnostic pop
    }
    
    
    /*! Destructeur.
     */
    RedirectStream::~RedirectStream()
    {
        dup2(mDuplicate, mOrigin);
    }


//------------------------------------------------------------------------------
//                                 Command Dump
//------------------------------------------------------------------------------

    /*! Constructeur.
        @param pCommand Commande syst�me � ex�cuter, dans le r�pertoire actuel (CHecK). Un encha�nement
        d'instructions est possible, mais sur une seule ligne (voir la syntaxe bash)
        @note Si <i>pCommand</i> �choue, Output() retournera une cha�ne vide
     */
    CommandDump::CommandDump(const std::string& pCommand)
        : mOutput("")
    {
        RedirectStream  lRedirector("dump.log", stdout);
        if (system(pCommand.c_str()) == 0)
        {
            std::fstream    lInfo("dump.log", std::fstream::in);
            while(!lInfo.eof())
            {
                std::string lLine;
                getline(lInfo, lLine);

                mOutput += (mOutput.empty() ? "" : "\n") + lLine;
            }
        }
        remove("dump.log");
    }


    /*! @return Le texte affich� en sortie de la commande pass�e en param�tre du constructeur
     */
    const std::string& CommandDump::output() const
    {
        return mOutput;
    }


//------------------------------------------------------------------------------
//                  String Stream : Constructeur & destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    StringStream::StringStream()
        : mMarkupStack()
        , mString("")
    { }


    /*!
     */
    StringStream::~StringStream() { }


//------------------------------------------------------------------------------
//               String Stream : Edition / R�cup�ration du Contenu
//------------------------------------------------------------------------------

    /*! 
     */
    const std::string& StringStream::str() const
    {
        return mString;
    }


    /*!
     */
    void StringStream::str(const std::string& pStr)
    {
        mString = pStr;
    }


//------------------------------------------------------------------------------
//                           String Stream : Affichage
//------------------------------------------------------------------------------

    /*! @copydoc LOG::Log::OutStream::print(bool)
        @note C'est la valeur C++ de <i>pBool</i> qui sera affich�e (ie: true ou false)
     */
    void StringStream::print(bool pBool)
    {
        std::ostringstream  lStream;
        lStream << (pBool ? "true" : "false");
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(char)
     */
    void StringStream::print(char pChar)
    {
        std::ostringstream  lStream;
        lStream << pChar;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(signed char)
     */
    void StringStream::print(signed char pInt)
    {
        std::ostringstream  lStream;
        lStream << int(pInt);
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(unsigned char)
     */
    void StringStream::print(unsigned char pInt)
    {
        std::ostringstream  lStream;
        lStream << int(pInt);
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(signed short)
     */
    void StringStream::print(signed short pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(unsigned short)
     */
    void StringStream::print(unsigned short pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(signed long)
     */
    void StringStream::print(signed long pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(unsigned long)
     */
    void StringStream::print(unsigned long pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(signed long long)
     */
    void StringStream::print(signed long long pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(unsigned long long)
     */
    void StringStream::print(unsigned long long pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(signed int)
     */
    void StringStream::print(signed int pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(unsigned int)
     */
    void StringStream::print(unsigned int pInt)
    {
        std::ostringstream  lStream;
        lStream << pInt;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(float)
     */
    void StringStream::print(float pFloat)
    {
        std::ostringstream  lStream;
        lStream << pFloat;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(double)
     */
    void StringStream::print(double pFloat)
    {
        std::ostringstream  lStream;
        lStream << pFloat;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::print(const char*)
     */
    void StringStream::print(const char* pString)
    {
        mString += pString;
    }


    /*! @copydoc Log::OutStream::print(const void*)
     */
    void StringStream::print(const void* pPointer)
    {
        std::ostringstream  lStream;
        lStream << pPointer;
        mString += lStream.str();
    }


    /*! @copydoc Log::OutStream::Flush()
     */
    void StringStream::flush()
    {
        for(auto lMarkupI = mMarkupStack.rbegin(); lMarkupI != mMarkupStack.rend(); ++lMarkupI)
        {
            print(lMarkupI->c_str());
        }

        mMarkupStack.clear();
        mString += '\n';
    }


    /*! @copydoc Log::OutStream::Markup(const char*)
     */
    void StringStream::markup(const char* pName)
    {
        if ((pName == 0) || (pName[0] == '\0'))
        {
            if (!mMarkupStack.empty())
            {
                print(mMarkupStack.back().c_str());
                mMarkupStack.pop_back();
            }
            // else, pas d'affichage d'erreur (pas � ce niveau).
        }
        else
        {
            mMarkupStack.push_back(std::string("</") + pName + ">");
            print((std::string("<") + pName + ">").c_str());
        }
    }
