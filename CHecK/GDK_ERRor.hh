/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_GDK_ERROR_HH
#define CHK_GDK_ERROR_HH

#include "CHecK.hh"

/*! @file CHecK/GDK_ERRor.hh
    @brief En-t�te de la classe GDK_ERRor.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cppunit/extensions/HelperMacros.h>

#include "ERRor/ERRor.hh"

//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Test du module ERRor.
        @version 0.9
     */
    class GDK_ERRor : public CppUnit::TestFixture
    {
        // Enregistrement des Test
        CPPUNIT_TEST_SUITE(GDK_ERRor);
        CPPUNIT_TEST(testAssertions);
#ifdef ASSERTIONS
        CPPUNIT_TEST_EXCEPTION(testAssertionsThrow, ERR::Exception);
#endif  // De ASSERTIONS
        CPPUNIT_TEST(testAssertionsHandler);
        CPPUNIT_TEST(testWarning);
        CPPUNIT_TEST(testLanguage);
        CPPUNIT_TEST_EXCEPTION(testException, ERR::Exception);
        CPPUNIT_TEST_SUITE_END();
    public:
        //! @name "Interface"
        //@{
virtual         ~GDK_ERRor();               //!< Destructeur.
        //@}

    private:
        //! @name Tests
        //@{
        void    testAssertions();           //!< ... des assertions.
#ifdef ASSERTIONS
        void    testAssertionsThrow();      //!< ... des assertions non-s�curi�es.
#endif  // De ASSERTIONS
        void    testAssertionsHandler();    //!< ... du gestionnaire personnalis� d'assertions.
        void    testWarning();              //!< ... des avertissements.
        void    testLanguage();             //!< ... du changement de langue.
        void    testException();            //!< ... des exceptions.
        //@}
    };

#endif  // De CHK_GDK_ERROR_HH
