/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_ALL_HH
#define CHK_ALL_HH

/*! @file CHecK/All.hh
    @brief Interface publique du module @ref CHecK.
    @author @ref Guillaume_Terrissol
    @date 27 D�cembre 2008 - 11 Avril 2009
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace CHK   //! <Descriptif du module>
{
    /*! @namespace CHK
        @version 0.22

        Ensemble de tests unitaires destin�s � valider le code, et d�tecter les r�gressions.
     */

    /*! @defgroup CHecK CHecK : Tests unitaires.
        <b>classes</b> GDK_ERRor, GDK_LOG...
     */

}

#include "GDK_LOG.hh"
#include "GDK_ERRor.hh"
#include "GDK_MEMory.hh
#include "GDK_PAcKage.hh"
#include "GDK_UTIlity.hh"

#endif  // De CHK_ALL_HH
