/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_GDK_MEMORY_HH
#define CHK_GDK_MEMORY_HH

#include "CHecK.hh"

/*! @file CHecK/GDK_MEMory.hh
    @brief En-t�te de la classe GDK_MEMory.
    @author @ref Guillaume_Terrissol
    @date 21 Janvier 2009 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cppunit/extensions/HelperMacros.h>

#include "MEMory/MEMory.hh"

//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Test du module MEMory.
        @version 0.2
     */
    class GDK_MEMory : public CppUnit::TestFixture
    {
        // Enregistrement des Test
        CPPUNIT_TEST_SUITE(GDK_MEMory);
        CPPUNIT_TEST(testMemoryMgr);
        CPPUNIT_TEST(testAllocator);
        CPPUNIT_TEST_EXCEPTION(testAllocatorOverflow, std::bad_alloc);
        CPPUNIT_TEST(testBuiltIn);
        CPPUNIT_TEST(testArray);
        CPPUNIT_TEST(testMemoryModels);
        CPPUNIT_TEST_SUITE_END();
    public:
        //! @name "Interface"
        //@{
virtual         ~GDK_MEMory();              //!< Destructeur.
        //@}

    private:
        //! @name Tests
        //@{
        void    testMemoryMgr();            //!< ... du gestionnaire m�moire.
        void    testAllocator();            //!< ... de l'allocateur STL "custom".
        void    testAllocatorOverflow();    //!< ... du d�bordement de capacit�.
        void    testBuiltIn();              //!< ... des types int�gr�s.
        void    testArray();                //!< ... des tableaux int�gr�s.
        void    testMemoryModels();         //!< ... des "mod�les" m�moire.
        //@}
    };

#endif  // De CHK_GDK_MEMORY_HH
