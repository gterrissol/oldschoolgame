/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "CHecK.hh"

/*! @file ENGine/CHecK/CHecK.cc
    @brief D�finitions diverses du module @ref CHecK.
    @author @ref Guillaume_Terrissol
    @date 27 D�cembre 2008 - 20 D�cembre 2017
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cstdlib>
#include <ctime>
#include <fstream>

#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TextTestProgressListener.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/XmlOutputterHook.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/tools/XmlDocument.h>
#include <cppunit/tools/XmlElement.h>
#include <cppunit/ui/text/TestRunner.h>

#include <QFile>
#include <QRegExp>
#include <QTextStream>

#include <OSGi/Activator.hh>
#include <OSGi/ApplicationService.hh>

#include "Utils.hh"

namespace CHK
{
//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Ajout d'informations au rapport de test.
        @version 0.3
     */
    class AddContextHook : public CppUnit::XmlOutputterHook
    {
    public:
        //! @name M�thodes
        //@{
virtual         ~AddContextHook();                                          //!< Destructeur.
        void    beginDocument(CppUnit::XmlDocument* pDocument) override;    //!< Ajout d'un en-t�te.
        //@}
    };


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    AddContextHook::~AddContextHook() { }


    /*!
     */
    void AddContextHook::beginDocument(CppUnit::XmlDocument* pDocument)
    {
        if (pDocument == 0)
        {
            return;
        }

        // Versions.
        CppUnit::XmlElement* lContext = new CppUnit::XmlElement("Context", "");
        lContext->addAttribute("Version", VERSION);

        std::string lCompiler   = CommandDump("g++ -dumpversion").output();
        lContext->addAttribute("Compiler", !lCompiler.empty() ? lCompiler : std::string("unknown"));

        std::string lSystem     = CommandDump("uname -s").output();
        lContext->addAttribute("System", !lSystem.empty() ? lSystem : std::string("unknown"));
        
        std::string lRelease    = CommandDump("uname -r").output();
        lContext->addAttribute("Release", !lRelease.empty() ? lRelease : std::string(""));

        std::string lRevisionNumber = "";
        std::string lRevision       = "unknow";
        {
            RedirectStream  lRedirect("hg.txt", stdout);
            if (system("hg summary -R " SRC_DIR) == 0)
            {
                QFile       lFile("hg.txt");
                if (lFile.open(QFile::ReadOnly | QFile::Text))
                {
                    QTextStream lStream(&lFile);
                    while(!lStream.atEnd())
                    {
                        QString lLine = lStream.readLine();
                        if (lLine.contains("parent"))
                        {
                            QRegExp lRE(R"(\b(\d+):(([a-f]|\d)+)\b)");
                            if (lLine.contains(lRE))
                            {
                                lRevisionNumber = qPrintable(lRE.cap(1));
                                lRevision       = qPrintable(lRE.cap(2));
                            }
                        }
                    }
                }
            }
        }
        remove("hg.txt");

        lContext->addAttribute("RevisionNumber", lRevisionNumber);
        lContext->addAttribute("Revision",       lRevision);

        // Time and Date.
        time_t      lRawtime;
        struct tm*  lTimeInfo;

        time(&lRawtime);
        lTimeInfo = localtime(&lRawtime);

        std::string lBuffer(100, ' ');
        strftime(&lBuffer[0], 100, "%Y-%m-%dT%H:%M:%S", lTimeInfo);
        lContext->addAttribute("Date", lBuffer.c_str());    // Keep .c_str() !

        pDocument->rootElement().addElement(lContext);

        // Externals
        CppUnit::XmlElement* lExternals = new CppUnit::XmlElement("Externals", "");
        lContext->addElement(lExternals);

        CppUnit::XmlElement*    lQt  = new CppUnit::XmlElement("External", QT_VERSION_STR);
        lQt->addAttribute("name", "Qt");
        lExternals->addElement(lQt);

        CppUnit::XmlElement*    lOSGi = new CppUnit::XmlElement("External", OSGi_VERSION);
        lOSGi->addAttribute("name", "OSGi");
        lExternals->addElement(lOSGi);

        CppUnit::XmlElement*    lSDL  = new CppUnit::XmlElement("External", SDL_VERSION);
        lSDL->addAttribute("name", "SDL");
        lExternals->addElement(lSDL);

        CppUnit::XmlElement*    lSLB = new CppUnit::XmlElement("External", SLB_VERSION);
        lSLB->addAttribute("name", "SLB");
        lExternals->addElement(lSLB);
    }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    class CHKService : public OSGi::ApplicationService
    {
    public:
        CHKService() { }
        virtual ~CHKService() { }

    private:
        /*! Fonction principale du programme <b>osg</b>, lanc�e par main().
            @param pArgC Nombre d'arguments pass�s sur la ligne de commande
            @param pArgV Liste des arguments pass�s sur la ligne de commande
            @return 0 si l'application s'est correctement termin�e, un code d'erreur sinon
         */
        int process(const Args&, OSGi::Context*) override final
        {
            CppUnit::TestResult                 lResult;
            CppUnit::TestResultCollector        lCollector;     // R�cup�ration des r�sultats.
            lResult.addListener(&lCollector);
            CppUnit::TextTestProgressListener   lProgressor;    // Trace des r�sultats.
            lResult.addListener(&lProgressor);
            CppUnit::TestRunner                 lRunner;
            lRunner.addTest(CppUnit::TestFactoryRegistry::getRegistry("LOG").makeTest());       // Les tests portant sur LOG,
            lRunner.addTest(CppUnit::TestFactoryRegistry::getRegistry("ERRor").makeTest());     // ERRor & MEMory doivent �tre 
            lRunner.addTest(CppUnit::TestFactoryRegistry::getRegistry("MEMory").makeTest());    // ex�cut�s avant les autres.
            lRunner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());

            // C'est parti.
            lRunner.run(lResult);

            // Cr�e un rapport de test
            {
                std::ofstream           lFile("Report.xml");
                CppUnit::XmlOutputter   lXml(&lCollector, lFile);
                CHK::AddContextHook     lHook;
                lXml.addHook(&lHook);
                lXml.setStyleSheet("Report.xsl");
                lXml.write();
            }

            // Return error code 1 if the one of test failed.
            return lCollector.wasSuccessful() ? 0 : 1;
        }
    };

    class Activator : public OSGi::Activator
    {
    public:
                Activator() : OSGi::Activator() { }
virtual         ~Activator() { }
    private:
virtual void    doStart(OSGi::Context* pContext) override final
                {
                    pContext->out() << "Starting OSG" << std::endl;
                    OSGi::ApplicationService::Ptr   lApp{new CHKService()};
                    pContext->services()->checkIn(BUNDLE_NAME, lApp);
                    pContext->out() << "OSG started" << std::endl;
                }
virtual void    doStop(OSGi::Context* pContext) override final
                {
                    pContext->out() << "Stopping OSG" << std::endl;
                    pContext->services()->checkOut(BUNDLE_NAME);
                }
    };
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(CHK::Activator)
OSGI_END_REGISTER_ACTIVATORS()
