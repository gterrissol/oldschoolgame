/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GDK_MEMory.hh"

/*! @file CHecK/GDK_MEMory.cc
    @brief M�thodes (non-inline) de la classe GDK_MEMory.
    @author @ref Guillaume_Terrissol
    @date 21 Janvier 2009 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/All.hh"

#include "Utils.hh"

CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(GDK_MEMory, "MEMory");

//------------------------------------------------------------------------------
//                                  "Interface"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    GDK_MEMory::~GDK_MEMory() { }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    void GDK_MEMory::testMemoryMgr()
    {
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        std::unique_ptr<MEM::MemoryMgr> lMgr(new MEM::MemoryMgr());
        // Allocateur principal.
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 0) &&
                                                                            (lMgr->usedMemorySize()      == 0) &&
                                                                            (lMgr->spareMemorySize()     == 0));
        void*   lV1 = lMgr->allocate(256);
        CPPUNIT_ASSERT_MESSAGE("Allocation incorrecte", lV1 != 0);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 256) &&
                                                                            (lMgr->usedMemorySize()      == 256) &&
                                                                            (lMgr->spareMemorySize()     == 0));
        lMgr->deallocate(lV1);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 0) &&
                                                                            (lMgr->usedMemorySize()      == 0) &&
                                                                            (lMgr->spareMemorySize()     == 0));
        void*   lV2 = lMgr->allocate(128);
        CPPUNIT_ASSERT_MESSAGE("Allocation incorrecte", lV2 != 0);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 128) &&
                                                                            (lMgr->usedMemorySize()      == 256) &&
                                                                            (lMgr->spareMemorySize()     == 128));
        lV1 = lMgr->allocate(256);
        CPPUNIT_ASSERT_MESSAGE("Allocation incorrecte", lV1 != 0);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 384) &&
                                                                            (lMgr->usedMemorySize()      == 512) &&
                                                                            (lMgr->spareMemorySize()     == 128));
        lMgr->deallocate(lV1);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 128) &&
                                                                            (lMgr->usedMemorySize()      == 256) &&
                                                                            (lMgr->spareMemorySize()     == 128));
        lMgr->deallocate(lV2);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 0) &&
                                                                            (lMgr->usedMemorySize()      == 0) &&
                                                                            (lMgr->spareMemorySize()     == 0));
        lV1 = lMgr->allocate(16776960);    // 16383.75 KB (plus grand bloc allouable).
        CPPUNIT_ASSERT_MESSAGE("Allocation incorrecte", lV1 != 0);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 16776960) &&
                                                                            (lMgr->usedMemorySize()      == 16776960) &&
                                                                            (lMgr->spareMemorySize()     == 0));
        lMgr->deallocate(lV1);
        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur m�moire", (lMgr->allocatedMemorySize() == 0) &&
                                                                            (lMgr->usedMemorySize()      == 0) &&
                                                                            (lMgr->spareMemorySize()     == 0));
    }


    /*!
     */
    void GDK_MEMory::testAllocator()
    {
        StringStream    lStringStreamErr;
        LOG::Cerr.plug(&lStringStreamErr);

        std::unique_ptr<MEM::MemoryMgr> lMgr(new MEM::MemoryMgr());

        std::vector<std::vector<U32, MEM::Allocator<U32>>> lVec(32);
        for(size_t lV = 0; lV < lVec.size(); ++lV)
        {
            lVec[lV].resize(65534 * 64, U32(lV));
            CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur STL", lVec[lV][0] == lV);
        }

        CPPUNIT_ASSERT_MESSAGE("Dysfonctionnement de l'allocateur STL", (lMgr->allocatedMemorySize() == 536854528));
    }


    /*!
     */
    void GDK_MEMory::testAllocatorOverflow()
    {
        StringStream    lStringStreamErr;
        LOG::Cerr.plug(&lStringStreamErr);

        std::unique_ptr<MEM::MemoryMgr> lMgr(new MEM::MemoryMgr());

        while(true)
        {
            lMgr->allocate(16776960);
        }
    }


    /*!
     */
    void GDK_MEMory::testBuiltIn()
    {
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        // A venir ...
//        CPPUNIT_FAIL("A coder");
    }


    /*!
     */
    void GDK_MEMory::testArray()
    {
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        // A venir ...
//        CPPUNIT_FAIL("A coder");
    }


    /*!
     */
    void GDK_MEMory::testMemoryModels()
    {
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        // A venir ...
//        CPPUNIT_FAIL("A coder");
    }
