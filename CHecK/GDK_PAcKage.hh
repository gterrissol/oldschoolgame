/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_GDK_PACKAGE_HH
#define CHK_GDK_PACKAGE_HH

#include "CHecK.hh"

/*! @file CHecK/GDK_PAcKage.hh
    @brief En-t�te de la classe GDK_PAcKage.
    @author @ref Guillaume_Terrissol
    @date 13 F�vrier 2009 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include <cppunit/extensions/HelperMacros.h>

//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*! @brief Test du module PAcKAge.
        @version 0.2
     */
    class GDK_PAcKage : public CppUnit::TestFixture
    {
        // Enregistrement des Test
        CPPUNIT_TEST_SUITE(GDK_PAcKage);
        CPPUNIT_TEST(testPakFile);
        CPPUNIT_TEST_SUITE_END();
    public:
        //! @name "Interface"
        //@{
virtual         ~GDK_PAcKage(); //!< Destructeur.
virtual void    setUp();        //!< Pr�paration de la suite de tests.
virtual void    tearDown();     //!< Nettoyage.
        //@}

    private:
        //! @name Tests
        //@{
        void    testPakFile();  //!< ... du pak file.
        
        //@}
    };

#endif  // De CHK_GDK_PACKAGE_HH
