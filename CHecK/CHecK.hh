/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef CHK_CHECK_HH
#define CHK_CHECK_HH

/*! @file CHecK/CHecK.hh
    @brief Pr�-d�clarations du module @ref CHecK.
    @author @ref Guillaume_Terrissol
    @date 27 D�cembre 2008 - 18 Ao�t 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

class GDK_ERRor;
class GDK_LOG;
class GDK_MEMory;
class GDK_PAcKage;
class GDK_UTIlity;
class ORedirectStream;
class OStringStream;

#endif  // De CHK_CHECK_HH
