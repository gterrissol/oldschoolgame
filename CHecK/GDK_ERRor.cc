/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "GDK_ERRor.hh"

/*! @file CHecK/GDK_ERRor.cc
    @brief M�thodes (non-inline) de la classe GDK_ERRor.
    @author @ref Guillaume_Terrissol
    @date 28 D�cembre 2008 - 6 Ao�t 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#define BREAK_POINT() // D�sactive les points d'arr�t.

#include "ERRor/All.hh"

#include "Utils.hh"

CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(GDK_ERRor, "ERRor");

//------------------------------------------------------------------------------
//                                  "Interface"
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    GDK_ERRor::~GDK_ERRor() { }


//------------------------------------------------------------------------------
//                                
//------------------------------------------------------------------------------

    /*!
     */
    void GDK_ERRor::testAssertions()
    {
#ifdef ASSERTIONS
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        bool    lHasAsserted    = false;
        // Assertion r�ussie.
        ASSERT_EX(true, "Echec", lHasAsserted = true;)
        CPPUNIT_ASSERT_MESSAGE("Fonctionnement des assertions incorrect", lStringStream.str().empty() && lHasAsserted == false);

        // Assertion en �chec.
        ASSERT_EX(false, "Echec", lHasAsserted = true;)
        CPPUNIT_ASSERT_MESSAGE("Fonctionnement des assertions incorrect", !lStringStream.str().empty() && lHasAsserted == true);
#endif  // De ASSERTIONS
    }

#ifdef ASSERTIONS
    /*!
     */
    void GDK_ERRor::testAssertionsThrow()
    {
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        // Assertion r�ussie.
        ASSERT(true, "Echec");
        CPPUNIT_ASSERT_MESSAGE("Fonctionnement des assertions incorrect", lStringStream.str().empty());

        // Assertion en �chec.
        ASSERT(false, "Echec");
        CPPUNIT_FAIL("Fonctionnement des assertions incorrect");
    }


    // Gestionnaire d'assertions.
    bool    Handler(const ERR::Assertion& pAssertion)
    {
        LOG::Cerr << pAssertion.mMessage << LOG::Endl;
        return false;
    }
#endif  // De ASSERTIONS

    /*!
     */
    void GDK_ERRor::testAssertionsHandler()
    {
#ifdef ASSERTIONS
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        ERR::Assertion::setCustomHandler(&Handler);

        bool    lHasAsserted    = false;
        // Assertion r�ussie.
        ASSERT_EX(true, "Echec", lHasAsserted = true;)
        CPPUNIT_ASSERT_MESSAGE("Fonctionnement du gestionnaire d'assertions incorrect", lStringStream.str().empty() && lHasAsserted == false);

        // Assertion en �chec.
        ASSERT_EX(false, "Echec", lHasAsserted = true;)
        CPPUNIT_ASSERT_MESSAGE("Fonctionnement des assertions incorrect", lStringStream.str() == "Echec\n" && lHasAsserted == true);

        ERR::Assertion::removeCustomHandler();
#endif  // De ASSERTIONS
    }


    /*!
     */
    void GDK_ERRor::testWarning()
    {
#ifdef ASSERTIONS
        StringStream    lStringStream;
        LOG::Cerr.plug(&lStringStream);

        WARNING("Avertissement standard")
        WARNING_IF(true, "Avertissement sur condition")
        WARNING_IF(false, "Avertissement d�sactiv�")
        LOG::Cerr.unplug();

        const std::string lReference = std::string() +
            ERR::kWarning + "Avertissement standard\n" +
            ERR::kWarning + "Avertissement sur condition\n";
        CPPUNIT_ASSERT_MESSAGE("Trace des avertissements incorrect", lStringStream.str() == lReference);
#endif  // De ASSERTIONS
    }


    /*!
     */
    void GDK_ERRor::testLanguage()
    {
#ifdef ASSERTIONS
        // Pas de langue activ�e par d�faut.
        const std::string   kUntranslated = "Untranslated error message";
        CPPUNIT_ASSERT_MESSAGE("Message d'erreur d'absence de traduction incorrect", ERR::kWarning == kUntranslated);

        { SET_ERR_MSG_LANGUAGE(eFr) }
        std::string lWarningFr  = ERR::kWarning;
        CPPUNIT_ASSERT_MESSAGE("Traduction fran�aise absente", lWarningFr != kUntranslated);

        { SET_ERR_MSG_LANGUAGE(eEn) }
        std::string lWarningEn  = ERR::kWarning;
        CPPUNIT_ASSERT_MESSAGE("Traduction anglaise absente", lWarningEn != kUntranslated);

        CPPUNIT_ASSERT_MESSAGE("Traductions fran�aise et anglaise identiques", lWarningFr != lWarningEn);

        // R�active la traduciton fran�aise.
        { SET_ERR_MSG_LANGUAGE(eFr) }
#endif  // De ASSERTIONS
    }


    /*!
     */
    void GDK_ERRor::testException()
    {
        const char* lMessage = "This is a test";

        try
        {
            LAUNCH_EXCEPTION(lMessage);
        }
        catch(ERR::Exception& pE)
        {
#ifdef ASSERTIONS
            CPPUNIT_ASSERT_MESSAGE("Message what() inattendu", pE.what() == lMessage);
#else
            CPPUNIT_ASSERT_MESSAGE("Message what() inattendu", pE.what().empty());
#endif  // De ASSERTIONS

            throw;
        }
    }
