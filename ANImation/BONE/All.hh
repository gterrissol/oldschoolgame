/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef BONE_BONE_hh
#define BONE_BONE_hh

/*! @file ANImation/BONE/All.hh
    @brief En-t�te du module BONE.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DYNamics/DYNamics.hh"
#include "ERRor/ERRor.hh"
#include "MATH/MATH.hh"
#include "MEMory/MEMory.hh"
#include "STL/STL.hh"

namespace BONE   //! Os et squelette.
{
    /*! @namespace BONE
        @version 0.1

        Ce module regroupe tous les objets destin�s � la gestion des squelettes des objets 3D anim�s.
     */
}

#include "BaseBone.hh"
#include "SimpleObject.hh"

#endif  // De BONE_BONE_hh
