/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef BONE_BASEBONE_HH
#define BONE_BASEBONE_HH

/*! @file ANImation/BONE/BaseBone.hh
    @brief En-t�te de la classe BONE::Bone.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 18 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/Q.hh"
#include "MATH/V3.hh"
#include "MEMory/Class.hh"
#include "STL/SharedPtr.hh"

namespace BONE
{
    /*! @brief Os.
        @version 0.3

        Os d'un squelette.
     */
    class Bone : public MEM::OnHeap
    {
    public:
        //! @name Types de pointeur
        //@{
        typedef WeakPtr<Bone const> ConstBonePtr;                               //!< Pointeur (constant) sur os.
        typedef WeakPtr<Bone>       BonePtr;                                    //!< Pointeur sur os.
        //@}
        //! @name Constructeur & destructeur
        //@{
                                Bone();                                         //!< Constructeur par d�faut.
                                Bone(BonePtr pParent);                          //!< Constructeur.
virtual                         ~Bone();                                        //!< Destructeur.
        //@}
        //! @name Gestion de la hi�rarchie
        //@{
        inline  void            reparent(BonePtr pParent);                      //!< Modification de la parent�e.
        inline  ConstBonePtr    parent() const;                                 //!< R�cup�ration de l'os parent.
        inline  BonePtr         parent();                                       //!< R�cup�ration de l'os parent.
        //@}
        //! @name R�cup�ration des donn�es
        //@{
        inline  const MATH::V3& position() const;                               //!< Position.
        inline  const MATH::Q&  orientation() const;                            //!< Orientation.
        inline  const MATH::V3& scale() const;                                  //!< Echelle.
                MATH::M4        frame() const;                                  //!< Matrice de transformation.
        //@}
        //! @name Etablissement des donn�es
        //@{
        inline  void            setPosition(const MATH::V3& pPosition);         //!< Position.
        inline  void            setOrientation(const MATH::Q& pOrientation);    //!< Orientation.
        inline  void            setScale(const MATH::V3& pScale);               //!< Echelle.
        //@}

    private:
        //! @name Attributs propres
        //@{
        MATH::V3    mPosition;                                                  //!< Position.
        MATH::Q     mOrientation;                                               //!< Orientation.
        MATH::V3    mScale;                                                     //!< Echelle.
        //@}
        BonePtr     mParent;                                                    //!< Lien vers le p�re.
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline                                
//------------------------------------------------------------------------------

#include "BaseBone.inl"

#endif  // De BONE_BASEBONE_HH
