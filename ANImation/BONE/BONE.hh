/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef BONE_BONE_HH
#define BONE_BONE_HH

/*! @file ANImation/BONE/BONE.hh
    @brief Pr�-d�clarations du moduke BONE.
    @author @ref Guillaume_Terrissol
    @date 28 Novembre 2008 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace DYN
{
    class Bone;
    class SimpleObject;
}

#endif  // De BONE_BONE_HH
