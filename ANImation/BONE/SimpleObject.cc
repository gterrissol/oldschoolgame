/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "SimpleObject.hh"

/*! @file ANImation/BONE/SimpleObject.cc
    @brief M�thodes (non-inline) de la classe BONE::SimpleObject.
    @author @ref Guillaume_Terrissol
    @date 29 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace BONE
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur.
     */
    SimpleObject::SimpleObject(BonePtr pParent)
        : Bone(pParent)
    { }


    /*! Destructeur.
     */    
    SimpleObject::~SimpleObject() { }
}
