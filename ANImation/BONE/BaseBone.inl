/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file ANImation/BONE/BaseBone.inl
    @brief M�thodes inline de la classe BONE::Bone.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace BONE
{
//------------------------------------------------------------------------------
//                           Gestion de la Hi�rarchie
//------------------------------------------------------------------------------

    /*! Modifie la parent�e de l'os (�quivaut � un changement de r�f�rentiel dynamique).
        @warning L'ancien parent ne sera pas pr�venu du changement (il n'a pas � l'�tre, dans le cas g�n�ral)
     */
    inline void Bone::reparent(BonePtr pParent)
    {
        mParent = pParent;
    }


    /*! @return L'os parent.
     */
    inline Bone::ConstBonePtr Bone::parent() const
    {
        return mParent;
    }


    /*! @return L'os parent.
     */
    inline Bone::BonePtr Bone::parent()
    {
        return mParent;
    }


//------------------------------------------------------------------------------
//                           R�cup�ration des Donn�es
//------------------------------------------------------------------------------

    /*! @return La position de l'os
     */
    inline const MATH::V3& Bone::position() const
    {
        return mPosition;
    }


    /*! @return L'orientation de l'os
     */
    inline const MATH::Q& Bone::orientation() const
    {
        return mOrientation;
    }


    /*! @return L'�chelle de l'os
     */
    inline const MATH::V3& Bone::scale() const
    {
        return mScale;
    }


//------------------------------------------------------------------------------
//                           Etablissement des Donn�es
//------------------------------------------------------------------------------

    /*! @param pPosition Nouvelle position de l'os
     */
    inline void Bone::setPosition(const MATH::V3& pPosition)
    {
        mPosition = pPosition;
    }


    /*! @param pOrientation Nouvelle orientation de l'os
     */
    inline void Bone::setOrientation(const MATH::Q& pOrientation)
    {
        mOrientation = pOrientation;
    }


    /*! @param pScale Nouvelle �chelle de l'os
     */
    inline void Bone::setScale(const MATH::V3& pScale)
    {
        mScale = pScale;
    }
}
