/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

/*! @file ANImation/BONE/SimpleObject.inl
    @brief M�thodes inline de la classe BONE::SimpleObject.
    @author @ref Guillaume_Terrissol
    @date 29 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace BONE
{
//------------------------------------------------------------------------------
//                            Gestion de la Dynamique
//------------------------------------------------------------------------------

    /*! @return La dynamique de l'objet
     */
    inline SimpleObject::DynamicsPtr SimpleObject::dynamics()
    {
        return mDynamics;
    }


    /*! @param pDynamics Dynamique � appliquer sur l'objet
     */
    inline void SimpleObject::setDynamics(DynamicsPtr pDynamics)
    {
        mDynamics = pDynamics;
    }
}
