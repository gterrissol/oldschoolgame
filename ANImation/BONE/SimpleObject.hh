/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef BONE_SIMPLEOBJECT_HH
#define BONE_SIMPLEOBJECT_HH

/*! @file ANImation/BONE/SimpleObject.hh
    @brief En-t�te de la classe BONE::SimpleObject.
    @author @ref Guillaume_Terrissol
    @date 29 Octobre 2002 - 25 Mars 2012
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "DYNamics/BaseDynamics.hh"

#include "BaseBone.hh"

namespace BONE
{
    /*! @brief Objet simple.
        @version 0.3

        Objet simple : os sur lequel s'applique une m�canique donn�e (DYN::Dynamics).
     */
    class SimpleObject : public BONE::Bone
    {
    public:
        //! @name Type de pointeur
        //@{
        typedef SharedPtr<DYN::Dynamics>    DynamicsPtr;        //!< Pointeur sur dynamique.
        //@}
        //! @name Constructeur & destructeur
        //@{
                            SimpleObject(BonePtr pParent);      //!< Constructeur.
virtual                     ~SimpleObject();                    //!< Destructeur.
        //@}
        //! @name Gestion de la dynamique
        //@{
        inline  DynamicsPtr dynamics();                         //!< R�cup�ration de la dynamique.
        inline  void        setDynamics(DynamicsPtr pDynamics); //!< Assignation de la dynamique.
        //@}

    private:

                            SimpleObject();                     //!< Constructeur par d�faut.

        //! @name Attribut
        //@{
        DynamicsPtr mDynamics;                                  //!< Dynamique.
        //@}
    };
}


//------------------------------------------------------------------------------
//                                M�thodes inline                                
//------------------------------------------------------------------------------

#include "SimpleObject.inl"

#endif  // De BONE_SIMPLEOBJECT_HH
