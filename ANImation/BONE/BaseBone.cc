/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseBone.hh"

/*! @file ANImation/BONE/BaseBone.cc
    @brief M�thodes (non-inline) de la classe BONE::Bone.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MATH/M3.hh"
#include "MATH/M4.hh"

namespace BONE
{
//------------------------------------------------------------------------------
//                          Constructeur & Destructeur
//------------------------------------------------------------------------------

    /*! Constructeur par d�faut.
     */
    Bone::Bone()
        : mParent()
    { }


    /*! Constructeur.
     */
    Bone::Bone(BonePtr pParent)
        : mParent(pParent)
    { }


    /*! Destructeur.
     */    
    Bone::~Bone() { }


//------------------------------------------------------------------------------
//                           R�cup�ration des Donn�es
//------------------------------------------------------------------------------

    /*! @return La matrice de transformation de l'os
     */
    MATH::M4 Bone::frame() const
    {
        return MATH::M4(MATH::M3(orientation()), position(), scale());
    }
}
