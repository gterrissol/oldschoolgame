/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#include "BaseDynamics.hh"

/*! @file ANImation/DYNamics/BaseDynamics.cc
    @brief M�thodes (non-inline) des classes DYN::Dynamics, DYN::PointDynamics & DYN::SolidDynamics.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 15 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

namespace DYN
{
//------------------------------------------------------------------------------
//                                 Destructeurs
//------------------------------------------------------------------------------

    /*! Destructeur.
     */
    Dynamics::~Dynamics() { }


    /*! Destructeur.
     */
    PointDynamics::~PointDynamics() { }


    /*! Destructeur.
     */
    SolidDynamics::~SolidDynamics() { }
}
