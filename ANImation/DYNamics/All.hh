/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DYN_DYNAMICS_HH
#define DYN_DYNAMICS_HH

/*! @file ANImation/DYNamics/All.hh
    @brief En-t�te du module DYN.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 27 Mars 2015
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "ERRor/ERRor.hh"
#include "MEMory/MEMory.hh"

namespace DYN   //! Dynamique.
{
    /*! @namespace DYN
        @version 0.1

        Ce module est charg� de fournir toutes les routines permettant l'application de la m�canique aux objets du monde.
     */
}

#include "BaseDynamics.hh"

#endif  // De DYN_DYNAMICS_HH
