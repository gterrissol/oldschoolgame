/*  This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,or (at
    your option) any later version.
    For more details, see the GNU General Public License (www.fsf.org or
    the COPYING file somewhere in the package)
 */

#ifndef DYN_BASEDYNAMICS_HH
#define DYN_BASEDYNAMICS_HH

#include "DYNamics.hh"

/*! @file ANImation/DYNamics/BaseDynamics.hh
    @brief En-t�te des classes DYN::Dynamics, DYN::PointDynamics & DYN::SolidDynamics.
    @author @ref Guillaume_Terrissol
    @date 28 Octobre 2002 - 17 Septembre 2011
    @note Ce fichier est diffus� sous licence GPL.
    Consultez le fichier COPYING (ou http://www.fsf.org) pour plus d'informations.
 */

#include "MEMory/Class.hh"

namespace DYN
{
    /*! @brief Classe dynamique.
        @version 0.2

        Interface pour des classes de m�canique.
     */
    class Dynamics : public MEM::OnHeap
    {
    public:
        //! @name Destructeur
        //@{
virtual ~Dynamics();    //!< Destructeur.
        //@}
    };


    /*! @brief Dynamique du point.
        @version 0.2

        Cette sp�cialisation est destin�e � simuler la "m�canique du point".
     */
    class PointDynamics : public Dynamics
    {
    public:
        //! @name Destructeur
        //@{
virtual ~PointDynamics();   //!< Destructeur.
        //@}
    };


    /*! @brief Dynamique du solide.
        @version 0.2

        Cette sp�cialisation est destin�e � simuler la "m�canique du solide".
     */
    class SolidDynamics : public Dynamics
    {
    public:
        //! @name Destructeur
        //@{
virtual ~SolidDynamics();   //!< Destructeur.
        //@}
    };
}

#endif  // De DYN_BASEDYNAMICS_HH
